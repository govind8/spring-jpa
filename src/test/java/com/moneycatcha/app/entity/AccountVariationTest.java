package com.moneycatcha.app.entity;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountVariationTest {

	@Test
	void test() {
		fail("Not yet implemented");
	}

	public List<AccountVariation> getAccountVariationData() {

		AccountVariation.LimitIncrease li = new AccountVariation.LimitIncrease();
//		li.setActionDate(LocalDate.now());
//		li.setNewLimit(new BigDecimal(500000.00));
		
		AccountVariation.AddGuarantor ag = new AccountVariation.AddGuarantor();
		ag.setXGuarantor("Yes Bank");
		
		AccountVariation av1 = new AccountVariation();
		av1.setVariationDescription("First Variation");
		av1.setUniqueID("av-001");
		av1.setXAccountToVary("Govind");
		
		AccountVariation av2 = new AccountVariation();
		av2.setUniqueID("av-002");
		av2.setXLendingGuaranteeToVary("Lending Guarantee");
		
		AccountVariation av3 = new AccountVariation();
		av3.setUniqueID("av-003");
		av3.setLimitIncrease(li);

		AccountVariation av4 = new AccountVariation();
		av4.setUniqueID("av-004");
		av4.setAddGuarantor(ag);
		
		List<AccountVariation> variations = new ArrayList<AccountVariation>();
		variations.add(av1);
		variations.add(av2);
		variations.add(av3);
		variations.add(av4);
		
		return variations;
	}
	
}
