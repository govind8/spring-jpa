package com.moneycatcha.app.entity;

import java.math.BigInteger;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Business.ConcentrationRisk;
import com.moneycatcha.app.entity.Business.Diversification;
import com.moneycatcha.app.entity.Business.ImportExport;
import com.moneycatcha.app.entity.Business.PropertyInvestment;
import com.moneycatcha.app.model.DurationUnitsList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.YesNoIntentList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.service.MessageService;

@Component
@RunWith(SpringRunner.class)
@SpringBootTest
public class BusinessTest {

	@Autowired
	MessageService messageService;
	
	@Test
	public void persist_company_applicant_business_record() {
		messageService.save(setMessageData());
	}


	public Business business() {

		ConcentrationRisk risk = new ConcentrationRisk();
		risk.setConcentrationRiskDetails(RandomStringUtils.randomAlphabetic(10));
		risk.setCustomerOrSupplierConcentration(YesNoList.NO);
		
		Diversification diversification = new Diversification();
		diversification.setDetails(RandomStringUtils.randomAlphabetic(10));
		diversification.setDiversifiedDate(LocalDate.now());
		diversification.setIsDiversified(YesNoList.NO);
		
		ImportExport importexport = new ImportExport();
		importexport.setDetails(RandomStringUtils.randomAlphabetic(10));
		importexport.setIsInvolved(YesNoIntentList.FUTURE_INTENT);
		
		PropertyInvestment propertyInvestment = new PropertyInvestment();
		propertyInvestment.setDetails(RandomStringUtils.randomAlphabetic(10));
		propertyInvestment.setIsInvolved(YesNoIntentList.YES);
		
		Business business = new Business();
		business.setNumberOfEmployees(new BigInteger(RandomStringUtils.randomNumeric(3)));
		business.setNumberOfLocations(new BigInteger(RandomStringUtils.randomNumeric(2)));
		business.setConcentrationRisk(risk);
		business.setDiversification(diversification);
		business.setImportExport(importexport);
		business.setPropertyInvestment(propertyInvestment);

		return business;
	}

	public Message setMessageData() {
		
		// company applicant
//		List<CompanyApplicant> applicants = new ArrayList<CompanyApplicant>();
//		applicants.add(new CompanyApplicant("ca-0003", "abn-003", "acn-004", business(), setContactData()));
//		applicants.add(new CompanyApplicant("ca-0004", "abn-003", "acn-005", business(), setContactData()));
		
		Application application = new Application();
		application.setProductionData(YesNoList.YES);
		application.setUniqueID("app_0003");
//		application.setCompanyApplicant(applicants);
		
		Content content = new Content();
		content.setApplication(application);
		
		Message message = new Message();
		message.setProductionData(YesNoList.YES);
		message.setUniqueID("message_0004");
		message.setContent(content);

		return message;
	}
	
	// Application - Company Applicant - Business Data
	public Contact setContactData() {
	
		PhoneType officefax = new PhoneType();
		officefax.setAustralianDialingCode("61");
		officefax.setCountryCode("0061");
		officefax.setNumber("802002");
		officefax.setOverseasDialingCode("00061");
		
		DurationType duration = new DurationType();
		duration.setLength(new BigInteger(RandomStringUtils.randomNumeric(2)));
		duration.setUnits(DurationUnitsList.MONTHS);
		Contact.ContactPerson person = new Contact.ContactPerson();
		person.setFirstName("govind"); person.setNameTitle(NameTitleList.MR); person.setSurname("karuppiah");
		person.setXContactPerson("00-3303");
		
		Contact contact = new Contact();
		contact.setEmail("govind@gmail.com");
		contact.setUniqueID("contact-001");
		contact.setPreviousRegisteredAddressStartDate(LocalDate.now());
		contact.setOfficeFax(officefax);
		contact.setRegisteredAddressDuration(duration);
		contact.setContactPerson(person);
		
		return contact;
		
	}	
	
}
