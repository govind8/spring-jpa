package com.moneycatcha.app.entity;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.service.MessageService;
import com.moneycatcha.app.testdata.AddBackData;
import com.moneycatcha.app.testdata.BeneficialOwnerData;
import com.moneycatcha.app.testdata.BusinessData;
import com.moneycatcha.app.testdata.CompanyApplicantData;
import com.moneycatcha.app.testdata.ContactData;
import com.moneycatcha.app.testdata.CreditHistoryData;
import com.moneycatcha.app.testdata.ExistingCustomerData;
import com.moneycatcha.app.testdata.FinancialAnalysisData;
import com.moneycatcha.app.testdata.ForeignTaxAssociationData;
import com.moneycatcha.app.testdata.IncomePreviousData;
import com.moneycatcha.app.testdata.IncomePriorData;
import com.moneycatcha.app.testdata.IncomeRecentData;
import com.moneycatcha.app.testdata.IncomeYearToDateData;
import com.moneycatcha.app.testdata.PartnerData;
import com.moneycatcha.app.testdata.ResponsibleLendingData;
import com.moneycatcha.app.testdata.SourceOfWealthData;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CompanyApplicantTest {

	@Autowired
	BeneficialOwnerData beneficialOwnerData;
	
	@Autowired
	AddBackData addbackData;
	
	@Autowired
	BusinessData businessData;
	
	@Autowired
	CompanyApplicantData companyApplicantData;
	
	@Autowired
	ContactData contactData;
	
	@Autowired
	CreditHistoryData creditHistoryData;
	
	@Autowired
	ExistingCustomerData existingCustomerData;
	
	@Autowired
	FinancialAnalysisData finAnalysisData;
	
	@Autowired
	ForeignTaxAssociationData foreignTaxData;
	
	@Autowired
	IncomePreviousData incomePreviousData;
	
	@Autowired
	IncomePriorData incomePriorData;
	
	@Autowired
	IncomeRecentData incomeRecentData;
	
	@Autowired
	IncomeYearToDateData incomeYearToDateData;
	
	@Autowired
	PartnerData partnerData;
	
	@Autowired
	ResponsibleLendingData responsibleData;
	
	@Autowired
	SourceOfWealthData sowData;
	
	@Autowired
	MessageService messageService;
	
	@Test
	public void persist_company_applicant_record() {
		
//		Optional<Message> message = messageService.save(setMessageData());
//		System.out.println("message : " + message);
//		assertNotNull(message);
//		System.out.println("message : " + message.toString());
	}
		
	public List<CompanyApplicant> setCompanyApplicantData() {
		// company applicant
		List<CompanyApplicant> applicants = new ArrayList<CompanyApplicant>();
		for (int i =0; i < 1; i++) {
			CompanyApplicant applicant = new CompanyApplicant();
			applicant.setAbn(RandomStringUtils.randomAlphabetic(15));
			applicant.setAbnVerified(YesNoList.NO);
			applicant.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			applicant.setAuthorisedSignatory(companyApplicantData.authorisedSignatory());
			applicant.setBeneficialOwner(beneficialOwnerData.setBeneficalOwners());
			applicant.setBusiness(businessData.business());
			applicant.setContact(contactData.companyApplicant());
			applicant.setExistingCustomer(existingCustomerData.existingCustomer());
			applicant.setFinancialAnalysis(finAnalysisData.financialAnalysis());
			applicant.setForeignTaxAssociation(foreignTaxData.foreignTaxAssociation());
			applicant.setIncomePrevious(incomePreviousData.incomePrevious());
			applicant.setIncomePrior(incomePriorData.incomePrior());
			applicant.setIncomeRecent(incomeRecentData.incomeRecent());
			applicant.setIncomeYearToDate(incomeYearToDateData.incomeYearToDate());
			applicant.setPartner(partnerData.setPartners());
			applicant.setRelatedLegalEntities(companyApplicantData.relatedLegalEntities());
			applicant.setResponsibleLending(responsibleData.responsibleLendingType());
			applicant.setShareholder(companyApplicantData.shareholder());
			applicant.setSourceOfWealth(sowData.sourceOfWealth());
			applicants.add(applicant);
		}
		
//		// company applicant 1
//		applicants.add(new CompanyApplicant("ca-0001", 
//				RandomStringUtils.randomAlphabetic(15), RandomStringUtils.randomAlphabetic(15), companyApplicantData.signatoriesList(),
//				beneficialOwnerData.setBeneficalOwners(), businessData.businessData(), contactData.companyApplicantContact(),
//				creditHistoryData.setCreditHistories()));
//		
//		// company applicant 2
//		applicants.add(new CompanyApplicant("ca-0002", 
//				RandomStringUtils.randomAlphabetic(15), RandomStringUtils.randomAlphabetic(15), companyApplicantData.declaredIncome(),
//				companyApplicantData.directors(), existingCustomerData.existingCustomer(), finAnalysisData.setData(),
//				foreignTaxData.foreignTaxAssociation()));
//
//		// company applicant 3
//		applicants.add(new CompanyApplicant("ca-0003", 
//				RandomStringUtils.randomAlphabetic(15), RandomStringUtils.randomAlphabetic(15), incomePreviousData.setData(),
//				incomePriorData.setData(), incomeRecentData.setData(), incomeYearToDateData.setData(), partnerData.setPartners()));
//
//		// company applicant 4
//		applicants.add(new CompanyApplicant("ca-0004", 
//				RandomStringUtils.randomAlphabetic(15), RandomStringUtils.randomAlphabetic(15), companyApplicantData.relatedLegalEntities(),
//				responsibleData.setResponsibleLending(), companyApplicantData.shareholder(), sowData.sows()));

		return applicants;
	}

	public Application setApplicationData() {
		Application application = new Application();
		application.setProductionData(YesNoList.YES);
		application.setUniqueID("app_0001");
//		application.setCompanyApplicant(setCompanyApplicantData());
		return application;
	}

	public Message setMessageData() {
	
		Message message = new Message();
		message.setProductionData(YesNoList.YES);
		message.setUniqueID("message_0001");

		Content content = new Content();
		
		Application application = new Application();
		application.setProductionData(YesNoList.YES);
		application.setUniqueID("app_0001");
		// set applicant
//		application.setCompanyApplicant(setCompanyApplicantData());
		
		content.setApplication(application);
		message.setContent(content);
		return message;

	}		
	

}
