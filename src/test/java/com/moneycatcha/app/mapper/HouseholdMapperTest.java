package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Household;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.HouseholdData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Household model to Household entity")
public class HouseholdMapperTest {


	@Autowired
	HouseholdMapper householdMapper;

	HouseholdData householdData = new HouseholdData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Household model to entity")
	public void mapHouseholdModelToEntity() {
		
		List<Household> houseEntity = householdMapper.toHouseholds(modelData());
		assertNotNull(houseEntity);
		assertTrue(houseEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map Household entity to model")
	public void mapHouseholdEntityToModel() {
				
		List<Application.Household> houseModel = householdMapper.fromHouseholds(householdData.households());
		assertNotNull(houseModel);
		assertTrue(houseModel.size() == 4);
	}
	
	/**
	 * Household  - model Data
	 * @return
	 */
	public List<Application.Household> modelData() {
		
		List<Application.Household> households = new ArrayList<Application.Household>();
	
		// Household
		for (int i =0; i < 2; i++) {
			Application.Household household = new Application.Household();
			household.setEducationExpenses(education());
			household.setName(RandomStringUtils.randomAlphabetic(15));
			household.setNumberOfAdults(new BigInteger(RandomStringUtils.randomNumeric(4)));
			household.setNumberOfDependants(new BigInteger(RandomStringUtils.randomNumeric(2)));
			household.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
	
			households.add(household);
		}
		return households;
	}
	
	// Education Expenses
	public Application.Household.EducationExpenses education() {
		Application.Household.EducationExpenses education = new Application.Household.EducationExpenses();
		education.setNumberOfAdultsInFullTimeStudy(new BigInteger(RandomStringUtils.randomNumeric(1)));
		education.setNumberOfAdultsInPartTimeStudy(new BigInteger(RandomStringUtils.randomNumeric(1)));
		education.setNumberOfChildrenInPrivateSchool(new BigInteger(RandomStringUtils.randomNumeric(1)));
		education.setNumberOfChildrenInPublicSchool(new BigInteger(RandomStringUtils.randomNumeric(1)));
		return education;
	}
	
}
