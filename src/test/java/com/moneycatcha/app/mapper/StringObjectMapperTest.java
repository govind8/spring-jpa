package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.moneycatcha.app.repository.RelatedPersonRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.moneycatcha.app.model.Message.Content.Application.AccountVariation;
import com.moneycatcha.app.mapper.StringObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
@DisplayName("Verify Object is successfully converted to String")
public class StringObjectMapperTest {

	@Autowired
	private StringObjectMapper stringObjectMapper;

	@Test
	@DisplayName("Convert Object value to String")
	public void convertObjectToString() throws Exception {

		Object object = new String("test");

		AccountVariation.AddGuarantee ag = new AccountVariation.AddGuarantee();
		ag.setXLendingGuarantee(object);

		String result = stringObjectMapper.objectToString(object);
		
		assertEquals(object.toString(), result);
	}

}

