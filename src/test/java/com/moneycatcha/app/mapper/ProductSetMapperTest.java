package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.ProductSet;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.ProductSetData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from ProductSet model to ProductSet entity")
public class ProductSetMapperTest {

	@Autowired
	ProductSetMapper productMapper;

	ProductSetData setData = new ProductSetData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map ProductSet model to entity")
	public void mapProductSetModelToEntity() {
		
		List<ProductSet> productEntity = productMapper.toProductSets(modelData());
		assertNotNull(productEntity);
		assertTrue(productEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map ProductSet entity to model")
	public void mapProductSetEntityToModel() {
				
		List<Application.ProductSet> productSetModel = productMapper.fromProductSets(setData.productSet());
		assertNotNull(productSetModel);
		assertTrue(productSetModel.size() == 3);
	}
	
	/**
	 * ProductSet  - model Data
	 */
	public List<Application.ProductSet> modelData() {
		
		List<Application.ProductSet> products = new ArrayList<Application.ProductSet>();
		
		// ProductSet
		for (int i =0; i < 2; i++) {
			Application.ProductSet product = new Application.ProductSet();
			product.setLodgementReferenceNumber(RandomStringUtils.randomAlphabetic(10));
			product.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			product.setXPrimaryApplicant(RandomStringUtils.randomAlphabetic(10));
			products.add(product);
		}
		return products;
	}
}
