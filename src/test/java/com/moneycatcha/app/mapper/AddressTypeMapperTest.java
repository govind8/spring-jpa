package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.model.AddressType;
import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.DataSourceList;
import com.moneycatcha.app.model.LevelTypeList;
import com.moneycatcha.app.model.PoBoxTypeList;
import com.moneycatcha.app.model.StreetSuffixList;
import com.moneycatcha.app.model.StreetTypeList;
import com.moneycatcha.app.model.UnitTypeList;
import com.moneycatcha.app.testdata.AddressTypeData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from AddressType model to AddressType entity")
public class AddressTypeMapperTest {

	@Autowired
	AddressTypeMapper addressTypeMapper;
	
	AddressTypeData addressTypeData = new AddressTypeData();

	@Test
	@DisplayName("Map Address model to entity")
	public void mapAddressModelToEntity() {
		
		List<com.moneycatcha.app.entity.AddressType> addressEntity = addressTypeMapper.toAddresses(modelData());
		assertNotNull(addressEntity);
		assertTrue(addressEntity.size() == 2);
		assertEquals(addressEntity.get(0).getCity(), "Perth");
	}
	
	@Test
	@DisplayName("Map Address entity to model")
	public void mapAddressEntityToModel() {
				
		List<com.moneycatcha.app.model.AddressType> addressModel = addressTypeMapper.fromAddresses(addressTypeData.addressList());
		assertNotNull(addressModel);
		assertTrue(addressModel.size() == 2);
	}


	/**
	 * Address Type  - model Data
	 * @return
	 */
	public List<com.moneycatcha.app.model.AddressType> modelData() {
		
		
		AddressType.DeliveryPoint deliveryPoint = new AddressType.DeliveryPoint();
		deliveryPoint.setIdentifier(RandomStringUtils.randomAlphabetic(10));
		deliveryPoint.setIdentifierBarcode(RandomStringUtils.randomAlphabetic(10));
		
		AddressType.DXBox doBox = new AddressType.DXBox();
		doBox.setExchange(RandomStringUtils.randomAlphabetic(10));
		doBox.setNumber(RandomStringUtils.randomAlphanumeric(10));
		doBox.setProvider(RandomStringUtils.randomAlphabetic(10));
		
		AddressType.NonStandard nonstandard = new AddressType.NonStandard();
		nonstandard.setLine1(RandomStringUtils.randomAlphabetic(10));
		nonstandard.setLine2(RandomStringUtils.randomAlphabetic(10));
		
		
		AddressType.POBox poBox = new AddressType.POBox();
		poBox.setExchange(RandomStringUtils.randomAlphabetic(10));
		poBox.setNumber(RandomStringUtils.randomAlphabetic(10));
		poBox.setType(PoBoxTypeList.COMMUNITY_MAIL_BAG);
		
		AddressType.Standard standard = new AddressType.Standard();
		standard.setBuildingName(RandomStringUtils.randomAlphabetic(10));
		standard.setLevel(RandomStringUtils.randomAlphabetic(10));
		standard.setLevelType(LevelTypeList.FLOOR);
		standard.setLotSection(RandomStringUtils.randomAlphabetic(10));
		standard.setStreetName(RandomStringUtils.randomAlphabetic(10));
		standard.setStreetNumber(RandomStringUtils.randomAlphanumeric(3));
		standard.setStreetSuffix(StreetSuffixList.CENTRAL);
		standard.setStreetType(StreetTypeList.ACCESS);
		standard.setLotSection(RandomStringUtils.randomAlphabetic(4));
		standard.setStreetName(RandomStringUtils.randomAlphabetic(10));
		standard.setStreetNumber(RandomStringUtils.randomNumeric(2));
		standard.setToUnitNumber(RandomStringUtils.randomNumeric(2));
		standard.setToStreetNumber(RandomStringUtils.randomNumeric(4));
		standard.setUnit(RandomStringUtils.randomAlphabetic(4));
		standard.setUnitType(UnitTypeList.BUILDING);
		

		AddressType addressType = new AddressType();
		addressType.setDeliveryPoint(deliveryPoint);
		addressType.setDXBox(doBox);
		addressType.setStandard(standard);
		addressType.setNonStandard(nonstandard);
		addressType.setPOBox(poBox);

		addressType.setAustralianPostCode(RandomStringUtils.randomNumeric(5));
		addressType.setAustralianState(AuStateList.WA);
		addressType.setCity("Perth");
		addressType.setCountry(CountryCodeList.AU);
		addressType.setDataSource(DataSourceList.GNAF);
		addressType.setGNAFID(RandomStringUtils.randomAlphabetic(5));
		addressType.setLGAName(RandomStringUtils.randomAlphabetic(5));
		addressType.setLatitude(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		addressType.setLongitude(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		
		List<AddressType> addressTypes = new ArrayList<AddressType>();
		for (int i=0; i < 2; i++) {
			addressTypes.add(addressType);
		}
		
		return addressTypes;
	}
}
