package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.StringReader;

import javax.xml.transform.stream.StreamSource;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.entity.Message;
import com.moneycatcha.app.parsers.validation.LixiUtils;
import com.moneycatcha.app.testdata.MessageData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Message model to Message entity")
public class MessageMapperTest {

    @Autowired
    private Jaxb2Marshaller marshaller;

    @Autowired
    private ObjectMapper jsonObjectMapper;
    
    @Value("classpath:lixi.xml")
    private Resource resourceFile;

    @Autowired
    LixiUtils lixiUtils;
	
	@Autowired
	MessageMapper messageMapper;
	
	@Autowired
	AddressTypeMapper addressMapper;

	MessageData messageData = new MessageData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Message model to entity")
	public void mapMessageModelToEntity() throws Exception {
		Message messageEntity = messageMapper.toMessage(modelData());
      	
       	System.out.println("entity address size : " + messageEntity.getContent().getApplication().getAddress().size());
 		assertNotNull(messageEntity);

	}
	
	@Test
	@DisplayName("Map Message entity to model")
	public void mapMessageEntityToModel() {
				
		com.moneycatcha.app.model.Message messageModel = messageMapper.fromMessage(messageData.message());
		assertNotNull(messageModel);
	}
	
	/**
	 * Message - model Data
	 */
	public com.moneycatcha.app.model.Message modelData() throws Exception{
		
		com.moneycatcha.app.model.Message messageModel;
		
		String sfilePath = resourceFile.getFile().getAbsolutePath();
		String smessage = lixiUtils.xmlAsString(sfilePath);
		messageModel = (com.moneycatcha.app.model.Message) marshaller.unmarshal(new StreamSource(new StringReader(smessage)));
        System.out.println("Xml to POJO: " + jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(messageModel));
//        
//        assertNotNull(messageModel);
//        Message entity = messageMapper.toMessage(messageModel);
//        assertNotNull(entity);
//        if (entity != null) {
//        	System.out.println("entity is not null");
//          List<AddressType> addressTypes = entity.getContent().getApplication().getAddress();
//          if (addressTypes != null) {
//          	System.out.println("size : " + addressTypes.size());
//          }
//          
//          addressTypes.forEach(address -> System.out.println("address: " + address));
//        	
//        	
//        }
//        List<AddressType> addressTypes = addressMapper.toAddressTypes(messageModel.getContent().getApplication().getAddress());
//        if (addressTypes != null) {
//        	System.out.println("size : " + addressTypes.size());
//        }
//        
//        addressTypes.forEach(address -> System.out.println("address: " + address));
         
		return messageModel;		
	}
}
