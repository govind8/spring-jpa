package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.MasterAgreement;
import com.moneycatcha.app.model.MasterAgreementStatusList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.PercentOwnedType;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.testdata.MasterAgreementData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;


@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from MasterAgreement model to MasterAgreement entity")
public class MasterAgreementMapperTest {

	@Autowired
	MasterAgreementMapper masterMapper;

	MasterAgreementData masterData = new MasterAgreementData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map MasterAgreement model to entity")
	public void mapMasterAgreementModelToEntity() {
		
		List<MasterAgreement> masterEntity = masterMapper.toMasterAgreements(modelData());
		assertNotNull(masterEntity);
		assertTrue(masterEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map MasterAgreement entity to model")
	public void mapMasterAgreementEntityToModel() {
				
		List<Application.MasterAgreement> masterModel = masterMapper.fromMasterAgreements(masterData.masterAgreement());
		assertNotNull(masterModel);
		assertTrue(masterModel.size() == 3);
	}
	
	/**
	 * MasterAgreement  - model Data
	 */
	public List<Application.MasterAgreement> modelData() {
		
		List<Application.MasterAgreement> masters = new ArrayList<Application.MasterAgreement>();
	
		// MasterAgreement
		for (int i =0; i < 2; i++) {
			Application.MasterAgreement master = new Application.MasterAgreement();
			master.setDateOfExecution(calendarData.localDateToXML());
			master.setDescription(RandomStringUtils.randomAlphabetic(10));
			master.setEndDate(calendarData.localDateToXML());
			master.setLenderAgreementNumber(RandomStringUtils.randomNumeric(6));
			master.setMasterFacilityLimit(new BigDecimal(RandomStringUtils.randomNumeric(2,5)));
			master.setPercentOwned(percentOwnership());
			master.setProductCode(RandomStringUtils.randomAlphanumeric(5));
			master.setProductName(RandomStringUtils.randomAlphabetic(10));
			master.setStatus(MasterAgreementStatusList.EXISTING);
			master.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			masters.add(master);
		}
		return masters;
	}
	
	
	
	// percentowned
	public PercentOwnedType percentOwnership() {
		PercentOwnedType po = new PercentOwnedType();
		po.setProportions(ProportionsList.SPECIFIED);
		return po;
	}
 
}
