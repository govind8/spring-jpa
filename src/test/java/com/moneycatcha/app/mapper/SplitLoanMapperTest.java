package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.SplitLoan;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.SplitLoanData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from SplitLoan model to SplitLoan entity")
public class SplitLoanMapperTest {


	@Autowired
	SplitLoanMapper loanMapper;

	SplitLoanData loanData = new SplitLoanData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map SplitLoan model to entity")
	public void mapSplitLoanModelToEntity() {
		
		List<SplitLoan> loanEntity = loanMapper.toSplitLoans(modelData());
		assertNotNull(loanEntity);
		assertTrue(loanEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map SplitLoan entity to model")
	public void mapSplitLoanEntityToModel() {
				
		List<Application.SplitLoan> loanModel = loanMapper.fromSplitLoans(loanData.splitLoan());
		assertNotNull(loanModel);
		assertTrue(loanModel.size() == 3);
	}
	
	/**
	 * SplitLoan  - model Data
	 */
	public List<Application.SplitLoan> modelData() {
		
		List<Application.SplitLoan> loans = new ArrayList<Application.SplitLoan>();
	
		// SplitLoan
		for (int i =0; i < 2; i++) {
			Application.SplitLoan loan = new Application.SplitLoan();
			loan.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			loans.add(loan);
		}
		return loans;
	}
	
}
