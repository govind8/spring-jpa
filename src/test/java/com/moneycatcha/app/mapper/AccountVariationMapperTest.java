package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.*;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.AccountVariation;
import com.moneycatcha.app.entity.AccountVariation.AddBorrower;
import com.moneycatcha.app.entity.AccountVariation.AddGuarantor;
import com.moneycatcha.app.entity.AmountInForeignCurrencyType;
import com.moneycatcha.app.model.CurrencyCodeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.VariationTypeList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.AccountVariationData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from AccountVariation model to AccountVariation entity")
public class AccountVariationMapperTest {

	@Autowired
	AccountVariationMapper avmapper;
	
	AccountVariationData avData = new AccountVariationData();
	
	@Test
	@DisplayName("Verify AccountVariation value from model is mapped to entity")
	 public void mapAccountVariationModelToAccountVariationEntity() {
		
		// given
		List<Application.AccountVariation> models = setModelData();
		
		List<AccountVariation> entities = avmapper.toAccountVariations(models);

		
		assertNotNull(entities);
		
		System.out.println("entity account to vary : " + entities.get(0).getXAccountToVary());
	}
	
	
	
	@Test
	@DisplayName("Verify AccountVariation value from entity is mapped to model")
	 public void mapAccountVariationEntityToAccountVariationEntityModel() {
		
		// given

		List<AccountVariation> entities = avData.accountVariationList();
		
		List<Application.AccountVariation> models = avmapper.fromAccountVariations(entities);
		
		assertNotNull(models);
		
		System.out.println("model account to vary : " + models.get(0).getXAccountToVary());
	}
	
	// entity data
	public List<AccountVariation> setEntityData() {
		
		List<AccountVariation> entities = new ArrayList<>();
		
		AccountVariation entity = new AccountVariation();
		entity.setVariationDescription("copy value from entity");
		entity.setXAccountToVary("ACC-VAR-001");
		
		AddGuarantor guarantor = new AddGuarantor();
		guarantor.setXGuarantor("GUARANTOR-001");
		
		entity.setAddGuarantor(guarantor);
		
		entities.add(entity);
		
		return entities;
	}
	
	// set model data for accountVariation
	public List<Application.AccountVariation> setModelData() {
		
		Application.AccountVariation.AddGuaranteeSecurity security = new Application.AccountVariation.AddGuaranteeSecurity();
		security.setXSecurity("model security");
		
		com.moneycatcha.app.model.AmountInForeignCurrencyType modelCurrency = new com.moneycatcha.app.model.AmountInForeignCurrencyType();
		modelCurrency.setAmount(new BigDecimal("200.00"));
		modelCurrency.setCurrencyCode(CurrencyCodeList.AOA);
		
		Application.AccountVariation.LimitIncrease limitIncrease = new Application.AccountVariation.LimitIncrease();
		limitIncrease.setIncreaseAmount(new BigDecimal("100.00"));
		limitIncrease.setIsNewLimitRequestedInForeignCurrency(YesNoList.YES);
		limitIncrease.setNewLimit(new BigDecimal("999.00"));
		limitIncrease.setNewLimitRequestedInForeignCurrency(modelCurrency);
		
		Application.AccountVariation.ReduceLimit reduceLimit = new Application.AccountVariation.ReduceLimit();
		reduceLimit.setNewLimit(new BigDecimal("1000.00"));
		reduceLimit.setIsNewLimitRequestedInForeignCurrency(YesNoList.YES);
		reduceLimit.setNewLimit(new BigDecimal("999.00"));
		reduceLimit.setNewLimitRequestedInForeignCurrency(modelCurrency);

		List<Application.AccountVariation> models = new ArrayList<>();
		Application.AccountVariation model = new Application.AccountVariation();
		model.setVariationDescription("model data");
		model.setXLendingGuaranteeToVary("model lending");
		model.setType(VariationTypeList.ADD_BORROWER);
		model.setAddGuaranteeSecurity(security);
		model.setLimitIncrease(limitIncrease);
		model.setReduceLimit(reduceLimit);
		models.add(model);
		return models;
	}

	
	@Test
	@DisplayName("map amount in foreign currency from model to entity")
	public void mapForeignCurrency() {
			
		com.moneycatcha.app.model.AmountInForeignCurrencyType modelCurrency = new com.moneycatcha.app.model.AmountInForeignCurrencyType();
		modelCurrency.setAmount(new BigDecimal("200.00"));
		modelCurrency.setCurrencyCode(CurrencyCodeList.AOA);
		
		AmountInForeignCurrencyType foreignType = AmountInForeignCurrencyTypeMapper.INSTANCE.toNewLimitRequestedInForeignCurrency(modelCurrency);
		
		assertNotNull(foreignType);
		assertTrue(foreignType.getAmount().intValue() > 0);

	}
	
	@Test
	@DisplayName("map addborrower entity to model")
	public void mapAddBorrowerEntityToModel() {
		
		AddBorrower entityBorrower = new AccountVariation.AddBorrower();
		entityBorrower.setXBorrower("entity borrower");

		Application.AccountVariation.AddBorrower model = AccountVariationMapper.INSTANCE.fromAddBorrower(entityBorrower);
		
		
		assertNotNull(model);
		assertEquals("entity borrower", model.getXBorrower());
		
	}
	
	@Test
	@DisplayName("map addborrower model to entity ")
	public void mapAddBorrowerModelToEntity() {
		
		Application.AccountVariation.AddBorrower modelBorrower = new Application.AccountVariation.AddBorrower();
		modelBorrower.setXBorrower("model borrower");
		
		AddBorrower entity = AccountVariationMapper.INSTANCE.toAddBorrower(modelBorrower);
		
		assertNotNull(entity);
		assertEquals("model borrower", entity.getXBorrower());

	}
	
}
