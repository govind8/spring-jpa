package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.OtherIncome;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.GovernmentBenefitsTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.OtherIncomeTypeList;
import com.moneycatcha.app.model.PercentOwnedType;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.testdata.OtherIncomeData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from OtherIncome model to OtherIncome entity")
public class OtherIncomeMapperTest {

	@Autowired
	OtherIncomeMapper incomeMapper;

	OtherIncomeData incomeData = new OtherIncomeData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map OtherIncome model to entity")
	public void mapOtherIncomeModelToEntity() {
		
		List<OtherIncome> incomeEntity = incomeMapper.toOtherIncomes(modelData());
		assertNotNull(incomeEntity);
		assertTrue(incomeEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map OtherIncome entity to model")
	public void mapOtherIncomeEntityToModel() {
				
		List<Application.OtherIncome> incomeModel = incomeMapper.fromOtherIncomes(incomeData.otherIncome());
		assertNotNull(incomeModel);
		assertTrue(incomeModel.size() == 3);
	}
	
	/**
	 * OtherIncome  - model Data
	 */
	public List<Application.OtherIncome> modelData() {
		
		List<Application.OtherIncome> incomes = new ArrayList<Application.OtherIncome>();
	
		// OtherIncome
		for (int i =0; i < 2; i++) {
			Application.OtherIncome income = new Application.OtherIncome();
			income.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			income.setBenefitsDescription(RandomStringUtils.randomAlphanumeric(5));
			income.setCountry(CountryCodeList.AD);
			income.setDescription(RandomStringUtils.randomAlphanumeric(10));
			income.setEndDate(calendarData.localDateToXML());
			income.setFrequency(FrequencyShortList.FORTNIGHTLY);
			income.setGovernmentBenefitsType(GovernmentBenefitsTypeList.ABSTUDY);
			income.setPercentOwned(percentOwnership());
			income.setType(OtherIncomeTypeList.ANNUITIES);
			income.setUniqueID(RandomStringUtils.randomAlphanumeric(5));
			incomes.add(income);
		}
		return incomes;
	}
	
	// percentowned
	public PercentOwnedType percentOwnership() {
		PercentOwnedType po = new PercentOwnedType();
		po.setProportions(ProportionsList.SPECIFIED);
		return po;
	}
 

}
