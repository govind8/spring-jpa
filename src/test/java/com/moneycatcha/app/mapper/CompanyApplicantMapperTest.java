package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.CompanyApplicant;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant.IncomePrevious;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant.IncomePrevious.Addback;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant.IncomePrevious.Addback.OtherAddback;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant.RelatedLegalEntities;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant.Shareholder;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.CompanyApplicantData;


@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from CompanyApplicant model to CompanyApplicant entity")
public class CompanyApplicantMapperTest {

	@Autowired
	CompanyApplicantMapper companyApplicantMapper;
	
	CompanyApplicantData companyApplicantData = new CompanyApplicantData();

	@Test
	@DisplayName("Map CompanyApplicant model to entity")
	public void mapCompanyApplicantModelToEntity() {
		
		List<CompanyApplicant> companyApplicantsEntity = companyApplicantMapper.toCompanyApplicants(modelData());
		assertNotNull(companyApplicantsEntity);
		assertTrue(companyApplicantsEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map CompanyApplicant entity to model")
	public void mapCompanyApplicantEntityToModel() {
				
		List<Application.CompanyApplicant> companyApplicantsModel = companyApplicantMapper.fromCompanyApplicants(companyApplicantData.applicants());
		assertNotNull(companyApplicantsModel);
		assertTrue(companyApplicantsModel.size() == 2);
	}
	
	
	/**
	 * CompanyApplicant  - model Data
	 * @return
	 */
	public List<Application.CompanyApplicant> modelData() {
		
		List<Application.CompanyApplicant> companyApplicants = new ArrayList<Application.CompanyApplicant>();
	
		// company applicant
		for (int i =0; i < 2; i++) {
			Application.CompanyApplicant applicant = new Application.CompanyApplicant();
			applicant.setABN(RandomStringUtils.randomAlphabetic(15));
			applicant.setABNVerified(YesNoList.NO);
			applicant.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			applicant.setAllowDirectMarketing(YesNoList.NO);
			applicant.setAllowThirdPartyDisclosure(YesNoList.YES);
			applicant.setApplicantType(ApplicantTypeList.BORROWER);
			applicant.setRelatedLegalEntities(relatedLegalEntities());
			applicant.setIncomePrevious(incomePrevious());
			companyApplicants.add(applicant);
		}
		return companyApplicants;
	}
	


	/*
	 *  Application -> Company Applicant -> Related Data
	 */
	public RelatedLegalEntities relatedLegalEntities() {
		
		RelatedLegalEntities rle = new RelatedLegalEntities();
		rle.setCurrentCustomers(RandomStringUtils.randomAlphabetic(10));
		rle.setEntityCount(new BigInteger(RandomStringUtils.randomNumeric(3)));
		return rle;
	}
	
	/*
	 *  Application -> Company Applicant -> Shareholders Data
	 */
	public List<Shareholder> shareholder() {
		
		List<Shareholder> shareholders = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Shareholder holder = new Shareholder();
			holder.setPercentOwned(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			holder.setXShareholder(RandomStringUtils.randomAlphabetic(10));
			shareholders.add(holder);
		}
		return shareholders;
	}
	
	
	/*
	 *  Application -> Company Applicant -> IncomePrevious Data
	 */
	public IncomePrevious incomePrevious() {
		
		IncomePrevious incomePrevious = new IncomePrevious();
		incomePrevious.setCompanyProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setCompanyProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setTaxOfficeAssessments(YesNoList.YES);
		incomePrevious.setXAccountant(RandomStringUtils.randomNumeric(15));
		incomePrevious.setAddback(addBack());
		return incomePrevious;
	}
	
	/**
	 * Addback
	 */
	public Addback addBack() {
		
		List<OtherAddback> oaList = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			OtherAddback otherAddback = new OtherAddback();
			otherAddback.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			otherAddback.setDescription(RandomStringUtils.randomAlphabetic(15));
			oaList.add(otherAddback);
		}
			
		Addback addback = new Addback();
		addback.setAllowances(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setAmortisationOfGoodwill(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setBonus(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setCarExpense(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setCarryForwardLosses(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setDepreciation(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setInterest(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setLease(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setNonCashBenefits(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setNonRecurringExpenses(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setPaymentToDirector(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addback.setSalary(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));

		return addback;
		
	}
	
	
	
}
