package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.SchemaVersion;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.testdata.SchemaVersionData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Recipient model to Recipient entity")
public class SchemaVersionMapperTest {


	@Autowired
	SchemaVersionMapper schemaMapper;

	SchemaVersionData schemaData = new SchemaVersionData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map SchemaVersion model to entity")
	public void mapSchemaVersionModelToEntity() {
		SchemaVersion schemaEntity = schemaMapper.toSchemaVersion(modelData());
		assertNotNull(schemaEntity);

	}
	
	@Test
	@DisplayName("Map SchemaVersion entity to model")
	public void mapSchemaVersionEntityToModel() {
				
		Message.SchemaVersion schemaModel = schemaMapper.fromSchemaVersion(schemaData.schemaVersion());
		assertNotNull(schemaModel);
	}
	
	/**
	 * Message.SchemaVersion  - model Data
	 */
	public Message.SchemaVersion modelData() {
		
		Message.SchemaVersion schema = new Message.SchemaVersion();
		schema.setGuidebookCode(RandomStringUtils.randomAlphanumeric(5));
		schema.setGuidebookName(RandomStringUtils.randomAlphanumeric(5));
		schema.setGuidebookVersion(RandomStringUtils.randomAlphanumeric(5));
		schema.setLIXICustomVersion(RandomStringUtils.randomAlphanumeric(5));
		schema.setLIXITransactionType(RandomStringUtils.randomAlphanumeric(5));
		schema.setLIXIVersion(RandomStringUtils.randomAlphanumeric(5));
		return schema;		
	}
	
}
