package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Recipient;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.testdata.RecipientData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Recipient model to Recipient entity")
public class RecipientMapperTest {

	@Autowired
	RecipientMapper recipientMapper;

	RecipientData recipientData = new RecipientData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Recipient model to entity")
	public void mapRecipientModelToEntity() {
		List<Recipient> recipientEntity = recipientMapper.toRecipients(modelData());
		assertNotNull(recipientEntity);
		assertTrue(recipientEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map Recipient entity to model")
	public void mapRecipientEntityToModel() {
				
		List<Message.Recipient> recipientModel = recipientMapper.fromRecipients(recipientData.recipient());
		assertNotNull(recipientModel);
		assertTrue(recipientModel.size() == 3);
	}
	
	/**
	 * Recipient  - model Data
	 */
	public List<Message.Recipient> modelData() {
		
		List<Message.Recipient> recipients = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Message.Recipient recipient = new Message.Recipient();
			recipient.setDescription(RandomStringUtils.randomAlphabetic(15));
			recipient.setLIXICode(RandomStringUtils.randomAlphanumeric(5));
			recipient.setRoutingCode(RandomStringUtils.randomAlphanumeric(5));
			recipient.setSoftware(software());
			recipients.add(recipient);
		}
		return recipients;
	}
	
	// Recipient.Software
	public Message.Recipient.Software software() {

		Message.Recipient.Software software = new Message.Recipient.Software();
		software.setEnvironment(RandomStringUtils.randomAlphabetic(15));
		return	software;
	}	
}
