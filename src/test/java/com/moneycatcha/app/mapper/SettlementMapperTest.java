package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Settlement;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.SettlementData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;


@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Settlement model to Settlement entity")
public class SettlementMapperTest {

	@Autowired
	SettlementMapper settlementMapper;

	SettlementData settlementData = new SettlementData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Settlement model to entity")
	public void mapSettlementModelToEntity() {
		
		List<Settlement> settlementEntity = settlementMapper.toSettlements(modelData());
		assertNotNull(settlementEntity);
		assertTrue(settlementEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map Settlement entity to model")
	public void mapSettlementEntityToModel() {
				
		List<Application.Settlement> settlementModel = settlementMapper.fromSettlements(settlementData.settlement());
		assertNotNull(settlementModel);
		assertTrue(settlementModel.size() == 3);
	}
	
	/**
	 * Settlement  - model Data
	 */
	public List<Application.Settlement> modelData() {
		
		List<Application.Settlement> settlements = new ArrayList<Application.Settlement>();
	
		// Settlement
		for (int i =0; i < 2; i++) {
			Application.Settlement settlement = new Application.Settlement();
			settlement.setPayoutFigure(new BigDecimal(RandomStringUtils.randomNumeric(2,5)));
			settlement.setSettlementBookingDateTime(calendarData.localDateTimeToXML());
			settlement.setSettlementReferenceNumber(RandomStringUtils.randomNumeric(6));
			settlement.setXSettlementBookingAddress(RandomStringUtils.randomAlphabetic(10));
			settlements.add(settlement);
		}
		return settlements;
	}
	
}
