package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Declarations;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.DeclarationsData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Declarations model to Declarations entity")
public class DeclarationsMapperTest {


	@Autowired
	DeclarationsMapper declarationsMapper;
	
	DeclarationsData declarationsData = new DeclarationsData();

	@Test
	@DisplayName("Map Declarations model to entity")
	public void mapDeclarationsModelToEntity() {
		
		Declarations declarationsEntity = declarationsMapper.toDeclarations(modelData());
		assertNotNull(declarationsEntity);
	}
	
	@Test
	@DisplayName("Map Declarations entity to model")
	public void mapCustomerTransactionAnalysisEntityToModel() {
				
		Application.Declarations declarationsModel = declarationsMapper.fromDeclarations(declarationsData.declaration());
		assertNotNull(declarationsModel);
	}
	
	
	/**
	 * Declarations  - model Data
	 * @return
	 */
	public Application.Declarations modelData() {
		
		Application.Declarations.BrokerDeclarations brokerDeclarations = new Application.Declarations.BrokerDeclarations();
		brokerDeclarations.setInterestOnlyMeetsRequirements(YesNoList.YES);
		brokerDeclarations.setInterestOnlyMeetsRequirementsDescription(RandomStringUtils.randomAlphabetic(15));
		brokerDeclarations.setInterestOnlyRisksExplained(YesNoList.YES);
		brokerDeclarations.setInterestOnlyRisksExplainedDescription(RandomStringUtils.randomAlphabetic(15));
		
		Application.Declarations declarations = new  Application.Declarations();
		declarations.setBrokerDeclarations(brokerDeclarations);
		
		return declarations;
	}
}
