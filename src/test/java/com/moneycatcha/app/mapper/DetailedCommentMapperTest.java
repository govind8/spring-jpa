package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.DetailedComment;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.DetailedCommentData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from DetailedComment model to DetailedComment entity")
public class DetailedCommentMapperTest {


	@Autowired
	DetailedCommentMapper commentMapper;

	DetailedCommentData commentData = new DetailedCommentData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map DetailedComment model to entity")
	public void mapDetailedCommentModelToEntity() {
		
		List<DetailedComment> commentEntity = commentMapper.toDetailedComments(modelData());
		assertNotNull(commentEntity);
		assertTrue(commentEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map DetailedComment entity to model")
	public void mapDetailedCommentEntityToModel() {
				
		List<Application.DetailedComment> commentModel = commentMapper.fromDetailedComments(commentData.comments());
		assertNotNull(commentModel);
		assertTrue(commentModel.size() > 2);
	}
	
	/**
	 * DetailedComment  - model Data
	 * @return
	 */
	public List<Application.DetailedComment> modelData() {
		
		List<Application.DetailedComment> comments = new ArrayList<Application.DetailedComment>();
	
		// DetailedComment
		for (int i =0; i < 2; i++) {
			Application.DetailedComment comment = new Application.DetailedComment();
			comment.setComment(RandomStringUtils.randomAlphabetic(15));
			comment.setContextDescription(RandomStringUtils.randomAlphabetic(15));
			comment.setCreatedDate(calendarData.localDateToXML());
			comment.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			comment.setXAuthor(RandomStringUtils.randomAlphanumeric(15));
			comment.setXContext(RandomStringUtils.randomAlphanumeric(15));
			comments.add(comment);
		}
		return comments;
	}
	
}
