package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.LendingGuarantee;
import com.moneycatcha.app.model.LendingGuaranteeTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.LendingGuaranteeData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from LendingGuarantee model to LendingGuarantee entity")
public class LendingGuaranteeMapperTest {

	@Autowired
	LendingGuaranteeMapper lendingMapper;

	LendingGuaranteeData lendingData = new LendingGuaranteeData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map LendingGuarantee model to entity")
	public void mapLendingGuaranteeModelToEntity() {
		
		List<LendingGuarantee> lendingEntity = lendingMapper.toLendingGuarantees(modelData());
		assertNotNull(lendingEntity);
		assertTrue(lendingEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map LendingGuarantee entity to model")
	public void mapLendingGuaranteeEntityToModel() {
				
		List<Application.LendingGuarantee> lendingModel = lendingMapper.fromLendingGuarantees(lendingData.lending());
		assertNotNull(lendingModel);
		assertTrue(lendingModel.size() == 3);
	}
	
	/**
	 * LendingGuarantee  - model Data
	 * @return
	 */
	public List<Application.LendingGuarantee> modelData() {
		
		List<Application.LendingGuarantee> guarantees = new ArrayList<Application.LendingGuarantee>();
	
		// LendingGuarantee
		for (int i =0; i < 2; i++) {
			Application.LendingGuarantee guarantee = new Application.LendingGuarantee();
			guarantee.setCrossGuarantee(YesNoList.YES);
			guarantee.setLimit(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			guarantee.setLimited(YesNoList.YES);
			guarantee.setLimitedToFacilityAmount(YesNoList.YES);
			guarantee.setType(LendingGuaranteeTypeList.SECURITY_ONLY);
			guarantee.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			guarantees.add(guarantee);
		}
		return guarantees;
	}
}
