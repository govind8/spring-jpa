package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.SalesChannel;
import com.moneycatcha.app.model.LicenceTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.SalesChannelTypeList;
import com.moneycatcha.app.testdata.SalesChannelData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from RelatedCompany model to RelatedCompany entity")
public class SalesChannelMapperTest {

	@Autowired
	SalesChannelMapper channelMapper;

	SalesChannelData salesData = new SalesChannelData();

	@Test
	@DisplayName("Map SalesChannel model to entity")
	public void mapSalesChannelModelToEntity() {
		
		SalesChannel channelEntity = channelMapper.toSalesChannel(modelData());
		assertNotNull(channelEntity);
	}
	
	@Test
	@DisplayName("Map SalesChannel entity to model")
	public void mapSalesChannelEntityToModel() {
				
		Application.SalesChannel channelModel = channelMapper.fromSalesChannel(salesData.salesChannel());
		assertNotNull(channelModel);
	}
	
	/**
	 * SalesChannel  - model Data
	 */
	public Application.SalesChannel modelData() {
		
		Application.SalesChannel channel = new Application.SalesChannel();
		channel.setType(SalesChannelTypeList.BRANCH_NETWORK);
		channel.setAggregator(aggregator());
		return channel;
	}
	
	// Aggregator
	public Application.SalesChannel.Aggregator aggregator() {
		Application.SalesChannel.Aggregator aggregator = new Application.SalesChannel.Aggregator();
		aggregator.setAccreditationNumber(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setLicenceNumber(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setLicenceType(LicenceTypeList.CRN);
		aggregator.setOtherIdentifier(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		return aggregator;
	}
}
