package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.NonRealEstateAsset;
import com.moneycatcha.app.model.AssetTransactionList;
import com.moneycatcha.app.model.ConditionList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.NonRealEstateAssetData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;


@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from NonRealEstateAsset model to NonRealEstateAsset entity")
public class NonRealEstateAssetMapperTest {

	@Autowired
	NonRealEstateAssetMapper nraMapper;

	NonRealEstateAssetData nraData = new NonRealEstateAssetData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map NonRealEstateAsset model to entity")
	public void mapNonRealEstateAssetModelToEntity() {
		
		List<NonRealEstateAsset> nraEntity = nraMapper.toNonRealEstateAssets(modelData());
		assertNotNull(nraEntity);
		assertTrue(nraEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map NonRealEstateAsset entity to model")
	public void mapNonRealEstateAssetEntityToModel() {
				
		List<Application.NonRealEstateAsset> nraModel = nraMapper.fromNonRealEstateAssets(nraData.nonRealEstateAsset());
		assertNotNull(nraModel);
		assertTrue(nraModel.size() == 3);
	}
	
	/**
	 * NonRealEstateAsset  - model Data
	 */
	public List<Application.NonRealEstateAsset> modelData() {
		
		List<Application.NonRealEstateAsset> nras = new ArrayList<Application.NonRealEstateAsset>();
	
		// NonRealEstateAsset
		for (int i =0; i < 2; i++) {
			Application.NonRealEstateAsset nra = new Application.NonRealEstateAsset();
			nra.setAgriculturalAsset(agriculturalAsset());
			nra.setPrimarySecurity(YesNoList.NO);
			nra.setContractOfSale(YesNoList.NO);
			nra.setToBeUsedAsSecurity(YesNoList.YES);
			nra.setTransaction(AssetTransactionList.OWNS);
			nra.setVerified(YesNoList.YES);
			nra.setXCustomerTransactionAnalysis(RandomStringUtils.randomAlphabetic(10));
			nra.setXVendorTaxInvoice(RandomStringUtils.randomAlphabetic(10));
			nras.add(nra);
		}
		return nras;
	}
	
	// AgriculturalAsset
	public Application.NonRealEstateAsset.AgriculturalAsset agriculturalAsset() {
		
		Application.NonRealEstateAsset.AgriculturalAsset agricultural = new Application.NonRealEstateAsset.AgriculturalAsset();
		agricultural.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		agricultural.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		agricultural.setCondition(ConditionList.DEMO);
		agricultural.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		agricultural.setDescription(RandomStringUtils.randomAlphabetic(10));
		agricultural.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		agricultural.setMake(RandomStringUtils.randomAlphabetic(10));
		agricultural.setModel(RandomStringUtils.randomAlphabetic(10));
		agricultural.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		agricultural.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		agricultural.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		agricultural.setYear(calendarData.localDateToXML());
		agricultural.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		return agricultural;		
	}
}
