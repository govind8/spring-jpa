package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.OtherExpense;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.OtherExpenseCategoryList;
import com.moneycatcha.app.model.OtherExpenseTypeList;
import com.moneycatcha.app.model.PercentOwnedType;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.OtherExpenseData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from OtherExpense model to OtherExpense entity")
public class OtherExpenseMapperTest {

	@Autowired
	OtherExpenseMapper expenseMapper;

	OtherExpenseData expenseData = new OtherExpenseData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map OtherExpense model to entity")
	public void mapOtherExpenseModelToEntity() {
		
		List<OtherExpense> expenseEntity = expenseMapper.toOtherExpenses(modelData());
		assertNotNull(expenseEntity);
		assertTrue(expenseEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map OtherExpense entity to model")
	public void mapOtherExpenseEntityToModel() {
				
		List<Application.OtherExpense> expenseModel = expenseMapper.fromOtherExpenses(expenseData.otherExpense());
		assertNotNull(expenseModel);
		assertTrue(expenseModel.size() == 3);
	}
	
	/**
	 * OtherExpense  - model Data
	 */
	public List<Application.OtherExpense> modelData() {
		
		List<Application.OtherExpense> expenses = new ArrayList<Application.OtherExpense>();
	
		// OtherExpense
		for (int i =0; i < 2; i++) {
			Application.OtherExpense expense = new Application.OtherExpense();
			expense.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2,5)));
			expense.setArrears(arrears());
			expense.setCategory(OtherExpenseCategoryList.DISCRETIONARY_LIVING_EXPENSES);
			expense.setDescription(RandomStringUtils.randomAlphabetic(10));
			expense.setEndDate(calendarData.localDateToXML());
			expense.setFrequency(FrequencyShortList.FORTNIGHTLY);
			expense.setHasArrears(YesNoList.YES);
			expense.setPercentOwned(percentOwnership());
			expense.setType(OtherExpenseTypeList.ADDITIONAL_CAR_S_MAINTENANCE);
			expense.setUniqueID(RandomStringUtils.randomAlphanumeric(5));
			expenses.add(expense);
		}
		return expenses;
	}
	
	// percentowned
	public PercentOwnedType percentOwnership() {
		PercentOwnedType po = new PercentOwnedType();
		po.setProportions(ProportionsList.SPECIFIED);
		return po;
	}
 
	// Arrears
	public Application.OtherExpense.Arrears arrears() {
		Application.OtherExpense.Arrears arrears = new Application.OtherExpense.Arrears();
		arrears.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		arrears.setNumberOfMissedPayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
		arrears.setToBePaidOut(YesNoList.NO);
		return arrears;
	}
}
