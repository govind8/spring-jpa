package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.TransformMetadata;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.model.Message.TransformMetadata.Identifier;
import com.moneycatcha.app.testdata.TransformMetadataData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from TransformMetadata model to TransformMetadata entity")
public class TransformMetadataMapperTest {

	@Autowired
	TransformMetadataMapper transformMapper;

	TransformMetadataData transformData = new TransformMetadataData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map TransformMetadata model to entity")
	public void mapTransformModelToEntity() {
		TransformMetadata transformEntity = transformMapper.toTransformMetadata(modelData());
		assertNotNull(transformEntity);

	}
	
	@Test
	@DisplayName("Map TransformMetadata entity to model")
	public void mapTransformEntityToModel() {
				
		Message.TransformMetadata transformModel = transformMapper.fromTransformMetadata(transformData.transformMetadata());
		assertNotNull(transformModel);
	}
	
	/**
	 * Message.TransformMetadata  - model Data
	 */
	public Message.TransformMetadata modelData() {
		
		Message.TransformMetadata transform = new Message.TransformMetadata();
//		transform.setIdentifier(identifiers());
		return transform;
		
	}
	
	// TranformMetadata -> identifier
	public List<Identifier> identifiers() {
		List<Identifier> identifiers = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			Identifier identifier = new Identifier();
			identifier.setAttributeName(RandomStringUtils.randomAlphabetic(15));
			identifier.setExternalID(RandomStringUtils.randomAlphabetic(15));
			identifier.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			identifier.setXContext(RandomStringUtils.randomAlphabetic(15));
			identifiers.add(identifier);
		}
		return identifiers;
	}
	
}
