package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Preferences;
import com.moneycatcha.app.model.FundsAccessTypeList;
import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.Message.Content;
import com.moneycatcha.app.model.Message.Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.PreferencesData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Preferences model to Preferences entity")
public class PreferencesMapperTest {

	@Autowired
	PreferencesMapper preferencesMapper;

	PreferencesData preferencesData = new PreferencesData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Preferences model to entity")
	public void mapPreferencesModelToEntity() {
		Preferences preferencesEntity = preferencesMapper.toPreferences(modelData());
		assertNotNull(preferencesEntity);

	}
	
	@Test
	@DisplayName("Map Preferences entity to model")
	public void mapPreferencesEntityToModel() {
				
		Content.NeedsAnalysis.Preferences preferencesModel = preferencesMapper.fromPreferences(preferencesData.preferences());
		assertNotNull(preferencesModel);
	}
	
	/**
	 * Preferences  - model Data
	 */
	public Content.NeedsAnalysis.Preferences modelData() {
		
		Content.NeedsAnalysis.Preferences preferences = new Content.NeedsAnalysis.Preferences();
		preferences.setConflictDescription(RandomStringUtils.randomAlphabetic(15));
		preferences.setFundsAccessType(FundsAccessTypeList.LINE_OF_CREDIT);
		preferences.setFundsAccessTypeDetails(fundsAccessTypeDetails());
		return preferences;
	}
	
	
	// Preferences -> FundsAccessTypeDetails - 
	public FundsAccessTypeDetails fundsAccessTypeDetails() {
		FundsAccessTypeDetails funds = new FundsAccessTypeDetails();
		funds.setOffsetAccount(offsetAccount());
		funds.setRedraw(redraw());
		return funds;
	}
	
	//  Preferences -> FundsAccessTypeDetails -> OffsetAccount
	public FundsAccessTypeDetails.OffsetAccount offsetAccount() {
		FundsAccessTypeDetails.OffsetAccount offset = new FundsAccessTypeDetails.OffsetAccount();
		offset.setImportance(ImportanceList.DON_T_WANT);
		offset.setRisksExplained(YesNoList.YES);
		offset.setReason(reason());
		return offset;
	}
	
	//  Preferences -> FundsAccessTypeDetails -> Redraw
	public FundsAccessTypeDetails.Redraw redraw() {
		FundsAccessTypeDetails.Redraw redraw = new FundsAccessTypeDetails.Redraw();
		redraw.setImportance(ImportanceList.DON_T_WANT);
		redraw.setReason(redrawReason());
		redraw.setRisksExplained(YesNoList.YES);
		return redraw;
		
	}
	
	
	// Reason -- reusable common function
	public FundsAccessTypeDetails.OffsetAccount.Reason reason() {
		FundsAccessTypeDetails.OffsetAccount.Reason reason = new FundsAccessTypeDetails.OffsetAccount.Reason();
		reason.setAllowsAccessToFunds(YesNoList.YES);
		reason.setAllowsPayingOffLoanSooner(YesNoList.YES);
		reason.setDescription(RandomStringUtils.randomAlphabetic(15));
		reason.setForTaxPurposes(YesNoList.YES);
		reason.setOther(YesNoList.YES);
		
		return reason;
	}

	
	// Reason -- reusable common function
	public FundsAccessTypeDetails.Redraw.Reason redrawReason() {
		FundsAccessTypeDetails.Redraw.Reason reason = new FundsAccessTypeDetails.Redraw.Reason();
		reason.setDescription(RandomStringUtils.randomAlphabetic(15));
		reason.setFlexibilityToAccessPrepaidFundsIfNeeded(YesNoList.YES);
		reason.setOther(YesNoList.YES);
		
		return reason;
	}

}
