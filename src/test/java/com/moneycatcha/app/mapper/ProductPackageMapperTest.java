package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.ProductPackage;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.ProductPackageData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from ProductPackage model to ProductPackage entity")
public class ProductPackageMapperTest {

	@Autowired
	ProductPackageMapper productMapper;

	ProductPackageData productData = new ProductPackageData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map ProductPackage model to entity")
	public void mapProductPackageModelToEntity() {
		
		List<ProductPackage> productEntity = productMapper.toProductPackages(modelData());
		assertNotNull(productEntity);
		assertTrue(productEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map ProductPackage entity to model")
	public void mapProductPackageEntityToModel() {
				
		List<Application.ProductPackage> productModel = productMapper.fromProductPackages(productData.productPackage());
		assertNotNull(productModel);
		assertTrue(productModel.size() == 3);
	}
	
	/**
	 * ProductPackage  - model Data
	 */
	public List<Application.ProductPackage> modelData() {
		
		List<Application.ProductPackage> products = new ArrayList<Application.ProductPackage>();
		
		// ProductPackage
		for (int i =0; i < 2; i++) {
			Application.ProductPackage product = new Application.ProductPackage();
			product.setExistingPackage(YesNoList.YES);
			product.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			products.add(product);
		}
		return products;
	}

}
