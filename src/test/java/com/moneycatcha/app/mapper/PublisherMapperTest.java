package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Publisher;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.testdata.PublisherData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Preferences model to Preferences entity")
public class PublisherMapperTest {

	@Autowired
	PublisherMapper publisherMapper;

	PublisherData publisherData = new PublisherData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Publisher model to entity")
	public void mapPublisherModelToEntity() {
		Publisher publisherEntity = publisherMapper.toPublisher(modelData());
		assertNotNull(publisherEntity);

	}
	
	@Test
	@DisplayName("Map Publisher entity to model")
	public void mapPublisherEntityToModel() {
				
		Message.Publisher publisherModel = publisherMapper.fromPublisher(publisherData.publisher());
		assertNotNull(publisherModel);
	}
	
	/**
	 * Message.Publisher  - model Data
	 */
	public Message.Publisher modelData() {
		
		Message.Publisher publisher = new Message.Publisher();
		publisher.setCompanyName(RandomStringUtils.randomAlphanumeric(10));
		publisher.setContactName(RandomStringUtils.randomAlphanumeric(10));
		publisher.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		publisher.setLIXICode(RandomStringUtils.randomAlphanumeric(10));
		publisher.setPublishedDateTime(calendarData.localDateToXML());
		publisher.setSoftware(software());
		return publisher;
	}
	
	// Publisher.Software
	public Message.Publisher.Software software() {

		Message.Publisher.Software software = new Message.Publisher.Software();
		software.setDescription(RandomStringUtils.randomAlphabetic(15));
		software.setEnvironment(RandomStringUtils.randomAlphabetic(15));
		software.setLIXICode(RandomStringUtils.randomAlphanumeric(5));
		software.setName(RandomStringUtils.randomAlphanumeric(5));
		software.setTechnicalEmail(RandomStringUtils.randomAlphanumeric(5));
		software.setVersion(RandomStringUtils.randomAlphanumeric(5));

		return	software;
	}	
	
}
