package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Liability;
import com.moneycatcha.app.model.ClearingFromOtherSourceList;
import com.moneycatcha.app.model.ClearingFromThisLoanList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.LiabilityData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Liability model to Liability entity")
public class LiabilityMapperTest {

	@Autowired
	LiabilityMapper liabilityMapper;

	LiabilityData liabilityData = new LiabilityData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Liability model to entity")
	public void mapLiabilityModelToEntity() {
		
		List<Liability> liabilityEntity = liabilityMapper.toLiabilities(modelData());
		assertNotNull(liabilityEntity);
		assertTrue(liabilityEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map Liability entity to model")
	public void mapLiabilityEntityToModel() {
				
		List<Application.Liability> liabilityModel = liabilityMapper.fromLiabilities(liabilityData.liability());
		assertNotNull(liabilityModel);
		assertTrue(liabilityModel.size() == 3);
	}
	
	/**
	 * Liability  - model Data
	 */
	public List<Application.Liability> modelData() {
		
		List<Application.Liability> liabilities = new ArrayList<Application.Liability>();
	
		// Liability
		for (int i =0; i < 2; i++) {
			Application.Liability liability = new Application.Liability();
			liability.setAccelerationPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2,2)));
			liability.setAnnualInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2,2)));
			liability.setArrears(arrears());
			liability.setAvailableForRedrawAmount(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			liability.setBalloonRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			liability.setBalloonRepaymentDate(calendarData.localDateToXML());
			liability.setClearingFromOtherSource(ClearingFromOtherSourceList.PARTIAL);
			liability.setClearingFromThisLoan(ClearingFromThisLoanList.PARTIAL);
			liability.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			liabilities.add(liability);
		}
		return liabilities;
	}
	
	// Arrears
	public Application.Liability.Arrears arrears() {
		Application.Liability.Arrears arrears = new Application.Liability.Arrears();
		arrears.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		arrears.setNumberOfMissedPayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
		return arrears;
	}
	
}
