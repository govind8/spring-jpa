package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Insurance;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.InsuranceTypeList;
import com.moneycatcha.app.model.InsurerList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.InsuranceData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Insurance model to Insurance entity")
public class InsuranceMapperTest {


	@Autowired
	InsuranceMapper insuranceMapper;

	InsuranceData insuranceData = new InsuranceData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Insurance model to entity")
	public void mapInsuranceModelToEntity() {
		
		List<Insurance> insuranceEntity = insuranceMapper.toInsuranceList(modelData());
		assertNotNull(insuranceEntity);
		assertTrue(insuranceEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map Insurance entity to model")
	public void mapInsuranceEntityToModel() {
				
		List<Application.Insurance> insuranceModel = insuranceMapper.fromInsuranceList(insuranceData.insurances());
		assertNotNull(insuranceModel);
		assertTrue(insuranceModel.size() == 4);
	}
	
	/**
	 * Insurance  - model Data
	 * @return
	 */
	public List<Application.Insurance> modelData() {
		
		List<Application.Insurance> insurances = new ArrayList<Application.Insurance>();
	
		// Insurance
		for (int i =0; i < 2; i++) {
			Application.Insurance insurance = new Application.Insurance();
			insurance.setCommissionPayable(payable());
			insurance.setDescription(RandomStringUtils.randomAlphabetic(15));
			insurance.setEffectiveDate(calendarData.localDateToXML());
			insurance.setExpiryDate(calendarData.localDateToXML());
			insurance.setInsuranceType(InsuranceTypeList.BUILDING_REPLACEMENT);
			insurance.setInsuredAmount(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			insurance.setInsurer(InsurerList.INSURANCE_AUSTRALIA_GROUP);
			insurance.setOtherInsurerName(RandomStringUtils.randomAlphanumeric(15));
			insurance.setPolicyNumber(RandomStringUtils.randomAlphanumeric(15));
			insurance.setPremium(premium());
			insurance.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			insurances.add(insurance);

		}
		return insurances;
	}
	
	
	// Commission Payable
	public Application.Insurance.CommissionPayable payable() {
		Application.Insurance.CommissionPayable payable = new Application.Insurance.CommissionPayable();
		payable.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(10)));
		payable.setPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		payable.setXPayer(RandomStringUtils.randomAlphanumeric(15));
		return payable;
	}

	// Premium premium
	public Application.Insurance.Premium premium() {
		Application.Insurance.Premium premium = new Application.Insurance.Premium();
		premium.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(10)));
		premium.setFrequency(FrequencyShortList.WEEKLY);
		return premium;
	}
	
}
