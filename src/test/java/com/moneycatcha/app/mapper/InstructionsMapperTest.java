package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Instructions;
import com.moneycatcha.app.model.AssessmentTypeApplicationInstructionsList;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.model.Message.Instructions.ApplicationInstructions;
import com.moneycatcha.app.model.Message.Instructions.ApplicationInstructions.Submit;
import com.moneycatcha.app.model.Message.Instructions.ApplicationInstructions.Update;
import com.moneycatcha.app.model.Message.Instructions.ApplicationInstructions.Update.Status;
import com.moneycatcha.app.model.StatusNameApplicationInstructionsList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.InstructionsData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Instructions model to Instructions entity")
public class InstructionsMapperTest {

	@Autowired
	InstructionsMapper instructionsMapper;

	InstructionsData instructionsData = new InstructionsData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Instructions model to entity")
	public void mapInstructionsModelToEntity() {
		Instructions instructionsEntity = instructionsMapper.toInstructions(modelData());
		assertNotNull(instructionsEntity);

	}
	
	@Test
	@DisplayName("Map Instructions entity to model")
	public void mapInstructionsEntityToModel() {
				
		Message.Instructions instructionsModel = instructionsMapper.fromInstructions(instructionsData.instructions());
		assertNotNull(instructionsModel);
	}
	
	/**
	 * Message.Instructions  - model Data
	 */
	public Message.Instructions modelData() {
		
		Message.Instructions instructions = new Message.Instructions();
		instructions.setApplicationInstructions(applicationInstructions());
		
		return instructions;
	}
	
	// Instructions -> ApplicationInstructions
	public ApplicationInstructions applicationInstructions() {
		
		ApplicationInstructions appInstructions = new ApplicationInstructions();
		appInstructions.setSubmit(submit());
		appInstructions.setUpdate(update());
		
		return appInstructions;
	}
	
	// Instructions -> ApplicationInstructions -> Submit
	public Submit submit() {
		
		Submit submit = new Submit();
		submit.setAssessmentType(AssessmentTypeApplicationInstructionsList.FULL);
		submit.setIsAccountVariation(YesNoList.YES);
		submit.setIsResubmission(YesNoList.YES);
		submit.setIsSubmissionDocuments(YesNoList.YES);
		submit.setIsSupportingDocuments(YesNoList.YES);
		return submit;
	}
	
	
	// Instructions -> ApplicationInstructions -> Update
	public Update update() {
		
		Update update = new Update();
		update.setStatus(status());
		
		return update;
	}

	// Instructions -> ApplicationInstructions -> Update -> Status
	public Status status() {
		
		Status status = new Status();
		status.setDateTime(calendarData.localDateTimeToXML());
		status.setDetails(RandomStringUtils.randomAlphabetic(15));
		status.setName(StatusNameApplicationInstructionsList.APPLICATION_REGISTERED);
		return status;
	}

}
