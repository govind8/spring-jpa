package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Attachment;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.testdata.AttachmentsData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Attachments model to Attachments entity")
public class AttachmentMapperTest {


	@Autowired
	AttachmentMapper attachmentMapper;

	AttachmentsData attachmentData = new AttachmentsData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Attachment model to entity")
	public void mapAttachmentModelToEntity() {
		
		List<Attachment> attachmentEntity = attachmentMapper.toAttachments(modelData());
		assertNotNull(attachmentEntity);
		assertTrue(attachmentEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map Attachment entity to model")
	public void mapAttachmentEntityToModel() {
				
		List<Message.Attachment> attachmentModel = attachmentMapper.fromAttachments(attachmentData.attachments());
		assertNotNull(attachmentModel);
		assertTrue(attachmentModel.size() == 2);
	}
	
	/**
	 * Attachment  - model Data
	 */
	public List<Message.Attachment> modelData() {
		
		List<Message.Attachment> attachments = new ArrayList<Message.Attachment>();
		
		double dec = 51.33;
		byte[] inlineAttachment = Double.toString(dec).getBytes();

		// Attachment
		for (int i =0; i < 2; i++) {
			Message.Attachment attachment = new Message.Attachment();
			attachment.setFilename(RandomStringUtils.randomAlphabetic(10));
			attachment.setInlineAttachment(inlineAttachment);
			attachment.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			attachment.setURI(RandomStringUtils.randomAlphabetic(10));
			attachments.add(attachment);
		}
		return attachments;
	}
}
