package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.PersonApplicant;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.KinRelationshipList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.PersonApplicantData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from PersonApplicant model to PersonApplicant entity")
public class PersonApplicantMapperTest {

	@Autowired
	PersonApplicantMapper personMapper;

	PersonApplicantData personData = new PersonApplicantData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map PersonApplicant model to entity")
	public void mapPersonApplicantModelToEntity() {
		
		List<PersonApplicant> personEntity = personMapper.toPersonApplicants(modelData());
		assertNotNull(personEntity);
		assertTrue(personEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map PersonApplicant entity to model")
	public void mapPersonApplicantEntityToModel() {
				
		List<Application.PersonApplicant> personModel = personMapper.fromPersonApplicants(personData.personApplicant());
		assertNotNull(personModel);
		assertTrue(personModel.size() == 3);
	}
	
	/**
	 * PersonApplicant  - model Data
	 */
	public List<Application.PersonApplicant> modelData() {
		
		List<Application.PersonApplicant> persons = new ArrayList<Application.PersonApplicant>();
	
		// PersonApplicant
		for (int i =0; i < 2; i++) {
			Application.PersonApplicant person = new Application.PersonApplicant();
			person.setApplicantType(ApplicantTypeList.BORROWER);
			person.setNextOfKin(nextOfKin());
			person.setNominatedBorrower(nominatedBorrower());
			person.setDateOfBirth(calendarData.localDateToXML());
			person.setDiscussedWithBeneficiaries(YesNoList.YES);
			persons.add(person);
		}
		return persons;
	}
	
	// NextOfKin
	public Application.PersonApplicant.NextOfKin nextOfKin() {
	
		Application.PersonApplicant.NextOfKin nok = new Application.PersonApplicant.NextOfKin();
		nok.setKinRelationship(KinRelationshipList.FRIEND);
		nok.setXPerson(RandomStringUtils.randomAlphabetic(15));
		return nok;
		
	}
	
	// NominatedBorrower
	public Application.PersonApplicant.NominatedBorrower nominatedBorrower() {
	
		Application.PersonApplicant.NominatedBorrower nominated = new Application.PersonApplicant.NominatedBorrower();
		nominated.setXNominee(RandomStringUtils.randomAlphabetic(15));
		return nominated;
	}	
 
}
