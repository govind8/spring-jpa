package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Summary;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.SummaryData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;



@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Summary model to Summary entity")
public class SummaryMapperTest {

	@Autowired
	SummaryMapper summaryMapper;

	SummaryData summaryData = new SummaryData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();

	@Test
	@DisplayName("Map Summary model to entity")
	public void mapSummaryModelToEntity() {
		
		Summary summaryEntity = summaryMapper.toSummary(modelData());
		assertNotNull(summaryEntity);
	}
	
	@Test
	@DisplayName("Map Summary entity to model")
	public void mapSummaryEntityToModel() {
				
		Application.Summary summaryModel = summaryMapper.fromSummary(summaryData.summary());
		assertNotNull(summaryModel);
	}
	
	/**
	 * Summary  - model Data
	 */
	public Application.Summary modelData() {
		
		Application.Summary summary = new Application.Summary();
		summary.setAllPartiesAgreeToElectronicSignature(YesNoList.YES);
		summary.setFeesDisclosureDate(calendarData.localDateToXML());
		summary.setLoanToValuationRatio(loanToValuationRatio());
		return summary;
	}
	
	
	// LoanToValuationRatio
	public Application.Summary.LoanToValuationRatio loanToValuationRatio() {
		Application.Summary.LoanToValuationRatio  ratio = new  Application.Summary.LoanToValuationRatio();
		ratio.setApplicationLVR(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		ratio.setPeakDebtLVR(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		ratio.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		
		return ratio;
	}	
}
