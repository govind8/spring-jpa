package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.TrustApplicant;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.TrustApplicantData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from TrustApplicant model to TrustApplicant entity")
public class TrustApplicantMapperTest {

	@Autowired
	TrustApplicantMapper trustMapper;

	TrustApplicantData trustData = new TrustApplicantData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map TrustApplicant model to entity")
	public void mapTrustApplicantModelToEntity() {
		
		List<TrustApplicant> trustEntity = trustMapper.toTrustApplicants(modelData());
		assertNotNull(trustEntity);
		assertTrue(trustEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map TrustApplicant entity to model")
	public void mapTrustApplicantEntityToModel() {
				
		List<Application.TrustApplicant> trustModel = trustMapper.fromTrustApplicants(trustData.trustApplicant());
		assertNotNull(trustModel);
		assertTrue(trustModel.size() == 3);
	}
	
	/**
	 * TrustApplicant  - model Data
	 */
	public List<Application.TrustApplicant> modelData() {
		
		List<Application.TrustApplicant> applicants = new ArrayList<Application.TrustApplicant>();
		
		// TrustApplicant
		for (int i =0; i < 2; i++) {
			Application.TrustApplicant applicant = new Application.TrustApplicant();
			applicant.setABN(RandomStringUtils.randomAlphanumeric(10));
			applicant.setApplicantType(ApplicantTypeList.BORROWER);
			applicant.setBusinessNameSameAsTrustName(YesNoList.YES);
			applicant.setCountryEstablished(CountryCodeList.AD);
			applicant.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			applicants.add(applicant);
		}
		return applicants;
	}
}
