package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.NeedsAnalysis;
import com.moneycatcha.app.model.Message.Content;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.NeedsAnalysisData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from NeedsAnalysis model to NeedsAnalysis entity")
public class NeedsAnalysisMapperTest {

	@Autowired
	NeedsAnalysisMapper needsMapper;

	NeedsAnalysisData needsData = new NeedsAnalysisData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map NeedsAnalysis model to entity")
	public void mapNeedsAnalysisModelToEntity() {
		NeedsAnalysis needsEntity = needsMapper.toNeedsAnalysis(modelData());
		assertNotNull(needsEntity);

	}
	
	@Test
	@DisplayName("Map NeedsAnalysis entity to model")
	public void mapNeedsAnalysisEntityToModel() {
				
		Content.NeedsAnalysis needsModel = needsMapper.fromNeedsAnalysis(needsData.needsAnalysis());
		assertNotNull(needsModel);
	}
	
	/**
	 * NeedsAnalysis  - model Data
	 */
	public Content.NeedsAnalysis modelData() {
		
		Content.NeedsAnalysis needsAnalysis = new Content.NeedsAnalysis();
		needsAnalysis.setBenefitToApplicants(benefitToApplicants());
		needsAnalysis.setInterview(interview());
		needsAnalysis.setLoanAmountSought(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
		needsAnalysis.setLoanTermSoughtDescription(RandomStringUtils.randomAlphabetic(15));
		needsAnalysis.setPrimaryApplicationPurpose(PrimaryPurposeLoanPurposeList.OWNER_OCCUPIED);
		needsAnalysis.setProductSelection(RandomStringUtils.randomAlphabetic(15));
		needsAnalysis.setRefinancingAndConsolidation(refinancingAndConsolidation());
		return needsAnalysis;
	}
	
	
	
	// NeedsAnalysis -> BenefitToApplicants
	public Content.NeedsAnalysis.BenefitToApplicants benefitToApplicants() {
		Content.NeedsAnalysis.BenefitToApplicants benefits = new Content.NeedsAnalysis.BenefitToApplicants();
		benefits.setAllApplicantsBenefit(YesNoList.YES);
		benefits.setBenefitEnquiries(RandomStringUtils.randomAlphabetic(15));
		return benefits;
	}
	
	
	//  NeedsAnalysis -> Interview
	public Content.NeedsAnalysis.Interview interview() {
		Content.NeedsAnalysis.Interview interview = new Content.NeedsAnalysis.Interview();
		interview.setAllApplicantsPresent(YesNoList.NO);
		interview.setAllApplicantsUnderstandEnglish(YesNoList.YES);
		interview.setDate(calendarData.localDateToXML());
		interview.setInterpreterRecommended(YesNoList.YES);
		interview.setXLocation(RandomStringUtils.randomAlphabetic(15));

		return interview;
	}
	
	 // NeedsAnalysis -> RefinancingAndConsolidation
	public Content.NeedsAnalysis.RefinancingAndConsolidation refinancingAndConsolidation() {
		 
		Content.NeedsAnalysis.RefinancingAndConsolidation.RefinancingReason refinancingReason = 
				new Content.NeedsAnalysis.RefinancingAndConsolidation.RefinancingReason(); 
		refinancingReason.setCloseToEndOfCurrentLoanTerm(YesNoList.YES);
		refinancingReason.setCloseToEndOfCurrentLoanTermDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setDebtConsolidation(YesNoList.YES);
		refinancingReason.setDebtConsolidationDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setDissatisfactionWithCurrentLender(YesNoList.YES);
		refinancingReason.setDissatisfactionWithCurrentLenderDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setGreaterFlexibility(YesNoList.YES);
		refinancingReason.setGreaterFlexibilityDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setIncreaseTotalLoanAmount(YesNoList.YES);
		refinancingReason.setIncreaseTotalLoanAmountDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setLowerInterestRate(YesNoList.YES);
		refinancingReason.setLowerInterestRateDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setOther(YesNoList.YES);
		refinancingReason.setOtherDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setReducedRepayments(YesNoList.YES);
		refinancingReason.setReducedRepaymentsDetails(RandomStringUtils.randomAlphabetic(15));

		Content.NeedsAnalysis.RefinancingAndConsolidation refinancing = 
				new Content.NeedsAnalysis.RefinancingAndConsolidation();
		refinancing.setDetailsOfCreditCardPlan(RandomStringUtils.randomAlphabetic(15));
		refinancing.setExplainedIncreaseInterestRisk(YesNoList.YES);
		refinancing.setExplainedIncreaseLoanTermRisk(YesNoList.YES);
		refinancing.setPlanToCloseOrReduceCreditCard(YesNoList.YES);
		refinancing.setRefinancingReason(refinancingReason);
		return refinancing;
		 
	}
}
