package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.LoanDetails;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.testdata.LoanDetailsData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from LoanDetails model to LoanDetails entity")
public class LoanDetailsMapperTest {

	@Autowired
	LoanDetailsMapper loanMapper;

	LoanDetailsData loanData = new LoanDetailsData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map LoanDetails model to entity")
	public void mapLoanModelToEntity() {
		
		List<LoanDetails> loanEntity = loanMapper.toLoanDetailsList(modelData());
		assertNotNull(loanEntity);
		assertTrue(loanEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map LoanDetails entity to model")
	public void mapLoanEntityToModel() {
				
		List<Application.LoanDetails> loanModel = loanMapper.fromLoanDetailsList(loanData.loanDetails());
		assertNotNull(loanModel);
		assertTrue(loanModel.size() == 3);
	}
	
	/**
	 * LoanDetails  - model Data
	 */
	public List<Application.LoanDetails> modelData() {
		
		List<Application.LoanDetails> details = new ArrayList<Application.LoanDetails>();
	
		// LoanDetails
		for (int i =0; i < 2; i++) {
			Application.LoanDetails loan = new Application.LoanDetails();
			loan.setAccelerationPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2,2)));
			loan.setAmountRequested(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			loan.setAmountRequestedInclusive(new BigDecimal(RandomStringUtils.randomNumeric(2,10)));
			loan.setBalloonRepaymentDate(calendarData.localDateToXML());
			loan.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			loan.setXMasterAgreement(RandomStringUtils.randomAlphanumeric(15));
			details.add(loan);
		}
		return details;
	}
}
