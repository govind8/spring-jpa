package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Overview;
import com.moneycatcha.app.model.ApplicationTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.OverviewData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Overview model to Overview entity")
public class OverviewMapperTest {

	@Autowired
	OverviewMapper overviewMapper;

	OverviewData overviewData = new OverviewData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Overview model to entity")
	public void mapOverviewModelToEntity() {
		
		Overview overviewEntity = overviewMapper.toOverview(modelData());
		assertNotNull(overviewEntity);

	}
	
	@Test
	@DisplayName("Map Overview entity to model")
	public void mapOverviewEntityToModel() {
				
		Application.Overview overviewModel = overviewMapper.fromOverview(overviewData.overview());
		assertNotNull(overviewModel);
		assertNotNull(overviewModel.getBranchDomicile());
	}
	
	/**
	 * Overview  - model Data
	 */
	public Application.Overview modelData() {
		
		// Overview
		Application.Overview overview = new Application.Overview();
		overview.setApplicationType(ApplicationTypeList.DEPOSIT_ACCOUNT);
		overview.setBranchDomicile(branchDomicile());
		overview.setBranchSign(branchSign());
		overview.setUrgent(YesNoList.YES);
		overview.setXMainContactPoint((RandomStringUtils.randomAlphanumeric(10)));

		return overview;
	}
	
	// BranchDomicile
	public Application.Overview.BranchDomicile branchDomicile() {
		Application.Overview.BranchDomicile domicile = new Application.Overview.BranchDomicile();
		domicile.setBSB(RandomStringUtils.randomNumeric(3) + "-" + RandomStringUtils.randomNumeric(3));
		domicile.setInternalName(RandomStringUtils.randomAlphabetic(15));
		domicile.setInternalNumber(RandomStringUtils.randomAlphabetic(15));
		return domicile;
	}
	
	// BranchSign
	public Application.Overview.BranchSign branchSign() {
		Application.Overview.BranchSign sign = new Application.Overview.BranchSign();
		sign.setBSB(RandomStringUtils.randomNumeric(3) + "-" + RandomStringUtils.randomNumeric(3));
		sign.setInternalName(RandomStringUtils.randomAlphabetic(15));
		sign.setInternalNumber(RandomStringUtils.randomAlphabetic(15));
		return sign;
	}
}
