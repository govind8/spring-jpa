package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.model.VendorTaxInvoiceType;
import com.moneycatcha.app.testdata.VendorTaxInvoiceData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from VendorTaxInvoiceType model to VendorTaxInvoiceType entity")
public class VendorTaxInvoiceTypeMapperTest {


	@Autowired
	VendorTaxInvoiceTypeMapper vendorMapper;

	VendorTaxInvoiceData vendorData = new VendorTaxInvoiceData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map VendorTaxInvoice model to entity")
	public void mapVendorTaxInvoiceModelToEntity() {
		
		List<com.moneycatcha.app.entity.VendorTaxInvoiceType> vendorEntity = vendorMapper.toVendorTaxInvoiceTypes(modelData());
		assertNotNull(vendorEntity);
		assertTrue(vendorEntity.size() == 2);

	}
	
	@Test
	@DisplayName("Map VendorTaxInvoice entity to model")
	public void mapVendorTaxInvoiceEntityToModel() {
				
		List<VendorTaxInvoiceType> vendorModel = vendorMapper.fromVendorTaxInvoiceTypes(vendorData.vendorTaxInvoiceType());
		assertNotNull(vendorModel);
		assertTrue(vendorModel.size() == 3);
	}
	
	/**
	 * VendorTaxInvoiceType  - model Data
	 */
	public List<VendorTaxInvoiceType> modelData() {
		
		List<VendorTaxInvoiceType> invoices = new ArrayList<VendorTaxInvoiceType>();
		
		// VendorTaxInvoiceType
		for (int i =0; i < 2; i++) {
			VendorTaxInvoiceType invoice = new VendorTaxInvoiceType();
			invoice.setInvoiceNumber(RandomStringUtils.randomAlphanumeric(10));
			invoice.setTaxInvoiceDate(calendarData.localDateToXML());
			invoice.setUniqueID(RandomStringUtils.randomAlphabetic(10));
			invoice.setXDeliverTo(RandomStringUtils.randomAlphabetic(10));
			invoice.setXPurchaser(RandomStringUtils.randomAlphabetic(10));
			invoice.setXVendor(RandomStringUtils.randomAlphabetic(10));
			invoices.add(invoice);
		}
		return invoices;
	}
}
