package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.RealEstateAsset;
import com.moneycatcha.app.model.AssetTransactionList;
import com.moneycatcha.app.model.CommercialTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.RealEstateAssetData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from RealEstateAsset model to RealEstateAsset entity")
public class RealEstateAssetMapperTest {

	@Autowired
	RealEstateAssetMapper raMapper;

	RealEstateAssetData raData = new RealEstateAssetData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map RealEstateAsset model to entity")
	public void mapRealEstateAssetModelToEntity() {
		
		List<RealEstateAsset> raEntity = raMapper.toRealEstateAssets(modelData());
		assertNotNull(raEntity);
		assertTrue(raEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map NonRealEstateAsset entity to model")
	public void mapRealEstateAssetEntityToModel() {
				
		List<Application.RealEstateAsset> raModel = raMapper.fromRealEstateAssets(raData.realEstateAsset());
		assertNotNull(raModel);
		assertTrue(raModel.size() == 3);
	}
	
	/**
	 * RealEstateAsset  - model Data
	 */
	public List<Application.RealEstateAsset> modelData() {
		
		List<Application.RealEstateAsset> ras = new ArrayList<Application.RealEstateAsset>();
	
		// RealEstateAsset
		for (int i =0; i < 2; i++) {
			Application.RealEstateAsset ra = new Application.RealEstateAsset();
			ra.setApprovalInPrinciple(YesNoList.NO);
			ra.setCommercial(commercial());
			ra.setPrimarySecurity(YesNoList.NO);
			ra.setContractOfSale(YesNoList.NO);
			ra.setToBeUsedAsSecurity(YesNoList.YES);
			ra.setTransaction(AssetTransactionList.OWNS);
			ra.setVerified(YesNoList.YES);
			ra.setXAddress(RandomStringUtils.randomAlphabetic(10));
			ra.setXPropertyAgent(RandomStringUtils.randomAlphabetic(10));
			ras.add(ra);
		}
		return ras;
	}
	
	//Commercial
	public Application.RealEstateAsset.Commercial commercial() {
		Application.RealEstateAsset.Commercial commercial = new Application.RealEstateAsset.Commercial();
		commercial.setType(CommercialTypeList.BLOCK_OF_UNITS_OR_FLATS);
		return commercial;
	}

}
