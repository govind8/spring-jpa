package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;
import com.moneycatcha.app.model.YesNoIntentList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.BusinessData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from CompanyApplicant Business model to Business entity")
public class BusinessMapperTest {

	@Autowired
	BusinessMapper businessMapper;
	
	BusinessData businessData = new BusinessData();
	
	@Test
	@DisplayName("Business model to entity")
	public void mapBusinessModelToEntity() {
		
//		Business businessEntity = BusinessMapper.INSTANCE.toBusiness(setModelData());
		Business businessEntity = businessMapper.toBusiness(setModelData());
		assertNotNull(businessEntity);
		assertEquals(businessEntity.getAustralianBIC(), "model");
		assertNotNull(businessEntity.getConcentrationRisk());
	}
	
	@Test
	@DisplayName("Business entity to model")
	public void mapBusinessEntityToModel() {
				
//		CompanyApplicant.Business businessModel = BusinessMapper.INSTANCE.fromBusiness(setEntityData());
		CompanyApplicant.Business businessModel = businessMapper.fromBusiness(businessData.business());
		assertNotNull(businessModel);
		System.out.println("bic code : " + businessModel.getAustralianBIC());
//		assertNotNull(businessModel.getAustralianBIC(), "entity");
//		assertEquals(businessModel.getAustralianBIC(), "entity");
	}
	
	// entity data
	public Business setEntityData() {
		
		Business.ConcentrationRisk risk = new Business.ConcentrationRisk();
		risk.setConcentrationRiskDetails("concentration risk");
		risk.setCustomerOrSupplierConcentration(YesNoList.YES);
		
		Business.Diversification diversification = new Business.Diversification();
		diversification.setDetails("diversification");
		diversification.setIsDiversified(YesNoList.NO);

		Business.ImportExport export = new Business.ImportExport();
		export.setDetails("import export");
		export.setIsInvolved(YesNoIntentList.FUTURE_INTENT);

		Business.PropertyInvestment property = new Business.PropertyInvestment();
		property.setDetails("property investment");
		property.setIsInvolved(YesNoIntentList.FUTURE_INTENT);

		
		Business business = new Business();
		business.setAustralianBIC("entity");
		business.setIsFranchiseIntent(YesNoIntentList.FUTURE_INTENT);
		business.setConcentrationRisk(risk);
		business.setDiversification(diversification);
		business.setImportExport(export);
		business.setPropertyInvestment(property);
		
		return business;
	}
	
	// model data
	public CompanyApplicant.Business setModelData() {
		
		CompanyApplicant.Business.ConcentrationRisk risk = new CompanyApplicant.Business.ConcentrationRisk();
		risk.setConcentrationRiskDetails("concentration risk");
		risk.setCustomerOrSupplierConcentration(YesNoList.YES);
		
		CompanyApplicant.Business.Diversification diversification = new CompanyApplicant.Business.Diversification();
		diversification.setDetails("diversification");
		diversification.setIsDiversified(YesNoList.NO);

		CompanyApplicant.Business.ImportExport export = new CompanyApplicant.Business.ImportExport();
		export.setDetails("import export");
		export.setIsInvolved(YesNoIntentList.FUTURE_INTENT);

		CompanyApplicant.Business.PropertyInvestment property = new CompanyApplicant.Business.PropertyInvestment();
		property.setDetails("property investment");
		property.setIsInvolved(YesNoIntentList.FUTURE_INTENT);

		
		CompanyApplicant.Business business = new CompanyApplicant.Business();
		business.setAustralianBIC("model");
		business.setIsAFranchise(YesNoIntentList.FUTURE_INTENT);
		business.setConcentrationRisk(risk);
		business.setDiversification(diversification);
//		business.setImportExport(export);
		business.setPropertyInvestment(property);
		
		return business;
	}

}
