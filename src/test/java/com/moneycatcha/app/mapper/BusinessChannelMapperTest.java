package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.BusinessChannel;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PhoneType;
import com.moneycatcha.app.model.Message.Content.Application.BusinessChannel.Contact;
import com.moneycatcha.app.model.Message.Content.Application.BusinessChannel.Contact.ContactPerson;
import com.moneycatcha.app.testdata.BusinessChannelData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from BusinessChannel model to BusinessChannel entity")
public class BusinessChannelMapperTest {

	@Autowired
	BusinessChannelMapper businessChannelMapper;
	
	BusinessChannelData businessChannelData = new BusinessChannelData();

	@Test
	@DisplayName("Map BusinessChannel model to entity")
	public void mapBusinessChannelModelToEntity() {
		
		BusinessChannel businessChannelEntity = businessChannelMapper.toBusinessChannel(modelData());
		assertNotNull(businessChannelEntity);
		assertEquals(businessChannelEntity.getContact().getEmail(), "welcome@moneycatcha.com");
		assertEquals(businessChannelEntity.getContact().getContactPerson().getNameTitle(), NameTitleList.MRS);
		assertEquals(businessChannelEntity.getContact().getOfficePhone().getCountryCode(), "0061");
	}
	
	@Test
	@DisplayName("Map BusinessChannel entity to model")
	public void mapBusinessChannelEntityToModel() {
				
		Application.BusinessChannel businessChannelModel = businessChannelMapper.fromBusinessChannel(businessChannelData.channelData());
		assertNotNull(businessChannelModel);
	}

	/**
	 * Application.BusinessChannel  - model Data
	 * @return
	 */
	public Application.BusinessChannel modelData() {

		Application.BusinessChannel channel = new Application.BusinessChannel();
		channel.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		channel.setACN(RandomStringUtils.randomAlphanumeric(15));
		channel.setABN(RandomStringUtils.randomNumeric(16));
		channel.setCompanyName(RandomStringUtils.randomAlphabetic(50));

		Contact contact = new Contact();
		contact.setContactPerson(contactPerson());
		contact.setEmail("welcome@moneycatcha.com");
		contact.setOfficeFax(officeFax());
		contact.setOfficePhone(officePhone());
		contact.setWebAddress("www.moneycatcha.com");
		contact.setXAddress(RandomStringUtils.randomAlphanumeric(15));
		
		channel.setContact(contact);
		
		return channel;
		
	}
	
	
	/*
	 * BusinessChannel.Contact.ContactPerson
	 */
	public ContactPerson contactPerson() {
		
		ContactPerson person = new ContactPerson();
		person.setFirstName(RandomStringUtils.randomAlphabetic(10));
		person.setNameTitle(NameTitleList.MRS);
		person.setRole(RandomStringUtils.randomAlphabetic(10));
		person.setSurname(RandomStringUtils.randomAlphabetic(10));
		return person;
	}
	
	/*
	 * 	BusinessChannel.Contact.OfficeFax - Data
	 */
	public PhoneType officeFax() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("44444448");
		phoneType.setOverseasDialingCode("0092");
		return phoneType;
		
	}
	
	/*
	 * 	BusinessChannel.Contact.OfficePhone - Data
	 */
	public PhoneType officePhone() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("43333333");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
}
