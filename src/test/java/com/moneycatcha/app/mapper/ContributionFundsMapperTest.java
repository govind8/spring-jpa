package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.ContributionFunds;
import com.moneycatcha.app.model.ContributionFundsTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.ContributionFundsData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from ContributionFunds model to ContributionFunds entity")
public class ContributionFundsMapperTest {

	@Autowired
	ContributionFundsMapper contributionFundsMapper;

	ContributionFundsData contributionFundsData = new ContributionFundsData();

	@Test
	@DisplayName("Map ContributionFunds model to entity")
	public void mapContributionFundsModelToEntity() {
		
		List<ContributionFunds> contributionFundsEntity = contributionFundsMapper.toContributionFundsList(modelData());
		assertNotNull(contributionFundsEntity);
		assertTrue(contributionFundsEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map ContributionFunds entity to model")
	public void mapContributionFundsEntityToModel() {
				
		List<Application.ContributionFunds> contributionFundsModel = contributionFundsMapper.fromContributionFundsList(contributionFundsData.funds());
		assertNotNull(contributionFundsModel);
		assertTrue(contributionFundsModel.size() > 2);
	}
	
	
	/**
	 * ContributionFunds  - model Data
	 * @return
	 */
	public List<Application.ContributionFunds> modelData() {
		
		List<Application.ContributionFunds> contributionFunds = new ArrayList<Application.ContributionFunds>();
	
		// ContributionFunds
		for (int i =0; i < 2; i++) {
			Application.ContributionFunds funds = new Application.ContributionFunds();
			funds.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
			funds.setDescription(RandomStringUtils.randomAlphabetic(15));
			funds.setLoan(YesNoList.NO);
			funds.setType(ContributionFundsTypeList.CASH);
			funds.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			funds.setXAssociatedLoanAccount(RandomStringUtils.randomAlphabetic(10));
			funds.setXFundsHeldInAccount(RandomStringUtils.randomAlphabetic(10));
			contributionFunds.add(funds);
		}
		return contributionFunds;
	}
	
	
	
}
