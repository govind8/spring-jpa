package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.DepositAccountDetails;
import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.DepositAccountTypeList;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.StatementCycleList;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.DepositAccountDetailsData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from DepositAccountDetails model to DepositAccountDetails entity")
public class DepositAccountDetailsMapperTest {


	@Autowired
	DepositAccountDetailsMapper depositMapper;

	DepositAccountDetailsData depositData = new DepositAccountDetailsData();

	@Test
	@DisplayName("Map DepositAccountDetails model to entity")
	public void mapDepositAccountDetailsModelToEntity() {
		
		List<DepositAccountDetails> depositEntity = depositMapper.toDepositAccountDetailsList(modelData());
		assertNotNull(depositEntity);
		assertTrue(depositEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map DepositAccountDetails entity to model")
	public void mapDepositAccountDetailsEntityToModel() {
				
		List<Application.DepositAccountDetails> depositModel = depositMapper.fromDepositAccountDetailsList(depositData.deposit());
		assertNotNull(depositModel);
		assertTrue(depositModel.size() > 2);
	}
	
	
	/**
	 * DepositAccountDetails  - model Data
	 * @return
	 */
	public List<Application.DepositAccountDetails> modelData() {
		
		List<Application.DepositAccountDetails> deposits = new ArrayList<Application.DepositAccountDetails>();
	
		// DepositAccountDetails
		for (int i =0; i < 2; i++) {
			Application.DepositAccountDetails deposit = new Application.DepositAccountDetails();
			deposit.setCommission(commission());
			deposit.setPackage(setpackage());
			deposit.setOriginatorReferenceID(RandomStringUtils.randomAlphanumeric(15));
			deposit.setProductCode(RandomStringUtils.randomAlphabetic(15));
			deposit.setProductName(RandomStringUtils.randomAlphabetic(15));
			deposit.setProposedAnnualInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			deposit.setStatementCycle(StatementCycleList.YEARLY);
			deposit.setType(DepositAccountTypeList.STANDARD);
			deposit.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			deposits.add(deposit);
		}
		return deposits;
	}
	
	
	// Set Commisssion
	public Application.DepositAccountDetails.Commission commission() {
		Application.DepositAccountDetails.Commission commission = new Application.DepositAccountDetails.Commission();
		commission.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		commission.setCommissionPaid(YesNoList.YES);
		commission.setCommissionStructure(CommissionStructureList.UP_FRONT_ONLY);
		commission.setOtherCommissionStructureDescription(RandomStringUtils.randomAlphabetic(150));
		commission.setPromotionCode(RandomStringUtils.randomAlphanumeric(15));
		commission.setThirdPartyReferee(YesNoList.YES);
		commission.setTrail(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
	
		return commission;
	}
	
	// DepositAccountDetail Package
	public Application.DepositAccountDetails.Package setpackage() {
		Application.DepositAccountDetails.Package p = new Application.DepositAccountDetails.Package();
		p.setCategory(RandomStringUtils.randomAlphabetic(15));
		p.setCode(RandomStringUtils.randomAlphabetic(15));
		p.setMemberID(RandomStringUtils.randomAlphanumeric(15));
		p.setName(RandomStringUtils.randomAlphabetic(15));
		p.setOptionCode(RandomStringUtils.randomAlphabetic(15));
		p.setOrganisation(RandomStringUtils.randomAlphabetic(15));
		return p;
	}
	
	
}
