package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.RelatedCompany;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.RelatedCompanyData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from RelatedCompany model to RelatedCompany entity")
public class RelatedCompanyMapperTest {

	@Autowired
	RelatedCompanyMapper relatedMapper;

	RelatedCompanyData relatedData = new RelatedCompanyData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map RelatedCompany model to entity")
	public void mapRelatedCompanyModelToEntity() {
		
		List<RelatedCompany> personEntity = relatedMapper.toRelatedCompanies(modelData());
		assertNotNull(personEntity);
		assertTrue(personEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map RelatedCompany entity to model")
	public void mapRelatedCompanyEntityToModel() {
				
		List<Application.RelatedCompany> relatedModel = relatedMapper.fromRelatedCompanies(relatedData.RelatedCompany());
		assertNotNull(relatedModel);
		assertTrue(relatedModel.size() == 3);
	}
	
	/**
	 * RelatedCompany  - model Data
	 */
	public List<Application.RelatedCompany> modelData() {
		
		List<Application.RelatedCompany> companies = new ArrayList<Application.RelatedCompany>();
	
		// RelatedCompany
		for (int i =0; i < 2; i++) {
			Application.RelatedCompany related = new Application.RelatedCompany();
			related.setABN(RandomStringUtils.randomAlphabetic(10));
			related.setABNVerified(YesNoList.YES);
			related.setACN(RandomStringUtils.randomAlphabetic(10));
			related.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			companies.add(related);
		}
		return companies;
	}
}
