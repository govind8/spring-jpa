package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.CustomerTransactionAnalysis;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.CustomerTransactionAnalysisData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from CustomerTransactionAnalysis model to CustomerTransactionAnalysis entity")
public class CustomerTransactionAnalysisMapperTest {

	@Autowired
	CustomerTransactionAnalysisMapper customerTransactionAnalysisMapper;
	
	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();

	CustomerTransactionAnalysisData customerTransactionData = new CustomerTransactionAnalysisData();

	@Test
	@DisplayName("Map CustomerTransactionAnalysis model to entity")
	public void mapCustomerTransactionAnalysisModelToEntity() {
		
		List<CustomerTransactionAnalysis> customerTransactionEntity = customerTransactionAnalysisMapper.toCustomerTransactionAnalysisList(modelData());
		assertNotNull(customerTransactionEntity);
		assertTrue(customerTransactionEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map CustomerTransactionAnalysis entity to model")
	public void mapCustomerTransactionAnalysisEntityToModel() {
				
		List<Application.CustomerTransactionAnalysis> customerTransactionModel = customerTransactionAnalysisMapper.fromCustomerTransactionAnalysisList(customerTransactionData.customerAnalysis());
		assertNotNull(customerTransactionModel);
		assertTrue(customerTransactionModel.size() > 2);
	}
	
	
	/**
	 * CustomerTransactionAnalysis  - model Data
	 * @return
	 */
	public List<Application.CustomerTransactionAnalysis> modelData() {
		
		List<Application.CustomerTransactionAnalysis> customers = new ArrayList<Application.CustomerTransactionAnalysis>();
	
		// Customer Transaction Analysis
		for (int i =0; i < 2; i++) {
			Application.CustomerTransactionAnalysis customer = new Application.CustomerTransactionAnalysis();
			customer.setAnalysisID(RandomStringUtils.randomAlphanumeric(15));
			customer.setDataAggregator(RandomStringUtils.randomAlphabetic(10));
			customer.setEndDate(calendarData.localDateTimeToXML());
			customer.setResult(result());
			customer.setStartDate(calendarData.localDateTimeToXML());
			customer.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			customers.add(customer);
		}
		return customers;
	}
	
	// Result
	public Application.CustomerTransactionAnalysis.Result result() {
		Application.CustomerTransactionAnalysis.Result result = new Application.CustomerTransactionAnalysis.Result();
		result.setManualCheckDetails(RandomStringUtils.randomAlphabetic(30));
		result.setReferForManualCheck(YesNoList.NO);
		return result;
	}

	
}
