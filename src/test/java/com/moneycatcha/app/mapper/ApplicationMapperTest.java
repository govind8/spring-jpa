package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.Application;
import com.moneycatcha.app.model.Message.Content;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.ApplicationData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Application model to Application entity")
public class ApplicationMapperTest {


	@Autowired
	ApplicationMapper applicationMapper;

	ApplicationData applicationData = new ApplicationData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map Application model to entity")
	public void mapApplicationModelToEntity() {
		
		Application applicationEntity = applicationMapper.toApplication(modelData());
		assertNotNull(applicationEntity);

	}
	
	@Test
	@DisplayName("Map Application entity to model")
	public void mapApplicationEntityToModel() {
				
		Content.Application applicationModel = applicationMapper.fromApplication(applicationData.application());
		assertNotNull(applicationModel);
	}
	
	/**
	 * Content.Application  - model Data
	 */
	public Content.Application modelData() {
		
		Content.Application application = new Content.Application();
		application.setProductionData(YesNoList.YES);
		application.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
		return application;
	}
}
