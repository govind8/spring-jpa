package com.moneycatcha.app.mapper;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.testdata.ContentData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from Content entity to Content model")
public class ContentMapperTest {


	@Autowired
	ContentMapper  contentMapper;

	ContentData contentData = new ContentData();

	@Test
	@DisplayName("Map Content entity to model")
	public void mapContentEntityToModel() {
				
		Message.Content contentModel = contentMapper.fromContent(contentData.content());
		assertNotNull(contentModel);
	}
}
