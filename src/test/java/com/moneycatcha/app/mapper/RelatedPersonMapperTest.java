package com.moneycatcha.app.mapper;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.moneycatcha.app.entity.RelatedPerson;
import com.moneycatcha.app.model.ForeignTaxAssociationStatusList;
import com.moneycatcha.app.model.ForeignTaxAssociationType;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PersonNameType;
import com.moneycatcha.app.model.YesNoList;
import com.moneycatcha.app.testdata.RelatedPersonData;
import com.moneycatcha.app.testdata.XmlGregorianCalendarData;

@RunWith(SpringRunner.class)
@SpringBootTest
@DisplayName("Map values from RelatedCompany model to RelatedCompany entity")
public class RelatedPersonMapperTest {

	@Autowired
	RelatedPersonMapper relatedMapper;

	RelatedPersonData relatedData = new RelatedPersonData();

	XmlGregorianCalendarData calendarData = new XmlGregorianCalendarData();
	
	@Test
	@DisplayName("Map RelatedPerson model to entity")
	public void mapRelatedPersonModelToEntity() {
		
		List<RelatedPerson> personEntity = relatedMapper.toRelatedPersons(modelData());
		assertNotNull(personEntity);
		assertTrue(personEntity.size() == 2);
	}
	
	@Test
	@DisplayName("Map RelatedPerson entity to model")
	public void mapRelatedPersonEntityToModel() {
				
		List<Application.RelatedPerson> relatedModel = relatedMapper.fromRelatedPersons(relatedData.relatedPerson());
		assertNotNull(relatedModel);
		assertTrue(relatedModel.size() == 3);
	}
	
	/**
	 * RelatedCompany  - model Data
	 */
	public List<Application.RelatedPerson> modelData() {
		
		List<Application.RelatedPerson> persons = new ArrayList<Application.RelatedPerson>();
	
		// RelatedCompany
		for (int i =0; i < 2; i++) {
			Application.RelatedPerson related = new Application.RelatedPerson();
			related.setDateOfBirth(calendarData.localDateToXML());
			related.setForeignTaxAssociation(foreignTaxAssociation());
			related.setPersonName(personName());
			related.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			persons.add(related);
		}
		return persons;
	}
	
	//ForeignTaxAssociationType
	public ForeignTaxAssociationType foreignTaxAssociation() {
		ForeignTaxAssociationType foriegnType = new ForeignTaxAssociationType();
		foriegnType.setSelfCertificationDeclaration(YesNoList.NO);
		foriegnType.setStatus(ForeignTaxAssociationStatusList.NONE);
		
		return foriegnType;
	}
	
	// PersonName
	public PersonNameType personName() {
	
		PersonNameType person = new PersonNameType();
		person.setFirstName(RandomStringUtils.randomAlphabetic(15));
		person.setKnownAs(RandomStringUtils.randomAlphabetic(15));
		person.setMiddleNames(RandomStringUtils.randomAlphabetic(15));
		person.setNameTitle(NameTitleList.HON);
		person.setOtherNameTitle(RandomStringUtils.randomAlphabetic(15));
		person.setSurname(RandomStringUtils.randomAlphabetic(15));
		return person;
	}
}
