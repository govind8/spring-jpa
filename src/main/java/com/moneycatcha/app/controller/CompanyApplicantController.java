package com.moneycatcha.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;
import com.moneycatcha.app.service.CompanyApplicantService;

@RestController
public class CompanyApplicantController {

	
	@Autowired
	CompanyApplicantService service;

    @Autowired
    private ObjectMapper jsonObjectMapper;

	
    /**
     * fetch a company applicant by id
     * @param id
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/companyapplicant/id/{id}")
	public String getCompanyApplicantById(@PathVariable(value="id") Long id) throws JsonProcessingException, EntityNotFoundException {
    	
    	CompanyApplicant companyApplicant = service.findById(id);
		if (companyApplicant == null) {
			return "{ no company applicant found for id : '" + id +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(companyApplicant);
	}
	
    /**
     * fetch all company applicants by company name
     * @param companyname
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/companyapplicant/name")
	public String getCompanyApplicantByCompanyName(@RequestParam(value="companyname") String companyname) throws JsonProcessingException, EntityNotFoundException {
    	
		List<CompanyApplicant> companyApplicants = service.findByCompanyName(companyname);
		if (companyApplicants.isEmpty()) {
			return "{ no company applicants found for company name : '" + companyname +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(companyApplicants);
	}	
    
    /**
     * fetch all company applicants for a given unique id
     * @param uniqueId
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/companyapplicant/uniqueid")
	public String getCompanyApplicantByUniqueId(@RequestParam(value="uniqueid") String uniqueId) throws JsonProcessingException, EntityNotFoundException {
    	
		List<CompanyApplicant> companyApplicants = service.findByUniqueId(uniqueId);
		if (companyApplicants.isEmpty()) {
			return "{ no company applicant found for the given unique id : '" + uniqueId +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(companyApplicants);
	}
}
