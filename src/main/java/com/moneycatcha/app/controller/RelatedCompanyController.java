package com.moneycatcha.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.model.Message.Content.Application.RelatedCompany;
import com.moneycatcha.app.service.RelatedCompanyService;

@RestController
public class RelatedCompanyController {

	
	@Autowired
	RelatedCompanyService service;

    @Autowired
    private ObjectMapper jsonObjectMapper;

	
    /**
     * 
     * @param id
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/relatedcompany/id/{id}")
	public String getRelatedCompanyById(@PathVariable(value="id") Long id) throws JsonProcessingException, EntityNotFoundException {
    	
    	RelatedCompany relatedCompany = service.findById(id);
		if (relatedCompany == null) {
			return "{ no related company found for id : '" + id +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(relatedCompany);
	}
	
    /**
     * 
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/relatedcompanies/")
	public String getAll() throws JsonProcessingException, EntityNotFoundException {
    	
		List<RelatedCompany> companies = service.findAll();
		if (companies.isEmpty()) {
			return "{ no related companies found in the database } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(companies);
	}
     
}
