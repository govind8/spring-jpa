package com.moneycatcha.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.AddressTypeMapper;
import com.moneycatcha.app.model.AddressType;
import com.moneycatcha.app.service.AddressService;

@RestController
public class AddressController {

	@Autowired
	AddressTypeMapper addressMapper;
	
	@Autowired
	AddressService service;

    @Autowired
    private ObjectMapper jsonObjectMapper;
	
    /**
     * fetch an address for the given id from the table.
     * @param id
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/address/id/{id}")
	public String getAddressById(@PathVariable(value="id") Long id) throws JsonProcessingException, EntityNotFoundException {
    	
		AddressType addressType = service.findById(id);
		if (addressType == null) {
			return "{ no address found for id : '" + id +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(addressType);
	}
	
    /**
     * fetch all addresses for a given city
     * @param cityName
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/address/city")
	public String getAddressByCity(@RequestParam(value="city") String cityName) throws JsonProcessingException, EntityNotFoundException {
    	
		List<AddressType> addresses = service.findByCity(cityName);
		if (addresses.isEmpty()) {
			return "{ no address found for city : '" + cityName +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(addresses);
	}	
    
    /**
     * fetch all addresses related to the unique id
     * @param uniqueId
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/address/uniqueid")
	public String getAddressByUniqueId(@RequestParam(value="uniqueid") String uniqueId) throws JsonProcessingException, EntityNotFoundException {
    	
		List<AddressType> addresses = service.findByUniqueId(uniqueId);
		if (addresses.isEmpty()) {
			return "{ no address found for the given unique id : '" + uniqueId +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(addresses);
	}
    
    /**
     * fetch all addresses from the database
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/addresses/")
	public String getAll() throws JsonProcessingException, EntityNotFoundException {
    	
		List<AddressType> addresses = service.findAll();
		if (addresses.isEmpty()) {
			return "{ no addresses found in the database } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(addresses);
	}
   
}
