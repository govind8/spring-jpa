package com.moneycatcha.app.controller;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Optional;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.MessageMapper;
import com.moneycatcha.app.model.Message;
import com.moneycatcha.app.parsers.validation.ValidateLixi;
import com.moneycatcha.app.service.MessageService;

@RestController
public class MessageController {


    @Autowired
    private Jaxb2Marshaller marshaller;

    @Autowired
    private ObjectMapper jsonObjectMapper;
    
    @Autowired
    private MessageService messageService;
    
    @Autowired
    MessageMapper messageMapper;
 
    
    @Autowired
    ValidateLixi validateLixi;
    
    com.moneycatcha.app.entity.Message messageEntity;

    
    @PostMapping(value = "/cal/v1/")
    public boolean messageToEntity(@RequestBody String smessage) throws EntityNotFoundException {
    	
    	
    	validateLixi.isValid(smessage);
   
		Message messageModel = (Message) marshaller.unmarshal(new StreamSource(new StringReader(smessage)));
        try {
			System.out.println("Xml(model) to JSON(entity): " + jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(messageModel));
		} catch (JsonProcessingException e) {
			throw new EntityNotFoundException(this.getClass(), "id", e.getMessage());
		}
       	
       	messageEntity = messageMapper.toMessage(messageModel);
		messageService.save(messageEntity);
		
		return true;
      }
    
    @GetMapping(value = "/cal/v1/{id}")
    public String entityToMessage(@PathVariable(value="id") Long id) throws EntityNotFoundException {
    	
    	if (id.toString().trim() == null) 
    		throw new EntityNotFoundException(this.getClass(), "id", "please enter a number to search message");
    	
  		StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    
    	Optional<com.moneycatcha.app.entity.Message> messageEntity = messageService.getMessageById(id);
    	
    	if (messageEntity.isEmpty()) {
    		return "{ '0' records found }";
    	}
    		
       	Message messageModel = messageMapper.fromMessage(messageEntity.get());
       	
    	marshaller.marshal(messageModel, result);
    	String sxmloutput = result.getWriter().toString();
        System.out.println("Entity to xml message: \n" + sxmloutput);
        return sxmloutput;
    }
}
