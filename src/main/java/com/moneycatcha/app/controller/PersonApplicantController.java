package com.moneycatcha.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant;
import com.moneycatcha.app.service.PersonApplicantService;

@RestController
public class PersonApplicantController {

	
	@Autowired
	PersonApplicantService service;

    @Autowired
    private ObjectMapper jsonObjectMapper;

	
    /**
     * fetch person applicant by id
     * @param id
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/personapplicant/id/{id}")
	public String getPersonApplicantById(@PathVariable(value="id") Long id) throws JsonProcessingException, EntityNotFoundException {
    	
    	PersonApplicant personApplicant = service.findById(id);
		if (personApplicant == null) {
			return "{ no person applicant found for id : '" + id +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(personApplicant);
	}
	
    /**
     * fetch all person applicants from a given country 
     * @param countycode
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/personapplicant/country")
	public String getPersonApplicantByCountry(@RequestParam(value="code") String code) throws JsonProcessingException, EntityNotFoundException {
    	
		List<PersonApplicant> personApplicants = service.findByCountry(code);
		if (personApplicants.isEmpty()) {
			return "{ no person applicants found for country : '" + code +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(personApplicants);
	}	
    
    /**
     * fetch all person applicants related to the unique id
     * @param uniqueId
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/personapplicant/uniqueid")
	public String getPersonApplicantByUniqueId(@RequestParam(value="uniqueid") String uniqueId) throws JsonProcessingException, EntityNotFoundException {
    	
		List<PersonApplicant> personApplicants = service.findByUniqueId(uniqueId);
		if (personApplicants.isEmpty()) {
			return "{ no person applicant found for the given unique id : '" + uniqueId +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(personApplicants);
	}
    

}
