package com.moneycatcha.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.model.Message.Content.Application.RelatedPerson;
import com.moneycatcha.app.service.RelatedPersonService;

@RestController
public class RelatedPersonController {
	
	@Autowired
	RelatedPersonService service;

    @Autowired
    private ObjectMapper jsonObjectMapper;

	
    /**
     * fetch the related person for a given id
     * @param id
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/relatedperson/id/{id}")
	public String getRelatedPersonById(@PathVariable(value="id") Long id) throws JsonProcessingException, EntityNotFoundException {
    	
    	RelatedPerson relatedPerson = service.findById(id);
		if (relatedPerson == null) {
			return "{ no related person found for id : '" + id +"' } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(relatedPerson);
	}
	
    /**
     * fetch all related persons from the database
     * @return
     * @throws JsonProcessingException
     * @throws EntityNotFoundException
     */
    @GetMapping(value = "/cal/relatedpersons/")
	public String getAll() throws JsonProcessingException, EntityNotFoundException {
    	
		List<RelatedPerson> persons = service.findAll();
		if (persons.isEmpty()) {
			return "{ no related persons found in the database } ";
		}
		
		return jsonObjectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(persons);
	}
}
