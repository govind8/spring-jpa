package com.moneycatcha.app.entity;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Summary {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "summary", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DocumentationInstructionsType documentationInstructions;
    
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "summary", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Fee> fee;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "summary", cascade = CascadeType.ALL, orphanRemoval = true)
    protected LoanToValuationRatio loanToValuationRatio;
    
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "summary", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ServiceabilityResults> serviceabilityResults;
    
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "summary", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SignatureType> signature;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allPartiesAgreeToElectronicSignature;

    @Column(columnDefinition = "DATE")
    protected LocalDate feesDisclosureDate;

    public Summary() {
    	// JPA
    }
    
	// Setter - DocumentationInstructionsType
	public void setDocumentationInstructions(DocumentationInstructionsType documentationInstructions) {
		if (documentationInstructions != null) {
			this.documentationInstructions = documentationInstructions;
			this.documentationInstructions.setSummary(this);
		}
	}

	// Setter - Fee
	public void setFee(List<Fee> fee) {
		if (fee != null) {
			this.fee = fee;
			this.fee.forEach(x -> x.setSummary(this));
		}
	}

	// Setter - LoanToValuationRatio
	public void setLoanToValuationRatio(LoanToValuationRatio loanToValuationRatio) {
		if (loanToValuationRatio != null) {
			this.loanToValuationRatio = loanToValuationRatio;
			this.loanToValuationRatio.setSummary(this);
		}
	}

	// Setter - ServiceabilityResults
	public void setServiceabilityResults(List<ServiceabilityResults> serviceabilityResults) {
		if (serviceabilityResults != null) {
			this.serviceabilityResults = serviceabilityResults;
			this.serviceabilityResults.forEach(x -> x.setSummary(this));
		}
	}

	// Setter - SignatureType
	public void setSignature(List<SignatureType> signature) {
		if (signature != null) {
			this.signature = signature;
			this.signature.forEach(x -> x.setSummary(this));
		}
	}

    

}