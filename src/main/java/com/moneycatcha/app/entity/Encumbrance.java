package com.moneycatcha.app.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.moneycatcha.app.model.EncumbranceInFavourOfCapacityList;
import com.moneycatcha.app.model.NonRealEstateAssetEncumbranceList;
import com.moneycatcha.app.model.NonRealEstateSecurityPriorityList;
import com.moneycatcha.app.model.RealEstateAssetEncumbranceList;
import com.moneycatcha.app.model.SecurityPriorityList;

import lombok.Data;

@Data
@Entity
public class Encumbrance {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    protected NonRealEstateAsset nonRealEstateAsset;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "real_estate_asset_id")
    protected RealEstateAsset realEstateAsset;
    
    @OneToMany(mappedBy = "encumbrance", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<InFavourOf> inFavourOf;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected NonRealEstateAssetEncumbranceList nonrealEncumbranceType;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected NonRealEstateSecurityPriorityList nonrealSecurityPriority;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected RealEstateAssetEncumbranceList realEncumbranceType;


    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SecurityPriorityList securityPriority;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String registeredNumber;

    @Column(columnDefinition = "DATE")
    protected LocalDate registrationDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    // Setters - InFavourOf
    public void setInFavourOf(List<InFavourOf> inFavourOf) {
    	if (inFavourOf != null) {
	        inFavourOf.forEach(x -> x.setEncumbrance(this));
	        this.inFavourOf = inFavourOf;    	
    	}
    }
    
    public Encumbrance() {
    	// JPA
    }
   

	@Data
    @Entity
    public static class InFavourOf {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "encumbrance_id")
        protected Encumbrance encumbrance;
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected EncumbranceInFavourOfCapacityList capacity;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String name;

//        @OneToOne(mappedBy = "", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xInFavourOf;

        public InFavourOf() {
        	// JPA
        }
        
    }
 
}
