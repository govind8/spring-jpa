package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.DepositAccountTypeList;
import com.moneycatcha.app.model.SourceOfFundsTypeList;
import com.moneycatcha.app.model.StatementCycleList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Entity
public class DepositAccountDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="depositAccountDetails", cascade=CascadeType.ALL, orphanRemoval=true)
    protected PercentOwnedType accountOwners;

    @Embedded
    protected Commission commission;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="depositAccountDetails", cascade=CascadeType.ALL, orphanRemoval=true)
    protected DocumentationInstructionsType documentationInstructions;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="depositAccountDetails", cascade=CascadeType.ALL, orphanRemoval=true)
    protected List<FeaturesSelected> featuresSelected;

    @Embedded
    protected Package _package;

    @Getter @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="depositAccountDetails", cascade=CascadeType.ALL, orphanRemoval=true, fetch=FetchType.LAZY)
    protected List<SourceOfInitialDeposit> sourceOfInitialDeposit;

    @Column(precision=19, scale=2)
    protected BigDecimal initialDepositAmount;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String originatorReferenceID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productName;

    @Column(precision=19, scale=2)
    protected BigDecimal proposedAnnualInterestRate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected StatementCycleList statementCycle;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected DepositAccountTypeList type;
	

    
    public DepositAccountDetails() {
    	// JPA
    }


	@Data
    @Embeddable
    public static class Commission {
    	
        @Column(precision=19, scale=2)
        protected BigDecimal commissionAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList commissionPaid;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected CommissionStructureList commissionStructure;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String otherCommissionStructureDescription;

        @Column(name="commission_promotionCode", columnDefinition = "VARCHAR(80)")
        protected String promotionCode;

        @Enumerated(EnumType.STRING)
        @Column(name="commission_thirdPartyReferee", length = 3)
        protected YesNoList thirdPartyReferee;

        @Column(name="commission_trail", precision=19, scale=2)
        protected BigDecimal trail;

        public Commission() {
        	// JPA
        }
        
    }

    @Data
    @Embeddable
    public static class Package {

        @Column(name="package_category", columnDefinition = "VARCHAR(80)")
        protected String category;

        @Column(name="package_code", columnDefinition = "VARCHAR(80)")
        protected String code;

        @Column(name="package_member_id", columnDefinition = "VARCHAR(80)")
        protected String memberID;

        @Column(name="package_name", columnDefinition = "VARCHAR(80)")
        protected String name;

        @Column(name="package_option_code", columnDefinition = "VARCHAR(80)")
        protected String optionCode;

        @Column(name="package_organisation", columnDefinition = "VARCHAR(80)")
        protected String organisation;

        public Package() {
        	// JPA
        }
       
    }


    @Data
    @Entity
    @Table(name = "source_of_initial_deposit")
    public static class SourceOfInitialDeposit {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
   	
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="deposit_account_details_id")
        protected DepositAccountDetails depositAccountDetails;
        
        @Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Column(precision=19, scale=2)
        protected BigDecimal percentage;

        @Enumerated(EnumType.STRING)
        @Column(length = 80)
        protected SourceOfFundsTypeList type;

        public SourceOfInitialDeposit() {
        	// JPA
        }
        
    }

	// Setters - PercentOwnedType public void
	public void setAccountOwners(PercentOwnedType accountOwners) { 
		if(accountOwners != null) {
			this.accountOwners = accountOwners; 
			this.accountOwners.setDepositAccountDetails(this); 
		}
	}
	  
	
	// Setters - DocumentationInstructionsType public void
	public void setDocumentationInstructions(DocumentationInstructionsType  documentationInstructions) { 
		if(documentationInstructions != null) {
			this.documentationInstructions = documentationInstructions; 
			this.documentationInstructions.setDepositAccountDetails(this); 
		}
	}
	
  
	// Setters - FeaturesSelected public void
	public void  setFeaturesSelected(List<FeaturesSelected> featuresSelected) { 
		if(featuresSelected != null) {
	 		this.featuresSelected = featuresSelected; 
			this.featuresSelected.forEach(x -> x.setDepositAccountDetails(this)); 
		}
	}
  
	  
	// Setters - SourceOfInitialDeposit public void
	public void setSourceOfInitialDeposit(List<SourceOfInitialDeposit> sourceOfInitialDeposit) { 
		if(sourceOfInitialDeposit != null) {
			this.sourceOfInitialDeposit = sourceOfInitialDeposit; 
			this.sourceOfInitialDeposit.forEach(x -> x.setDepositAccountDetails(this));
		}
	}
	

}
