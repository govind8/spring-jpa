package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.PartnerTypeList;

import lombok.Data;
@Entity
@Data
public class Partner {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;    
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "related_company_id")
	RelatedCompany relatedCompany;    
	
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PartnerTypeList partnerType;

    @Column(precision=19, scale=2)
    protected BigDecimal percentOwned;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPartner;

    public Partner() {
    	// JPA
    }
 
}
