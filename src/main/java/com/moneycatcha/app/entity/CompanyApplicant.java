package com.moneycatcha.app.entity;
import java.io.Serializable;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.BusinessStructureExcludingTrustList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.CreditStatusList;
import com.moneycatcha.app.model.ExemptStatusList;
import com.moneycatcha.app.model.OecdCRSStatusList;
import com.moneycatcha.app.model.TypeOfIncorporationList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class CompanyApplicant implements Serializable {
	
	private static final long serialVersionUID = -7284867612946717979L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "application_id")
    protected Application application;
 	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL,  orphanRemoval = true, fetch=FetchType.LAZY)
	protected List<AuthorisedSignatory> authorisedSignatory;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, orphanRemoval = true, fetch=FetchType.LAZY)
    protected List<BeneficialOwner> beneficialOwner;
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, orphanRemoval = true)
    protected Business business;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<CreditHistory> creditHistory;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	protected DeclaredIncome declaredIncome;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<Director> director;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected ExistingCustomer existingCustomer;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected FinancialAnalysis financialAnalysis;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected ForeignTaxAssociationType foreignTaxAssociation;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
	protected IncomePrevious incomePrevious;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected IncomePrior incomePrior;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected IncomeRecent incomeRecent;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected IncomeYearToDate incomeYearToDate;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<Partner> partner;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected RelatedLegalEntities relatedLegalEntities;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected ResponsibleLendingType responsibleLending;

	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<Shareholder> shareholder;

	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="companyApplicant", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<SourceOfWealth> sourceOfWealth;
    
	@Column(columnDefinition = "VARCHAR(80)")
    protected String abn;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList abnVerified;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String acn;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList allowDirectMarketing;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList allowThirdPartyDisclosure;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected ApplicantTypeList applicantType;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String arbn;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String arsn;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String businessName;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList businessNameSameAsCompanyName;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected BusinessStructureExcludingTrustList businessStructure;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String companyName;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected CreditStatusList creditStatus;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String customerTypeCode;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String customerTypeDescription;

	@Column(columnDefinition = "DATE")
    protected LocalDate dateRegistered;

	@Column(columnDefinition = "VARCHAR(100)")
    protected String exchangeListedOn;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected ExemptStatusList exemptStatus;

	@Column(columnDefinition = "DATE")
    protected LocalDate gstRegisteredDate;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList governmentOrganisation;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList isExistingCustomer;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList isHoldingCompany;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String licenceNumber;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList localAgentOfForeignCompany;
	
	@Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfDirectors;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfPartners;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfShareholders;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected OecdCRSStatusList oecdcrsStatus;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList primaryApplicant;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList publicAuthority;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String reasonForObtainingLoan;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList registeredForGST;

	@Enumerated(EnumType.STRING)
	@Column(length = 5)
    protected CountryCodeList registeredInCountry;

	@Enumerated(EnumType.STRING)
	@Column(length = 5)
    protected AuStateList registeredInState;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList tradingMoreThanTwoYears;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected TypeOfIncorporationList typeOfIncorporation;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xAccountant;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xSoleTrader;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xSolicitor;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xTradeReference;
     
	
    public CompanyApplicant() {
    	// JPA
    }

    // Setter - 
	public void setAuthorisedSignatory(List<AuthorisedSignatory> authorisedSignatory) {
		if (authorisedSignatory != null) {
			this.authorisedSignatory = authorisedSignatory;
			this.authorisedSignatory.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setBeneficialOwner(List<BeneficialOwner> beneficialOwner) {
		if (beneficialOwner != null) {
			this.beneficialOwner = beneficialOwner;
			this.beneficialOwner.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setBusiness(Business business) {
		if (business != null) {
			this.business = business;
			this.business.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setCreditHistory(List<CreditHistory> creditHistory) {
		if (creditHistory != null) {
			this.creditHistory = creditHistory;
			this.creditHistory.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setDeclaredIncome(DeclaredIncome declaredIncome) {
		if (declaredIncome != null) {
			this.declaredIncome = declaredIncome;
			this.declaredIncome.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setDirector(List<Director> director) {
		if (director != null) {
			this.director = director;
			this.director.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setExistingCustomer(ExistingCustomer existingCustomer) {
		if (existingCustomer != null) {
			this.existingCustomer = existingCustomer;
			this.existingCustomer.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setFinancialAnalysis(FinancialAnalysis financialAnalysis) {
		if (financialAnalysis != null) {
			this.financialAnalysis = financialAnalysis;
			this.financialAnalysis.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setForeignTaxAssociation(ForeignTaxAssociationType foreignTaxAssociation) {
		if (foreignTaxAssociation != null) {
			this.foreignTaxAssociation = foreignTaxAssociation;
			this.foreignTaxAssociation.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setIncomePrevious(IncomePrevious incomePrevious) {
		if (incomePrevious != null) {
			this.incomePrevious = incomePrevious;
			this.incomePrevious.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setIncomePrior(IncomePrior incomePrior) {
		if (incomePrior != null) {
			this.incomePrior = incomePrior;
			this.incomePrior.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setIncomeRecent(IncomeRecent incomeRecent) {
		if (incomeRecent != null) {
			this.incomeRecent = incomeRecent;
			this.incomeRecent.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setIncomeYearToDate(IncomeYearToDate incomeYearToDate) {
		if (incomeYearToDate != null) {
			this.incomeYearToDate = incomeYearToDate;
			this.incomeYearToDate.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setPartner(List<Partner> partner) {
		if (partner != null) {
			this.partner = partner;
			this.partner.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setRelatedLegalEntities(RelatedLegalEntities relatedLegalEntities) {
		if (relatedLegalEntities != null) {
			this.relatedLegalEntities = relatedLegalEntities;
			this.relatedLegalEntities.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setResponsibleLending(ResponsibleLendingType responsibleLending) {
		if (responsibleLending != null) {
			this.responsibleLending = responsibleLending;
			this.responsibleLending.setCompanyApplicant(this);
		}
	}

    // Setter - 
	public void setShareholder(List<Shareholder> shareholder) {
		if (shareholder != null) {
			this.shareholder = shareholder;
			this.shareholder.forEach(x -> x.setCompanyApplicant(this));
		}
	}

    // Setter - 
	public void setSourceOfWealth(List<SourceOfWealth> sourceOfWealth) {
		if (sourceOfWealth != null) {
			this.sourceOfWealth = sourceOfWealth;
			this.sourceOfWealth.forEach(x -> x.setCompanyApplicant(this));
		}
	}
   
    
}