package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.entity.Instructions.ApplicationInstructions;
import com.moneycatcha.app.model.CreditReportingBodyList;
import com.moneycatcha.app.model.DeclinedReasonApplicationInstructionsList;
import com.moneycatcha.app.model.EventNameApplicationInstructionsList;
import com.moneycatcha.app.model.StatusNameApplicationInstructionsList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Update {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_instructions_id")
	ApplicationInstructions applicationInstructions;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "update", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Event> event;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "update", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Status status;

    @Data
    @Entity
    @Table(name = "update_event")
    public static class Event {
   
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "update_id")
    	Update update;
    	
        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;
        
        @Column(columnDefinition = "TIMESTAMP")
        protected LocalDateTime dateTime;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected EventNameApplicationInstructionsList name;

        public Event() {
        	// jPA
        }
    }

    @Data
    @Entity
    @Table(name = "update_status")
    public static class Status {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "update_id")
    	Update update;
    	
    	@Setter(AccessLevel.NONE)
    	@OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<Condition> condition;

    	@Setter(AccessLevel.NONE)
        @OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<Declined> declined;

    	@Setter(AccessLevel.NONE)
        @OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<PreApproved> preApproved;

        @Column(columnDefinition = "TIMESTAMP")
        protected LocalDateTime dateTime;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected StatusNameApplicationInstructionsList name;


        @Data
        @Entity
        @Table(name = "status_declined")
        public static class Declined {
        	
           	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;

        	@ManyToOne(fetch=FetchType.LAZY)
        	@JoinColumn(name = "status_id")
        	Status status;
       	
            @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;   
            
            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected CreditReportingBodyList creditReportingBody;

            @Column(columnDefinition = "VARCHAR(255)")
            protected String details;

            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected DeclinedReasonApplicationInstructionsList reason;

            public Declined() {
            	// JPA
            }

        }


        @Data
        @Entity
        @Table(name = "status_preapproved")
        public static class PreApproved {
        	
           	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;

           	@ManyToOne(fetch=FetchType.LAZY)
        	@JoinColumn(name = "status_id")
        	Status status;
        	
            @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;       
            
            @Column(precision=19, scale=2)
            protected BigDecimal establishmentAndGovernmentFees;

            @Column(precision=19, scale=2)
            protected BigDecimal estimatedBorrowingPower;

            @Column(precision=19, scale=2)
            protected BigDecimal estimatedRepaymentAmount;

            public PreApproved() {
            	// JPA
            }

        }

        // Constructors
		public Status() {
			//JPA
		}

		// Setters - Condition
		public void setCondition(List<Condition> condition) {
			if (condition != null) {
				this.condition = condition;
				this.condition.forEach(x -> x.setStatus(this));
			}
		}
		
		// Setters - Declined
		public void setDeclined(List<Declined> declined) {
			if (declined != null) {
				this.declined = declined;
				this.declined.forEach(x -> x.setStatus(this));
			}
		}
		
		// Setters - PreApproved
		public void setPreApproved(List<PreApproved> preApproved) {
			if (preApproved != null) {
				this.preApproved = preApproved;
				this.preApproved.forEach(x -> x.setStatus(this));
			}
		}
        
    }
    
    // Constructors
    
    public Update() {
    	// JPA
    }
 
    // Setters -  Event
	public void setEvent(List<Update.Event> event) {
		if (event != null) {
			this.event = event;
			this.event.forEach(x -> x.setUpdate(this));
		}
	}
	
	// Setters - Status
	public void setStatus(Update.Status status) {
		if (status != null) {
			this.status = status;
			this.status.setUpdate(this);
		}
	}
     
}
