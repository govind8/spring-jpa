package com.moneycatcha.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.PowerOfAttorneyTypeList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class PowerOfAttorney {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "powerOfAttorney", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<DealingNumberType> dealingNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PowerOfAttorneyTypeList type;

    //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xpoaHolder;

    public PowerOfAttorney() {
    	// JPA
    }
    
    // Setter - DealingNumber Type
	public void setDealingNumber(List<DealingNumberType> dealingNumber) {
		if (dealingNumber != null) {
			this.dealingNumber = dealingNumber;
			this.dealingNumber.forEach(x -> x.setPowerOfAttorney(this));
		}
	}

}
