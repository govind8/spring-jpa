package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.AccountStatusList;
import com.moneycatcha.app.model.CreditCardTypeList;

import lombok.Data;

@Data
@Entity
public class CreditCard {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "proposed_repayment_id")
    ProposedRepayment proposedRepayment;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_package_id")
    ProductPackage productPackage;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fee_id")
    Fee fee;
    
	@OneToOne(mappedBy = "creditCard", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Features features;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String cardHolderName;

    @Column(columnDefinition = "VARCHAR(20)")
    protected String expiryMonth;

    @Column(columnDefinition = "VARCHAR(20)")
    protected String expiryYear;

    @Column(columnDefinition = "VARCHAR(20)")
    protected String number;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CreditCardTypeList type;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AccountStatusList inclusionType;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xCreditCard;
    

    public CreditCard() {
    	// JPA
    }
 
    // Setter
	public void setFeatures(Features features) {
		if (features != null) {
			features.setCreditCard(this);
			this.features = features;
		}
	}
    


}