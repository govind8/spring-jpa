package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.InsuranceTypeList;
import com.moneycatcha.app.model.InsurerList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
@Data
@Entity
public class Insurance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="insurance", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<AssociatedLoanAccount> associatedLoanAccount;

    @Embedded
    protected CommissionPayable commissionPayable;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="insurance", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<InsuredParty> insuredParty;

//    @OneToOne(fetch=FetchType.LAZY)
//    @JoinColumn(name = "xInsurance", referencedColumnName = "uniqueid")
//    protected NonRealEstateAsset nonRealEstateAsset;
    
    @Embedded
    protected Premium premium;    
    
    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "DATE")
    protected LocalDate effectiveDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate expiryDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected InsuranceTypeList insuranceType;

    @Column(precision=19, scale=2)
    protected BigDecimal insuredAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected InsurerList insurer;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherInsurerName;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String policyNumber;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    


    public Insurance() {
    	// JPA
    }

    // Setters - AssociatedLoanAccount
    public void setAssociatedLoanAccount(List<AssociatedLoanAccount> associatedLoanAccount) {
    	if (associatedLoanAccount != null) {
	        this.associatedLoanAccount = associatedLoanAccount;    	
	    	this.associatedLoanAccount.forEach(x -> x.setInsurance(this));
    	}
    }
    
    // Setters - InsuredParty
    public void setInsuredParty(List<InsuredParty> insuredParty) {
    	if (insuredParty != null) {
	         this.insuredParty = insuredParty;    	
	         this.insuredParty.forEach(x -> x.setInsurance(this));
    	}
    }
    

    @Data
    @Embeddable
    public static class CommissionPayable {

        @Column(name = "commission_payable_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Column(name = "commission_payable_percentage", precision=19, scale=2)
        protected BigDecimal percentage;

        @Column(name = "commission_payable_xPayer", columnDefinition = "VARCHAR(80)")
        protected String xPayer;
        
        public CommissionPayable() {
        	// JPA
        }
      
    }


    @Data
    @Entity
    @Table(name = "insurance_insured_party")
    public static class InsuredParty {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
 
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected Insurance insurance;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xInsuredParty;

        public InsuredParty() {
        	// JPA
        }
    }

    @Data
    @Embeddable
    public static class Premium {
    	
        @Column(name = "premium_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "premium_frequency", length = 15)
        protected FrequencyShortList frequency;
        
        public Premium() {
        	// premium
        }

    }
}

