
package com.moneycatcha.app.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RepaymentOptions {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
 
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "retirement_details_id")
	protected RetirementDetails retirementDetails;    
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "applicant_id")
	protected Applicant applicant;    
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "repaymentOptions", cascade = CascadeType.ALL, orphanRemoval = true)
	protected RecurringIncomeFromSuperannuationDetails recurringIncomeFromSuperannuationDetails; 
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "repaymentOptions", cascade = CascadeType.ALL, orphanRemoval = true)
	protected SaleOfAssetsDetails saleOfAssetsDetails; 
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "repaymentOptions", cascade = CascadeType.ALL, orphanRemoval = true)
	protected SavingsDetails savingsDetails; 
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "repaymentOptions", cascade = CascadeType.ALL, orphanRemoval = true)
	protected SuperannuationLumpSumFollowingRetirementDetails superannuationLumpSumFollowingRetirementDetails; 
	
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList coApplicantIncome;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList downsizingHome;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList incomeFromOtherInvestments;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList other;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList recurringIncomeFromSuperannuation;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList repaymentOfLoanPriorToRetirement;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList saleOfAssets;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList savings;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList superannuationLumpSumFollowingRetirement;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList workPastStatutoryRetirementAge;


    @Data
    @Entity
    @Table(name = "recurring_income_from_superannuation")
    public static class RecurringIncomeFromSuperannuationDetails {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
     
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_options_id")
    	protected RepaymentOptions repaymentOptions;  
    	
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList financialPlannerDocumentation;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String financialPlannerDocumentationDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList superStatementShowingProjectedRecurringIncome;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String superStatementShowingProjectedRecurringIncomeDescription;

    }


    @Data
    @Entity
    @Table(name = "sales_of_assets_details")
    public static class SaleOfAssetsDetails {
 
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
     
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_options_id")
    	protected RepaymentOptions repaymentOptions;  
    	
    	@Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList lenderHeldSecurity;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String lenderHeldSecurityDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList otherAssets;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String otherAssetsDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList otherLenderHeldProperty;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String otherLenderHeldPropertyDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList otherLenderHeldSecurity;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String otherLenderHeldSecurityDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList shares;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String sharesDescription;

    }


    @Data
    @Entity
    @Table(name = "savings_details")
    public static class SavingsDetails {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
     
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_options_id")
    	protected RepaymentOptions repaymentOptions;  
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList lenderHeldSavingsAccount;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String lenderHeldSavingsAccountDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList otherLenderHeldSavingsAccount;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String otherLenderHeldSavingsAccountDescription;

    }


    @Data
    @Entity
    @Table(name = "sa_lumpsum_details")
    public static class SuperannuationLumpSumFollowingRetirementDetails {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
     
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_options_id")
    	protected RepaymentOptions repaymentOptions;  
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList fpDocumentation;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String fpDocumentationDescription;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList ssspSuperannuationAmount;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String ssspSuperannuationAmountDescription;

    }
    
    // Constructors
    public RepaymentOptions() {
    	// JPA
    }

    // Setters - RecurringIncomeFromSuperannuationDetails
	public void setRecurringIncomeFromSuperannuationDetails(
			RecurringIncomeFromSuperannuationDetails recurringIncomeFromSuperannuationDetails) {
		if (recurringIncomeFromSuperannuationDetails != null) {
			this.recurringIncomeFromSuperannuationDetails = recurringIncomeFromSuperannuationDetails;
			this.recurringIncomeFromSuperannuationDetails.setRepaymentOptions(this);
		}
	}

	// Setters - SavingsDetails
	public void setSavingsDetails(SavingsDetails savingsDetails) {
		if (savingsDetails != null) {
			this.savingsDetails = savingsDetails;
			this.savingsDetails.setRepaymentOptions(this);
		}
	}

	// Setters - SuperannuationLumpSumFollowingRetirementDetails
	public void setSuperannuationLumpSumFollowingRetirementDetails(
			SuperannuationLumpSumFollowingRetirementDetails superannuationLumpSumFollowingRetirementDetails) {
		if (superannuationLumpSumFollowingRetirementDetails != null) {
			this.superannuationLumpSumFollowingRetirementDetails = superannuationLumpSumFollowingRetirementDetails;
			this.superannuationLumpSumFollowingRetirementDetails.setRepaymentOptions(this);
		}
	}

	// Setters - SaleOfAssets
	public void setSaleOfAssetsDetails(SaleOfAssetsDetails saleOfAssetsDetails) {
		if (saleOfAssetsDetails != null) {
			this.saleOfAssetsDetails = saleOfAssetsDetails;
			this.saleOfAssetsDetails.setRepaymentOptions(this);
		}
	}

    
}