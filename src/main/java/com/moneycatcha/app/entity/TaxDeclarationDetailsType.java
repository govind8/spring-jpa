package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.TaxDeclarationExemptionCategoryList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;


@Data
@Entity
public class TaxDeclarationDetailsType {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
    protected PersonApplicant personApplicant;    	

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "related_person_id")
	protected RelatedPerson relatedPerson;    
    
	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList alreadyProvided;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected TaxDeclarationExemptionCategoryList exemptionCategory;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList undeclared;
	
	public TaxDeclarationDetailsType() {
		//jPA
	}

}
