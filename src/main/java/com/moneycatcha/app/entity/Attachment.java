package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Attachment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
	String uniqueID;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String filename;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String uri;
    
    @Column(name = "inline_attachment", columnDefinition = "BYTEA")
    protected byte[] inlineAttachment;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Message message;

	public Attachment() {
		//JPA
	}

    
}
