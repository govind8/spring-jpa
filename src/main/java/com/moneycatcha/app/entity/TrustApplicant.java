package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.OecdCRSStatusList;
import com.moneycatcha.app.model.TrustPurposeList;
import com.moneycatcha.app.model.TrustStructureList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class TrustApplicant {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    	
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<BeneficialOwner> beneficialOwner;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Beneficiary> beneficiary;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Business business;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<DealingNumberType> dealingNumber;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DeclaredIncome declaredIncome;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ExistingCustomer existingCustomer;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAnalysis financialAnalysis;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ForeignTaxAssociationType foreignTaxAssociation;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected IncomePrevious incomePrevious;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected IncomePrior incomePrior;
 
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected IncomeRecent incomeRecent;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected IncomeYearToDate incomeYearToDate;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Settlor> settlor;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SourceOfWealth> sourceOfWealth;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<TrustDeedVariation> trustDeedVariation;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "trustApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Trustee> trustee;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String abn;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ApplicantTypeList applicantType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String businessName;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList businessNameSameAsTrustName;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList countryEstablished;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String customerTypeCode;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String customerTypeDescription;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String documentationType;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList established;

    @Column(columnDefinition = "DATE")
    protected LocalDate establishmentDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList governmentOrganisation;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isExistingCustomer;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfBeneficiaries;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfTrustees;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OecdCRSStatusList oecdcrsStatus;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList primaryApplicant;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
	@Column(columnDefinition = "VARCHAR(255)")
    protected String settlorName;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String trustName;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected TrustPurposeList trustPurpose;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected TrustStructureList trustStructure;
  

	@Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "DATE")
    protected LocalDate vestingDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccountant;

    @Data
    @Entity
    @Table(name = "ta_beneficial_owner")
    public static class BeneficialOwner {
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "application_id")
    	protected TrustApplicant trustApplicant;    	
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xBeneficialOwner;

    }


    @Data
    @Entity
    @Table(name = "ta_beneficiary")
    public static class Beneficiary {
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "application_id")
    	protected TrustApplicant trustApplicant;    	
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xBeneficiary;

    }


    @Data
    @Entity
    @Table(name = "ta_settlor")
    public static class Settlor {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "application_id")
    	protected TrustApplicant trustApplicant;    	
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xSettlor;

    }

    // Constructor
	public TrustApplicant() {
		// JPAs
	}

	// Setters - BeneficialOwner
	public void setBeneficialOwner(List<BeneficialOwner> beneficialOwner) {
		if (beneficialOwner != null) {
			this.beneficialOwner = beneficialOwner;
			this.beneficialOwner.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - Beneficiary
	public void setBeneficiary(List<Beneficiary> beneficiary) {
		if (beneficiary != null) {
			this.beneficiary = beneficiary;
			this.beneficiary.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - Business
	public void setBusiness(Business business) {
		if (business != null) {
			business.setTrustApplicant(this);
			this.business = business;
		}
	}


	// Setters - Contact
	public void setContact(Contact contact) {
		if (contact != null) {
			contact.setTrustApplicant(this);
			this.contact = contact;
		}
	}


	// Setters - DealingNumberType
	public void setDealingNumber(List<DealingNumberType> dealingNumber) {
		if (dealingNumber != null) {
			this.dealingNumber = dealingNumber;
			this.dealingNumber.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - DeclaredIncome
	public void setDeclaredIncome(DeclaredIncome declaredIncome) {
		if (declaredIncome != null) {
			this.declaredIncome = declaredIncome;
			this.declaredIncome.setTrustApplicant(this);
		}
	}


	// Setters - ExistingCustomer
	public void setExistingCustomer(ExistingCustomer existingCustomer) {
		if (existingCustomer != null) {
			this.existingCustomer = existingCustomer;
			this.existingCustomer.setTrustApplicant(this);
		}
	}


	// Setters - BeneficialOwner
	public void setFinancialAnalysis(FinancialAnalysis financialAnalysis) {
		if (financialAnalysis != null) {
			this.financialAnalysis = financialAnalysis;
			this.financialAnalysis.setTrustApplicant(this);
		}
	}


	// Setters - ForeignTaxAssociationType
	public void setForeignTaxAssociation(ForeignTaxAssociationType foreignTaxAssociation) {
		if (foreignTaxAssociation != null) {
			this.foreignTaxAssociation = foreignTaxAssociation;
			this.foreignTaxAssociation.setTrustApplicant(this);
		}
	}


	// Setters - BeneficialOwner
	public void setIncomePrevious(IncomePrevious incomePrevious) {
		if (incomePrevious != null) {
			this.incomePrevious = incomePrevious;
			this.incomePrevious.setTrustApplicant(this);
		}
	}


	// Setters - BeneficialOwner
	public void setIncomePrior(IncomePrior incomePrior) {
		if (incomePrior != null) {
			this.incomePrior = incomePrior;
			this.incomePrior.setTrustApplicant(this);
		}
	}


	// Setters - IncomeRecent
	public void setIncomeRecent(IncomeRecent incomeRecent) {
		if (incomeRecent != null) {
			this.incomeRecent = incomeRecent;
			this.incomeRecent.setTrustApplicant(this);
		}
	}


	// Setters - IncomeYearToDate
	public void setIncomeYearToDate(IncomeYearToDate incomeYearToDate) {
		if (incomeYearToDate != null) {
			this.incomeYearToDate = incomeYearToDate;
			this.incomeYearToDate.setTrustApplicant(this);
		}
	}


	// Setters - Settlor
	public void setSettlor(List<Settlor> settlor) {
		if (settlor != null) {
			this.settlor = settlor;
			this.settlor.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - SourceOfWealth
	public void setSourceOfWealth(List<SourceOfWealth> sourceOfWealth) {
		if (sourceOfWealth != null) {
			this.sourceOfWealth = sourceOfWealth;
			this.sourceOfWealth.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - TrustDeedVariation
	public void setTrustDeedVariation(List<TrustDeedVariation> trustDeedVariation) {
		if (trustDeedVariation != null) {
			this.trustDeedVariation = trustDeedVariation;
			this.trustDeedVariation.forEach(x -> x.setTrustApplicant(this));
		}
	}


	// Setters - Trustee
	public void setTrustee(List<Trustee> trustee) {
		if (trustee != null) {
			this.trustee = trustee;
			this.trustee.forEach(x -> x.setTrustApplicant(this));
		}
	}


}