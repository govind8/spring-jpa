package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CurrencyCodeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class BusinessIncomeYearToDate {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "self_employed_id")
    protected SelfEmployed selfEmployed;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "businessIncomeYearToDate", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Addback addback;

    @Embedded
    protected ForeignSourcedIncome foreignSourcedIncome;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasForeignSourcedIncome;

    @Column(precision=19, scale=2)
    protected BigDecimal profitAfterTax;

    @Column(precision=19, scale=2)
    protected BigDecimal profitBeforeTax;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Data
    @Embeddable
    public static class ForeignSourcedIncome {
        @Column(name = "fsi_audAmount", precision=19, scale=2)
        protected BigDecimal audAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "fsi_primaryForeignCurrency", length = 60)
        protected CurrencyCodeList primaryForeignCurrency;

    }
    
    // Setters - Addback
    public void setAddback(Addback addback) {
    	if (addback != null) {
    		this.addback = addback;    	
    		this.addback.setBusinessIncomeYearToDate(this);
    	}
    }

    public BusinessIncomeYearToDate() {
    	// JPA
    }
}