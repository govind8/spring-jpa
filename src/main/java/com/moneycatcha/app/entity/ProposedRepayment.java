package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.DayOfWeekList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.LoanPaymentScheduleTypeList;
import com.moneycatcha.app.model.PaymentTimingList;
import com.moneycatcha.app.model.ProposedRepaymentMethodList;
import com.moneycatcha.app.model.WeekList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class ProposedRepayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    LoanDetails loanDetails;
	
    @OneToMany(mappedBy = "proposedRepayment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Authoriser> authoriser;

    @OneToOne(mappedBy = "proposedRepayment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected CreditCard creditCard;

    @OneToOne(mappedBy = "proposedRepayment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType fromAccount;

    @OneToMany(mappedBy = "proposedRepayment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<RegularRepayment> regularRepayment;

    @OneToOne(mappedBy = "proposedRepayment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ProposedRepayment.StructuredPayments structuredPayments;

    @Column(columnDefinition = "DATE")
    protected LocalDate anniversaryDate;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ProposedRepaymentMethodList method;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaymentTimingList paymentTiming;

    @Column(precision=19, scale=2)
    protected BigDecimal perDiemPaymentAmount;

    @Column(columnDefinition = "DATE")
    protected LocalDate perDiemPaymentDate;

    @Column(precision=19, scale=2)
    protected BigDecimal perDiemPaymentGSTAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal perDiemPaymentStampDutyAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList regular;
    
//    @OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccount;


    public ProposedRepayment() {
    	//JPA
    }

    // Setters  Authoriser
	public void setAuthoriser(List<Authoriser> authoriser) {
		if (authoriser != null) {
			this.authoriser = authoriser;
			this.authoriser.forEach(x -> x.setProposedRepayment(this));
		}
	}

    // Setters  CreditCard
	public void setCreditCard(CreditCard creditCard) {
		if (creditCard != null) {
			this.creditCard = creditCard;
			this.creditCard.setProposedRepayment(this);
		}
	}

    // Setters  FinancialAccountType
	public void setFromAccount(FinancialAccountType fromAccount) {
		if (fromAccount != null) {
			this.fromAccount = fromAccount;
			this.fromAccount.setProposedRepayment(this);
		}
	}

    // Setters  RegularRepayment
	public void setRegularRepayment(List<RegularRepayment> regularRepayment) {
		if (regularRepayment != null) {
			this.regularRepayment = regularRepayment;
			this.regularRepayment.forEach(x -> x.setProposedRepayment(this));
		}
	}

    // Setters  StructuredPayments
	public void setStructuredPayments(StructuredPayments structuredPayments) {
		if (structuredPayments != null) {
			this.structuredPayments = structuredPayments;
			this.structuredPayments.setProposedRepayment(this);
		}
	}
 
	@Data
    @Entity
    @Table(name = "authoriser")    
    public static class Authoriser {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "proposed_repayment_id")
        ProposedRepayment proposedRepayment;
        
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList authorised;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList authorityVerified;
        
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xParty;

        public Authoriser() {
        	// JPA
        }
       
    }


    @Data
    @Entity
    @Table(name = "regular_repayment")
    public static class RegularRepayment {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "proposed_repayment_id")
        ProposedRepayment proposedRepayment;
        
        @Column(precision=19, scale=2)
        protected BigDecimal amount;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String dayOfMonth;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected DayOfWeekList dayOfWeek;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList endOfPeriod;

        @Column(columnDefinition = "DATE")
        protected LocalDate firstRepaymentDate;

        @Enumerated(EnumType.STRING)
        @Column(length = 15)
        protected FrequencyFullList frequency;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger frequencyInterval;

        @Column(precision=19, scale=2)
        protected BigDecimal gstAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList interestPayment;

        @Column(columnDefinition = "DATE")
        protected LocalDate lastRepaymentDate;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanPaymentScheduleTypeList loanPaymentScheduleType;

        @Column(precision=19, scale=2)
        protected BigDecimal minimumAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList principalPayment;

        @Column(precision=19, scale=2)
        protected BigDecimal stampDutyAmount;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger totalRepayments;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected WeekList week;
        
        public RegularRepayment() {
        	//JPA
        }

    }

    @Data
    @Entity
    @Table(name = "structured_payments")
    public static class StructuredPayments {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "proposed_repayment_id")
        ProposedRepayment proposedRepayment;

        @OneToMany(mappedBy = "structuredPayments", cascade = CascadeType.ALL, orphanRemoval = true)
    	protected List<Payment> payment;

        @Enumerated(EnumType.STRING)
        @Column(length = 15)
        protected FrequencyFullList baseFrequency;

		
        public StructuredPayments() {
        	// JPA
        }

		public void setPayment(List<Payment> payment) {
			if (payment != null) {
				this.payment = payment;
				this.payment.forEach(x -> x.setStructuredPayments(this));
			}
		}


		@Data
        @Entity
        @Table(name = "payment")
        public static class Payment {
			
        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
            @ManyToOne(fetch = FetchType.LAZY)
            @JoinColumn
            StructuredPayments structuredPayments;
            
            @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;

            @Column(precision=19, scale=2)
            protected BigDecimal amount;

            @Column(columnDefinition = "DATE")
            protected LocalDate date;

            @Column(precision=19, scale=2)
            protected BigDecimal gstAmount;

            @Column(columnDefinition = "BIGINT")
            protected BigInteger sequenceNumber;

            @Column(precision=19, scale=2)
            protected BigDecimal stampDutyAmount;
            
            public Payment() {
            	// JPA
            }
        }

    }

}
