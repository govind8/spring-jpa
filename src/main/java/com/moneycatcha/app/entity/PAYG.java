package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.EmployerTypeList;
import com.moneycatcha.app.model.EmploymentStatusList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.PaygBasisList;
import com.moneycatcha.app.model.ProofCodePAYGList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class PAYG {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employment_id")
    protected Employment employment;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "payg_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "payg_duration_units"))
    })
    protected DurationType duration;

    @Embedded
    protected Income income;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String anzscoOccupationCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String australianBIC;

    @Column(precision=19, scale=2)
    protected BigDecimal averageHoursPerWeek;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaygBasisList basis;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList companyCar;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String customIndustryCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EmployerTypeList employerType;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String gicsCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeStatusOnOrBeforeSettlementList incomeStatusOnOrBeforeSettlement;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String industry;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String industryCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String mainBusinessActivity;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String occupation;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String occupationCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList onProbation;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String positionTitle;

    @Column(columnDefinition = "DATE")
    protected LocalDate probationDateEnds;

    @Column(columnDefinition = "DATE")
    protected LocalDate probationDateStarts;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EmploymentStatusList status;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xEmployer;

	public PAYG() {
		// JPA
	}
    
    @Data
    @Embeddable
    public static class Income {

        @Column(name = "income_bonusAmount", precision=19, scale=2)
        protected BigDecimal bonusAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_bonusFrequency", length = 15)
        protected FrequencyShortList bonusFrequency;

        @Column(name = "income_carAllowanceAmount", precision=19, scale=2)
        protected BigDecimal carAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_carAllowanceFrequency", length = 15)
        protected FrequencyShortList carAllowanceFrequency;

        @Column(name = "income_commissionAmount", precision=19, scale=2)
        protected BigDecimal commissionAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_commissionFrequency", length = 15)
        protected FrequencyShortList commissionFrequency;

        @Column(name = "income_grossRegularOvertimeAmount", precision=19, scale=2)
        protected BigDecimal grossRegularOvertimeAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_grossRegularOvertimeFrequency", length = 15)
        protected FrequencyShortList grossRegularOvertimeFrequency;

        @Column(name = "income_grossSalaryAmount", precision=19, scale=2)
        protected BigDecimal grossSalaryAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_grossSalaryFrequency", length = 15)
        protected FrequencyShortList grossSalaryFrequency;

        @Column(name = "income_netBonusAmount", precision=19, scale=2)
        protected BigDecimal netBonusAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netBonusFrequency", length = 15)
        protected FrequencyShortList netBonusFrequency;

        @Column(name = "income_netCarAllowanceAmount", precision=19, scale=2)
        protected BigDecimal netCarAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netCarAllowanceFrequency", length = 15)
        protected FrequencyShortList netCarAllowanceFrequency;

        @Column(name = "income_netCommissionAmount", precision=19, scale=2)
        protected BigDecimal netCommissionAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netCommissionFrequency", length = 15)
        protected FrequencyShortList netCommissionFrequency;

        @Column(name = "income_netRegularOvertimeAmount", precision=19, scale=2)
        protected BigDecimal netRegularOvertimeAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netRegularOvertimeFrequency", length = 15)
        protected FrequencyShortList netRegularOvertimeFrequency;

        @Column(name = "income_netSalaryAmount", precision=19, scale=2)
        protected BigDecimal netSalaryAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netSalaryFrequency", length = 15)
        protected FrequencyShortList netSalaryFrequency;

        @Column(name = "income_netWorkAllowanceAmount", precision=19, scale=2)
        protected BigDecimal netWorkAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netWorkAllowanceFrequency", length = 15)
        protected FrequencyShortList netWorkAllowanceFrequency;

        @Column(name = "income_netWorkersCompensationAmount", precision=19, scale=2)
        protected BigDecimal netWorkersCompensationAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netWorkersCompensationFrequency", length = 15)
        protected FrequencyShortList netWorkersCompensationFrequency;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_proofCode", length = 60)
        protected ProofCodePAYGList proofCode;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_proofSighted", length = 3)
        protected YesNoList proofSighted;

        @Column(name = "income_workAllowanceAmount", precision=19, scale=2)
        protected BigDecimal workAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_workAllowanceFrequency", length = 15)
        protected FrequencyShortList workAllowanceFrequency;

        @Column(name = "income_workersCompensationAmount", precision=19, scale=2)
        protected BigDecimal workersCompensationAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_workersCompensationFrequency", length = 15)
        protected FrequencyShortList workersCompensationFrequency;

		public Income() {
			//JPA
		}
        
		
    }


}