package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.entity.Instructions.ApplicationInstructions;
import com.moneycatcha.app.model.AssessmentTypeApplicationInstructionsList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Submit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_instructions_id")
	ApplicationInstructions applicationInstructions;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "submit", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Condition> condition;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AssessmentTypeApplicationInstructionsList assessmentType;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isAccountVariation;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isResubmission;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isSubmissionDocuments;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isSupportingDocuments;

     
    public Submit() {
    	// JPA
    }

    // Setter - Condition
	public void setCondition(List<Condition> condition) {
		if (condition != null) {
			this.condition = condition;
			this.condition.forEach(x -> x.setSubmit(this));
		}
	}
    
}