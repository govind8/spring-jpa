package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public  class IncomePrevious {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "incomePrevious", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Addback addback;
    
    @Column(precision=19, scale=2)
    protected BigDecimal companyProfitAfterTax;

    @Column(precision=19, scale=2)
    protected BigDecimal companyProfitBeforeTax;

    @Column(columnDefinition = "VARCHAR(80)")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList incomeGreaterThanPreviousYear;

    @Column(columnDefinition = "VARCHAR(80)")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList taxOfficeAssessments;

    @Column(precision=19, scale=2)
    protected BigDecimal profitAfterTax;

    @Column(precision=19, scale=2)
    protected BigDecimal profitBeforeTax;

    @Column(columnDefinition = "VARCHAR(80)")
	protected String xAccountant;
 
    // Setters - Addback
    public void setAddback(Addback addback) {
    	if (addback != null) {
	        this.addback = addback;    	
	    	this.addback.setIncomePrevious(this);
    	}
    }    	
	
    public IncomePrevious() {
    	// JPA
    }


}

