package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ClearingFromOtherSourceList;
import com.moneycatcha.app.model.ClearingFromThisLoanList;
import com.moneycatcha.app.model.CreditCardTypeList;
import com.moneycatcha.app.model.InterestChargeFrequencyList;
import com.moneycatcha.app.model.InterestTypeList;
import com.moneycatcha.app.model.LiabilityTypeList;
import com.moneycatcha.app.model.LoanPaymentScheduleTypeList;
import com.moneycatcha.app.model.LoanTermUnitsList;
import com.moneycatcha.app.model.NccpStatusList;
import com.moneycatcha.app.model.PaymentTimingList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.RepaymentFrequencyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Liability {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Application application;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Arrears arrears;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ContinuingRepayment> continuingRepayment;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<DiscountMargin> discountMargin;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DocumentationInstructionsType documentationInstructions;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DSH dsh;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FeaturesSelected featuresSelected;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<LendingPurpose> lendingPurpose;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected AmountInForeignCurrencyType originalAmountRequestedInForeignCurrency;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected OriginalTerm originalTerm;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwned percentOwned;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<RateComposition> rateComposition;

	@Embedded
    protected RemainingTerm remainingTerm;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Repayment> repayment;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Security> security;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Software software;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "liability", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<TermsAndConditions> termsAndConditions;

    @Column(precision=19, scale=2)
    protected BigDecimal accelerationPercentage;

    @Column(precision=19, scale=2)
    protected BigDecimal annualInterestRate;

    @Column(precision=19, scale=2)
    protected BigDecimal availableForRedrawAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal balloonRepaymentAmount;

    @Column(columnDefinition = "DATE")
    protected LocalDate balloonRepaymentDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ClearingFromOtherSourceList clearingFromOtherSource;

    @Column(precision=19, scale=2)
    protected BigDecimal clearingFromOtherSourceAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ClearingFromThisLoanList clearingFromThisLoan;

    @Column(precision=19, scale=2)
    protected BigDecimal clearingFromThisLoanAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList closingOnSettlement;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CreditCardTypeList creditCardType;

    @Column(precision=19, scale=2)
    protected BigDecimal creditLimit;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String creditRiskGrade;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasArrears;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasPreviousArrears;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasUndrawnFunds;

    @Column(precision=19, scale=2)
    protected BigDecimal inAdvanceAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected InterestChargeFrequencyList interestCalculationFrequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected InterestChargeFrequencyList interestChargeFrequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isOriginalAmountRequestedInForeignCurrency;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String lenderAssessmentReason;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList lenderAssessmentRequired;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList limitExceededCurrently;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList limitExceededPreviously;

    @Column(columnDefinition = "DATE")
    protected LocalDate maturityDate;

    @Column(precision=19, scale=2)
    protected BigDecimal minimumRepaymentRate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected NccpStatusList nccpStatus;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList negativelyGeared;

    @Column(precision=19, scale=2)
    protected BigDecimal negativelyGearedPercentage;

    @Column(precision=19, scale=2)
    protected BigDecimal newLimit;

    @Column(precision=19, scale=2)
    protected BigDecimal nonCapitalisedInterest;

    @Column(precision=19, scale=2)
    protected BigDecimal originalAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PrimaryPurposeLoanPurposeList originalLoanPurpose;

    @Column(columnDefinition = "DATE")
    protected LocalDate originationDate;

    @Column(precision=19, scale=2)
    protected BigDecimal outstandingBalance;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList overdrawn;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productName;

    @Column(precision=19, scale=2)
    protected BigDecimal refinanceAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal refinanceCosts;

    @Column(columnDefinition = "DATE")
    protected LocalDate repaidDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList secured;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList smsfLoan;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspended;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LiabilityTypeList type;

    @Column(precision=19, scale=2)
    protected BigDecimal undrawnAmount;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList verified;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList writtenOff;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xCustomerTransactionAnalysis;

   
    public Liability() {
    	// JPA
    }
 

	@Data
    @Entity
    @Table(name = "continuing_repayment")
    public static class ContinuingRepayment {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
 
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "liability_id")
        protected Liability liability;
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanPaymentScheduleTypeList loanPaymentScheduleType;

        @Column(precision=19, scale=2)
        protected BigDecimal minimumAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected PaymentTimingList paymentTiming;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected PaymentTypeList paymentType;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger remainingRepayments;

        @Column(precision=19, scale=2)
        protected BigDecimal repaymentAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 15)
        protected RepaymentFrequencyList repaymentFrequency;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList taxDeductible;

        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        public ContinuingRepayment() {
        	// JPA
        }
        
    }

    @Data
    @Entity
    @Table(name = "original_term")
    public static class OriginalTerm {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
 
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "liability_id")
        protected Liability liability;
    	
        @Setter(AccessLevel.NONE)
        @OneToMany(mappedBy = "originalTerm", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<DistinctLoanPeriod> distinctLoanPeriod;

        @Column(columnDefinition = "DATE")
        protected LocalDate interestOnlyEndDate;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected InterestTypeList interestType;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger interestTypeDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList interestTypeUnits;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected PaymentTypeList paymentType;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger paymentTypeDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList paymentTypeUnits;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger rolloverPeriodDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList rolloverPeriodUnits;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger totalTermDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList totalTermUnits;

        
        public OriginalTerm() {
        	// JPA 
        }
        
        // Setters - DistinctLoanPeriod
        public void setDistinctLoanPeriod(List<DistinctLoanPeriod> distinctLoanPeriod) {
        	if (distinctLoanPeriod != null) {
	            this.distinctLoanPeriod = distinctLoanPeriod;    	
	        	this.distinctLoanPeriod.forEach(x -> x.setOriginalTerm(this));
        	}
        }	        
       
    }
    
    @Data
    @Entity
    @Table(name = "percent_owned")
    public static class PercentOwned {
        
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @OneToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "liability_id")
        Liability liability;
    	
        @Setter(AccessLevel.NONE)
        @OneToMany(mappedBy = "percentOwned", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<Owner> owner;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected ProportionsList proportions;
        
        public PercentOwned() {
        	// JPA
        }
        
        // Setters - Owner
        public void setOwner(List<Owner> owner) {
        	if (owner != null) {
	            this.owner = owner;    	
	        	this.owner.forEach(x -> x.setPercentOwned(this));
        	}
        }	        

    }
    
    @Data
    @Embeddable
    public static class RemainingTerm {
    	
        @Column(name = "remainingterm_duration", columnDefinition = "BIGINT")
        protected BigInteger duration;

        @Enumerated(EnumType.STRING)
        @Column(name = "remainingterm_units", length = 20)
        protected LoanTermUnitsList units;
        
        public RemainingTerm() {
        	// JPA
        }

    }


    // Setters - FinancialAccountType
    public void setAccountNumber(FinancialAccountType accountNumber) {
    	if (accountNumber != null) {
	        this.accountNumber = accountNumber;    	
	        this.accountNumber.setLiability(this);
    	}
    }	
	
    // Setters - Arrears
    public void setArrears(Arrears arrears) {
    	if (arrears != null) {
	        this.arrears = arrears;    	
	        this.arrears.setLiability(this);
    	}
    }	
    
    // Setters - ContinuingRepayment
    public void setContinuingRepayment(List<ContinuingRepayment> continuingRepayment) {
    	if (continuingRepayment != null) {
	         this.continuingRepayment = continuingRepayment;    	
	         this.continuingRepayment.forEach(x -> x.setLiability(this));
    	}
    }
    
    
    // Setters - DiscountMargin
    public void setDiscountMargin(List<DiscountMargin> discountMargin) {
    	if (discountMargin != null) {
	        this.discountMargin = discountMargin;    	
	    	this.discountMargin.forEach(x -> x.setLiability(this));
    	}
    }	

    // Setters - DocumentationInstructions
    public void setDocumentationInstructions(DocumentationInstructionsType documentationInstructions) {
    	if (documentationInstructions != null) {
	        this.documentationInstructions = documentationInstructions;    	
	    	this.documentationInstructions.setLiability(this);
    	}
    }	    
    
    // Setters - DSH
    public void setDSH(DSH dsh) {
    	if (dsh != null) {
	        this.dsh = dsh;    	
	    	this.dsh.setLiability(this);
    	}
    }	

    // Setters - FeaturesSelected
    public void setFeaturesSelected(FeaturesSelected featuresSelected) {
    	if (featuresSelected != null) {
	        this.featuresSelected = featuresSelected;    	
	    	this.featuresSelected.setLiability(this);
    	}
    }	
    
    // Setters - LendingPurpose
    public void setLendingPurpose(List<LendingPurpose> lendingPurpose) {
    	if (lendingPurpose != null) {
	        this.lendingPurpose = lendingPurpose;    	
	    	this.lendingPurpose.forEach(x -> x.setLiability(this));
    	}
    }	
    
    // Setters - AmountInForeignCurrencyType
    public void setOriginalAmountRequestedInForeignCurrency(AmountInForeignCurrencyType originalAmountRequestedInForeignCurrency) {
    	if (originalAmountRequestedInForeignCurrency != null) {
	        this.originalAmountRequestedInForeignCurrency = originalAmountRequestedInForeignCurrency;    	
	    	this.originalAmountRequestedInForeignCurrency.setLiability(this);
    	}
    }	
    
    // Setters - OriginalTerm
    public void setOriginalTerm(OriginalTerm originalTerm) {
    	if (originalTerm != null) {
	        this.originalTerm = originalTerm;    	
	    	this.originalTerm.setLiability(this);
    	}
    }	
    
    // Setters - PercentOwned
    public void setPercentOwned(PercentOwned percentOwned) {
    	if (percentOwned != null) {
	        this.percentOwned = percentOwned;    	
	    	this.percentOwned.setLiability(this);
    	}
    }	
    
    // Setters - RateComposition
    public void setRateComposition(List<RateComposition> rateComposition) {
    	if (rateComposition != null) {
	        this.rateComposition = rateComposition;    	
	    	this.rateComposition.forEach(x -> x.setLiability(this));
    	}
    }	
    
    // Setters - Repayment
    public void setRepayment(List<Repayment> repayment) {
    	if (repayment != null) {
	        this.repayment = repayment;    	
	    	this.repayment.forEach(x -> x.setLiability(this));
    	}
    }	

    // Setters - Security
    public void setSecurity(List<Security> security) {
    	if (security != null) {
	        this.security = security;    	
	    	this.security.forEach(x -> x.setLiability(this));
    	}
    }	
    
    // Setters - Software
    public void setSoftware(Software software) {
    	if (software != null) {
	        this.software = software;    	
	    	this.software.setLiability(this);
    	}
    }	
    
    // Setters - TermsAndConditions
    public void setTermsAndConditions(List<TermsAndConditions> termsAndConditions) {
    	if (termsAndConditions != null) {
	        this.termsAndConditions = termsAndConditions;    	
	    	this.termsAndConditions.forEach(x -> x.setLiability(this));
    	}
    }	    
}