package com.moneycatcha.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CollateralClassList;
import com.moneycatcha.app.model.CollateralTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class PPSR {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "non_real_estate_asset_id" )
    protected NonRealEstateAsset nonRealEstateAsset;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "ppsr", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Registration> registration;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CollateralClassList collateralClass;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CollateralTypeList collateralType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String descriptionOfProceeds;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList pmsi;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList ppsRegistrable;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList proceedsToBeClaimed;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList subjectToControl;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList subordinateRegistration;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String subordinateToNumber;

    
    public PPSR() {
    	// JPA
	}

    // setters - registration
	public void setRegistration(List<Registration> registration) {
		if (registration != null) {
			registration.forEach(x -> x.setPpsr(this));
			this.registration = registration;
		}
	}
}