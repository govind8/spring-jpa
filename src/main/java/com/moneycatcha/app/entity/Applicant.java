package com.moneycatcha.app.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.BenefitEnquiriesList;
import com.moneycatcha.app.model.CoApplicantBenefitList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Applicant {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "serviceability_results_id")
	protected ServiceabilityResults ServiceabilityResults;    

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "co_applicant_interview_id")
	protected CoApplicantInterview coApplicantInterview;    

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "future_circumstances_id")
	protected FutureCircumstances futureCircumstances;    

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "retirement_details_id")
	protected RetirementDetails retirementDetails;
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "applicant", cascade = CascadeType.ALL, orphanRemoval = true)
	protected RepaymentOptions repaymentOptions;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected BenefitEnquiriesList benefitEnquiries;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String benefitEnquiriesDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CoApplicantBenefitList coApplicantBenefit;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String coApplicantReason;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String name;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String number;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectDifficultyUnderstandingObligations;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectExperiencingFinancialAbuse;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectUnderPressure;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectUnfairConduct;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList understandsCoApplicantObligations;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xApplicant;

	// Constructor
    public Applicant() {
    	// JPA
    }
	
    // Setter - RepaymentOptions
	public void setRepaymentOptions(RepaymentOptions repaymentOptions) {
		if (repaymentOptions != null) {
			this.repaymentOptions = repaymentOptions;
			this.repaymentOptions.setApplicant(this);
		}
	}

    @java.lang.Override
    public java.lang.String toString() {
        return "Applicant{" +
                "Id=" + Id +
                ", ServiceabilityResults=" + ServiceabilityResults +
                ", coApplicantInterview=" + coApplicantInterview +
                ", futureCircumstances=" + futureCircumstances +
                ", retirementDetails=" + retirementDetails +
                ", repaymentOptions=" + repaymentOptions +
                ", benefitEnquiries=" + benefitEnquiries +
                ", benefitEnquiriesDescription='" + benefitEnquiriesDescription + '\'' +
                ", coApplicantBenefit=" + coApplicantBenefit +
                ", coApplicantReason='" + coApplicantReason + '\'' +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                ", suspectDifficultyUnderstandingObligations=" + suspectDifficultyUnderstandingObligations +
                ", suspectExperiencingFinancialAbuse=" + suspectExperiencingFinancialAbuse +
                ", suspectUnderPressure=" + suspectUnderPressure +
                ", suspectUnfairConduct=" + suspectUnfairConduct +
                ", understandsCoApplicantObligations=" + understandsCoApplicantObligations +
                ", xApplicant='" + xApplicant + '\'' +
                '}';
    }
}
