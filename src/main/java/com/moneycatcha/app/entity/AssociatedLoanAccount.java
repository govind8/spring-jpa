package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class AssociatedLoanAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "insurance_id")
    protected Insurance insurance;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "lending_guarantee_id")
    protected LendingGuarantee lendingGuarantee;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAssociatedLoanAccount;

    public AssociatedLoanAccount() {
    	// JPA
    }
//	public AssociatedLoanAccount(String xAssociatedLoanAccount) {
//		this.xAssociatedLoanAccount = xAssociatedLoanAccount;
//	}

}