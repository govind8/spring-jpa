package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class DeclaredIncome {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;    
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "self_employed_id")
	SelfEmployed selfEmployed;    
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
    
    @Column(precision=19, scale=2)
    protected BigDecimal incomeAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal netIncomeAmount;
    
    public DeclaredIncome() {
    	// JPA
    }

}
