package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.AccountHoldingList;
import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class OffsetAccount {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "features_selected_id")
    protected FeaturesSelected featuresSelected;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "funds_access_type_details_id")
    protected FundsAccessTypeDetails fundsAccessTypeDetails;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "offsetAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "offsetAccount", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Reason reason;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isExisting;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AccountHoldingList holding;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 30)
    protected ImportanceList importance;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList risksExplained;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccount;
    
    
    public OffsetAccount() {
    	// JPA
    }
     

    // Setters - FinancialAccountType
    public void setAccountNumber(FinancialAccountType accountNumber) {
    	if (accountNumber != null) {
	        this.accountNumber = accountNumber;    	
	    	this.accountNumber.setOffsetAccount(this);
    	}
    }

    // Setters - Reason
	public void setReason(Reason reason) {
		if (reason != null) {
			this.reason = reason;
			this.reason.setOffsetAccount(this);
		}
	}    
    
}