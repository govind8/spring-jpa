package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.BusinessStructureFullList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RelatedCompany {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "relatedCompany", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "relatedCompany", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Partner> partner;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String abn;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList abnVerified;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String acn;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String businessName;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList businessNameSameAsCompanyName;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected BusinessStructureFullList businessStructure;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String companyName;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateRegistered;

    @Column(columnDefinition = "DATE")
    protected LocalDate gstRegisteredDate;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfPartners;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList registeredForGST;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList registeredInCountry;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name ="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    public RelatedCompany() {
    	// JPA
    }

	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setRelatedCompany(this);
		}
	}

	public void setPartner(List<Partner> partner) {
		if (partner != null) {
			this.partner = partner;
			this.partner.forEach(x -> x.setRelatedCompany(this));
		}
	}

    

}