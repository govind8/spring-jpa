package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.AccountHoldingList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class FeaturesSelected {
  

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "deposit_account_details_id")
    protected DepositAccountDetails depositAccountDetails;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Liability liability;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected LoanDetails loanDetails;
   
    // Deposit Account
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="featuresSelected", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<DepositAccount> depositAccount;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="featuresSelected", cascade=CascadeType.ALL,  orphanRemoval = true)
	protected List<ExtraFeature> extraFeature;
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="featuresSelected", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<OffsetAccount> offsetAccount;
    
   
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList chequeBook;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList creditCard;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList debitCard;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList depositBook;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList eftposCard;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList holidayLeave;

    @Enumerated(EnumType.STRING)
    @Column(name = "offset1", length = 3)
    protected YesNoList offset;

    @Column(precision=19, scale=2)
    protected BigDecimal offsetPercentage;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList parentalLeave;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList partialOffset;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList portability;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList progressiveDraw;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList rateLock;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList redraw;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList splitLoan;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList statementEmailDelivery;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList statementPaperDelivery;
    
//    @OneToOne(mappedBy="featuresSelected", cascade=CascadeType.ALL,  orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xApplicant;
    

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList depositAccountRequested;
    
    public FeaturesSelected() {
    	// JPA
    }

	@Data
    @Entity
    @Table(name = "extra_feature")
    public static class ExtraFeature {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected FeaturesSelected featuresSelected;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String description;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String name;

        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        public ExtraFeature() {
        	// JPA
        }
  
    }
	
    @Data
    @Entity
    @Table(name = "features_selected_deposit_account")
    public static class DepositAccount {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="features_selected_id")
        protected FeaturesSelected featuresSelected;
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 80)
        protected AccountHoldingList holding;

        public DepositAccount() {
        	// JPA 
        }

    }

	public void setOffsetAccount(List<OffsetAccount> offsetAccount) {
		if (offsetAccount != null) {
			this.offsetAccount = offsetAccount;
			this.offsetAccount.forEach(x -> x.setFeaturesSelected(this));
		}
	}

	public void setExtraFeature(List<ExtraFeature> extraFeature) {
		if (extraFeature != null) {
			this.extraFeature = extraFeature;
			this.extraFeature.forEach(x -> x.setFeaturesSelected(this));
		}
	}
    
	public void setDepositAccount(List<DepositAccount> depositAccount) {
		if (depositAccount != null) {
			this.depositAccount = depositAccount;
			this.depositAccount.forEach(x -> x.setFeaturesSelected(this));
		}
	}
    	
}