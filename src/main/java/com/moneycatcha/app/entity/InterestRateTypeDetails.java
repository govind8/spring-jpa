package com.moneycatcha.app.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class InterestRateTypeDetails {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "preferences_id")
	protected Preferences preferences;    
	
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "interestRateTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FixedAndVariableRate fixedAndVariableRate;
	
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "interestRateTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FixedRate fixedRate;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "interestRateTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected VariableRate variableRate;

    @Data
    @Entity
    @Table(name = "fixed_and_variable_rate")
    public static class FixedAndVariableRate {

    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "interest_rate_type_details_id")
    	protected InterestRateTypeDetails interestRateTypeDetails;   
    	
    	@Embedded
        @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "fixedandvariablerate_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "fixedandvariablerate_duration_units"))
        })
        protected DurationType fixedPeriodDuration;

        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "fixedAndVariableRate", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "favr_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "favr_risksExplained", length = 3)
        protected YesNoList risksExplained;

        public FixedAndVariableRate() {
        	//JPA
        }
        // Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null) {
				this.reason = reason;
				this.reason.setFixedAndVariableRate(this);
			}
		}
       
    }

    @Data
    @Entity
    @Table(name = "fixed_rate")
    public static class FixedRate {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "interest_rate_type_details_id")
    	protected InterestRateTypeDetails interestRateTypeDetails;   
    	
        @Embedded
	    @AttributeOverrides(value = {
            @AttributeOverride(name = "fixedPeriodDuration.length", column = @Column(name = "fixedrate_duration_length")),
            @AttributeOverride(name = "fixedPeriodDuration.units", column = @Column(name = "fixedrate_duration_units"))
        })
        protected DurationType fixedPeriodDuration;
        
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "fixedRate", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "fr_flexibility", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "fr_risksExplained", length = 3)
        protected YesNoList risksExplained;
       
        public FixedRate() {
        	// JPA
        }
       
        // Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null) {
				this.reason = reason;
				this.reason.setFixedRate(this);
			}
		}
    }

    @Data
    @Entity
    @Table(name = "variable_rate")
    public static class VariableRate {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "interest_rate_type_details_id")
    	protected InterestRateTypeDetails interestRateTypeDetails;   
    	
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "variableRate", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "vr_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "vr_risksExplained", length = 3)
        protected YesNoList risksExplained;


        // Constructors
        public VariableRate() {
        	// jPA
        }
       
        
        // Setter - Reason
		public void setReason(Reason reason) {
			this.reason = reason;
			this.reason.setVariableRate(this);
		}
    }
    
    // Constructors
	public InterestRateTypeDetails() {
		// JPA
	}
	
	// Setter - FixedAndVariableRate
	public void setFixedAndVariableRate(FixedAndVariableRate fixedAndVariableRate) {
		if (fixedAndVariableRate != null) {
			this.fixedAndVariableRate = fixedAndVariableRate;
			this.fixedAndVariableRate.setInterestRateTypeDetails(this);
		}
	}

	// Setter - FixedRate
	public void setFixedRate(FixedRate fixedRate) {
		if (fixedRate != null) {
			this.fixedRate = fixedRate;
			this.fixedRate.setInterestRateTypeDetails(this);
		}
	}

	// Setter - VariableRate
	public void setVariableRate(VariableRate variableRate) {
		if (variableRate != null) {
			this.variableRate = variableRate;
			this.variableRate.setInterestRateTypeDetails(this);
		}
	}
    
		
}