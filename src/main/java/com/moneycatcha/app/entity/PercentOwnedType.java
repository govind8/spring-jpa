package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.entity.Household.ExpenseDetails.LivingExpense;
import com.moneycatcha.app.entity.Household.ExpenseDetails.OtherCommitment;
import com.moneycatcha.app.model.ProportionsList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class PercentOwnedType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "deposit_account_details_id" )
    protected DepositAccountDetails depositAccountDetails;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "lending_purpose_id" )
    protected LendingPurpose lendingPurpose;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "living_expense_id" )
    protected LivingExpense livingExpense;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "other_commitment_id" )
    protected OtherCommitment otherCommitment;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "non_real_estate_asset_id" )
    protected NonRealEstateAsset nonRealEstateAsset;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "real_estate_asset_id" )
    protected RealEstateAsset realEstateAsset;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "security_id" )
    protected Security security;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "master_agreement_id")
    protected MasterAgreement masterAgreement;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "other_expense_id")
    protected OtherExpense otherExpense;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "other_income_id")
    protected OtherIncome otherIncome;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="percentOwnedType", cascade=CascadeType.ALL, orphanRemoval = true)
    protected List<Owner> owner;

	@Enumerated(EnumType.STRING)
	@Column(length = 20)
    protected ProportionsList proportions;

	public PercentOwnedType() {
		// JPA
	}
	
    // Setters - Owner
	public void setOwner(List<Owner> owner) {
		if (owner != null) {
			this.owner = owner;
			this.owner.forEach(x -> x.setPercentOwnedType(this));
		}
	}

}
