package com.moneycatcha.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Content {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
	Message message;
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="content", cascade = CascadeType.ALL, orphanRemoval = true)
	protected Application application;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="content", cascade = CascadeType.ALL, orphanRemoval = true)
	protected NeedsAnalysis needsAnalysis;	

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="content", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<StatementOfPosition> statementOfPosition;	

	public Content() {
		// JPA
	}
	
	// Setter - Application
	public void setApplication(Application application) {
		if(application != null) {
			this.application = application;
			this.application.setContent(this);
		}
	}

	// Setter - NeedsAnalysis
	public void setNeedsAnalysis(NeedsAnalysis needsAnalysis) {
		if(needsAnalysis != null) {
			this.needsAnalysis = needsAnalysis;
			this.needsAnalysis.setContent(this);
		}
	}
	
	// Setter - StatementOfPosition
	public void setStatementOfPosition(List<StatementOfPosition> statementOfPosition) {
		if(statementOfPosition != null) {
			this.statementOfPosition = statementOfPosition;
			this.statementOfPosition.forEach(x ->x.setContent(this));
		}
	}
	
}
