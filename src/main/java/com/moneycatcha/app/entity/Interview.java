package com.moneycatcha.app.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Interview {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "guarantor_id")
    protected Guarantor guarantor;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;    	
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "interview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Attendee> attendee;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isFaceToFace;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isOnlyPersonPresent;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allApplicantsPresent;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allApplicantsUnderstandEnglish;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList interpreterRecommended;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xLocation;

    
    @Data
    @Entity
    @Table(name = "attendee")
    public static class Attendee {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "interview_id")
        protected Interview interview;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xParty;
        
        public Attendee() {
        	// JPA
        }

    }
    
    public Interview() {
    	// JPA
    }
   
	public void setAttendee(List<Attendee> attendee) {
		if (attendee != null) {
			this.attendee = attendee;
			this.attendee.forEach(x -> x.setInterview(this));
		}
	}

}