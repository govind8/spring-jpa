package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class LoanToValuationRatio {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "summary_id")
	protected Summary summary;    
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanToValuationRatio", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ContributingValuation> contributingValuation;

    @Column(precision=19, scale=2)
    protected BigDecimal applicationLVR;

    @Column(precision=19, scale=2)
    protected BigDecimal peakDebtLVR;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    
    @Data
    @Entity
    @Table(name = "contributing_valuation")
    public static class ContributingValuation {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "loan_to_valuation_ratio_id")
    	protected LoanToValuationRatio loanToValuationRatio;    
   	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xValuation;

        public ContributingValuation() { 
        	// JPA 
        }
	
    }

    public LoanToValuationRatio() {
    	// JPA 
    }
    
    // Setters - ContributingValuation
	public void setContributingValuation(List<ContributingValuation> contributingValuation) {
		if (contributingValuation != null) {
			this.contributingValuation = contributingValuation;
			this.contributingValuation.forEach(x -> x.setLoanToValuationRatio(this));
		}
	}
    
    
    
}
