package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
@Data
@Embeddable
public class PhoneType {
	
	@Column(columnDefinition = "VARCHAR(10)")
    protected String australianDialingCode;

	@Column(columnDefinition = "VARCHAR(10)")
    protected String countryCode;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String number;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String overseasDialingCode;

	public PhoneType() {
		// JPA
	}

}
