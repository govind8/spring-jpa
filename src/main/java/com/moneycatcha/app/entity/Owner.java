package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.entity.LoanDetails.Borrowers;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Owner {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="percent_owned_type_id")
    protected PercentOwnedType percentOwnedType;
   
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="percent_owned_id")
    protected Liability.PercentOwned percentOwned;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="borrowers_id")
    protected Borrowers borrowers;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ApplicantTypeList ownerType;
    
    @Column(precision=19, scale=2)
    protected BigDecimal percent;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList primaryBorrower;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xParty;

    public Owner() {
    	// JPA
    }
      

}