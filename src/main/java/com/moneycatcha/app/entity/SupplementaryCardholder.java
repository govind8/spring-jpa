package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class SupplementaryCardholder{
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    LoanDetails loanDetails;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "features_id")
    Features features;    
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSupplementaryCardholder;

}