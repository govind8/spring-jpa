package com.moneycatcha.app.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.RegistrationEventTypeList;

import lombok.Data;

@Data
@Entity
public class RegistrationEvent {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Registration registration;
	
    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime changeDateTime;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String changeNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String ppsrTransactionID;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected RegistrationEventTypeList type;

	public RegistrationEvent() {
		// JPA
	}
    
}