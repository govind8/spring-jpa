package com.moneycatcha.app.entity;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.moneycatcha.app.model.DurationUnitsList;

import lombok.Data;

@Data
@Embeddable
public class DurationType {
	
	@Column(columnDefinition = "BIGINT")
    protected BigInteger length;

	@Enumerated(EnumType.STRING)
	@Column(length = 10)
    protected DurationUnitsList units;


	public DurationType() {
		// JPA
	}

}
