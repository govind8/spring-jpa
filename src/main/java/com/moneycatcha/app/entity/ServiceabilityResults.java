package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.RepaymentFrequencyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ServiceabilityResults {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "summary_id")
	protected Summary summary;    
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "serviceabilityResults", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Applicant> applicant;
    
    @Embedded
    protected NetDisposableIncome netDisposableIncome;
    
    @Embedded
    protected TotalGrossIncome totalGrossIncome;
    
    @Embedded
    protected TotalNetIncome totalNetIncome;
    
    @Embedded
    protected TotalSystemCalculatedExpenses totalSystemCalculatedExpenses;
    
    @Embedded
    protected TotalSystemCalculatedLivingExpenses totalSystemCalculatedLivingExpenses;
    
    @Embedded
    protected TotalUserStatedLivingExpenses totalUserStatedLivingExpenses;

    @Column(precision=19, scale=2)
    protected BigDecimal dsr;

    @Column(precision=19, scale=2)
    protected BigDecimal dti;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String groupIdentifier;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String groupName;

    @Column(precision=19, scale=2)
    protected BigDecimal lti;

    @Column(precision=19, scale=2)
    protected BigDecimal maximumLoanAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal maximumRepaymentAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected RepaymentFrequencyList maximumRepaymentFrequency;

    @Column(precision=19, scale=2)
    protected BigDecimal mrim;

    @Column(precision=19, scale=2)
    protected BigDecimal nsr;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList serviceable;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;


    @Data
    @Entity
    @Table(name = "sr_applicant")
    public static class Applicant {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "serviceability_results_id")
    	protected ServiceabilityResults serviceabilityResults;    
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xParty;

    }


    @Data
    @Embeddable
    public static class NetDisposableIncome {
    	
        @Column(name = "ndi_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "ndi_frequency", length = 15)
        protected FrequencyShortList frequency;

    }


    @Data
    @Embeddable
    public static class TotalGrossIncome {
        @Column(name = "tgi_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "tgi_frequency", length = 15)
        protected FrequencyShortList frequency;

    }


    @Data
    @Embeddable
    public static class TotalNetIncome {
        @Column(name = "tni_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "tni_frequency", length = 15)
        protected FrequencyShortList frequency;

    }


    @Data
    @Embeddable
    public static class TotalSystemCalculatedExpenses {
        @Column(name = "tsce_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "tsce_frequency", length = 15)
        protected FrequencyShortList frequency;

    }


    @Data
    @Embeddable
    public static class TotalSystemCalculatedLivingExpenses {
        @Column(name = "tscle_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "tscle_frequency", length = 15)
        protected FrequencyShortList frequency;

    }


    @Data
    @Embeddable
    public static class TotalUserStatedLivingExpenses {
        @Column(name = "tusle_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Enumerated(EnumType.STRING)
        @Column(name = "tusle_frequency", length = 15)
        protected FrequencyShortList frequency;

    }

    // constructor
    public ServiceabilityResults() {
		// JPA
	}


	// Setter - Applicant
	public void setApplicant(List<Applicant> applicant) {
		if (applicant != null) {
			this.applicant = applicant;
			this.applicant.forEach(x -> x.setServiceabilityResults(this));
		}
	}
    
}