package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.SupplierTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class ContractDetails {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "non_real_estate_asset_id" )
    protected NonRealEstateAsset nonRealEstateAsset;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "real_estate_asset_id")
    protected RealEstateAsset realEstateAsset;
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected LocalDate contractDate;

    @Column(precision=19, scale=2)
    protected BigDecimal contractPriceAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal depositAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal depositPaid;

    @Column(columnDefinition = "DATE")
    protected LocalDate estimatedSettlementDate;

    @Column(precision=19, scale=2)
    protected BigDecimal gstAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList gstInclusive;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList gstOverwritten;

    @Column(precision=19, scale=2)
    protected BigDecimal inputTaxCredit;

    @Column(precision=19, scale=2)
    protected BigDecimal insurancePremium;

    @Column(precision=19, scale=2)
    protected BigDecimal luxuryCarTax;

    @Column(precision=19, scale=2)
    protected BigDecimal netTradeIn;

    @Column(precision=19, scale=2)
    protected BigDecimal nonClaimableGST;

    @Column(precision=19, scale=2)
    protected BigDecimal onRoadCosts;

    @Column(precision=19, scale=2)
    protected BigDecimal otherCost;

    @Column(precision=19, scale=2)
    protected BigDecimal registrationCost;

    @Column(precision=19, scale=2)
    protected BigDecimal stampDuty;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SupplierTypeList supplierType;

    @Column(precision=19, scale=2)
    protected BigDecimal totalCost;

    @Column(precision=19, scale=2)
    protected BigDecimal tradeInAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal tradeInExistingFinance;

    //OneToOne(mappedBy = "contractDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xVendor;

    public ContractDetails() {
    	// JPA
    }


   
	// Real Estate - arguments
	
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList armsLengthTransaction;


    @Column(precision=19, scale=2)
    protected BigDecimal depositPercentageRequested;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList earlyReleaseOfDepositAuthorityRequested;


    @Column(columnDefinition = "DATE")
    protected LocalDate financeApprovalDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList licencedRealEstateAgentContract;

    @Column(precision=19, scale=2)
    protected BigDecimal transferOfLandAmount;

}
