package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.OtherExpenseCategoryList;
import com.moneycatcha.app.model.OtherExpenseTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class OtherExpense {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "otherExpense", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Arrears arrears;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "otherExpense", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentOwned;
    
	@Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(precision=19, scale=2)
    protected BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OtherExpenseCategoryList category;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyShortList frequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasArrears;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OtherExpenseTypeList type;

	public OtherExpense() {
		// JPA
	}

	// Setters - Arrears
	public void setArrears(Arrears arrears) {
		if (arrears != null) {
			this.arrears = arrears;
			arrears.setOtherExpense(this);
		}
	}
	
	// Setters - PercentOwnedType
	public void setPercentOwned(PercentOwnedType percentOwned) {
		if (percentOwned != null) {
			this.percentOwned = percentOwned;
			this.percentOwned.setOtherExpense(this);
		}
	}


}