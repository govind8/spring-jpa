package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ApplicationDocTypeList;
import com.moneycatcha.app.model.ApplicationTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Overview {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Application application;
	
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BranchDomicile branchDomicile;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BranchSign branchSign;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BranchStamp branchStamp;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BridgingFinance bridgingFinance;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ExistingCustomer> existingCustomer;
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "overview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SignatureType> signature;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ApplicationTypeList applicationType;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String brokerApplicationReferenceNumber;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger brokerApplicationSequenceNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList combinationLoan;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ApplicationDocTypeList docType;

    @Column(columnDefinition = "DATE")
    protected LocalDate expectedSettlementDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList fastRefinance;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasSpecialCircumstances;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isBridgingFinance;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lenderApplicationReferenceNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lenderPreapprovalReferenceNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList linkedCommercialApplication;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lodgementReferenceNumber;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger lodgementSequenceNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList privateBanking;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList proPack;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList smsfLoan;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList urgent;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xMainContactPoint;

  
    public Overview() {
    	// JPA
	}


	@Data
    @Entity
    @Table(name = "branch_domicile")
    public static class BranchDomicile {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	    
        @OneToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "overview_id")
        protected Overview overview;
        
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "branchDomicile", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Contact contact;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String bsb;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String internalName;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String internalNumber;

        // Setters - Contact
        public void setContact(Contact contact) {
        	if (contact != null) {
	            this.contact = contact;    	
	            this.contact.setBranchDomicile(this);
        	}
        }                

    }

    @Data
    @Entity
    @Table(name = "branch_sign")
    public static class BranchSign {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	    
        @OneToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "overview_id")
        protected Overview overview;
            	
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "branchSign", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Contact contact;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String bsb;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String internalName;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String internalNumber;

        public BranchSign() {
        	// JPA
        }
        
        // Setters - Contact
        public void setContact(Contact contact) {
        	if (contact != null) {
	            this.contact = contact;    	
	            this.contact.setBranchSign(this);
        	}
        }        

    }

    @Data
    @Entity
    @Table(name = "branch_stamp")
    public static class BranchStamp {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	    
        @OneToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "overview_id")
        protected Overview overview;
           	
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList stamped;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList stampRequired;
    }


    @Data
    @Entity
    @Table(name = "bridging_finance")
    public static class BridgingFinance {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	    
        @OneToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "overview_id")
        protected Overview overview;
        
        @Column(columnDefinition = "BIGINT")
        protected BigInteger bridgingTerm;

        @Column(precision=19, scale=2)
        protected BigDecimal capitalisedInterestAmount;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList capitaliseInterest;

        @Column(precision=19, scale=2)
        protected BigDecimal endDebt;

        @Column(precision=19, scale=2)
        protected BigDecimal estimatedSellingCosts;

        @Column(precision=19, scale=2)
        protected BigDecimal peakDebt;
    }
    
    // Setters - BranchDomicile
    public void setBranchDomicile(BranchDomicile branchDomicile) {
    	if (branchDomicile != null) {
	        this.branchDomicile = branchDomicile;    	
	        this.branchDomicile.setOverview(this);        
    	}
    }
    
    // Setters - BranchSign
    public void setBranchSign(BranchSign branchSign) {
    	if (branchSign != null) {
	        this.branchSign = branchSign;    	
	        this.branchSign.setOverview(this);
    	}
    }
     
    // Setters - BranchStamp
    public void setBranchStamp(BranchStamp branchStamp) {
    	if (branchStamp != null) {
	        this.branchStamp = branchStamp;    	
	        this.branchStamp.setOverview(this);
    	}
    }    
    
    // Setters - BridgingFinance
    public void setBridgingFinance(BridgingFinance bridgingFinance) {
    	if (bridgingFinance != null) {
	        this.bridgingFinance = bridgingFinance;    	
	        this.bridgingFinance.setOverview(this);
    	}
     }    
    
    // Setters - ExistingCustomer
    public void setExistingCustomer(List<ExistingCustomer> existingCustomer) {
    	if (existingCustomer != null) {
	        this.existingCustomer = existingCustomer;    	
	        this.existingCustomer.forEach(x -> x.setOverview(this));
    	}
    }
    
    // Setters - SignatureType
    public void setSignature(List<SignatureType> signature) {
    	if (signature != null) {
	        this.signature = signature;    	
	        this.signature.forEach(x -> x.setOverview(this));
    	}
    }
}