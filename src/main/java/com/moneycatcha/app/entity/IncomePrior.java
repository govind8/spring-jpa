package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class IncomePrior {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    

    @Column(precision=19, scale=2)
    protected BigDecimal companyProfitAfterTax;

    @Column(precision=19, scale=2)
    protected BigDecimal companyProfitBeforeTax;

    @Column(precision=19, scale=2)
    protected BigDecimal profitAfterTax;

    @Column(precision=19, scale=2)
    protected BigDecimal profitBeforeTax;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList incomeGreaterThanPreviousYear;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList taxOfficeAssessments;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccountant;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "incomePrior", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Addback addback;

    // Setters - Addback
    public void setAddback(Addback addback) {
    	if (addback != null) {
	        this.addback = addback; 
	    	this.addback.setIncomePrior(this);
    	}
    }    	
    

    public IncomePrior() {
    	
    	//JPA
	}

}
