
package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CapacityTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class SignatureType {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "overview_id")
    protected Overview overview;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "summary_id")
    protected Summary summary;
    
    @Embedded
    protected Capacity capacity;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "signatureType", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

	@Column(columnDefinition = "DATE")
    protected LocalDate date;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList electronicSignature;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger rank;

	@Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xSignatory;

	public SignatureType() {
		
	}

	@Data
    @Embeddable
    public static class Capacity {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "capacity_type", length = 60)
        protected CapacityTypeList type;
        
    	@Column(name = "capacity_x_parent_entity", columnDefinition = "VARCHAR(80)")
        protected String xParentEntity;

    	@Column(name = "capacity_x_product_set", columnDefinition = "VARCHAR(80)")
        protected String xProductSet;
    	
    	public Capacity() {
    		// JPA
    	}

    }

	// Setter - Contact
	
	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setSignatureType(this);
		}
	}

     
}
