package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Application {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "content_id")
	Content content;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<AccountVariation> accountVariation; 

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<AddressType> address;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected BusinessChannel businessChannel; 

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<CompanyApplicant> companyApplicant; 
 
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<CompanyFinancials> companyFinancials;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<ContributionFunds> contributionFunds;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<CustomerTransactionAnalysis> customerTransactionAnalysis;
  
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected Declarations declarations;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<DepositAccountDetails> depositAccountDetails;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<DetailedComment> detailedComment;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Household> household;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Insurance> insurance;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<LendingGuarantee> lendingGuarantee;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Liability> liability;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<LoanDetails> loanDetails;
  
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<MasterAgreement> masterAgreement;
  
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<NonRealEstateAsset> nonRealEstateAsset;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<OtherExpense> otherExpense;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<OtherIncome> otherIncome;
  
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected Overview overview;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<PersonApplicant> personApplicant;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<ProductPackage> productPackage;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<ProductSet> productSet;
  
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<RealEstateAsset> realEstateAsset;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<RelatedCompany> relatedCompany;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<RelatedPerson> relatedPerson;
  
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected SalesChannel salesChannel;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Settlement> settlement;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<SplitLoan> splitLoan;
  
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected Summary summary;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<TrustApplicant> trustApplicant;
  
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<VendorTaxInvoiceType> vendorTaxInvoice;
	 
	
	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
	String uniqueID;
	
   	@Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList productionData;

    public Application() {
    	//JPA
    }
    
    // Setters - AccountVariation
    public void setAccountVariation(List<AccountVariation> accountVariation) {
    	if(accountVariation != null) {
	    	this.accountVariation = accountVariation;    	
	    	this.accountVariation.forEach(x -> x.setApplication(this));
    	}
    }
    
    // Setters - Address
    public void setAddress(List<AddressType> address) {
    	if(address != null) {
	        this.address = address;    	
	    	this.address.forEach(x -> x.setApplication(this));
    	}
	}  
    
    // Setters - BusinessChannel
    public void setBusinessChannel(BusinessChannel businessChannel) {
    	if(businessChannel != null) {
	        this.businessChannel = businessChannel;    	
	    	this.businessChannel.setApplication(this);
    	}
    }    
    
    // Setters - CompanyApplicant
    public void setCompanyApplicant(List<CompanyApplicant> companyApplicant) {
    	if(companyApplicant != null) {
	        this.companyApplicant = companyApplicant;    	
	    	this.companyApplicant.forEach(x -> x.setApplication(this));    
    	}
	}  
	
	
	  // Setters - CompanyFinancials 
	  public void setCompanyFinancials(List<CompanyFinancials> companyFinancials) {
		  if(companyFinancials != null) {
			  this.companyFinancials = companyFinancials; 
			  this.companyFinancials.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - ContributionFunds 
	  public void setContributionFunds(List<ContributionFunds> contributionFunds) {
		  if(contributionFunds != null) {
			  this.contributionFunds = contributionFunds; 
			  this.contributionFunds.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - CustomerTransactionAnalysis 
	  public void setCustomerTransactionAnalysis(List<CustomerTransactionAnalysis> customerTransactionAnalysis) { 
		  if(customerTransactionAnalysis != null) {
			  this.customerTransactionAnalysis = customerTransactionAnalysis; 
			  this.customerTransactionAnalysis.forEach(x ->  x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - Declarations 
	  public void setDeclarations(Declarations declarations) { 
		  if(declarations != null) {
			  this.declarations = declarations; 
			  this.declarations.setApplication(this); 
		  }
	  }
	  
	  // Setters - DepositAccountDetails 
	  public void setDepositAccountDetails(List<DepositAccountDetails> depositAccountDetails) {
		  if(depositAccountDetails != null) {
			  this.depositAccountDetails = depositAccountDetails; 
			  this.depositAccountDetails.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - DetailedComment 
	  public void setDetailedComment(List<DetailedComment> detailedComment) {
		  if(detailedComment != null) {
			  this.detailedComment = detailedComment; 
			  this.detailedComment.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - CompanyApplicant 
	  public void setHousehold(List<Household> household) { 
		  if(household != null) {
			  this.household =  household; 
			  this.household.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - Insurance 
	  public void setInsurance(List<Insurance> insurance) {
		  if(insurance != null) {
			  this.insurance = insurance; 
			  this.insurance.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - LendingGuarantee 
	  public void setLendingGuarantee(List<LendingGuarantee> lendingGuarantee) {
		  if(lendingGuarantee != null) {
			  this.lendingGuarantee = lendingGuarantee; 
			  this.lendingGuarantee.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - Liability 
	  public void setLiability(List<Liability> liability) {
		  if(liability != null) {
			  this.liability = liability; 
		  	  this.liability.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - LoanDetails 
	  public void setLoanDetails(List<LoanDetails> loanDetails) { 
		  if(loanDetails != null) {
			  this.loanDetails = loanDetails; 
			  this.loanDetails.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - MasterAgreement 
	  public void setMasterAgreement(List<MasterAgreement> masterAgreement) {
		  if(masterAgreement != null) {
			  this.masterAgreement = masterAgreement; 
			  this.masterAgreement.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - NonRealEstateAsset 
	  public void setNonRealEstateAsset(List<NonRealEstateAsset> nonRealEstateAsset) {
		  if(nonRealEstateAsset != null) {
			  this.nonRealEstateAsset = nonRealEstateAsset; 
			  this.nonRealEstateAsset.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - OtherExpense 
	  public void setOtherExpense(List<OtherExpense> otherExpense) { 
		  if(otherExpense != null) {
			  this.otherExpense = otherExpense; 
			  this.otherExpense.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - OtherIncome 
	  public void setOtherIncome(List<OtherIncome> otherIncome) { 
		  if(otherIncome != null) {
    		  this.otherIncome = otherIncome; 
    		  this.otherIncome.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - Overview 
	  public void setOverview(Overview overview) {
		  if(overview != null) {
			  this.overview = overview; 
	    	  this.overview.setApplication(this); 
		  }
	  }
	  
	  // Setters - PersonApplicant 
	  public void setPersonApplicant(List<PersonApplicant> personApplicant) {
		  if(personApplicant != null) {
			  this.personApplicant =  personApplicant; 
			  this.personApplicant.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - ProductPackage 
	  public void setProductPackage(List<ProductPackage> productPackage) {
		  if(productPackage != null) {
			  this.productPackage =  productPackage; 
			  this.productPackage.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - ProductSet 
	  public void setProductSet(List<ProductSet> productSet) { 
		  if(productSet != null) {
			  this.productSet = productSet; 
			  this.productSet.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - RealEstateAsset 
	  public void  setRealEstateAsset(List<RealEstateAsset> realEstateAsset) {
		  if(realEstateAsset != null) {
			  this.realEstateAsset = realEstateAsset; 
			  this.realEstateAsset.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - RelatedCompany 
	  public void setRelatedCompany(List<RelatedCompany> relatedCompany) {
		  if(relatedCompany != null) {
			  this.relatedCompany = relatedCompany; 
			  this.relatedCompany.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - RelatedPerson 
	  public void setRelatedPerson(List<RelatedPerson> relatedPerson) { 
		  if(relatedPerson != null) {
	    		this.relatedPerson = relatedPerson; 
	    		this.relatedPerson.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - SalesChannel 
	  public void setSalesChannel(SalesChannel salesChannel) { 
		  if(salesChannel != null) {
			  this.salesChannel = salesChannel; 
			  this.salesChannel.setApplication(this); 
		  }
	  }
	  
	  // Setters - Settlement 
	  public void setSettlement(List<Settlement> settlement) { 
		  if(settlement != null) {
			  this.settlement = settlement; 
			  this.settlement.forEach(x -> x.setApplication(this));
		  }
	  }
	  
	  // Setters - SplitLoan 
	  public void setSplitLoan(List<SplitLoan> splitLoan) {
		  if(splitLoan != null) {
	    		this.splitLoan = splitLoan; 
	    		this.splitLoan.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - Summary 
	  public void setSummary(Summary summary) {
		  if(summary != null) {
			  this.summary = summary; 
			  this.summary.setApplication(this); 
		  }
	  }
	  
	  // Setters - TrustApplicant 
	  public void setTrustApplicant(List<TrustApplicant> trustApplicant) {
		  if(trustApplicant != null) {
			  this.trustApplicant =  trustApplicant; 
			  this.trustApplicant.forEach(x -> x.setApplication(this)); 
		  }
	  }
	  
	  // Setters - VendorTaxInvoiceType 
	  public void setVendorTaxInvoice(List<VendorTaxInvoiceType> vendorTaxInvoice) {
		  if(vendorTaxInvoice != null) {
			  this.vendorTaxInvoice =  vendorTaxInvoice; 
			  this.vendorTaxInvoice.forEach(x -> x.setApplication(this)); 
		  }
	  }

	@java.lang.Override
	public java.lang.String toString() {
		return "Application{" +
				"Id=" + Id +
				", uniqueID='" + uniqueID + '\'' +
				", productionData=" + productionData +
				'}';
	}
}