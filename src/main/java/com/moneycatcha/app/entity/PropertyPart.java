package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.PropertyPartTypeList;

import lombok.Data;

@Data
@Entity
public class PropertyPart {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "future_rental_income_id" )
    protected FutureRentalIncome futureRentalIncome;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "rental_income_id" )
    protected RentalIncome rentalIncome;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PropertyPartTypeList type;

}
