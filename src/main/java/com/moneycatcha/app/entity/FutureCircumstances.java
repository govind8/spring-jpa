package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class FutureCircumstances {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
  
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;    	
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "futureCircumstances", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Applicant> applicant;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "futureCircumstances", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ResponsibleLendingType responsibleLending;
	
	public FutureCircumstances() {
		//JPA
	}

	// Setter - ResponsibleLendingType
	public void setResponsibleLending(ResponsibleLendingType responsibleLending) {
		if(responsibleLending != null) {
			this.responsibleLending = responsibleLending;
			this.responsibleLending.setFutureCircumstances(this);
		}
	}
	
	// Setter - Applicant
	public void setApplicant(List<Applicant> applicant) {
		if (applicant != null) {
			this.applicant = applicant;
			this.applicant.forEach(x -> x.setFutureCircumstances(this));
		}
	}
	
	

}
