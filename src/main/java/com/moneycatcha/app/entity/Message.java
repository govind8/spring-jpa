package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Message {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

  	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Attachment> attachment;

  	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private Content content;

  	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private Instructions instructions;

  	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private Publisher publisher;

  	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<Recipient> recipient;

  	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private SchemaVersion schemaVersion;

  	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "message", cascade = CascadeType.ALL, orphanRemoval = true)
	private TransformMetadata transformMetadata;

  	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList productionData;
   
	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
	String uniqueID;
	

  	public Message() {
    	// JPA 
    }
 	
    
    // Setters - List<Attachment>
    public void setAttachment(List<Attachment> attachment) {
    	if (attachment != null) {
	        this.attachment = attachment;    	
	    	this.attachment.forEach(x -> x.setMessage(this));
    	}
    }	
    
    // Setters - Content
    public void setContent(Content content) {
    	if (content != null) {
	        this.content = content;    	
	    	this.content.setMessage(this);
    	}
    }	
  
    // Setters - Content
	public void setInstructions(Instructions instructions) {
    	if (instructions != null) {
			this.instructions = instructions;
	    	this.instructions.setMessage(this);
    	}
	}

    // Setters - Publisher
	public void setPublisher(Publisher publisher) {
    	if (publisher != null) {
			this.publisher = publisher;
	    	this.publisher.setMessage(this);
    	}
	}

    // Setters - List<Recipient>
	public void setRecipient(List<Recipient> recipient) {
    	if (recipient != null) {
			this.recipient = recipient;
	    	this.recipient.forEach(x -> x.setMessage(this));
    	}
	}

    // Setters - SchemaVersion
	public void setSchemaVersion(SchemaVersion schemaVersion) {
    	if (schemaVersion != null) {
			this.schemaVersion = schemaVersion;
	    	this.schemaVersion.setMessage(this);
    	}
	}

    // Setters - TransformMetadata
	public void setTransformMetadata(TransformMetadata transformMetadata) {
    	if (transformMetadata != null) {
			this.transformMetadata = transformMetadata;
	    	this.transformMetadata.setMessage(this);
    	}
	}
    
//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//
//        if (o == null || getClass() != o.getClass()) return false;
//
//        Message message = (Message) o;
//
//        return new EqualsBuilder()
//                .append(uniqueID, message.uniqueID)
//                .append(content, message.content)
//                .isEquals();
//    }
//    
//    @Override
//    public int hashCode() {
//        return 31;
//    }
//
//	@Override
//	public String toString() {
//		return "Message {messageId=" + getId() + ", uniqueID=" + uniqueID + ", productionData=" + productionData
//				+ "}";
//	}


 }
