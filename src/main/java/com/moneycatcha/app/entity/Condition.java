package com.moneycatcha.app.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.entity.Update.Status;
import com.moneycatcha.app.model.ConditionOwnerApplicationInstructionsList;
import com.moneycatcha.app.model.ConditionResponseStatusList;
import com.moneycatcha.app.model.ConditionStatusApplicationInstructionsList;
import com.moneycatcha.app.model.PreConditionToStageApplicationInstructionsList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Condition {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "submit_id")
	Submit submit;

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "status_id")
	Status status;
    
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "condition", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Product> product;
	
    @Column(columnDefinition = "VARCHAR(255)")
    protected String conditionResponseDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionResponseStatusList conditionResponseStatus;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String loanConditionText;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionOwnerApplicationInstructionsList conditionOwner;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionStatusApplicationInstructionsList conditionStatus;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String conditionType;

    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime createdDateTime;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList docRequirement;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PreConditionToStageApplicationInstructionsList preconditionToStage;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "DATE")
    protected LocalDateTime updatedDateTime;

	@Column(columnDefinition = "VARCHAR(80)")
	protected String xSupportingDocument;

    @Data
    @Entity
    @Table(name = "condition_product")
    public static class Product {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "condition_id")
    	Condition condition;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xFinancialProduct;

        public Product() {
        	// JPA
        }
    }

    public Condition() {
    	// JPA 
    }

	// Setters - Product
	public void setProduct(List<Product> product) {
		if(product != null) {
			this.product = product;
			this.product.forEach(x -> x.setCondition(this));
		}
	}
     
    
}
