package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.BuilderTypeList;
import com.moneycatcha.app.model.ConstructionTypeList;
import com.moneycatcha.app.model.LandValueEstimateBasisList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ConstructionDetails {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "real_estate_asset_id")
    protected RealEstateAsset realEstateAsset;
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "constructionDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ConstructionStage> constructionStage;
	
	@Embedded
    protected OwnerBuilderExperience ownerBuilderExperience;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected BuilderTypeList builderType;

    @Column(precision=19, scale=2)
    protected BigDecimal buildPriceAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList constructionInsuranceHeld;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList copyBuildersLicenceHeld;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList copyHomeOwnersWarrantyCertificateHeld;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList councilApprovalHeld;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList detailedCostingsHeld;

    @Column(columnDefinition = "DATE")
    protected LocalDate estimatedCompletionDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList fixedPriceContract;

    @Column(precision=19, scale=2)
    protected BigDecimal initialAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal landValue;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LandValueEstimateBasisList landValueBasis;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList satisfactoryTOCValuationHeld;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList signedContract;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList tenPercentOfLendingAsCash;

    @Column(precision=19, scale=2)
    protected BigDecimal totalAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConstructionTypeList type;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xBuilder;

    @Data
    @Entity
    @Table(name = "construction_stage")
    public static class ConstructionStage {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "construction_details_id")
        protected ConstructionDetails constructionDetails;
    	
        @Column(precision=19, scale=2)
        protected BigDecimal stageAmount;

        @Column(columnDefinition = "VARCHAR(500)")
        protected String stageDescription;

        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

		public ConstructionStage() {
			// JPA
		}
 
    }

    @Data
    @Embeddable
    public static class OwnerBuilderExperience {

        @Enumerated(EnumType.STRING)
        @Column(name = "obe_completedSimilarProjectInLast2Years", length = 3)
        protected YesNoList completedSimilarProjectInLast2Years;

        @Enumerated(EnumType.STRING)
        @Column(name = "obe_minimalExperience", length = 3)
        protected YesNoList minimalExperience;

        @Column(name = "obe_otherExperience", columnDefinition = "VARCHAR(500)")
        protected String otherExperience;

        @Enumerated(EnumType.STRING)
        @Column(name = "obe_worksInBuildingIndustry", length = 3)
        protected YesNoList worksInBuildingIndustry;

		public OwnerBuilderExperience() {
			// JPA
		}

    }

    public ConstructionDetails() {
    	// JPA
    }


	// setters ConstructionStage
	public void setConstructionStage(List<ConstructionStage> constructionStage) {
		if (constructionStage != null) {
			this.constructionStage = constructionStage;
			this.constructionStage.forEach(x -> x.setConstructionDetails(this));
		}
	}
 	
    
}
