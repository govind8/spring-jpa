package com.moneycatcha.app.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.FundsAccessTypeList;
import com.moneycatcha.app.model.InterestRateTypeList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Preferences {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;    	
	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "preferences", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FundsAccessTypeDetails fundsAccessTypeDetails;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "preferences", cascade = CascadeType.ALL, orphanRemoval = true)
    protected InterestRateTypeDetails interestRateTypeDetails;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "preferences", cascade = CascadeType.ALL, orphanRemoval = true)
    protected RepaymentTypeDetails repaymentTypeDetails;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String conflictDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList conflictExists;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FundsAccessTypeList fundsAccessType;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected InterestRateTypeList interestRateType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherPreferences;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList preferredLenders;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String preferredLendersDetails;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String prioritiesAndReasons;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaymentTypeList repaymentType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String summary;

       
	// constructor
    public Preferences() {
		// JPA
	}


	// Setter - FundsAccessTypeDetails
	public void setFundsAccessTypeDetails(FundsAccessTypeDetails fundsAccessTypeDetails) {
		if (fundsAccessTypeDetails != null) {
			this.fundsAccessTypeDetails = fundsAccessTypeDetails;
			this.fundsAccessTypeDetails.setPreferences(this);
		}
	}

	// Setter - InterestRateTypeDetails
	public void setInterestRateTypeDetails(InterestRateTypeDetails interestRateTypeDetails) {
		if (interestRateTypeDetails != null) {
			this.interestRateTypeDetails = interestRateTypeDetails;
			this.interestRateTypeDetails.setPreferences(this);
		}
	}

	// Setter - RepaymentTypeDetails
	public void setRepaymentTypeDetails(RepaymentTypeDetails repaymentTypeDetails) {
		if (repaymentTypeDetails != null) {
			this.repaymentTypeDetails = repaymentTypeDetails;
			this.repaymentTypeDetails.setPreferences(this);
		}
	}
    
	
	
    
}
