package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class StatementOfPosition {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "content_id")
	protected Content content;    	
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "statementOfPosition", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Applicant> applicants;

    @Column(columnDefinition = "DATE")
    protected LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isSigned;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Data
    @Entity
    @Table(name = "sop_applicant")
    public static class Applicant {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "statement_of_position_id")
    	protected StatementOfPosition statementOfPosition;    	
   	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xApplicant;
        
        public Applicant() {
        	// JPA
        }

    }
    
    // Constructor
    public StatementOfPosition() {
    	// JPA
    }
    
	
	// Setter - Applicants
	public void setApplicants(List<Applicant> applicants) {
		if (applicants != null) {
			this.applicants = applicants;
			this.applicants.forEach(x -> x.setStatementOfPosition(this));
		}
	}
    
}


