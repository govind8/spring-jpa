package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.AbsLendingPurposeCodeList;
import com.moneycatcha.app.model.PrincipalRefinancingReasonList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class LendingPurpose {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Liability liability;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "lendingPurpose", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentBenefit;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String absLendingPurpose;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AbsLendingPurposeCodeList absLendingPurposeCode;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList includesRefinancing;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lenderCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList payoutQuoteObtained;

    @Column(precision=19, scale=2)
    protected BigDecimal purposeAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PrincipalRefinancingReasonList refinancingReason;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList securityForMarginLoan;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    // Setters - PercentOwnedType
    public void setDetail(PercentOwnedType percentBenefit) {
    	if (percentBenefit != null) {
	        this.percentBenefit = percentBenefit;    	
	    	this.percentBenefit.setLendingPurpose(this);
    	}
    }    
    
    public LendingPurpose() {
    	// JPA
    }
    

}