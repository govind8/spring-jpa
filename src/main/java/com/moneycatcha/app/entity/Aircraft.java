package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.AircraftTypeList;
import com.moneycatcha.app.model.ConditionList;

import lombok.Data;

@Data
@Entity
public class Aircraft {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    protected NonRealEstateAsset nonRealEstateAsset;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String additionalIDType;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String additionalIDValue;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger age;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger airFrameHours;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionList condition;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String conditionDescription;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(precision=19, scale=2)
    protected BigDecimal effectiveLife;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger engineHoursTotal;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger engineHoursTTR;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String engineID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String make;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String model;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherInformation;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger quantity;

    @Column(columnDefinition = "DATE")
    protected LocalDate registrationExpiryDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String registrationNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String serialNumber;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String servicingHistory;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AircraftTypeList type;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xGoodToBeUsedAddress;

    @Column(columnDefinition = "INT")
    protected int year;

    public Aircraft() {
    	
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Aircraft{" +
                "Id=" + Id +
                ", nonRealEstateAsset=" + nonRealEstateAsset +
                ", additionalIDType='" + additionalIDType + '\'' +
                ", additionalIDValue='" + additionalIDValue + '\'' +
                ", age=" + age +
                ", airFrameHours=" + airFrameHours +
                ", condition=" + condition +
                ", conditionDescription='" + conditionDescription + '\'' +
                ", description='" + description + '\'' +
                ", effectiveLife=" + effectiveLife +
                ", engineHoursTotal=" + engineHoursTotal +
                ", engineHoursTTR=" + engineHoursTTR +
                ", engineID='" + engineID + '\'' +
                ", make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", otherInformation='" + otherInformation + '\'' +
                ", quantity=" + quantity +
                ", registrationExpiryDate=" + registrationExpiryDate +
                ", registrationNumber='" + registrationNumber + '\'' +
                ", serialNumber='" + serialNumber + '\'' +
                ", servicingHistory='" + servicingHistory + '\'' +
                ", type=" + type +
                ", xGoodToBeUsedAddress='" + xGoodToBeUsedAddress + '\'' +
                ", year=" + year +
                '}';
    }
}