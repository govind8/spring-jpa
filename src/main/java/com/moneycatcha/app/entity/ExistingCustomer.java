package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ExistingCustomer {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "overview_id")
	Overview overview;    
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String customerNumber;

    @Column(columnDefinition = "INT")
    protected int customerSince;

    @Column(columnDefinition = "VARCHAR(15)")
    protected String customerSinceMonth;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="existingCustomer", cascade=CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;

    public ExistingCustomer() {
    	// JPA
    }

    // Setter - AccountNumber
	public void setAccountNumber(FinancialAccountType accountNumber) {
		if (accountNumber != null) {
			this.accountNumber = accountNumber;
			this.accountNumber.setExistingCustomer(this);
		}
	}

    
}
