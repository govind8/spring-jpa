package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.MitigantFactorList;
import com.moneycatcha.app.model.SignificantChangeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class ResponsibleLendingType {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "future_circumstances_id")
	FutureCircumstances futureCircumstances;
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "responsibleLendingType", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Mitigant> mitigant;

	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "responsibleLendingType", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SignificantChange> significantChange;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList anticipatedChanges;

    public ResponsibleLendingType() {
    	// JPA
    }
	
    // Setters - Mitigant
	public void setMitigant(List<Mitigant> mitigant) {
		this.mitigant = mitigant;
	 	this.mitigant.forEach(x -> x.setResponsibleLendingType(this));
	}
	
	// Setters - Significant Change
	public void setSignificantChange(List<SignificantChange> significantChange) {
	 	this.significantChange = significantChange;
	 	this.significantChange.forEach(x -> x.setResponsibleLendingType(this));
	}
    
    @Data
	@Entity
	@Table(name = "mitigant")
    public static class Mitigant {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "responsible_lending_type_id")
        protected ResponsibleLendingType responsibleLendingType;  
        
        @Column(columnDefinition = "VARCHAR(255)")
        protected String description;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected MitigantFactorList factor;

    	@OneToOne(mappedBy = "mitigant", cascade = CascadeType.ALL, orphanRemoval = true)
        protected SaleOfAssets saleOfAssets;
    	
    	@Embedded
        protected SavingsOrSuperannuation savingsOrSuperannuation;
        
        @Column(columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        public Mitigant() {
        	// JPA
        }

        @Data
        @Embeddable
        public static class SavingsOrSuperannuation {
        	
            @Enumerated(EnumType.STRING)
            @Column(name = "sos_lenderHeldSavingsAccount", length = 3)
            protected YesNoList lenderHeldSavingsAccount;

            @Column(name = "sos_lenderHeldSavingsAccountDescription", columnDefinition = "VARCHAR(255)")
            protected String lenderHeldSavingsAccountDescription;

            @Enumerated(EnumType.STRING)
            @Column(name = "sos_otherLenderHeldSavingsAccount", length = 3)
            protected YesNoList otherLenderHeldSavingsAccount;

        	@Column(name = "sos_otherLenderHeldSavingsAccountDescription", columnDefinition = "VARCHAR(255)")
            protected String otherLenderHeldSavingsAccountDescription;

        }

    	// Setter - SaleOfAssets
		public void setSaleOfAssets(SaleOfAssets saleOfAssets) {
			this.saleOfAssets = saleOfAssets;
			this.saleOfAssets.setMitigant(this);
		}
     
    }

	@Data
	@Entity
	@Table(name = "significant_change")
    public static class SignificantChange {
        
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "responsible_lending_type_id")
        protected ResponsibleLendingType responsibleLendingType;  
		
		
        @Enumerated(EnumType.STRING)
        @Column(name = "sc_change", length = 60)
        protected SignificantChangeList change;

    	@Column(name = "sc_description", columnDefinition = "VARCHAR(255)")
        protected String description;

    	@Column(name = "sc_endDate", columnDefinition = "DATE")
        protected LocalDate endDate;

    	@Column(name = "sc_monthlyFinancialImpact", precision=19, scale=2)
        protected BigDecimal monthlyFinancialImpact;

    	@Column(name = "sc_startDate", columnDefinition = "DATE")
        protected LocalDate startDate;

     	@Column(columnDefinition = "VARCHAR(80)")
        protected String uniqueID;
    	
     	public SignificantChange() {
    		// JPA
    	}

    }


}
