package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.CreditStatusList;
import com.moneycatcha.app.model.DocumentationInstructionsMethodList;
import com.moneycatcha.app.model.GenderList;
import com.moneycatcha.app.model.KinRelationshipList;
import com.moneycatcha.app.model.MaritalStatusList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.ResidencyStatusList;
import com.moneycatcha.app.model.SendDocumentsToPersonList;
import com.moneycatcha.app.model.SourceOfFundsTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class PersonApplicant {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<CreditHistory> creditHistory;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DocumentationInstructions documentationInstructions;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Employment> employment;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ExistingCustomer existingCustomer;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<FinancialSituationCheck> financialSituationCheck;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ForeignTaxAssociationType foreignTaxAssociation;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<IdentityCheck> identityCheck;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Insurance> insurance;

    @Embedded
    protected MaritalStatusDetails maritalStatusDetails;

    @Embedded
    protected NextOfKin nextOfKin;

    @Embedded
    protected NominatedBorrower nominatedBorrower;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PersonNameType personName;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PowerOfAttorney powerOfAttorney;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PreviousName previousName;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Privacy privacy;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<ProofOfIdentity> proofOfIdentity;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ResponsibleLendingType responsibleLending;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SourceOfFunds> sourceOfFunds;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SourceOfWealth> sourceOfWealth;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "personApplicant", cascade = CascadeType.ALL, orphanRemoval = true)
    protected TaxDeclarationDetailsType taxDeclarationDetails;

    @Embedded
    protected PersonApplicant.Will will;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ApplicantTypeList applicantType;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList citizenship;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList companyDirector;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList countryOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CreditStatusList creditStatus;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String customerTypeCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String customerTypeDescription;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList discussedWithBeneficiaries;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList eligibleForFHOG;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList firstHomeBuyer;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList firstPropertyBuyer;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected GenderList gender;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasAppliedForAustralianCitizenship;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasAppliedForPermanentResidencyVisa;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasAWill;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList hasPreviousName;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList immigrant;

    @Column(columnDefinition = "DATE")
    protected LocalDate immigrationDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList independentFinancialAdvice;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList independentLegalAdvice;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isExistingCustomer;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isLenderStaff;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList jointNomination;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList jointStatementOfPosition;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList localAgentOfForeignCompany;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected MaritalStatusList maritalStatus;

    @Column(precision=19, scale=2)
    protected BigDecimal monthsInCurrentProfession;

    @Column(columnDefinition = "VARCHAR(60)")
    protected String mothersMaidenName;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList onBehalfOfUnincorporatedAssociation;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate permanentResidencyDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String placeOfBirth;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList poaGranted;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList primaryApplicant;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList principalForeignResidence;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected KinRelationshipList relationshipToPrimaryApplicant;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ResidencyStatusList residencyStatus;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate temporaryVisaExpiryDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList underDuress;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList understandApplication;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String visaSubclassCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccountant;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xHousehold;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPersonalReference;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSolicitor;

    @Column(precision=19, scale=2)
    protected BigDecimal yearsInCurrentProfession;
    
    
    @Data
    @Entity
    @Table(name = "person_applicant_insurance")
    public static class Insurance {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "person_applicant_id")
    	PersonApplicant personApplicant;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xInsurance;
        
        public Insurance() {
        	// JPA
        }
    }


    @Data
    @Embeddable
    public static class MaritalStatusDetails {
   
        @Column(columnDefinition = "DATE")
        protected LocalDate maritalStatusChangeDate;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xSpouse;

    }


    @Data
    @Embeddable
    public static class NextOfKin {

    	@Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected KinRelationshipList kinRelationship;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xPerson;

    }


    @Data
    @Embeddable
    public static class NominatedBorrower {
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xNominee;

    }

    @Data
    @Entity
    @Table(name = "documentation_instructions")
    public static class DocumentationInstructions {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "person_applicant_id")
    	PersonApplicant personApplicant;
    	
        @Enumerated(EnumType.STRING)
    	@Column(length = 60)
        protected DocumentationInstructionsMethodList method;

    	@Enumerated(EnumType.STRING)
    	@Column(length = 60)
        protected SendDocumentsToPersonList sendDocumentsTo;

    	@Column(columnDefinition = "VARCHAR(80)")
        protected String xNominatedAuthority;

    }
    
    @Data
    @Entity
    @Table(name = "previous_name")
    public static class PreviousName {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@OneToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "person_applicant_id")
    	PersonApplicant personApplicant;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String firstName;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String middleNames;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String nameChangeReason;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected NameTitleList nameTitle;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String surname;

    }


    @Data
    @Entity
    @Table(name = "source_of_funds")
    public static class SourceOfFunds {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

     	@ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "person_applicant_id")
    	PersonApplicant personApplicant;
    	
    	@Column(columnDefinition = "VARCHAR(255)")
        protected String detail;

        @Column(precision=19, scale=2)
        protected BigDecimal percentage;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected SourceOfFundsTypeList type;

        public SourceOfFunds() {
        	// JPA
        }
    }


    @Data
    @Embeddable
    public static class Will {
 
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xExecutor;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xWillHeldBy;

    }


    // Setters - Contact
	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setPersonApplicant(this);
		}
	}


    // Setters - CreditHistory
	public void setCreditHistory(List<CreditHistory> creditHistory) {
		if (creditHistory != null) {
			this.creditHistory = creditHistory;
			this.creditHistory.forEach(x -> x.setPersonApplicant(this));
		}
	}

	// Setters - DocumentationInstructions
	public void setDocumentationInstructions(DocumentationInstructions documentationInstructions) {
		if (documentationInstructions != null) {
			this.documentationInstructions = documentationInstructions;
			this.documentationInstructions.setPersonApplicant(this);
		}
	}
	

	// Setters - Employment
	public void setEmployment(List<Employment> employment) {
		if (employment != null) {
			this.employment = employment;
			this.employment.forEach(x -> x.setPersonApplicant(this));
		}
	}


	// Setters - ExistingCustomer
	public void setExistingCustomer(ExistingCustomer existingCustomer) {
		if (existingCustomer != null) {
			this.existingCustomer = existingCustomer;
			this.existingCustomer.setPersonApplicant(this);
		}
	}


	// Setters - FinancialSituationCheck
	public void setFinancialSituationCheck(List<FinancialSituationCheck> financialSituationCheck) {
		if (financialSituationCheck != null) {
			this.financialSituationCheck = financialSituationCheck;
			this.financialSituationCheck.forEach(x -> x.setPersonApplicant(this));
		}
	}


	// Setters - ForeignTaxAssociationType
	public void setForeignTaxAssociation(ForeignTaxAssociationType foreignTaxAssociation) {
		if (foreignTaxAssociation != null) {
			this.foreignTaxAssociation = foreignTaxAssociation;
			this.foreignTaxAssociation.setPersonApplicant(this);
		}
	}
	
    // Setters - IdentityCheck
	public void setIdentityCheck(List<IdentityCheck> identityCheck) {
		if (identityCheck != null) {
			this.identityCheck = identityCheck;
			this.identityCheck.forEach(x -> x.setPersonApplicant(this));
		}
	}

    // Setters - Insurance
	public void setInsurance(List<Insurance> insurance) {
		if (insurance != null) {
			this.insurance = insurance;
			this.insurance.forEach(x -> x.setPersonApplicant(this));
		}
	}

 
    // Setters - PowerOfAttorney
	public void setPowerOfAttorney(PowerOfAttorney powerOfAttorney) {
		if (powerOfAttorney != null) {
			this.powerOfAttorney = powerOfAttorney;
			this.powerOfAttorney.setPersonApplicant(this);
		}
	}


    // Setters - PreviousName
	public void setPreviousName(PreviousName previousName) {
		if (previousName != null) {
			this.previousName = previousName;
			this.previousName.setPersonApplicant(this);
		}
	}


    // Setters - Privacy
	public void setPrivacy(Privacy privacy) {
		if (privacy != null) {
			this.privacy = privacy;
			this.privacy.setPersonApplicant(this);
		}
	}


    // Setters - ProofOfIdentity
	public void setProofOfIdentity(List<ProofOfIdentity> proofOfIdentity) {
		if (proofOfIdentity != null) {
			this.proofOfIdentity = proofOfIdentity;
			this.proofOfIdentity.forEach(x -> x.setPersonApplicant(this));
		}
	}

	// Setters - PersonNameType
	public void setPersonName(PersonNameType personName) {
		if (personName != null) {
			this.personName = personName;
			this.personName.setPersonApplicant(this);
		}
	}


    // Setters - ResponsibleLendingType
	public void setResponsibleLending(ResponsibleLendingType responsibleLending) {
		if (responsibleLending != null) {
			this.responsibleLending = responsibleLending;
			this.responsibleLending.setPersonApplicant(this);
		}
	}

	// Setters - SourceofFunds
	public void setSourceOfFunds(List<SourceOfFunds> sourceOfFunds) {
		if (sourceOfFunds != null) {
			this.sourceOfFunds = sourceOfFunds;
			this.sourceOfFunds.forEach(x -> x.setPersonApplicant(this));
		}
	}

	
    // Setters - SourceOfWealth
	public void setSourceOfWealth(List<SourceOfWealth> sourceOfWealth) {
		if (sourceOfWealth != null) {
			this.sourceOfWealth = sourceOfWealth;
			this.sourceOfWealth.forEach(x -> x.setPersonApplicant(this));
		}
	}


    // Setters - TaxDeclarationDetailsType
	public void setTaxDeclarationDetails(TaxDeclarationDetailsType taxDeclarationDetails) {
		if (taxDeclarationDetails != null) {
			this.taxDeclarationDetails = taxDeclarationDetails;
			this.taxDeclarationDetails.setPersonApplicant(this);
		}
	}

}