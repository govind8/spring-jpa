package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Software {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "recipient_id")
    protected Recipient recipient;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "publisher_id")
    protected Publisher publisher;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String environment;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lixiCode;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String name;

    @Column(columnDefinition = "VARCHAR(100)")
    protected String technicalEmail;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String version;

    public Software() {
    	// JPA
    }
    
}
