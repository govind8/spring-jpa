package com.moneycatcha.app.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RepaymentTypeDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "preferences_id")
	protected Preferences preferences;    
	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "repaymentTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected InterestInAdvance interestInAdvance;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "repaymentTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected InterestOnly interestOnly;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "repaymentTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected LineOfCredit lineOfCredit;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "repaymentTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PrincipalAndInterest principalAndInterest;


    @Data
    @Entity
    @Table(name = "interest_in_advance")
    public static class InterestInAdvance {

    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_type_details_id")
    	protected RepaymentTypeDetails repaymentTypeDetails;   
    	
    	@Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "interestInAdvance", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "iia_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "iia_risksExplained", length = 3)
        protected YesNoList risksExplained;
        
        public InterestInAdvance() {
        	// JPA
        }

        // Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null) {
				this.reason = reason;
				this.reason.setInterestInAdvance(this);
			}
		}
        
    }

    @Data
    @Entity
    @Table(name = "interest_only")
    public static class InterestOnly {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_type_details_id")
    	protected RepaymentTypeDetails repaymentTypeDetails; 
    	
        @Embedded
	    @AttributeOverrides(value = {
	            @AttributeOverride(name = "length", column = @Column(name = "io_duration_length")),
	            @AttributeOverride(name = "units", column = @Column(name = "io_duration_units"))
	        })
        protected DurationType interestOnlyDuration;
        
    	@Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "interestOnly", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "io_other", length = 60)
        protected ImportanceList importance;

        @Column(name = "io_interestOnlyDurationReason", columnDefinition = "VARCHAR(255)")
        protected String interestOnlyDurationReason;

        @Enumerated(EnumType.STRING)
        @Column(name = "io_repaymentFrequency", length = 15)
        protected FrequencyShortList repaymentFrequency;

        @Enumerated(EnumType.STRING)
        @Column(name = "io_risksExplained", length = 3)
        protected YesNoList risksExplained;

        public InterestOnly() {
        	// JPA
        }
        // Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null) {
				this.reason = reason;
				this.reason.setInterestOnly(this);
			}
		}
        
    }

    @Data
    @Entity
    @Table(name = "line_of_credit")
    public static class LineOfCredit {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_type_details_id")
    	protected RepaymentTypeDetails repaymentTypeDetails; 
    	
    	@Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "lineOfCredit", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;
    	
        @Embedded
        protected RepaymentPlan repaymentPlan;

        @Enumerated(EnumType.STRING)
        @Column(name = "loc_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "loc_risksExplained", length = 3)
        protected YesNoList risksExplained;

        // Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null) {
				this.reason = reason;
				this.reason.setLineOfCredit(this);
			}
		}
        

        @Data
        @Embeddable
        public static class RepaymentPlan {
        	
            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_coApplicantIncome", length = 3)
            protected YesNoList coApplicantIncome;

            @Column(name = "loc_rp_description", columnDefinition = "VARCHAR(255)")
            protected String description;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_downsizing", length = 3)
            protected YesNoList downsizing;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_incomeFromOtherInvestments", length = 3)
            protected YesNoList incomeFromOtherInvestments;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_other", length = 3)
            protected YesNoList other;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_repaymentOfLoanPriorToEndOfTerm", length = 3)
            protected YesNoList repaymentOfLoanPriorToEndOfTerm;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_saleOfAssets", length = 3)
            protected YesNoList saleOfAssets;

            @Enumerated(EnumType.STRING)
            @Column(name = "loc_rp_savings", length = 3)
            protected YesNoList savings;

            public RepaymentPlan() {
            	// JPA
            }

        }

        public LineOfCredit() {
        	// JPA
        }
	}	

    @Data
    @Entity
    @Table(name = "principal_and_interest")
    public static class PrincipalAndInterest {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "repayment_type_details_id")
    	protected RepaymentTypeDetails repaymentTypeDetails; 
    	
    	@Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "prinicpalAndInterest", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "pai_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "pai_repaymentFrequency", length = 15)
        protected FrequencyShortList repaymentFrequency;

        @Enumerated(EnumType.STRING)
        @Column(name = "pai_risksExplained", length = 3)
        protected YesNoList risksExplained;


        public PrincipalAndInterest() {
        	// JPA
        }

        // Setter - Reason
		public void setReason(Reason reason) {
			this.reason = reason;
			this.reason.setPrinicpalAndInterest(this);
		}
    }
    
    public RepaymentTypeDetails() {
    	// JPA
    }

    // Setter - InterestInAdvance
	public void setInterestInAdvance(InterestInAdvance interestInAdvance) {
		if (interestInAdvance != null) {
			this.interestInAdvance = interestInAdvance;
			this.interestInAdvance.setRepaymentTypeDetails(this);
		}
	}

    // Setter - InterestOnly
	public void setInterestOnly(InterestOnly interestOnly) {
		if (interestOnly != null) {
			this.interestOnly = interestOnly;
			this.interestOnly.setRepaymentTypeDetails(this);
		}
	}

    // Setter - LineOfCredit
	public void setLineOfCredit(LineOfCredit lineOfCredit) {
		if (lineOfCredit != null) {
			this.lineOfCredit = lineOfCredit;
			this.lineOfCredit.setRepaymentTypeDetails(this);
		}
	}

    // Setter - PrincipalAndInterest
	public void setPrincipalAndInterest(PrincipalAndInterest principalAndInterest) {
		if (principalAndInterest != null) {
			this.principalAndInterest = principalAndInterest;
			this.principalAndInterest.setRepaymentTypeDetails(this);
		}
	}
    
    
}
