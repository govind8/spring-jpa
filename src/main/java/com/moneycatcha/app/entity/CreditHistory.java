package com.moneycatcha.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.CreditHistoryIssueList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class CreditHistory implements Serializable {

	private static final long serialVersionUID = -4822847963796424701L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn (name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "person_applicant_id")
    protected PersonApplicant personApplicant;    
    
    @Column(columnDefinition = "VARCHAR(255)")
    protected String details;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isCurrent;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CreditHistoryIssueList issue;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    public CreditHistory() {
    	// JPA
    }

}
