package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class SplitLoan {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
		
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "splitLoan", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SplitLoanComponent> splitLoanComponent;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;


    @Data
    @Entity
    @Table(name="splitloan_component")
    public static class SplitLoanComponent {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "split_loan_id")
    	protected SplitLoan splitLoan;    
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xLiability;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xLoanDetails;

    }

    // Setters - SplitLoanComponent
	public void setSplitLoanComponent(List<SplitLoanComponent> splitLoanComponent) {
		if (splitLoanComponent != null) {
			this.splitLoanComponent = splitLoanComponent;
			this.splitLoanComponent.forEach(x -> x.setSplitLoan(this));
		}
	}
    
    
}