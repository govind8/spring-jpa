package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.EmploymentStatusList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.SelfEmployedBasisList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class SelfEmployed {
 
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "employment_id")
    protected Employment employment;
  	
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Business business;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BusinessIncomePrevious businessIncomePrevious;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BusinessIncomePrior businessIncomePrior;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BusinessIncomeRecent businessIncomeRecent;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected BusinessIncomeYearToDate businessIncomeYearToDate;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DeclaredIncome declaredIncome;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "self_employed_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "self_employed_duration_units"))
    })
    protected DurationType duration;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "selfEmployed", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAnalysis financialAnalysis;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String anzscoOccupationCode;

    @Column(precision=19, scale=2)
    protected BigDecimal averageHoursPerWeek;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SelfEmployedBasisList basis;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeStatusOnOrBeforeSettlementList incomeStatusOnOrBeforeSettlement;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String occupation;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String occupationCode;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EmploymentStatusList status;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccountant;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xEmployer;

    // Setters - Business
    public void setBusiness(Business business) {
    	if (business != null) {
	        this.business = business;    	
	    	this.business.setSelfEmployed(this);
    	}
    }
    
    // Setters - BusinessIncomeRecent
    public void setBusinessIncomeRecent(BusinessIncomeRecent businessIncomeRecent) {
    	if (businessIncomeRecent != null) {
	        this.businessIncomeRecent = businessIncomeRecent;    	
	    	businessIncomeRecent.setSelfEmployed(this);
    	}
    }

    // Setters - BusinessIncomePrior
    public void setBusinessIncomePrior(BusinessIncomePrior businessIncomePrior) {
    	if (businessIncomePrior != null) {
	        this.businessIncomePrior = businessIncomePrior;    	
	    	this.businessIncomePrior.setSelfEmployed(this);
    	}
    }

    // Setters - BusinessIncomePrevious
    public void setBusinessIncomePrevious(BusinessIncomePrevious businessIncomePrevious) {
    	if (businessIncomePrevious != null) {
	        this.businessIncomePrevious = businessIncomePrevious;    	
	        this.businessIncomePrevious.setSelfEmployed(this);
    	}
    }

    // Setters - BusinessIncomeYearToDate
    public void setBusinessIncomeYearToDate(BusinessIncomeYearToDate businessIncomeYearToDate) {
    	if (businessIncomeYearToDate != null) {
	        this.businessIncomeYearToDate = businessIncomeYearToDate;    	
	    	businessIncomeYearToDate.setSelfEmployed(this);
    	}
    }

    // Setters - DeclaredIncome
    public void setDeclaredIncome(DeclaredIncome declaredIncome) {
    	if (declaredIncome != null) {
	        this.declaredIncome = declaredIncome;    	
	    	this.declaredIncome.setSelfEmployed(this);
    	}
    }
  
	public void setFinancialAnalysis(FinancialAnalysis financialAnalysis) {
		if (financialAnalysis != null) { 
			this.financialAnalysis = financialAnalysis;
			this.financialAnalysis.setSelfEmployed(this);
		}
	}
        
    public SelfEmployed() {
    	// JPA
    }



}