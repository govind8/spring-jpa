
package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.AddressTypeList;
import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.DataSourceList;
import com.moneycatcha.app.model.LevelTypeList;
import com.moneycatcha.app.model.PoBoxTypeList;
import com.moneycatcha.app.model.StreetSuffixList;
import com.moneycatcha.app.model.StreetTypeList;
import com.moneycatcha.app.model.UnitTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;


@Data
@Entity
public class AddressType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "application_id")
    Application application;
 	
	@Column(columnDefinition = "VARCHAR(80)")
    protected String australianPostCode;

	@Enumerated(EnumType.STRING)
	@Column(length = 5)
    protected AuStateList australianState;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String city;

	@Enumerated(EnumType.STRING)
	@Column(length = 5)
    protected CountryCodeList country;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected DataSourceList dataSource;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String gnafid;

	@Column(precision=19, scale=2)
    protected BigDecimal latitude;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String lgaName;

	@Column(precision=19, scale=2)
    protected BigDecimal longitude;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String overseasPostCode;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String overseasState;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
	@Column(columnDefinition = "VARCHAR(80)")
    protected String suburb;

	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList tbaAddress;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected AddressTypeList type;

	@Embedded
    protected AddressType.DeliveryPoint deliveryPoint;
	
	@Embedded
    protected AddressType.DXBox dxBox;
	
	@Embedded
    protected AddressType.NonStandard nonStandard;
	
	@Embedded
    protected AddressType.POBox poBox;
	
	@Embedded
    protected AddressType.Standard standard;

	   
    public AddressType() {
    	//JPA
    }


    @Data
    @Embeddable
    public static class DXBox {
    	@Column(name="dxbox_exchange ",  columnDefinition = "VARCHAR(80)")
        protected String exchange;

    	@Column(name="dxbox_number ", columnDefinition = "VARCHAR(80)")
        protected String number;

    	@Column(name="dxbox_provider", columnDefinition = "VARCHAR(80)")
        protected String provider;

    	public DXBox() {
    		
    	}
     	
    }


    @Data
    @Embeddable
    public static class DeliveryPoint {
    	@Column(columnDefinition = "VARCHAR(80)")
        protected String identifier;

    	@Column(columnDefinition = "VARCHAR(80)")
        protected String identifierBarcode;
    	
    	public DeliveryPoint() {
    		
    	}
    	
    	
    }


    @Data
    @Embeddable
    public static class NonStandard {
    	@Column(columnDefinition = "VARCHAR(255)")
        protected String line1;

    	@Column(columnDefinition = "VARCHAR(255)")
        protected String line2;

    	public NonStandard() {
    		
    	}

    }

    @Data
    @Embeddable
    public static class POBox {
    	@Column(name="pobox_exchange ",columnDefinition = "VARCHAR(80)")
        protected String exchange;

    	@Column(name="pobox_number ",columnDefinition = "VARCHAR(80)")
        protected String number;

    	@Enumerated(EnumType.STRING)
    	@Column(name="pobox_type ", length = 100)
        protected PoBoxTypeList type;

    	public POBox() {
    		
    	}
    }

    @Data
    @Embeddable
    public static class Standard {
    	@Column(columnDefinition = "VARCHAR(80)")
        protected String buildingName;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String level;

		@Enumerated(EnumType.STRING)
		@Column(length = 60)
        protected LevelTypeList levelType;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String lotSection;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String streetName;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String streetNumber;

		@Enumerated(EnumType.STRING)
		@Column(length = 60)
        protected StreetSuffixList streetSuffix;

		@Enumerated(EnumType.STRING)
		@Column(length = 60)
        protected StreetTypeList streetType;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String toStreetNumber;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String toUnitNumber;

		@Column(columnDefinition = "VARCHAR(80)")
        protected String unit;

		@Enumerated(EnumType.STRING)
		@Column(length = 60)
        protected UnitTypeList unitType;

		public Standard() {
			
		}
		
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "AddressType{" +
                "Id=" + Id +
                ", uniqueID='" + uniqueID + '\'' +
                ", application=" + application +
                ", australianPostCode='" + australianPostCode + '\'' +
                ", australianState=" + australianState +
                ", city='" + city + '\'' +
                ", country=" + country +
                ", dataSource=" + dataSource +
                ", gnafid='" + gnafid + '\'' +
                ", latitude=" + latitude +
                ", lgaName='" + lgaName + '\'' +
                ", longitude=" + longitude +
                ", overseasPostCode='" + overseasPostCode + '\'' +
                ", overseasState='" + overseasState + '\'' +
                ", suburb='" + suburb + '\'' +
                ", tbaAddress=" + tbaAddress +
                ", type=" + type +
                ", deliveryPoint=" + deliveryPoint +
                ", dxBox=" + dxBox +
                ", nonStandard=" + nonStandard +
                ", poBox=" + poBox +
                ", standard=" + standard +
                '}';
    }
}
