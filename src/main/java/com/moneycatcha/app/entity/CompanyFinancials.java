package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class CompanyFinancials {

	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
 
	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    Application application;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "financial_analysis_id")
    FinancialAnalysis financialAnalysis;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

	@Embedded
    protected BalanceSheet balanceSheet;
	
	@Embedded
    protected CurrentMarketData currentMarketData;
	
	@Embedded
    protected ProfitAndLoss profitAndLoss;	
    
	public CompanyFinancials() {
		// default constructor JPA
	}

    @Data
    @Embeddable
    public static class BalanceSheet {

        @Embedded
        protected Assets assets;
      
        @Embedded
        protected Liabilities liabilities;
        
        @Column(name="balancesheet_equity", precision=19, scale=2)
        protected BigDecimal equity;

        @Column(name="balancesheet_netAssets", precision=19, scale=2)
        protected BigDecimal netAssets;

        @Column(name="balancesheet_paidUpCapital", precision=19, scale=2)
        protected BigDecimal paidUpCapital;

        @Column(name="balancesheet_retainedEarnings", precision=19, scale=2)
        protected BigDecimal retainedEarnings;

        @Column(name="balancesheet_shareholderFunds", precision=19, scale=2)
        protected BigDecimal shareholderFunds;
        
        
		@Data
        @Embeddable
        public static class Assets {

        	@Column(name="assets_cash", precision=19, scale=2)
            protected BigDecimal cash;

            @Column(name="assets_cashAndCashEquivalents", precision=19, scale=2)
            protected BigDecimal cashAndCashEquivalents;

            @Column(name="assets_intangibles", precision=19, scale=2)
            protected BigDecimal intangibles;

            @Column(name="assets_inventory", precision=19, scale=2)
            protected BigDecimal inventory;

            @Column(name="assets_tangibleAssets", precision=19, scale=2)
            protected BigDecimal tangibleAssets;

            @Column(name="assets_totalAssets", precision=19, scale=2)
            protected BigDecimal totalAssets;

            @Column(name="assets_totalCurrentAssets", precision=19, scale=2)
            protected BigDecimal totalCurrentAssets;

            @Column(name="assets_tradeDebtors", precision=19, scale=2)
            protected BigDecimal tradeDebtors;
            
        }


        @Data
        @Embeddable
        public static class Liabilities {

            @Column(name="liabilities_debtAtNominalValue", precision=19, scale=2)
            protected BigDecimal debtAtNominalValue;

            @Column(name="liabilities_totalCurrentLiabilities", precision=19, scale=2)
            protected BigDecimal totalCurrentLiabilities;

            @Column(name="liabilities_totalLiabilities", precision=19, scale=2)
            protected BigDecimal totalLiabilities;

            @Column(name="liabilities_tradeCreditors", precision=19, scale=2)
            protected BigDecimal tradeCreditors;

        }
    }


    @Data
    @Embeddable
    public static class CurrentMarketData {
    	
        @Column(name="currentmarketdata_valueOfCommonEquity", precision=19, scale=2)
        protected BigDecimal valueOfCommonEquity;

        @Column(name="currentmarketdata_valueOfMinorityInterests", precision=19, scale=2)
        protected BigDecimal valueOfMinorityInterests;

        @Column(name="currentmarketdata_valueOfPreferredEquity", precision=19, scale=2)
        protected BigDecimal valueOfPreferredEquity;

     }


    @Data
    @Embeddable
    public static class ProfitAndLoss {
    	
        @Column(name="profitandloss_amortisationExpense", precision=19, scale=2)
        protected BigDecimal amortisationExpense;

        @Column(name="profitandloss_costOfGoodsSold", precision=19, scale=2)
        protected BigDecimal costOfGoodsSold;

        @Column(name="profitandloss_depreciationExpense", precision=19, scale=2)
        protected BigDecimal depreciationExpense;

        @Column(name="profitandloss_ebit", precision=19, scale=2)
        protected BigDecimal ebit;

        @Column(name="profitandloss_ebitda", precision=19, scale=2)
        protected BigDecimal ebitda;

        @Column(name="profitandloss_grossProfit", precision=19, scale=2)
        protected BigDecimal grossProfit;

        @Column(name="profitandloss_incomeTax", precision=19, scale=2)
        protected BigDecimal incomeTax;

        @Column(name="profitandloss_interestExpense", precision=19, scale=2)
        protected BigDecimal interestExpense;

        @Column(name="profitandloss_netProfitBeforeTax", precision=19, scale=2)
        protected BigDecimal netProfitBeforeTax;

        @Column(name="profitandloss_paymentsToDirectors", precision=19, scale=2)
        protected BigDecimal paymentsToDirectors;

        @Column(name="profitandloss_sales", precision=19, scale=2)
        protected BigDecimal sales;

        @Column(name="profitandloss_totalExpenses", precision=19, scale=2)
        protected BigDecimal totalExpenses;

         
    }


}
