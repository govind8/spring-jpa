package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.MasterAgreementStatusList;

import lombok.Data;

@Data
@Entity
public class MasterAgreement {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
	
    @OneToMany(mappedBy = "masterAgreement", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<AccountToIncorporate> accountToIncorporate;

    @OneToOne(mappedBy = "masterAgreement", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentOwned;

    @OneToMany(mappedBy = "masterAgreement", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Security> security;

    @Column(columnDefinition = "VARCHAR(80)")
    protected LocalDate dateOfExecution;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String description;

    @Column(columnDefinition = "VARCHAR(80)")
    protected LocalDate endDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lenderAgreementNumber;

    @Column(precision=19, scale=2)
    protected BigDecimal masterFacilityLimit;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productName;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected MasterAgreementStatusList status;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

  
    // Setters - AccountToIncorporate
    public void setAccountToIncorporate(List<AccountToIncorporate> accountToIncorporate) {
    	if (accountToIncorporate != null) {
	        this.accountToIncorporate = accountToIncorporate;    	
	    	this.accountToIncorporate.forEach(x -> x.setMasterAgreement(this));
    	}
    }	
    
    // Setters - PercentOwnedType
    public void setPercentOwned(PercentOwnedType percentOwned) {
    	if (percentOwned != null) {
	        this.percentOwned = percentOwned;    	
	    	this.percentOwned.setMasterAgreement(this);
    	}
    }	
    
    // Setters - Security
    public void setSecurity(List<Security> security) {
    	if (security != null) {
	        this.security = security;    	
	    	this.security.forEach(x -> x.setMasterAgreement(this));
    	}
    }	
    
    // constructor
    public MasterAgreement() {
    	// JPA
    }

  
    @Data
    @Entity
    public static class AccountToIncorporate {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected MasterAgreement masterAgreement;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xExistingAccount;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xNewAccount;

        public AccountToIncorporate() {
        	// JPA
        }
        
    }
    
}