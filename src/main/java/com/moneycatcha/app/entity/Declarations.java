package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Declarations {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;    

	public Declarations() {
		// JPA
	}
    

	@Embedded
    protected BrokerDeclarations brokerDeclarations;

    @Data
    @Embeddable
    public static class BrokerDeclarations {

        @Enumerated(EnumType.STRING)
        @Column(name="bd_interestOnlyMeetsRequirements", length = 3)
        protected YesNoList interestOnlyMeetsRequirements;

        @Column(name="bd_interestOnlyMeetsRequirementsDescription", columnDefinition = "VARCHAR(255)")
        protected String interestOnlyMeetsRequirementsDescription;

        @Enumerated(EnumType.STRING)
        @Column(name="bd_interestOnlyRisksExplained", length = 3)
        protected YesNoList interestOnlyRisksExplained;

        @Column(name="bd_interestOnlyRisksExplainedDescription", columnDefinition = "VARCHAR(255)")
        protected String interestOnlyRisksExplainedDescription;
        
        public BrokerDeclarations() {
        	// JPA
        }
        
     }
}