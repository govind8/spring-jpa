package com.moneycatcha.app.entity;

import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.HousingStatusList;

import lombok.Data;

@Data
@Entity
public class PreviousAddress {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "contact_id")
    protected Contact contact;
	
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "duration_units"))
    })
    protected DurationType duration;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected HousingStatusList housingStatus;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xResidentialAddress;

	public PreviousAddress() {
		// JPA
	}
    
}
