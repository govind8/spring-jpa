package com.moneycatcha.app.entity;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.DocumentCategoryList;
import com.moneycatcha.app.model.DocumentTypeList;
import com.moneycatcha.app.model.MedicareCardColourList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class ProofOfIdentity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
    @Embedded
    protected MedicareCard medicareCard;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected AuStateList australianStateOfIssue;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList certifiedCopy;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList countryOfIssue;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateDocumentVerified;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateOfIssue;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList dobVerified;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected DocumentCategoryList documentCategory;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String documentNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected DocumentTypeList documentType;

    @Column(columnDefinition = "DATE")
    protected LocalDate expiryDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isPreviousNameIdentification;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String issuingOrganisation;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String middleNameOnDocument;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String nameOnDocument;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList nameVerified;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList original;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String otherDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList photographVerified;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String placeOfIssue;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList residentialAddressVerified;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList signatureVerified;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xLocationDocumentVerified;


    @Data
    @Embeddable
    public static class MedicareCard {
    	
    	@Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected MedicareCardColourList colour;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger individualReferenceNumber;

    }
    
    public ProofOfIdentity() {
    	// JPA
    }
}