package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class Addback {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "income_previous_id")
	IncomePrevious incomePrevious;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "income_prior_id")
	IncomePrior incomePrior;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "income_recent_id")
	IncomeRecent incomeRecent;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "income_year_to_date_id")
	IncomeYearToDate incomeYearToDate;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "business_income_previous_id")
	BusinessIncomePrevious businessIncomePrevious;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "business_income_prior_id")
	BusinessIncomePrior businessIncomePrior;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "business_income_recent_id")
	BusinessIncomeRecent businessIncomeRecent;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "business_income_year_to_date_id_id")
	BusinessIncomeYearToDate businessIncomeYearToDate;    
	
    @Column(precision=19, scale=2)
    protected BigDecimal allowances;

    @Column(precision=19, scale=2)
    protected BigDecimal amortisationOfGoodwill;

    @Column(precision=19, scale=2)
    protected BigDecimal bonus;

    @Column(precision=19, scale=2)
    protected BigDecimal carExpense;
    
    @Column(precision=19, scale=2)
    protected BigDecimal carryForwardLosses;

    @Column(precision=19, scale=2)
    protected BigDecimal depreciation;

    @Column(precision=19, scale=2)
    protected BigDecimal interest;

    @Column(precision=19, scale=2)
    protected BigDecimal lease;

    @Column(precision=19, scale=2)
    protected BigDecimal nonCashBenefits;

    @Column(precision=19, scale=2)
    protected BigDecimal nonRecurringExpenses;

    @Column(precision=19, scale=2)
    protected BigDecimal paymentToTrustee;

    @Column(precision=19, scale=2)
    protected BigDecimal salary;

    @Column(precision=19, scale=2)
    protected BigDecimal superannuationExcess;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="addBack", cascade=CascadeType.ALL,  orphanRemoval = true, fetch=FetchType.LAZY)
    protected List<OtherAddback> otherAddback;
    
    public Addback() {
    	// default constructor JPA
    }

	@Data
	@Entity
	public static class OtherAddback {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;    	
		
	    @ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumn
	    protected Addback addBack;

	    @Column(precision=19, scale=2)
	    protected BigDecimal amount;

	    @Column(columnDefinition = "VARCHAR(255)")
	    protected String description;
	    
	    public OtherAddback() {
	    	// default constructor JPA
	    }
	    
	 }

    // Setters - OtherAddback
	public void setOtherAddback(List<OtherAddback> otherAddback) {
		if(otherAddback != null) {
			this.otherAddback = otherAddback;
			this.otherAddback.forEach(x -> x.setAddBack(this));
		}
	}

	@java.lang.Override
	public java.lang.String toString() {
		return "Addback{" +
				"Id=" + Id +
				", incomePrevious=" + incomePrevious +
				", incomePrior=" + incomePrior +
				", incomeRecent=" + incomeRecent +
				", incomeYearToDate=" + incomeYearToDate +
				", businessIncomePrevious=" + businessIncomePrevious +
				", businessIncomePrior=" + businessIncomePrior +
				", businessIncomeRecent=" + businessIncomeRecent +
				", businessIncomeYearToDate=" + businessIncomeYearToDate +
				", allowances=" + allowances +
				", amortisationOfGoodwill=" + amortisationOfGoodwill +
				", bonus=" + bonus +
				", carExpense=" + carExpense +
				", carryForwardLosses=" + carryForwardLosses +
				", depreciation=" + depreciation +
				", interest=" + interest +
				", lease=" + lease +
				", nonCashBenefits=" + nonCashBenefits +
				", nonRecurringExpenses=" + nonRecurringExpenses +
				", paymentToTrustee=" + paymentToTrustee +
				", salary=" + salary +
				", superannuationExcess=" + superannuationExcess +
				", otherAddback=" + otherAddback +
				'}';
	}
}