package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.AnnotationTypeList;
import com.moneycatcha.app.model.ErrorInstructionsTypeList;
import com.moneycatcha.app.model.ErrorSourceList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ErrorInstructions {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "instructions_id")
	Instructions instructions;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "errorInstructions", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Annotation> annotation;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ErrorSourceList errorSource;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String errorSourceVersion;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ErrorInstructionsTypeList type;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPath;


    @Data
    @Entity
    @Table(name = "error_annotation")
    public static class Annotation {
    	
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;

		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name = "error_instructions_id")
		ErrorInstructions errorInstructions;
				
		@Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected AnnotationTypeList type;

        @Column(name = "unqiueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

    }
}