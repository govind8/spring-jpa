package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.AssetTransactionList;
import com.moneycatcha.app.model.FinancialTransactionTypeList;
import com.moneycatcha.app.model.NonRealEstateAssetTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class NonRealEstateAsset {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected AgriculturalAsset agriculturalAsset;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected Aircraft aircraft;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected Business business;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected CleaningEquipment cleaningEquipment;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected ContractDetails contractDetails;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected EarthMovingMiningAndConstruction earthMovingMiningAndConstruction;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<Encumbrance> encumbrance;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected EstimatedValue estimatedValue;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected FinancialAsset financialAsset;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<FundsDisbursementType> fundsDisbursement;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected HospitalityAndLeisure hospitalityAndLeisure;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<Insurance> insurance;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected ITAndAVEquipment itAndAVEquipment;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected MaterialsHandlingAndLifting materialsHandlingAndLifting;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected MedicalEquipment medicalEquipment;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected MobileComputing mobileComputing;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected MotorVehicle motorVehicle;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected OfficeEquipment officeEquipment;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected OtherAsset otherAsset;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected PercentOwnedType percentOwned;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected PlantEquipmentAndIndustrial plantEquipmentAndIndustrial;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected PPSR ppsr;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy="nonRealEstateAsset", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected ToolsOfTrade toolsOfTrade;

    @Column(precision=19, scale=2)
    protected BigDecimal amountToBeReduced;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList contractOfSale;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList encumbered;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FinancialTransactionTypeList financialTransactionType;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lenderAssessmentReason;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList lenderAssessmentRequired;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList primarySecurity;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList toBeReduced;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList toBeSold;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList toBeUsedAsSecurity;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AssetTransactionList transaction;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected NonRealEstateAssetTypeList type;

    @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList verified;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xCustomerTransactionAnalysis;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xVendorTaxInvoice;

    // Setters - AgriculturalAsset
    public void setAgriculturalAsset(AgriculturalAsset agriculturalAsset) {
    	if (agriculturalAsset != null) {
	        this.agriculturalAsset = agriculturalAsset;    	
	    	this.agriculturalAsset.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - Aircraft
    public void setAircraft(Aircraft aircraft) {
    	if (aircraft != null) {
	        this.aircraft = aircraft;    	
	    	this.aircraft.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - Business
    public void setBusiness(Business business) {
    	if (business != null) {
	        this.business = business;    	
	    	this.business.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - CleaningEquipment
    public void setCleaningEquipment(CleaningEquipment cleaningEquipment) {
    	if (cleaningEquipment != null) {
	        this.cleaningEquipment = cleaningEquipment;    	
	    	this.cleaningEquipment.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - ContractDetails
    public void setContractDetails(ContractDetails contractDetails) {
    	if (contractDetails != null) {
	        this.contractDetails = contractDetails;    	
	    	this.contractDetails.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - EarthMovingMiningAndConstruction
    public void setEarthMovingMiningAndConstruction(EarthMovingMiningAndConstruction earthMovingMiningAndConstruction) {
    	if (earthMovingMiningAndConstruction != null) {
	        this.earthMovingMiningAndConstruction = earthMovingMiningAndConstruction;    	
	    	this.earthMovingMiningAndConstruction.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - Encumbrance
    public void setEncumbrance(List<Encumbrance> encumbrance) {
    	if (encumbrance != null) {
	        this.encumbrance = encumbrance;    	
	    	this.encumbrance.forEach(x -> x.setNonRealEstateAsset(this));
    	}
    }
    
    // Setters - EstimatedValue
    public void setAgriculturalAsset(EstimatedValue estimatedValue) {
    	if (estimatedValue != null) {
	        this.estimatedValue = estimatedValue;    	
	    	this.estimatedValue.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - FinancialAsset
    public void setFinancialAsset(FinancialAsset financialAsset) {
    	if (financialAsset != null) {
	        this.financialAsset = financialAsset;    	
	    	financialAsset.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - FundsDisbursementType
    public void setFundsDisbursementType(List<FundsDisbursementType> fundsDisbursement) {
    	if (fundsDisbursement != null) {
	        this.fundsDisbursement = fundsDisbursement;    	
	    	this.fundsDisbursement.forEach(x -> x.setNonRealEstateAsset(this));
    	}
    }
    
    // Setters - HospitalityAndLeisure
    public void setHospitalityAndLeisure(HospitalityAndLeisure hospitalityAndLeisure) {
    	if (hospitalityAndLeisure != null) {
	    	this.hospitalityAndLeisure = hospitalityAndLeisure;    	
	    	this.hospitalityAndLeisure.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - Insurance
    public void setInsurance(List<Insurance> insurance) {
    	if (insurance != null) {
	        this.insurance = insurance;    	
	    	this.insurance.forEach(x -> x.setNonRealEstateAsset(this));
    	}
    }
    
    // Setters - ITAndAVEquipment
    public void setITAndAVEquipment(ITAndAVEquipment itAndAVEquipment) {
    	if (itAndAVEquipment != null) {
	        this.itAndAVEquipment = itAndAVEquipment;    	
	    	this.itAndAVEquipment.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - MaterialsHandlingAndLifting
    public void setMaterialsHandlingAndLifting(MaterialsHandlingAndLifting materialsHandlingAndLifting) {
    	if (materialsHandlingAndLifting != null) {
	        this.materialsHandlingAndLifting = materialsHandlingAndLifting;    	
	    	this.materialsHandlingAndLifting.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - MedicalEquipment
    public void setMedicalEquipment(MedicalEquipment medicalEquipment) {
    	if (medicalEquipment != null) {
	        this.medicalEquipment = medicalEquipment;    	
	    	this.medicalEquipment.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - MobileComputing
    public void setMobileComputing(MobileComputing mobileComputing) {
    	if (mobileComputing != null) {
	        this.mobileComputing = mobileComputing;    	
	    	this.mobileComputing.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - MotorVehicle
    public void setMotorVehicle(MotorVehicle motorVehicle) {
    	if (motorVehicle != null) {
	        this.motorVehicle = motorVehicle;    	
	    	this.motorVehicle.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - OfficeEquipment
    public void setOfficeEquipment(OfficeEquipment officeEquipment) {
    	if (officeEquipment != null) {
	        this.officeEquipment = officeEquipment;    	
	    	this.officeEquipment.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - OtherAsset
    public void setOtherAsset(OtherAsset otherAsset) {
    	if (otherAsset != null) {
	        this.otherAsset = otherAsset;    	
	        this.otherAsset.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - PercentOwnedType
    public void setPercentOwned(PercentOwnedType percentOwned) {
    	if (percentOwned != null) {
	        this.percentOwned = percentOwned;    	
	    	this.percentOwned.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - PlantEquipmentAndIndustrial
    public void setPlantEquipmentAndIndustrial(PlantEquipmentAndIndustrial plantEquipmentAndIndustrial) {
    	if (plantEquipmentAndIndustrial != null) {
	        this.plantEquipmentAndIndustrial = plantEquipmentAndIndustrial;    	
	    	this.plantEquipmentAndIndustrial.setNonRealEstateAsset(this);
    	}
    }	
    
    // Setters - PPSR
    public void setPPSR(PPSR ppsr) {
    	if (ppsr != null) {
	        this.ppsr = ppsr;    	
	    	this.ppsr.setNonRealEstateAsset(this);
    	}
    }	

    // Setters - ToolsOfTrade
    public void setToolsOfTrade(ToolsOfTrade toolsOfTrade) {
    	if (toolsOfTrade != null) {
	        this.toolsOfTrade = toolsOfTrade;    	
	    	this.toolsOfTrade.setNonRealEstateAsset(this);
    	}
    }	
    
    
    @Data
    @Entity
    @Table(name="non_real_estate_asset_insurance")
    public static class Insurance {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
 
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected NonRealEstateAsset nonRealEstateAsset;
        
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xInsurance;

		public Insurance() {
			//JPA
		}
        
    }


}