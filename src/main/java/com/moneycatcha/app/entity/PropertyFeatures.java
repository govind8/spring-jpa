package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.LandAreaUnitsList;
import com.moneycatcha.app.model.PoolTypeList;

import lombok.Data;

@Data
@Entity
public class PropertyFeatures {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "real_estate_asset_id" )
    protected RealEstateAsset realEstateAsset;
	
    @Column(columnDefinition = "VARCHAR(255)")
    protected String allPropertyFeatures;

    @Column(precision=19, scale=2)
    protected BigDecimal floorArea;

    @Column(precision=19, scale=2)
    protected BigDecimal landArea;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LandAreaUnitsList landAreaUnits;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfBathrooms;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfBedrooms;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfCarSpaces;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PoolTypeList poolType;

    @Column(columnDefinition = "VARCHAR(500)")
    protected String propertyDescription;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String propertyID;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String propertyImage;

    @Column(columnDefinition = "VARCHAR(500)")
    protected String roofMaterial;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(500)")
    protected String wallMaterial;

	public PropertyFeatures() {
		// JPA
	}
    
    

}