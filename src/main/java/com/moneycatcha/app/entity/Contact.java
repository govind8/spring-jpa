package com.moneycatcha.app.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.entity.Overview.BranchDomicile;
import com.moneycatcha.app.entity.Overview.BranchSign;
import com.moneycatcha.app.model.ContactEmailTypeList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PreferredContactCompanyList;
import com.moneycatcha.app.model.PreferredContactPersonList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @Column(name="uniqueid", columnDefinition="VARCHAR(80)")
    protected String uniqueID;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "aggregrator_id")
	Aggregator aggregator;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "branch_sign_id")
	BranchSign branchSign;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "branch_domicile_id")
	BranchDomicile branchDomicile;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_id")
	Company company;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "loan_writer_id")
	LoanWriter loanWriter;
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "related_company_id")
	RelatedCompany relatedCompany;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "related_person_id")
	RelatedPerson relatedPerson;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "signature_type_id")
	SignatureType signatureType;
	
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;
	
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected CurrentAddress currentAddress;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<EmailAddress> emailAddress;
	
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PostSettlementAddress postSettlementAddress;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PreviousAddress previousAddress;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<PriorAddress> priorAddress;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "contact", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ContactPerson contactPerson;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "of_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "of_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "of_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "of_overseas_dialing_code"))
             })
    protected PhoneType officeFax;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "om_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "om_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "om_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "om_overseas_dialing_code"))
             })
    protected PhoneType officeMobile;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "op_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "op_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "op_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "op_overseas_dialing_code"))
             })
    protected PhoneType officePhone;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "fax_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "fax_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "fax_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "fax_overseas_dialing_code"))
             })
    protected PhoneType fax;

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "wp_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "wp_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "wp_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "wp_overseas_dialing_code"))
             })
    protected PhoneType workPhone;

    @Embedded
    @AttributeOverrides(value = {
    		@AttributeOverride(name = "australianDialingCode", column = @Column(name = "mobile_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "mobile_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "mobile_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "mobile_overseas_dialing_code"))
             })
    protected PhoneType mobile;
    

    @Embedded
    @AttributeOverrides(value = {
    		@AttributeOverride(name = "australianDialingCode", column = @Column(name = "hp_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "hp_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "hp_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "hp_overseas_dialing_code"))
             })
    protected PhoneType homePhone;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "fn_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "fn_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "fn_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "fn_overseas_dialing_code"))
             })
    protected PhoneType faxNumber;

    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "prev_registered_address_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "prev_registered_address_duration_units")),
             })
    
    protected DurationType previousRegisteredAddressDuration;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "principal_trading_address_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "principal_trading_address_duration_units")),
             })
    protected DurationType principalTradingAddressDuration;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "registered_address_duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "registered_address_duration_units")),
             })
    protected DurationType registeredAddressDuration;
    
	
    @Column(columnDefinition = "VARCHAR(100)")
    protected String email;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ContactEmailTypeList emailType;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PreferredContactCompanyList preferredContactCompanyList;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PreferredContactPersonList preferredContactPersonList;

    @Column(columnDefinition = "DATE")
    protected LocalDate previousRegisteredAddressEndDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate previousRegisteredAddressStartDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate principalTradingAddressStartDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate registeredAddressStartDate;

    @Column(columnDefinition = "VARCHAR(100)")
    protected String webAddress;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAddress;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xMailingAddress;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPreviousRegisteredAddress;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPrincipalTradingAddress;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xRegisteredAddress;
    
    public Contact() {
 		// JPA
 	}

	@Data
	@Entity
	@Table(name = "contact_person")
	public static class ContactPerson {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;
	    
	    @OneToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name = "contact_id")
	    protected Contact contact;
	    
	    @Column(columnDefinition = "VARCHAR(80)")
	    protected String firstName;
	
	    @Enumerated(EnumType.STRING)
	    @Column(length = 60)
	    protected NameTitleList nameTitle;
	
	    @Column(columnDefinition = "VARCHAR(80)")
        protected String role;
	
	    @Column(columnDefinition = "VARCHAR(80)")
        protected String surname;
	
	    @Column(columnDefinition = "VARCHAR(80)")
        protected String xContactPerson;

	    public ContactPerson() {
	    	//
	    }

    }
	
	
	// Setter - CurrentAddress
	public void setCurrentAddress(CurrentAddress currentAddress) {
		if (currentAddress != null) {
			this.currentAddress = currentAddress;
			this.currentAddress.setContact(this);
		}
	}

	// Setter - EmailAddress
	public void setEmailAddress(List<EmailAddress> emailAddress) {
		if (emailAddress != null) {
			this.emailAddress = emailAddress;
			this.emailAddress.forEach(x -> x.setContact(this));
		}
	}

	// Setter - PostSettlementAddress
	public void setPostSettlementAddress(PostSettlementAddress postSettlementAddress) {
		if (postSettlementAddress != null) {
			this.postSettlementAddress = postSettlementAddress;
			this.postSettlementAddress.setContact(this);
		}
	}

	// Setter - PreviousAddress
	public void setPreviousAddress(PreviousAddress previousAddress) {
		if (previousAddress != null) {
			this.previousAddress = previousAddress;
			this.previousAddress.setContact(this);
		}
	}

	// Setter - PriorAddress
	public void setPriorAddress(List<PriorAddress> priorAddress) {
		if (priorAddress != null) {
			this.priorAddress = priorAddress;
			this.priorAddress.forEach( x-> x.setContact(this));
		}
	}

	// Setter - ContactPerson
	public void setContactPerson(ContactPerson contactPerson) {
		if (contactPerson != null) {
			this.contactPerson = contactPerson;
			this.contactPerson.setContact(this);
		}
	}

}
