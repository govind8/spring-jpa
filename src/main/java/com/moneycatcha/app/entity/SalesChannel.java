package com.moneycatcha.app.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.SalesChannelTypeList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class SalesChannel {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
    	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "salesChannel", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Aggregator aggregator;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "salesChannel", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Commission commission;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "salesChannel", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Company company;
    
    @Embedded
    protected Introducer introducer;
    
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "salesChannel", cascade = CascadeType.ALL, orphanRemoval = true)
    protected LoanWriter loanWriter;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SalesChannelTypeList type;


    @Data
    @Embeddable
    public static class Introducer {
    	
        @Column(name = "introducer_companyname", columnDefinition = "VARCHAR(255)")
        protected String companyName;

        @Column(name = "introducer_contactname", columnDefinition = "VARCHAR(80)")
        protected String contactName;

        @Column(name = "introducer_introducerid", columnDefinition = "VARCHAR(80)")
        protected String introducerID;

    }
    
    // Constructor
	public SalesChannel() {
		// JPA
	}

    // Setters - Aggregator
	public void setAggregator(Aggregator aggregator) {
		if (aggregator != null) {
			this.aggregator = aggregator;
			this.aggregator.setSalesChannel(this);
		}
	}

    // Setters - Commission
	public void setCommission(Commission commission) {
		if (commission != null) {
			this.commission = commission;
			this.commission.setSalesChannel(this);
		}
	}

    // Setters - Company
	public void setCompany(Company company) {
		if (company != null) {
			this.company = company;
			this.company.setSalesChannel(this);
		}
	}

    // Setters - LoanWriter
	public void setLoanWriter(LoanWriter loanWriter) {
		if (loanWriter != null) {
			this.loanWriter = loanWriter;
			this.loanWriter.setSalesChannel(this);
		}
	}
    
}


