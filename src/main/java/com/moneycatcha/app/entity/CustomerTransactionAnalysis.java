package com.moneycatcha.app.entity;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.AggregatedTransactionsCategoryTypeList;
import com.moneycatcha.app.model.TransactionTypeList;
import com.moneycatcha.app.model.YesNoList;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;


@Data
@Entity
public class CustomerTransactionAnalysis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "applicatino_id")
    protected Application application;
    
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="customerTransactionAnalysis", cascade=CascadeType.ALL,  orphanRemoval = true)
	protected List<CategorySet> categorySet;

    @Embedded
    protected Result result;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String analysisID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String dataAggregator;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    // Setters - CategorySet
    public void setCategorySet(List<CategorySet> categorySet) {
        if (categorySet == null) {
            if (this.categorySet != null) {
                this.categorySet.forEach(x -> x.setCustomerTransactionAnalysis(null));
            }
        }
        else {
        	categorySet.forEach(x -> x.setCustomerTransactionAnalysis(this));
        }
        this.categorySet = categorySet;    	
    }
    
    public CustomerTransactionAnalysis() {
    	// JPA
    }
    
    
	@Data
	@Entity
	@Table(name = "category_set")
    public static class CategorySet {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String categorySetName;

        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected CustomerTransactionAnalysis customerTransactionAnalysis;
        
    	@Setter(AccessLevel.NONE)
        @OneToMany(mappedBy="categorySet", cascade=CascadeType.ALL,  orphanRemoval = true, fetch=FetchType.LAZY)
    	protected List<AggregatedTransactions> aggregatedTransactions;

        public CategorySet() {
        	// default constructor JPA
        }

		@Data
        @Entity
        @Table(name="aggregated_transactions")
        public static class AggregatedTransactions {

	    	@Id
	    	@GeneratedValue(strategy = GenerationType.IDENTITY)
	        protected Long Id;
	    	
            @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;
            
	        @ManyToOne(fetch=FetchType.LAZY)
	        @JoinColumn(name="category_set_id")
	        protected CategorySet categorySet;
	        
            @Column(precision=19, scale=2)
            protected BigDecimal amount;

            @Column(precision=19, scale=2)
            protected BigDecimal amountOnMonthlyBasis;

            @Enumerated(EnumType.STRING)
            @Column(length = 80)
            protected AggregatedTransactionsCategoryTypeList category;

            @Column(columnDefinition = "VARCHAR(80)")
            protected String otherCategory;

            @Column(columnDefinition = "VARCHAR(80)")
            protected String otherCategoryCode;

            @Enumerated(EnumType.STRING)
            @Column(length = 80)
            protected TransactionTypeList type;
            
            public AggregatedTransactions() {
            	// default constructor JPA
            }

        }

		public void setAggregatedTransactions(List<AggregatedTransactions> aggregatedTransactions) {
			if (aggregatedTransactions != null) {
				this.aggregatedTransactions = aggregatedTransactions;
				this.aggregatedTransactions.forEach(x -> x.setCategorySet(this));
			}
		}
    }

    @Data
    @Embeddable
    public static class Result {

        @Column(name="result_manualcheckdetails", columnDefinition = "VARCHAR(80)")
        protected String manualCheckDetails;

        @Enumerated(EnumType.STRING)
        @Column(name="result_refermanualcheck", length = 3)
        protected YesNoList referForManualCheck;

    }
}
