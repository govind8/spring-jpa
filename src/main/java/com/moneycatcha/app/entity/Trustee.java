package com.moneycatcha.app.entity;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;


@Data
@Entity
public class Trustee {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    

    @Column(columnDefinition = "DATE")
    protected LocalDate appointmentDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xTrustee;
    
    public Trustee() {
    	// JPA
    }

}