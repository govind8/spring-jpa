package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.LivingExpenseCategoryList;
import com.moneycatcha.app.model.OtherCommitmentCategoryList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Household {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;    

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="application_id")
    protected Application application;
    
    @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;	

    @Column(columnDefinition = "VARCHAR(80)")
    protected String name;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfAdults;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfDependants;
    
	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "household", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Dependant> dependant;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "household", cascade = CascadeType.ALL, orphanRemoval = true)
    protected EducationExpenses educationExpenses;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "household", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ExpenseDetails expenseDetails;

    
    public Household() {
    	// JPA
    }
    

	@Data
    @Entity
    @Table(name="dependant")
    public static class Dependant {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "household_id")
    	Household household;
    	
    	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger age;

        @Column(columnDefinition = "DATE")
        protected LocalDate dateOfBirth;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String name;
        
        @Setter(AccessLevel.NONE)
        @OneToMany(mappedBy="dependant", cascade=CascadeType.ALL,  orphanRemoval = true)
        protected List<FinancialProvider> financialProvider;
        
        public Dependant() {
        	// JPA
        }
        

		@Data
        @Entity
        @Table(name = "hh_financial_provider")
        public static class FinancialProvider {

        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
        	@ManyToOne(fetch = FetchType.LAZY)
        	@JoinColumn(name = "dependant_id")
        	Dependant dependant; 
        	
        	@Column(columnDefinition = "VARCHAR(80)")
            protected String xParty;
        	
        	public FinancialProvider() {
        		// JPA
        	}
        }
		
	    // Setters - EducationExpenses
	    public void setFinancialProvider(List<FinancialProvider> financialProvider) {
	    	if (financialProvider != null ) {
		        this.financialProvider = financialProvider;    
		        this.financialProvider.forEach(x -> x.setDependant(this));
	    	}
	    }        
    }

    @Data
    @Entity
    @Table(name = "hh_education_expenses")
    public static class EducationExpenses {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@OneToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "household_id")
    	Household household;
    	
        @Column(name="numberOfAdultsInFullTimeStudy", columnDefinition = "BIGINT")
        protected BigInteger numberOfAdultsInFullTimeStudy;

        @Column(name="numberOfAdultsInPartTimeStudy", columnDefinition = "BIGINT")
        protected BigInteger numberOfAdultsInPartTimeStudy;

        @Column(name="numberOfChildrenInPrivateSchool", columnDefinition = "BIGINT")
        protected BigInteger numberOfChildrenInPrivateSchool;

        @Column(name="numberOfChildrenInPublicSchool", columnDefinition = "BIGINT")
        protected BigInteger numberOfChildrenInPublicSchool;

        public EducationExpenses() {
        	// JPA
        }      

    }

    @Data
    @Entity
    @Table(name = "hh_expense_details")
    public static class ExpenseDetails {

       	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@Enumerated(EnumType.STRING)
    	@Column(length = 3)
    	protected YesNoList brokerVerifiedExpense;
        
        @Column(columnDefinition = "VARCHAR(255)")
        protected String brokerVerifiedExpenseDetails;
        
        @Column(columnDefinition = "VARCHAR(255)")
        protected String statedLessThanCalculatedLivingExpensesDetails;
        
       	
    	@OneToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "household_id")
    	Household household;     	
    	
        @Setter(AccessLevel.NONE)
        @OneToMany(mappedBy="expenseDetails", cascade=CascadeType.ALL,  orphanRemoval = true)
        protected List<LivingExpense> livingExpense;

        @Setter(AccessLevel.NONE)
        @OneToMany(mappedBy="expenseDetails", cascade=CascadeType.ALL,  orphanRemoval = true)
        protected List<OtherCommitment> otherCommitment;

        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy="expenseDetails", cascade=CascadeType.ALL,  orphanRemoval = true)
        protected TotalSystemCalculatedLivingExpenses totalSystemCalculatedLivingExpenses;
        
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy="expenseDetails", cascade=CascadeType.ALL,  orphanRemoval = true)
        protected TotalUserStatedLivingExpenses totalUserStatedLivingExpenses;
        
        public ExpenseDetails() {
        	
        }
        
		@Data
        @Entity
        @Table(name = "hh_living_expense")
        public static class LivingExpense {

        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
        	@ManyToOne(fetch = FetchType.LAZY)
        	@JoinColumn(name = "expense_details_id")
        	ExpenseDetails expenseDetails;
        	
            @Setter(AccessLevel.NONE)
            @OneToOne(mappedBy="livingExpense", cascade=CascadeType.ALL,  orphanRemoval = true)
            protected PercentOwnedType percentResponsible;
            
        	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;
        	
            @Column(precision=19, scale=2)
            protected BigDecimal amount;

            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected LivingExpenseCategoryList category;

            @Column(columnDefinition = "VARCHAR(255)")
            protected String description;

            @Column(columnDefinition = "DATE")
            protected LocalDate endDate;

            @Enumerated(EnumType.STRING)
            @Column(length = 15)
            protected FrequencyFullList frequency;

            @Column(columnDefinition = "DATE")
            protected LocalDate startDate;


            public LivingExpense() {
            	// JPA
            }
            
            // Setters - PercentResponsible
            public void setPercentResponsible(PercentOwnedType percentResponsible) {
            	if (percentResponsible != null) {
	                this.percentResponsible = percentResponsible;    
	                this.percentResponsible.setLivingExpense(this);
            	}
            }        
            
        }


        @Data
        @Entity
        @Table(name = "hh_other_commitment")
        public static class OtherCommitment {
        	
        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
        	@ManyToOne(fetch = FetchType.LAZY)
        	@JoinColumn(name = "expense_details_id")
        	ExpenseDetails expenseDetails;
        	
            @Setter(AccessLevel.NONE)
            @OneToOne(mappedBy="otherCommitment", cascade=CascadeType.ALL,  orphanRemoval = true)
            protected PercentOwnedType percentResponsible;
            
        	@Column(name="uniqueid",  columnDefinition = "VARCHAR(80)")
            protected String uniqueID;
            
            @Column(precision=19, scale=2)
            protected BigDecimal amount;

            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected OtherCommitmentCategoryList category;

            @Column(columnDefinition = "VARCHAR(255)")
            protected String description;

            @Column(columnDefinition = "DATE")
            protected LocalDate endDate;

            @Enumerated(EnumType.STRING)
            @Column(length = 15)
            protected FrequencyFullList frequency;

            @Column(columnDefinition = "DATE")
            protected LocalDate startDate;


            public OtherCommitment() {
            	// JPA
            }
       
            // Setters - PercentOwnedType
            public void setPercentResponsible(PercentOwnedType percentResponsible) {
            	if (percentResponsible != null) {
	                this.percentResponsible = percentResponsible;    
	                this.percentResponsible.setOtherCommitment(this);
            	}
            }           
            
        }
		
        @Data
        @Entity
        @Table(name = "hh_total_system_calcuated_living_expenses")
        public static class TotalSystemCalculatedLivingExpenses {

        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;            
        	
        	@Column(precision=19, scale=2)
            protected BigDecimal amount;
            
            @Enumerated(EnumType.STRING)
            @Column(length = 40)
            protected FrequencyShortList frequency;

        	@OneToOne(fetch = FetchType.LAZY)
        	@JoinColumn(name = "expense_details_id")
        	ExpenseDetails expenseDetails;     	
    	    	
        	
        	public TotalSystemCalculatedLivingExpenses() {
            	//JPA
            }
        }


         
        @Data
        @Entity
        @Table(name = "hh_total_users_calcuated_living_expenses")
        public static class TotalUserStatedLivingExpenses {

        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
        	@Column(precision=19, scale=2)
            protected BigDecimal amount;
            
            @Enumerated(EnumType.STRING)
            @Column(length = 40)
            protected FrequencyShortList frequency;
            
        	@OneToOne(fetch = FetchType.LAZY)
        	@JoinColumn(name = "expense_details_id")
        	ExpenseDetails expenseDetails;     	
        	
        	public TotalUserStatedLivingExpenses() {
        		// JPA
        	}
        }
        
        
	    // Setters - LivingExpense
	    public void setLivingExpense(List<LivingExpense> livingExpense) {
	    	if (livingExpense != null) {
		        this.livingExpense = livingExpense;    
		        this.livingExpense.forEach(x -> x.setExpenseDetails(this));
	    	}
	    }        

	    // Setters - OtherCommitment
	    public void setOtherCommitment(List<OtherCommitment> otherCommitment) {
	    	if (otherCommitment != null) {
		        this.otherCommitment = otherCommitment;    
		        this.otherCommitment.forEach(x -> x.setExpenseDetails(this));
	    	}
	    }        
	    
	    // Setters - TotalSystemCalculatedLivingExpenses
	    public void setTotalSystemCalculatedLivingExpenses(TotalSystemCalculatedLivingExpenses totalSystemCalculatedLivingExpenses) {
	    	if (totalSystemCalculatedLivingExpenses != null) {
		        this.totalSystemCalculatedLivingExpenses = totalSystemCalculatedLivingExpenses;    
		        this.totalSystemCalculatedLivingExpenses.setExpenseDetails(this);
	    	}
	    }   
	    
	    // Setters - TotalUserStatedLivingExpenses
	    public void setTotalUserStatedLivingExpenses(TotalUserStatedLivingExpenses totalUserStatedLivingExpenses) {
	    	if (totalUserStatedLivingExpenses != null) {
		        this.totalUserStatedLivingExpenses = totalUserStatedLivingExpenses;    
		        this.totalUserStatedLivingExpenses.setExpenseDetails(this);
	    	}
	    }   	    
	    
    }
    
    // Setters - Dependant
    public void setDependant(List<Dependant> dependant) {
    	 if(dependant != null) {
	         this.dependant = dependant;    	
	         this.dependant.forEach(x -> x.setHousehold(this));
    	 }
    }
    
    // Setters - EducationExpenses
    public void setEducationExpenses(EducationExpenses educationExpenses) {
    	if (educationExpenses != null) {
	        this.educationExpenses = educationExpenses;    
	        this.educationExpenses.setHousehold(this);
    	}
    }        

    // Setters - ExpenseDetails
    public void setExpenseDetails(ExpenseDetails expenseDetails) {
    	if (expenseDetails != null) {
	        this.expenseDetails = expenseDetails;    
	        this.expenseDetails.setHousehold(this);
    	}
    }        

}