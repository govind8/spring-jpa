package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Features {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "credit_card_id")
    CreditCard creditCard;
 
	@OneToMany(mappedBy = "features", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SupplementaryCardholder> supplementaryCardholder;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList cardUpgrade;

    @Column(name = "features_limit", precision=19, scale=2)
    protected BigDecimal limit;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String loyaltyProgramNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productCategory;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productName;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPrimaryCardholder;
    
    public Features() {
    	// JPA
    }
    
	public void setSupplementaryCardholder(List<SupplementaryCardholder> supplementaryCardholder) {
		if (supplementaryCardholder != null) {
			this.supplementaryCardholder = supplementaryCardholder;
			this.supplementaryCardholder.forEach(x -> x.setFeatures(this));
		}
	}

}