package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;


@Entity
@Data
public class Shareholder {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    

    @Column(precision=19, scale=2)
    protected BigDecimal percentOwned;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xShareholder;

    public Shareholder() {
    	// Default constructor
    }

}
