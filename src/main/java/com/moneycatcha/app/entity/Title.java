package com.moneycatcha.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.PlanTypeList;
import com.moneycatcha.app.model.TenureTypeList;
import com.moneycatcha.app.model.TitleSystemList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Title {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "real_estate_asset_id" )
    protected RealEstateAsset realEstateAsset;
    
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "title", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<RegisteredProprietor> registeredProprietor;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String autoConsol;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String block;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String county;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String district;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList duplicateTitleIssued;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String extent;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String folio;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String location;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String lot;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String lotPlan;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherDetails;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String parcel;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String parish;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String plan;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PlanTypeList planType;

    @Column(columnDefinition = "VARCHAR(500)")
    protected String realPropertyDescriptor;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String register;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String section;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected TenureTypeList tenureType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String titleReference;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected TitleSystemList titleSystem;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String unit;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList unregisteredLand;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList unregisteredPlan;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String volume;


    @Data
    @Entity
    @Table(name = "registered_proprietor")
    public static class RegisteredProprietor {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "title_id" )
        protected Title title;
    	
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList changeNameFormRequired;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String name;
        
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xRegisteredProprietor;

		public RegisteredProprietor() {
			// JPA
		}

    }


	public Title() {
		// JPA
	}

	// Setter RegisteredProprietor 
	public void setRegisteredProprietor(List<RegisteredProprietor> registeredProprietor) {
		if (registeredProprietor != null) {
			this.registeredProprietor = registeredProprietor;
			this.registeredProprietor.forEach(x -> x.setTitle(this));
		}
	}
    
	
}