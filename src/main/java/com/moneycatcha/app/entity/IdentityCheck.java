package com.moneycatcha.app.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class IdentityCheck {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String dataSource;

    @Column(columnDefinition = "DATE")
    protected LocalDate date;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList politicallyExposedPerson;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String referenceNumber;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String result;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList sanctionedPerson;
    
    public IdentityCheck() {
    	// JPA
    }

}