package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class FinancialAnalysis {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="company_applicant_id")
	CompanyApplicant companyApplicant;    
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "business_id")
	Business business;   
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "self_employed_id")
	SelfEmployed selfEmployed;   

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="financialAnalysis", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    protected List<CompanyFinancials> companyFinancials;

    @Column(precision=19, scale=2)
    protected BigDecimal annualPaymentOnCommitments;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList completeFinancialAnalysis;

    public FinancialAnalysis() {
    	// JPA
    }
    
    // Setters - CompanyFinancials
    public void setCompanyFinancials(List<CompanyFinancials> companyFinancials) {
    	if (companyFinancials != null) {
	        this.companyFinancials = companyFinancials;    	
	    	this.companyFinancials.forEach(x -> x.setFinancialAnalysis(this));
    	}
    }
    
    /***
     *  Check the implementation for x_reference
     * @author govind
     *
     */

	@Data
    @Entity
    @Table(name = "fa_company_financials")
    public static class CompanyFinancials {

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;
	    
	    @ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumn
	    protected FinancialAnalysis financialAnalysis;   
	    
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xCompanyFinancials;
        
        public CompanyFinancials() {
        	
        }
    }
	
	
}
