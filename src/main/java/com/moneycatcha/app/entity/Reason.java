package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.entity.FundsAccessTypeDetails.Redraw;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedAndVariableRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.VariableRate;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestInAdvance;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestOnly;
import com.moneycatcha.app.entity.RepaymentTypeDetails.LineOfCredit;
import com.moneycatcha.app.entity.RepaymentTypeDetails.PrincipalAndInterest;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Reason {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "offset_account_id")
	protected OffsetAccount offsetAccount;   
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "redraw_id")
	protected Redraw redraw;   
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fixed_and_variable_rate_id")
	protected FixedAndVariableRate fixedAndVariableRate;   
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "fixed__rate_id")
	protected FixedRate fixedRate;  
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "variable_rate_id")
	protected VariableRate variableRate;  
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "interest_in_advance_id")
	protected InterestInAdvance interestInAdvance;  
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "interest_only_id")
	protected InterestOnly interestOnly;  
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "line_of_credit_id")
	protected LineOfCredit lineOfCredit;  
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "principal_and_interest_id")
	protected PrincipalAndInterest prinicpalAndInterest;  


    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowsAccessToFunds;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowsPayingOffLoanSooner;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList forTaxPurposes;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList other;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList flexibilityToAccessPrepaidFundsIfNeeded;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList flexibility;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList limitingRateIncreaseRisk;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList avoidingRateIncreaseRisk;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList potentialRateDecreases;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList discountsOnInterestRate;

    public Reason() {
    	// JPA
    }
}