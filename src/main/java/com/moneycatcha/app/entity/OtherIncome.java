package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.CurrencyCodeList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.GovernmentBenefitsTypeList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.IncomeVerificationList;
import com.moneycatcha.app.model.OtherIncomeTypeList;
import com.moneycatcha.app.model.ProofCodeOtherList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class OtherIncome {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Application application;
    
    @OneToOne(mappedBy = "otherIncome", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentOwned;

    @Column(precision=19, scale=2)
    protected BigDecimal amount;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String benefitsDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected CountryCodeList country;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyShortList frequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected GovernmentBenefitsTypeList governmentBenefitsType;

    @Column(precision=19, scale=2)
    protected BigDecimal gstAmount;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeStatusOnOrBeforeSettlementList incomeStatusOnOrBeforeSettlement;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeVerificationList incomeVerification;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isTaxable;

    @Column(precision=19, scale=2)
    protected BigDecimal netAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyShortList netAmountFrequency;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfRepeats;
   
    @Column(precision=19, scale=2)
    protected BigDecimal previousYearAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CurrencyCodeList primaryForeignCurrency;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ProofCodeOtherList proofCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList proofSighted;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList taxed;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OtherIncomeTypeList type;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAsset;    
    
	public OtherIncome() {
		// JPA
	}
	
	// Setters - PercentOwned
	public void setPercentOwned(PercentOwnedType percentOwned) {
		if (percentOwned != null) {
			this.percentOwned = percentOwned;
			this.percentOwned.setOtherIncome(this);
		}
	}
    
    
}
