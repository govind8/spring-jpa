package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.ReasonForDifferentNameList;
import com.moneycatcha.app.model.SecurityPriorityList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Security {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected LendingGuarantee lendingGuarantee;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Liability liability;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected LoanDetails loanDetails;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected MasterAgreement masterAgreement;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "security", cascade = CascadeType.ALL, orphanRemoval = true)
    protected SecurityAgreementType securityAgreement;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "security", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentOwned;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SecurityPriorityList priority;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSecurity;

  
	// Liability
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "security", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<MortgagorDetails> mortgagorDetails;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String mortgageNumber;

    @Column(precision=19, scale=2)
    protected BigDecimal priorityAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList requiresOriginalMortgageDocument;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList signedDocumentsRequired;

    // Loan Details
    @Column(precision=19, scale=2)
    protected BigDecimal allocatedAmount;
    
	// Setters - SecurityAgreementType
	public void setSecurityAgreement(SecurityAgreementType securityAgreement) {
		if (securityAgreement != null) {
			this.securityAgreement = securityAgreement;
	    	this.securityAgreement.setSecurity(this);
		}
	}

	// Setters - MortgagorDetails
	public void setMortgagorDetails(List<MortgagorDetails> mortgagorDetails) {
		if (mortgagorDetails != null) {
			this.mortgagorDetails = mortgagorDetails;
			this.mortgagorDetails.forEach(x -> x.setSecurity(this));
		}
	}     
	
	// Setters - PercentOwnedType
	public void setPercentOwned(PercentOwnedType percentOwned) {
		if (percentOwned != null) {
			this.percentOwned = percentOwned;
			this.percentOwned.setSecurity(this);
		}
	} 
	
    public Security() {
    	// JPA
    }
    
  	@Data
    @Entity
    public static class MortgagorDetails {
        
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn
        protected Security security;
        
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList changeNameFormRequired;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String name;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected ReasonForDifferentNameList reasonForDifferentName;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xMortgagor;
        
        public MortgagorDetails() {
        	// JPA
        }
        
    }


}
