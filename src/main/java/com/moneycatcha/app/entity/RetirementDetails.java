package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RetirementDetails {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;   
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "retirementDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Applicant> applicant;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "retirementDetails", cascade = CascadeType.ALL, orphanRemoval = true)
	protected RepaymentOptions repaymentOptions;

	// Constructor
	public RetirementDetails() {
		// JPA
	}
	
	// Setter - Applicant
	public void setApplicant(List<Applicant> applicant) {
		if (applicant != null) {
			this.applicant = applicant;
			this.applicant.forEach(x -> x.setRetirementDetails(this));
		}
	}

	// Setter - RepaymentOptions
	public void setRepaymentOptions(RepaymentOptions repaymentOptions) {
		if(repaymentOptions != null) {
			this.repaymentOptions = repaymentOptions;
			this.repaymentOptions.setRetirementDetails(this);
		}
	}
 
	
	
}
