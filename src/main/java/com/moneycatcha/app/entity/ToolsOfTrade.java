package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.ConditionList;
import com.moneycatcha.app.model.ToolsOfTradeTypeList;

import lombok.Data;

@Data
@Entity
public class ToolsOfTrade {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    protected NonRealEstateAsset nonRealEstateAsset;

	@Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionList condition;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String make;

    @Column(columnDefinition = "VARCHAR(20)")
    protected String model;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ToolsOfTradeTypeList type;

	public ToolsOfTrade() {
		// JPA
	}

}