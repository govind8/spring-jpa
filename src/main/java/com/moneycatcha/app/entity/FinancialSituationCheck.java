package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class FinancialSituationCheck {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String dataSource;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String referenceNumber;

    public FinancialSituationCheck() {
    	// JPA
    }

}