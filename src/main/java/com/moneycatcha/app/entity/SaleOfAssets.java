package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class SaleOfAssets {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mitigant_id")
    protected Mitigant mitigant;

	@Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList lenderHeldSecurity;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String lenderHeldSecurityDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList otherAssets;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherAssetsDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList otherLenderHeldSecurity;

	@Column(columnDefinition = "VARCHAR(255)")
    protected String otherLenderHeldSecurityDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList shares;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String sharesDescription;

    public SaleOfAssets() {
    	// default JPA constructor
    }
    
}
