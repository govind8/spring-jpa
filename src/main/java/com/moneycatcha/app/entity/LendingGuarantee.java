package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.moneycatcha.app.model.LendingGuaranteeTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class LendingGuarantee {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected Application application;
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="lendingGuarantee", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<AssociatedLoanAccount> associatedLoanAccount;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="lendingGuarantee", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<Guarantor> guarantor;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy="lendingGuarantee", cascade=CascadeType.ALL,  orphanRemoval = true)
    protected List<Security> security;

    @Enumerated(EnumType.STRING)
    @Column(name = "lg_crossGuarantee", length = 3)
    protected YesNoList crossGuarantee;

    @Column(name = "lg_limit", precision=19, scale=2)
    protected BigDecimal limit;

    @Enumerated(EnumType.STRING)
    @Column(name = "lg_limited", length = 3)
    protected YesNoList limited;

    @Enumerated(EnumType.STRING)
    @Column(name = "lg_limitedToFacilityAmount", length = 3)
    protected YesNoList limitedToFacilityAmount;

    @Enumerated(EnumType.STRING)
    @Column(name = "lg_type", length = 60)
    protected LendingGuaranteeTypeList type;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    // Setters - AssociatedLoanAccount
    public void setDetail(List<AssociatedLoanAccount> associatedLoanAccount) {
    	if (associatedLoanAccount != null) {
	        this.associatedLoanAccount = associatedLoanAccount;    	
	     	this.associatedLoanAccount.forEach(x -> x.setLendingGuarantee(this));
    	}
    }    

    // Setters - Guarantor
    public void setGuarantor(List<Guarantor> guarantor) {
    	if (guarantor != null) {
	        this.guarantor = guarantor;    	
	        this.guarantor.forEach(x -> x.setLendingGuarantee(this));
    	}
    }    

    // Setters - Security
    public void setSecurity(List<Security> security) {
    	if (security != null) {
	        this.security = security;    	
	    	this.security.forEach(x -> x.setLendingGuarantee(this));
    	}
    }    
    
    public LendingGuarantee() {
    	// JPA
    }
    
 

}