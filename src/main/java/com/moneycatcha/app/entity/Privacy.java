package com.moneycatcha.app.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Privacy {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
	PersonApplicant personApplicant;
	
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowApplicationStatusUpdates;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowCreditBureauIdentityCheck;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowCreditCheck;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowDirectMarketing;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowElectronicIdentityCheck;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowTelemarketing;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList allowThirdPartyDisclosure;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList creditAuthoritySigned;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateSigned;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList privacyActConsentSigned;

}