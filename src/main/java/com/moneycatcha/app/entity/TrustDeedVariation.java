package com.moneycatcha.app.entity;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class TrustDeedVariation {
	
    @Id
    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
    
    @Column(columnDefinition = "DATE")
    protected LocalDate date;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;
    
    public TrustDeedVariation() {
    	//jPA
    }

}