package com.moneycatcha.app.entity;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class Publisher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
	Message message;
	

    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "phone_australianDialingCode")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "phone_countryCode")),
            @AttributeOverride(name = "number", column = @Column(name = "phone_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "phone_overseasDialingCode"))
        })
    protected PhoneType phoneNumber;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "publisher", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Publisher.RelatedSoftware> relatedSoftware;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "publisher", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Software software;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String companyName;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String contactName;

    @Column(columnDefinition = "VARCHAR(120)")
    protected String email;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lixiCode;

    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime publishedDateTime;

    @Data
    @Entity
    @Table(name = "related_software")
    public static class RelatedSoftware {

    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "publisher_id")
    	Publisher publisher;
    	
        @Column(columnDefinition = "VARCHAR(255)")
        protected String description;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String environment;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String lixiCode;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String name;

        @Column(columnDefinition = "VARCHAR(120)")
        protected String technicalEmail;

        @Column(columnDefinition = "VARCHAR(40)")
        protected String version;

        public RelatedSoftware() {
        	// JPA
        }
       
    }

    // Constructor
    public Publisher() {
    	// JPA
    }

	// Setter RelatedSoftware
	public void setRelatedSoftware(List<Publisher.RelatedSoftware> relatedSoftware) {
		if (relatedSoftware != null) {
			this.relatedSoftware = relatedSoftware;
			this.relatedSoftware.forEach(x -> x.setPublisher(this));
		}
	}

	// Setter - Software
	public void setSoftware(Software software) {
		if (software != null) {
			this.software = software;
			this.software.setPublisher(this);
		}
	}
        
}