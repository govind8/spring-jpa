package com.moneycatcha.app.entity;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Recipient {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
	Message message;
    
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "recipient", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Software software;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lixiCode;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String routingCode;

    public Recipient() {
    	// JPA
    }
  
    // Setter - Software
	public void setSoftware(Software software) {
		if (software != null) {
			this.software = software;
			this.software.setRecipient(this);
		}
	}

}