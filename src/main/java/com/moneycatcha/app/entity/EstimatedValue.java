package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.BalloonRVInputPatternList;
import com.moneycatcha.app.model.EstimateBasisNonRealEstateAssetList;
import com.moneycatcha.app.model.EstimateBasisRealEstateAssetList;
import com.moneycatcha.app.model.TaxDepreciationMethodList;

import lombok.Data;

@Data
@Entity
public class EstimatedValue {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    protected NonRealEstateAsset nonRealEstateAsset;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "real_estate_asset_id")
    protected RealEstateAsset realEstateAsset;
    
    @Column(precision=19, scale=2)
    protected BigDecimal balloonRVAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected BalloonRVInputPatternList balloonRVInputPattern;

    @Column(precision=19, scale=2)
    protected BigDecimal balloonRVPercent;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EstimateBasisNonRealEstateAssetList estimateBasisNonRealEstateAsset;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EstimateBasisRealEstateAssetList estimateBasisRealEstateAsset;
    
    @Column(precision=19, scale=2)
    protected BigDecimal estimatedCGTLiability;

    @Column(precision=19, scale=2)
    protected BigDecimal minimumResidualValue;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected TaxDepreciationMethodList taxDepreciationMethod;

    @Column(precision=19, scale=2)
    protected BigDecimal taxDepreciationRate;

    @Column(precision=19, scale=2)
    protected BigDecimal value;
    
    @Column(columnDefinition = "DATE")
    protected LocalDate valuedDate;

    //OneToOne(mappedBy = "", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xValuer;

    public EstimatedValue() {
    	// JPA
    }
  
}