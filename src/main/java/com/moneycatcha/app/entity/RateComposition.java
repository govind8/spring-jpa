package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class RateComposition {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distinct_loan_period_id", referencedColumnName = "uniqueid")
	DistinctLoanPeriod distinctLoanPeriod;	
    
    @Embedded
    protected BaseRate baseRate;

    @Column(precision=19, scale=2)
    protected BigDecimal customerRiskMargin;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList disclosedToCustomer;

    @Column(precision=19, scale=2)
    protected BigDecimal introductoryMargin;

    @Column(precision=19, scale=2)
    protected BigDecimal netCustomerRiskMargin;

    @Column(precision=19, scale=2)
    protected BigDecimal paymentTypeMargin;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String pricingConstruct;

    @Column(precision=19, scale=2)
    protected BigDecimal productMargin;

    @Column(precision=19, scale=2)
    protected BigDecimal termPremium;

    @Column(precision=19, scale=2)
    protected BigDecimal totalInterestRate;
    
    // Loan Details - fields
	@Column(precision=19, scale=2)
    protected BigDecimal introducerMargin;

    @Column(precision=19, scale=2)
    protected BigDecimal originatorMargin;


    @Column(precision=19, scale=2)
    protected BigDecimal subsidyBaseAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal subsidyBasePercent;

    @Column(precision=19, scale=2)
    protected BigDecimal subsidyRateSacrifice;


    @Data
    @Embeddable
    public static class BaseRate {

        @Column(name = "baserate_code", columnDefinition = "VARCHAR(80)")
        protected String code;

        @Column(name = "baserate_name", columnDefinition = "VARCHAR(80)")
        protected String name;

        @Column(name = "baserate_rate", precision=19, scale=2)
        protected BigDecimal rate;

    }
    
    public RateComposition() {
    	// JPA
    }

}