package com.moneycatcha.app.entity;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ProductSet {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
	
    @Embedded
    protected Contact contact;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "productSet", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Product> product;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String lodgementReferenceNumber;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPrimaryApplicant;

    @Data
    @Embeddable
    public static class Contact {
        @Column(name = "contact_x_mailing_address", columnDefinition = "VARCHAR(80)")
        protected String xMailingAddress;

    }

    @Data
    @Entity
    @Table(name = "product")
    public static class Product {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "product_set_id")
        protected ProductSet productSet;

        @Column(name = "product_x_financial_product",columnDefinition = "VARCHAR(80)")
        protected String xFinancialProduct;

    }

	public ProductSet() {
		// JPA
	}
    
	// Setter - Product
	public void setProduct(List<Product> product) {
		if (product != null) {
			this.product = product;
			this.product.forEach(x -> x.setProductSet(this));
		}
	}
    
}
