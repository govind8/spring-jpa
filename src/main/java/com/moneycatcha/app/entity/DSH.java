package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class DSH {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String assignmentCertificate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String entitlementCertificate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String fileNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList subsidised;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList supplement;
    
    public DSH() {
    	// JPA
    }
}
