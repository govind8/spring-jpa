package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.LoanPaymentScheduleTypeList;
import com.moneycatcha.app.model.PaymentTimingList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.RepaymentFrequencyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Repayment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "distinct_loan_period_id")
	DistinctLoanPeriod distinctLoanPeriod;	
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xContinuingRepayment;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xRepayment;
    
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "liability_id")
	Liability liability;	

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList interestPayment;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LoanPaymentScheduleTypeList loanPaymentScheduleType;

    @Column(precision=19, scale=2)
    protected BigDecimal minimumAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaymentTimingList paymentTiming;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaymentTypeList paymentType;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList principalPayment;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList regular;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger remainingRepayments;

    @Column(precision=19, scale=2)
    protected BigDecimal repaymentAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected RepaymentFrequencyList repaymentFrequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList taxDeductible;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    public Repayment() {
    	// JPA
    }
 

}
