package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.FinancialAssetTypeList;
import com.moneycatcha.app.model.ShareTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

	
@Data
@Entity
public class FinancialAsset {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    protected NonRealEstateAsset nonRealEstateAsset;
    
    @OneToOne(mappedBy = "financialAssset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;
    
    @Embedded
    protected Shares shares;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList transferOwnershipToSMSF;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FinancialAssetTypeList type;

    @Data
    @Embeddable
    public static class Shares {
        @Enumerated(EnumType.STRING)
        @Column(name = "shares_type", length = 60)
        protected ShareTypeList type;
        
        public Shares() {
        	//JPA
        }

    }

    // Setters - FinancialAccountType
    public void setAccountNumber(FinancialAccountType accountNumber) {
    	if (accountNumber != null) {
	     	accountNumber.setFinancialAssset(this);
	        this.accountNumber = accountNumber;    	
    	}
    }
    
    public FinancialAsset() {
    	// JPA
    }
    
    
}
