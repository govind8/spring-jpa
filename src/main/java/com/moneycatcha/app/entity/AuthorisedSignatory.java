package com.moneycatcha.app.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class AuthorisedSignatory implements Serializable {
	
	private static final long serialVersionUID = 5737460525142713998L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String positionTitle;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSignatory;
    

    public AuthorisedSignatory() {
    	// JPA
    }

    
   
    
 }
