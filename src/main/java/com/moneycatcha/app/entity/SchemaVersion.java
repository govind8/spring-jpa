package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class SchemaVersion {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
	Message message;
	
	@Column(columnDefinition = "VARCHAR(80)")
    protected String guidebookCode;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String guidebookName;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String guidebookVersion;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String lixiCustomVersion;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String lixiTransactionType;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String lixiVersion;

	public SchemaVersion() {
		//jPA
	}
}