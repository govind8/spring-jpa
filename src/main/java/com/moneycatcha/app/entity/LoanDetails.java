package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.FunderList;
import com.moneycatcha.app.model.InstalmentsFrequencyList;
import com.moneycatcha.app.model.InterestChargeFrequencyList;
import com.moneycatcha.app.model.InterestOnlyReasonList;
import com.moneycatcha.app.model.InterestTypeList;
import com.moneycatcha.app.model.LoanTermUnitsList;
import com.moneycatcha.app.model.LoanTypeList;
import com.moneycatcha.app.model.NccpStatusList;
import com.moneycatcha.app.model.OccupancyList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.RbaLendingPurposeList;
import com.moneycatcha.app.model.StatementCycleList;
import com.moneycatcha.app.model.TotalTermTypeList;
import com.moneycatcha.app.model.VaryOnValuationList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class LoanDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected AmountInForeignCurrencyType amountRequestedInForeignCurrency;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Borrowers borrowers;

	@Embedded
	protected BulkReduction bulkReduction;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Commission commission;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DiscountMargin discountMargin;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DocumentationInstructionsType documentationInstructions;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected DSH dsh;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected EquityRelease equityRelease;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FeaturesSelected featuresSelected;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<FundsDisbursementType> fundsDisbursement;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Guarantor> guarantor;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<LendingPurpose> lendingPurpose;

	@Embedded
    protected LoanPurpose loanPurpose;

	@Embedded
    protected Package _package;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ProposedRepayment proposedRepayment;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<RateComposition> rateComposition;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Security> security;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Software software;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected StatementInstructions statementInstructions;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<SupplementaryCardholder> supplementaryCardholder;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Term term;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "loanDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<TermsAndConditions> termsAndConditions;

    @Column(precision=19, scale=2)
    protected BigDecimal accelerationPercentage;

    @Column(precision=19, scale=2)
    protected BigDecimal amountRequested;

    @Column(precision=19, scale=2)
    protected BigDecimal amountRequestedInclusive;

    @Column(precision=19, scale=2)
    protected BigDecimal balloonRepaymentAmount;

    @Column(columnDefinition = "DATE")
    protected LocalDate balloonRepaymentDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList buyNowPayLater;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList considerLowerLimitIfNotEligible;

    @Column(columnDefinition = "DATE")
    protected LocalDate estimatedSettlementDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FunderList funder;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected InterestChargeFrequencyList interestCalculationFrequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected InterestChargeFrequencyList interestChargeFrequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isAmountRequestedInForeignCurrency;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LoanTypeList loanType;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList mainProduct;

    @Column(columnDefinition = "DATE")
    protected LocalDate maturityDate;

    @Column(precision=19, scale=2)
    protected BigDecimal minimumRepaymentRate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList negativelyGeared;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList nominateBalloonRepayment;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String originatorReferenceID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String productName;

    @Column(precision=19, scale=2)
    protected BigDecimal proposedAnnualInterestRate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList secured;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList smsfLoan;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String specialConcessionCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected StatementCycleList statementCycle;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList taxDeductible;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String templateID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xMasterAgreement;

    // Setters - AmountInForeignCurrencyType
    public void setAmountRequestedInForeignCurrency(AmountInForeignCurrencyType amountRequestedInForeignCurrency) {
    	if (amountRequestedInForeignCurrency != null) {
	        this.amountRequestedInForeignCurrency = amountRequestedInForeignCurrency;    	
	    	this.amountRequestedInForeignCurrency.setLoanDetails(this);
    	}
    }	
	
    // Setters - Borrowers
    public void setBorrowers(Borrowers borrowers) {
    	if (borrowers != null) {
	        this.borrowers = borrowers;    	
	    	this.borrowers.setLoanDetails(this);
    	}
    }	
    
    // Setters - Commission
    public void setCommission(Commission commission) {
    	if (commission != null) {
	        this.commission = commission;    	
	    	this.commission.setLoanDetails(this);
    	}
    }
    
    
    // Setters - DiscountMargin
    public void setDiscountMargin(DiscountMargin discountMargin) {
    	if (discountMargin != null) {
	        this.discountMargin = discountMargin;    	
	    	this.discountMargin.setLoanDetails(this);
    	}
    }	

    // Setters - DocumentationInstructions
    public void setDocumentationInstructions(DocumentationInstructionsType documentationInstructions) {
    	if (documentationInstructions != null) {
	        this.documentationInstructions = documentationInstructions;    	
	    	this.documentationInstructions.setLoanDetails(this);
    	}
    }	    
    
    // Setters - DSH
    public void setDSH(DSH dsh) {
    	if (dsh != null) {
	        this.dsh = dsh;    	
	    	dsh.setLoanDetails(this);
    	}
    }	
    
    // Setters - EquityRelease
    public void setEquityRelease(EquityRelease equityRelease) {
    	if (equityRelease != null) {
	        this.equityRelease = equityRelease;    	
	    	this.equityRelease.setLoanDetails(this);
    	}
    }	
    

    // Setters - FeaturesSelected
    public void setFeaturesSelected(FeaturesSelected featuresSelected) {
    	if (featuresSelected != null) {
	        this.featuresSelected = featuresSelected;    	
	    	this.featuresSelected.setLoanDetails(this);
    	}
    }	
    
    // Setters - FundsDisbursementType
    public void setFundsDisbursementType(List<FundsDisbursementType> fundsDisbursement) {
    	if (fundsDisbursement != null) {
	        this.fundsDisbursement = fundsDisbursement;    	
	    	this.fundsDisbursement.forEach(x -> x.setLoanDetails(this));
    	}
    }	
 
    // Setters - Guarantor
    public void setGuarantor(List<Guarantor> guarantor) {
    	if (guarantor != null) {
	        this.guarantor = guarantor;    	
	    	this.guarantor.forEach(x -> x.setLoanDetails(this));
    	}
    }	
    
    // Setters - LendingPurpose
    public void setLendingPurpose(List<LendingPurpose> lendingPurpose) {
    	if (lendingPurpose != null) {
	        this.lendingPurpose = lendingPurpose;    	
	    	this.lendingPurpose.forEach(x -> x.setLoanDetails(this));
    	}
    }	
    
    // Setters - ProposedRepayment
    public void setProposedRepayment(ProposedRepayment proposedRepayment) {
    	if (proposedRepayment != null) {
	        this.proposedRepayment = proposedRepayment;    	
	    	this.proposedRepayment.setLoanDetails(this);
    	}
    }	
    
    // Setters - RateComposition
    public void setRateComposition(List<RateComposition> rateComposition) {
    	if (rateComposition != null) {
	        this.rateComposition = rateComposition;    	
	    	this.rateComposition.forEach(x -> x.setLoanDetails(this));
    	}
    }	

    // Setters - Security
    public void setSecurity(List<Security> security) {
    	if (security != null) {
	        this.security = security;    	
	    	this.security.forEach(x -> x.setLoanDetails(this));
    	}
    }	
    
    
    // Setters - Software
    public void setSoftware(Software software) {
    	if (software != null) {
	         this.software = software;    	
	         this.software.setLoanDetails(this);
    	}
    }	
   
    // Setters - StatementInstructions
    public void setStatementInstructions(StatementInstructions statementInstructions) {
    	if (statementInstructions != null) {
	        this.statementInstructions = statementInstructions;    	
	    	this.statementInstructions.setLoanDetails(this);
    	}
    }	
    
    // Setters - SupplementaryCardholder
    public void setSupplementaryCardholder( List<SupplementaryCardholder> supplementaryCardholder) {
    	if (supplementaryCardholder != null) {
	        this.supplementaryCardholder = supplementaryCardholder;    	
	    	this.supplementaryCardholder.forEach(x -> x.setLoanDetails(this));
    	}
    }	
    
    // Setters - Term
    public void setTerm( Term term) {
    	if (term != null) {
	        this.term = term;    	
	    	term.setLoanDetails(this);
    	}
    }	
    
    // Setters - TermsAndConditions
    public void setTermsAndConditions(List<TermsAndConditions> termsAndConditions) {
    	if (termsAndConditions != null) {
	        this.termsAndConditions = termsAndConditions;    	
	    	termsAndConditions.forEach(x -> x.setLoanDetails(this));
    	}
    }	    
	// constructor
    public LoanDetails() {
    	// JPA
    }
    

	@Data
    @Entity
    @Table(name = "borrowers")
    public static class Borrowers {
 
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn
    	LoanDetails loanDetails;
    	
    	@OneToMany(mappedBy = "borrowers", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<Owner> owner;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected ProportionsList proportions;
        
        public Borrowers() {
        	// JPA
        }
        
    }


    @Data
    @Embeddable
    public static class BulkReduction {
        @Column(name = "bulkreduction_amount", precision=19, scale=2)
        protected BigDecimal amount;

        @Column(name = "bulkreduction_estimate_date", columnDefinition = "DATE")
        protected LocalDate estimatedDate;
        
        public BulkReduction() {
        	// JPA
        }

    }

    @Data
    @Entity
    @Table(name = "loan_details_guarantor")
    public static class Guarantor {
    	
       	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name="loan_details_id")
    	LoanDetails loanDetails;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xGuarantor;

    }
    
    
    @Data
    @Entity
    @Table(name = "equity_release")
    public static class EquityRelease {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name="loan_details_id")
    	LoanDetails loanDetails;
    	
    	@Embedded
        protected AccommodationBond accommodationBond;

        @Embedded
        protected Amount amount;

        @Embedded
        protected Instalments instalments;

        @Enumerated(EnumType.STRING)
        @Column(name="er_protectedEquity", length = 3)
        protected YesNoList protectedEquity;

        @Column(name="er_protectedEquityPercentage", precision=19, scale=2)
        protected BigDecimal protectedEquityPercentage;

        public EquityRelease() {
        	// JPA
        }

		@Data
        @Embeddable
        public static class AccommodationBond {
			
            @Column(name="ab_expectedAdmissionDate", columnDefinition = "DATE")
            protected LocalDate expectedAdmissionDate;

            @Column(name="ab_serviceProviderName", columnDefinition = "VARCHAR(80)")
            protected String serviceProviderName;

            @Column(name="ab_serviceProviderNumber", columnDefinition = "VARCHAR(80)")
            protected String serviceProviderNumber;

            public AccommodationBond() {
            	// JPA
            }
            
        }


        @Data
        @Embeddable
        public static class Amount {
            @Column(name = "amount_calculateAsPercentage", precision=19, scale=2)
            protected BigDecimal calculateAsPercentage;

            @Column(name = "amount_cashReserve", precision=19, scale=2)
            protected BigDecimal cashReserve;

            @Column(name = "amount_instalmentsAmount", precision=19, scale=2)
            protected BigDecimal instalmentsAmount;

            @Column(name = "amount_lumpSum", precision=19, scale=2)
            protected BigDecimal lumpSum;

            @Enumerated(EnumType.STRING)
            @Column(name = "amount_varyOnValuation", length = 60)
            protected VaryOnValuationList varyOnValuation;

            public Amount() {
            	// JPA 
            }

        }


        @Data
        @Embeddable
        public static class Instalments {
            @Column(name = "inst_amountPerInstalment", precision=19, scale=2)
            protected BigDecimal amountPerInstalment;

            @Enumerated(EnumType.STRING)
            @Column(name = "inst_frequency", length = 15)
            protected InstalmentsFrequencyList frequency;

            @Enumerated(EnumType.STRING)
            @Column(name = "inst_indexed", length = 3)
            protected YesNoList indexed;

            @Column(name = "instalment_indexRate", precision=19, scale=2)
            protected BigDecimal indexRate;

            @Column(name = "inst_numberOfInstalments", columnDefinition = "BIGINT")
            protected BigInteger numberOfInstalments;
            
            public Instalments() {
            	// JPA
            }

        }
    }


    @Data
    @Embeddable
    public static class LoanPurpose {

        @Enumerated(EnumType.STRING)
        @Column(name="lp_nccpStatus", length = 60)
        protected NccpStatusList nccpStatus;

        @Enumerated(EnumType.STRING)
        @Column(name="lp_occupancy", length = 60)
        protected OccupancyList occupancy;

        @Enumerated(EnumType.STRING)
        @Column(name="lp_ownerBuilderApplication", length = 3)
        protected YesNoList ownerBuilderApplication;

        @Enumerated(EnumType.STRING)
        @Column(name="lp_primaryPurpose", length = 60)
        protected PrimaryPurposeLoanPurposeList primaryPurpose;
        
      
        @Enumerated(EnumType.STRING)
        @Column(name="lp_rbaLendingPurpose", length = 100)        
        protected RbaLendingPurposeList rbaLendingPurpose;
        
        public LoanPurpose() {
        	// JPA
        }

    }

    @Data
    @Embeddable
    public static class Package {

        @Column(name="pkg_category", columnDefinition = "VARCHAR(80)")
        protected String category;

        @Column(name="pkg_code", columnDefinition = "VARCHAR(80)")
        protected String code;

        @Column(name="pkg_memberID", columnDefinition = "VARCHAR(80)")
        protected String memberID;

        @Column(name="pkg_name", columnDefinition = "VARCHAR(80)")
        protected String name;

        @Column(name="pkg_optionCode", columnDefinition = "VARCHAR(80)")
        protected String optionCode;

        @Column(name="pkg_organisation", columnDefinition = "VARCHAR(255)")
        protected String organisation;

        public Package() {
        	// JPA
        }
 
    }


    @Data
    @Entity
    @Table(name = "statement_instructions")
    public static class StatementInstructions {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn
        LoanDetails loanDetails;
        
        @OneToMany(mappedBy = "statementInstructions", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<NameOnStatement> nameOnStatement;
        
        public StatementInstructions() {
        	// JPA
        }
        //
        public void setNameOnStatement(List<NameOnStatement> nameOnStatement) {
        	if (nameOnStatement != null) {
				this.nameOnStatement = nameOnStatement;
				this.nameOnStatement.forEach(x -> x.setStatementInstructions(this));
        	}
		}


		@Data
        @Entity
        @Table(name = "name_on_statement")
        public static class NameOnStatement {
        	
        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
            @OneToOne(fetch = FetchType.LAZY)
            @JoinColumn
            StatementInstructions statementInstructions;
            
            @Column(columnDefinition = "VARCHAR(80)")
            protected String xApplicant;

            public NameOnStatement() {
            	// JPA
            }

		}
    }


    @Data
    @Entity
    @Table(name = "term")
    public static class Term {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @OneToOne(fetch = FetchType.LAZY)
        @JoinColumn
        LoanDetails loanDetails;
        
        @OneToMany(mappedBy = "term", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<DistinctLoanPeriod> distinctLoanPeriod;
    	
        @OneToMany(mappedBy = "term", cascade = CascadeType.ALL, orphanRemoval = true)
        protected List<InterestOnlyReason> interestOnlyReason;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected InterestTypeList interestType;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger interestTypeDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList interestTypeUnits;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected PaymentTypeList paymentType;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger paymentTypeDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList paymentTypeUnits;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger rolloverPeriodDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList rolloverPeriodUnits;

        @Column(precision=19, scale=2)
        protected BigDecimal totalFeesAmount;

        @Column(precision=19, scale=2)
        protected BigDecimal totalInterestAmount;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger totalRepayments;

        @Column(precision=19, scale=2)
        protected BigDecimal totalRepaymentsAmount;

        @Column(columnDefinition = "BIGINT")
        protected BigInteger totalTermDuration;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected TotalTermTypeList totalTermType;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected LoanTermUnitsList totalTermUnits;

        public Term() {
        	// JPA 
        }

		@Data
        @Entity
        @Table(name = "interest_only_reason")
        public static class InterestOnlyReason {
        	
        	@Id
        	@GeneratedValue(strategy = GenerationType.IDENTITY)
            protected Long Id;
        	
            @ManyToOne(fetch = FetchType.LAZY)
            @JoinColumn(name = "term_id")
            Term term;
            
            @Column(columnDefinition = "VARCHAR(255)")
            protected String description;

            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected InterestOnlyReasonList reason;

            @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
            protected String uniqueID;

            public InterestOnlyReason() {
            	// JPA
            }

        }
    

		// setter - setDistinctLoanPeriod
        public void setDistinctLoanPeriod(List<DistinctLoanPeriod> distinctLoanPeriod) {
        	if (distinctLoanPeriod != null) {
				this.distinctLoanPeriod = distinctLoanPeriod;
				this.distinctLoanPeriod.forEach(x -> x.setTerm(this));
        	}
		}

		// setter - setInterestOnlyReason
        public void setInterestOnlyReason(List<InterestOnlyReason> interestOnlyReason) {
        	if (interestOnlyReason != null) {
				this.interestOnlyReason = interestOnlyReason;
				this.interestOnlyReason.forEach(x -> x.setTerm(this));
        	}
		}

		
    }

 }