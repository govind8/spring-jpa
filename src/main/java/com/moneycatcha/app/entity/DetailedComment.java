package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class DetailedComment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;
	
    @Column(columnDefinition = "VARCHAR(255)")
    protected String comment;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String contextDescription;

    @Column(columnDefinition = "DATE")
    protected LocalDate createdDate;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAuthor;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xContext;

    public DetailedComment() {
    	// jPA
    }

    
}
