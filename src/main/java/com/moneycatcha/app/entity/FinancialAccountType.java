package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;
    
@Data
@Entity
public class FinancialAccountType {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "existing_customer_id")
	protected ExistingCustomer existingCustomer;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "financial_asset_id")
	protected FinancialAsset financialAssset;    
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "funds_disbursement_type_id")
	FundsDisbursementType fundsDisbursementType;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fee_id")
	Fee fee;
  	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "liability_id")
	protected Liability liability;    

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "offset_account_id")
	protected OffsetAccount offsetAccount;    
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "proposed_repayment_id")
	protected ProposedRepayment proposedRepayment;    
	
	@Embedded
    protected BranchDomicile branchDomicile;
    
	@Column(columnDefinition = "VARCHAR(80)")
    protected String accountName;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String accountNumber;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String accountTypeName;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String bsb;

	@Column(columnDefinition = "VARCHAR(150)")
    protected String financialInstitution;

	@Column(columnDefinition = "VARCHAR(150)")
    protected String otherFIName;

	public FinancialAccountType() {
		// JPA
	}

	@Data
    @Embeddable
    public static class BranchDomicile {
    	
    	@Column(columnDefinition = "VARCHAR(80)")
        protected String internalName;

    	@Column(columnDefinition = "VARCHAR(80)")
        protected String internalNumber;
    	
    	public BranchDomicile() {
    		// JPA
    	}

     }
}
