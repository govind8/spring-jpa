package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;


@Data
@Entity
public class RefinancingAndConsolidation {


	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;   
	
	@Embedded
    protected RefinancingReason refinancingReason;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String detailsOfCreditCardPlan;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList explainedIncreaseInterestRisk;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList explainedIncreaseLoanTermRisk;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList planToCloseOrReduceCreditCard;

    public  RefinancingAndConsolidation() {
    	// JPA
    }
    
    @Data
    @Embeddable
    public static class RefinancingReason {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "rr_closeToEndOfCurrentLoanTerm", length = 3)
        protected YesNoList closeToEndOfCurrentLoanTerm;

        @Column(name = "rr_closeToEndOfCurrentLoanTermDetails", columnDefinition = "VARCHAR(255)")
        protected String closeToEndOfCurrentLoanTermDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_debtConsolidation", length = 3)
        protected YesNoList debtConsolidation;

        @Column(name = "rr_debtConsolidationDetails", columnDefinition = "VARCHAR(255)")
        protected String debtConsolidationDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_dissatisfactionWithCurrentLender", length = 3)
        protected YesNoList dissatisfactionWithCurrentLender;

        @Column(name = "rr_dissatisfactionWithCurrentLenderDetails", columnDefinition = "VARCHAR(255)")
        protected String dissatisfactionWithCurrentLenderDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_greaterFlexibility", length = 3)
        protected YesNoList greaterFlexibility;

        @Column(name = "rr_greaterFlexibilityDetails", columnDefinition = "VARCHAR(255)")
        protected String greaterFlexibilityDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_increaseTotalLoanAmount", length = 3)
        protected YesNoList increaseTotalLoanAmount;

        @Column(name = "rr_", columnDefinition = "VARCHAR(255)")
        protected String increaseTotalLoanAmountDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_lowerInterestRate", length = 3)
        protected YesNoList lowerInterestRate;

        @Column(name = "rr_lowerInterestRateDetails", columnDefinition = "VARCHAR(255)")
        protected String lowerInterestRateDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_other", length = 3)
        protected YesNoList other;

        @Column(name = "rr_otherDetails", columnDefinition = "VARCHAR(255)")
        protected String otherDetails;

        @Enumerated(EnumType.STRING)
        @Column(name = "rr_reducedRepayments", length = 3)
        protected YesNoList reducedRepayments;

        @Column(name = "rr_reducedRepaymentsDetails", columnDefinition = "VARCHAR(255)")
        protected String reducedRepaymentsDetails;

        public RefinancingReason() {
        	// jPA
        }
    }
    
}