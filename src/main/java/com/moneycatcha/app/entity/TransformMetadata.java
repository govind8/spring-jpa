
package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class TransformMetadata {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
	Message message;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "transformMetadata", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Identifier> identifier;

    @Data
    @Entity
    @Table(name = "identifier")
    public static class Identifier {
    	
    	@Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
    	@ManyToOne(fetch = FetchType.LAZY)
    	@JoinColumn(name = "transform_metadata_id")
    	TransformMetadata transformMetadata;
    	
        @Column(columnDefinition = "VARCHAR(80)")
        protected String attributeName;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String externalID;

        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xContext;
        
        // Constructors
        public Identifier() {
        	// JPA
        }

    }
    
    // Constructors
    public TransformMetadata() {
    	// JPA
    }

	// Setters - Identifier
	public void setIdentifier(List<Identifier> identifier) {
		if (identifier != null) {
			this.identifier = identifier;
			this.identifier.forEach(x -> x.setTransformMetadata(this));
		}
	}
    
}