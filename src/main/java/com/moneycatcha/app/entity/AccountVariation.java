package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.ReleaseSecurityReasonList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity

public class AccountVariation {
	
    @Embedded
    protected AddBorrower addBorrower;
    @Embedded
    protected AddGuarantee addGuarantee;
    @Embedded
    protected AddGuaranteeSecurity addGuaranteeSecurity;
    @Embedded
    protected AddGuarantor addGuarantor;
    @Embedded
    protected AddSecurity addSecurity;
    @Embedded
    protected BalanceIncrease balanceIncrease;
    @Embedded
    protected ChangeLoanTerm changeLoanTerm;
    @Embedded
    protected ChangeRepaymentAmount changeRepaymentAmount;
    @Embedded
    protected ChangeRepaymentFrequency changeRepaymentFrequency;
    @Embedded
    protected ChangeRepaymentType changeRepaymentType;
    @Embedded
    protected CloseAccount closeAccount;
    @Embedded
    protected InterestRateDiscount interestRateDiscount;
    @Embedded
    protected LendingGuaranteeLimitDecrease lendingGuaranteeLimitDecrease;
    @Embedded
    protected LendingGuaranteeLimitIncrease lendingGuaranteeLimitIncrease;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="accountVariation", cascade=CascadeType.ALL, orphanRemoval = true)
	protected LimitIncrease limitIncrease;
	
    @Embedded
    protected ReduceBalance reduceBalance;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy="accountVariation", cascade=CascadeType.ALL, orphanRemoval = true)
    protected ReduceLimit reduceLimit;
	
    @Embedded
    protected ReleaseGuarantee releaseGuarantee;
    @Embedded
    protected ReleaseGuaranteeSecurity releaseGuaranteeSecurity;
    @Embedded
    protected ReleaseGuarantor releaseGuarantor;
    @Embedded
    protected ReleaseSecurity releaseSecurity;
    @Embedded
    protected RemoveBorrower removeBorrower;
    @Embedded
    protected SplitAccount splitAccount;
    @Embedded
    protected SwitchProduct switchProduct;
    @Embedded
    protected UpdatePartyDetails updatePartyDetails;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "av_variation_desc", columnDefinition = "VARCHAR(80)")
    protected String variationDescription;
    
    @Column(name = "av_xaccounttovary", columnDefinition = "VARCHAR(80)")
    protected String xAccountToVary;
    
    @Column(name = "av_xlendingguaranteetovary", columnDefinition = "VARCHAR(80)")
    protected String xLendingGuaranteeToVary;
    
    @Column(name = "av_xpostvariationaccount", columnDefinition = "VARCHAR(80)")
    protected String xPostVariationAccount;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "application_id")
    Application application;
    
    public AccountVariation() {
    	//JPA
    }

    @Data
    @Embeddable
    public static class AddBorrower {
    	
        @Column(name = "ad_xborrower", columnDefinition = "VARCHAR(80)")
        protected String xBorrower;
        
    }
    
    @Data
    @Embeddable
    public static class AddGuarantee {
    	
        @Column(name = "ag_xlendingguarantee", columnDefinition = "VARCHAR(80)")
        protected String xLendingGuarantee;
        
    }
    
    @Data
    @Embeddable
    public static class AddGuaranteeSecurity {
    	
        @Column(name = "ads_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        @Column(name = "ags_xsecurity", columnDefinition = "VARCHAR(80)")
        protected String xSecurity;
        
    }
    
    @Data
    @Embeddable
    public static class AddGuarantor {
        @Column(name = "ag_xguarantor", columnDefinition = "VARCHAR(80)")
        protected String xGuarantor;
    }
    
    @Data
    @Embeddable    
    public static class AddSecurity {
    	
        @Column(name = "as_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        @Column(name = "as_xsecurity", columnDefinition = "VARCHAR(80)")
        protected String xSecurity;
        
    }

    @Data
    @Embeddable    
    public static class BalanceIncrease {
    	
        @Column(name = "bi_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        @Column(name = "bi_increase_amount", precision=19, scale=2) 
        protected BigDecimal increaseAmount;
        @Column(name = "bi_new_balance",precision=19, scale=2) 
        protected BigDecimal newBalance;
    }
    
    @Data
    @Embeddable
    public static class ChangeLoanTerm {
    	
        @Column(name = "clt_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Column(name = "clt_new_fixed_loan_term", precision=19, scale=2) 
        protected BigInteger newFixedLoanTerm;
        
        @Column(name = "clt_new_total_loan_term", precision=19, scale=2) 
        protected BigInteger newTotalLoanTerm;
        
    }
    
    @Data
    @Embeddable
    public static class ChangeRepaymentAmount {
    	
        @Column(name = "cra_repayment_amount", precision=19, scale=2) 
        protected BigDecimal repaymentAmount;
        
    }

    @Data
    @Embeddable
    public static class ChangeRepaymentFrequency {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "crf_repayment_frequency", length = 15)    	
        protected FrequencyFullList repaymentFrequency;
        
    }

    @Data 
    @Embeddable
    public static class ChangeRepaymentType {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "crf_repayment_type", length = 25)    	
        protected PaymentTypeList repaymentType;
        
    }

    @Data 
    @Embeddable
    public static class CloseAccount {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "ca_destroy_card", length = 3)    	
        protected YesNoList destroyCard;
        
        @Column(name = "ca_payout_amount", precision=19, scale=2) 
        protected BigDecimal payoutAmount;
        
    }

    @Data 
    @Embeddable
    public static class InterestRateDiscount {
    	
        @Column(name = "ird_discount_rate", precision=19, scale=2) 
        protected BigDecimal discountRate;
        
    }

    @Data 
    @Embeddable
    public static class LendingGuaranteeLimitDecrease {
    	
        @Column(name = "lgld_decrease_amount", precision=19, scale=2) 
        protected BigDecimal decreaseAmount;
        
        @Column(name = "lgld_newlimit", precision=19, scale=2) 
        protected BigDecimal newLimit;
        
    }

    @Data 
    @Embeddable
    public static class LendingGuaranteeLimitIncrease {
    	
        @Column(name = "lgli_increase_amount", precision=19, scale=2) 
        protected BigDecimal increaseAmount;
        
        @Column(name = "lgli_new_limit", precision=19, scale=2) 
        protected BigDecimal newLimit;
        
    }

	@Data
	@Entity
	@Table(name = "account_variation_limit_increase")
    public static class LimitIncrease {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
       	@OneToOne(fetch = FetchType.LAZY)
       	@JoinColumn(name = "account_variation_id")
        protected AccountVariation accountVariation;

    	@OneToOne(mappedBy = "limitIncrease", cascade = CascadeType.ALL, orphanRemoval = true)
        protected AmountInForeignCurrencyType newLimitRequestedInForeignCurrency;
        
        @Column(columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Column(precision=15, scale=2) 
        protected BigDecimal increaseAmount;
        
        @Enumerated(EnumType.STRING)
        @Column(length = 3) 
        protected YesNoList isNewLimitRequestedInForeignCurrency;
        
        @Column(precision=15, scale=2) 
        protected BigDecimal newLimit;

		public void setNewLimitRequestedInForeignCurrency(AmountInForeignCurrencyType newLimitRequestedInForeignCurrency) {
			newLimitRequestedInForeignCurrency.setLimitIncrease(this);
			this.newLimitRequestedInForeignCurrency = newLimitRequestedInForeignCurrency;
		}
        
    }
	
    @Data 
    @Embeddable
    public static class ReduceBalance {
    	
        @Column(name = "rb_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Column(name = "rb_new_balance", precision=19, scale=2) 
        protected BigDecimal newBalance;
        
        @Column(name = "rb_reduction_amount", precision=19, scale=2) 
        protected BigDecimal reductionAmount;
        
    }
    
    @Data 
    @Entity
	@Table(name = "account_variation_reduce_limit")
    public static class ReduceLimit {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
       	@OneToOne(fetch = FetchType.LAZY)
       	@JoinColumn(name = "account_variation_id")
        protected AccountVariation accountVariation;

    	@OneToOne(mappedBy = "reduceLimit", cascade = CascadeType.ALL, orphanRemoval = true)
        protected AmountInForeignCurrencyType newLimitRequestedInForeignCurrency;
        
	
        @Enumerated(EnumType.STRING)
        @Column(length = 3) 
        protected YesNoList isNewLimitRequestedInForeignCurrency;
        
        @Column(precision=15, scale=2) 
        protected BigDecimal limitReduction;
        
        @Column(precision=15, scale=2) 
        protected BigDecimal newLimit;

		public void setNewLimitRequestedInForeignCurrency(AmountInForeignCurrencyType newLimitRequestedInForeignCurrency) {
			newLimitRequestedInForeignCurrency.setReduceLimit(this);
			this.newLimitRequestedInForeignCurrency = newLimitRequestedInForeignCurrency;
		}
       
        
    }

    @Data 
    @Embeddable
    public static class ReleaseGuarantee {
    	
        @Column(name = "rg_xlending_guarantee", columnDefinition = "VARCHAR(80)")
        protected String xLendingGuarantee;
        
    }

    @Data 
    @Embeddable
    public static class ReleaseGuaranteeSecurity {
    	
        @Column(name = "rgs_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Column(name = "rgs_reason", columnDefinition = "VARCHAR(60)")
        protected ReleaseSecurityReasonList reason;
        
        @Column(name = "rgs_xsecurity", columnDefinition = "VARCHAR(80)")
        protected String xSecurity;
        
    }

    @Data 
    @Embeddable
    public static class ReleaseGuarantor {
    	
        @Column(name = "rg_xguarantor", columnDefinition = "VARCHAR(80)")
        protected String xGuarantor;
        
    }

    @Data 
    @Embeddable
    public static class ReleaseSecurity {
    	
        @Column(name = "rs_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Column(name = "rs_reason", columnDefinition = "VARCHAR(80)")
        protected ReleaseSecurityReasonList reason;
        
        @Column(name = "rs_security", columnDefinition = "VARCHAR(80)")
        protected String xSecurity;
        
    }

    @Data 
    @Embeddable
    public static class RemoveBorrower {
    	
        @Column(name="rb_xborrower", columnDefinition = "VARCHAR(80)")
        protected String xBorrower;
        
    }

	@Data
	@Embeddable
    public static class SplitAccount {
		
		@Embedded
        protected List<SplitAccount.Account> account = new ArrayList<>();
        
        @Column(name = "sa_action_date", columnDefinition = "DATE")
        protected LocalDate actionDate;
        
        @Data
        @Embeddable
        public static class Account {
        	
            @Column(name = "acc_xaccount_details", columnDefinition = "VARCHAR(80)")
            protected String xAccountDetails; //LoanDetails
            
        }
    }
	
    @Data 
    @Embeddable
    public static class SwitchProduct {
		
        @Column(name = "sp_xproduct_details", columnDefinition = "VARCHAR(80)")
        protected String xProductDetails;
        
    }
	
    @Data 
    @Embeddable
    public static class UpdatePartyDetails {
    	
        @Column(name = "upd_xLoan_Party", columnDefinition = "VARCHAR(80)")
        protected String xLoanParty;
        
    }
     
    // Setters - LimitIncrease
	public void setLimitIncrease(LimitIncrease limitIncrease) {
		if(limitIncrease != null) {
			this.limitIncrease = limitIncrease;
			this.limitIncrease.setAccountVariation(this);
		}
	}

    // Setters - ReduceLimit
	public void setReduceLimit(ReduceLimit reduceLimit) {
		if(reduceLimit != null) {
			this.reduceLimit = reduceLimit;
			this.reduceLimit.setAccountVariation(this);
		}
	}

    @java.lang.Override
    public java.lang.String toString() {
        return "AccountVariation{" +
                "addBorrower=" + addBorrower +
                ", addGuarantee=" + addGuarantee +
                ", addGuaranteeSecurity=" + addGuaranteeSecurity +
                ", addGuarantor=" + addGuarantor +
                ", addSecurity=" + addSecurity +
                ", balanceIncrease=" + balanceIncrease +
                ", changeLoanTerm=" + changeLoanTerm +
                ", changeRepaymentAmount=" + changeRepaymentAmount +
                ", changeRepaymentFrequency=" + changeRepaymentFrequency +
                ", changeRepaymentType=" + changeRepaymentType +
                ", closeAccount=" + closeAccount +
                ", interestRateDiscount=" + interestRateDiscount +
                ", lendingGuaranteeLimitDecrease=" + lendingGuaranteeLimitDecrease +
                ", lendingGuaranteeLimitIncrease=" + lendingGuaranteeLimitIncrease +
                ", limitIncrease=" + limitIncrease +
                ", reduceBalance=" + reduceBalance +
                ", reduceLimit=" + reduceLimit +
                ", releaseGuarantee=" + releaseGuarantee +
                ", releaseGuaranteeSecurity=" + releaseGuaranteeSecurity +
                ", releaseGuarantor=" + releaseGuarantor +
                ", releaseSecurity=" + releaseSecurity +
                ", removeBorrower=" + removeBorrower +
                ", splitAccount=" + splitAccount +
                ", switchProduct=" + switchProduct +
                ", updatePartyDetails=" + updatePartyDetails +
                ", Id=" + Id +
                ", uniqueID='" + uniqueID + '\'' +
                ", variationDescription='" + variationDescription + '\'' +
                ", xAccountToVary='" + xAccountToVary + '\'' +
                ", xLendingGuaranteeToVary='" + xLendingGuaranteeToVary + '\'' +
                ", xPostVariationAccount='" + xPostVariationAccount + '\'' +
                ", application=" + application +
                '}';
    }
}
