package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.AssetTransactionList;
import com.moneycatcha.app.model.CommercialTypeList;
import com.moneycatcha.app.model.DataSourceList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.HoldingList;
import com.moneycatcha.app.model.IndustrialTypeList;
import com.moneycatcha.app.model.OccupancyList;
import com.moneycatcha.app.model.PrimaryPurposeRealEstateAssetList;
import com.moneycatcha.app.model.PrimaryUsageList;
import com.moneycatcha.app.model.RealEstateAssetStatusList;
import com.moneycatcha.app.model.ResidentialTypeList;
import com.moneycatcha.app.model.RuralUsageList;
import com.moneycatcha.app.model.ValuationProgramList;
import com.moneycatcha.app.model.VisitContactTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class RealEstateAsset {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;

	@Embedded
    protected Commercial commercial;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ConstructionDetails constructionDetails;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ContractDetails contractDetails;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Encumbrance> encumbrance;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected EstimatedValue estimatedValue;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<FundsDisbursementType> fundsDisbursement;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<FutureRentalIncome> futureRentalIncome;

	@Embedded
    protected HeritageListing heritageListing;

	@Embedded
    protected Industrial industrial;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Insurance> insurance;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PercentOwnedType percentOwned;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PropertyExpense propertyExpense;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<PropertyFeatures> propertyFeatures;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PropertyType propertyType;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<RentalIncome> rentalIncome;

	@Embedded
    protected Representation representation;

	@Embedded
    protected Residential residential;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected RestrictionOnUseOfLand restrictionOnUseOfLand;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Rural rural;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Title> title;

	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "realEstateAsset", cascade = CascadeType.ALL, orphanRemoval = true)
    protected VisitContact visitContact;

	@Embedded
    protected Zoning zoning;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList approvalInPrinciple;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList construction;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList contractOfSale;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected DataSourceList dataSource;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList encumbered;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected HoldingList holding;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList innerCity;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList investmentPropertyLetter;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList legalRepresentation;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList multipleDwellings;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String nrasConsortium;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList nrasProperty;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OccupancyList occupancy;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList onMarketTransaction;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String primaryLandUse;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PrimaryPurposeRealEstateAssetList primaryPurpose;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList primarySecurity;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PrimaryUsageList primaryUsage;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String propertyID;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList purchaseUnderAOCWrap;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected RealEstateAssetStatusList status;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList toBeSold;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList toBeUsedAsSecurity;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected AssetTransactionList transaction;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ValuationProgramList valuationProgram;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList valuationRequired;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList verified;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAddress;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xPropertyAgent;

    @Data
    @Embeddable
    public static class Commercial {
        @Enumerated(EnumType.STRING)
        @Column(name = "commercial_type", length = 60)
        protected CommercialTypeList type;
    }

    @Data
    @Embeddable
    public static class HeritageListing {
        @Enumerated(EnumType.STRING)
        @Column(name = "hl_heritageListed", length = 3)
        protected YesNoList heritageListed;

    }


    @Data
    @Embeddable
    public static class Industrial {
        @Enumerated(EnumType.STRING)
        @Column(name = "industrial_type", length = 60)
        protected IndustrialTypeList type;

    }


    @Data
    @Entity
    @Table(name = "real_estate_asset_insurance")
    public static class Insurance {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @ManyToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;
        
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xInsurance;

    }


    @Data
    @Entity
    @Table(name = "property_expense")
    public static class PropertyExpense {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @OneToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;
        
        @Column(precision=19, scale=2)
        protected BigDecimal amount;
        
        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected FrequencyFullList frequency;
    }
    
    
    @Data
    @Entity
    @Table(name = "property_type")
    public static class PropertyType {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @OneToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String categoryTypeName;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String propertyTypeCode;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String propertyTypeName;

    }
 


    @Data
    @Embeddable
    public static class Representation {
    	
    	//@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(name = "representation_xConveyancer", columnDefinition = "VARCHAR(80)")
        protected String xConveyancer;

      //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(name = "representation_xVendorConveyancer", columnDefinition = "VARCHAR(80)")
        protected String xVendorConveyancer;

    }


    @Data
    @Embeddable
    public static class Residential {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "residential_type", length = 60)
        protected ResidentialTypeList type;

        @Enumerated(EnumType.STRING)
        @Column(name = "residential_willOwn25PercentOfComplex", length = 3)
        protected YesNoList willOwn25PercentOfComplex;

        @Enumerated(EnumType.STRING)
        @Column(name = "residential_willOwn3UnitsInComplex", length = 3)
        protected YesNoList willOwn3UnitsInComplex;

    }


    @Data
    @Entity
    @Table(name = "restrictions_on_use_of_land")
    public static class RestrictionOnUseOfLand {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @OneToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;
    	
        @Column(columnDefinition = "VARCHAR(500)")
        protected String description;

        @Column(columnDefinition = "VARCHAR(80)")
        protected String registrationNumber;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList restrictionExists;

    }


    @Data
    @Entity
    @Table(name = "rural")
    public static class Rural {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @OneToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;
    	
        @Column(columnDefinition = "BIGINT")
        protected BigInteger numberOfBuildings;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String type;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected RuralUsageList usage;

    }


    @Data
    @Entity
    @Table(name = "visit_contact")
    public static class VisitContact {
    	
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;

        @OneToOne( fetch = FetchType.LAZY )
        @JoinColumn( name = "real_estate_asset_id" )
        protected RealEstateAsset realEstateAsset;

        
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList boatAccessOnly;

        @Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList fourWDAccessOnly;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected VisitContactTypeList type;

        //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xContactParty;

    }

    @Data
    @Embeddable
    public static class Zoning {
        @Column(name = "zoning_type", columnDefinition = "VARCHAR(255)")
        protected String type;

    }

    // Setters - ConstructionDetails
	public void setConstructionDetails(ConstructionDetails constructionDetails) {
		if(constructionDetails != null) {
			this.constructionDetails = constructionDetails;
			this.constructionDetails.setRealEstateAsset(this);
		}
	}

    // Setters - ContractDetails
	public void setContractDetails(ContractDetails contractDetails) {
		if(contractDetails != null) {
			this.contractDetails = contractDetails;
			this.contractDetails.setRealEstateAsset(this);
		}
	}

    // Setters - Encumbrance
	public void setEncumbrance(List<Encumbrance> encumbrance) {
		if(encumbrance != null) {
			this.encumbrance = encumbrance;
			this.encumbrance.forEach(x -> x.setRealEstateAsset(this));
		}
	}

    // Setters - EstimatedValue
	public void setEstimatedValue(EstimatedValue estimatedValue) {
		if(estimatedValue != null) {
			this.estimatedValue = estimatedValue;
			this.estimatedValue.setRealEstateAsset(this);
		}
	}

    // Setters - FundsDisbursementType
	public void setFundsDisbursement(List<FundsDisbursementType> fundsDisbursement) {
		if(fundsDisbursement != null) {
			this.fundsDisbursement = fundsDisbursement;
			this.fundsDisbursement.forEach(x -> x.setRealEstateAsset(this));
		}
	}

    // Setters - FutureRentalIncome
	public void setFutureRentalIncome(List<FutureRentalIncome> futureRentalIncome) {
		if(futureRentalIncome != null) {
			this.futureRentalIncome = futureRentalIncome;
			this.futureRentalIncome.forEach(x->x.setRealEstateAsset(this));
		}
	}

    // Setters - Insurance
	public void setInsurance(List<Insurance> insurance) {
		if(insurance != null) {
			this.insurance = insurance;
			this.insurance.forEach(x -> x.setRealEstateAsset(this));
		}
	}

    // Setters - PercentOwnedType
	public void setPercentOwned(PercentOwnedType percentOwned) {
		if(percentOwned != null) {
			this.percentOwned = percentOwned;
			this.percentOwned.setRealEstateAsset(this);
		}
	}

    // Setters - PropertyExpense
	public void setPropertyExpense(PropertyExpense propertyExpense) {
		if(propertyExpense != null) {
			this.propertyExpense = propertyExpense;
			this.propertyExpense.setRealEstateAsset(this);
		}
	}	
    // Setters - PropertyFeatures
	public void setPropertyFeatures(List<PropertyFeatures> propertyFeatures) {
		if(propertyFeatures != null) {
			this.propertyFeatures = propertyFeatures;
			this.propertyFeatures.forEach(x -> x.setRealEstateAsset(this));
		}
	}

    // Setters - PropertyType
	public void setPropertyType(PropertyType propertyType) {
		if(propertyType != null) {
			propertyType.setRealEstateAsset(this);
			this.propertyType = propertyType;
		}
	}

    // Setters - RentalIncome
	public void setRentalIncome(List<RentalIncome> rentalIncome) {
		if(rentalIncome != null) {
			this.rentalIncome = rentalIncome;
			rentalIncome.forEach(x -> x.setRealEstateAsset(this));
		}
	}


    // Setters - Rural
	public void setRural(Rural rural) {
		if(rural != null) {
			this.rural = rural;
			this.rural.setRealEstateAsset(this);
		}
	}

    // Setters - Title
	public void setTitle(List<Title> title) {
		if(title != null) {
			this.title = title;
			this.title.forEach(x -> x.setRealEstateAsset(this));
		}
	}

    // Setters - VisitContact
	public void setVisitContact(VisitContact visitContact) {
		if(visitContact != null) {
			this.visitContact = visitContact;
			this.visitContact.setRealEstateAsset(this);
		}
	}


    
}