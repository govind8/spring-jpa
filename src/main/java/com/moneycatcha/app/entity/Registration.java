package com.moneycatcha.app.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Registration {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected PPSR ppsr;
    
    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "registration", cascade = CascadeType.ALL, orphanRemoval=true)
    protected List<RegistrationEvent> registrationEvent;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String givingOfNoticeIdentifier;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList purchaseMoneySecurityInterest;

    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime registrationEndDateTime;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String registrationNumber;

    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime registrationStartDateTime;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String registrationToken;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String securedPartyGroupNumber;

    
    public Registration() {
    	// JPA
	}

    // Setter - RegistrationEvent
	public void setRegistrationEvent(List<RegistrationEvent> registrationEvent) {
		if (registrationEvent != null) {
			this.registrationEvent = registrationEvent;
			this.registrationEvent.forEach(x -> x.setRegistration(this));
		}
	}

}

