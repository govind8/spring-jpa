package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.DurationUnitsList;

import lombok.Data;

@Data
@Entity
public class DiscountMargin {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;
    
    @Column(precision=19, scale=2)
    protected BigDecimal discountedAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal discountRate;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String discountReason;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger duration;

    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    protected DurationUnitsList durationUnits;

    public DiscountMargin() {
    	// JPA
    }
  

}
