//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.2 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.08.20 at 10:22:11 PM AWST 
//
package com.moneycatcha.app.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.DocumentationInstructionsMethodList;
import com.moneycatcha.app.model.SendDocumentsToList;

import lombok.Data;

@Data
@Entity
public class DocumentationInstructionsType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "deposit_account_details_id")
    protected DepositAccountDetails depositAccountDetails;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "liability_id")
	protected Liability liability;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "loan_details_id")
	protected LoanDetails loanDetails;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "summary_id")
    protected Summary summary;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "person_applicant_id")
	protected PersonApplicant personApplicant;
	
    @Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected DocumentationInstructionsMethodList method;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected SendDocumentsToList sendDocumentsTo;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xDeliverTo;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xNominatedAuthority;

	public DocumentationInstructionsType() {
		//JPA
	}

	
}
