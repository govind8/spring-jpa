package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class TermsAndConditions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String termsCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String termsName;

    public TermsAndConditions() {
    	// JPA
    }
 
}