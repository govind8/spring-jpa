package com.moneycatcha.app.entity;

import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoIntentList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Business {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "business", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAnalysis financialAnalysis;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "non_real_estate_asset_id")
	NonRealEstateAsset nonRealEstateAsset;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "self_employed_id")
	SelfEmployed selfEmployed;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String australianBIC;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String businessName;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String customIndustryCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String franchiseDetails;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String gicsCode;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String industry;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String industryCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isFranchise;

    @Enumerated(EnumType.STRING)
    @Column(length = 25)
    protected YesNoIntentList isFranchiseIntent;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String mainBusinessActivity;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfEmployees;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfLocations;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList ownPremises;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String principalTradingAddressLeaseAgreementDetails;
    

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList principalTradingAddressLeased;

    @Column(columnDefinition = "Date")
    protected LocalDate startDate;

	@Embedded
    protected ConcentrationRisk concentrationRisk;
	
	@Embedded
    protected Diversification diversification;
	
	@Embedded
    protected ImportExport importExport;
	
	@Embedded
    protected PropertyInvestment propertyInvestment;
	
	public Business() {
		//JPA
	}
	
    @Data
    @Embeddable
    public static class ConcentrationRisk {

        @Column(columnDefinition = "VARCHAR(255)")
        protected String concentrationRiskDetails;

        @Enumerated(EnumType.STRING)
        @Column(name="risk_customerOrSupplierConcentration", length = 3)
        protected YesNoList customerOrSupplierConcentration;
        
        public ConcentrationRisk() {
        	// JPA
        }
       
    }


    @Data
    @Embeddable
    public static class Diversification {

        @Column(columnDefinition = "VARCHAR(255)")
        protected String details;

        @Column(columnDefinition = "Date")
        protected LocalDate diversifiedDate;

        @Enumerated(EnumType.STRING)
        @Column(name="diversification_isDiversified", length = 3)
        protected YesNoList isDiversified;

        public Diversification() {
        	// JPA
        }
    }


    @Data
    @Embeddable
    public static class ImportExport {

        @Column(name="import_export_details", columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(name="import_export_is_involved", length = 15)
        protected YesNoIntentList isInvolved;

        public ImportExport() {
        	// JPA
        }

    }


    @Data
    @Embeddable
    public static class PropertyInvestment {
        
        @Column(name="property_invsetment_details", columnDefinition = "VARCHAR(255)")
        protected String details;

        @Enumerated(EnumType.STRING)
        @Column(name="property_invsetment_is_involved", length = 15)
        protected YesNoIntentList isInvolved;

        public PropertyInvestment() {
        	// JPA
        }
 
    }
    
    // Setters - FinancialAnalysis
    public void setFinancialAnalysis(FinancialAnalysis financialsAnalysis) {
    	if (financialAnalysis != null) {
	        this.financialAnalysis = financialsAnalysis;    
	        this.financialAnalysis.setBusiness(this);
    	}
    }
    

}

