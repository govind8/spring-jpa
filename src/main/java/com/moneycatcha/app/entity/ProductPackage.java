package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class ProductPackage {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "application_id")
    protected Application application;

    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "productPackage", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<CreditCard> creditCard;
	
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "productPackage", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<DepositAccount> depositAccount;
	
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "productPackage", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Liability> liability;
	
    @Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "productPackage", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<LoanDetails> loanDetails;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList existingPackage;
  
	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;


    @Data
    @Entity
    @Table(name = "pp_liability")
    public static class Liability {

    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "product_package_id")
        protected ProductPackage productPackage;
        
        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;
        
        //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xLiability;

    }


    @Data
    @Entity
    @Table(name = "pp_loan_details")
    public static class LoanDetails {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
    	
        @ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name = "product_package_id")
        protected ProductPackage productPackage;
        
        @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
        protected String uniqueID;
        
        //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
        @Column(columnDefinition = "VARCHAR(80)")
        protected String xLoanDetails;

    }

    // Constructors
	public ProductPackage() {

	}

	// Setters - CreditCard
	public void setCreditCard(List<CreditCard> creditCard) {
		if (creditCard != null) {
			this.creditCard = creditCard;
			this.creditCard.forEach(x -> x.setProductPackage(this));
		}
	}

	// Setters - DepositAccount
	public void setDepositAccount(List<DepositAccount> depositAccount) {
		if (depositAccount != null) {
			this.depositAccount = depositAccount;
			this.depositAccount.forEach(x -> x.setProductPackage(this));
		}
	}

	// Setters - Liability
	public void setLiability(List<Liability> liability) {
		if (liability != null) {
			this.liability = liability;
			this.liability.forEach(x -> x.setProductPackage(this));
		}
	}

	// Setters - LoanDetails
	public void setLoanDetails(List<LoanDetails> loanDetails) {
		if (loanDetails != null) {
			this.loanDetails = loanDetails;
			this.loanDetails.forEach(x -> x.setProductPackage(this));
		}
	}
     
}