package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class DepositAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "product_package_id")
    protected ProductPackage productPackage;
    
    @Embedded
    protected Overdraft overdraft;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList alreadyExists;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    //@OneToOne(mappedBy = " ", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xDepositAccount;

    @Data
    @Embeddable
    public static class Overdraft {

        @Enumerated(EnumType.STRING)
        @Column(name="od_considerLowerOverdraftLimitIfNotEligible", length = 3)
        protected YesNoList considerLowerOverdraftLimitIfNotEligible;

        @Enumerated(EnumType.STRING)
        @Column(name="od_isOverdraftUsedForTemporaryExpenses", length = 3)
        protected YesNoList isOverdraftUsedForTemporaryExpenses;

        @Column(name="od_limit", precision=19, scale=2)
        protected BigDecimal limit;

        @Column(name="od_optionCode", columnDefinition = "VARCHAR(80)")
        protected String optionCode;

        @Enumerated(EnumType.STRING)
        @Column(name="od_overdraftRequested", length = 3)
        protected YesNoList overdraftRequested;

    }
}