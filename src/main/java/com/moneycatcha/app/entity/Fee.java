package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CalculationBasisList;
import com.moneycatcha.app.model.FeeCategoryList;
import com.moneycatcha.app.model.FeePaymentTimingList;
import com.moneycatcha.app.model.FeeTypeList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.PayFeesFromList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Fee {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "summary_id")
	protected Summary summary;    
	
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fee", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "length", column = @Column(name = "duration_length")),
            @AttributeOverride(name = "units", column = @Column(name = "duration_units"))
    })
    protected DurationType applicableDuration;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fee", cascade = CascadeType.ALL, orphanRemoval = true)
    protected CreditCard creditCard;
    
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fee", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType payableToAccount;
    
    @Embedded
    protected Percentage percentage;

    @Column(precision=19, scale=2)
    protected BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList capitaliseFee;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FeeCategoryList category;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String feeCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyFullList frequency;

    @Column(precision=19, scale=2)
    protected BigDecimal gstAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isDisclosed;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isEstimated;

    @Column(precision=19, scale=2)
    protected BigDecimal itcAmount;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfRepeats;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList paid;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String payableTo;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PayFeesFromList payFeesFrom;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FeePaymentTimingList paymentTiming;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected FeeTypeList type;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAccount;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xFinancialProduct;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xProductSet;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSecurity;

    @Data
    @Embeddable
    public static class Percentage {
    	
        @Enumerated(EnumType.STRING)
        @Column(name = "percentage_calculationBasis", length = 60)
        protected CalculationBasisList calculationBasis;

        @Column(name = "percentage_rate", precision=19, scale=2)
        protected BigDecimal rate;

    }

    public Fee() {
    	// JPA
    }

	// Setters - FinancialAccountType
	public void setAccountNumber(FinancialAccountType accountNumber) {
		if (accountNumber != null) {
			this.accountNumber = accountNumber;
			this.accountNumber.setFee(this);
		}
	}

	// Setters - FinancialAccountType
	public void setCreditCard(CreditCard creditCard) {
		if (creditCard != null) {
			this.creditCard = creditCard;
			this.creditCard.setFee(this);
		}
	}

	// Setters - FinancialAccountType
	public void setPayableToAccount(FinancialAccountType payableToAccount) {
		if (payableToAccount != null) {
			payableToAccount.setFee(this);
			this.payableToAccount = payableToAccount;
		}
	}
    

    
    
}