package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class Settlement {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "settlement", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Security> security;

    @Column(precision=19, scale=2)
    protected BigDecimal payoutFigure;

    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime settlementBookingDateTime;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String settlementReferenceNumber;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xSettlementBookingAddress;

    @Data
    @Entity
    @Table(name="settlement_security")
    public static class Security {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "settlement_id")
    	protected Settlement settlement;    

        @Column(columnDefinition = "VARCHAR(80)")
        protected String xSecurity;

		public Security() {
			// JPA
		}
    
    }

	public Settlement() {
		// JPA
	}

	public void setSecurity(List<Security> security) {
		if (security != null) {
			this.security = security;
			this.security.forEach(x -> x.setSettlement(this));
		}
	}
    
    
}