package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.moneycatcha.app.entity.Liability.OriginalTerm;
import com.moneycatcha.app.entity.LoanDetails.Term;
import com.moneycatcha.app.model.InterestTypeList;
import com.moneycatcha.app.model.PaymentTypeList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class DistinctLoanPeriod {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "original_term_id")
	OriginalTerm originalTerm;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "term_id")
	Term term;
	
    @Embedded
    @AttributeOverrides(value = {
        @AttributeOverride(name = "length", column = @Column(name = "duration_length")),
        @AttributeOverride(name = "units", column = @Column(name = "duration_units"))
    })
    protected DurationType duration;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "distinctLoanPeriod", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Repayment> repayment;

    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected InterestTypeList interestType;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PaymentTypeList paymentType;

    @Column(precision=19, scale=2)
    protected BigDecimal totalFeesAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal totalInterestAmount;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger totalRepayments;

    @Column(precision=19, scale=2)
    protected BigDecimal totalRepaymentsAmount;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xRateComposition;

    public DistinctLoanPeriod() {
    	// JPA
    }

    // Setters - Repayment
    public void setRepayment(List<Repayment> repayment) {
    	if(repayment != null) {
	        this.repayment = repayment;    	
	    	this.repayment.forEach(x -> x.setDistinctLoanPeriod(this));
    	}
    }

    
}