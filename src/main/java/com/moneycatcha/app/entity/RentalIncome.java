package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.RentalIncomeBasisList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RentalIncome {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "real_estate_asset_id" )
    protected RealEstateAsset realEstateAsset;
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "rentalIncome", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<PropertyPart> propertyPart;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected RentalIncomeBasisList basis;

    @Column(precision=19, scale=2)
    protected BigDecimal confidencePercentage;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList evidenceOfTenancy;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyFullList frequency;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeStatusOnOrBeforeSettlementList incomeStatusOnOrBeforeSettlement;

    @Column(precision=19, scale=2)
    protected BigDecimal netRentalAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyShortList netRentalAmountFrequency;

    @Column(precision=19, scale=2)
    protected BigDecimal rentalAmount;

    @Column(columnDefinition = "DATE")
    protected LocalDate valuedDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xOwner;

    // Constructor
    public RentalIncome() {
    	// JPA
    }
    
	public void setPropertyPart(List<PropertyPart> propertyPart) {
		if (propertyPart != null) {
			this.propertyPart = propertyPart;
			this.propertyPart.forEach(x -> x.setRentalIncome(this));
		}
	}
    
    
}

