package com.moneycatcha.app.entity;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.SurplusFundsDisbursementMethodList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
 

@Data
@Entity
public class FundsDisbursementType {
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;
	
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn
    LoanDetails loanDetails;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "non_real_estate_asset_id")
    NonRealEstateAsset nonRealEstateAsset;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "real_estate_asset_id")
    RealEstateAsset realEstateAsset;
    
//    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fundsDisbursementType", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FinancialAccountType accountNumber;

	@Embedded
	protected BillerDetails billerDetails;

    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fundsDisbursementType", cascade = CascadeType.ALL, orphanRemoval = true)
	protected PersonNameType personName;

	@Column(precision=19, scale=2)
    protected BigDecimal amount;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String companyName;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected SurplusFundsDisbursementMethodList method;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xAccount;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String xAddress;

	
	public FundsDisbursementType() {
		// JPA
	}
    
	@Data
    @Embeddable
    public static class BillerDetails {
	    @Column(name = "billerdetails_billerCode", columnDefinition = "VARCHAR(80)")
        protected String billerCode;

	    @Column(name = "billerdetails_billerName", columnDefinition = "VARCHAR(80)")
        protected String billerName;

	    @Column(name = "billerdetails_crn", columnDefinition = "VARCHAR(80)")
        protected String crn;
	    
	    public BillerDetails() {
	    	// JPA
	    }
	}
	
	
    // Setters - FinancialAccountType
    public void setAccountNumber(FinancialAccountType accountNumber) {
    	if (accountNumber != null) {
	        this.accountNumber = accountNumber;    	
	    	this.accountNumber.setFundsDisbursementType(this);
    	}
    }

	// Setters - PersonNameType
	public void setPersonName(PersonNameType personName) {
		if (personName != null) {
			this.personName = personName;
	    	this.personName.setFundsDisbursementType(this);
		}
	}
    
}
