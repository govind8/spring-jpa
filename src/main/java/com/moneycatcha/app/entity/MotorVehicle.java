package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.ConditionList;
import com.moneycatcha.app.model.VehicleTypeList;

import lombok.Data;

@Data
@Entity
public class MotorVehicle {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "non_real_estate_asset_id" )
    protected NonRealEstateAsset nonRealEstateAsset;
 	    	
    @Column(columnDefinition = "VARCHAR(80)")
    protected String additionalIDType;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String additionalIDValue;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger age;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String badge;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String body;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String colour;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionList condition;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String conditionDescription;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger cylinders;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger doors;

    @Column(precision=19, scale=2)
    protected BigDecimal effectiveLife;

    @Column(precision=19, scale=2)
    protected BigDecimal engineCapacity;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger engineHoursTotal;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String engineID;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String fuelType;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger kilometres;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String make;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String model;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String options;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherInformation;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger quantity;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected AuStateList registeredInState;

    @Column(columnDefinition = "DATE")
    protected LocalDate registrationExpiryDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String registrationNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String serialNumber;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String series;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String transmission;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected VehicleTypeList type;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String variant;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xGoodToBeUsedAddress;

    @Column(columnDefinition = "INT")
    protected int year;

	public MotorVehicle() {
		// JPA
	}
    
    

}