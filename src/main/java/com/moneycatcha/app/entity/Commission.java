package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Commission {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "deposit_account_details_id")
    protected DepositAccountDetails depositAccountDetails;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "sales_channel_id")
    protected SalesChannel salesChannel;
    
    @Column(precision=19, scale=2)
    protected BigDecimal commissionAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList commissionPaid;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected CommissionStructureList commissionStructure;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherCommissionStructureDescription;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String promotionCode;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList thirdPartyReferee;

    @Column(precision=19, scale=2)
    protected BigDecimal trail;
    
    // Loan Details
    @Column(precision=19, scale=2)
    protected BigDecimal gstAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal itcAmount;

    @Column(precision=19, scale=2)
    protected BigDecimal upfrontPayment;

    public Commission() {
    	// JPA
    }

    
}
