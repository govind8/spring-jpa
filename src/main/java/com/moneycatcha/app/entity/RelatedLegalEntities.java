package com.moneycatcha.app.entity;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class RelatedLegalEntities {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "company_applicant_id")
	CompanyApplicant companyApplicant;    

	@Column(columnDefinition = "VARCHAR(80)")
    protected String currentCustomers;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger entityCount;

    public RelatedLegalEntities() {
    	// JPA
    }
    
	public RelatedLegalEntities(String currentCustomers, BigInteger entityCount) {
		
		if (currentCustomers != null) {
			this.currentCustomers = currentCustomers;
			this.entityCount = entityCount;
		}
	}
    
}
