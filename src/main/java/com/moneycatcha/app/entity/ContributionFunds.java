package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.ContributionFundsTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class ContributionFunds {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;	
	
     @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "application_id")
    Application application;
    
    
    @Column(precision=19, scale=2)
    protected BigDecimal amount;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList loan;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ContributionFundsTypeList type;
    
	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
    @Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;	

//    @OneToOne(mappedBy = "contributionFunds", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xAssociatedLoanAccount;

//    @OneToOne(mappedBy = "contributionFunds", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xFundsHeldInAccount;
 
    
    public ContributionFunds() {
    	// JPA
    }
    

}
