package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.moneycatcha.app.model.GoodsDeliveryList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class VendorTaxInvoiceType {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    	

	@Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
	protected String uniqueID;
	
	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "vendorTaxInvoiceType", cascade = CascadeType.ALL, orphanRemoval = true)
	protected List<Asset> asset;
	
	@Column(columnDefinition = "VARCHAR(80)")
	protected String invoiceNumber;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
	
	@Column(columnDefinition = "DATE")
	protected LocalDate taxInvoiceDate;
	
	@Column(columnDefinition = "VARCHAR(80)")
	protected String xDeliverTo;
	
	@Column(columnDefinition = "VARCHAR(80)")
	protected String xPurchaser;
	
	@Column(columnDefinition = "VARCHAR(80)")
	protected String xVendor;


    @Data
    @Entity
    @Table(name= "vendor_asset")
    public static class Asset {
    	
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@ManyToOne(fetch=FetchType.LAZY)
    	@JoinColumn
    	protected VendorTaxInvoiceType vendorTaxInvoiceType;    	

    	@Column(columnDefinition = "VARCHAR(80)")
        protected String uniqueAssetIdentifier;
    	
    	@Column(columnDefinition = "VARCHAR(255)")
        protected String assetDescription;

    	@Column(columnDefinition = "DATE")
        protected LocalDate deliveryDate;

    	@Column(precision=19, scale=2)
        protected BigDecimal depositPaid;

    	@Column(precision=19, scale=2)
        protected BigDecimal finalPosition;

    	@Enumerated(EnumType.STRING)
    	@Column(length = 60)
        protected GoodsDeliveryList goodsDelivery;

    	@Column(precision=19, scale=2)
        protected BigDecimal gstComponent;

    	@Column(precision=19, scale=2)
        protected BigDecimal nonTaxableAmount;

    	@Column(precision=19, scale=2)
        protected BigDecimal onCosts;

    	@Column(precision=19, scale=2)
        protected BigDecimal payout;

    	@Column(precision=19, scale=2)
        protected BigDecimal refund;

    	@Column(columnDefinition = "BIGINT")
        protected BigInteger sequenceNumber;
    	
    	@Column(precision=19, scale=2)
        protected BigDecimal taxableAmountOfAsset;

    	@Column(precision=19, scale=2)
        protected BigDecimal totalCostOfAsset;

    	@Column(precision=19, scale=2)
        protected BigDecimal tradeIn;

		public Asset() {
			// JPA
		}

    }

    public VendorTaxInvoiceType() {
    	// JPA
    }
    
	
	// Setters - Asset
	public void setAsset(List<Asset> asset) {
		if (asset != null) {
			this.asset = asset;
			this.asset.forEach(x -> x.setVendorTaxInvoiceType(this));
		}
	}
    
	
    
}
