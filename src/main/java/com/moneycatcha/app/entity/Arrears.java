package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class Arrears {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "other_expense_id")
    protected OtherExpense otherExpense;
    
    @Column(precision=19, scale=2)
    protected BigDecimal amount;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger numberOfMissedPayments;
    
    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    protected YesNoList toBePaidOut;

    public Arrears() {
    	// JPA
    }
    

}