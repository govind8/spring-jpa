package com.moneycatcha.app.entity;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.BusinessChannelTypeList;
import com.moneycatcha.app.model.LicenceTypeList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PhoneType;

import lombok.Data;

@Data
@Entity
public class BusinessChannel {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

	@Column(name="uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	Application application;
	
    @Embedded
    protected Contact contact;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String abn;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String acn;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String companyName;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String lenderID;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String licenceNumber;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected LicenceTypeList licenceType;

	@Column(columnDefinition = "VARCHAR(80)")
    protected String otherIdentifier;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected BusinessChannelTypeList type;
	
	public BusinessChannel() {
		// required for JPA
	}

	
    @Data
    @Embeddable
    public static class Contact {
        @Embedded
        protected ContactPerson contactPerson;

        @Embedded
        @AttributeOverrides(value = {
            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "office_fax_australian_dialing_code")),
            @AttributeOverride(name = "countryCode", column = @Column(name = "office_fax_country_code")),
            @AttributeOverride(name = "number", column = @Column(name = "office_fax_number")),
            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "office_fax_overseas_dialing_code"))
         })
        protected PhoneType officeFax;

        @Embedded
	    @AttributeOverrides(value = {
	            @AttributeOverride(name = "australianDialingCode", column = @Column(name = "office_phone_australian_dialing_code")),
	            @AttributeOverride(name = "countryCode", column = @Column(name = "office_phone_country_code")),
	            @AttributeOverride(name = "number", column = @Column(name = "office_phone_number")),
	            @AttributeOverride(name = "overseasDialingCode", column = @Column(name = "office_phone_overseas_dialing_code"))
	         })
        protected PhoneType officePhone;

	    @Column(columnDefinition = "VARCHAR(100)")
        protected String email;

	    @Column(columnDefinition = "VARCHAR(100)")
        protected String webAddress;

	    @Column(columnDefinition = "VARCHAR(80)")
        protected String xAddress;


        @Data
        @Embeddable
        public static class ContactPerson {
            
            @Column(columnDefinition = "VARCHAR(80)")
            protected String firstName;

            @Enumerated(EnumType.STRING)
            @Column(length = 60)
            protected NameTitleList nameTitle;

            @Column(columnDefinition = "VARCHAR(80)")
            protected String role;

            @Column(columnDefinition = "VARCHAR(80)")
            protected String surname;
        }
    }
    

}