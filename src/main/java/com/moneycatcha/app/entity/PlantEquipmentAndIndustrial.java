package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.ConditionList;
import com.moneycatcha.app.model.GoodToBeUsedLocationList;
import com.moneycatcha.app.model.PlantEquipmentAndIndustrialTypeList;

import lombok.Data;

@Data
@Entity
public class PlantEquipmentAndIndustrial {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn( name = "non_real_estate_asset_id" )
    protected NonRealEstateAsset nonRealEstateAsset;
    
    @Column(columnDefinition = "VARCHAR(80)")
    protected String additionalIDType;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String additionalIDValue;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger age;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ConditionList condition;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String conditionDescription;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Column(precision=19, scale=2)
    protected BigDecimal effectiveLife;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger engineHoursTotal;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected GoodToBeUsedLocationList goodToBeUsedLocation;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String make;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String model;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String otherInformation;

    @Column(columnDefinition = "BIGINT")
    protected BigInteger quantity;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String serialNumber;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String servicingHistory;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PlantEquipmentAndIndustrialTypeList type;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xGoodToBeUsedAddress;

    @Column(columnDefinition = "INT")
    protected int year;

	public PlantEquipmentAndIndustrial() {
		//JPA
	}

}
