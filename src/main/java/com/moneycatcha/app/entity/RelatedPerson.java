package com.moneycatcha.app.entity;
import java.math.BigInteger;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class RelatedPerson {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "application_id")
	protected Application application;    
	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "relatedPerson", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Contact contact;

    @OneToOne(mappedBy = "relatedPerson", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ForeignTaxAssociationType foreignTaxAssociation;
   
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "relatedPerson", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PersonNameType personName;

	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "relatedPerson", cascade = CascadeType.ALL, orphanRemoval = true)
    protected TaxDeclarationDetailsType taxDeclarationDetails;

    @Column(columnDefinition = "DATE")
    protected LocalDate dateOfBirth;

	@Column(columnDefinition = "BIGINT")
    protected BigInteger sequenceNumber;
    
    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    // Constructors
	public RelatedPerson() {
		// JPA
	}

	// Setters - Contact
	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setRelatedPerson(this);
		}
	}

	// Setters - ForeignTaxAssociationType
	public void setForeignTaxAssociation(ForeignTaxAssociationType foreignTaxAssociation) {
		if (foreignTaxAssociation != null) {
			this.foreignTaxAssociation = foreignTaxAssociation;
			this.foreignTaxAssociation.setRelatedPerson(this);
		}
	}

	// Setters - PersonName
	public void setPersonName(PersonNameType personName) {
		if (personName != null) {
			this.personName = personName;
			this.personName.setRelatedPerson(this);
		}
	}

	// Setters - TaxDeclarationDetailsType
	public void setTaxDeclarationDetails(TaxDeclarationDetailsType taxDeclarationDetails) {
		if (taxDeclarationDetails != null) {
			this.taxDeclarationDetails = taxDeclarationDetails;
			this.taxDeclarationDetails.setRelatedPerson(this);
		}
	}

}
