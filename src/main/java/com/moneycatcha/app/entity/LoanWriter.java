package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.LicenceTypeList;
import com.moneycatcha.app.model.NameTitleList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class LoanWriter {
	
   	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "sales_channel_id")
 	protected SalesChannel salesChannel; 
	
	@Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "loanWriter", cascade = CascadeType.ALL, orphanRemoval = true)
	protected Contact contact;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String accreditationNumber;

    @Column(columnDefinition = "VARCHAR(120)")
    protected String department;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String firstName;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String licenceNumber;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected LicenceTypeList licenceType;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String managerName;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected NameTitleList nameTitle;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String otherIdentifier;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String personRole;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String surname;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    // constructors
    public LoanWriter() {
    	//JPA
    }
    
	// Setters - Contact
	public void setContact(Contact contact) {
		if (contact != null) {
			this.contact = contact;
			this.contact.setLoanWriter(this);
		}
	}

    
}