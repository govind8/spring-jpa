package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.PurposeOfApplicationList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;


@Data
@Entity
public class NeedsAnalysis {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "content_id")
	protected Content content;    	
	
    @Embedded
    protected BenefitToApplicants benefitToApplicants;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected CoApplicantInterview coApplicantInterview;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected FutureCircumstances futureCircumstances;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Interview interview;

    @Embedded
    @AttributeOverrides(value = {
        @AttributeOverride(name = "length", column = @Column(name = "loantermsought_duration_length")),
        @AttributeOverride(name = "units", column = @Column(name = "loantermsought_duration_units"))
    })
    protected DurationType loanTermSought;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Preferences preferences;

    @Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<PurposeOfApplication> purposeOfApplication;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected RefinancingAndConsolidation refinancingAndConsolidation;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "needsAnalysis", cascade = CascadeType.ALL, orphanRemoval = true)
    protected RetirementDetails retirementDetails;

    @Column(precision=19, scale=2)
    protected BigDecimal loanAmountSought;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String loanTermSoughtDescription;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected PrimaryPurposeLoanPurposeList primaryApplicationPurpose;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String productSelection;

    @Column(columnDefinition = "VARCHAR(255)")
    protected String purposeSummary;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;


	@Data
	@Embeddable
	public static class BenefitToApplicants {
		
		@Enumerated(EnumType.STRING)
		@Column(name = "bta_allApplicantsBenefit", length = 3)
	    protected YesNoList allApplicantsBenefit;
	
		@Column(name = "bta_benefitEnquiries", columnDefinition = "VARCHAR(255)")
	    protected String benefitEnquiries;
	}
	
	@Data
	@Entity
	@Table(name = "purpose_of_application")
	public static class PurposeOfApplication {

		@Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;
	    
		@ManyToOne(fetch=FetchType.LAZY)
		@JoinColumn(name = "needs_analysis_id")
		protected NeedsAnalysis needsAnalysis;    	

	    @Column(precision=19, scale=2)
	    protected BigDecimal amount;

	    @Column(columnDefinition = "VARCHAR(255)")
	    protected String description;

	    @Enumerated(EnumType.STRING)
	    @Column(length = 60)
	    protected PurposeOfApplicationList purpose;

	}
	
	// Constructors

	public NeedsAnalysis() {
		// JPA
	}


	// Setters - CoApplicantInterview
	public void setCoApplicantInterview(CoApplicantInterview coApplicantInterview) {
		if (coApplicantInterview != null) {
			this.coApplicantInterview = coApplicantInterview;
			this.coApplicantInterview.setNeedsAnalysis(this);
		}
	}

	// Setters - FutureCircumstances
	public void setFutureCircumstances(FutureCircumstances futureCircumstances) {
		if (futureCircumstances != null) {
			this.futureCircumstances = futureCircumstances;
			this.futureCircumstances.setNeedsAnalysis(this);
		}
	}

	// Setters - Interview
	public void setInterview(Interview interview) {
		if (interview != null) {
			this.interview = interview;
			this.interview.setNeedsAnalysis(this);
		}
	}

	// Setters - Preferences
	public void setPreferences(Preferences preferences) {
		if (preferences != null) {
			this.preferences = preferences;
			this.preferences.setNeedsAnalysis(this);
		}
	}

	// Setters - PurposeOfApplication
	public void setPurposeOfApplication(List<PurposeOfApplication> purposeOfApplication) {
		if (purposeOfApplication != null) {
			this.purposeOfApplication = purposeOfApplication;
			this.purposeOfApplication.forEach(x -> x.setNeedsAnalysis(this));
		}
	}

	// Setters - RefinancingAndConsolidation
	public void setRefinancingAndConsolidation(RefinancingAndConsolidation refinancingAndConsolidation) {
		if (refinancingAndConsolidation != null) {
			this.refinancingAndConsolidation = refinancingAndConsolidation;
			this.refinancingAndConsolidation.setNeedsAnalysis(this);
		}
	}

	// Setters - RetirementDetails
	public void setRetirementDetails(RetirementDetails retirementDetails) {
		if (retirementDetails != null) {
			this.retirementDetails = retirementDetails;
			this.retirementDetails.setNeedsAnalysis(this);
		}
	}
	

}