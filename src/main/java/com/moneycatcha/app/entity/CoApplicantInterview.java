    
package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class CoApplicantInterview {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "needs_analysis_id")
	protected NeedsAnalysis needsAnalysis;    	

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy = "coApplicantInterview", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<Applicant> applicant;

	public CoApplicantInterview() {
		// JPA
	}

	public void setApplicant(List<Applicant> applicant) {
		if (applicant != null) {
			this.applicant = applicant;
			this.applicant.forEach(x -> x.setCoApplicantInterview(this));
		}
	}

	
}

