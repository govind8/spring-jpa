package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.OtherAssetTypeList;

import lombok.Data;

@Data
@Entity
public class OtherAsset {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn
    protected NonRealEstateAsset nonRealEstateAsset;
    
    @Column(columnDefinition = "VARCHAR(255)")
    protected String description;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected OtherAssetTypeList type;

	public OtherAsset() {
		//JPA
	}

}
