package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.ContactEmailTypeList;

import lombok.Data;

@Data
@Entity
public class EmailAddress {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "contact_id")
    protected Contact contact;
	
    @Column(columnDefinition = "VARCHAR(255)")
    protected String email;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected ContactEmailTypeList emailType;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

	public EmailAddress() {
		// JPA
	}
	
}



