package com.moneycatcha.app.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.moneycatcha.app.model.SourceOfFundsTypeList;

import lombok.Data;

@Entity
@Data
public class SourceOfWealth {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
    protected PersonApplicant personApplicant;    

    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
    
    @Column(columnDefinition = "VARCHAR(255)")
    protected String detail;

    @Column(precision=19, scale=2)
    protected BigDecimal percentage;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected SourceOfFundsTypeList type;
    
    public SourceOfWealth() {
    	// JPA
    }

}
