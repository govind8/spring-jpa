package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.model.EmploymentStatusList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.ProofCodeNotEmployedList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
@Entity
public class NotEmployed {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employment_id")
    protected Employment employment;
    
    @Embedded
    @AttributeOverrides(value = {
            @AttributeOverride(name = "duration.length", column = @Column(name = "not_employed_duration_length")),
            @AttributeOverride(name = "duration.units", column = @Column(name = "not_employed_duration_units"))
    })
    protected DurationType duration;

    @Embedded
    protected Income income;

 
    @Column(columnDefinition = "DATE")
    protected LocalDate endDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList homeDuties;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IncomeStatusOnOrBeforeSettlementList incomeStatusOnOrBeforeSettlement;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList onBenefits;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList retired;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected EmploymentStatusList status;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList student;

    @Column(name = "uniqueid", columnDefinition = "VARCHAR(80)")
    protected String uniqueID;

    @Data
    @Embeddable
    public static class Income {
    	
        @Column(name = "income_governmentBenefitsAmount", precision=19, scale=2)
        protected BigDecimal governmentBenefitsAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_governmentBenefitsFrequency", length = 15)
        protected FrequencyShortList governmentBenefitsFrequency;

        @Column(name = "income_netGovernmentBenefitsAmount", precision=19, scale=2)
        protected BigDecimal netGovernmentBenefitsAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netGovernmentBenefitsFrequency", length = 15)
        protected FrequencyShortList netGovernmentBenefitsFrequency;

        @Column(name = "income_netNewstartAllowanceAmount", precision=19, scale=2)
        protected BigDecimal netNewstartAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netNewstartAllowanceFrequency", length = 15)
        protected FrequencyShortList netNewstartAllowanceFrequency;

        @Column(name = "income_netOtherIncomeAmount", precision=19, scale=2)
        protected BigDecimal netOtherIncomeAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netOtherIncomeFrequency", length = 15)
        protected FrequencyShortList netOtherIncomeFrequency;

        @Column(name = "income_netPrivatePensionAmount", precision=19, scale=2)
        protected BigDecimal netPrivatePensionAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netPrivatePensionFrequency", length = 15)
        protected FrequencyShortList netPrivatePensionFrequency;

        @Column(name = "income_netSuperannuationAmount", precision=19, scale=2)
        protected BigDecimal netSuperannuationAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_netSuperannuationFrequency", length = 15)
        protected FrequencyShortList netSuperannuationFrequency;

        @Column(name = "income_newstartAllowanceAmount", precision=19, scale=2)
        protected BigDecimal newstartAllowanceAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_newstartAllowanceFrequency", length = 15)
        protected FrequencyShortList newstartAllowanceFrequency;

        @Column(name = "income_otherIncomeAmount", precision=19, scale=2)
        protected BigDecimal otherIncomeAmount;

        @Column(name = "income_otherIncomeDescription", columnDefinition = "VARCHAR(80)")
        protected String otherIncomeDescription;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_otherIncomeFrequency", length = 15)
        protected FrequencyShortList otherIncomeFrequency;

        @Column(name = "income_privatePensionAmount", precision=19, scale=2)
        protected BigDecimal privatePensionAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_privatePensionFrequency", length = 15)
        protected FrequencyShortList privatePensionFrequency;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_proofCode", length = 60)
        protected ProofCodeNotEmployedList proofCode;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_proofSighted", length = 3)
        protected YesNoList proofSighted;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_smsf", length = 3)
        protected YesNoList smsf;

        @Column(name = "income_superannuationAmount", precision=19, scale=2)
        protected BigDecimal superannuationAmount;

        @Enumerated(EnumType.STRING)
        @Column(name = "income_superannuationFrequency", length = 15)
        protected FrequencyShortList superannuationFrequency;

        // income - constructor
        public Income() {
        	//JPA
        }
        

    }
    
    public NotEmployed() {
    	//JPA
    }

 
}