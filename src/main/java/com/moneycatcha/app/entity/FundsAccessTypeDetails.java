package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class FundsAccessTypeDetails {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "preferences_id")
	protected Preferences preferences;    
	
    @Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "fundsAccessTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected OffsetAccount offsetAccount;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "fundsAccessTypeDetails", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Redraw redraw;


    @Data
    @Entity
    @Table(name = "redraw")
    public static class Redraw {
 
    	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        protected Long Id;
        
    	@OneToOne(fetch=FetchType.LAZY)
    	@JoinColumn(name = "funds_access_type_details_id")
    	protected FundsAccessTypeDetails fundsAccessTypeDetails;    
    	
        @Setter(AccessLevel.NONE)
        @OneToOne(mappedBy = "redraw", cascade = CascadeType.ALL, orphanRemoval = true)
        protected Reason reason;

        @Enumerated(EnumType.STRING)
        @Column(name = "redraw_importance", length = 60)
        protected ImportanceList importance;

        @Enumerated(EnumType.STRING)
        @Column(name = "redraw_risksExplained", length = 3)
        protected YesNoList risksExplained;


        public Redraw() {
        	// JPA
        }

        //Setter - Reason
		public void setReason(Reason reason) {
			if (reason != null ) {
				this.reason = reason;
				this.reason.setRedraw(this);
			}
		}
        
        
    }
    
    public FundsAccessTypeDetails() {
    	// JPA
    }
    
    // Setters - FundsAccessTypeDetails
	public void setOffsetAccount(OffsetAccount offsetAccount) {
		if (offsetAccount != null ) {
			this.offsetAccount = offsetAccount;
			this.offsetAccount.setFundsAccessTypeDetails(this);
		}
	}

	public void setRedraw(Redraw redraw) {
		if (redraw != null ) {
			this.redraw = redraw;
			this.redraw.setFundsAccessTypeDetails(this);
		}
	}

    
}