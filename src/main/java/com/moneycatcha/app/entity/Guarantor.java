package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.DocumentationDeliveryMethodList;
import com.moneycatcha.app.model.GuarantorAccessList;
import com.moneycatcha.app.model.IndependentAdviceTypeRequiredList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Guarantor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "lending_guarantee_id")
    protected LendingGuarantee lendingGuarantee;
	        
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "guarantor", cascade = CascadeType.ALL, orphanRemoval = true)
    protected Interview interview;
    
    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "guarantor", cascade = CascadeType.ALL, orphanRemoval = true)
    protected LoanWriterConfirmations loanWriterConfirmations;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "guarantor", cascade = CascadeType.ALL, orphanRemoval = true)
    protected SecurityGuarantee securityGuarantee;

    @Setter(AccessLevel.NONE)
    @OneToOne(mappedBy = "guarantor", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ServicingGuarantee servicingGuarantee;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected GuarantorAccessList accessType;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList advisedToSeekIndependentAdvice;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList confirmNotUnderPressure;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList demonstratesReadingEnglish;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected DocumentationDeliveryMethodList documentationDeliveryMethod;

    @Enumerated(EnumType.STRING)
    @Column(length = 60)
    protected IndependentAdviceTypeRequiredList independentAdviceTypeRequired;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList isUnderPowerOfAttorney;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectDifficultyUnderstandingEnglish;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectDifficultyUnderstandingObligations;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectMisrepresentation;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectUnderBorrowerInfluence;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectUnderPressure;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList suspectUnfairConduct;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList understandsGuaranteeSigningPeriod;

    @Enumerated(EnumType.STRING)
    @Column(length = 3)
    protected YesNoList understandsGuarantorObligations;

//    @OneToOne(mappedBy = "guarantor", cascade = CascadeType.ALL, orphanRemoval = true)
    @Column(columnDefinition = "VARCHAR(80)")
    protected String xGuarantor;

    public Guarantor() {
    	// JPA
    }
    
    // Setter - Interview
	public void setInterview(Interview interview) {
		if (interview != null) {
			this.interview = interview;
			this.interview.setGuarantor(this);
		}
	}

    // Setter - LoanWriterConfirmations
	public void setLoanWriterConfirmations(LoanWriterConfirmations loanWriterConfirmations) {
		if (loanWriterConfirmations != null) {
			this.loanWriterConfirmations = loanWriterConfirmations;
			this.loanWriterConfirmations.setGuarantor(this);
		}
	}

    // Setter - SecurityGuarantee
	public void setSecurityGuarantee(SecurityGuarantee securityGuarantee) {
		if (securityGuarantee != null) {
			this.securityGuarantee = securityGuarantee;
			this.securityGuarantee.setGuarantor(this);
		}
	}

    // Setter - ServicingGuarantee
	public void setServicingGuarantee(ServicingGuarantee servicingGuarantee) {
		if (servicingGuarantee != null) {
			this.servicingGuarantee = servicingGuarantee;
			this.servicingGuarantee.setGuarantor(this);
		}
	}
    
	@Data
	@Entity
	@Table(name = "loan_writer_confirmations")
    public static class LoanWriterConfirmations {
		
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;

	    @OneToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name = "guarantor_id")
	    protected Guarantor guarantor;
		
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList advisedDocumentsImminentIfSuccessful;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList advisedIdentificationProcessRequired;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList advisedInformationUsedInAssessment;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList advisedOfWithdrawalInstructions;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList completedStatementOfPosition;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList obtainedAgreementForJointStatementOfPosition;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList obtainedElectronicCommunicationConsent;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList obtainedSignedDeclaration;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList verifiedGuarantorIncome;

        public LoanWriterConfirmations() {
        	// JPA
        }
       
    }


    @Data
    @Entity
	@Table(name = "security_guarantee")
    public static class SecurityGuarantee {

	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;

	    @OneToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name = "guarantor_id")
	    protected Guarantor guarantor;
	    
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmCouldMeetLoanRepayments;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmWouldSellFamilyHome;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList understandsSecurityGuarantee;

        public SecurityGuarantee() {
        	// JPA
        }
 
    }


    @Data
    @Entity
	@Table(name = "servicing_guarantee")
    public static class ServicingGuarantee {
    	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;

	    @OneToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name = "guarantor_id")
	    protected Guarantor guarantor;
	    
        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmIsBenefitToFamily;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmRelationshipToBorrower;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmUnderstandsBorrowerReliance;

        @Enumerated(EnumType.STRING)
        @Column(length = 3)
        protected YesNoList confirmUnderstandsRepaymentsRequired;

        public ServicingGuarantee() {
        	// JPA
        }
    }


    
}