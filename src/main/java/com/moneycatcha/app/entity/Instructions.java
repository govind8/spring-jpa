package com.moneycatcha.app.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class Instructions {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "message_id")
//	@MapsId
	Message message;
	
	@Setter(AccessLevel.NONE)
	@OneToOne(mappedBy = "instructions", cascade = CascadeType.ALL, orphanRemoval = true)
	ApplicationInstructions applicationInstructions;	

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="instructions", cascade = CascadeType.ALL, orphanRemoval = true)
	List<ErrorInstructions> errorInstructions;	
	
	@Data
	@Entity
	@Table(name = "application_instructions")
	public static class ApplicationInstructions {
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;

		@OneToOne(fetch=FetchType.LAZY)
		@JoinColumn(name = "instructions_id")
		Instructions instructions;
		
		@Setter(AccessLevel.NONE)
		@OneToOne(mappedBy = "applicationInstructions", cascade = CascadeType.ALL, orphanRemoval = true)
		Submit submit;	

		@Setter(AccessLevel.NONE)
		@OneToOne(mappedBy="applicationInstructions", cascade = CascadeType.ALL, orphanRemoval = true)
		Update update;

		// Constructors - Instructions
		public ApplicationInstructions() {
			// JPA
		}
		
		// Setter - Submit
		public void setSubmit(Submit submit) {
			if (submit != null) {
				this.submit = submit;
				this.submit.setApplicationInstructions(this);
			}
		}

		// Setter - Update
		public void setUpdate(Update update) {
			if (update != null) {
				this.update = update;
				this.update.setApplicationInstructions(this);
			}
		}
		
	}
	
	// Constructors - Instructions
	public Instructions() {
		// JPA
	}

	// Setters - ApplicationInstructions
	public void setApplicationInstructions(ApplicationInstructions applicationInstructions) {
		if (applicationInstructions != null) {
			this.applicationInstructions = applicationInstructions;
			this.applicationInstructions.setInstructions(this);
		}
	}
	// Setters -  ErrorInstructions
	public void setErrorInstructions(List<ErrorInstructions> errorInstructions) {
		if (errorInstructions != null) {
			this.errorInstructions = errorInstructions;
			this.errorInstructions.forEach(x -> x.setInstructions(this));
		}
	}

	
}
