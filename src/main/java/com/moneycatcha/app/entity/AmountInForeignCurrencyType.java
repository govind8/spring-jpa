package com.moneycatcha.app.entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.moneycatcha.app.entity.AccountVariation.LimitIncrease;
import com.moneycatcha.app.entity.AccountVariation.ReduceLimit;
import com.moneycatcha.app.model.CurrencyCodeList;

import lombok.Data;

@Data
@Entity
public class AmountInForeignCurrencyType {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "limit_increase_id")
    protected LimitIncrease limitIncrease;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "reduce_limit_id")
    protected ReduceLimit reduceLimit;
    
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "liability_id")
    protected Liability liability;

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "loan_details_id")
    protected LoanDetails loanDetails;
    
	@Column(precision=19, scale=2)
    protected BigDecimal amount;

	@Enumerated(EnumType.STRING)
	@Column(length = 5)
    protected CurrencyCodeList currencyCode;

	@Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime exchangeDateTime;

	@Column(precision=19, scale=2)
    protected BigDecimal exchangeRate;
	
	public AmountInForeignCurrencyType() {
		// JPA
	}

    @java.lang.Override
    public java.lang.String toString() {
        return "AmountInForeignCurrencyType{" +
                "Id=" + Id +
                ", limitIncrease=" + limitIncrease +
                ", reduceLimit=" + reduceLimit +
                ", liability=" + liability +
                ", loanDetails=" + loanDetails +
                ", amount=" + amount +
                ", currencyCode=" + currencyCode +
                ", exchangeDateTime=" + exchangeDateTime +
                ", exchangeRate=" + exchangeRate +
                '}';
    }
}
