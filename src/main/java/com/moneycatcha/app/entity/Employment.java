package com.moneycatcha.app.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Employment {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
    protected PersonApplicant personApplicant;

    @OneToOne(mappedBy = "employment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected ForeignEmployed foreignEmployed;
    
    @OneToOne(mappedBy = "employment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected NotEmployed notEmployed;

    @OneToOne(mappedBy = "employment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected PAYG pAYG;

    @OneToOne(mappedBy = "employment", cascade = CascadeType.ALL, orphanRemoval = true)
    protected SelfEmployed selfEmployed;

    // default constructor
    public Employment() {
    	//JPA
    }

    // Setters - ForeignEmployed
    public void setForeignEmployed(ForeignEmployed foreignEmployed) {
    	if (foreignEmployed != null) {
	    	foreignEmployed.setEmployment(this);
	        this.foreignEmployed = foreignEmployed;    	
    	}
    }
    
    // Setters - NotEmployed
    public void setNotEmployed(NotEmployed notEmployed) {
    	if (notEmployed != null) {
	    	notEmployed.setEmployment(this);
	        this.notEmployed = notEmployed;    	
    	}
    }
    
    // Setters - Payg
    public void setPAYG(PAYG pAYG) {
    	if (pAYG != null) {
	        this.pAYG = pAYG;    	
	    	this.pAYG.setEmployment(this);
    	}
    }
    
    // Setters - SelfEmployed
    public void setSelfEmployed(SelfEmployed selfEmployed) {
    	if (selfEmployed != null) {
	        this.selfEmployed = selfEmployed;    	
	    	this.selfEmployed.setEmployment(this);
    	}
    }
 
}
