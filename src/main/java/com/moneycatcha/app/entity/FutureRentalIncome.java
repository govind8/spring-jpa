package com.moneycatcha.app.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.moneycatcha.app.model.FrequencyFullList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class FutureRentalIncome {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;

    @ManyToOne( fetch = FetchType.LAZY )
    @JoinColumn( name = "real_estate_asset_id" )
    protected RealEstateAsset realEstateAsset;
	
	@Setter(AccessLevel.NONE)
    @OneToMany(mappedBy = "futureRentalIncome", cascade = CascadeType.ALL, orphanRemoval = true)
    protected List<PropertyPart> propertyPart;

    @Column(precision=19, scale=2)
    protected BigDecimal grossRentalAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyFullList grossRentalFrequency;

    @Column(precision=19, scale=2)
    protected BigDecimal netRentalAmount;

    @Enumerated(EnumType.STRING)
    @Column(length = 15)
    protected FrequencyFullList netRentalFrequency;

    @Column(columnDefinition = "DATE")
    protected LocalDate startDate;

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xOwner;
    
    
    public FutureRentalIncome() {
    	//JPA
    }
    
    // Setters - PropertyPart
	public void setPropertyPart(List<PropertyPart> propertyPart) {
		if (propertyPart != null) {
			this.propertyPart = propertyPart;
			this.propertyPart.forEach(x -> x.setFutureRentalIncome(this));
		}
	}
    
}