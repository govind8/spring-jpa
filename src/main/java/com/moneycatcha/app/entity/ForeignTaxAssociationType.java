
package com.moneycatcha.app.entity;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.ForeignTaxAssociationStatusList;
import com.moneycatcha.app.model.TinNotProvidedReasonList;
import com.moneycatcha.app.model.YesNoList;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

@Data
@Entity
public class ForeignTaxAssociationType {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
	
    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    

    @OneToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "person_applicant_id")
    protected PersonApplicant personApplicant;    
    
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "related_person_id")
	protected RelatedPerson relatedPerson;    
 
	@OneToOne(fetch=FetchType.LAZY)
	@JoinColumn(name = "trust_applicant_id")
	TrustApplicant trustApplicant;    
	
 	@Enumerated(EnumType.STRING)
	@Column(length = 3)
    protected YesNoList selfCertificationDeclaration;

	@Enumerated(EnumType.STRING)
	@Column(length = 60)
    protected ForeignTaxAssociationStatusList status;

	@Setter(AccessLevel.NONE)
	@OneToMany(mappedBy="foreignTaxAssociationType", cascade=CascadeType.ALL, orphanRemoval=true)
	protected List<Detail> detail;
	
	public ForeignTaxAssociationType() {
		// default constructor - jpa
	}
	
    // Setters - Detail
    public void setDetail(List<Detail> detail) {
    	if (detail != null) {
	        this.detail = detail;    	
	    	this.detail.forEach(x -> x.setForeignTaxAssociationType(this));
    	}
    }    


	@Data
    @Entity
    @Table(name = "foreign_tax_association_detail")
    public static class Detail {
		
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    protected Long Id;
	    
	    @ManyToOne(fetch=FetchType.LAZY)
	    @JoinColumn(name = "foreign_tax_association_type_id")
	    protected ForeignTaxAssociationType foreignTaxAssociationType;    
		
		
        @Enumerated(EnumType.STRING)
        @Column(length = 5)
        protected CountryCodeList country;

        @Enumerated(EnumType.STRING)
        @Column(length = 60)
        protected ForeignTaxAssociationStatusList status;

    	@Column(columnDefinition = "VARCHAR(80)")
        protected String taxIdentificationNumber;

        @Enumerated(EnumType.STRING)
        @Column(length = 255)
        protected TinNotProvidedReasonList tinNotProvidedReason;

    	@Column(columnDefinition = "VARCHAR(255)")
        protected String tinNotProvidedReasonDetail;
    	
    	public Detail() {
    		// JPA
    	}

	  
    }
}
