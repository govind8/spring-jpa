package com.moneycatcha.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;

@Data
@Entity
public class Director {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long Id;
    
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name = "company_applicant_id")
    protected CompanyApplicant companyApplicant;    

    @Column(columnDefinition = "VARCHAR(80)")
    protected String xDirector;
    

    public Director() {
    	// JPA
    }
}
