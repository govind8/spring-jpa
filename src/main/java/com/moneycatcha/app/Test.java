package com.moneycatcha.app;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
			
			String year = String.valueOf(1999);
			XMLGregorianCalendar xyear;
			try {
				xyear = DatatypeFactory.newInstance().newXMLGregorianCalendar(year);
		        System.out.println("year " + xyear.getYear());
		        System.out.println("xyear : " + xyear.toXMLFormat());
			} catch (DatatypeConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			String xdate = "2019-08-05T02:51:38.688780";
			String xdate = "2019-12-31T00:00:00";   //2019-08-05T02:51:38.688780
			XMLGregorianCalendar xmlGregorianCalendar;
			LocalDateTime localDateTime;
			try {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
				xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(xdate);
				String localDate = xmlGregorianCalendar.toGregorianCalendar().toZonedDateTime().toLocalDateTime().format(formatter);
					
				System.out.println("date :" + localDate.toString());

				localDateTime = LocalDateTime.parse(localDate, formatter);
					
				System.out.println("xml -> localdatetime : " + localDateTime.toString());
				
				System.out.println("xml -> localdatetime format : " + localDateTime.format(formatter).toString());			
				
				XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(localDateTime.format(formatter).toString());
				
				System.out.println("local -> xml : " + xmlDate.toString());
				
				
			} catch (DatatypeConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
//		}
		
	}

}
