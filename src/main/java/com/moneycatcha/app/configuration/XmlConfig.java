package com.moneycatcha.app.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.moneycatcha.app.entity.Message;

@Configuration
public class XmlConfig {

	@Bean
	public Jaxb2Marshaller jabx2Marshaller() {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		String[] packagesToScan = {"com.moneycatcha.app.model", "com.moneycatcha.app.entity"};
		jaxb2Marshaller.setPackagesToScan(packagesToScan);
//		jaxb2Marshaller.setPackagesToScan("com.moneycatcha.app.model");
		return jaxb2Marshaller;
	}
	
	
}
