package com.moneycatcha.app.parsers.validation;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Component
public class ParseLixi extends LixiUtils {
	
	private static HashMap<String, String> resultMap = new HashMap<>();
	private static int depthOfXML = 1;
	private static Logger logger = LoggerFactory.getLogger(ParseLixi.class);
	
	/**
	 * clear hash map
	 */
	public ParseLixi() {
		resultMap.clear();
	}
	
	/**
	 * Traverse the XML tree and if element exists fetch the element's attributes and child nodes.
	 * @param baseNode
	 */
	public HashMap<String, String> getChildElements(String xml, String elementName) {
		int level = 1;
		
		try {
			if (elementName.trim() != null) {
				Document document = stringToXmlDocument(xml);
				document.getDocumentElement().normalize();
	
				Node baseNode =  document.getElementsByTagName(elementName).item(0);
	
				if (baseNode.getNodeType() == Node.ELEMENT_NODE) {
					
					getNodeNameValue(baseNode);
					
					if (baseNode.hasAttributes()) {
	
	
						resultMap.putAll(getAttributes(baseNode));
					}
					level++;
					if (baseNode.hasChildNodes()) {
						getChildNodes(baseNode.getChildNodes(), level);
					}
				}
				logger.info(String.format("total attributes for %s is %s.", elementName, resultMap.size()));				
				resultMap.forEach((k, v) -> System.out.println("attributes " + k + ":" + v));				
			}			

		} catch(Exception e) {
			logger.error(String.format("parse exception: %s", e.getMessage())); 
			return null;
		}		
		
		return resultMap;
	}
	
	/**
	 * Parse through each node element recursively until all child elements are traversed.
	 * Fetch the values of each node attributes name:value and store it in Hashmap
	 * 
	 * @param nodeList
	 * @param level
	 */
	private void getChildNodes(NodeList nodeList, int level) {
		
		level++;
		
		if (nodeList != null && nodeList.getLength() > 0) {
			
			for (int i=0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);
				
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					
					getNodeNameValue(node);
					
					if (node.hasAttributes()) {
						resultMap.putAll(getAttributes(node));
					}
					
					getChildNodes(node.getChildNodes(), level);
					
					if (level > depthOfXML) {
						depthOfXML = level;
					}
				}
			}
		}		
	}
	
	/**
	 * Fetch all the attributes name:value for the element and store it in HashMap
	 * @param node
	 * @return
	 */
	public HashMap<String, String> getAttributes(Node node) {
		HashMap<String, String> attrMap = new HashMap<String, String>();
		if (node.hasAttributes()) {
			NamedNodeMap attributes = node.getAttributes();
			for(int i=0; i < attributes.getLength(); i++) {

				String key = node.getNodeName() + ":";
				key += attributes.item(i).getNodeName();
				String value = attributes.item(i).getNodeValue();
				attrMap.put(key, value);
			}
		}
		return attrMap;
	}
	
	
	/**
	 * Get the node name and value if exists only.
	 * @param node
	 */
	public void getNodeNameValue(Node node) {
		
		if (node.getNodeValue() != null) {

			resultMap.put(node.getLocalName(), node.getNodeValue());
		}
	}
	
	/**
	 * Traverse the XML tree and if element exists fetch the element's attributes .
	 * @param baseNode
	 */
	public HashMap<String, String> getAttributes(String xml, String elementName) {

		HashMap<String, String> attributesMap = new HashMap<>();
		
		try {
			if (elementName.trim() != null) {
				Document document = stringToXmlDocument(xml);
				document.getDocumentElement().normalize();
	
				Node baseNode =  document.getElementsByTagName(elementName).item(0);
	
				if (baseNode.getNodeType() == Node.ELEMENT_NODE) {
					
					getNodeNameValue(baseNode);
					
					if (baseNode.hasAttributes()) {
						NamedNodeMap nodeAttributes = baseNode.getAttributes();
						for (int i=0; i < nodeAttributes.getLength(); i++) {
							attributesMap.put(nodeAttributes.item(i).getNodeName(), nodeAttributes.item(i).getNodeValue());
						}
					}
				}
				logger.info(String.format("total attributes for %s is %s.", elementName, attributesMap.size()));				
				attributesMap.forEach((k, v) -> System.out.println("attributes " + k + ":" + v));				
			}			

		} catch(Exception e) {
			logger.error(String.format("parse exception: %s", e.getMessage())); 
			return null;
		}		
		
		return attributesMap;
	}	

	/**
	 * Traverse the XML tree, check if element exists .
	 * @param baseNode
	 */
	public boolean isElementPresent(String xml, String elementName) {

		try {
			if (elementName.trim() != null) {
				Document document = stringToXmlDocument(xml);
				document.getDocumentElement().normalize();
	
				Node baseNode =  document.getElementsByTagName(elementName).item(0);
	
				if (baseNode != null && baseNode.hasAttributes()) {
					logger.info(String.format("node is present with name is %s.", baseNode.getNodeName()));				
					return true;
				}
			}			

		} catch(Exception e) {
			logger.error(String.format("parse exception: %s", e.getMessage())); 
		}		
		
		return false;
	}		
}
