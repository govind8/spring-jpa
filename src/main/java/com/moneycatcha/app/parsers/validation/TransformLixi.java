package com.moneycatcha.app.parsers.validation;

import java.io.IOException;
import java.io.StringWriter;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.moneycatcha.app.exception.EntityNotFoundException;

@Component
public class TransformLixi {

	private static Logger logger = LoggerFactory.getLogger(TransformLixi.class);

	@Autowired
	ValidateLixi validateLixi;

	@Autowired
	LixiUtils lixiUtils;
	
	DOMSource source;
	TransformerFactory factory;
    Transformer transformer;	
	/**
	 * Transform the XML to another type using XSL.
	 * @param xml
	 * @return
	 * @throws EntityNotFoundException 
	 * @throws TransformerException 
	 */
	public String transformXML(String sxml) {
		
		StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    String xslname = "/data/transform.xsl";
	    
	    try {
			source = new DOMSource(lixiUtils.getXmlDocument(sxml));
		    
		    factory = TransformerFactory.newInstance();
		    transformer = factory.newTransformer(new StreamSource(lixiUtils.getFile(xslname)));
		    transformer.transform(source, result);
		    return writer.getBuffer().toString();
	    } catch (Exception e) {
	    	logger.error("{} ::transformXML:  {}", e.getClass(), e.getMessage());
	    	throw new RuntimeException(e);
	    }
 	}
	
	/**
	 * Extract the child element (xml node) from the main xml and return xml as string.
	 * Specify the element to extract in xpath expresion 
	 * exampe: PersonApplicant - will fetch the node PersonApplicant and its child node.
	 * @param xml
	 * @param PersonApplicant
	 * @return
	 * @throws XPathExpressionException
	 * @throws TransformerFactoryConfigurationError
	 * @throws TransformerException
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public String getChildElement(String xml, String childNode) throws EntityNotFoundException {
		
		Document document = null;
		StringWriter writer = new StringWriter();
	    XPath xPath = XPathFactory.newInstance().newXPath();
	    Node node;
	    
	
		try {
			document = lixiUtils.stringToXmlDocument(xml);
			if (document != null && childNode != null && !(childNode.trim()).equals("")) {
				node = (Node)xPath.evaluate("//" + childNode, document, XPathConstants.NODE);
	
				factory = TransformerFactory.newInstance();
				factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
				
				transformer = factory.newTransformer();
			    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
				transformer.transform(new DOMSource(node), new StreamResult(writer));
				
			    return(writer.toString());	
			}
			return "The element provided " + childNode + " is not valid";
	    } catch (XPathExpressionException | TransformerException e) {
			logger.error("getChildXmlAsString {} ", e);

			throw new EntityNotFoundException(e.getClass(), e.getMessage(), e.getLocalizedMessage());
		}
	}
	
	/**
	 * 
	 * @param <T>
	 * @param xml
	 * @param typeParameter
	 * @return
	 * @throws EntityNotFoundException 
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public <T> T deserializeXml(String xml, Class<T> typeParameter) throws EntityNotFoundException  {
   	
    	try {
			XmlMapper xmlMapper = new XmlMapper();
	    	xmlMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	    	xmlMapper.setPropertyNamingStrategy(PropertyNamingStrategy.LOWER_CAMEL_CASE);

			return  (T) xmlMapper.readValue(xml, typeParameter); 
		} catch (IOException e) {
	    	logger.error("xml deserialize error : {} ", e);
	    	throw new EntityNotFoundException(e.getClass(), e.getLocalizedMessage(), typeParameter.getSimpleName());
		}
 	}
}
