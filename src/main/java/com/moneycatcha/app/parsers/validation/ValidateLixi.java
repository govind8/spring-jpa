package com.moneycatcha.app.parsers.validation;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import com.helger.schematron.ISchematronResource;
import com.helger.schematron.pure.SchematronResourcePure;
import com.moneycatcha.app.exception.EntityNotFoundException;
import lombok.NonNull;

@Component
public class ValidateLixi{
	
	@Autowired
	LixiUtils lixiUtils;

	private static Logger logger = LoggerFactory.getLogger(ValidateLixi.class);
	
	private String slixiSchema = "/xsd/LIXI-CAL-2_6_21.xsd";
	
	public boolean isValid(@NonNull String xml) throws EntityNotFoundException {
		
		try {
        	SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
         	Schema schema = schemaFactory.newSchema(lixiUtils.getFile(slixiSchema));
            
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new StringReader(xml)));
            return true;
            
 	    } catch (IOException | SAXException e ){
	    	logger.error("xml validation error: {} ", e);
	    	throw new EntityNotFoundException(this.getClass(), e.getMessage() + " in LIXI_CAL schema.", "The xml payload is not valid");
		}
	}
	
	
	/**
	 * Validate with PureSchematron
	 * @param aSchematronFile
	 * @param aXMLFile
	 * @return
	 * @throws Exception
	 */
	public static boolean validateXMLViaPureSchematron (@NonNull final File aSchematronFile, @NonNull final File aXMLFile) throws Exception
	{
		final ISchematronResource aResPure = SchematronResourcePure.fromFile (aSchematronFile);
		if (!aResPure.isValidSchematron ())
			throw new IllegalArgumentException ("Invalid Schematron!");
		return aResPure.getSchematronValidity(new StreamSource(aXMLFile)).isValid ();
	}
	
	
}
