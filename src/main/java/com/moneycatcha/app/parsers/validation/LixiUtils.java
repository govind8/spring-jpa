package com.moneycatcha.app.parsers.validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Objects;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.moneycatcha.app.exception.EntityNotFoundException;

@Component
public class LixiUtils {


	static Logger logger = LoggerFactory.getLogger(LixiUtils.class);
	
	/**
	 * Fetch the file from the resources directory and return the full dir path
	 * @param filename
	 * @return
	 * @throws FileNotFoundException
	 */
	public File getFile(String filename) throws EntityNotFoundException {
		try {
	        Resource resource = new ClassPathResource(filename);
	        Objects.requireNonNull(resource);
 			return resource.getFile();
		} catch (IOException e) {
			logger.error("failed to find transform.xsl {} ", e);
		}
		return null;
     }
	

	/**
	 * Convert string to XML Document object.
	 * @param xml
	 * @return
	 * @throws EntityNotFoundException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws FileNotFoundException 
	 * @throws ParserConfigurationException 
	 */
	public Document stringToXmlDocument(String xml) throws EntityNotFoundException {
		

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = null;
		
		
		try {
			builder = factory.newDocumentBuilder();
			return builder.parse(new InputSource(new StringReader(xml)));
		} catch (Exception e) {
			logger.info("xml reader info {} ", e.getMessage(), e.getLocalizedMessage());
			logger.error("xml reader error {} ", e);
			throw new EntityNotFoundException(e.getClass(), e.getMessage(), e.getLocalizedMessage());
		}
	}

	/**
	 * Convert the xml file into xml document
	 * @param document
	 * @return
	 */
	public Document getXmlDocument(String filePath) throws EntityNotFoundException
    {
        
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
         
        
        DocumentBuilder builder = null;
        try
        {
            
            builder = factory.newDocumentBuilder();
             
            
            return builder.parse(new File(filePath));
        }
        catch (Exception e)
        {
        	throw new EntityNotFoundException(e.getClass(), e.getMessage(), e.getLocalizedMessage());

        }
     }
	
	/**
	 * Convert XML into String. Read the content from xml file.
	 * @return
	 */
	
	public String xmlAsString(String sfilePath) throws IOException {

		File xmlFile = new File(sfilePath);
		Reader fileReader = new FileReader(xmlFile); 
		BufferedReader bufferedReader = new BufferedReader(fileReader); 
		StringBuilder sb = new StringBuilder(); 
		String line = bufferedReader.readLine(); 
		while( line != null) { 
			sb.append(line).append("\n"); 
			line = bufferedReader.readLine(); 
		}
		String xml2String = sb.toString(); 
		System.out.println("XML to String using BufferedReader : \n" + xml2String); 
		bufferedReader.close();
		return xml2String;
	}

	
}
