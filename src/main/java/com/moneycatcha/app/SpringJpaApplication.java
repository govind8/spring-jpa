package com.moneycatcha.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.moneycatcha.app.repository.MessageRepository;
import com.moneycatcha.app.service.MessageService;
import com.moneycatcha.app.testdata.MessageData;


@SpringBootApplication
@EnableJpaAuditing
public class SpringJpaApplication implements CommandLineRunner {

	@Autowired
	MessageService messageService;

	@Autowired
	MessageRepository messageRepository;
	
	
	public static void main(String[] args) {
		SpringApplication.run(SpringJpaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		  MessageData messageData = new MessageData();
		  
		  messageService.save(messageData.message());
	  
		 		 
	}


}
