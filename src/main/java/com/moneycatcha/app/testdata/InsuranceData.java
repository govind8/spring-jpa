package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.AssociatedLoanAccount;
import com.moneycatcha.app.entity.Insurance;
import com.moneycatcha.app.entity.Insurance.CommissionPayable;
import com.moneycatcha.app.entity.Insurance.InsuredParty;
import com.moneycatcha.app.entity.Insurance.Premium;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.InsuranceTypeList;
import com.moneycatcha.app.model.InsurerList;

import lombok.Data;


@Data
public class InsuranceData {

	// Insurance List
	public List<Insurance> insurances() {
		List<Insurance> insurances = new ArrayList<Insurance>();
		for(int i=0; i < 4; i++) {
			Insurance insurance = new Insurance();
			insurance.setAssociatedLoanAccount(accounts());
			insurance.setCommissionPayable(payable());
			insurance.setDescription(RandomStringUtils.randomAlphabetic(250));
			insurance.setEffectiveDate(LocalDate.now().minusYears(1));
			insurance.setExpiryDate(LocalDate.now().plusYears(2));
			insurance.setInsuranceType(InsuranceTypeList.LIFE_INSURANCE);
			insurance.setInsuredAmount(new BigDecimal(RandomStringUtils.randomNumeric(10)));
			insurance.setInsuredParty(parties());
			insurance.setInsurer(InsurerList.AUSTRALIAN_PENSIONERS_INSURANCE);
			insurance.setOtherInsurerName(RandomStringUtils.randomAlphabetic(35));
			insurance.setPolicyNumber(RandomStringUtils.randomAlphabetic(15));
			insurance.setPolicyNumber(RandomStringUtils.randomAlphabetic(15));
			insurance.setPremium(premium());
			insurance.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			insurances.add(insurance);
		}
		return insurances;
	}
	
	// Commission Payable
	public CommissionPayable payable() {
		CommissionPayable payable = new CommissionPayable();
		payable.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(10)));
		payable.setPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		payable.setXPayer(RandomStringUtils.randomAlphanumeric(15));
		return payable;
	}
	
	// AssociatedLoanAccount
	public List<AssociatedLoanAccount> accounts() {
		List<AssociatedLoanAccount> accounts = new ArrayList<>();
		for(int i=0; i < 4; i++) {
			AssociatedLoanAccount associated = new AssociatedLoanAccount();
			associated.setXAssociatedLoanAccount(RandomStringUtils.randomAlphanumeric(15));
			accounts.add(associated);
		}
		
		return accounts;
	}
	
	// Insured Parties
	public List<InsuredParty> parties() {
		List<InsuredParty> parties = new ArrayList<InsuredParty>();
		for(int i=0; i < 4; i++) {
			InsuredParty party = new InsuredParty();
			party.setXInsuredParty(RandomStringUtils.randomAlphanumeric(15));
			parties.add(party);
		}
		return parties;

	}
	
	// Premium premium
	public Premium premium() {
		Premium premium = new Premium();
		premium.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(10)));
		premium.setFrequency(FrequencyShortList.WEEKLY);
		return premium;
	}
	
}
