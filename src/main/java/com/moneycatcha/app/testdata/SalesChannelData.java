package com.moneycatcha.app.testdata;

import java.math.BigDecimal;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Aggregator;
import com.moneycatcha.app.entity.Commission;
import com.moneycatcha.app.entity.Company;
import com.moneycatcha.app.entity.LoanWriter;
import com.moneycatcha.app.entity.SalesChannel;
import com.moneycatcha.app.entity.SalesChannel.Introducer;
import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.LicenceTypeList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.SalesChannelTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class SalesChannelData {

	ContactData contactData = new ContactData();
	
	// SalesChannel
	public SalesChannel salesChannel() {
		
		SalesChannel channel = new SalesChannel();
		channel.setAggregator(aggregator()); // object
		channel.setCommission(commission()); // object
		channel.setCompany(company()); // object
		channel.setIntroducer(introducer()); // object
		channel.setLoanWriter(loanWriter()); // object
		channel.setType(SalesChannelTypeList.CALL_CENTRE);
			
		return channel;
	}
	
	// Aggregator
	public Aggregator aggregator() {
		Aggregator aggregator = new Aggregator();
		aggregator.setAccreditationNumber(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setContact(contactData.SalesChannelContact()); // Object
		aggregator.setLicenceNumber(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setLicenceType(LicenceTypeList.CRN);
		aggregator.setOtherIdentifier(RandomStringUtils.randomAlphanumeric(15));
		aggregator.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		return aggregator;
	}
	
	// Commission
	public Commission commission() {
		Commission commission = new Commission();
		commission.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		commission.setCommissionPaid(YesNoList.NO);
		commission.setCommissionStructure(CommissionStructureList.OTHER);
		commission.setGstAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		commission.setItcAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		commission.setOtherCommissionStructureDescription(RandomStringUtils.randomAlphabetic(15));
		commission.setPromotionCode(RandomStringUtils.randomAlphabetic(15));
		commission.setThirdPartyReferee(YesNoList.NO);
		commission.setTrail(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		commission.setUpfrontPayment(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));

		return commission;
		
	}
	
	// Introducer
	public Introducer introducer() {
		Introducer introducer = new Introducer();
		introducer.setCompanyName(RandomStringUtils.randomAlphabetic(15));
		introducer.setContactName(RandomStringUtils.randomAlphabetic(15));
		introducer.setIntroducerID(RandomStringUtils.randomAlphabetic(15));
		return introducer;
	}
	
	// Company
	public Company company() {
		
		Company company = new Company();
		company.setAbn(RandomStringUtils.randomAlphabetic(15));
		company.setAccreditationNumber(RandomStringUtils.randomAlphanumeric(15));
		company.setBsb(RandomStringUtils.randomAlphanumeric(3) + "-" + RandomStringUtils.randomAlphanumeric(3));
		company.setBusinessName(RandomStringUtils.randomAlphabetic(15));
		company.setBusinessNameSameAsCompanyName(YesNoList.NO);
		company.setCompanyName(RandomStringUtils.randomAlphabetic(15));
		company.setContact(contactData.SalesChannelContact()); // object
		company.setLicenceNumber(RandomStringUtils.randomAlphanumeric(15));
		company.setLicenceType(LicenceTypeList.ACL);
		company.setOtherIdentifier(RandomStringUtils.randomAlphanumeric(15));
		company.setUniqueID(RandomStringUtils.randomAlphanumeric(15));

		return company;
	}
	
	// LoanWriter
	public LoanWriter loanWriter() {
		LoanWriter loanWriter = new LoanWriter();
		loanWriter.setAccreditationNumber(RandomStringUtils.randomAlphanumeric(15));
		loanWriter.setContact(contactData.SalesChannelContact());
		loanWriter.setDepartment(RandomStringUtils.randomAlphabetic(15));
		loanWriter.setFirstName(RandomStringUtils.randomAlphabetic(15));
		loanWriter.setLicenceNumber(RandomStringUtils.randomAlphanumeric(15));
		loanWriter.setLicenceType(LicenceTypeList.CRN);
		loanWriter.setManagerName(RandomStringUtils.randomAlphabetic(15));
		loanWriter.setNameTitle(NameTitleList.MRS);
		loanWriter.setOtherIdentifier(RandomStringUtils.randomAlphanumeric(15));
		loanWriter.setPersonRole(RandomStringUtils.randomAlphabetic(15));
		loanWriter.setSurname(RandomStringUtils.randomAlphabetic(15));
		loanWriter.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		return loanWriter;
	}
	
	
}
