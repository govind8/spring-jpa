package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ExistingCustomer;
import com.moneycatcha.app.entity.Overview;
import com.moneycatcha.app.entity.SignatureType;
import com.moneycatcha.app.entity.Overview.BranchDomicile;
import com.moneycatcha.app.entity.Overview.BranchSign;
import com.moneycatcha.app.entity.Overview.BranchStamp;
import com.moneycatcha.app.entity.Overview.BridgingFinance;
import com.moneycatcha.app.entity.SignatureType.Capacity;
import com.moneycatcha.app.model.ApplicationDocTypeList;
import com.moneycatcha.app.model.ApplicationTypeList;
import com.moneycatcha.app.model.CapacityTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class OverviewData {

	ContactData contactData = new ContactData();
	
	public Overview overview() {
		
		Overview overview = new Overview();
		overview.setApplicationType(ApplicationTypeList.DEPOSIT_ACCOUNT);
		overview.setBranchDomicile(branchDomicile()); //object
		overview.setBranchSign(branchSign()); //object
		overview.setBranchStamp(branchStamp()); //object
		overview.setBridgingFinance(bridgingFinance()); //object
		overview.setBrokerApplicationReferenceNumber(RandomStringUtils.randomAlphabetic(15));
		overview.setBrokerApplicationSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(5)));
		overview.setCombinationLoan(YesNoList.NO);
		overview.setDocType(ApplicationDocTypeList.FULL_DOC);
		overview.setExpectedSettlementDate(LocalDate.now().plusYears(1));
		overview.setExistingCustomer(customers()); //object
		overview.setFastRefinance(YesNoList.NO);
		overview.setHasSpecialCircumstances(YesNoList.NO);
		overview.setIsBridgingFinance(YesNoList.NO);
		overview.setLenderApplicationReferenceNumber(RandomStringUtils.randomAlphabetic(15));
		overview.setLenderPreapprovalReferenceNumber(RandomStringUtils.randomAlphabetic(15));
		overview.setLinkedCommercialApplication(YesNoList.NO);
		overview.setLodgementReferenceNumber(RandomStringUtils.randomAlphabetic(15));
		overview.setLodgementSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(5)));
		overview.setPrivateBanking(YesNoList.NO);
		overview.setProPack(YesNoList.NO);
		overview.setSignature(signatureType());  //object
		overview.setSmsfLoan(YesNoList.NO);
		overview.setUrgent(YesNoList.NO);
		overview.setXMainContactPoint(RandomStringUtils.randomAlphabetic(15));
		return overview;
	}
	
	// BranchDomicile
	public BranchDomicile branchDomicile() {
		BranchDomicile domicile = new BranchDomicile();
		domicile.setBsb(RandomStringUtils.randomNumeric(3) + "-" + RandomStringUtils.randomNumeric(3));
		domicile.setContact(contactData.branchContact());
		domicile.setInternalName(RandomStringUtils.randomAlphabetic(15));
		domicile.setInternalNumber(RandomStringUtils.randomAlphabetic(15));
		return domicile;
	}
	
	// BranchSign
	public BranchSign branchSign() {
		BranchSign sign = new BranchSign();
		sign.setBsb(RandomStringUtils.randomNumeric(3) + "-" + RandomStringUtils.randomNumeric(3));
		sign.setContact(contactData.branchContact());
		sign.setInternalName(RandomStringUtils.randomAlphabetic(15));
		sign.setInternalNumber(RandomStringUtils.randomAlphabetic(15));
		return sign;
	}
	
	// BranchStamp
	public BranchStamp branchStamp() {
		BranchStamp stamp = new BranchStamp();
		stamp.setStamped(YesNoList.NO);
		stamp.setStampRequired(YesNoList.YES);
		return stamp;
	}
	
	// BridgingFinance
	public BridgingFinance bridgingFinance() {
		BridgingFinance bridging = new BridgingFinance();
		bridging.setBridgingTerm(new BigInteger(RandomStringUtils.randomNumeric(5)));
		bridging.setCapitalisedInterestAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		bridging.setCapitaliseInterest(YesNoList.YES);
		bridging.setEndDebt(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		bridging.setPeakDebt(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		return bridging;
	}
	
	public List<ExistingCustomer> customers() {
		List<ExistingCustomer> ecustomers = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			ExistingCustomer ec = new ExistingCustomer();
			ec.setCustomerNumber(RandomStringUtils.randomAlphanumeric(15));
			ec.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			ecustomers.add(ec);
		}
		return ecustomers;
	}
	
	// SignatureType
	public List<SignatureType> signatureType() {
		
		List<SignatureType> signatures = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			
			Capacity capacity = new Capacity();
			capacity.setType(CapacityTypeList.BANKER);
			capacity.setXParentEntity(RandomStringUtils.randomAlphabetic(15));
			capacity.setXProductSet(RandomStringUtils.randomAlphabetic(15));
			
			SignatureType signatureType = new SignatureType();
			signatureType.setCapacity(capacity);
			signatureType.setContact(contactData.signatureTypeContact());
			signatureType.setDate(LocalDate.now());
			signatureType.setElectronicSignature(YesNoList.NO);
			signatureType.setRank(new BigInteger(RandomStringUtils.randomNumeric(1)));
			signatureType.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			signatureType.setXSignatory(RandomStringUtils.randomAlphabetic(15));
			signatures.add(signatureType);
		
		}
		return signatures;
	}
	
}
