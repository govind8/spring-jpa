package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ForeignTaxAssociationType;
import com.moneycatcha.app.entity.ForeignTaxAssociationType.Detail;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.ForeignTaxAssociationStatusList;
import com.moneycatcha.app.model.TinNotProvidedReasonList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class ForeignTaxAssociationData {

	public ForeignTaxAssociationType foreignTaxAssociation() {
		
		List<Detail> details = new ArrayList<>();
		for (int i=0; i<2; i++) {
			Detail detail = new Detail();
			detail.setCountry(CountryCodeList.AM);
			detail.setStatus(ForeignTaxAssociationStatusList.NOT_PROVIDED);
			detail.setTaxIdentificationNumber(RandomStringUtils.randomAlphanumeric(10));
			detail.setTinNotProvidedReason(TinNotProvidedReasonList.COUNTRY_OF_TAX_ASSOCIATION_DOES_NOT_ISSUE_A_TIN);
			detail.setTinNotProvidedReasonDetail(RandomStringUtils.randomAlphabetic(10));
			details.add(detail);
		}
		
		ForeignTaxAssociationType foriegnType = new ForeignTaxAssociationType();
		foriegnType.setDetail(details);
		foriegnType.setSelfCertificationDeclaration(YesNoList.NO);
		foriegnType.setStatus(ForeignTaxAssociationStatusList.NONE);
		
		return foriegnType;
	}
}
