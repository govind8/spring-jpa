package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.FinancialAccountType;
import com.moneycatcha.app.entity.FinancialAccountType.BranchDomicile;

import lombok.Data;

@Data
public class FinancialAccountTypeData {

	
	/****************************************************
	 * common functions 
	 * @return
	 ****************************************************/
	public FinancialAccountType accountNumber() {
		
		BranchDomicile branchDomicile = new BranchDomicile(); //"govindraj karuppiah", "bd-200002"
		branchDomicile.setInternalName(RandomStringUtils.randomAlphabetic(15));
		branchDomicile.setInternalNumber(RandomStringUtils.randomAlphanumeric(10));
		
		FinancialAccountType financialAccountType = new FinancialAccountType();
		financialAccountType.setAccountName(RandomStringUtils.randomAlphabetic(15));
		financialAccountType.setAccountNumber(RandomStringUtils.randomNumeric(10));
		financialAccountType.setAccountTypeName(RandomStringUtils.randomAlphabetic(10));
		financialAccountType.setBsb(RandomStringUtils.randomNumeric(3) + "-" + RandomStringUtils.randomNumeric(3));
		financialAccountType.setFinancialInstitution(RandomStringUtils.randomAlphabetic(10));
		financialAccountType.setOtherFIName(RandomStringUtils.randomAlphabetic(10));
		
		return financialAccountType;
	}
	
}
