package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.FundsAccessTypeDetails;
import com.moneycatcha.app.entity.InterestRateTypeDetails;
import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.entity.Preferences;
import com.moneycatcha.app.entity.Reason;
import com.moneycatcha.app.entity.RepaymentTypeDetails;
import com.moneycatcha.app.entity.FundsAccessTypeDetails.Redraw;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedAndVariableRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.VariableRate;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestInAdvance;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestOnly;
import com.moneycatcha.app.entity.RepaymentTypeDetails.LineOfCredit;
import com.moneycatcha.app.entity.RepaymentTypeDetails.PrincipalAndInterest;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.FundsAccessTypeList;
import com.moneycatcha.app.model.ImportanceList;
import com.moneycatcha.app.model.InterestRateTypeList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class PreferencesData {

	DurationTypeData durationData = new DurationTypeData();
	
	// NeedsAnalysis -> Preferences 
	public Preferences preferences() {
		Preferences preferences = new Preferences();
		preferences.setConflictDescription(RandomStringUtils.randomAlphabetic(15));
		preferences.setConflictExists(YesNoList.YES);
		preferences.setFundsAccessType(FundsAccessTypeList.LINE_OF_CREDIT);
		preferences.setFundsAccessTypeDetails(fundsAccessTypeDetails()); //object
		preferences.setInterestRateType(InterestRateTypeList.BASIC_VARIABLE); //object
		preferences.setInterestRateTypeDetails(interestRateTypeDetails());  //object
		preferences.setOtherPreferences(RandomStringUtils.randomAlphabetic(15));
		preferences.setPreferredLenders(YesNoList.YES);
		preferences.setPreferredLendersDetails(RandomStringUtils.randomAlphabetic(15));
		preferences.setPrioritiesAndReasons(RandomStringUtils.randomAlphabetic(15));
		preferences.setRepaymentType(PaymentTypeList.INTEREST_CAPITALISED);
		preferences.setRepaymentTypeDetails(repaymentTypeDetails());  //object
		preferences.setSummary(RandomStringUtils.randomAlphabetic(15));
	
		return preferences;
	}
	
	// Preferences -> FundsAccessTypeDetails - 
	public FundsAccessTypeDetails fundsAccessTypeDetails() {
		FundsAccessTypeDetails funds = new FundsAccessTypeDetails();
		funds.setOffsetAccount(offsetAccount());
		funds.setRedraw(redraw());
		return funds;
	}
	
	//  Preferences -> FundsAccessTypeDetails -> OffsetAccount
	public OffsetAccount offsetAccount() {
		OffsetAccount offset = new OffsetAccount();
		offset.setImportance(ImportanceList.DON_T_WANT);
		offset.setRisksExplained(YesNoList.YES);
		offset.setReason(reason());
		return offset;
	}
	
	//  Preferences -> FundsAccessTypeDetails -> Redraw
	public Redraw redraw() {
		Redraw redraw = new Redraw();
		redraw.setImportance(ImportanceList.DON_T_WANT);
		redraw.setReason(reason());
		redraw.setRisksExplained(YesNoList.YES);
		return redraw;
		
	}
	
	// Preferences -> InterestRateTypeDetails -
	public InterestRateTypeDetails interestRateTypeDetails() {
		InterestRateTypeDetails interest = new InterestRateTypeDetails();
		interest.setFixedAndVariableRate(fixedAndVariableRate());
		interest.setFixedRate(fixedRate());
		interest.setVariableRate(variableRate());
		return interest;
	}
	
	// Preferences -> InterestRateTypeDetails -> FixedAndVariableRate
	public FixedAndVariableRate fixedAndVariableRate() {
		FixedAndVariableRate fav = new FixedAndVariableRate();
		fav.setFixedPeriodDuration(durationData.duration());
		fav.setImportance(ImportanceList.DON_T_WANT);
		fav.setReason(reason());
		fav.setRisksExplained(YesNoList.YES);
		return fav;
	}
	
	// Preferences -> InterestRateTypeDetails -> FixedRate
	public FixedRate fixedRate() {
		FixedRate fixedRate = new FixedRate();
		fixedRate.setFixedPeriodDuration(null);
		fixedRate.setImportance(ImportanceList.DON_T_WANT);
		fixedRate.setReason(reason());
		fixedRate.setRisksExplained(YesNoList.NO);

		return fixedRate ;
	}
	
	// Preferences -> InterestRateTypeDetails -> VariableRate
	public VariableRate variableRate() {
		VariableRate variable = new VariableRate();
		variable.setImportance(ImportanceList.NOT_IMPORTANT);
		variable.setReason(reason());
		variable.setRisksExplained(YesNoList.NO);
		return variable;
	}
	
	// Preferences -> RepaymentTypeDetails 
	public RepaymentTypeDetails repaymentTypeDetails() {
		RepaymentTypeDetails repayment = new RepaymentTypeDetails();
		repayment.setInterestInAdvance(interestInAdvance());
		repayment.setInterestOnly(interestOnly());
		repayment.setLineOfCredit(lineOfCredit());
		repayment.setPrincipalAndInterest(principalAndInterest());
		
		return repayment;
	}
	
	// Preferences -> RepaymentTypeDetails -> InterestInAdvance
	public InterestInAdvance interestInAdvance() {
		InterestInAdvance advance = new InterestInAdvance();
		advance.setImportance(ImportanceList.IMPORTANT);
		advance.setReason(reason());
		advance.setRisksExplained(YesNoList.YES);
		return advance;
	
	}
	
	// Preferences -> RepaymentTypeDetails -> InterestOnly
	public InterestOnly interestOnly() {
		InterestOnly only = new InterestOnly();
		only.setImportance(ImportanceList.DON_T_WANT);
		only.setInterestOnlyDuration(durationData.duration());
		only.setInterestOnlyDurationReason(RandomStringUtils.randomAlphabetic(15));
		only.setReason(reason());
		only.setRepaymentFrequency(FrequencyShortList.WEEKLY);
		only.setRisksExplained(YesNoList.YES);

		return only;
		
	}

	// Preferences -> RepaymentTypeDetails -> 
	public LineOfCredit lineOfCredit() {
		LineOfCredit credit = new LineOfCredit();
		credit.setImportance(ImportanceList.DON_T_WANT);
		credit.setReason(reason());
		credit.setRisksExplained(YesNoList.YES);
		return credit;
	}

	// Preferences -> RepaymentTypeDetails -> 
	public PrincipalAndInterest principalAndInterest() {
		PrincipalAndInterest principal = new PrincipalAndInterest();
		principal.setImportance(ImportanceList.NOT_IMPORTANT);
		principal.setReason(reason());
		principal.setRepaymentFrequency(FrequencyShortList.FORTNIGHTLY);
		principal.setRisksExplained(YesNoList.YES);
		return principal;
	}

	
	// Reason -- reusable common function
	public Reason reason() {
		Reason reason = new Reason();
		reason.setAllowsAccessToFunds(YesNoList.YES);
		reason.setAllowsPayingOffLoanSooner(YesNoList.YES);
		reason.setAvoidingRateIncreaseRisk(YesNoList.YES);
		reason.setDescription(RandomStringUtils.randomAlphabetic(15));
		reason.setDiscountsOnInterestRate(YesNoList.YES);
		reason.setFlexibility(YesNoList.YES);
		reason.setFlexibilityToAccessPrepaidFundsIfNeeded(YesNoList.YES);
		reason.setForTaxPurposes(YesNoList.YES);
		reason.setLimitingRateIncreaseRisk(YesNoList.YES);
		reason.setOther(YesNoList.YES);
		reason.setPotentialRateDecreases(YesNoList.YES);
		
		return reason;
	}
 
			
}
