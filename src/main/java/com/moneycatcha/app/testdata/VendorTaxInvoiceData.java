package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.VendorTaxInvoiceType;
import com.moneycatcha.app.entity.VendorTaxInvoiceType.Asset;
import com.moneycatcha.app.model.GoodsDeliveryList;

import lombok.Data;

@Data
public class VendorTaxInvoiceData {
	
	// VendorTaxInvoiceType
	public List<VendorTaxInvoiceType> vendorTaxInvoiceType() {
		
		List<VendorTaxInvoiceType> vendor = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			VendorTaxInvoiceType invoiceType = new VendorTaxInvoiceType();
			invoiceType.setAsset(asset()); //object
			invoiceType.setInvoiceNumber(RandomStringUtils.randomAlphanumeric(15));
			invoiceType.setTaxInvoiceDate(LocalDate.now().plusWeeks(1));
			invoiceType.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			invoiceType.setXDeliverTo(RandomStringUtils.randomAlphabetic(15));
			invoiceType.setXPurchaser(RandomStringUtils.randomAlphabetic(15));
			invoiceType.setXVendor(RandomStringUtils.randomAlphabetic(15));
			invoiceType.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			vendor.add(invoiceType);
		}
	
		return vendor;
	}
	
	
	// Asset
	public List<Asset> asset() {
		List<Asset> assets = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Asset asset = new Asset();
			asset.setAssetDescription(RandomStringUtils.randomAlphabetic(15));
			asset.setDeliveryDate(LocalDate.now().plusWeeks(1));
			asset.setDepositPaid(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setFinalPosition(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setGoodsDelivery(GoodsDeliveryList.TO_BE_DELIVERED_ON_PAYMENT);
			asset.setGstComponent(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setNonTaxableAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setOnCosts(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setGstComponent(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setPayout(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setRefund(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setTaxableAmountOfAsset(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setTotalCostOfAsset(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setTradeIn(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			asset.setUniqueAssetIdentifier(RandomStringUtils.randomAlphabetic(15));
			assets.add(asset);
		}
	
		return assets;
	}

}
