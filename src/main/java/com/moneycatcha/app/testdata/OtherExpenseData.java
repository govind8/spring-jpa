package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.OtherExpense;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.OtherExpenseCategoryList;
import com.moneycatcha.app.model.OtherExpenseTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class OtherExpenseData {

	PercentOwnedTypeData perOwnedTypeData = new PercentOwnedTypeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	public List<OtherExpense> otherExpense() {
		
		List<OtherExpense> other = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			OtherExpense expense = new OtherExpense();
			expense.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			expense.setArrears(liabilityData.arrears()); //object
			expense.setCategory(OtherExpenseCategoryList.BASIC_LIVING_EXPENSES);
			expense.setDescription(RandomStringUtils.randomAlphabetic(25));
			expense.setEndDate(LocalDate.now().plusYears(1));
			expense.setFrequency(FrequencyShortList.MONTHLY);
			expense.setHasArrears(YesNoList.YES);
			expense.setPercentOwned(perOwnedTypeData.percentOwnership()); //object
			expense.setStartDate(LocalDate.now());
			expense.setType(OtherExpenseTypeList.ADDITIONAL_CAR_S_MAINTENANCE);
			expense.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			expense.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			other.add(expense);			
		}
		return other;
	}
		
}
