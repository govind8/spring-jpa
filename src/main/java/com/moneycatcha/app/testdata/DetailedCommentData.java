package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.DetailedComment;

import lombok.Data;

@Data
public class DetailedCommentData {

	// List - DetailedComment
	public List<DetailedComment> comments() {
		List<DetailedComment> comments = new ArrayList<DetailedComment>();
		for(int i=1; i < 4; i++) {
			comments.add(new DetailedComment());
		}
		for(int i=0; i < comments.size(); i++)  {
			comments.get(i).setComment(RandomStringUtils.randomAlphabetic(120));
			comments.get(i).setContextDescription(RandomStringUtils.randomAlphabetic(250));
			comments.get(i).setCreatedDate(LocalDate.now().minusYears(1));
			comments.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			comments.get(i).setXAuthor(RandomStringUtils.randomAlphabetic(25));
			comments.get(i).setXContext(RandomStringUtils.randomAlphabetic(25));
			comments.get(i).setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
		}
		return comments;
	}
}
