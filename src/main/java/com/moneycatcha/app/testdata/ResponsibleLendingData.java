package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ResponsibleLendingType;
import com.moneycatcha.app.entity.SaleOfAssets;
import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant;
import com.moneycatcha.app.entity.ResponsibleLendingType.SignificantChange;
import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant.SavingsOrSuperannuation;
import com.moneycatcha.app.model.MitigantFactorList;
import com.moneycatcha.app.model.SignificantChangeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class ResponsibleLendingData {

	// ResponsibleLendingType
	public ResponsibleLendingType responsibleLendingType() {
		ResponsibleLendingType responsible = new ResponsibleLendingType();
		responsible.setAnticipatedChanges(YesNoList.YES );
		responsible.setMitigant(mitigants());
		responsible.setSignificantChange(significantChange());
		return responsible;
	}
	
	// Mitigants
	public List<Mitigant> mitigants() {
		
		List<Mitigant> mitigants = new ArrayList<Mitigant>();
		for(int i=0; i < 3; i++) {
			Mitigant mitigant = new Mitigant();
			mitigant.setDescription(RandomStringUtils.randomAlphabetic(15));
			mitigant.setFactor(MitigantFactorList.SALE_OF_ASSETS);
			mitigant.setSaleOfAssets(saleOfAssets());
			mitigant.setSavingsOrSuperannuation(savingsOrSuperannuation());
			mitigant.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			mitigants.add(mitigant);
		}
		return mitigants;
	}
	
	// sale of assets
	public SaleOfAssets saleOfAssets() {
		SaleOfAssets assets = new SaleOfAssets();
		assets.setLenderHeldSecurity(YesNoList.NO);
		assets.setLenderHeldSecurityDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setOtherAssets(YesNoList.NO);
		assets.setOtherAssetsDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setOtherLenderHeldSecurity(YesNoList.YES);
		assets.setOtherLenderHeldSecurityDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setShares(YesNoList.NO);
		assets.setSharesDescription(RandomStringUtils.randomAlphabetic(15));
		return assets;
	}
	
	// savings or Superannuation
	public SavingsOrSuperannuation savingsOrSuperannuation() {
		SavingsOrSuperannuation savings = new SavingsOrSuperannuation();
		savings.setLenderHeldSavingsAccount(YesNoList.NO);
		savings.setLenderHeldSavingsAccountDescription(RandomStringUtils.randomAlphabetic(15));
		savings.setOtherLenderHeldSavingsAccount(YesNoList.NO);
		savings.setOtherLenderHeldSavingsAccountDescription(RandomStringUtils.randomAlphabetic(15));
		return savings;
	}
	
	// Significant Changes
	public List<SignificantChange> significantChange() {
		List<SignificantChange> changes = new ArrayList<SignificantChange>();
		for (int i=0; i < 3; i++) {
			SignificantChange change = new SignificantChange();
			change.setChange(SignificantChangeList.LEAVING_EMPLOYMENT);
			change.setDescription(RandomStringUtils.randomAlphabetic(15));
			change.setEndDate(LocalDate.now().plusYears(2));
			change.setMonthlyFinancialImpact(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			change.setStartDate(LocalDate.now().minusYears(1));
			change.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			changes.add(change);
		}
		return changes;
	}
	
}
