package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.MasterAgreement;
import com.moneycatcha.app.entity.MasterAgreement.AccountToIncorporate;
import com.moneycatcha.app.model.MasterAgreementStatusList;

import lombok.Data;

@Data
public class MasterAgreementData {

	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	PercentOwnedTypeData percentOwnedData = new PercentOwnedTypeData();
	
	// List - MasterAgreement
	public List<MasterAgreement> masterAgreement() {
		List<MasterAgreement> masters = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			MasterAgreement master = new MasterAgreement();
			master.setAccountToIncorporate(accountToIncorporate()); //object
			master.setDateOfExecution(LocalDate.now().plusWeeks(2));
			master.setDescription(RandomStringUtils.randomAlphabetic(20));
			master.setEndDate(LocalDate.now().plusWeeks(2));
			master.setLenderAgreementNumber(RandomStringUtils.randomAlphanumeric(15));
			master.setMasterFacilityLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			master.setProductCode(RandomStringUtils.randomAlphabetic(10));
			master.setPercentOwned(percentOwnedData.percentOwnership()); //Object
			master.setProductName(RandomStringUtils.randomAlphabetic(10));
			master.setSecurity(lendingData.security()); //Object
			master.setStatus(MasterAgreementStatusList.EXISTING);
			master.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			master.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			masters.add(master);
		}
		return masters;
	}
	
	// List - AccountToIncorporate
	public List<AccountToIncorporate> accountToIncorporate() {
		List<AccountToIncorporate> corporates = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			AccountToIncorporate corporate = new AccountToIncorporate();
			corporate.setXExistingAccount(RandomStringUtils.randomAlphabetic(10));
			corporate.setXNewAccount(RandomStringUtils.randomAlphabetic(10));
			corporates.add(corporate);
		}
		return corporates;
	}
	
}
