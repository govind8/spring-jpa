package com.moneycatcha.app.testdata;

import com.moneycatcha.app.entity.ExistingCustomer;

import lombok.Data;

@Data
public class ExistingCustomerData {

	FinancialAccountTypeData financialAccountTypeData = new FinancialAccountTypeData();
	
	public ExistingCustomer existingCustomer() {
		
		ExistingCustomer customer = new ExistingCustomer();
		customer.setAccountNumber(financialAccountTypeData.accountNumber());
		customer.setCustomerNumber("customer-00001");
		customer.setCustomerSince(2000);
		customer.setCustomerSinceMonth("02");
		return customer;
	}
}
