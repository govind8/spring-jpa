package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.TransformMetadata;
import com.moneycatcha.app.entity.TransformMetadata.Identifier;

import lombok.Data;

@Data
public class TransformMetadataData {

	// TranformMetadata
	public TransformMetadata transformMetadata() {
		TransformMetadata transform = new TransformMetadata();
		transform.setIdentifier(identifiers());
		return transform;
	}
	
	// TranformMetadata -> identifier
	public List<Identifier> identifiers() {
		List<Identifier> identifiers = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			Identifier identifier = new Identifier();
			identifier.setAttributeName(RandomStringUtils.randomAlphabetic(15));
			identifier.setExternalID(RandomStringUtils.randomAlphabetic(15));
			identifier.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			identifier.setXContext(RandomStringUtils.randomAlphabetic(15));
			identifiers.add(identifier);
		}
		return identifiers;
	}
	
}
