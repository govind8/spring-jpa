package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.IncomeRecent;
import com.moneycatcha.app.model.ProofCodeCompanyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class IncomeRecentData {
	
	AddBackData addbackData = new AddBackData();

	public IncomeRecent incomeRecent() {
		
		IncomeRecent incomeRecent = new IncomeRecent();
		
		incomeRecent.setCompanyProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeRecent.setCompanyProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeRecent.setEndDate(LocalDate.now());
		incomeRecent.setIncomeGreaterThanPreviousYear(YesNoList.YES);
		incomeRecent.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeRecent.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeRecent.setProofCode(ProofCodeCompanyList.FINANCIAL_STATEMENT);
		incomeRecent.setProofSighted(YesNoList.NO);
		incomeRecent.setStartDate(LocalDate.now().minusYears(20));
		incomeRecent.setTaxOfficeAssessments(YesNoList.YES);
		incomeRecent.setXAccountant(RandomStringUtils.randomNumeric(15));
		incomeRecent.setAddback(addbackData.addBack());
	
		return incomeRecent;
	}
}
