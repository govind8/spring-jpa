package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Declarations;
import com.moneycatcha.app.entity.Declarations.BrokerDeclarations;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class DeclarationsData {

	public Declarations declaration() {
		
		BrokerDeclarations bd = new BrokerDeclarations();
		bd.setInterestOnlyMeetsRequirements(YesNoList.YES);
		bd.setInterestOnlyMeetsRequirementsDescription(RandomStringUtils.randomAlphabetic(255));
		bd.setInterestOnlyRisksExplained(YesNoList.YES);
		bd.setInterestOnlyRisksExplainedDescription(RandomStringUtils.randomAlphabetic(255));
		
		Declarations declaration = new Declarations();
		declaration.setBrokerDeclarations(bd);

		return declaration;
	}
}
