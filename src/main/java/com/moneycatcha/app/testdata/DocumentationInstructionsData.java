package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.PersonApplicant.DocumentationInstructions;
import com.moneycatcha.app.model.DocumentationInstructionsMethodList;
import com.moneycatcha.app.model.SendDocumentsToPersonList;

import lombok.Data;

@Data
public class DocumentationInstructionsData {

	public DocumentationInstructions document() {
		DocumentationInstructions doc = new DocumentationInstructions();
		doc.setMethod(DocumentationInstructionsMethodList.POST);
		doc.setSendDocumentsTo(SendDocumentsToPersonList.MYSELF);
		doc.setXNominatedAuthority(RandomStringUtils.randomAlphanumeric(15));
		
		return doc;
	}
	
}
