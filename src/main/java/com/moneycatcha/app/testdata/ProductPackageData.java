package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.DepositAccount;
import com.moneycatcha.app.entity.ProductPackage;
import com.moneycatcha.app.entity.DepositAccount.Overdraft;
import com.moneycatcha.app.entity.ProductPackage.Liability;
import com.moneycatcha.app.entity.ProductPackage.LoanDetails;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class ProductPackageData {

	LoanDetailsData loanDetailsData = new LoanDetailsData();
	
	// ProductPackage
	public List<ProductPackage> productPackage() {
		
		List<ProductPackage> pp = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			ProductPackage product = new ProductPackage();
			product.setCreditCard(creditCard());
			product.setDepositAccount(depositAccount());
			product.setLiability(liability());
			product.setLoanDetails(loanDetails());
			product.setExistingPackage(YesNoList.NO);
			product.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			product.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			pp.add(product);
		}
	
		return pp;
	}
	
	// CreditCard
	public List<CreditCard> creditCard() {
		List<CreditCard> cc = new ArrayList<>();
		cc.add(loanDetailsData.creditCard());
		cc.add(loanDetailsData.creditCard());
		return cc;
	}
	
	// DepositAccount
	public List<DepositAccount> depositAccount() {
		List<DepositAccount> da = new ArrayList<>();
		
		for (int i=0; i < 3; i++) {
			Overdraft od = new Overdraft();
			od.setConsiderLowerOverdraftLimitIfNotEligible(YesNoList.YES);
			od.setIsOverdraftUsedForTemporaryExpenses(YesNoList.YES);
			od.setLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			od.setOptionCode(RandomStringUtils.randomAlphabetic(10));
			od.setOverdraftRequested(YesNoList.NO);
			
			DepositAccount deposit = new DepositAccount();
			deposit.setAlreadyExists(YesNoList.NO);
			deposit.setOverdraft(od); //object
			deposit.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			deposit.setXDepositAccount(RandomStringUtils.randomAlphabetic(10));
			da.add(deposit);
		}
		return da;
	}
	
	// List LoanDetails
	public List<LoanDetails> loanDetails() {
		List<LoanDetails> loanDetails = new ArrayList<>();

		for(int i=0; i < 3; i++)  {
			LoanDetails details = new LoanDetails();
			details.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			details.setXLoanDetails(RandomStringUtils.randomAlphabetic(10));
			loanDetails.add(details);
		}
		return loanDetails;
	}
	
	// List Liability
	public List<Liability> liability() {
		List<Liability> liability = new ArrayList<>();

		for(int i=0; i < 3; i++)  {
			Liability liable = new Liability();
			liable.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			liable.setXLiability(RandomStringUtils.randomAlphabetic(10));
			liability.add(liable);
		}
		return liability;
	}
}