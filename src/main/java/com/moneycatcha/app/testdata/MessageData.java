package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Message;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class MessageData {

	AttachmentsData attachmentsData = new AttachmentsData();
	
	ContentData contentData = new ContentData();
	
	InstructionsData instructionsData = new InstructionsData();
	
	PublisherData publisherData = new PublisherData();
	
	RecipientData recipientData = new RecipientData();
	
	SchemaVersionData schemaVersionData = new SchemaVersionData();
	
	TransformMetadataData transformMetadataData = new TransformMetadataData();

	// Message
	public Message message() {
		
		Message message = new Message();
		message.setAttachment(attachmentsData.attachments()); //object
		message.setContent(contentData.content()); //object
		message.setInstructions(instructionsData.instructions()); //object
		message.setPublisher(publisherData.publisher()); //object
		message.setRecipient(recipientData.recipient()); //object
		message.setSchemaVersion(schemaVersionData.schemaVersion()); //object
		message.setTransformMetadata(transformMetadataData.transformMetadata()); //object
		
		message.setProductionData(YesNoList.NO);
		message.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		
		return message;
	}
}
