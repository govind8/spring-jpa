package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Attachment;

public class AttachmentsData {

	public List<Attachment> attachments() {
		List<Attachment> attachments = new ArrayList<>();
		
		for (int i=0; i< 2; i++) {
			double dec = 51.33;
			byte[] inlineAttachment = Double.toString(dec).getBytes();
			
			Attachment attachment = new Attachment();
			attachment.setFilename(RandomStringUtils.randomAlphabetic(15));
			attachment.setInlineAttachment(inlineAttachment);
			attachment.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			attachment.setUri("www." + RandomStringUtils.randomAlphabetic(10) + ".com");
			attachments.add(attachment);
		}
		return attachments;
	}

}

//double dec = 51.33;
//byte[] inlineAttachment1 = Double.toString(dec).getBytes();
//int num = 35;
//byte[] inlineAttachment2 = Integer.toString(num).getBytes();
//Attachment attachment1 = new Attachment("attach_0001", "sparrow", "www.google.com", inlineAttachment1);
//Attachment attachment2 = new Attachment("attach_0002", "parrot", "www.apple.com", inlineAttachment2);
//attachments.add(attachment1); 
//attachments.add(attachment2);		
//"attach_0001", "sparrow", "www.google.com", inlineAttachment1);
