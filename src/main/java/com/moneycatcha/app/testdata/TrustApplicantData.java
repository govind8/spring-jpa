package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.TrustApplicant;
import com.moneycatcha.app.entity.TrustDeedVariation;
import com.moneycatcha.app.entity.Trustee;
import com.moneycatcha.app.entity.TrustApplicant.BeneficialOwner;
import com.moneycatcha.app.entity.TrustApplicant.Beneficiary;
import com.moneycatcha.app.entity.TrustApplicant.Settlor;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.OecdCRSStatusList;
import com.moneycatcha.app.model.TrustPurposeList;
import com.moneycatcha.app.model.TrustStructureList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class TrustApplicantData {

	BusinessData businessData = new BusinessData();
	
	ContactData contactData = new ContactData();
	
	DealingNumberTypeData dealingData = new DealingNumberTypeData();
	
	CompanyApplicantData companyApplicantData = new CompanyApplicantData();
		
	DocumentationInstructionsData documentData = new DocumentationInstructionsData();
	
	ExistingCustomerData existingData = new ExistingCustomerData();
	
	FinancialAnalysisData financialAnalysisData = new FinancialAnalysisData();
	
	ForeignTaxAssociationData foreignData = new ForeignTaxAssociationData();
	
	IncomePreviousData incomePreviousData = new IncomePreviousData();
	
	IncomePriorData incomePriorData = new IncomePriorData();
	
	IncomeRecentData incomeRecentData = new IncomeRecentData();
	
	IncomeYearToDateData incomeYearToDateData = new IncomeYearToDateData();
	
	SourceOfWealthData sowData = new SourceOfWealthData();
	
		
	// TrustApplicant
	public List<TrustApplicant> trustApplicant() {
		List<TrustApplicant> trust = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			TrustApplicant applicant = new TrustApplicant();
			applicant.setAbn(RandomStringUtils.randomAlphanumeric(15));
			applicant.setApplicantType(ApplicantTypeList.BORROWER);
			applicant.setBeneficialOwner(beneficialOwner()); //object
			applicant.setBeneficiary(beneficiary()); //object
			applicant.setBusiness(businessData.taBusiness()); //object
			applicant.setBusinessName(RandomStringUtils.randomAlphabetic(15));
			applicant.setBusinessNameSameAsTrustName(YesNoList.YES);
			applicant.setContact(contactData.TrustApplicantContact()); //object
			applicant.setCountryEstablished(CountryCodeList.AD);
			applicant.setCustomerTypeCode(RandomStringUtils.randomAlphabetic(15));
			applicant.setCustomerTypeDescription(RandomStringUtils.randomAlphabetic(15));
			applicant.setDealingNumber(dealingData.dealingNumberType()); //object
			applicant.setDeclaredIncome(companyApplicantData.declaredIncome()); //object
			applicant.setDocumentationType(RandomStringUtils.randomAlphabetic(15));
			applicant.setEstablished(YesNoList.NO);
			applicant.setEstablishmentDate(LocalDate.now().minusYears(2));
			applicant.setExistingCustomer(existingData.existingCustomer()); //object
			applicant.setFinancialAnalysis(financialAnalysisData.financialAnalysis()); //object
			applicant.setForeignTaxAssociation(foreignData.foreignTaxAssociation()); //object
			applicant.setIsExistingCustomer(YesNoList.NO);
			applicant.setIncomePrevious(incomePreviousData.incomePrevious()); //object
			applicant.setIncomePrior(incomePriorData.incomePrior()); //object
			applicant.setIncomeRecent(incomeRecentData.incomeRecent()); //object
			applicant.setIncomeYearToDate(incomeYearToDateData.incomeYearToDate()); //object
			applicant.setNumberOfBeneficiaries(new BigInteger(RandomStringUtils.randomNumeric(2)));
			applicant.setNumberOfTrustees(new BigInteger(RandomStringUtils.randomNumeric(2)));
			applicant.setOecdcrsStatus(OecdCRSStatusList.ACTIVE);
			applicant.setPrimaryApplicant(YesNoList.NO);
			applicant.setSettlor(settlor()); //object
			applicant.setSourceOfWealth(sowData.sourceOfWealth()); //object
			applicant.setTrustDeedVariation(trustDeedVariation()); //object
			applicant.setTrustee(trustee()); //object
			applicant.setTrustName(RandomStringUtils.randomAlphabetic(15)); //object
			applicant.setTrustPurpose(TrustPurposeList.FAMILY_TRUST);
			applicant.setTrustStructure(TrustStructureList.DISCRETIONARY);
			applicant.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			applicant.setVestingDate(LocalDate.now().plusYears(1));
			applicant.setXAccountant(RandomStringUtils.randomAlphabetic(15));
			applicant.setGovernmentOrganisation(YesNoList.YES);
			applicant.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));

			trust.add(applicant);
		}
		return trust;
	}
		
	//new BigDecimal(RandomStringUtils.randomNumeric(2, 4))
	//new BigInteger(RandomStringUtils.randomNumeric(2))
	// RandomStringUtils.randomAlphabetic(15)
	// RandomStringUtils.randomAlphanumeric(15)
	
	// BeneficialOwner
	public List<BeneficialOwner> beneficialOwner() {
		
		List<BeneficialOwner> owners = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			BeneficialOwner bo = new BeneficialOwner();
			bo.setXBeneficialOwner(RandomStringUtils.randomAlphanumeric(15));
			owners.add(bo);
		}
	
		return owners;
	}
	
	
	// Beneficiary
	public List<Beneficiary> beneficiary() {
		
		List<Beneficiary> beneficiaries = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Beneficiary beneficiary = new Beneficiary();
			beneficiary.setXBeneficiary(RandomStringUtils.randomAlphanumeric(15));
			beneficiaries.add(beneficiary);
		}
	
		return beneficiaries;
	}
	
	
	// Settlor
	public List<Settlor> settlor() {
		
		List<Settlor> settlors = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Settlor settlor = new Settlor();
			settlor.setXSettlor(RandomStringUtils.randomAlphanumeric(15));
			settlors.add(settlor);
		}
	
		return settlors;
	}
	
	
	// TrustDeedVariation
	public List<TrustDeedVariation> trustDeedVariation() {
		
		List<TrustDeedVariation> variations = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			TrustDeedVariation variation = new TrustDeedVariation();
			variation.setDate(LocalDate.now().minusWeeks(20));
			variation.setDescription(RandomStringUtils.randomAlphabetic(15));
			variation.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			variations.add(variation);
		}
	
		return variations;
	}
	
	// Trustee
	public List<Trustee> trustee() {
		
		List<Trustee> trustees = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Trustee trustee = new Trustee();
			trustee.setAppointmentDate(LocalDate.now());
			trustee.setXTrustee(RandomStringUtils.randomAlphanumeric(15));
			trustees.add(trustee);
		}
	
		return trustees;
	}
	
}
