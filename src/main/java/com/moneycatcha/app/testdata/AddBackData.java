package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Addback;
import com.moneycatcha.app.entity.Addback.OtherAddback;

import lombok.Data;

@Data
public class AddBackData {

	public Addback addBack() {
		
		List<OtherAddback> oaList = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			OtherAddback otherAddback = new OtherAddback();
			otherAddback.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			otherAddback.setDescription(RandomStringUtils.randomAlphabetic(15));
			oaList.add(otherAddback);
		}
			
		Addback addBack = new Addback();
		addBack.setAllowances(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setAmortisationOfGoodwill(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setBonus(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setCarExpense(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setCarryForwardLosses(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setDepreciation(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setInterest(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setLease(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setNonCashBenefits(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setNonRecurringExpenses(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setPaymentToTrustee(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setSalary(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setSuperannuationExcess(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		addBack.setOtherAddback(oaList);
		
		return addBack;
		
	}
}
