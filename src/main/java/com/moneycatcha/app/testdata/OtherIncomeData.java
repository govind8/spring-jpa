package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.OtherIncome;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.CurrencyCodeList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.GovernmentBenefitsTypeList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.IncomeVerificationList;
import com.moneycatcha.app.model.OtherIncomeTypeList;
import com.moneycatcha.app.model.ProofCodeOtherList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class OtherIncomeData {

	PercentOwnedTypeData perOwnedTypeData = new PercentOwnedTypeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	public List<OtherIncome> otherIncome() {
		
		List<OtherIncome> other = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			OtherIncome income = new OtherIncome();
			income.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			income.setBenefitsDescription(RandomStringUtils.randomAlphabetic(25));
			income.setCountry(CountryCodeList.CM);
			income.setDescription(RandomStringUtils.randomAlphabetic(25));
			income.setEndDate(LocalDate.now().plusYears(1));
			income.setFrequency(FrequencyShortList.MONTHLY);
			income.setGovernmentBenefitsType(GovernmentBenefitsTypeList.CRISIS_PAYMENT);
			income.setPercentOwned(perOwnedTypeData.percentOwnership()); //object
			income.setStartDate(LocalDate.now());
			income.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.ONGOING);
			income.setIncomeVerification(IncomeVerificationList.FULLY_VERIFIED);
			income.setIsTaxable(YesNoList.NO);
			income.setNetAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			income.setNetAmountFrequency(FrequencyShortList.WEEKLY);
			income.setPreviousYearAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			income.setPrimaryForeignCurrency(CurrencyCodeList.AED);
			income.setProofCode(ProofCodeOtherList.LETTER_FROM_APPROPRIATE_BODY);
			income.setProofSighted(YesNoList.NO);
			income.setType(OtherIncomeTypeList.ANNUITIES);
			income.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			income.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			other.add(income);			
		}
		return other;
	}
		
}
