package com.moneycatcha.app.testdata;

import com.moneycatcha.app.entity.Content;

public class ContentData {

	ApplicationData applicationData = new ApplicationData();

	NeedsAnalysisData needsAnalysisData = new NeedsAnalysisData();
	
	StatementOfPositionsData statementOfPositionsData = new StatementOfPositionsData();
	
	public Content content() {
		
		Content content = new Content();
		content.setApplication(applicationData.application());
		content.setNeedsAnalysis(needsAnalysisData.needsAnalysis());
		content.setStatementOfPosition(statementOfPositionsData.statementOfPosition());
		return content;
	}
}
