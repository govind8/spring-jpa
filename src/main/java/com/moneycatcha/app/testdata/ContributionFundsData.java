package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ContributionFunds;
import com.moneycatcha.app.model.ContributionFundsTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class ContributionFundsData {

	// List - ContributionFunds
	public List<ContributionFunds> funds() {
		List<ContributionFunds> funds = new ArrayList<ContributionFunds>();
		for(int i=1; i < 4; i++) {
			funds.add(new ContributionFunds());
		}
		for(int i=0; i < funds.size(); i++)  {
			funds.get(i).setAmount(new BigDecimal(RandomStringUtils.randomNumeric(3, 10)));
			funds.get(i).setDescription(RandomStringUtils.randomAlphabetic(255));
			funds.get(i).setLoan(YesNoList.YES);
			funds.get(i).setType(ContributionFundsTypeList.FIRST_HOME_OWNER_GRANT);
			funds.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			funds.get(i).setXAssociatedLoanAccount(RandomStringUtils.randomAlphabetic(15));
		}
		return funds;
	}
	
}
