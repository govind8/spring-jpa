package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.CustomerTransactionAnalysis;
import com.moneycatcha.app.entity.CustomerTransactionAnalysis.CategorySet;
import com.moneycatcha.app.entity.CustomerTransactionAnalysis.Result;
import com.moneycatcha.app.entity.CustomerTransactionAnalysis.CategorySet.AggregatedTransactions;
import com.moneycatcha.app.model.AggregatedTransactionsCategoryTypeList;
import com.moneycatcha.app.model.TransactionTypeList;
import com.moneycatcha.app.model.YesNoList;

public class CustomerTransactionAnalysisData {

	// List - CustomerTransactionAnalysis
	public List<CustomerTransactionAnalysis> customerAnalysis() {
		List<CustomerTransactionAnalysis> analysis = new ArrayList<CustomerTransactionAnalysis>();
		for(int i=1; i < 4; i++) {
			analysis.add(new CustomerTransactionAnalysis());
		}
		for(int i=0; i < analysis.size(); i++)  {
			analysis.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			analysis.get(i).setCategorySet(categorySet());
			analysis.get(i).setDataAggregator(RandomStringUtils.randomAlphabetic(10));
			analysis.get(i).setEndDate(LocalDate.now().plusMonths(5));
			analysis.get(i).setResult(result());
			analysis.get(i).setStartDate(LocalDate.now().minusYears(1));	
		}
		return analysis;
	}
	
	// List - CategorySet
	public List<CategorySet> categorySet() {
		List<CategorySet> category = new ArrayList<CategorySet>();
		for(int i=1; i < 4; i++) {
			category.add(new CategorySet());
		}
		for(int i=0; i < category.size(); i++)  {
			category.get(i).setAggregatedTransactions(transactions());
			category.get(i).setCategorySetName(RandomStringUtils.randomAlphabetic(10));
			category.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		}
		return category;
	}
	
	// List - AggregatedTransactions
	
	public List<AggregatedTransactions> transactions() {
		List<AggregatedTransactions> transaction = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			transaction.add(new AggregatedTransactions());
		}
		for(int i=0; i < transaction.size(); i++)  {
			transaction.get(i).setAmount(new BigDecimal(RandomStringUtils.randomNumeric(3, 8)));
			transaction.get(i).setAmountOnMonthlyBasis(new BigDecimal(RandomStringUtils.randomNumeric(3, 10)));
			transaction.get(i).setCategory(AggregatedTransactionsCategoryTypeList.AMBULANCE_INSURANCE);
			transaction.get(i).setOtherCategory(RandomStringUtils.randomAlphabetic(30));
			transaction.get(i).setType(TransactionTypeList.CREDIT);
			transaction.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		}
		return transaction;
	}
	
	// Result
	public Result result() {
		Result result = new Result();
		result.setManualCheckDetails(RandomStringUtils.randomAlphabetic(30));
		result.setReferForManualCheck(YesNoList.NO);
		return result;
	}
}
