package com.moneycatcha.app.testdata;

import java.math.BigInteger;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.DurationType;
import com.moneycatcha.app.model.DurationUnitsList;

import lombok.Data;

@Data
public class DurationTypeData {
	
	public DurationType duration() {
		DurationType duration = new DurationType();
		duration.setLength(new BigInteger(RandomStringUtils.randomNumeric(3)));
		duration.setUnits(DurationUnitsList.MONTHS);
		return duration;
	}
}
