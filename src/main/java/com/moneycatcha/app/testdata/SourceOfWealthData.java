package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.SourceOfWealth;
import com.moneycatcha.app.model.SourceOfFundsTypeList;

import lombok.Data;

@Data
public class SourceOfWealthData {

	public List<SourceOfWealth> sourceOfWealth() {
		List<SourceOfWealth> sows = new ArrayList<SourceOfWealth>();
		for (int i=0; i < 2; i++) {
			SourceOfWealth sow = new SourceOfWealth();
			sow.setDetail(RandomStringUtils.randomAlphabetic(15));
			sow.setPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			sow.setType(SourceOfFundsTypeList.BUSINESS_OWNERSHIP);
			sows.add(sow);
		}
		return sows;
	}
}
