package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Condition;
import com.moneycatcha.app.entity.ErrorInstructions;
import com.moneycatcha.app.entity.Instructions;
import com.moneycatcha.app.entity.Submit;
import com.moneycatcha.app.entity.Update;
import com.moneycatcha.app.entity.Condition.Product;
import com.moneycatcha.app.entity.ErrorInstructions.Annotation;
import com.moneycatcha.app.entity.Instructions.ApplicationInstructions;
import com.moneycatcha.app.entity.Update.Event;
import com.moneycatcha.app.entity.Update.Status;
import com.moneycatcha.app.entity.Update.Status.Declined;
import com.moneycatcha.app.entity.Update.Status.PreApproved;
import com.moneycatcha.app.model.AnnotationTypeList;
import com.moneycatcha.app.model.AssessmentTypeApplicationInstructionsList;
import com.moneycatcha.app.model.ConditionOwnerApplicationInstructionsList;
import com.moneycatcha.app.model.ConditionResponseStatusList;
import com.moneycatcha.app.model.ConditionStatusApplicationInstructionsList;
import com.moneycatcha.app.model.CreditReportingBodyList;
import com.moneycatcha.app.model.DeclinedReasonApplicationInstructionsList;
import com.moneycatcha.app.model.ErrorInstructionsTypeList;
import com.moneycatcha.app.model.ErrorSourceList;
import com.moneycatcha.app.model.EventNameApplicationInstructionsList;
import com.moneycatcha.app.model.PreConditionToStageApplicationInstructionsList;
import com.moneycatcha.app.model.StatusNameApplicationInstructionsList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class InstructionsData {
	
	// Instructions 
	public Instructions instructions() {
		
		Instructions instructions = new Instructions();
		instructions.setApplicationInstructions(applicationInstructions());
		instructions.setErrorInstructions(errorInstructions());
		
		return instructions;
	}
	
	// Instructions -> ApplicationInstructions
	public ApplicationInstructions applicationInstructions() {
		
		ApplicationInstructions appInstructions = new ApplicationInstructions();
		appInstructions.setSubmit(submit());
		appInstructions.setUpdate(update());
		
		return appInstructions;
	}
	
	// Instructions -> List<ErrorInstructions>
	public List<ErrorInstructions> errorInstructions() {
		List<ErrorInstructions> errorInstructions = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			ErrorInstructions error = new ErrorInstructions();
			error.setErrorSource(ErrorSourceList.DTD); // object
			error.setErrorSourceVersion(RandomStringUtils.randomAlphanumeric(5));
			error.setType(ErrorInstructionsTypeList.DATA_ERROR);
			error.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			error.setXPath(RandomStringUtils.randomAlphabetic(15));
			errorInstructions.add(error);
		}
		return errorInstructions;
	}
	
	// Instructions -> List<ErrorInstructions> -> List<Annotation>
	public List<Annotation> annotations() {
		List<Annotation> annotations = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Annotation annotation = new Annotation();
			annotation.setDetails(RandomStringUtils.randomAlphabetic(15));
			annotation.setType(AnnotationTypeList.TECHNICAL);
			annotation.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			annotations.add(annotation);
		}
		return annotations;
	}
	
	// Instructions -> ApplicationInstructions -> Submit
	public Submit submit() {
		
		Submit submit = new Submit();
		submit.setAssessmentType(AssessmentTypeApplicationInstructionsList.FULL);
		submit.setIsAccountVariation(YesNoList.YES);
		submit.setIsResubmission(YesNoList.YES);
		submit.setIsSubmissionDocuments(YesNoList.YES);
		submit.setIsSupportingDocuments(YesNoList.YES);
		submit.setCondition(submitConditions()); // object
		return submit;
	}
	
	// Instructions -> ApplicationInstructions -> Submit -> List<Condition>
	public List<Condition> statusConditions() {
		List<Condition> conditions = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Condition condition = new Condition();
			condition.setConditionResponseDescription(RandomStringUtils.randomAlphabetic(15));
			condition.setConditionResponseStatus(ConditionResponseStatusList.NOT_MET);
			condition.setLoanConditionText(RandomStringUtils.randomAlphabetic(15));
			condition.setXSupportingDocument(RandomStringUtils.randomAlphanumeric(15));
			conditions.add(condition);
		}
		return conditions;
	}
	
	
	// Instructions -> ApplicationInstructions -> Update
	public Update update() {
		
		Update update = new Update();
		update.setEvent(event());
		update.setStatus(status());
		
		return update;
	}
	
	// Instructions -> ApplicationInstructions -> Update -> List<Event>
	public List<Event> event() {
		List<Event> events = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Event event = new Event();
			event.setDateTime(LocalDateTime.now().plusDays(2));
			event.setDetails(RandomStringUtils.randomAlphabetic(15));
			event.setName(EventNameApplicationInstructionsList.APPLICATION_RECEIVED);
			event.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			events.add(event);
		}
		return events;
	}
	
	// Instructions -> ApplicationInstructions -> Update -> Status
	public Status status() {
		
		Status status = new Status();
		status.setCondition(statusConditions()); //object
		status.setDateTime(LocalDateTime.now().minusDays(2));
		status.setDeclined(declined()); //object
		status.setDetails(RandomStringUtils.randomAlphabetic(15));
		status.setName(StatusNameApplicationInstructionsList.APPLICATION_REGISTERED);
		status.setPreApproved(preApproved()); //object
		return status;
	}


	// Instructions -> ApplicationInstructions -> Update -> Status -> List<Declined>
	public List<Declined> declined() {
		List<Declined> declined = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Declined decline = new Declined();
			decline.setCreditReportingBody(CreditReportingBodyList.D_AND_B);
			decline.setDetails(RandomStringUtils.randomAlphabetic(15));
			decline.setReason(DeclinedReasonApplicationInstructionsList.LEVEL_OF_DEBT);
			decline.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			declined.add(decline);
		}
		return declined;
	}

	// Instructions -> ApplicationInstructions -> Update -> Status -> List<PreApproved>
	public List<PreApproved> preApproved() {
		List<PreApproved> preApproved = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			PreApproved approved = new PreApproved();
			approved.setEstablishmentAndGovernmentFees(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			approved.setEstimatedBorrowingPower(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			approved.setEstimatedRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			approved.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			preApproved.add(approved);
		}
		return preApproved;
	}

	
	// Instructions -> ApplicationInstructions -> Update -> Status -> List<Condition>
	public List<Condition> submitConditions() {
		List<Condition> conditions = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Condition condition = new Condition();
			condition.setConditionOwner(ConditionOwnerApplicationInstructionsList.APPLICANT);
			condition.setConditionStatus(ConditionStatusApplicationInstructionsList.NOT_SATISFIED);
			condition.setConditionType(RandomStringUtils.randomAlphabetic(15));
			condition.setDocRequirement(YesNoList.YES);
			condition.setLoanConditionText(RandomStringUtils.randomAlphabetic(15));
			condition.setPreconditionToStage(PreConditionToStageApplicationInstructionsList.CONDITIONAL_APPROVAL);
			condition.setProduct(product());
			condition.setUpdatedDateTime(LocalDateTime.now().plusYears(2));
			condition.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			condition.setUpdatedDateTime(LocalDateTime.now());
			conditions.add(condition);
		}
		return conditions;
	}
	
	// Instructions -> ApplicationInstructions -> Update -> Status -> List<Condition> -> Product
	public List<Product> product() {
		List<Product> products = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			Product product = new Product();
			product.setXFinancialProduct(RandomStringUtils.randomAlphabetic(15));
			products.add(product);
		}
		return products;
	}
}
