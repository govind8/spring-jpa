package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.CreditHistory;
import com.moneycatcha.app.model.CreditHistoryIssueList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;
@Data
public class CreditHistoryData {

	public List<CreditHistory> creditHistory() {
		List<CreditHistory> chList = new ArrayList<>();
		CreditHistory ch = new CreditHistory();
		ch.setDetails(RandomStringUtils.randomAlphabetic(15));
		ch.setIsCurrent(YesNoList.YES);
		ch.setIssue(CreditHistoryIssueList.ARREARS);
		chList.add(ch);
		return chList;
	}
}
