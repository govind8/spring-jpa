package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.FinancialAnalysis;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class FinancialAnalysisData {

	// FinancialAnalysis
	public FinancialAnalysis financialAnalysis() {
		
		List<FinancialAnalysis.CompanyFinancials> cfList = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			FinancialAnalysis.CompanyFinancials financials = new FinancialAnalysis.CompanyFinancials();
			financials.setXCompanyFinancials(RandomStringUtils.randomAlphabetic(15));
			cfList.add(financials);
		}		
		FinancialAnalysis fa = new FinancialAnalysis();
		fa.setCompanyFinancials(cfList);
		fa.setAnnualPaymentOnCommitments(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		fa.setCompleteFinancialAnalysis(YesNoList.NO);
		return fa;
	}
}
