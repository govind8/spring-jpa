package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.AccountVariation;
import com.moneycatcha.app.entity.AccountVariation.AddBorrower;
import com.moneycatcha.app.entity.AccountVariation.AddGuarantee;
import com.moneycatcha.app.entity.AccountVariation.AddGuaranteeSecurity;
import com.moneycatcha.app.entity.AccountVariation.AddGuarantor;
import com.moneycatcha.app.entity.AccountVariation.AddSecurity;
import com.moneycatcha.app.entity.AccountVariation.BalanceIncrease;
import com.moneycatcha.app.entity.AccountVariation.LimitIncrease;
import com.moneycatcha.app.entity.AccountVariation.ReduceLimit;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class AccountVariationData {

	// Application -> AccountVariation
	public List<AccountVariation> accountVariationList() {

		List<AccountVariation> av = new ArrayList<>();
		
		for(int i=0; i < 2; i++) {
			AccountVariation variation = new AccountVariation();
			
			AddBorrower borrower = new AddBorrower();
			borrower.setXBorrower(RandomStringUtils.randomAlphabetic(10));
			
			AddGuarantee guarantee = new AddGuarantee();
			guarantee.setXLendingGuarantee(RandomStringUtils.randomAlphabetic(10));
			
			AddGuaranteeSecurity gsecurity = new AddGuaranteeSecurity();
			gsecurity.setActionDate(LocalDate.now());
			gsecurity.setXSecurity(RandomStringUtils.randomAlphabetic(10));
			
			AddGuarantor guarantor = new AddGuarantor();
			guarantor.setXGuarantor(RandomStringUtils.randomAlphabetic(10));
			
			AddSecurity security = new AddSecurity();
			security.setActionDate(LocalDate.now());
			security.setXSecurity(RandomStringUtils.randomAlphabetic(10));
			
			BalanceIncrease bi = new BalanceIncrease();
			bi.setActionDate(LocalDate.now());
			bi.setIncreaseAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			bi.setNewBalance(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			
			LimitIncrease li = new LimitIncrease();
			li.setActionDate(LocalDate.now());
			li.setNewLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			li.setIncreaseAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			li.setIsNewLimitRequestedInForeignCurrency(YesNoList.NO);

			ReduceLimit rl = new ReduceLimit();
			rl.setNewLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			rl.setLimitReduction(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			rl.setIsNewLimitRequestedInForeignCurrency(YesNoList.YES);
			
			// Account Variation
			variation.setAddBorrower(borrower); //object
			variation.setAddGuarantee(guarantee); //object
			variation.setAddGuaranteeSecurity(gsecurity); //object
			variation.setAddGuarantor(guarantor); //object
			variation.setLimitIncrease(li); //object
			variation.setReduceLimit(rl); //object
			variation.setVariationDescription(RandomStringUtils.randomAlphabetic(10));
			variation.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			variation.setXAccountToVary(RandomStringUtils.randomAlphabetic(10));
			variation.setXLendingGuaranteeToVary(RandomStringUtils.randomAlphabetic(10));
			variation.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			
			av.add(variation);
		}
		
		return av;
		
	}
}

//AccountVariation.LimitIncrease li = new AccountVariation.LimitIncrease();
//li.setActionDate(LocalDate.now());
//li.setNewLimit(new BigDecimal("500000.00"));
//li.setIncreaseAmount(new BigDecimal("3500000.00"));
//li.setIsNewLimitRequestedInForeignCurrency(YesNoList.NO);
//
//AccountVariation.ReduceLimit rl = new AccountVariation.ReduceLimit();
//rl.setNewLimit(new BigDecimal("1200000.00"));
//rl.setLimitReduction(new BigDecimal("2000000.00"));
//rl.setIsNewLimitRequestedInForeignCurrency(YesNoList.YES);
//		
//AccountVariation.AddGuarantor ag = new AccountVariation.AddGuarantor();
//ag.setXGuarantor("Yes Bank");
//
//
//AccountVariation av1 = new AccountVariation();
//av1.setVariationDescription("First Variation");
//av1.setUniqueID("av-001");
//av1.setXAccountToVary("Govind");
//
//AccountVariation av2 = new AccountVariation();
//av2.setUniqueID("av-002");
//av2.setXLendingGuaranteeToVary("Lending Guarantee");
//
//AccountVariation av3 = new AccountVariation();
//av3.setUniqueID("av-003");
//av3.setLimitIncrease(li);
//
//AccountVariation av4 = new AccountVariation();
//av4.setUniqueID("av-004");
//av4.setAddGuarantor(ag);
//av4.setReduceLimit(rl);
//
//List<AccountVariation> variations = new ArrayList<AccountVariation>();
//variations.add(av1);
//variations.add(av2);
//variations.add(av3);
//variations.add(av4);
//
//return variations;