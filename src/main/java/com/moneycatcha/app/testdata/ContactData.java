package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.CurrentAddress;
import com.moneycatcha.app.entity.DurationType;
import com.moneycatcha.app.entity.EmailAddress;
import com.moneycatcha.app.entity.PhoneType;
import com.moneycatcha.app.entity.PostSettlementAddress;
import com.moneycatcha.app.entity.PreviousAddress;
import com.moneycatcha.app.entity.PriorAddress;
import com.moneycatcha.app.entity.Contact.ContactPerson;
import com.moneycatcha.app.model.ContactEmailTypeList;
import com.moneycatcha.app.model.DurationUnitsList;
import com.moneycatcha.app.model.HousingStatusList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PreferredContactCompanyList;
import com.moneycatcha.app.model.PreferredContactPersonList;

import lombok.Data;

@Data
public class ContactData {

	// 
	public Contact companyApplicant() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		contact.setPreferredContactCompanyList(PreferredContactCompanyList.EMAIL);
		contact.setPreviousRegisteredAddressEndDate(LocalDate.now().minusYears(1));
		contact.setPreviousRegisteredAddressStartDate(LocalDate.now());
		contact.setPrincipalTradingAddressStartDate(LocalDate.now().minusDays(2));
		contact.setRegisteredAddressStartDate(LocalDate.now().minusWeeks(1));
		contact.setWebAddress("www." + RandomStringUtils.randomAlphabetic(5)  + ".com");
		contact.setXMailingAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setXPreviousRegisteredAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setXPrincipalTradingAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setXRegisteredAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setUniqueID(RandomStringUtils.randomAlphabetic(15));
		// elements
		contact.setContactPerson(contactPerson());
		contact.setOfficeFax(phoneType());
		contact.setOfficeMobile(phoneType());
		contact.setOfficePhone(phoneType());
		contact.setPreviousRegisteredAddressDuration(durationType());
		contact.setPrincipalTradingAddressDuration(durationType());
		contact.setRegisteredAddressDuration(durationType());
		return contact;
	}
	
	// PersonApplicant Contact
	public Contact personApplicantContact() {
		Contact contact = new Contact();
		// attributes
		contact.setPreferredContactPersonList(PreferredContactPersonList.EMAIL); 
		// element (object)
		contact.setCurrentAddress(currentAddress());//object
		contact.setEmailAddress(emailAddress());//object
		contact.setFaxNumber(phoneType());//object
		contact.setHomePhone(phoneType());//object
		contact.setMobile(phoneType());//object
		contact.setPostSettlementAddress(postSettlementAddress());//object
		contact.setPreviousAddress(previousAddress());//object
		contact.setPriorAddress(priorAddress());//object
		contact.setWorkPhone(phoneType());//object

		return contact;
	}
	
	// SignatureType Contact
	public Contact signatureTypeContact() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		// element (object)
		contact.setMobile(phoneType());//object
		return contact;
	}
	
	// BranchDomicile & BranchSign Contact
	public Contact branchContact() {
		Contact contact = new Contact();
		// element (object)
		contact.setOfficeFax(phoneType()); //object
		contact.setOfficePhone(phoneType()); //object
		return contact;
	}
	
	// Related Company Contact
	public Contact RelatedCompanyContact() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		contact.setWebAddress("www." + RandomStringUtils.randomAlphabetic(5) + ".com");
		contact.setXAddress(RandomStringUtils.randomAlphabetic(15));
		// element (object)
		contact.setContactPerson(contactPerson()); //object
		contact.setOfficeFax(phoneType()); //object
		contact.setOfficeMobile(phoneType()); //object
		contact.setOfficePhone(phoneType()); //object
		return contact;		
	}
	
	// Sales Channel Contact
	public Contact SalesChannelContact() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		contact.setWebAddress("www." + RandomStringUtils.randomAlphabetic(5) + ".com");
		contact.setXAddress(RandomStringUtils.randomAlphabetic(15));
		// element (object)
		contact.setFax(phoneType()); //object
		contact.setMobile(phoneType()); //object
		contact.setOfficeFax(phoneType()); //object
		contact.setOfficeMobile(phoneType()); //object
		contact.setOfficePhone(phoneType()); //object
		return contact;		
	}
	
	// Related Person Contact
	public Contact RelatedPersonContact() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		contact.setEmailType(ContactEmailTypeList.HOME);
		contact.setPreferredContactPersonList(PreferredContactPersonList.EMAIL);
		contact.setXMailingAddress(RandomStringUtils.randomAlphabetic(15));
		// element (object)
		contact.setHomePhone(phoneType()); //object
		contact.setMobile(phoneType()); //object
		contact.setWorkPhone(phoneType()); //object
		return contact;		
	}
	
	// Trust Applicant Contact
	public Contact TrustApplicantContact() {
		Contact contact = new Contact();
		// attributes
		contact.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		contact.setEmailType(ContactEmailTypeList.HOME);
		contact.setPreferredContactCompanyList(PreferredContactCompanyList.EMAIL);
		contact.setPrincipalTradingAddressDuration(principalTradingAddressDuration()); //object
		contact.setRegisteredAddressDuration(registeredAddressDuration()); //object
		contact.setWebAddress("www." + RandomStringUtils.randomAlphabetic(5) + ".com");
		contact.setXPrincipalTradingAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setXRegisteredAddress(RandomStringUtils.randomAlphabetic(15));
		contact.setXAddress(RandomStringUtils.randomAlphabetic(15));
		// element (object)
		contact.setHomePhone(phoneType()); //object
		contact.setMobile(phoneType()); //object
		contact.setWorkPhone(phoneType()); //object
		return contact;		
	}
	
	// Contact Person
	public ContactPerson contactPerson() {
		
		ContactPerson person = new ContactPerson();
		person.setFirstName(RandomStringUtils.randomAlphabetic(10));
		person.setNameTitle(NameTitleList.MRS);
		person.setRole(RandomStringUtils.randomAlphabetic(10));
		person.setSurname(RandomStringUtils.randomAlphabetic(10));
		person.setXContactPerson(RandomStringUtils.randomAlphabetic(10));
		return person;
	}
	
	// Office Mobile - Data
	public PhoneType officeMobile() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("44444444");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
	
	// Office Fax - Data
	public PhoneType officeFax() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("44444448");
		phoneType.setOverseasDialingCode("0092");
		return phoneType;
		
	}
	
	// Office Phone - Data
	public PhoneType officePhone() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("43333333");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
	
	// Fax - Data
	public PhoneType fax() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("43333338");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
	
	// Mobile - Data
	public PhoneType mobile() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("42222020");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
	
	// Home Phone - Data
	public PhoneType homePhone() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode("61");
		phoneType.setCountryCode("0061");
		phoneType.setNumber("82222223");
		phoneType.setOverseasDialingCode("0091");
		return phoneType;
		
	}
	
	// Work Phone - Data
	public PhoneType workPhone() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode((new BigInteger(RandomStringUtils.randomNumeric(2))).toString());
		phoneType.setCountryCode((new BigInteger(RandomStringUtils.randomNumeric(4))).toString());
		phoneType.setNumber((new BigInteger(RandomStringUtils.randomNumeric(8))).toString());
		phoneType.setOverseasDialingCode((new BigInteger(RandomStringUtils.randomNumeric(5))).toString());
		return phoneType;
		
	}
	
	// Phone, Fax, Mobile - Data
	public PhoneType phoneType() {
		PhoneType phoneType = new PhoneType();
		phoneType.setAustralianDialingCode((new BigInteger(RandomStringUtils.randomNumeric(2))).toString());
		phoneType.setCountryCode((new BigInteger(RandomStringUtils.randomNumeric(4))).toString());
		phoneType.setNumber((new BigInteger(RandomStringUtils.randomNumeric(8))).toString());
		phoneType.setOverseasDialingCode((new BigInteger(RandomStringUtils.randomNumeric(5))).toString());
		return phoneType;
		
	}	
	
	// Previous Registered Address Duration - Data
	public DurationType durationType() {
		DurationType durationType = new DurationType();
		durationType.setLength(new BigInteger(RandomStringUtils.randomNumeric(2)));
		durationType.setUnits(DurationUnitsList.YEARS);
		return durationType;
		
	}
	
	
	// Previous Registered Address Duration - Data
	public DurationType previousRegisteredAddressDuration() {
		DurationType durationType = new DurationType();
		durationType.setLength(new BigInteger(RandomStringUtils.randomNumeric(1)));
		durationType.setUnits(DurationUnitsList.YEARS);
		return durationType;
		
	}
	
	// principal trading address - Data
	public DurationType principalTradingAddressDuration() {
		DurationType durationType = new DurationType();
		durationType.setLength(new BigInteger("40"));
		durationType.setUnits(DurationUnitsList.MONTHS);
		return durationType;
		
	}
	
	// Registered Address Duration - Data
	public DurationType registeredAddressDuration() {
		DurationType durationType = new DurationType();
		durationType.setLength(new BigInteger("30"));
		durationType.setUnits(DurationUnitsList.MONTHS);
		return durationType;
		
	}
	
	// Email Address
	public List<EmailAddress> emailAddress() {
		List<EmailAddress> emailAddress = new ArrayList<>();
		for (int i=0; i <2; i++) {
			EmailAddress email = new EmailAddress();
			email.setEmail(RandomStringUtils.randomAlphabetic(5) + "@example.com");
			email.setEmailType(ContactEmailTypeList.HOME);
			email.setUniqueID(RandomStringUtils.randomNumeric(15));

			emailAddress.add(email);
		}
		return emailAddress;
	}
	
	
	// PostSettlementAddress
	public PostSettlementAddress postSettlementAddress() {
		
		PostSettlementAddress settlement = new PostSettlementAddress();
		settlement.setHousingStatus(HousingStatusList.BOARDING);
		settlement.setStartDate(LocalDate.now().minusYears(1));
		settlement.setXResidentialAddress(RandomStringUtils.randomAlphabetic(15));
		settlement.setXMailingAddress(RandomStringUtils.randomAlphabetic(15));
		return settlement;
	}
	
	// Current Address
	public CurrentAddress currentAddress() {
		
		CurrentAddress current = new CurrentAddress();
		current.setDuration(durationType());
		current.setHousingStatus(HousingStatusList.BOARDING);
		current.setStartDate(LocalDate.now().minusYears(1));
		current.setXLandlord(RandomStringUtils.randomAlphabetic(15));
		current.setXMailingAddress(RandomStringUtils.randomAlphabetic(15));
		current.setXResidentialAddress(RandomStringUtils.randomAlphabetic(15));
		return current;
	}
	
	// Previous Address
	public PreviousAddress previousAddress() {
		
		PreviousAddress previous = new PreviousAddress();
		previous.setDuration(durationType());
		previous.setEndDate(LocalDate.now().plusYears(1));
		previous.setHousingStatus(HousingStatusList.BOARDING);
		previous.setStartDate(LocalDate.now().minusYears(1));
		previous.setXResidentialAddress(RandomStringUtils.randomAlphabetic(15));
		return previous;
	}
	
	// Prior Address
	public List<PriorAddress> priorAddress() {
		List<PriorAddress> priorAddress = new ArrayList<>();
		for (int i=0; i <2; i++) {
			PriorAddress prior = new PriorAddress();
			prior.setDuration(durationType());
			prior.setEndDate(LocalDate.now().plusYears(1));
			prior.setHousingStatus(HousingStatusList.BOARDING);
			prior.setStartDate(LocalDate.now().minusYears(1));
			prior.setXResidentialAddress(RandomStringUtils.randomAlphabetic(15));
			priorAddress.add(prior);
		}
		return priorAddress;
	}
	
	
}
