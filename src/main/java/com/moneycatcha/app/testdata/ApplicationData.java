package com.moneycatcha.app.testdata;

import com.moneycatcha.app.entity.Application;
import com.moneycatcha.app.model.YesNoList;

public class ApplicationData {
	
	AddressTypeData addressData = new AddressTypeData();

	AccountVariationData avData = new AccountVariationData();

	BusinessChannelData bchannelData = new BusinessChannelData();
	
	CompanyApplicantData companyApplicantData = new CompanyApplicantData();
		
	CompanyFinancialsData financialsData = new CompanyFinancialsData();
	
	ContributionFundsData fundsData = new ContributionFundsData();
	
	CustomerTransactionAnalysisData customerData = new CustomerTransactionAnalysisData();
	
	DeclarationsData declarationsData = new DeclarationsData();
	
	DepositAccountDetailsData depositData = new DepositAccountDetailsData();
	
	DetailedCommentData commentData = new DetailedCommentData();
	
	HouseholdData householdData = new HouseholdData();
		
	InsuranceData insuranceData = new InsuranceData();
	
	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	LoanDetailsData loanDetailsData = new LoanDetailsData();
	
	MasterAgreementData masterAgreementData = new MasterAgreementData();
	
	NonRealEstateAssetData nonRealEstateAssetData = new NonRealEstateAssetData();
	
	PersonApplicantData personApplicantData = new PersonApplicantData();
	
	RealEstateAssetData realEstateAssetData = new RealEstateAssetData();
	
	OtherIncomeData otherIncomeData = new OtherIncomeData();
	
	OtherExpenseData otherExpenseData = new OtherExpenseData();
	
	OverviewData overviewData = new OverviewData();
	
	ProductPackageData productPackageData = new ProductPackageData();
	
	ProductSetData productSetData = new ProductSetData();
	
	RelatedCompanyData relatedCompanyData = new RelatedCompanyData();
	
	RelatedPersonData relatedPersonData = new RelatedPersonData();
	
	SalesChannelData salesChannelData = new SalesChannelData();
	
	SettlementData settlementData = new SettlementData();
	
	SplitLoanData splitLoanData = new SplitLoanData();
	
	SummaryData summaryData = new SummaryData();
	
	TrustApplicantData trustApplicantData = new TrustApplicantData();
	
	VendorTaxInvoiceData vendorTaxInvoiceData = new VendorTaxInvoiceData();

	public Application application() {
		
		Application application = new Application();
		application.setAccountVariation(avData.accountVariationList());
		application.setAddress(addressData.addressList());
		application.setBusinessChannel(bchannelData.channelData());
		application.setCompanyApplicant(companyApplicantData.applicants());
		application.setCompanyFinancials(financialsData.financials());
		application.setContributionFunds(fundsData.funds());
		application.setCustomerTransactionAnalysis(customerData.customerAnalysis());
		application.setDeclarations(declarationsData.declaration());
		application.setDepositAccountDetails(depositData.deposit());
		application.setDetailedComment(commentData.comments());
		application.setHousehold(householdData.households());
		application.setInsurance(insuranceData.insurances());
		application.setLendingGuarantee(lendingData.lending());
		application.setLiability(liabilityData.liability());
		application.setLoanDetails(loanDetailsData.loanDetails());
		application.setMasterAgreement(masterAgreementData.masterAgreement());
		application.setNonRealEstateAsset(nonRealEstateAssetData.nonRealEstateAsset());
		application.setOtherExpense(otherExpenseData.otherExpense());
		application.setOtherIncome(otherIncomeData.otherIncome());
		application.setOverview(overviewData.overview());
		application.setPersonApplicant(personApplicantData.personApplicant());
		application.setProductPackage(productPackageData.productPackage());
		application.setProductSet(productSetData.productSet());
		application.setRealEstateAsset(realEstateAssetData.realEstateAsset());
		application.setRelatedCompany(relatedCompanyData.RelatedCompany());
		application.setRelatedPerson(relatedPersonData.relatedPerson());
		application.setSalesChannel(salesChannelData.salesChannel());
		application.setSettlement(settlementData.settlement());
		application.setSplitLoan(splitLoanData.splitLoan());
		application.setSummary(summaryData.summary());
		application.setTrustApplicant(trustApplicantData.trustApplicant());
		application.setVendorTaxInvoice(vendorTaxInvoiceData.vendorTaxInvoiceType());
	
		
		application.setProductionData(YesNoList.YES);
		application.setUniqueID("app-00001");
		
		return application;
		
	}
}
