package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.RelatedPerson;

import lombok.Data;

@Data
public class RelatedPersonData {

	ContactData contactData = new ContactData();
	
	PersonApplicantData personApplicantData = new PersonApplicantData();
	
	ForeignTaxAssociationData foreignTaxData = new ForeignTaxAssociationData();
	
	// RelatedPerson
	public List<RelatedPerson> relatedPerson() {
		
		List<RelatedPerson> relatedPerson = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RelatedPerson person = new RelatedPerson();
			person.setContact(contactData.RelatedPersonContact()); //object
			person.setDateOfBirth(LocalDate.now().minusYears(35));
			person.setForeignTaxAssociation(foreignTaxData.foreignTaxAssociation()); //object
			person.setPersonName(personApplicantData.personName()); //object
			person.setTaxDeclarationDetails(personApplicantData.taxDeclarationDetails()); //object
			person.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			person.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			relatedPerson.add(person);
		}
	
		return relatedPerson;
	}
	
	
}
