package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.BusinessIncomePrevious;
import com.moneycatcha.app.entity.BusinessIncomePrior;
import com.moneycatcha.app.entity.BusinessIncomeRecent;
import com.moneycatcha.app.entity.BusinessIncomeYearToDate;
import com.moneycatcha.app.entity.DeclaredIncome;
import com.moneycatcha.app.entity.SelfEmployed;
import com.moneycatcha.app.model.CurrencyCodeList;
import com.moneycatcha.app.model.EmploymentStatusList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.ProofCodeSelfEmployedList;
import com.moneycatcha.app.model.SelfEmployedBasisList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class SelfEmployedData {

	BusinessData businessData = new BusinessData();
	
	DurationTypeData durationData = new DurationTypeData();
	
	FinancialAnalysisData finAnalysisData = new FinancialAnalysisData();
	
	AddBackData addbackData = new AddBackData();
	
	// Self Employed	
	public SelfEmployed selfEmployed() {
		
		SelfEmployed selfEmployed = new SelfEmployed();
		selfEmployed.setAnzscoOccupationCode(RandomStringUtils.randomAlphabetic(10));
		selfEmployed.setAverageHoursPerWeek(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		selfEmployed.setBasis(SelfEmployedBasisList.TEMPORARY);
		selfEmployed.setBusiness(businessData.business()); //object
		selfEmployed.setBusinessIncomePrevious(businessIncomePrevious()); //object
		selfEmployed.setBusinessIncomePrior(businessIncomePrior());  // object
		selfEmployed.setBusinessIncomeRecent(businessIncomeRecent());  // object
		selfEmployed.setBusinessIncomeYearToDate(businessIncomeYearToDate());  // object
		selfEmployed.setDeclaredIncome(declaredIncome());  // object
		selfEmployed.setDuration(durationData.duration());  // object
		selfEmployed.setFinancialAnalysis(finAnalysisData.financialAnalysis());  // object
		selfEmployed.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.STARTS);
		selfEmployed.setOccupation(RandomStringUtils.randomAlphabetic(10));
		selfEmployed.setOccupationCode(RandomStringUtils.randomAlphabetic(10));
		selfEmployed.setStartDate(LocalDate.now());
		selfEmployed.setStatus(EmploymentStatusList.SECONDARY);
		selfEmployed.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		selfEmployed.setXAccountant(RandomStringUtils.randomAlphabetic(10));
		selfEmployed.setXEmployer(RandomStringUtils.randomAlphabetic(10));
		
		return selfEmployed;
	}
	
	// BusinessIncomePrevious
	public BusinessIncomePrevious businessIncomePrevious() {
		
		BusinessIncomePrevious.ForeignSourcedIncome fincome = new BusinessIncomePrevious.ForeignSourcedIncome();
		fincome.setAudAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		fincome.setPrimaryForeignCurrency(CurrencyCodeList.CUC);
		
		BusinessIncomePrevious bip = new BusinessIncomePrevious();
		bip.setAddback(addbackData.addBack());
		bip.setEndDate(LocalDate.now().plusYears(1));
		bip.setForeignSourcedIncome(fincome);
		bip.setHasForeignSourcedIncome(YesNoList.NO);
		bip.setIncomeGreaterThanPreviousYear(YesNoList.NO);
		bip.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bip.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bip.setStartDate(LocalDate.now());
		bip.setTaxOfficeAssessments(YesNoList.NO);
		bip.setXAccountant(RandomStringUtils.randomAlphanumeric(15));

		return bip;
	}
	
	// BusinessIncomePrior
	public BusinessIncomePrior businessIncomePrior() {
		BusinessIncomePrior.ForeignSourcedIncome fincome = new BusinessIncomePrior.ForeignSourcedIncome();
		fincome.setAudAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		fincome.setPrimaryForeignCurrency(CurrencyCodeList.CUC);
		
		BusinessIncomePrior bip = new BusinessIncomePrior();
		bip.setAddback(addbackData.addBack());
		bip.setEndDate(LocalDate.now().plusYears(1));
		bip.setForeignSourcedIncome(fincome);
		bip.setHasForeignSourcedIncome(YesNoList.NO);
		bip.setIncomeGreaterThanPreviousYear(YesNoList.NO);
		bip.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bip.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bip.setStartDate(LocalDate.now());
		bip.setTaxOfficeAssessments(YesNoList.NO);
		bip.setXAccountant(RandomStringUtils.randomAlphanumeric(15));		
		return bip;
	}
	
	// BusinessIncomeRecent
	public BusinessIncomeRecent businessIncomeRecent() {

		BusinessIncomeRecent.ForeignSourcedIncome fincome = new BusinessIncomeRecent.ForeignSourcedIncome();
		fincome.setAudAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		fincome.setPrimaryForeignCurrency(CurrencyCodeList.CUC);

		BusinessIncomeRecent bir = new BusinessIncomeRecent();
		bir.setAddback(addbackData.addBack());
		bir.setEndDate(LocalDate.now().plusYears(1));
		bir.setForeignSourcedIncome(fincome);
		bir.setHasForeignSourcedIncome(YesNoList.NO);
		bir.setIncomeGreaterThanPreviousYear(YesNoList.NO);
		bir.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bir.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		bir.setProofCode(ProofCodeSelfEmployedList.TAX_RETURN);
		bir.setStartDate(LocalDate.now());
		bir.setTaxOfficeAssessments(YesNoList.NO);
		bir.setXAccountant(RandomStringUtils.randomAlphanumeric(15));		
		return bir;
	}
	
	// BusinessIncomeYearToDate
	public BusinessIncomeYearToDate businessIncomeYearToDate() {
		
		BusinessIncomeYearToDate biyr = new BusinessIncomeYearToDate();
		biyr.setAddback(addbackData.addBack());
		biyr.setEndDate(LocalDate.now().plusYears(1));
		biyr.setHasForeignSourcedIncome(YesNoList.NO);
		biyr.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		biyr.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		biyr.setStartDate(LocalDate.now());

		return biyr;
	}
	
	// DeclaredIncome
	public DeclaredIncome declaredIncome() {
		DeclaredIncome declared = new DeclaredIncome();
		declared.setIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		declared.setNetIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		return declared;
	}


}
