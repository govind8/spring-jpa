package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Guarantor;
import com.moneycatcha.app.entity.Interview;
import com.moneycatcha.app.entity.LendingGuarantee;
import com.moneycatcha.app.entity.Security;
import com.moneycatcha.app.entity.SecurityAgreementType;
import com.moneycatcha.app.entity.Guarantor.LoanWriterConfirmations;
import com.moneycatcha.app.entity.Guarantor.SecurityGuarantee;
import com.moneycatcha.app.entity.Guarantor.ServicingGuarantee;
import com.moneycatcha.app.entity.Interview.Attendee;
import com.moneycatcha.app.model.DocumentationDeliveryMethodList;
import com.moneycatcha.app.model.GuarantorAccessList;
import com.moneycatcha.app.model.IndependentAdviceTypeRequiredList;
import com.moneycatcha.app.model.LendingGuaranteeTypeList;
import com.moneycatcha.app.model.SecurityPriorityList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class LendingGuaranteeData {

	InsuranceData insuranceData = new InsuranceData();

	PercentOwnedTypeData percentTypeData = new PercentOwnedTypeData();
	
	// List - LendingGuarantee
	public List<LendingGuarantee> lending() {
		List<LendingGuarantee> lendings = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			LendingGuarantee lending = new LendingGuarantee();
			lending.setCrossGuarantee(YesNoList.YES);
			lending.setDetail(insuranceData.accounts());
			lending.setGuarantor(guarantors());
			lending.setLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			lending.setLimitedToFacilityAmount(YesNoList.NO);
			lending.setSecurity(security()); 
			lending.setType(LendingGuaranteeTypeList.INCOME_ONLY);
			lending.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			lending.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			lendings.add(lending);
		}
		return lendings;
	}
	
	// List - Guarantor
	public List<Guarantor> guarantors() {
		List<Guarantor> guarantors = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Guarantor guarantor = new Guarantor();
			guarantor.setAccessType(GuarantorAccessList.FULL_ACCESS);
			guarantor.setAdvisedToSeekIndependentAdvice(YesNoList.YES);
			guarantor.setConfirmNotUnderPressure(YesNoList.YES);
			guarantor.setDemonstratesReadingEnglish(YesNoList.YES);
			guarantor.setDocumentationDeliveryMethod(DocumentationDeliveryMethodList.COLLECT_FROM_NEAREST_BRANCH);
			guarantor.setIndependentAdviceTypeRequired(IndependentAdviceTypeRequiredList.FINANCIAL_AND_LEGAL_ADVICE);
			guarantor.setInterview(interview());
			guarantor.setIsUnderPowerOfAttorney(YesNoList.YES);
			guarantor.setLoanWriterConfirmations(loanwriter());
			guarantor.setSecurityGuarantee(guarantee());
			guarantor.setServicingGuarantee(servicing());
			guarantor.setSuspectDifficultyUnderstandingEnglish(YesNoList.NO);
			guarantor.setSuspectDifficultyUnderstandingObligations(YesNoList.NO);
			guarantor.setSuspectMisrepresentation(YesNoList.NO);
			guarantor.setSuspectUnderBorrowerInfluence(YesNoList.YES);
			guarantor.setSuspectUnderPressure(YesNoList.NO);
			guarantor.setSuspectUnfairConduct(YesNoList.YES);
			guarantor.setUnderstandsGuaranteeSigningPeriod(YesNoList.YES);
			guarantor.setUnderstandsGuarantorObligations(YesNoList.YES);
			guarantor.setXGuarantor(RandomStringUtils.randomAlphabetic(15));
			guarantors.add(guarantor);
		}
		return guarantors;
	}
	
	// List - Security
	public List<Security> security() {
		List<Security> securities = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Security security = new Security();
			security.setAllocatedAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			security.setMortgageNumber(RandomStringUtils.randomAlphanumeric(30));
			security.setPercentOwned(percentTypeData.percentOwnership());
			security.setPriority(SecurityPriorityList.REGISTERED_SECURITY);
			security.setPriorityAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			security.setRequiresOriginalMortgageDocument(YesNoList.YES);
			security.setSecurityAgreement(agreement());
			security.setSignedDocumentsRequired(YesNoList.YES);
			security.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			security.setXSecurity(RandomStringUtils.randomAlphabetic(15));
			securities.add(security);
		}
		return securities;
	}
	
	// interview
	public Interview interview() {
		Interview interview = new Interview();
		interview.setAllApplicantsPresent(YesNoList.YES);
		interview.setAllApplicantsUnderstandEnglish(YesNoList.YES);
		interview.setDate(LocalDate.now().minusMonths(12));
		interview.setInterpreterRecommended(YesNoList.NO);
		interview.setIsFaceToFace(YesNoList.NO);
		interview.setIsOnlyPersonPresent(YesNoList.YES);
		interview.setXLocation(RandomStringUtils.randomAlphabetic(30));
		interview.setAttendee(attendees());
		return interview;
	}
	
	// LoanWriterConfirmations
	public LoanWriterConfirmations loanwriter() {
		LoanWriterConfirmations writer = new LoanWriterConfirmations();
		writer.setAdvisedDocumentsImminentIfSuccessful(YesNoList.NO);
		writer.setAdvisedIdentificationProcessRequired(YesNoList.YES);
		writer.setAdvisedInformationUsedInAssessment(YesNoList.YES);
		writer.setAdvisedOfWithdrawalInstructions(YesNoList.YES);
		writer.setCompletedStatementOfPosition(YesNoList.YES);
		writer.setObtainedAgreementForJointStatementOfPosition(YesNoList.YES);
		writer.setObtainedElectronicCommunicationConsent(YesNoList.YES);
		writer.setObtainedSignedDeclaration(YesNoList.YES);
		writer.setVerifiedGuarantorIncome(YesNoList.YES);
		return writer;
	}
	
	//  SecurityGuarantee
	public SecurityGuarantee guarantee() {
		SecurityGuarantee sg = new SecurityGuarantee();
		sg.setConfirmCouldMeetLoanRepayments(YesNoList.NO);
		sg.setConfirmWouldSellFamilyHome(YesNoList.NO);
		sg.setUnderstandsSecurityGuarantee(YesNoList.YES);
		return sg;
	}
	
	// ServicingGuarantee
	public ServicingGuarantee servicing() {
		ServicingGuarantee sg = new ServicingGuarantee();
		sg.setConfirmIsBenefitToFamily(YesNoList.YES);
		sg.setConfirmRelationshipToBorrower(YesNoList.YES);
		sg.setConfirmUnderstandsBorrowerReliance(YesNoList.YES);
		sg.setConfirmUnderstandsRepaymentsRequired(YesNoList.YES);
		return sg;
	}
	
	// SecurityAgreementType
	public SecurityAgreementType agreement() {
		SecurityAgreementType agreement = new SecurityAgreementType();
		agreement.setHeld(YesNoList.YES);
		agreement.setToBeDischarged(YesNoList.YES);
		agreement.setType(RandomStringUtils.randomAlphabetic(30));
		return agreement;
	}
	
	// Interview Attendees -- 
	public List<Attendee> attendees() {
		List<Attendee> attendees = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Attendee attendee = new Attendee();
			attendee.setXParty(RandomStringUtils.randomAlphabetic(30));
			attendees.add(attendee);
		}
		return attendees;
	}
}
