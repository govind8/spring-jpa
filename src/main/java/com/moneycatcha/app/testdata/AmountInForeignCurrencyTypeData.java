package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.AmountInForeignCurrencyType;
import com.moneycatcha.app.model.CurrencyCodeList;

public class AmountInForeignCurrencyTypeData {

	public AmountInForeignCurrencyType foreignCurrency() {
		AmountInForeignCurrencyType currency = new AmountInForeignCurrencyType();
		currency.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
		currency.setCurrencyCode(CurrencyCodeList.USD);
		currency.setExchangeDateTime(LocalDateTime.now().minusMonths(3));
		currency.setExchangeRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
		return currency;
	}
}
