package com.moneycatcha.app.testdata;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Publisher;
import com.moneycatcha.app.entity.Publisher.RelatedSoftware;

import lombok.Data;

@Data
public class PublisherData {

	ContactData contactData = new ContactData();
	
	RecipientData recipientData = new RecipientData();
	
	public Publisher publisher() {
		
		Publisher publisher = new Publisher();
		publisher.setCompanyName(RandomStringUtils.randomAlphanumeric(10));
		publisher.setContactName(RandomStringUtils.randomAlphanumeric(10));
		publisher.setEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
		publisher.setLixiCode(RandomStringUtils.randomAlphanumeric(10));
		publisher.setPublishedDateTime(LocalDateTime.now());
		publisher.setPhoneNumber(contactData.phoneType()); //object
		publisher.setRelatedSoftware(relatedSoftware()); //object
		publisher.setSoftware(recipientData.software()); //object
		return publisher;
	}
	
	public List<RelatedSoftware> relatedSoftware() {
		
		List<RelatedSoftware> rs = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			RelatedSoftware related = new RelatedSoftware();
			related.setDescription(RandomStringUtils.randomAlphanumeric(10));
			related.setEnvironment(RandomStringUtils.randomAlphanumeric(10));
			related.setLixiCode(RandomStringUtils.randomAlphanumeric(10));
			related.setName(RandomStringUtils.randomAlphanumeric(10));
			related.setTechnicalEmail(RandomStringUtils.randomAlphabetic(5) + "@gmail.com");
			related.setVersion(RandomStringUtils.randomAlphanumeric(10));
			rs.add(related);
		}
		return rs;
	}
}
