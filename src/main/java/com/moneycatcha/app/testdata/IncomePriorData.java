package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.IncomePrior;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class IncomePriorData {
	
	AddBackData addbackData = new AddBackData();

	
	public IncomePrior incomePrior() {
		
		IncomePrior incomePrior = new IncomePrior();
		incomePrior.setCompanyProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrior.setCompanyProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrior.setEndDate(LocalDate.now());
		incomePrior.setIncomeGreaterThanPreviousYear(YesNoList.YES);
		incomePrior.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrior.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrior.setStartDate(LocalDate.now().minusYears(20));
		incomePrior.setTaxOfficeAssessments(YesNoList.YES);
		incomePrior.setXAccountant(RandomStringUtils.randomNumeric(15));
		incomePrior.setAddback(addbackData.addBack());
		return incomePrior;
	}

}
