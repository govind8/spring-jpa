package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.CompanyFinancials;
import com.moneycatcha.app.entity.CompanyFinancials.BalanceSheet;
import com.moneycatcha.app.entity.CompanyFinancials.CurrentMarketData;
import com.moneycatcha.app.entity.CompanyFinancials.ProfitAndLoss;
import com.moneycatcha.app.entity.CompanyFinancials.BalanceSheet.Assets;

import lombok.Data;

@Data
public class CompanyFinancialsData {

	// list of company financials
	
	public List<CompanyFinancials> financials() {
		List<CompanyFinancials> financials = new ArrayList<CompanyFinancials>();
		for(int i=1; i < 4; i++) {
			financials.add(new CompanyFinancials());
		}
		for(int i=0; i < financials.size(); i++)  {
			financials.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			financials.get(i).setEndDate(LocalDate.now().plusYears(i)); 
			financials.get(i).setStartDate(LocalDate.now().minusYears(i));
			financials.get(i).setBalanceSheet(balanceSheet());
			financials.get(i).setCurrentMarketData(currentMarketData());
			financials.get(i).setProfitAndLoss(profitAndLoss());		
			financials.get(i).setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
		}
		return financials;
	}
		
	// Single object
	public CompanyFinancials companyFinancials() {
	   CompanyFinancials compFinancials = new CompanyFinancials();
	   compFinancials.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
	   compFinancials.setEndDate(LocalDate.now().plusYears(1)); 
	   compFinancials.setStartDate(LocalDate.now().minusYears(1));
	   compFinancials.setBalanceSheet(balanceSheet());
	   compFinancials.setCurrentMarketData(currentMarketData());
	   compFinancials.setProfitAndLoss(profitAndLoss());
        return compFinancials;
    }
	
   // BalanceSheet
   public BalanceSheet balanceSheet() {
	   
	   BalanceSheet balanceSheet = new BalanceSheet();
	   balanceSheet.setEquity(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   balanceSheet.setNetAssets(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   balanceSheet.setPaidUpCapital(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   balanceSheet.setRetainedEarnings(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   balanceSheet.setShareholderFunds(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	  		   
	   BalanceSheet.Assets assets = new Assets();
	   assets.setCash(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setCashAndCashEquivalents(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setIntangibles(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setInventory(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setTangibleAssets(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setTotalAssets(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setTotalCurrentAssets(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   assets.setTradeDebtors(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));

	   BalanceSheet.Liabilities liabilities = new BalanceSheet.Liabilities();
	   liabilities.setDebtAtNominalValue(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   liabilities.setTotalCurrentLiabilities(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   liabilities.setTotalLiabilities(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   liabilities.setTradeCreditors(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
   	   
	   balanceSheet.setAssets(assets);
	   balanceSheet.setLiabilities(liabilities);
	   return balanceSheet;

   }
   
   // CurrentMarket Data
   public CurrentMarketData currentMarketData() {
	   CurrentMarketData cmd = new CurrentMarketData();
	   cmd.setValueOfCommonEquity(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   cmd.setValueOfMinorityInterests(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   cmd.setValueOfPreferredEquity(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   return cmd;
   }
   
   // Profit And Loss
   public ProfitAndLoss profitAndLoss() {
	   ProfitAndLoss pnl = new ProfitAndLoss();
	   pnl.setAmortisationExpense(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setCostOfGoodsSold(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setDepreciationExpense(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setEbit(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setEbitda(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setGrossProfit(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setIncomeTax(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setInterestExpense(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setNetProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setPaymentsToDirectors(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setSales(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   pnl.setTotalExpenses(new BigDecimal(RandomStringUtils.randomNumeric(4, 10)));
	   return pnl;
   }   
}
