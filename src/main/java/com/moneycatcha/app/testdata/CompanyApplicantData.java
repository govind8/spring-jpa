package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.AuthorisedSignatory;
import com.moneycatcha.app.entity.CompanyApplicant;
import com.moneycatcha.app.entity.DeclaredIncome;
import com.moneycatcha.app.entity.Director;
import com.moneycatcha.app.entity.RelatedLegalEntities;
import com.moneycatcha.app.entity.Shareholder;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class CompanyApplicantData {

	AddBackData addbackData = new AddBackData();
	
	BeneficialOwnerData beneficialOwnerData = new BeneficialOwnerData();
	
	BusinessData businessData = new BusinessData();
	
	ContactData contactData = new ContactData();
	
	CreditHistoryData creditHistoryData = new CreditHistoryData();
	
	ExistingCustomerData existingCustomerData = new ExistingCustomerData();
	
	FinancialAnalysisData finAnalysisData = new FinancialAnalysisData();
	
	ForeignTaxAssociationData foreignTaxData = new ForeignTaxAssociationData();
	
	IncomePreviousData incomePreviousData = new IncomePreviousData();
	
	IncomePriorData incomePriorData = new IncomePriorData();
	
	IncomeRecentData incomeRecentData = new IncomeRecentData();
	
	IncomeYearToDateData incomeYearToDateData = new IncomeYearToDateData();
	
	PartnerData partnerData = new PartnerData();
	
	ResponsibleLendingData responsibleData = new ResponsibleLendingData();
	
	SourceOfWealthData sowData = new SourceOfWealthData();

	
	// List of Company Applicants
	public List<CompanyApplicant> applicants() {
		
		// company applicant
		List<CompanyApplicant> applicants = new ArrayList<CompanyApplicant>();
		for (int i =0; i < 2; i++) {
			CompanyApplicant applicant = new CompanyApplicant();
			applicant.setAbn(RandomStringUtils.randomAlphabetic(15));
			applicant.setAbnVerified(YesNoList.NO);
			applicant.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			applicant.setAllowDirectMarketing(YesNoList.NO);
			applicant.setAllowThirdPartyDisclosure(YesNoList.YES);
			applicant.setApplicantType(ApplicantTypeList.BORROWER);
			applicant.setAuthorisedSignatory(authorisedSignatory());
			applicant.setBeneficialOwner(beneficialOwnerData.setBeneficalOwners());
			applicant.setBusiness(businessData.business());
			applicant.setContact(contactData.companyApplicant());
			applicant.setExistingCustomer(existingCustomerData.existingCustomer());
			applicant.setFinancialAnalysis(finAnalysisData.financialAnalysis());
			applicant.setForeignTaxAssociation(foreignTaxData.foreignTaxAssociation());
			applicant.setIncomePrevious(incomePreviousData.incomePrevious());
			applicant.setIncomePrior(incomePriorData.incomePrior());
			applicant.setIncomeRecent(incomeRecentData.incomeRecent());
			applicant.setIncomeYearToDate(incomeYearToDateData.incomeYearToDate());
			applicant.setPartner(partnerData.setPartners());
			applicant.setRelatedLegalEntities(relatedLegalEntities());
			applicant.setResponsibleLending(responsibleData.responsibleLendingType());
			applicant.setShareholder(shareholder());
			applicant.setSourceOfWealth(sowData.sourceOfWealth());
			applicant.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			applicants.add(applicant);
		}

		return applicants;
	}

	// Application - Company Applicant -authorised signatory
	public List<AuthorisedSignatory> authorisedSignatory() {
		List<AuthorisedSignatory> asList = new ArrayList<AuthorisedSignatory>();
		for (int i=0; i < 2 ; i++) {
			AuthorisedSignatory as = new AuthorisedSignatory();
			as.setPositionTitle(RandomStringUtils.randomAlphabetic(10));
			as.setXSignatory(RandomStringUtils.randomAlphabetic(10));
			asList.add(as);
		}
		return asList;
	}

	
	// Application -> Company Applicant -> DeclaredIncome - Data
	public DeclaredIncome declaredIncome() {
		DeclaredIncome declaredIncome = new DeclaredIncome();
		declaredIncome.setIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 6)));
		declaredIncome.setNetIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 6)));
		return declaredIncome;
	}

	// Application -> Company Applicant -> Director Data
	public List<Director> directors() {
		List<Director> directors = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Director director = new Director();
			director.setXDirector(RandomStringUtils.randomAlphabetic(10));
			directors.add(director);
		}
		return directors;
	}

	// Application -> Company Applicant -> Related Data
	public RelatedLegalEntities relatedLegalEntities() {
		RelatedLegalEntities rle = new RelatedLegalEntities();
		rle.setCurrentCustomers(RandomStringUtils.randomAlphabetic(10));
		rle.setEntityCount(new BigInteger(RandomStringUtils.randomNumeric(3)));
		return rle;
	}
	
	// Application -> Company Applicant -> Shareholders Data
	public List<Shareholder> shareholder() {
		
		List<Shareholder> shareholders = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Shareholder holder = new Shareholder();
			holder.setPercentOwned(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			holder.setXShareholder(RandomStringUtils.randomAlphabetic(10));
			shareholders.add(holder);
		}
		return shareholders;
	}

}
