package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.BusinessChannel;

import lombok.Data;


@Data
public class BusinessChannelData {

	public BusinessChannel channelData() {
		
		BusinessChannel channel = new BusinessChannel();
		channel.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		channel.setAcn(RandomStringUtils.randomAlphanumeric(15));
		channel.setAbn(RandomStringUtils.randomNumeric(16));
		channel.setCompanyName(RandomStringUtils.randomAlphabetic(50));
		BusinessChannel.Contact contact = new BusinessChannel.Contact();
		contact.setEmail("acb@example.com");
		contact.setWebAddress("www.examplle.com");
		contact.setXAddress(RandomStringUtils.randomAlphabetic(40));
		channel.setContact(contact);
		
		return channel;
	}
	
}
