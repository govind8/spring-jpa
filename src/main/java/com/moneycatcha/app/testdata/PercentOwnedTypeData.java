package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Owner;
import com.moneycatcha.app.entity.PercentOwnedType;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class PercentOwnedTypeData {
	
	// DepositAccountDetails - Account Owners
	public PercentOwnedType percentOwnership() {
		PercentOwnedType po = new PercentOwnedType();
		po.setOwner(owners());
		po.setProportions(ProportionsList.SPECIFIED);
		return po;
	}
	
	// Owners
	public List<Owner> owners() {
		List<Owner> owners = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Owner owner = new Owner();
			owner.setPercent(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			owner.setXParty(RandomStringUtils.randomAlphabetic(20));
			owner.setPrimaryBorrower(YesNoList.YES);
			owners.add(owner);
		}
		return owners;
	}
}
