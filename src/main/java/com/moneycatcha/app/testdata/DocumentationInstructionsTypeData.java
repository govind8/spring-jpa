package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.DocumentationInstructionsType;
import com.moneycatcha.app.model.DocumentationInstructionsMethodList;
import com.moneycatcha.app.model.SendDocumentsToList;

import lombok.Data;

@Data
public class DocumentationInstructionsTypeData {
	public DocumentationInstructionsType document() {
		DocumentationInstructionsType doc = new DocumentationInstructionsType();
		doc.setMethod(DocumentationInstructionsMethodList.POST);
		doc.setSendDocumentsTo(SendDocumentsToList.APPLICANT);
		doc.setXDeliverTo(RandomStringUtils.randomAlphanumeric(15));
		doc.setXNominatedAuthority(RandomStringUtils.randomAlphanumeric(15));
		
		return doc;
	}
	
}
