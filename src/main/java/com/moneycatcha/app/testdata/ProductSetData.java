package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ProductSet;
import com.moneycatcha.app.entity.ProductSet.Contact;
import com.moneycatcha.app.entity.ProductSet.Product;

import lombok.Data;

@Data
public class ProductSetData {

	
	// ProductSet
	public List<ProductSet> productSet() {
		
		List<ProductSet> productSet = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			
			Contact contact = new Contact();
			contact.setXMailingAddress(RandomStringUtils.randomAlphabetic(10));
			
			ProductSet set = new ProductSet();
			set.setContact(contact);
			set.setProduct(product());
			set.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			set.setXPrimaryApplicant(RandomStringUtils.randomAlphabetic(10));
			set.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			productSet.add(set);
		}
	
		return productSet;
	}
	
	// product
	public List<Product> product() {
		List<Product> products = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Product product = new Product();
			product.setXFinancialProduct(RandomStringUtils.randomAlphabetic(10));
			products.add(product);
		}

		return products;
	}
}
