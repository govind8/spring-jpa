package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.RelatedCompany;
import com.moneycatcha.app.model.BusinessStructureFullList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class RelatedCompanyData {

	ContactData contactData = new ContactData();
	
	PartnerData partnerData = new PartnerData();
	
	// RelatedCompany
	public List<RelatedCompany> RelatedCompany() {
		
		List<RelatedCompany> company = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RelatedCompany rc = new RelatedCompany();
			rc.setAbn(RandomStringUtils.randomAlphabetic(15));
			rc.setAbnVerified(YesNoList.YES);
			rc.setAcn(RandomStringUtils.randomAlphabetic(15));
			rc.setBusinessName(RandomStringUtils.randomAlphabetic(15));
			rc.setBusinessNameSameAsCompanyName(YesNoList.NO);
			rc.setBusinessStructure(BusinessStructureFullList.COMPANY);
			rc.setCompanyName(RandomStringUtils.randomAlphabetic(15));
			rc.setContact(contactData.RelatedCompanyContact()); //object
			rc.setDateRegistered(LocalDate.now().minusYears(5));
			rc.setGstRegisteredDate(LocalDate.now().minusYears(3));
			rc.setNumberOfPartners(new BigInteger(RandomStringUtils.randomNumeric(2)));
			rc.setPartner(partnerData.setPartners()); //object
			rc.setRegisteredForGST(YesNoList.NO);
			rc.setRegisteredInCountry(CountryCodeList.BV);
			rc.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			rc.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			company.add(rc);
		}
	
		return company;
	}
}
