package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.DealingNumberType;
import com.moneycatcha.app.model.AuStateList;

import lombok.Data;

@Data
public class DealingNumberTypeData {

	public List<DealingNumberType> dealingNumberType() {
		List<DealingNumberType> dealingTypes = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			DealingNumberType dealingNumber = new DealingNumberType();
			dealingNumber.setAustralianState(AuStateList.ACT);
			dealingNumber.setBookNumber(RandomStringUtils.randomAlphabetic(15));
			dealingNumber.setRegisteredNumber(RandomStringUtils.randomAlphabetic(15));
			dealingNumber.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			dealingTypes.add(dealingNumber);
		}
		return dealingTypes;
	}
}
