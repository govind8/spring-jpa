package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.StatementOfPosition;
import com.moneycatcha.app.entity.StatementOfPosition.Applicant;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class StatementOfPositionsData {

	// List<StatementOfPosition> 
	public List<StatementOfPosition> statementOfPosition() {
		
		List<StatementOfPosition> statement = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			StatementOfPosition sop = new StatementOfPosition();
			sop.setApplicants(applicants());
			sop.setIsSigned(YesNoList.YES);
			sop.setUniqueID(RandomStringUtils.randomAlphabetic(15));		
			sop.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			statement.add(sop);
		}
		return statement;
	}
	
	// List<StatementOfPosition> -> List<Applicant>
	public List<Applicant> applicants() {
		List<Applicant> applicants = new ArrayList<Applicant>();
		
		for (int i=0; i < 2; i++) {
			Applicant applicant = new Applicant();
			applicant.setXApplicant(RandomStringUtils.randomAlphabetic(15));
			applicants.add(applicant);
		}
		return applicants;
	}
	
}
