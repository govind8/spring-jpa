package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Arrears;
import com.moneycatcha.app.entity.DSH;
import com.moneycatcha.app.entity.DiscountMargin;
import com.moneycatcha.app.entity.DistinctLoanPeriod;
import com.moneycatcha.app.entity.LendingPurpose;
import com.moneycatcha.app.entity.Liability;
import com.moneycatcha.app.entity.RateComposition;
import com.moneycatcha.app.entity.Repayment;
import com.moneycatcha.app.entity.Software;
import com.moneycatcha.app.entity.TermsAndConditions;
import com.moneycatcha.app.entity.Liability.ContinuingRepayment;
import com.moneycatcha.app.entity.Liability.OriginalTerm;
import com.moneycatcha.app.entity.Liability.PercentOwned;
import com.moneycatcha.app.entity.Liability.RemainingTerm;
import com.moneycatcha.app.entity.RateComposition.BaseRate;
import com.moneycatcha.app.model.AbsLendingPurposeCodeList;
import com.moneycatcha.app.model.ClearingFromOtherSourceList;
import com.moneycatcha.app.model.ClearingFromThisLoanList;
import com.moneycatcha.app.model.CreditCardTypeList;
import com.moneycatcha.app.model.DurationUnitsList;
import com.moneycatcha.app.model.InterestChargeFrequencyList;
import com.moneycatcha.app.model.InterestTypeList;
import com.moneycatcha.app.model.LiabilityTypeList;
import com.moneycatcha.app.model.LoanPaymentScheduleTypeList;
import com.moneycatcha.app.model.LoanTermUnitsList;
import com.moneycatcha.app.model.NccpStatusList;
import com.moneycatcha.app.model.PaymentTimingList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.PrincipalRefinancingReasonList;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.RepaymentFrequencyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class LiabilityData {
	
	AmountInForeignCurrencyTypeData foreignData = new AmountInForeignCurrencyTypeData();
	
	DocumentationInstructionsTypeData documentData = new DocumentationInstructionsTypeData();
	
	FinancialAccountTypeData accountData = new FinancialAccountTypeData();
	
	PercentOwnedTypeData percentOwnedData = new PercentOwnedTypeData();
	
	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	DepositAccountDetailsData depositData = new DepositAccountDetailsData();

	DurationTypeData durationData = new DurationTypeData();
	
	// List - Liability
	public List<Liability> liability() {
		List<Liability> liabilities = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Liability liability = new Liability();
			liability.setAccelerationPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setOriginalAmountRequestedInForeignCurrency(foreignData.foreignCurrency());
			liability.setAnnualInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setArrears(arrears()); // object
			liability.setAvailableForRedrawAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setBalloonRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setBalloonRepaymentDate(LocalDate.now().minusWeeks(2));
			liability.setClearingFromOtherSource(ClearingFromOtherSourceList.PARTIAL);
			liability.setClearingFromOtherSourceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setClearingFromThisLoan(ClearingFromThisLoanList.PARTIAL);
			liability.setClearingFromThisLoanAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setClosingOnSettlement(YesNoList.YES);
			liability.setContinuingRepayment(continuing()); // object
			liability.setCreditLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setCreditCardType(CreditCardTypeList.VISA);
			liability.setCreditRiskGrade(RandomStringUtils.randomAlphabetic(20));
			liability.setDescription(RandomStringUtils.randomAlphabetic(50));
			liability.setDiscountMargin(margin()); //object
			liability.setDocumentationInstructions(documentData.document()); // object
			liability.setDSH(dsh()); // object
			liability.setFeaturesSelected(depositData.featuresSelected().get(0)); // object
			liability.setAccountNumber(accountData.accountNumber());
			liability.setHasArrears(YesNoList.YES);
			liability.setHasPreviousArrears(YesNoList.YES);
			liability.setHasUndrawnFunds(YesNoList.NO);
			liability.setInAdvanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setIsOriginalAmountRequestedInForeignCurrency(YesNoList.NO);
			liability.setInterestCalculationFrequency(InterestChargeFrequencyList.FORTNIGHTLY);
			liability.setInterestChargeFrequency(InterestChargeFrequencyList.MONTHLY);
			liability.setLenderAssessmentReason(RandomStringUtils.randomAlphabetic(50));
			liability.setLenderAssessmentRequired(YesNoList.YES);
			liability.setLendingPurpose(lendingPurpose()); //object
			liability.setLimitExceededCurrently(YesNoList.YES);
			liability.setLimitExceededPreviously(YesNoList.YES);
			liability.setMaturityDate(LocalDate.now().plusYears(1));
			liability.setMinimumRepaymentRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setNccpStatus(NccpStatusList.REGULATED);
			liability.setNegativelyGeared(YesNoList.YES);
			liability.setNegativelyGearedPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setNewLimit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			liability.setNonCapitalisedInterest(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setOriginalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			liability.setOriginalLoanPurpose(PrimaryPurposeLoanPurposeList.BUSINESS);
			liability.setOriginalTerm(originalTerm()); // Object
			liability.setOriginationDate(LocalDate.now().minusYears(3));
			liability.setOutstandingBalance(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			liability.setOverdrawn(YesNoList.YES);
			liability.setPercentOwned(percentOwned()); // object
			liability.setProductCode(RandomStringUtils.randomAlphabetic(5));
			liability.setProductName(RandomStringUtils.randomAlphabetic(5));
			liability.setRateComposition(composition()); //object
			liability.setRefinanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			liability.setRefinanceCosts(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			liability.setRemainingTerm(remainingTerm());
			liability.setRepaidDate(LocalDate.now().minusDays(20));
			liability.setRepayment(repayments()); //object
			liability.setSecured(YesNoList.YES);
			liability.setSecurity(lendingData.security());
			liability.setSmsfLoan(YesNoList.NO);
			liability.setSoftware(software()); //object
			liability.setSuspended(YesNoList.NO);
			liability.setTermsAndConditions(termsandConditions()); //object
			liability.setType(LiabilityTypeList.HIRE_PURCHASE);
			liability.setUndrawnAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			liability.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			liability.setVerified(YesNoList.YES);
			liability.setWrittenOff(YesNoList.NO);
			liability.setXCustomerTransactionAnalysis(RandomStringUtils.randomAlphanumeric(10));
			liability.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			liabilities.add(liability);
		}
		return liabilities;
	}
	
	//new BigDecimal(RandomStringUtils.randomNumeric(2, 10))
	//YesNoList.YES 
	
	// Arrears
	public Arrears arrears() {
		Arrears arrears = new Arrears();
		arrears.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		arrears.setNumberOfMissedPayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
		arrears.setToBePaidOut(YesNoList.NO);
		return arrears;
	}
	
	// List - ContinuingRepayment
	public List<ContinuingRepayment> continuing() {
		List<ContinuingRepayment> repayments = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			ContinuingRepayment repayment = new ContinuingRepayment();
			repayment.setLoanPaymentScheduleType(LoanPaymentScheduleTypeList.HALF_THE_MONTHLY_AMOUNT_PAID_IN_FORTNIGHTLY_PAYMENTS);
			repayment.setMinimumAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setPaymentTiming(PaymentTimingList.IN_ADVANCE);
			repayment.setPaymentType(PaymentTypeList.PREPAID_INTEREST);
			repayment.setRemainingRepayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
			repayment.setRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setRepaymentFrequency(RepaymentFrequencyList.QUARTERLY);
			repayment.setTaxDeductible(YesNoList.YES);
			repayment.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			repayments.add(repayment);
		}
		return repayments;
	}
	

	// List - DiscountMargin
	public List<DiscountMargin> margin() {
		List<DiscountMargin> discounts = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			DiscountMargin discount = new DiscountMargin();
			discount.setDiscountedAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			discount.setDiscountRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			discount.setDiscountReason("holiday season");
			discount.setDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
			discount.setDurationUnits(DurationUnitsList.YEARS);
			discounts.add(discount);
		}
		return discounts;
	}
	
	// DSH
	public DSH dsh() {
		DSH dsh = new DSH();
		dsh.setAssignmentCertificate(RandomStringUtils.randomAlphanumeric(15));
		dsh.setEntitlementCertificate(RandomStringUtils.randomAlphanumeric(15));
		dsh.setFileNumber(RandomStringUtils.randomAlphanumeric(15));
		dsh.setSubsidised(YesNoList.NO);
		dsh.setSupplement(YesNoList.NO);
		return dsh;
	}
	
	// List - LendingPurpose
	public List<LendingPurpose> lendingPurpose() {
		List<LendingPurpose> purposes = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			LendingPurpose purpose = new LendingPurpose();
			purpose.setAbsLendingPurpose(RandomStringUtils.randomAlphanumeric(15));
			purpose.setAbsLendingPurposeCode(AbsLendingPurposeCodeList.ABS_129);
			purpose.setDescription(RandomStringUtils.randomAlphanumeric(15));
			purpose.setDetail(percentOwnedData.percentOwnership());
			purpose.setIncludesRefinancing(YesNoList.NO);
			purpose.setLenderCode(RandomStringUtils.randomAlphanumeric(15));
			purpose.setPayoutQuoteObtained(YesNoList.YES);
			purpose.setRefinancingReason(PrincipalRefinancingReasonList.SPECIFIC_PRODUCT_OR_FEATURE);
			purpose.setSecurityForMarginLoan(YesNoList.YES);
			purpose.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			purposes.add(purpose);
		}
		return purposes;
	}
	
	// Original Term
	public OriginalTerm originalTerm() {
		OriginalTerm original = new OriginalTerm();
		original.setDistinctLoanPeriod(distinct());
		original.setInterestOnlyEndDate(LocalDate.now().plusYears(1));
		original.setInterestType(InterestTypeList.FIXED_RATE);
		original.setInterestTypeDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
		original.setInterestTypeUnits(LoanTermUnitsList.DAYS);
		original.setPaymentType(PaymentTypeList.INTEREST_CAPITALISED);
		original.setPaymentTypeDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
		original.setPaymentTypeUnits(LoanTermUnitsList.WEEKS);
		original.setRolloverPeriodDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
		original.setRolloverPeriodUnits(LoanTermUnitsList.WEEKS);
		original.setTotalTermDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
		original.setTotalTermUnits(LoanTermUnitsList.WEEKS);
		return original;
	}

	// PercentOwned
	public PercentOwned percentOwned() {
		PercentOwned owned = new PercentOwned();
		owned.setOwner(percentOwnedData.owners());
		owned.setProportions(ProportionsList.NOT_SPECIFIED);
		return owned;
	}
	
	// List - RateComposition
	public List<RateComposition> composition() {
		List<RateComposition> compositions = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RateComposition composition = new RateComposition();
			composition.setBaseRate(baserate());
			composition.setCustomerRiskMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setDisclosedToCustomer(YesNoList.YES);
			composition.setIntroducerMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setIntroductoryMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setNetCustomerRiskMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setOriginatorMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setPaymentTypeMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setPricingConstruct(RandomStringUtils.randomAlphabetic(10));
			composition.setProductMargin(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setSubsidyBaseAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setSubsidyBasePercent(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setSubsidyRateSacrifice(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setTermPremium(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setTotalInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			composition.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			compositions.add(composition);
		}
		return compositions;
	}
	
	// BaseRate
	public BaseRate baserate() {
		BaseRate rate = new BaseRate();
		rate.setCode(RandomStringUtils.randomAlphanumeric(5));
		rate.setName(RandomStringUtils.randomAlphabetic(10));
		rate.setRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		return rate;
	}
	
	// Base Rate - DistinctLoanPeriod
	public List<DistinctLoanPeriod> distinct() {
		List<DistinctLoanPeriod> distincts= new ArrayList<>();
		for(int i=0; i < 3; i++) {
			DistinctLoanPeriod distinct = new DistinctLoanPeriod();
			distinct.setDuration(durationData.duration());
			distinct.setEndDate(LocalDate.now());
			distinct.setInterestType(InterestTypeList.FIXED_RATE);
			distinct.setPaymentType(PaymentTypeList.INTEREST_CAPITALISED);
			distinct.setRepayment(repayments());
			distinct.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(5)));
			distinct.setStartDate(LocalDate.now().minusYears(3));
			distinct.setTotalFeesAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			distinct.setTotalInterestAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			distinct.setTotalRepayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
			distinct.setTotalRepaymentsAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			distinct.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			distincts.add(distinct);
		}
		return distincts;
	}
	
	// Repayments
	// List - Repayment
	public List<Repayment> repayments() {
		List<Repayment> repayments = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Repayment repayment = new Repayment();
			repayment.setInterestPayment(YesNoList.NO);
			repayment.setLoanPaymentScheduleType(LoanPaymentScheduleTypeList.EVEN_PRINCIPAL_PAYMENTS);
			repayment.setMinimumAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			repayment.setPaymentTiming(PaymentTimingList.IN_ADVANCE);
			repayment.setPaymentType(PaymentTypeList.INTEREST_CAPITALISED);
			repayment.setPrincipalPayment(YesNoList.YES);
			repayment.setRegular(YesNoList.NO);
			repayment.setRemainingRepayments(new BigInteger(RandomStringUtils.randomNumeric(2)));
			repayment.setRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			repayment.setRepaymentFrequency(RepaymentFrequencyList.MONTHLY);
			repayment.setTaxDeductible(YesNoList.NO);
			repayment.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			repayments.add(repayment);
		}
		return repayments;
	}
	
	// Embedded - RemainingTerm
	public RemainingTerm remainingTerm() {
		RemainingTerm remainingTerm = new RemainingTerm();
		remainingTerm.setDuration(new BigInteger(RandomStringUtils.randomNumeric(2)));
		remainingTerm.setUnits(LoanTermUnitsList.WEEKS);
		return remainingTerm;
	}
	// Software
	public Software software() {
		Software software = new Software();
		software.setDescription(RandomStringUtils.randomAlphanumeric(10));
		software.setEnvironment(RandomStringUtils.randomAlphanumeric(10));
		software.setLixiCode(RandomStringUtils.randomAlphanumeric(10));
		software.setName(RandomStringUtils.randomAlphanumeric(10));
		software.setTechnicalEmail("test@test.com");
		software.setVersion("v1.0");
		return software;
	}

	// TermsAndConditions
	public List<TermsAndConditions> termsandConditions() {
		List<TermsAndConditions> terms = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			TermsAndConditions term = new TermsAndConditions();
			term.setTermsCode(RandomStringUtils.randomAlphanumeric(10));
			term.setTermsName(RandomStringUtils.randomAlphanumeric(10));
			terms.add(term);
		}
		return terms;
	}
}
