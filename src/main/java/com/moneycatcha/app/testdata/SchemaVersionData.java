package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.SchemaVersion;

import lombok.Data;


@Data
public class SchemaVersionData {

	// SchemaVersion
	public SchemaVersion schemaVersion() {
		SchemaVersion schema = new SchemaVersion();
		schema.setGuidebookCode(RandomStringUtils.randomAlphanumeric(5));
		schema.setGuidebookName(RandomStringUtils.randomAlphanumeric(5));
		schema.setGuidebookVersion(RandomStringUtils.randomAlphanumeric(5));
		schema.setLixiCustomVersion(RandomStringUtils.randomAlphanumeric(5));
		schema.setLixiTransactionType(RandomStringUtils.randomAlphanumeric(5));
		schema.setLixiVersion(RandomStringUtils.randomAlphanumeric(5));
		return schema;		
		
	}
}
