package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Fee;
import com.moneycatcha.app.entity.LoanToValuationRatio;
import com.moneycatcha.app.entity.ServiceabilityResults;
import com.moneycatcha.app.entity.Summary;
import com.moneycatcha.app.entity.Fee.Percentage;
import com.moneycatcha.app.entity.LoanToValuationRatio.ContributingValuation;
import com.moneycatcha.app.entity.ServiceabilityResults.NetDisposableIncome;
import com.moneycatcha.app.entity.ServiceabilityResults.TotalGrossIncome;
import com.moneycatcha.app.entity.ServiceabilityResults.TotalNetIncome;
import com.moneycatcha.app.entity.ServiceabilityResults.TotalSystemCalculatedExpenses;
import com.moneycatcha.app.entity.ServiceabilityResults.TotalSystemCalculatedLivingExpenses;
import com.moneycatcha.app.entity.ServiceabilityResults.TotalUserStatedLivingExpenses;
import com.moneycatcha.app.model.CalculationBasisList;
import com.moneycatcha.app.model.FeeCategoryList;
import com.moneycatcha.app.model.FeePaymentTimingList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.PayFeesFromList;
import com.moneycatcha.app.model.RepaymentFrequencyList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class SummaryData {
	
	ContactData contactData = new ContactData();
	
	DocumentationInstructionsTypeData documentData = new DocumentationInstructionsTypeData();
	
	DurationTypeData durationData = new DurationTypeData();
	
	LoanDetailsData loanDetailsData = new LoanDetailsData();
	
	FinancialAccountTypeData financeData = new FinancialAccountTypeData();
	
	OverviewData overviewData = new OverviewData();
	
	
	// Summary
	public Summary summary() {
		
		Summary summary = new Summary();
		summary.setAllPartiesAgreeToElectronicSignature(YesNoList.NO);
		summary.setDocumentationInstructions(documentData.document());  //object
		summary.setFee(fee()); //object
		summary.setLoanToValuationRatio(loanToValuationRatio());  //object
		summary.setServiceabilityResults(serviceabilityResults()); //object
		summary.setSignature(overviewData.signatureType());
	
		return summary;
	}
	
	// Fee
	public List<Fee> fee() {
		
		Percentage percentage = new Percentage();
		percentage.setCalculationBasis(CalculationBasisList.BALANCE);
		percentage.setRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));

		List<Fee> fees = new ArrayList<>();
		for(int i=0; i < 2; i++) {
			Fee fee = new Fee();
			fee.setAccountNumber(financeData.accountNumber());
			fee.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			fee.setApplicableDuration(durationData.duration()); //object
			fee.setCapitaliseFee(YesNoList.NO);
			fee.setCategory(FeeCategoryList.GOVERNMENT);
			fee.setCreditCard(loanDetailsData.creditCard());
			fee.setDescription(RandomStringUtils.randomAlphabetic(10));
			fee.setFeeCode(RandomStringUtils.randomAlphabetic(10));
			fee.setFrequency(FrequencyFullList.DAILY);
			fee.setGstAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			fee.setIsDisclosed(YesNoList.NO);
			fee.setIsEstimated(YesNoList.YES);
			fee.setItcAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			fee.setNumberOfRepeats(new BigInteger(RandomStringUtils.randomNumeric(2)));
			fee.setPaid(YesNoList.NO);
			fee.setPayableTo(RandomStringUtils.randomAlphabetic(10));
			fee.setPayFeesFrom(PayFeesFromList.ACCOUNT);
			fee.setPayableToAccount(financeData.accountNumber());
			fee.setPayFeesFrom(PayFeesFromList.ACCOUNT);
			fee.setPaymentTiming(FeePaymentTimingList.ON_CREDIT_DRAWDOWN);
			fee.setPercentage(percentage);
			fee.setStartDate(LocalDate.now().minusYears(1));
			fee.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			fee.setXAccount(RandomStringUtils.randomAlphabetic(10));
			fee.setXFinancialProduct(RandomStringUtils.randomAlphabetic(10));
			fee.setXProductSet(RandomStringUtils.randomAlphabetic(10));
			fee.setXSecurity(RandomStringUtils.randomAlphabetic(10));
			fees.add(fee);
		}
	
		return fees;
	}
	
	// LoanToValuationRatio
	public LoanToValuationRatio loanToValuationRatio() {
		LoanToValuationRatio ratio = new LoanToValuationRatio();
		ratio.setApplicationLVR(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		ratio.setContributingValuation(contributingValuation()); //object
		ratio.setPeakDebtLVR(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		ratio.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		
		return ratio;
	}
	
	
	// ContributingValuation
	public List<ContributingValuation> contributingValuation() {
		
		List<ContributingValuation> loans = new ArrayList<>();
		for(int i=0; i < 2; i++) {
			ContributingValuation contributing = new ContributingValuation();
			contributing.setXValuation(RandomStringUtils.randomAlphanumeric(15));
			loans.add(contributing);
		}
	
		return loans;
	}
	
		
	// ServiceabilityResults
	public List<ServiceabilityResults> serviceabilityResults() {
		
		TotalGrossIncome tgi = new TotalGrossIncome();
		tgi.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		tgi.setFrequency(FrequencyShortList.MONTHLY);
		
		NetDisposableIncome ndi = new NetDisposableIncome();
		ndi.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		ndi.setFrequency(FrequencyShortList.MONTHLY);
		
		TotalNetIncome tni = new TotalNetIncome();
		tni.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		tni.setFrequency(FrequencyShortList.MONTHLY);
		
		TotalSystemCalculatedExpenses tsce = new TotalSystemCalculatedExpenses();
		tni.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		tni.setFrequency(FrequencyShortList.MONTHLY);
		
		TotalSystemCalculatedLivingExpenses tscle = new TotalSystemCalculatedLivingExpenses();
		tscle.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		tscle.setFrequency(FrequencyShortList.MONTHLY);
		
		TotalUserStatedLivingExpenses tusle = new TotalUserStatedLivingExpenses();
		tusle.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		tusle.setFrequency(FrequencyShortList.MONTHLY);
		
		List<ServiceabilityResults> serviceabilities = new ArrayList<>();
		for(int i=0; i < 2; i++) {
			ServiceabilityResults results = new ServiceabilityResults();
			results.setApplicant(applicant()); //object
			results.setDsr(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setDti(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setGroupIdentifier(RandomStringUtils.randomAlphabetic(10));
			results.setGroupName(RandomStringUtils.randomAlphabetic(10));
			results.setLti(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setMaximumLoanAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setMaximumRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setMaximumRepaymentFrequency(RepaymentFrequencyList.DAILY);
			results.setMrim(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setNetDisposableIncome(ndi);
			results.setNsr(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			results.setServiceable(YesNoList.YES);
			results.setTotalGrossIncome(tgi); //object
			results.setTotalNetIncome(tni); //object
			results.setTotalSystemCalculatedExpenses(tsce); //object
			results.setTotalSystemCalculatedLivingExpenses(tscle); //object
			results.setTotalUserStatedLivingExpenses(tusle); //object
			results.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		
			serviceabilities.add(results);
		}
	
		return serviceabilities;
	}
	
	// ServiceabilityResults - Applicants
	public List<ServiceabilityResults.Applicant> applicant() {
		
		List<ServiceabilityResults.Applicant> applicants = new ArrayList<>();
		for(int i=0; i < 2; i++) {
			ServiceabilityResults.Applicant applicant = new ServiceabilityResults.Applicant();
			applicant.setXParty(RandomStringUtils.randomAlphanumeric(15));
			applicants.add(applicant);
		}
	
		return applicants;
	}
	

}
