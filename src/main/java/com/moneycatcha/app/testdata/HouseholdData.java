package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Household;
import com.moneycatcha.app.entity.Household.Dependant;
import com.moneycatcha.app.entity.Household.EducationExpenses;
import com.moneycatcha.app.entity.Household.ExpenseDetails;
import com.moneycatcha.app.entity.Household.Dependant.FinancialProvider;
import com.moneycatcha.app.entity.Household.ExpenseDetails.LivingExpense;
import com.moneycatcha.app.entity.Household.ExpenseDetails.OtherCommitment;
import com.moneycatcha.app.entity.Household.ExpenseDetails.TotalSystemCalculatedLivingExpenses;
import com.moneycatcha.app.entity.Household.ExpenseDetails.TotalUserStatedLivingExpenses;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.LivingExpenseCategoryList;
import com.moneycatcha.app.model.OtherCommitmentCategoryList;

import lombok.Data;

@Data
public class HouseholdData {

	PercentOwnedTypeData percentTypeData = new PercentOwnedTypeData();
	
	// List - Household
	public List<Household> households() {
		List<Household> households = new ArrayList<>();
		for(int i=0; i < 4; i++) {
			Household household = new Household();
			household.setDependant(dependants());
			household.setEducationExpenses(education());
			household.setExpenseDetails(details());
			household.setName(RandomStringUtils.randomAlphabetic(25));
			household.setNumberOfAdults(new BigInteger(RandomStringUtils.randomNumeric(10)));
			household.setNumberOfDependants(new BigInteger(RandomStringUtils.randomNumeric(10)));
			household.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			household.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			households.add(household);
		}
		return households;
	}
	
	
	// List - Dependant
	public List<Dependant> dependants() {
		List<Dependant> dependants = new ArrayList<>();
		for(int i=0; i < 4; i++)  {
			Dependant dependant = new Dependant();
			dependant.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
			dependant.setDateOfBirth(LocalDate.now().minusYears(40));
			dependant.setFinancialProvider(provider());
			dependant.setName(RandomStringUtils.randomAlphabetic(25));
			dependant.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			dependants.add(dependant);
		}
		return dependants;
	}
	
	// Education Expenses
	public EducationExpenses education() {
		EducationExpenses education = new EducationExpenses();
		education.setNumberOfAdultsInFullTimeStudy(new BigInteger("1"));
		education.setNumberOfAdultsInPartTimeStudy(new BigInteger("3"));
		education.setNumberOfChildrenInPrivateSchool(new BigInteger("3"));
		education.setNumberOfChildrenInPublicSchool(new BigInteger("1"));
		return education;
	}
	
	
	// Expense Details
	public ExpenseDetails details() {
		ExpenseDetails details = new ExpenseDetails();
		details.setLivingExpense(living());
		details.setOtherCommitment(commitments());
		details.setTotalSystemCalculatedLivingExpenses(systemCalculated());
		details.setTotalUserStatedLivingExpenses(userStated());
		return details;
	}
	
	// List - LivingExpense
	public List<LivingExpense> living() {
		List<LivingExpense> expenses = new ArrayList<>();
		for(int i=0; i < 4; i++) {
			LivingExpense expense = new LivingExpense();
			expense.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			expense.setCategory(LivingExpenseCategoryList.CLOTHING_AND_PERSONAL_CARE);
			expense.setDescription(RandomStringUtils.randomAlphabetic(255));
			expense.setEndDate(LocalDate.now().plusYears(1));
			expense.setFrequency(FrequencyFullList.SEASONAL);
			expense.setPercentResponsible(percentTypeData.percentOwnership());
			expense.setStartDate(LocalDate.now().minusYears(2));
			expense.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			expenses.add(expense);

		}
		return expenses;
	}

	// List - OtherCommitment
	public List<OtherCommitment> commitments() {
		List<OtherCommitment> commitments = new ArrayList<>();
		for(int i=0; i < 4; i++) {
			OtherCommitment commitment = new OtherCommitment();
			commitment.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			commitment.setCategory(OtherCommitmentCategoryList.CHILD_MAINTENANCE);
			commitment.setDescription(RandomStringUtils.randomAlphabetic(255));
			commitment.setEndDate(LocalDate.now().plusYears(1));
			commitment.setFrequency(FrequencyFullList.YEARLY);
			commitment.setPercentResponsible(percentTypeData.percentOwnership());
			commitment.setStartDate(LocalDate.now().minusYears(2));
			commitment.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			commitments.add(commitment);
		}
		return commitments;
	}

	// ExpenseDetails::TotalSystemCalculatedLivingExpenses
	public TotalSystemCalculatedLivingExpenses systemCalculated() {
		TotalSystemCalculatedLivingExpenses systemCalculated = new TotalSystemCalculatedLivingExpenses();
		systemCalculated.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
		systemCalculated.setFrequency(FrequencyShortList.FORTNIGHTLY);
		return systemCalculated;
	}

	// ExpenseDetails::TotalUserStatedLivingExpenses
	public TotalUserStatedLivingExpenses userStated() {
		TotalUserStatedLivingExpenses userStated = new TotalUserStatedLivingExpenses();
		userStated.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
		userStated.setFrequency(FrequencyShortList.FORTNIGHTLY);
		return userStated;
	}

	// List - FinancialProvider
	public List<FinancialProvider> provider() {
		List<FinancialProvider> providers = new ArrayList<>();
		for(int i=0; i < 4; i++) {
			FinancialProvider provider = new FinancialProvider();
			provider.setXParty(RandomStringUtils.randomAlphabetic(25));
			providers.add(provider);
		}
		return providers;
	}
}
