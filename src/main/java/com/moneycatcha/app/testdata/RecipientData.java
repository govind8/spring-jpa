package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Recipient;
import com.moneycatcha.app.entity.Software;

import lombok.Data;

@Data
public class RecipientData {

	public List<Recipient> recipient() {
		
		List<Recipient> recipients = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Recipient recipient = new Recipient();
			recipient.setDescription(RandomStringUtils.randomAlphabetic(15));
			recipient.setLixiCode(RandomStringUtils.randomAlphanumeric(5));
			recipient.setRoutingCode(RandomStringUtils.randomAlphanumeric(5));
			recipient.setSoftware(software());
			recipients.add(recipient);
		}
		return recipients;
	}
	
	public Software software() {

		Software software = new Software();
		software.setDescription(RandomStringUtils.randomAlphabetic(15));
		software.setEnvironment(RandomStringUtils.randomAlphabetic(15));
		software.setLixiCode(RandomStringUtils.randomAlphanumeric(5));
		software.setName(RandomStringUtils.randomAlphanumeric(5));
		software.setTechnicalEmail(RandomStringUtils.randomAlphanumeric(5));
		software.setVersion(RandomStringUtils.randomAlphanumeric(5));

		return	software;
	}
}
