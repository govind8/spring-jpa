package com.moneycatcha.app.testdata;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 * Convert date to XMLCalendar Date
 */

public class XmlGregorianCalendarData {
	
	
	/**
	 * convert date to XMLGreogorianCalendar
	 * @return
	 */
	public XMLGregorianCalendar localDateToXML() {
	
		try {
			LocalDate date = LocalDate.now();
			XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date.toString());
		    return xmlDate;
		}
		catch (DatatypeConfigurationException e) {
		    throw new RuntimeException(e);
		}
	}	
	
	/**
	 * convert LocalDateTime to XMLGreogorianCalendar
	 * @return
	 */
	public XMLGregorianCalendar localDateTimeToXML() {
	
		try {
			LocalDateTime dateTime = LocalDateTime.now();
			XMLGregorianCalendar xmlDateTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(dateTime.toString());
		    return xmlDateTime;
		}
		catch (DatatypeConfigurationException e) {
		    throw new RuntimeException(e);
		}
	}	
	

}

