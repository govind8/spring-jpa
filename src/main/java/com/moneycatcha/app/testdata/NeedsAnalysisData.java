package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.entity.CoApplicantInterview;
import com.moneycatcha.app.entity.FutureCircumstances;
import com.moneycatcha.app.entity.Interview;
import com.moneycatcha.app.entity.NeedsAnalysis;
import com.moneycatcha.app.entity.RefinancingAndConsolidation;
import com.moneycatcha.app.entity.Interview.Attendee;
import com.moneycatcha.app.entity.NeedsAnalysis.BenefitToApplicants;
import com.moneycatcha.app.entity.NeedsAnalysis.PurposeOfApplication;
import com.moneycatcha.app.entity.RefinancingAndConsolidation.RefinancingReason;
import com.moneycatcha.app.model.BenefitEnquiriesList;
import com.moneycatcha.app.model.CoApplicantBenefitList;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.PurposeOfApplicationList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class NeedsAnalysisData {

	DurationTypeData durationData = new DurationTypeData();
	
	ResponsibleLendingData responsibleLendingData = new ResponsibleLendingData();
	
	PreferencesData preferencesData = new PreferencesData();
	
	RetirementDetailsData retirementDetailsData = new RetirementDetailsData();
	
	// RandomStringUtils.randomAlphanumeric(15)
	// RandomStringUtils.randomAlphabetic(15)
	// new BigDecimal(RandomStringUtils.randomNumeric(2, 5))
	// new BigInteger(RandomStringUtils.randomNumeric(2)
	// LocalDate.now()
	
	public NeedsAnalysis needsAnalysis() {
		
		NeedsAnalysis needsAnalysis = new NeedsAnalysis();
		needsAnalysis.setBenefitToApplicants(benefitToApplicants()); //object
		needsAnalysis.setCoApplicantInterview(coApplicantInterview()); //object
		needsAnalysis.setFutureCircumstances(futureCircumstances()); //object
		needsAnalysis.setInterview(interview()); //object
		needsAnalysis.setLoanAmountSought(new BigDecimal(RandomStringUtils.randomNumeric(2, 5))); //object
		needsAnalysis.setLoanTermSought(durationData.duration()); // object
		needsAnalysis.setLoanTermSoughtDescription(RandomStringUtils.randomAlphabetic(15)); //object
		needsAnalysis.setPreferences(preferencesData.preferences()); //object
		needsAnalysis.setPrimaryApplicationPurpose(PrimaryPurposeLoanPurposeList.BUSINESS);
		needsAnalysis.setProductSelection(RandomStringUtils.randomAlphabetic(15));
		needsAnalysis.setPurposeOfApplication(purposeOfApplication());  //object
		needsAnalysis.setPurposeSummary(RandomStringUtils.randomAlphabetic(15));
		needsAnalysis.setRefinancingAndConsolidation(refinancingAndConsolidation());  //object
		needsAnalysis.setRetirementDetails(retirementDetailsData.retirementDetails());  //object
		needsAnalysis.setUniqueID(RandomStringUtils.randomAlphanumeric(15));

		return needsAnalysis;
	}
	
	// NeedsAnalysis -> BenefitToApplicants
	public BenefitToApplicants benefitToApplicants() {
		BenefitToApplicants benefits = new BenefitToApplicants();
		benefits.setAllApplicantsBenefit(YesNoList.YES);
		benefits.setBenefitEnquiries(RandomStringUtils.randomAlphabetic(15));
		return benefits;
	}
	
	// NeedsAnalysis -> CoApplicantInterview
	public CoApplicantInterview coApplicantInterview() {
		CoApplicantInterview interview = new CoApplicantInterview();
		interview.setApplicant(applicants());
		return interview;
	}
	
	//  NeedsAnalysis -> List - Purpose Of Application
	public List<PurposeOfApplication> purposeOfApplication() {
		List<PurposeOfApplication> poa = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			PurposeOfApplication purpose = new PurposeOfApplication();
			purpose.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			purpose.setDescription(RandomStringUtils.randomAlphabetic(15));
			purpose.setPurpose(PurposeOfApplicationList.COMMERCIAL_DEVELOPMENT);
			poa.add(purpose);
			
		}
		
		return poa;
	}
	
	//  NeedsAnalysis -> FutureCircumstances
	public FutureCircumstances futureCircumstances() {
		FutureCircumstances future = new FutureCircumstances();
		future.setResponsibleLending(responsibleLendingData.responsibleLendingType()); //object
		future.setApplicant(applicants()); //object
		return future;
	}	
	
	//  NeedsAnalysis -> Interview
	public Interview interview() {
		Interview interview = new Interview();
		interview.setAllApplicantsPresent(YesNoList.NO);
		interview.setAllApplicantsUnderstandEnglish(YesNoList.YES);
		interview.setAttendee(attendee()); //object
		interview.setDate(LocalDate.now().plusDays(10));
		interview.setInterpreterRecommended(YesNoList.YES);
		interview.setIsFaceToFace(YesNoList.YES);
		interview.setIsOnlyPersonPresent(YesNoList.YES);
		interview.setXLocation(RandomStringUtils.randomAlphabetic(15));

		return interview;
	}
	
	// List - Applicant - common function
	public List<Applicant> applicants() {
		List<Applicant> applicants = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			Applicant applicant = new Applicant();
			applicant.setBenefitEnquiries(BenefitEnquiriesList.PROPORTION_IN_ASSETS_PURCHASED);
			applicant.setBenefitEnquiriesDescription(RandomStringUtils.randomAlphabetic(15));
			applicant.setCoApplicantBenefit(CoApplicantBenefitList.NO_SUBSTANTIAL_BENEFIT);
			applicant.setCoApplicantReason(RandomStringUtils.randomAlphabetic(15));
			applicant.setName(RandomStringUtils.randomAlphabetic(15));
			applicant.setNumber(RandomStringUtils.randomAlphanumeric(15));
			applicant.setSuspectDifficultyUnderstandingObligations(YesNoList.NO);
			applicant.setSuspectExperiencingFinancialAbuse(YesNoList.NO);
			applicant.setSuspectUnderPressure(YesNoList.NO);
			applicant.setSuspectUnfairConduct(YesNoList.NO);
			applicant.setUnderstandsCoApplicantObligations(YesNoList.YES);
			applicant.setUnderstandsCoApplicantObligations(YesNoList.YES);
			applicant.setXApplicant(RandomStringUtils.randomAlphabetic(15));

			applicants.add(applicant);
			
		}
		
		return applicants;
	}
	
	//  NeedsAnalysis -> Interview -> List Attendee
	public List<Attendee> attendee() {
		List<Attendee> attendees = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			Attendee attendee = new Attendee();
			attendee.setXParty(RandomStringUtils.randomAlphabetic(15));
			attendees.add(attendee);
		}
		
		return attendees;
	}
	
	 // NeedsAnalysis -> RefinancingAndConsolidation
	public RefinancingAndConsolidation refinancingAndConsolidation() {
		 
		RefinancingReason refinancingReason = new RefinancingReason(); 
		refinancingReason.setCloseToEndOfCurrentLoanTerm(YesNoList.YES);
		refinancingReason.setCloseToEndOfCurrentLoanTermDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setDebtConsolidation(YesNoList.YES);
		refinancingReason.setDebtConsolidationDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setDissatisfactionWithCurrentLender(YesNoList.YES);
		refinancingReason.setDissatisfactionWithCurrentLenderDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setGreaterFlexibility(YesNoList.YES);
		refinancingReason.setGreaterFlexibilityDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setIncreaseTotalLoanAmount(YesNoList.YES);
		refinancingReason.setIncreaseTotalLoanAmountDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setLowerInterestRate(YesNoList.YES);
		refinancingReason.setLowerInterestRateDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setOther(YesNoList.YES);
		refinancingReason.setOtherDetails(RandomStringUtils.randomAlphabetic(15));
		refinancingReason.setReducedRepayments(YesNoList.YES);
		refinancingReason.setReducedRepaymentsDetails(RandomStringUtils.randomAlphabetic(15));

		RefinancingAndConsolidation refinancing = new RefinancingAndConsolidation();
		refinancing.setDetailsOfCreditCardPlan(RandomStringUtils.randomAlphabetic(15));
		refinancing.setExplainedIncreaseInterestRisk(YesNoList.YES);
		refinancing.setExplainedIncreaseLoanTermRisk(YesNoList.YES);
		refinancing.setPlanToCloseOrReduceCreditCard(YesNoList.YES);
		refinancing.setRefinancingReason(refinancingReason);
		return refinancing;
		 
	}
	
	
}
