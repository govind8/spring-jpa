package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.ConstructionDetails;
import com.moneycatcha.app.entity.FutureRentalIncome;
import com.moneycatcha.app.entity.PropertyFeatures;
import com.moneycatcha.app.entity.PropertyPart;
import com.moneycatcha.app.entity.RealEstateAsset;
import com.moneycatcha.app.entity.RentalIncome;
import com.moneycatcha.app.entity.Title;
import com.moneycatcha.app.entity.ConstructionDetails.ConstructionStage;
import com.moneycatcha.app.entity.ConstructionDetails.OwnerBuilderExperience;
import com.moneycatcha.app.entity.RealEstateAsset.Commercial;
import com.moneycatcha.app.entity.RealEstateAsset.HeritageListing;
import com.moneycatcha.app.entity.RealEstateAsset.Industrial;
import com.moneycatcha.app.entity.RealEstateAsset.PropertyExpense;
import com.moneycatcha.app.entity.RealEstateAsset.Representation;
import com.moneycatcha.app.entity.RealEstateAsset.Residential;
import com.moneycatcha.app.entity.RealEstateAsset.RestrictionOnUseOfLand;
import com.moneycatcha.app.entity.RealEstateAsset.Rural;
import com.moneycatcha.app.entity.RealEstateAsset.VisitContact;
import com.moneycatcha.app.entity.RealEstateAsset.Zoning;
import com.moneycatcha.app.entity.Title.RegisteredProprietor;
import com.moneycatcha.app.model.BuilderTypeList;
import com.moneycatcha.app.model.CommercialTypeList;
import com.moneycatcha.app.model.ConstructionTypeList;
import com.moneycatcha.app.model.DataSourceList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.HoldingList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.IndustrialTypeList;
import com.moneycatcha.app.model.LandAreaUnitsList;
import com.moneycatcha.app.model.LandValueEstimateBasisList;
import com.moneycatcha.app.model.OccupancyList;
import com.moneycatcha.app.model.PlanTypeList;
import com.moneycatcha.app.model.PoolTypeList;
import com.moneycatcha.app.model.PrimaryPurposeRealEstateAssetList;
import com.moneycatcha.app.model.PrimaryUsageList;
import com.moneycatcha.app.model.PropertyPartTypeList;
import com.moneycatcha.app.model.RealEstateAssetStatusList;
import com.moneycatcha.app.model.RentalIncomeBasisList;
import com.moneycatcha.app.model.ResidentialTypeList;
import com.moneycatcha.app.model.RuralUsageList;
import com.moneycatcha.app.model.TenureTypeList;
import com.moneycatcha.app.model.TitleSystemList;
import com.moneycatcha.app.model.ValuationProgramList;
import com.moneycatcha.app.model.VisitContactTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class RealEstateAssetData {

	PercentOwnedTypeData percentOwnedData = new PercentOwnedTypeData();
	
	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	BusinessData businessData = new BusinessData();
	
	DepositAccountDetailsData depositData = new DepositAccountDetailsData();

	DurationTypeData durationData = new DurationTypeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	LoanDetailsData loanDetailsData = new LoanDetailsData();
	
	FinancialAccountTypeData financialAccountTypeData = new FinancialAccountTypeData();
	
	NonRealEstateAssetData nonRealEstateAssetData = new NonRealEstateAssetData();
	
	// List <RealEstateAsset>
	public List<RealEstateAsset> realEstateAsset() {
		
		List<RealEstateAsset> reas = new ArrayList<>();
		for( int i=0; i < 3; i++) {
			
			RealEstateAsset rea = new RealEstateAsset();
			
			rea.setApprovalInPrinciple(YesNoList.YES);
			rea.setCommercial(commercial()); //object
			rea.setConstruction(YesNoList.NO);
			rea.setConstructionDetails(details()); //object
			rea.setContractDetails(nonRealEstateAssetData.contractDetails());  //object
			rea.setContractOfSale(YesNoList.NO);
			rea.setDataSource(DataSourceList.GNAF);
			rea.setEncumbered(YesNoList.YES);
			rea.setEncumbrance(nonRealEstateAssetData.encumbrance());  //object
			rea.setFundsDisbursement(loanDetailsData.fundsDisbursementType());  //object
			rea.setFutureRentalIncome(futureRentalIncomes());  //object
			rea.setHeritageListing(heritageListing());  //object
			rea.setHolding(HoldingList.SOLE);
			rea.setIndustrial(industrial());  //object
			rea.setInnerCity(YesNoList.NO);
			rea.setInsurance(insurance());  //object
			rea.setInvestmentPropertyLetter(YesNoList.NO);
			rea.setLegalRepresentation(YesNoList.NO);
			rea.setMultipleDwellings(YesNoList.YES);
			rea.setNrasConsortium(RandomStringUtils.randomAlphabetic(15));
			rea.setNrasProperty(YesNoList.NO);
			rea.setOccupancy(OccupancyList.NON_OWNER);
			rea.setOnMarketTransaction(YesNoList.YES);
			rea.setPercentOwned(percentOwnedData.percentOwnership());  //object
			rea.setPrimaryLandUse(RandomStringUtils.randomAlphabetic(15));
			rea.setPrimaryPurpose(PrimaryPurposeRealEstateAssetList.BUSINESS);
			rea.setPrimarySecurity(YesNoList.NO);
			rea.setPrimaryUsage(PrimaryUsageList.COMMERCIAL);
			rea.setPropertyExpense(propertyExpense()); //object
			rea.setPropertyFeatures(propertyFeatures());  //object
			rea.setPropertyID(RandomStringUtils.randomAlphabetic(15));
			rea.setPurchaseUnderAOCWrap(YesNoList.NO);
			rea.setRentalIncome(rentalIncomes());  //object
			rea.setRepresentation(representation());  //object
			rea.setResidential(residential());  //object
			rea.setRural(rural());  //object
			rea.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			rea.setStatus(RealEstateAssetStatusList.ESTABLISHED);
			rea.setTitle(title());  //object
			rea.setToBeSold(YesNoList.YES);
			rea.setToBeUsedAsSecurity(YesNoList.YES);
			rea.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			rea.setValuationProgram(ValuationProgramList.AVM);
			rea.setValuationRequired(YesNoList.NO);
			rea.setVerified(YesNoList.NO);			
			rea.setVisitContact(visitContact());  //object
			rea.setXAddress(RandomStringUtils.randomAlphabetic(15));
			rea.setXPropertyAgent(RandomStringUtils.randomAlphabetic(15));
			reas.add(rea);
		}
		return reas;
			
	}
	
	//Commercial
	public Commercial commercial() {
		Commercial commercial = new Commercial();
		commercial.setType(CommercialTypeList.BLOCK_OF_UNITS_OR_FLATS);
		return commercial;
	}
	
	//Commercial
	public ConstructionDetails details() {
		ConstructionDetails details = new ConstructionDetails();
		details.setBuilderType(BuilderTypeList.LICENSED_BUILDER);
		details.setBuildPriceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		details.setConstructionInsuranceHeld(YesNoList.NO);
		details.setConstructionStage(constructionStage()); //object
		details.setCopyBuildersLicenceHeld(YesNoList.NO);
		details.setCopyHomeOwnersWarrantyCertificateHeld(YesNoList.NO);
		details.setCouncilApprovalHeld(YesNoList.YES);
		details.setDetailedCostingsHeld(YesNoList.YES);
		details.setEstimatedCompletionDate(LocalDate.now().plusYears(1));
		details.setFixedPriceContract(YesNoList.YES);
		details.setInitialAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		details.setLandValue(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		details.setLandValueBasis(LandValueEstimateBasisList.ESTIMATED_MARKET_VALUE);
		details.setOwnerBuilderExperience(ownerBuilderExperience()); //object
		details.setSatisfactoryTOCValuationHeld(YesNoList.YES);
		details.setSignedContract(YesNoList.YES);
		details.setTenPercentOfLendingAsCash(YesNoList.YES);
		details.setTotalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		details.setType(ConstructionTypeList.FULL_CONSTRUCTION);
		details.setXBuilder(RandomStringUtils.randomAlphabetic(15));
		return details;
	}
	
	// ConstructionStage
	public List<ConstructionStage> constructionStage() {
		List<ConstructionStage> stages = new ArrayList<>();
		for (int i=0; i <3; i++) {
			ConstructionStage stage = new ConstructionStage();
			stage.setStageAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));	
			stage.setStageDescription(RandomStringUtils.randomAlphabetic(15));
			stage.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			stages.add(new ConstructionStage());
		}
		return stages;
	}
	
	//OwnerBuilderExperience
	public OwnerBuilderExperience ownerBuilderExperience() {
		OwnerBuilderExperience experience = new OwnerBuilderExperience();
		experience.setCompletedSimilarProjectInLast2Years(YesNoList.NO);
		experience.setMinimalExperience(YesNoList.NO);
		experience.setOtherExperience(RandomStringUtils.randomAlphabetic(15));
		experience.setWorksInBuildingIndustry(YesNoList.NO);
		return experience;
	}
	
	// FutureRentalIncome
	public List<FutureRentalIncome> futureRentalIncomes() {
		List<FutureRentalIncome> incomes = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			FutureRentalIncome rental = new FutureRentalIncome();
			rental.setGrossRentalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			rental.setGrossRentalFrequency(FrequencyFullList.DAILY);
			rental.setNetRentalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			rental.setNetRentalFrequency(FrequencyFullList.DAILY);
			rental.setPropertyPart(propertyPart()); //object
			rental.setStartDate(LocalDate.now());
			incomes.add(rental);
		}

		return incomes;
	}
	
	//HeritageListing
	public HeritageListing heritageListing() {
		HeritageListing heritageListing = new HeritageListing();
		heritageListing.setHeritageListed(YesNoList.NO);
		return heritageListing;
	}
	
	//Industrial
	public Industrial industrial() {
		Industrial industrial = new Industrial();
		industrial.setType(IndustrialTypeList.LIGHT_INDUSTRIAL);
		return industrial;
	}
	
	
	// List Insurance
	public List<RealEstateAsset.Insurance> insurance() {
		List<RealEstateAsset.Insurance> insurances = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RealEstateAsset.Insurance insurance = new RealEstateAsset.Insurance();
			insurance.setXInsurance(RandomStringUtils.randomAlphanumeric(10));
			insurances.add(insurance);
		}
		return insurances;
	}
	
	//Representation
	public PropertyExpense propertyExpense() {
		PropertyExpense propertyExpense = new PropertyExpense();
		propertyExpense.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		propertyExpense.setFrequency(FrequencyFullList.DAILY);
		return propertyExpense;
	}
	
	// List PropertyFeatures
	public List<PropertyFeatures> propertyFeatures() {
		List<PropertyFeatures> features = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			PropertyFeatures feature = new PropertyFeatures();
			feature.setAllPropertyFeatures(RandomStringUtils.randomAlphanumeric(15));	//setXInsurance(RandomStringUtils.randomAlphanumeric(10));
			feature.setFloorArea(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			feature.setLandArea(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			feature.setLandAreaUnits(LandAreaUnitsList.ACRES);
			feature.setNumberOfBathrooms(new BigInteger(RandomStringUtils.randomNumeric(2)));
			feature.setNumberOfBedrooms(new BigInteger(RandomStringUtils.randomNumeric(2)));
			feature.setNumberOfCarSpaces(new BigInteger(RandomStringUtils.randomNumeric(2)));
			feature.setPoolType(PoolTypeList.ABOVE_GROUND);
			feature.setPropertyID(RandomStringUtils.randomAlphanumeric(15));
			feature.setPropertyImage(RandomStringUtils.randomAlphabetic(15));
			feature.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			feature.setWallMaterial(RandomStringUtils.randomAlphabetic(15));
			features.add(feature);
		}
		return features;
	}
	
	//RentalIncome
	public List<RentalIncome> rentalIncomes() {
		
		List<RentalIncome> rentals = new ArrayList<>();
		for (int i=0; i < 2; i++) {
			RentalIncome income = new RentalIncome();
			income.setBasis(RentalIncomeBasisList.APPLICANT_ESTIMATE);
			income.setConfidencePercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			income.setEvidenceOfTenancy(YesNoList.NO);
			income.setFrequency(FrequencyFullList.DAILY);
			income.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.ENDS);
			income.setNetRentalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			income.setNetRentalAmountFrequency(FrequencyShortList.FORTNIGHTLY);
			income.setPropertyPart(propertyPart()); //object
			income.setRentalAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
			income.setValuedDate(LocalDate.now());
			rentals.add(income);
		}
		return rentals;
	}
	
	// List PropertyPart
	public List<PropertyPart> propertyPart() {
		List<PropertyPart> part = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			PropertyPart ppart = new PropertyPart();
			ppart.setType(PropertyPartTypeList.ROOM_WITHIN_MAIN_RESIDENCE);
			part.add(ppart);
		}
		return part;
	}
	
	//Representation
	public Representation representation() {
		Representation representation = new Representation();
		representation.setXConveyancer(RandomStringUtils.randomAlphabetic(15));
		representation.setXVendorConveyancer(RandomStringUtils.randomAlphabetic(15));
		return representation;
	}
		
	//Residential
	public Residential residential() {
		Residential residential = new Residential();
		residential.setType(ResidentialTypeList.AGED_CARE_UNIT);
		residential.setWillOwn25PercentOfComplex(YesNoList.YES);
		residential.setWillOwn3UnitsInComplex(YesNoList.YES);
		return residential;
	}
	
	// RestrictionOnUseOfLand
	public RestrictionOnUseOfLand restrictionOnUseOfLand() {
		RestrictionOnUseOfLand useofland = new RestrictionOnUseOfLand();
		useofland.setDescription(RandomStringUtils.randomAlphabetic(15));
		useofland.setRegistrationNumber(RandomStringUtils.randomAlphabetic(15));
		useofland.setRestrictionExists(YesNoList.NO);
		return useofland;
	}
	
	// Rural
	public Rural rural() {
		Rural rural = new Rural();
		rural.setNumberOfBuildings(new BigInteger(RandomStringUtils.randomNumeric(2)));
		rural.setType(RandomStringUtils.randomAlphabetic(15));
		rural.setUsage(RuralUsageList.INCOME_PRODUCING);
		return rural;
	}
	
	// Title
	public List<Title> title() {
		
		List<Title> titles = new ArrayList<Title>();
		for (int i=0; i < 3; i++) {
			Title title = new Title();
			title.setAutoConsol(RandomStringUtils.randomAlphabetic(10));
			title.setBlock(RandomStringUtils.randomAlphabetic(10));
			title.setCounty(RandomStringUtils.randomAlphabetic(10));
			title.setDistrict(RandomStringUtils.randomAlphabetic(10));
			title.setDuplicateTitleIssued(YesNoList.NO);
			title.setExtent(RandomStringUtils.randomAlphabetic(10));
			title.setFolio(RandomStringUtils.randomAlphabetic(10));
			title.setLocation(RandomStringUtils.randomAlphabetic(10));
			title.setLot(RandomStringUtils.randomAlphabetic(10));
			title.setLotPlan(RandomStringUtils.randomAlphabetic(10));
			title.setOtherDetails(RandomStringUtils.randomAlphabetic(10));
			title.setParcel(RandomStringUtils.randomAlphabetic(10));
			title.setParish(RandomStringUtils.randomAlphabetic(10));
			title.setPlan(RandomStringUtils.randomAlphabetic(10));
			title.setPlanType(PlanTypeList.BUILDING_UNIT_PLAN);
			title.setRealPropertyDescriptor(RandomStringUtils.randomAlphabetic(10));
			title.setRegister(RandomStringUtils.randomAlphabetic(10));
			title.setRegisteredProprietor(registeredProprietor());
			title.setSection(RandomStringUtils.randomAlphabetic(10));
			title.setTenureType(TenureTypeList.CROWN_LEASE);
			title.setTitleReference(RandomStringUtils.randomAlphabetic(10));
			title.setTitleSystem(TitleSystemList.COMPANY);
			title.setUnit(RandomStringUtils.randomAlphabetic(10));
			title.setUnregisteredLand(YesNoList.NO);
			title.setUnregisteredPlan(YesNoList.YES);
			title.setVolume(RandomStringUtils.randomAlphabetic(10));
			titles.add(title);
		}
		
		return titles;
	}
	
	
	// List RegisteredProprietor
	public List<RegisteredProprietor> registeredProprietor() {
		List<RegisteredProprietor> registeredProprietors = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RegisteredProprietor registeredProprietor = new RegisteredProprietor();
			registeredProprietor.setChangeNameFormRequired(YesNoList.NO);//setXInsurance(RandomStringUtils.randomAlphanumeric(10));
			registeredProprietor.setName(RandomStringUtils.randomAlphanumeric(10));
			registeredProprietor.setXRegisteredProprietor(RandomStringUtils.randomAlphanumeric(10));
			registeredProprietors.add(registeredProprietor);
		}
		return registeredProprietors;
	}
	
	// VisitContact
	public VisitContact visitContact() {
		VisitContact visit = new VisitContact();
		visit.setBoatAccessOnly(YesNoList.NO);
		visit.setDetails(RandomStringUtils.randomAlphanumeric(10));
		visit.setFourWDAccessOnly(YesNoList.YES);
		visit.setType(VisitContactTypeList.TENANT);
		visit.setXContactParty(RandomStringUtils.randomAlphanumeric(10));
		return visit;
	}
	
	//Zoning
	public Zoning zoning() {
		Zoning zoning = new Zoning();
		zoning.setType(RandomStringUtils.randomAlphanumeric(10));
		return zoning;
	}
}
