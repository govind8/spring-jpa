package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.IncomePrevious;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class IncomePreviousData {

	
	AddBackData addbackData = new AddBackData();

	public IncomePrevious incomePrevious() {
		
		IncomePrevious incomePrevious = new IncomePrevious();
		incomePrevious.setCompanyProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setCompanyProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setEndDate(LocalDate.now());
		incomePrevious.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomePrevious.setStartDate(LocalDate.now().minusYears(20));
		incomePrevious.setTaxOfficeAssessments(YesNoList.YES);
		incomePrevious.setXAccountant(RandomStringUtils.randomNumeric(15));
		incomePrevious.setAddback(addbackData.addBack());
		return incomePrevious;
	}
}
