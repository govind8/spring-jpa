package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.BeneficialOwner;

import lombok.Data;

@Data
public class BeneficialOwnerData {

	public List<BeneficialOwner> setBeneficalOwners() {
		List<BeneficialOwner> owners = new ArrayList<>();
		for (int i=0; i<2; i++) {
			BeneficialOwner owner = new BeneficialOwner();
			owner.setXBeneficialOwner(RandomStringUtils.randomAlphabetic(10));
			owners.add(owner);
		}
		return owners;
	}
}
