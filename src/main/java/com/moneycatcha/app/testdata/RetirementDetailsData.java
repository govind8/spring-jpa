package com.moneycatcha.app.testdata;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.RepaymentOptions;
import com.moneycatcha.app.entity.RetirementDetails;
import com.moneycatcha.app.entity.RepaymentOptions.RecurringIncomeFromSuperannuationDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SaleOfAssetsDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SavingsDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class RetirementDetailsData {
	
	ApplicantData applicantData = new ApplicantData();

	// NeedsAnalysis -> RetirementDetails
	public RetirementDetails retirementDetails() {
		RetirementDetails retirement = new RetirementDetails();
		retirement.setApplicant(applicantData.applicants());  // object
		retirement.setRepaymentOptions(repaymentOptions());  // object
		
		return retirement;
	}
	
	// NeedsAnalysis -> RetirementDetails -> RepaymentOptions
	public RepaymentOptions repaymentOptions() {
		RepaymentOptions repayment = new RepaymentOptions();
		repayment.setCoApplicantIncome(YesNoList.YES);
		repayment.setDescription(RandomStringUtils.randomAlphabetic(15));
		repayment.setDownsizingHome(YesNoList.YES);
		repayment.setIncomeFromOtherInvestments(YesNoList.YES);
		repayment.setOther(YesNoList.YES);
		repayment.setRecurringIncomeFromSuperannuation(YesNoList.YES);
		repayment.setRepaymentOfLoanPriorToRetirement(YesNoList.YES);
		repayment.setSaleOfAssets(YesNoList.NO);
		repayment.setSavings(YesNoList.YES);
		repayment.setSuperannuationLumpSumFollowingRetirement(YesNoList.NO);
		repayment.setWorkPastStatutoryRetirementAge(YesNoList.YES);
		repayment.setRecurringIncomeFromSuperannuationDetails(recurringIncome()); // object
		repayment.setSaleOfAssetsDetails(saleOfAssetsDetails()); // object
		repayment.setSavingsDetails(savingsDetails()); // object
		repayment.setSuperannuationLumpSumFollowingRetirementDetails(superannuationLumpSum()); // object
		
		return repayment;
		
	}
	
	// NeedsAnalysis -> RetirementDetails -> RepaymentOptions -> RecurringIncomeFromSuperannuationDetails
	public RecurringIncomeFromSuperannuationDetails recurringIncome() {
		RecurringIncomeFromSuperannuationDetails recurringIncome = new RecurringIncomeFromSuperannuationDetails();
		recurringIncome.setFinancialPlannerDocumentation(YesNoList.NO);
		recurringIncome.setFinancialPlannerDocumentationDescription(RandomStringUtils.randomAlphabetic(15));
		recurringIncome.setSuperStatementShowingProjectedRecurringIncome(YesNoList.NO);
		recurringIncome.setSuperStatementShowingProjectedRecurringIncomeDescription(RandomStringUtils.randomAlphabetic(15));
		return recurringIncome;
	}
	
	// NeedsAnalysis -> RetirementDetails -> RepaymentOptions -> SaleOfAssetsDetails
	public SaleOfAssetsDetails saleOfAssetsDetails() {
		SaleOfAssetsDetails assets = new SaleOfAssetsDetails();
		assets.setLenderHeldSecurity(YesNoList.NO);
		assets.setLenderHeldSecurityDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setOtherAssets(YesNoList.NO);
		assets.setOtherAssetsDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setOtherLenderHeldProperty(YesNoList.NO);
		assets.setOtherLenderHeldPropertyDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setOtherLenderHeldSecurity(YesNoList.NO);
		assets.setOtherLenderHeldSecurityDescription(RandomStringUtils.randomAlphabetic(15));
		assets.setShares(YesNoList.NO);
		assets.setSharesDescription(RandomStringUtils.randomAlphabetic(15));
		return assets;
	}
	
	// NeedsAnalysis -> RetirementDetails -> RepaymentOptions -> SavingsDetails	
	public SavingsDetails savingsDetails() {
		SavingsDetails savings = new SavingsDetails();
		savings.setLenderHeldSavingsAccount(YesNoList.NO);
		savings.setLenderHeldSavingsAccountDescription(RandomStringUtils.randomAlphabetic(15));
		savings.setOtherLenderHeldSavingsAccount(YesNoList.NO);	
		savings.setOtherLenderHeldSavingsAccountDescription(RandomStringUtils.randomAlphabetic(15));
		return savings;
	}
	
	// NeedsAnalysis -> RetirementDetails -> RepaymentOptions -> SuperannuationLumpSumFollowingRetirementDetails
	public SuperannuationLumpSumFollowingRetirementDetails superannuationLumpSum() {
		SuperannuationLumpSumFollowingRetirementDetails superannuation = new SuperannuationLumpSumFollowingRetirementDetails();
		superannuation.setFpDocumentation(YesNoList.NO);
		superannuation.setFpDocumentationDescription(RandomStringUtils.randomAlphabetic(15));
		superannuation.setSsspSuperannuationAmount(YesNoList.NO);
		superannuation.setSsspSuperannuationAmountDescription(RandomStringUtils.randomAlphabetic(15));
		return superannuation;
	}
}
