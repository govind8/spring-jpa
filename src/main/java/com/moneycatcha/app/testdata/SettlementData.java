package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Settlement;

import lombok.Data;

@Data
public class SettlementData {
	
	// Settlement
	public List<Settlement> settlement() {
		
		List<Settlement> settlements = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Settlement settlement = new Settlement();
			settlement.setPayoutFigure(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			settlement.setSecurity(security()); // object
			settlement.setSettlementBookingDateTime(LocalDateTime.now().plusYears(1));
			settlement.setSettlementReferenceNumber(RandomStringUtils.randomAlphanumeric(15));
			settlement.setXSettlementBookingAddress(RandomStringUtils.randomAlphanumeric(15));
			settlement.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));

			settlements.add(settlement);
		}
	
		return settlements;
	}
	
	// Security
	public List<Settlement.Security> security() {
		List<Settlement.Security> securities = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Settlement.Security security = new Settlement.Security();
			security.setXSecurity(RandomStringUtils.randomAlphanumeric(15));
			securities.add(security);
		}
		return securities;
	}
	
}
