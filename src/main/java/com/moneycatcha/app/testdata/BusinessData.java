package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.entity.Business.ConcentrationRisk;
import com.moneycatcha.app.entity.Business.Diversification;
import com.moneycatcha.app.entity.Business.ImportExport;
import com.moneycatcha.app.entity.Business.PropertyInvestment;
import com.moneycatcha.app.model.YesNoIntentList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class BusinessData {

	FinancialAnalysisData financialAnalyisData = new FinancialAnalysisData();
	
	
	// Application - Company Applicant - Business Data
	public Business business() {

		Business business = new Business();
		business.setCustomIndustryCode(RandomStringUtils.randomAlphanumeric(5));
		business.setBusinessName("Test");
		business.setGicsCode(RandomStringUtils.randomAlphanumeric(5));
		business.setIndustry(RandomStringUtils.randomAlphabetic(15));
		business.setIndustryCode(RandomStringUtils.randomAlphanumeric(5));
		business.setIsFranchise(YesNoList.YES);
		business.setIsFranchiseIntent(YesNoIntentList.FUTURE_INTENT);
		business.setNumberOfEmployees(new BigInteger(RandomStringUtils.randomNumeric(4)));
		business.setNumberOfLocations(new BigInteger(RandomStringUtils.randomNumeric(4)));
		business.setOwnPremises(YesNoList.YES);
		business.setStartDate(LocalDate.now());
		business.setConcentrationRisk(concentrationRisk());
		business.setDiversification(diversification());
		business.setImportExport(importExport());
		business.setPropertyInvestment(propertyInvestment());
		business.setUniqueID(RandomStringUtils.randomAlphanumeric(5));
		business.setFinancialAnalysis(financialAnalyisData.financialAnalysis()); //object - financial analysis

		return business;
	}
	
	// ConcentrationRisk
	public ConcentrationRisk concentrationRisk() {
		ConcentrationRisk risk = new ConcentrationRisk();
		risk.setConcentrationRiskDetails(RandomStringUtils.randomAlphabetic(15));
		risk.setCustomerOrSupplierConcentration(YesNoList.NO);
		return risk;
	}
	
	// Diversification
	public Diversification diversification() {
		Diversification diversification = new Diversification();
		diversification.setDetails(RandomStringUtils.randomAlphabetic(15));
		diversification.setDiversifiedDate(LocalDate.now().minusYears(5));
		diversification.setIsDiversified(YesNoList.NO);
		return diversification;
	}
	
	// ImportExport
	public ImportExport importExport() {
		ImportExport ie = new ImportExport();
		ie.setDetails("import-export");
		ie.setIsInvolved(YesNoIntentList.FUTURE_INTENT);
		return ie;
	}
	
	// PropertyInvestment
	public PropertyInvestment propertyInvestment() {
		PropertyInvestment pi = new PropertyInvestment();
		pi.setDetails("property-investment");
		pi.setIsInvolved(YesNoIntentList.YES);
		return pi;
	}
	
	// Trust Applicant - Business
	public Business taBusiness() {
		Business business = new Business();
		business.setAustralianBIC(RandomStringUtils.randomAlphanumeric(5));
		business.setCustomIndustryCode(RandomStringUtils.randomAlphanumeric(5));
		business.setGicsCode(RandomStringUtils.randomAlphanumeric(5));
		business.setIndustry(RandomStringUtils.randomAlphabetic(15));
		business.setIndustryCode(RandomStringUtils.randomAlphanumeric(5));
		business.setMainBusinessActivity(RandomStringUtils.randomAlphanumeric(5));
		return business;		
	}
	
	// FinancialAnalysis
}
