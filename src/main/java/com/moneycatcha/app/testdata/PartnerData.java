package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Partner;
import com.moneycatcha.app.model.PartnerTypeList;

import lombok.Data;

@Data
public class PartnerData {

	public Partner setData() {
		Partner partner = new Partner();
		partner.setPartnerType(PartnerTypeList.GENERAL_PARTNER);
		partner.setPercentOwned(new BigDecimal("3000000.00"));
		partner.setXPartner("partner-0000001");
		return partner;
	}
	
	public List<Partner> setPartners() {
		List<Partner> partners = new ArrayList<Partner>();
		
		for (int i=0; i < 4; i++) {
			Partner partner = new Partner();
			partner.setPartnerType(PartnerTypeList.GENERAL_PARTNER);
			partner.setPercentOwned(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			partner.setXPartner(RandomStringUtils.randomAlphanumeric(10));
			partners.add(partner);
		}
		return partners;
	}
}
