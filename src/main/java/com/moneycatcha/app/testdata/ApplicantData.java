package com.moneycatcha.app.testdata;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.model.BenefitEnquiriesList;
import com.moneycatcha.app.model.CoApplicantBenefitList;
import com.moneycatcha.app.model.YesNoList;

public class ApplicantData {
	
	// List - Applicant - common function
	public List<Applicant> applicants() {
		List<Applicant> applicants = new ArrayList<>();
		
		for (int i=0; i < 2; i++) {
			Applicant applicant = new Applicant();
			applicant.setBenefitEnquiries(BenefitEnquiriesList.PROPORTION_IN_ASSETS_PURCHASED);
			applicant.setBenefitEnquiriesDescription(RandomStringUtils.randomAlphabetic(15));
			applicant.setCoApplicantBenefit(CoApplicantBenefitList.NO_SUBSTANTIAL_BENEFIT);
			applicant.setCoApplicantReason(RandomStringUtils.randomAlphabetic(15));
			applicant.setName(RandomStringUtils.randomAlphabetic(15));
			applicant.setNumber(RandomStringUtils.randomAlphanumeric(15));
			applicant.setSuspectDifficultyUnderstandingObligations(YesNoList.NO);
			applicant.setSuspectExperiencingFinancialAbuse(YesNoList.NO);
			applicant.setSuspectUnderPressure(YesNoList.NO);
			applicant.setSuspectUnfairConduct(YesNoList.NO);
			applicant.setUnderstandsCoApplicantObligations(YesNoList.YES);
			applicant.setUnderstandsCoApplicantObligations(YesNoList.YES);
			applicant.setXApplicant(RandomStringUtils.randomAlphabetic(15));

			applicants.add(applicant);
			
		}
		
		return applicants;
	}
}
