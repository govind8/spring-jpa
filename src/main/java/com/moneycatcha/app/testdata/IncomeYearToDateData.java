package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.IncomeYearToDate;

import lombok.Data;

@Data
public class IncomeYearToDateData {
	
	AddBackData addbackData = new AddBackData();
	
	public IncomeYearToDate incomeYearToDate() {
			
		IncomeYearToDate incomeYearToDate = new IncomeYearToDate();
		incomeYearToDate.setCompanyProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeYearToDate.setCompanyProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeYearToDate.setProfitAfterTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeYearToDate.setProfitBeforeTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 8)));
		incomeYearToDate.setEndDate(LocalDate.now());
		incomeYearToDate.setStartDate(LocalDate.now().minusYears(20));
		
		incomeYearToDate.setAddback(addbackData.addBack());
		return incomeYearToDate;
	}
}