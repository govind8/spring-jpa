package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.AgriculturalAsset;
import com.moneycatcha.app.entity.Aircraft;
import com.moneycatcha.app.entity.CleaningEquipment;
import com.moneycatcha.app.entity.ContractDetails;
import com.moneycatcha.app.entity.EarthMovingMiningAndConstruction;
import com.moneycatcha.app.entity.Encumbrance;
import com.moneycatcha.app.entity.EstimatedValue;
import com.moneycatcha.app.entity.FinancialAsset;
import com.moneycatcha.app.entity.HospitalityAndLeisure;
import com.moneycatcha.app.entity.ITAndAVEquipment;
import com.moneycatcha.app.entity.MaterialsHandlingAndLifting;
import com.moneycatcha.app.entity.MedicalEquipment;
import com.moneycatcha.app.entity.MobileComputing;
import com.moneycatcha.app.entity.MotorVehicle;
import com.moneycatcha.app.entity.NonRealEstateAsset;
import com.moneycatcha.app.entity.OfficeEquipment;
import com.moneycatcha.app.entity.OtherAsset;
import com.moneycatcha.app.entity.PPSR;
import com.moneycatcha.app.entity.PlantEquipmentAndIndustrial;
import com.moneycatcha.app.entity.Registration;
import com.moneycatcha.app.entity.RegistrationEvent;
import com.moneycatcha.app.entity.ToolsOfTrade;
import com.moneycatcha.app.entity.Encumbrance.InFavourOf;
import com.moneycatcha.app.entity.FinancialAsset.Shares;
import com.moneycatcha.app.entity.NonRealEstateAsset.Insurance;
import com.moneycatcha.app.model.AssetTransactionList;
import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.BalloonRVInputPatternList;
import com.moneycatcha.app.model.CollateralClassList;
import com.moneycatcha.app.model.CollateralTypeList;
import com.moneycatcha.app.model.ConditionList;
import com.moneycatcha.app.model.EncumbranceInFavourOfCapacityList;
import com.moneycatcha.app.model.EstimateBasisNonRealEstateAssetList;
import com.moneycatcha.app.model.FinancialAssetTypeList;
import com.moneycatcha.app.model.FinancialTransactionTypeList;
import com.moneycatcha.app.model.GoodToBeUsedLocationList;
import com.moneycatcha.app.model.MedicalEquipmentTypeList;
import com.moneycatcha.app.model.MobileComputingTypeList;
import com.moneycatcha.app.model.NonRealEstateAssetEncumbranceList;
import com.moneycatcha.app.model.NonRealEstateAssetTypeList;
import com.moneycatcha.app.model.NonRealEstateSecurityPriorityList;
import com.moneycatcha.app.model.OtherAssetTypeList;
import com.moneycatcha.app.model.PlantEquipmentAndIndustrialTypeList;
import com.moneycatcha.app.model.RegistrationEventTypeList;
import com.moneycatcha.app.model.SecurityPriorityList;
import com.moneycatcha.app.model.ShareTypeList;
import com.moneycatcha.app.model.SupplierTypeList;
import com.moneycatcha.app.model.TaxDepreciationMethodList;
import com.moneycatcha.app.model.ToolsOfTradeTypeList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class NonRealEstateAssetData {

	PercentOwnedTypeData percentOwnedData = new PercentOwnedTypeData();
	
	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	BusinessData businessData = new BusinessData();
	
	DepositAccountDetailsData depositData = new DepositAccountDetailsData();

	DurationTypeData durationData = new DurationTypeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	LoanDetailsData loanDetailsData = new LoanDetailsData();
	
	FinancialAccountTypeData financialAccountTypeData = new FinancialAccountTypeData();

	public List<NonRealEstateAsset> nonRealEstateAsset() {
		
		List<NonRealEstateAsset> nreAsset = new ArrayList<>();
		
		for (int i=0; i < 3; i++) {
			NonRealEstateAsset nonRealEstateAsset = new NonRealEstateAsset();
			nonRealEstateAsset.setAgriculturalAsset(agriculturalAsset()); //object
			nonRealEstateAsset.setAircraft(aircraft()); //object
			nonRealEstateAsset.setBusiness(businessData.business());
			nonRealEstateAsset.setCleaningEquipment(cleaningEquipment()); //object
			nonRealEstateAsset.setContractDetails(contractDetails());  //object
			nonRealEstateAsset.setEarthMovingMiningAndConstruction(earthMovingMiningAndConstruction()); //object
			nonRealEstateAsset.setEncumbrance(encumbrance());  //object
			nonRealEstateAsset.setFinancialAsset(financialAsset());  //object
			nonRealEstateAsset.setFundsDisbursementType(loanDetailsData.fundsDisbursementType()); //object
			nonRealEstateAsset.setHospitalityAndLeisure(hospitalityAndLeisure()); //object
			nonRealEstateAsset.setInsurance(insurance()); //object
			nonRealEstateAsset.setITAndAVEquipment(itAndAVEquipment()); //object
			nonRealEstateAsset.setMaterialsHandlingAndLifting(materialsHandlingAndLifting()); //object
			nonRealEstateAsset.setMedicalEquipment(medicalEquipment());  //object
			nonRealEstateAsset.setMobileComputing(mobileComputing()); //object
			nonRealEstateAsset.setMotorVehicle(motorVehicle()); //object
			nonRealEstateAsset.setOfficeEquipment(officeEquipment()); //object
			nonRealEstateAsset.setOtherAsset(otherAsset()); //object
			nonRealEstateAsset.setPercentOwned(percentOwnedData.percentOwnership()); //object
			nonRealEstateAsset.setPlantEquipmentAndIndustrial(plantEquipmentAndIndustrial()); //object
			nonRealEstateAsset.setPPSR(ppsr()); //object
			nonRealEstateAsset.setToolsOfTrade(toolsOfTrade()); //object
			nonRealEstateAsset.setAmountToBeReduced(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			nonRealEstateAsset.setContractOfSale(YesNoList.NO);
			nonRealEstateAsset.setEncumbered(YesNoList.NO);
			nonRealEstateAsset.setFinancialTransactionType(FinancialTransactionTypeList.LEASEBACK);
			nonRealEstateAsset.setLenderAssessmentReason(RandomStringUtils.randomAlphabetic(15));
			nonRealEstateAsset.setLenderAssessmentRequired(YesNoList.NO);
			nonRealEstateAsset.setPrimarySecurity(YesNoList.NO);
			nonRealEstateAsset.setToBeReduced(YesNoList.YES);
			nonRealEstateAsset.setToBeSold(YesNoList.YES);
			nonRealEstateAsset.setToBeUsedAsSecurity(YesNoList.YES);
			nonRealEstateAsset.setTransaction(AssetTransactionList.PURCHASING);
			nonRealEstateAsset.setType(NonRealEstateAssetTypeList.CLEANING);
			nonRealEstateAsset.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			nonRealEstateAsset.setVerified(YesNoList.NO);
			nonRealEstateAsset.setXCustomerTransactionAnalysis(RandomStringUtils.randomAlphabetic(15));
			nonRealEstateAsset.setXVendorTaxInvoice(RandomStringUtils.randomAlphabetic(15));
			nonRealEstateAsset.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			nreAsset.add(nonRealEstateAsset);
		}
			
		return nreAsset;
	}
	
	// AgriculturalAsset
	public AgriculturalAsset agriculturalAsset() {
		
		AgriculturalAsset agricultural = new AgriculturalAsset();
		agricultural.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		agricultural.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		agricultural.setCondition(ConditionList.DEMO);
		agricultural.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		agricultural.setDescription(RandomStringUtils.randomAlphabetic(10));
		agricultural.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		agricultural.setMake(RandomStringUtils.randomAlphabetic(10));
		agricultural.setModel(RandomStringUtils.randomAlphabetic(10));
		agricultural.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		agricultural.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		agricultural.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		agricultural.setYear(1999);
		agricultural.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		return agricultural;		
	}
	
	// Aircraft	
	public Aircraft aircraft() {
		Aircraft aircraft = new Aircraft();

		aircraft.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		aircraft.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		aircraft.setCondition(ConditionList.DEMO);
		aircraft.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		aircraft.setDescription(RandomStringUtils.randomAlphabetic(10));
		aircraft.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		aircraft.setMake(RandomStringUtils.randomAlphabetic(10));
		aircraft.setModel(RandomStringUtils.randomAlphabetic(10));
		aircraft.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		aircraft.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		aircraft.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		aircraft.setYear(1999);
		aircraft.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		return aircraft;
	}
	
	public CleaningEquipment cleaningEquipment() {
		
		CleaningEquipment cleaning = new CleaningEquipment(); 
		
		cleaning.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		cleaning.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		cleaning.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		cleaning.setCondition(ConditionList.DEMO);
		cleaning.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		cleaning.setDescription(RandomStringUtils.randomAlphabetic(10));
		cleaning.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		cleaning.setMake(RandomStringUtils.randomAlphabetic(10));
		cleaning.setModel(RandomStringUtils.randomAlphabetic(10));
		cleaning.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		cleaning.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		cleaning.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		cleaning.setYear(1099);
		cleaning.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		
		return cleaning;
	}
	
	public ContractDetails contractDetails() {
		
		ContractDetails contract = new ContractDetails();
		
		contract.setArmsLengthTransaction(YesNoList.NO);
		contract.setContractDate(LocalDate.now());
		contract.setContractPriceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setDepositAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setDepositPaid(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setDepositPercentageRequested(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setEarlyReleaseOfDepositAuthorityRequested(YesNoList.YES);
		contract.setEstimatedSettlementDate(LocalDate.now().plusMonths(2));
		contract.setFinanceApprovalDate(LocalDate.now().plusMonths(1));
		contract.setGstAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setGstOverwritten(YesNoList.NO);
		contract.setInputTaxCredit(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setLicencedRealEstateAgentContract(YesNoList.NO);
		contract.setLuxuryCarTax(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));		
		contract.setNetTradeIn(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setNonClaimableGST(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setOnRoadCosts(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setOtherCost(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setRegistrationCost(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setStampDuty(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setSupplierType(SupplierTypeList.LICENSED_DEALER);
		contract.setTotalCost(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setTradeInAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setTradeInExistingFinance(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setTransferOfLandAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		contract.setXVendor(RandomStringUtils.randomAlphabetic(10));
		
		return contract;
	}
	
	public EarthMovingMiningAndConstruction earthMovingMiningAndConstruction() {
		
		EarthMovingMiningAndConstruction emmc = new EarthMovingMiningAndConstruction();
		
		emmc.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		emmc.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		emmc.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		emmc.setCondition(ConditionList.DEMO);
		emmc.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		emmc.setDescription(RandomStringUtils.randomAlphabetic(10));
		emmc.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		emmc.setMake(RandomStringUtils.randomAlphabetic(10));
		emmc.setModel(RandomStringUtils.randomAlphabetic(10));
		emmc.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		emmc.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		emmc.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		emmc.setYear(2099);
		emmc.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		
		return emmc;
	}

	public List<Encumbrance> encumbrance() {
		List<Encumbrance> encumbrances = new ArrayList<>();
		
		for(int i=0; i < 3; i++) {
			Encumbrance encumbrance = new Encumbrance();
			encumbrance.setDescription(RandomStringUtils.randomAlphabetic(10));
			encumbrance.setNonrealEncumbranceType(NonRealEstateAssetEncumbranceList.LEASE);
			encumbrance.setInFavourOf(inFavourOf());
			encumbrance.setSecurityPriority(SecurityPriorityList.FIRST_MORTGAGE);
			encumbrance.setNonrealSecurityPriority(NonRealEstateSecurityPriorityList.FIRST_SECURITY_INTEREST);
			encumbrance.setRegisteredNumber(RandomStringUtils.randomAlphabetic(10));
			encumbrance.setRegistrationDate(LocalDate.now());
			encumbrance.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			encumbrances.add(encumbrance);
		}
		
		return encumbrances;
	}
	
	// Encumbrance.InFavourOf
	public List<InFavourOf> inFavourOf() {
		List<InFavourOf> favours = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			InFavourOf favour = new InFavourOf();
			favour.setCapacity(EncumbranceInFavourOfCapacityList.ASSET_HELD_IN_OWN_RIGHT_AND_ON_BEHALF_OF_TRUST);
			favour.setName(RandomStringUtils.randomAlphabetic(10));
			favour.setXInFavourOf(RandomStringUtils.randomAlphabetic(10));
			favours.add(favour);
		}
		return favours;
	}

	public EstimatedValue estimatedValue() {
		EstimatedValue estimated = new EstimatedValue();
		
		estimated.setBalloonRVAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		estimated.setBalloonRVInputPattern(BalloonRVInputPatternList.AMOUNT);
		estimated.setBalloonRVPercent(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		estimated.setEstimateBasisNonRealEstateAsset(EstimateBasisNonRealEstateAssetList.APPLICANT_ESTIMATE);
		estimated.setEstimatedCGTLiability(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		estimated.setMinimumResidualValue(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		estimated.setTaxDepreciationMethod(TaxDepreciationMethodList.DIMINISHING_VALUE);
		estimated.setTaxDepreciationRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		estimated.setValue(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		estimated.setValuedDate(LocalDate.now());
		estimated.setXValuer(RandomStringUtils.randomAlphabetic(10));

		return estimated;
	}

	// FinancialAsset
	public FinancialAsset financialAsset() {
		FinancialAsset fa = new FinancialAsset();
		fa.setAccountNumber(financialAccountTypeData.accountNumber());
		fa.setDescription(RandomStringUtils.randomAlphabetic(10));
		fa.setShares(shares());
		fa.setTransferOwnershipToSMSF(YesNoList.NO);
		fa.setType(FinancialAssetTypeList.RECEIVABLES);
		
		return fa;
	}
	
	public Shares shares() {
		Shares share = new Shares();
		share.setType(ShareTypeList.DEBENTURES);
		return share;
	}
	
	//HospitalityAndLeisure
	public HospitalityAndLeisure hospitalityAndLeisure() {
		
		HospitalityAndLeisure hospitality = new HospitalityAndLeisure();
		
		hospitality.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		hospitality.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		hospitality.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		hospitality.setCondition(ConditionList.DEMO);
		hospitality.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		hospitality.setDescription(RandomStringUtils.randomAlphabetic(10));
		hospitality.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		hospitality.setMake(RandomStringUtils.randomAlphabetic(10));
		hospitality.setModel(RandomStringUtils.randomAlphabetic(10));
		hospitality.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		hospitality.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		hospitality.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		hospitality.setYear(1229);
		hospitality.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
	
		return hospitality;
	}

	// List Insurance
	public List<Insurance> insurance() {
		List<Insurance> insurances = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Insurance insurance = new Insurance();
			insurance.setXInsurance(RandomStringUtils.randomAlphanumeric(10));
			insurances.add(insurance);
		}
		return insurances;
	}

	// ITAV
	public ITAndAVEquipment itAndAVEquipment() {
		
		ITAndAVEquipment itav = new ITAndAVEquipment();
		
		itav.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		itav.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		itav.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		itav.setCondition(ConditionList.DEMO);
		itav.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		itav.setDescription(RandomStringUtils.randomAlphabetic(10));
		itav.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		itav.setMake(RandomStringUtils.randomAlphabetic(10));
		itav.setModel(RandomStringUtils.randomAlphabetic(10));
		itav.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		itav.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		itav.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		itav.setYear(2019);
		itav.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));				
		return itav;
	}

	// MaterialsHandlingAndLifting
	public MaterialsHandlingAndLifting materialsHandlingAndLifting() {
		
		MaterialsHandlingAndLifting materials = new MaterialsHandlingAndLifting();

		materials.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		materials.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		materials.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		materials.setCondition(ConditionList.DEMO);
		materials.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		materials.setDescription(RandomStringUtils.randomAlphabetic(10));
		materials.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		materials.setMake(RandomStringUtils.randomAlphabetic(10));
		materials.setModel(RandomStringUtils.randomAlphabetic(10));
		materials.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		materials.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		materials.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		materials.setYear(2020);
		materials.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));		
		return materials;
	}

	// MedicalEquipment
	public MedicalEquipment medicalEquipment() {
		MedicalEquipment medical = new MedicalEquipment();
		medical.setCondition(ConditionList.DEMO);
		medical.setDescription(RandomStringUtils.randomAlphabetic(10));
		medical.setMake(RandomStringUtils.randomAlphabetic(10));
		medical.setModel(RandomStringUtils.randomAlphabetic(10));
		medical.setType(MedicalEquipmentTypeList.DIAGNOSTIC_EQUIPMENT);
		return medical;
	}

	// MobileComputing
	public MobileComputing mobileComputing() {
		
		MobileComputing mobile = new MobileComputing();
		mobile.setCondition(ConditionList.DEMO);
		mobile.setDescription(RandomStringUtils.randomAlphabetic(10));
		mobile.setMake(RandomStringUtils.randomAlphabetic(10));
		mobile.setModel(RandomStringUtils.randomAlphabetic(10));
		mobile.setType(MobileComputingTypeList.TABLET);

		return mobile;
	}

	public MotorVehicle motorVehicle() {
		
		MotorVehicle vehicle = new MotorVehicle();
		vehicle.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		vehicle.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		vehicle.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		vehicle.setBadge(RandomStringUtils.randomAlphabetic(10));
		vehicle.setBody(RandomStringUtils.randomAlphabetic(10));
		vehicle.setColour(RandomStringUtils.randomAlphabetic(10));
		vehicle.setCondition(ConditionList.DEMO);
		vehicle.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		vehicle.setCylinders(new BigInteger(RandomStringUtils.randomNumeric(2)));
		vehicle.setDescription(RandomStringUtils.randomAlphabetic(10));
		vehicle.setDoors(new BigInteger(RandomStringUtils.randomNumeric(2)));
		vehicle.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		vehicle.setEngineCapacity(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		vehicle.setEngineHoursTotal(new BigInteger(RandomStringUtils.randomNumeric(2)));
		vehicle.setEngineID(RandomStringUtils.randomAlphanumeric(10));
		vehicle.setOptions(RandomStringUtils.randomAlphabetic(10));
		vehicle.setKilometres(new BigInteger(RandomStringUtils.randomNumeric(5)));
		vehicle.setMake(RandomStringUtils.randomAlphabetic(10));
		vehicle.setModel(RandomStringUtils.randomAlphabetic(10));
		vehicle.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		vehicle.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		vehicle.setRegisteredInState(AuStateList.WA);
		vehicle.setRegistrationExpiryDate(LocalDate.now().plusYears(2));
		vehicle.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		vehicle.setSeries(RandomStringUtils.randomAlphabetic(10));
		vehicle.setTransmission(RandomStringUtils.randomAlphabetic(10));
		vehicle.setVariant(RandomStringUtils.randomAlphabetic(10));
		vehicle.setYear(2009);
		vehicle.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));
		
		return vehicle;
	}

	public OfficeEquipment officeEquipment() {
		
		OfficeEquipment equipment = new OfficeEquipment();
		equipment.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		equipment.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		equipment.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		equipment.setCondition(ConditionList.DEMO);
		equipment.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		equipment.setDescription(RandomStringUtils.randomAlphabetic(10));
		equipment.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		equipment.setGoodToBeUsedLocation(GoodToBeUsedLocationList.ON_BUSINESS_PREMISES);
		equipment.setMake(RandomStringUtils.randomAlphabetic(10));
		equipment.setModel(RandomStringUtils.randomAlphabetic(10));
		equipment.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		equipment.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		equipment.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		equipment.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));
		
		return equipment;
	}

	// public OtherAsset
	public OtherAsset otherAsset() {
		OtherAsset asset = new OtherAsset();
		asset.setDescription(RandomStringUtils.randomAlphabetic(10));
		asset.setType(OtherAssetTypeList.ART_WORKS);
		return asset;
	}

	// PlantEquipmentAndIndustrial
	public PlantEquipmentAndIndustrial plantEquipmentAndIndustrial() {
		
		PlantEquipmentAndIndustrial plant = new PlantEquipmentAndIndustrial();
		plant.setAdditionalIDType(RandomStringUtils.randomAlphabetic(10));
		plant.setAdditionalIDValue(RandomStringUtils.randomAlphabetic(10));
		plant.setAge(new BigInteger(RandomStringUtils.randomNumeric(2)));
		plant.setCondition(ConditionList.DEMO);
		plant.setConditionDescription(RandomStringUtils.randomAlphabetic(10));
		plant.setDescription(RandomStringUtils.randomAlphabetic(10));
		plant.setEffectiveLife(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		plant.setEngineHoursTotal(new BigInteger(RandomStringUtils.randomNumeric(5)));
		plant.setGoodToBeUsedLocation(GoodToBeUsedLocationList.ON_BUSINESS_PREMISES);
		plant.setMake(RandomStringUtils.randomAlphabetic(10));
		plant.setModel(RandomStringUtils.randomAlphabetic(10));
		plant.setOtherInformation(RandomStringUtils.randomAlphabetic(10));
		plant.setQuantity(new BigInteger(RandomStringUtils.randomNumeric(3)));
		plant.setSerialNumber(RandomStringUtils.randomAlphabetic(10));
		plant.setServicingHistory(RandomStringUtils.randomAlphabetic(10));
		plant.setType(PlantEquipmentAndIndustrialTypeList.COMPRESSORS);
		plant.setXGoodToBeUsedAddress(RandomStringUtils.randomAlphabetic(10));
		
		return plant;
	}

	// PPSR
	public PPSR ppsr() {
		
		PPSR ppsr = new PPSR();
		ppsr.setCollateralClass(CollateralClassList.AGRICULTURE);
		ppsr.setCollateralType(CollateralTypeList.CONSUMER);
		ppsr.setDescriptionOfProceeds(RandomStringUtils.randomAlphabetic(10));
		ppsr.setPmsi(YesNoList.NO);
		ppsr.setPpsRegistrable(YesNoList.NO);
		ppsr.setProceedsToBeClaimed(YesNoList.NO);
		ppsr.setRegistration(registration()); //object
		ppsr.setSubjectToControl(YesNoList.NO);
		ppsr.setSubordinateRegistration(YesNoList.NO);
		ppsr.setSubordinateToNumber(RandomStringUtils.randomAlphabetic(10));
		return ppsr;
	}
	
	// List - Registration
	public List<Registration> registration() {
		List<Registration> registrations = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Registration registration = new Registration();
			registration.setGivingOfNoticeIdentifier(RandomStringUtils.randomAlphanumeric(10));
			registration.setPurchaseMoneySecurityInterest(YesNoList.NO);
			registration.setRegistrationNumber(RandomStringUtils.randomAlphanumeric(10));
			registration.setRegistrationStartDateTime(LocalDateTime.now().minusMonths(12));
			registration.setRegistrationToken(RandomStringUtils.randomAlphanumeric(10));
			registration.setRegistrationEndDateTime(LocalDateTime.now());
			registration.setRegistrationEvent(registrationEvent()); //object
			registration.setSecuredPartyGroupNumber(RandomStringUtils.randomAlphanumeric(10));
			registrations.add(registration);
		}
		return registrations;
	}
	
	// PPSR List RegistrationEvent
	public List<RegistrationEvent> registrationEvent() {
		List<RegistrationEvent> events = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RegistrationEvent event = new RegistrationEvent();
			event.setChangeDateTime(LocalDateTime.now());
			event.setChangeNumber(RandomStringUtils.randomAlphanumeric(10));
			event.setPpsrTransactionID(RandomStringUtils.randomAlphanumeric(10));
			event.setType(RegistrationEventTypeList.AMENDMENT);
			events.add(event);
		}
		return events;
	}
	
	// ToolsOfTrade
	public ToolsOfTrade toolsOfTrade() {
		ToolsOfTrade tools = new ToolsOfTrade();
		tools.setCondition(ConditionList.REFURBISHED);
		tools.setDescription(RandomStringUtils.randomAlphabetic(10));
		tools.setMake(RandomStringUtils.randomAlphabetic(10));
		tools.setModel(RandomStringUtils.randomAlphabetic(10));
		tools.setType(ToolsOfTradeTypeList.CAMERA);
		return tools;
	}

	
}
