package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.Employment;
import com.moneycatcha.app.entity.ForeignEmployed;
import com.moneycatcha.app.entity.IdentityCheck;
import com.moneycatcha.app.entity.NotEmployed;
import com.moneycatcha.app.entity.PAYG;
import com.moneycatcha.app.entity.PersonApplicant;
import com.moneycatcha.app.entity.PersonNameType;
import com.moneycatcha.app.entity.PowerOfAttorney;
import com.moneycatcha.app.entity.Privacy;
import com.moneycatcha.app.entity.ProofOfIdentity;
import com.moneycatcha.app.entity.TaxDeclarationDetailsType;
import com.moneycatcha.app.entity.PersonApplicant.Insurance;
import com.moneycatcha.app.entity.PersonApplicant.MaritalStatusDetails;
import com.moneycatcha.app.entity.PersonApplicant.NextOfKin;
import com.moneycatcha.app.entity.PersonApplicant.NominatedBorrower;
import com.moneycatcha.app.entity.PersonApplicant.PreviousName;
import com.moneycatcha.app.entity.PersonApplicant.SourceOfFunds;
import com.moneycatcha.app.entity.PersonApplicant.Will;
import com.moneycatcha.app.entity.ProofOfIdentity.MedicareCard;
import com.moneycatcha.app.model.ApplicantTypeList;
import com.moneycatcha.app.model.AuStateList;
import com.moneycatcha.app.model.CountryCodeList;
import com.moneycatcha.app.model.CreditStatusList;
import com.moneycatcha.app.model.DocumentCategoryList;
import com.moneycatcha.app.model.DocumentTypeList;
import com.moneycatcha.app.model.EmployerTypeList;
import com.moneycatcha.app.model.EmploymentStatusList;
import com.moneycatcha.app.model.ForeignEmployedBasisList;
import com.moneycatcha.app.model.FrequencyShortList;
import com.moneycatcha.app.model.GenderList;
import com.moneycatcha.app.model.IncomeStatusOnOrBeforeSettlementList;
import com.moneycatcha.app.model.KinRelationshipList;
import com.moneycatcha.app.model.MaritalStatusList;
import com.moneycatcha.app.model.MedicareCardColourList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.PaygBasisList;
import com.moneycatcha.app.model.PowerOfAttorneyTypeList;
import com.moneycatcha.app.model.ProofCodeNotEmployedList;
import com.moneycatcha.app.model.ProofCodePAYGList;
import com.moneycatcha.app.model.SourceOfFundsTypeList;
import com.moneycatcha.app.model.TaxDeclarationExemptionCategoryList;
import com.moneycatcha.app.model.YesNoList;

import lombok.Data;

@Data
public class PersonApplicantData {
	
	AddBackData addbackData = new AddBackData();
	
	BeneficialOwnerData beneficialOwnerData = new BeneficialOwnerData();

	BusinessData businessData = new BusinessData();
	
	ContactData contactData = new ContactData();
	
	CreditHistoryData creditHistoryData = new CreditHistoryData();

	DurationTypeData durationData = new DurationTypeData();

	DocumentationInstructionsData documentData = new DocumentationInstructionsData();
	
	ExistingCustomerData existingCustomerData = new ExistingCustomerData();
	
	FinancialAnalysisData finAnalysisData = new FinancialAnalysisData();
	
	ForeignTaxAssociationData foreignTaxData = new ForeignTaxAssociationData();
	
	IncomePreviousData incomePreviousData = new IncomePreviousData();
	
	IncomePriorData incomePriorData = new IncomePriorData();
	
	IncomeRecentData incomeRecentData = new IncomeRecentData();
	
	IncomeYearToDateData incomeYearToDateData = new IncomeYearToDateData();
	
	PartnerData partnerData = new PartnerData();
	
	ResponsibleLendingData responsibleData = new ResponsibleLendingData();
	
	SelfEmployedData selfEmployedData = new SelfEmployedData();
	
	DealingNumberTypeData dealingNumberTypeData = new DealingNumberTypeData();
	
	SourceOfWealthData sowData = new SourceOfWealthData();

	// List<PersonApplicant>
	public List<PersonApplicant> personApplicant() {
		
		List<PersonApplicant> personApplicants = new ArrayList<>();
		
		for (int i=0; i < 3; i++) {
		
			PersonApplicant pa = new PersonApplicant();
			pa.setApplicantType(ApplicantTypeList.BORROWER);
			pa.setCitizenship(CountryCodeList.AD);
			pa.setCompanyDirector(YesNoList.NO);
			pa.setContact(contactData.personApplicantContact()); //object
			pa.setCountryOfBirth(CountryCodeList.AD);
			pa.setCreditHistory(creditHistoryData.creditHistory()); //object
			pa.setCreditStatus(CreditStatusList.CLEAN);
			pa.setCustomerTypeCode(RandomStringUtils.randomAlphabetic(10));
			pa.setCustomerTypeDescription(RandomStringUtils.randomAlphabetic(10));
			pa.setDateOfBirth(LocalDate.now().minusYears(30));
			pa.setDiscussedWithBeneficiaries(YesNoList.NO);
			pa.setDocumentationInstructions(documentData.document());
			pa.setEligibleForFHOG(YesNoList.YES);
			pa.setFirstHomeBuyer(YesNoList.NO);
			pa.setFirstPropertyBuyer(YesNoList.NO);
			pa.setEmployment(employment());
			pa.setGender(GenderList.MALE);
			pa.setHasAppliedForAustralianCitizenship(YesNoList.NO);
			pa.setHasAppliedForPermanentResidencyVisa(YesNoList.NO);
			pa.setHasAWill(YesNoList.NO);
			pa.setHasPreviousName(YesNoList.YES);
			pa.setIdentityCheck(identityCheck()); //object
			pa.setImmigrant(YesNoList.NO);
			pa.setImmigrationDate(LocalDate.now().minusYears(2));
			pa.setIndependentFinancialAdvice(YesNoList.NO);
			pa.setIndependentLegalAdvice(YesNoList.NO);
			pa.setInsurance(insurance()); //object
			pa.setIsExistingCustomer(YesNoList.YES);
			pa.setIsLenderStaff(YesNoList.YES);
			pa.setJointNomination(YesNoList.NO);
			pa.setJointStatementOfPosition(YesNoList.NO);
			pa.setLocalAgentOfForeignCompany(YesNoList.NO);
			pa.setMaritalStatus(MaritalStatusList.MARRIED);
			pa.setMaritalStatusDetails(maritalStatusDetails());  //object
			pa.setMonthsInCurrentProfession(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			pa.setMothersMaidenName(RandomStringUtils.randomAlphabetic(10));
			pa.setNextOfKin(nextOfKin()); //object 
			pa.setNominatedBorrower(nominatedBorrower()); //object
			pa.setOnBehalfOfUnincorporatedAssociation(YesNoList.YES);
			pa.setPersonName(personName()); //object
			pa.setPermanentResidencyDate(LocalDate.now().minusYears(4));
			pa.setPlaceOfBirth(RandomStringUtils.randomAlphabetic(10));
			pa.setPoaGranted(YesNoList.YES);
			pa.setPowerOfAttorney(powerOfAttorney());  //object
			pa.setPreviousName(previousName());  //object
			pa.setPrimaryApplicant(YesNoList.YES);
			pa.setPrincipalForeignResidence(CountryCodeList.AQ);
			pa.setPrivacy(privacy());
			pa.setProofOfIdentity(proofOfIdentity());  //object
			pa.setResponsibleLending(responsibleData.responsibleLendingType()); //object
			pa.setSourceOfWealth(sowData.sourceOfWealth());  //object
			pa.setSourceOfFunds(sourceOfFunds()); //object
			pa.setTaxDeclarationDetails(taxDeclarationDetails());  //object
			pa.setTemporaryVisaExpiryDate(LocalDate.now().minusYears(1));
			pa.setUnderDuress(YesNoList.YES);
			pa.setUnderstandApplication(YesNoList.YES);
			pa.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			pa.setVisaSubclassCode(RandomStringUtils.randomAlphabetic(10));
			pa.setWill(will()); //object
			pa.setXAccountant(RandomStringUtils.randomAlphabetic(10));
			pa.setXHousehold(RandomStringUtils.randomAlphabetic(10));
			pa.setXPersonalReference(RandomStringUtils.randomAlphabetic(10));
			pa.setXSolicitor(RandomStringUtils.randomAlphabetic(10));
			pa.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
	
			personApplicants.add(pa);
		}
		return personApplicants;
	}

	// List<Employment>
	public List<Employment> employment() {
		List<Employment> employments = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			Employment employment = new Employment();
			employment.setForeignEmployed(foreignEmployed());  // object
			employment.setNotEmployed(notEmployed());  // object
			employment.setPAYG(payg());  // object
			employment.setSelfEmployed(selfEmployedData.selfEmployed()); // object
			employments.add(employment);
		}
		return employments;
	}
	
	// ForeignEmployed
	public ForeignEmployed foreignEmployed() {
		
		ForeignEmployed foreignEmployed = new ForeignEmployed();
		// Income
		ForeignEmployed.Income income = new ForeignEmployed.Income();
		income.setBonusAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setBonusFrequency(FrequencyShortList.FORTNIGHTLY);
		income.setCarAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setCarAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setCommissionFrequency(FrequencyShortList.WEEKLY);
		income.setGrossRegularOvertimeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setGrossRegularOvertimeFrequency(FrequencyShortList.WEEKLY);
		income.setGrossSalaryAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setGrossSalaryFrequency(FrequencyShortList.WEEKLY);
		income.setNetBonusAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetBonusFrequency(FrequencyShortList.WEEKLY);
		income.setNetCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetCommissionFrequency(FrequencyShortList.WEEKLY);
		income.setNetCarAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetCarAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setNetSalaryAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetSalaryFrequency(FrequencyShortList.WEEKLY);
		income.setNetWorkersCompensationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetWorkersCompensationFrequency(FrequencyShortList.WEEKLY);
		income.setProofSighted(YesNoList.NO);
		income.setWorkAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setWorkAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setWorkersCompensationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setWorkersCompensationFrequency(FrequencyShortList.WEEKLY);
		
		foreignEmployed.setAverageHoursPerWeek(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		foreignEmployed.setBasis(ForeignEmployedBasisList.COMMISSION_ONLY);
		foreignEmployed.setCompanyCar(YesNoList.YES);
		foreignEmployed.setCustomIndustryCode(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setEmployerType(EmployerTypeList.PUBLIC);
		foreignEmployed.setEndDate(LocalDate.now().plusYears(2));
		foreignEmployed.setGicsCode(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setIncome(income);
		foreignEmployed.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.ENDS);
		foreignEmployed.setIndustry(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setMainBusinessActivity(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setOccupation(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setOnProbation(YesNoList.NO);
		foreignEmployed.setPositionTitle(RandomStringUtils.randomAlphabetic(10));
		foreignEmployed.setProbationDateEnds(LocalDate.now());
		foreignEmployed.setProbationDateStarts(LocalDate.now());
		foreignEmployed.setStartDate(LocalDate.now());
		foreignEmployed.setUniqueID(RandomStringUtils.randomAlphabetic(15));
		foreignEmployed.setStatus(EmploymentStatusList.PREVIOUS);
		foreignEmployed.setXEmployer(RandomStringUtils.randomAlphabetic(10));
		
		return foreignEmployed;
	}
	
	// NotEmployed
	public NotEmployed notEmployed() {
		
		NotEmployed.Income income = new NotEmployed.Income();
		income.setGovernmentBenefitsAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setGovernmentBenefitsFrequency(FrequencyShortList.WEEKLY);
		income.setNetGovernmentBenefitsAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetGovernmentBenefitsFrequency(FrequencyShortList.WEEKLY);
		income.setNetNewstartAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetNewstartAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setNetOtherIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetOtherIncomeFrequency(FrequencyShortList.WEEKLY);
		income.setNetPrivatePensionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetPrivatePensionFrequency(FrequencyShortList.WEEKLY);
		income.setNetSuperannuationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetSuperannuationFrequency(FrequencyShortList.WEEKLY);
		income.setOtherIncomeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setOtherIncomeDescription(RandomStringUtils.randomAlphabetic(15));
		income.setOtherIncomeFrequency(FrequencyShortList.WEEKLY);
		income.setPrivatePensionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setPrivatePensionFrequency(FrequencyShortList.WEEKLY);
		income.setProofCode(ProofCodeNotEmployedList.LETTER_FROM_APPROPRIATE_BODY);
		income.setProofSighted(YesNoList.NO);
		income.setSmsf(YesNoList.YES);
		income.setSuperannuationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setSuperannuationFrequency(FrequencyShortList.WEEKLY);
	
		NotEmployed notEmployed = new NotEmployed();
		notEmployed.setDuration(durationData.duration());
		notEmployed.setEndDate(LocalDate.now().plusYears(1));
		notEmployed.setHomeDuties(YesNoList.YES);
		notEmployed.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.ENDS);
		notEmployed.setOnBenefits(YesNoList.YES);
		notEmployed.setRetired(YesNoList.YES);
		notEmployed.setStatus(EmploymentStatusList.PRIMARY);
		notEmployed.setStudent(YesNoList.YES);
		notEmployed.setUniqueID(RandomStringUtils.randomAlphabetic(15));
		
		return notEmployed;

	}
	
	// PAYG
	public PAYG payg() {
		PAYG.Income income = new PAYG.Income();
		income.setBonusAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setBonusFrequency(FrequencyShortList.WEEKLY);
		income.setCarAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setCarAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setCommissionFrequency(FrequencyShortList.WEEKLY);
		income.setGrossRegularOvertimeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setGrossRegularOvertimeFrequency(FrequencyShortList.WEEKLY);
		income.setGrossSalaryAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setGrossSalaryFrequency(FrequencyShortList.WEEKLY);
		income.setNetBonusAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetBonusFrequency(FrequencyShortList.WEEKLY);
		income.setNetCarAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetCarAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setNetCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetCommissionFrequency(FrequencyShortList.WEEKLY);
		income.setNetRegularOvertimeAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetRegularOvertimeFrequency(FrequencyShortList.WEEKLY);
		income.setNetSalaryAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetSalaryFrequency(FrequencyShortList.WEEKLY);
		income.setNetRegularOvertimeFrequency(FrequencyShortList.WEEKLY);
		income.setNetWorkAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetWorkAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setNetWorkersCompensationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setNetWorkersCompensationFrequency(FrequencyShortList.WEEKLY);
		income.setProofCode(ProofCodePAYGList.GROUP_CERTIFICATE);
		income.setProofSighted(YesNoList.NO);
		income.setWorkAllowanceAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setWorkAllowanceFrequency(FrequencyShortList.WEEKLY);
		income.setWorkersCompensationAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 4)));
		income.setWorkersCompensationFrequency(FrequencyShortList.WEEKLY);
		
		PAYG payg = new PAYG();
		payg.setAnzscoOccupationCode(RandomStringUtils.randomAlphabetic(10));
		payg.setAustralianBIC(RandomStringUtils.randomAlphabetic(10));
		payg.setAverageHoursPerWeek(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		payg.setBasis(PaygBasisList.CASUAL);
		payg.setCompanyCar(YesNoList.NO);
		payg.setCustomIndustryCode(RandomStringUtils.randomAlphabetic(10));
		payg.setDuration(durationData.duration());
		payg.setEmployerType(EmployerTypeList.PRIVATE);
		payg.setEndDate(LocalDate.now().plusYears(1));
		payg.setGicsCode(RandomStringUtils.randomAlphabetic(10));
		payg.setIncomeStatusOnOrBeforeSettlement(IncomeStatusOnOrBeforeSettlementList.ENDS);
		payg.setIndustry(RandomStringUtils.randomAlphabetic(10));
		payg.setIndustryCode(RandomStringUtils.randomAlphabetic(10));
		payg.setMainBusinessActivity(RandomStringUtils.randomAlphabetic(10));
		payg.setOccupation(RandomStringUtils.randomAlphabetic(10));
		payg.setOccupationCode(RandomStringUtils.randomAlphabetic(10));
		payg.setOnProbation(YesNoList.NO);
		payg.setPositionTitle(RandomStringUtils.randomAlphabetic(10));
		payg.setProbationDateEnds(LocalDate.now().plusYears(1));
		payg.setProbationDateStarts(LocalDate.now().plusYears(1));
		payg.setStartDate(LocalDate.now());
		payg.setStatus(EmploymentStatusList.PREVIOUS);
		payg.setUniqueID(RandomStringUtils.randomAlphabetic(15));
		payg.setXEmployer(RandomStringUtils.randomAlphabetic(10));
		return payg;
		
	}

	// IdentityCheck
	public List<IdentityCheck> identityCheck() {
		
		List<IdentityCheck> checks = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			IdentityCheck identity = new IdentityCheck();
			identity.setDataSource(RandomStringUtils.randomAlphabetic(15));
			identity.setDate(LocalDate.now());
			identity.setPoliticallyExposedPerson(YesNoList.YES);
			identity.setResult(RandomStringUtils.randomAlphabetic(15));
			identity.setSanctionedPerson(YesNoList.YES);
			checks.add(identity);
		}
		return checks;
		
	}
	
	// List Insurance
	public List<Insurance> insurance() {
		List<Insurance> insurance = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			insurance.add(new Insurance());
		}
		for(int i=0; i < insurance.size(); i++)  {
			insurance.get(i).setXInsurance(RandomStringUtils.randomAlphanumeric(10));
		}
		return insurance;
	}
	
	// MaritalStatusDetails
	public MaritalStatusDetails maritalStatusDetails() {
	
		MaritalStatusDetails marital = new MaritalStatusDetails();
		marital.setMaritalStatusChangeDate(LocalDate.now());
		marital.setXSpouse(RandomStringUtils.randomAlphabetic(15));
		return marital;
		
	}
	
	// NextOfKin
	public NextOfKin nextOfKin() {
	
		NextOfKin nok = new NextOfKin();
		nok.setKinRelationship(KinRelationshipList.FRIEND);
		nok.setXPerson(RandomStringUtils.randomAlphabetic(15));
		return nok;
		
	}
	
	// NominatedBorrower
	public NominatedBorrower nominatedBorrower() {
	
		NominatedBorrower nominated = new NominatedBorrower();
		nominated.setXNominee(RandomStringUtils.randomAlphabetic(15));
		return nominated;
	}
	
	// PersonName
	public PersonNameType personName() {
	
		PersonNameType person = new PersonNameType();
		person.setFirstName(RandomStringUtils.randomAlphabetic(15));
		person.setKnownAs(RandomStringUtils.randomAlphabetic(15));
		person.setMiddleNames(RandomStringUtils.randomAlphabetic(15));
		person.setNameTitle(NameTitleList.HON);
		person.setOtherNameTitle(RandomStringUtils.randomAlphabetic(15));
		person.setSurname(RandomStringUtils.randomAlphabetic(15));
		return person;
	}

	
	// PowerOfAttorney
	public PowerOfAttorney powerOfAttorney() {
	
		PowerOfAttorney poa = new PowerOfAttorney();
		poa.setDealingNumber(dealingNumberTypeData.dealingNumberType());
		poa.setType(PowerOfAttorneyTypeList.GENERAL);
		poa.setXpoaHolder(RandomStringUtils.randomAlphabetic(15));
		return poa;
	}
	
	
	// PreviousName
	public PreviousName previousName() {
	
		PreviousName previousName = new PreviousName();
		previousName.setFirstName(RandomStringUtils.randomAlphabetic(15));
		previousName.setMiddleNames(RandomStringUtils.randomAlphabetic(15));
		previousName.setNameChangeReason(RandomStringUtils.randomAlphabetic(15));
		previousName.setNameTitle(NameTitleList.MISS);
		previousName.setSurname(RandomStringUtils.randomAlphabetic(15));
		
		return previousName;
	}
	
	
	// Privacy
	public Privacy privacy() {
	
		Privacy privacy = new Privacy();
		privacy.setAllowApplicationStatusUpdates(YesNoList.YES);
		privacy.setAllowCreditBureauIdentityCheck(YesNoList.YES);
		privacy.setAllowCreditCheck(YesNoList.YES);
		privacy.setAllowDirectMarketing(YesNoList.YES);
		privacy.setAllowElectronicIdentityCheck(YesNoList.YES);
		privacy.setAllowTelemarketing(YesNoList.YES);
		privacy.setAllowThirdPartyDisclosure(YesNoList.YES);
		privacy.setCreditAuthoritySigned(YesNoList.YES);
		privacy.setDateSigned(LocalDate.now().minusYears(1));
		privacy.setPrivacyActConsentSigned(YesNoList.YES);
		return privacy;
	}
	
	// List ProofOfIdentity
	public List<ProofOfIdentity> proofOfIdentity() {
		
		List<ProofOfIdentity> poi = new ArrayList<ProofOfIdentity>();
	
		for(int i=0; i < 3; i++)  {
			
			MedicareCard medicare = new MedicareCard();
			medicare.setColour(MedicareCardColourList.GREEN);
			medicare.setIndividualReferenceNumber(new BigInteger(RandomStringUtils.randomNumeric(1)));

			ProofOfIdentity identity = new ProofOfIdentity();
			identity.setAustralianStateOfIssue(AuStateList.ACT);
			identity.setCertifiedCopy(YesNoList.YES);
			identity.setCountryOfIssue(CountryCodeList.CG);
			identity.setDateDocumentVerified(LocalDate.now().minusYears(1));
			identity.setDateOfIssue(LocalDate.now().minusYears(1));
			identity.setDobVerified(YesNoList.YES);
			identity.setDocumentCategory(DocumentCategoryList.PHOTOGRAPHIC);
			identity.setDocumentNumber(RandomStringUtils.randomNumeric(5));
			identity.setDocumentType(DocumentTypeList.AUSTRALIAN_TAXATION_OFFICE_CORRESPONDENCE_WITH_TFN);
			identity.setExpiryDate(LocalDate.now().plusYears(1));
			identity.setIsPreviousNameIdentification(YesNoList.YES);
			identity.setMedicareCard(medicare); //object
			identity.setMiddleNameOnDocument(RandomStringUtils.randomAlphabetic(15));
			identity.setNameOnDocument(RandomStringUtils.randomAlphabetic(15));
			identity.setNameVerified(YesNoList.YES);
			identity.setOriginal(YesNoList.YES);
			identity.setOtherDescription(RandomStringUtils.randomAlphabetic(15));
			identity.setPhotographVerified(YesNoList.YES);
			identity.setPlaceOfIssue(RandomStringUtils.randomAlphabetic(15));
			identity.setResidentialAddressVerified(YesNoList.YES);
			identity.setSignatureVerified(YesNoList.YES);
			identity.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			identity.setXLocationDocumentVerified(RandomStringUtils.randomAlphabetic(15));
			poi.add(identity);

		}
		return poi;
	}
		

	// List SourceOfFunds
	public List<SourceOfFunds> sourceOfFunds() {
		List<SourceOfFunds> sofs = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			SourceOfFunds fund = new SourceOfFunds();
			fund.setDetail(RandomStringUtils.randomAlphabetic(15));
			fund.setPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			fund.setType(SourceOfFundsTypeList.BUSINESS_OWNERSHIP);
			sofs.add(fund);
		}
		return sofs;
	}
	
	// TaxDeclarationDetails
	public TaxDeclarationDetailsType taxDeclarationDetails() {
	
		TaxDeclarationDetailsType tax = new TaxDeclarationDetailsType();
		tax.setAlreadyProvided(YesNoList.YES);
		tax.setExemptionCategory(TaxDeclarationExemptionCategoryList.CHILD_EXEMPTION);
		tax.setUndeclared(YesNoList.YES);
		return tax;
	}
	
	// List Will
	public Will will() {
		Will will = new Will();
		will.setXExecutor(RandomStringUtils.randomAlphabetic(15));
		will.setXWillHeldBy(RandomStringUtils.randomAlphabetic(15));
		return will;
	}
	
}
