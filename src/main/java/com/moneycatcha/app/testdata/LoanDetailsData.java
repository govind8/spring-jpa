package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Component;

import com.moneycatcha.app.entity.Commission;
import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.FundsDisbursementType;
import com.moneycatcha.app.entity.LoanDetails;
import com.moneycatcha.app.entity.PersonNameType;
import com.moneycatcha.app.entity.ProposedRepayment;
import com.moneycatcha.app.entity.SupplementaryCardholder;
import com.moneycatcha.app.entity.FundsDisbursementType.BillerDetails;
import com.moneycatcha.app.entity.LoanDetails.Borrowers;
import com.moneycatcha.app.entity.LoanDetails.BulkReduction;
import com.moneycatcha.app.entity.LoanDetails.EquityRelease;
import com.moneycatcha.app.entity.LoanDetails.LoanPurpose;
import com.moneycatcha.app.entity.LoanDetails.StatementInstructions;
import com.moneycatcha.app.entity.LoanDetails.Term;
import com.moneycatcha.app.entity.LoanDetails.EquityRelease.AccommodationBond;
import com.moneycatcha.app.entity.LoanDetails.EquityRelease.Amount;
import com.moneycatcha.app.entity.LoanDetails.EquityRelease.Instalments;
import com.moneycatcha.app.entity.LoanDetails.StatementInstructions.NameOnStatement;
import com.moneycatcha.app.entity.LoanDetails.Term.InterestOnlyReason;
import com.moneycatcha.app.entity.ProposedRepayment.Authoriser;
import com.moneycatcha.app.entity.ProposedRepayment.RegularRepayment;
import com.moneycatcha.app.entity.ProposedRepayment.StructuredPayments;
import com.moneycatcha.app.entity.ProposedRepayment.StructuredPayments.Payment;
import com.moneycatcha.app.model.AccountStatusList;
import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.CreditCardTypeList;
import com.moneycatcha.app.model.DayOfWeekList;
import com.moneycatcha.app.model.FrequencyFullList;
import com.moneycatcha.app.model.FunderList;
import com.moneycatcha.app.model.InstalmentsFrequencyList;
import com.moneycatcha.app.model.InterestChargeFrequencyList;
import com.moneycatcha.app.model.InterestOnlyReasonList;
import com.moneycatcha.app.model.InterestTypeList;
import com.moneycatcha.app.model.LoanPaymentScheduleTypeList;
import com.moneycatcha.app.model.LoanTermUnitsList;
import com.moneycatcha.app.model.LoanTypeList;
import com.moneycatcha.app.model.NameTitleList;
import com.moneycatcha.app.model.NccpStatusList;
import com.moneycatcha.app.model.OccupancyList;
import com.moneycatcha.app.model.PaymentTimingList;
import com.moneycatcha.app.model.PaymentTypeList;
import com.moneycatcha.app.model.PrimaryPurposeLoanPurposeList;
import com.moneycatcha.app.model.ProportionsList;
import com.moneycatcha.app.model.ProposedRepaymentMethodList;
import com.moneycatcha.app.model.StatementCycleList;
import com.moneycatcha.app.model.SurplusFundsDisbursementMethodList;
import com.moneycatcha.app.model.TotalTermTypeList;
import com.moneycatcha.app.model.VaryOnValuationList;
import com.moneycatcha.app.model.WeekList;
import com.moneycatcha.app.model.YesNoList;

@Component
public class LoanDetailsData {

	AmountInForeignCurrencyTypeData foreignData = new AmountInForeignCurrencyTypeData();
	
	DocumentationInstructionsTypeData documentData = new DocumentationInstructionsTypeData();
	
	FinancialAccountTypeData accountData = new FinancialAccountTypeData();
	
	PercentOwnedTypeData percentOwnedData = new PercentOwnedTypeData();
	
	LendingGuaranteeData lendingData = new LendingGuaranteeData();
	
	DepositAccountDetailsData depositData = new DepositAccountDetailsData();

	DurationTypeData durationData = new DurationTypeData();
	
	LiabilityData liabilityData = new LiabilityData();
	
	InsuranceData insuranceData = new InsuranceData();
	
	public List<LoanDetails> loanDetails() {
		
		List<LoanDetails> loans = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			LoanDetails loan = new LoanDetails();
			loan.set_package(pckage()); //object
			loan.setAccelerationPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			loan.setAmountRequestedInForeignCurrency(foreignData.foreignCurrency());
			loan.setAmountRequested(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			loan.setAmountRequestedInclusive(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			loan.setBalloonRepaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			loan.setBalloonRepaymentDate(LocalDate.now().plusWeeks(1));
			loan.setBorrowers(borrowers()); // object
			loan.setBulkReduction(bulkReduction()); //object
			loan.setBuyNowPayLater(YesNoList.NO);
			loan.setCommission(commission()); // object
			loan.setConsiderLowerLimitIfNotEligible(YesNoList.YES);
			loan.setDiscountMargin(liabilityData.margin().get(0)); //object
			loan.setDocumentationInstructions(documentData.document()); //object
			loan.setDSH(liabilityData.dsh());
			loan.setEquityRelease(equityRelease()); //object
			loan.setEstimatedSettlementDate(LocalDate.now().plusYears(5));
			loan.setFeaturesSelected(depositData.featuresSelected().get(0));
			loan.setFunder(FunderList.MACQUARIE);
			loan.setFundsDisbursementType(fundsDisbursementType()); //object
			loan.setGuarantor(guarantors()); // object
			loan.setInterestCalculationFrequency(InterestChargeFrequencyList.DAILY);
			loan.setInterestChargeFrequency(InterestChargeFrequencyList.DAILY);
			loan.setIsAmountRequestedInForeignCurrency(YesNoList.NO);
			loan.setLendingPurpose(liabilityData.lendingPurpose()); //object
			loan.setLoanPurpose(loanPurpose());//object
			loan.setLoanType(LoanTypeList.CHATTEL_MORTGAGE);
			loan.setMainProduct(YesNoList.YES);
			loan.setMaturityDate(LocalDate.now().plusYears(10));
			loan.setMinimumRepaymentRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			loan.setNegativelyGeared(YesNoList.YES);
			loan.setNominateBalloonRepayment(YesNoList.YES);
			loan.setOriginatorReferenceID(RandomStringUtils.randomAlphanumeric(5));
			loan.setProductCode(RandomStringUtils.randomAlphanumeric(5));
			loan.setProductName(RandomStringUtils.randomAlphabetic(5));
			loan.setProposedAnnualInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			loan.setProposedRepayment(proposedRepayment()); //object
			loan.setRateComposition(liabilityData.composition()); //object
			loan.setSecured(YesNoList.YES);
			loan.setSecurity(lendingData.security());//object
			loan.setSmsfLoan(YesNoList.YES);
			loan.setSoftware(liabilityData.software()); //object
			loan.setStatementCycle(StatementCycleList.FORTNIGHTLY);
			loan.setSpecialConcessionCode(RandomStringUtils.randomAlphabetic(5));
			loan.setStatementInstructions(statementInstructions()); //object
			loan.setSupplementaryCardholder(supplementaryCardholders()); //object
			loan.setTaxDeductible(YesNoList.NO);
			loan.setTemplateID(RandomStringUtils.randomAlphabetic(5));
			loan.setTerm(term()); //Object
			loan.setTermsAndConditions(liabilityData.termsandConditions());
			loan.setUniqueID(RandomStringUtils.randomAlphabetic(15));
			loan.setXMasterAgreement(RandomStringUtils.randomAlphabetic(5));
			loan.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			loans.add(loan);
		}
		return loans;
	}
	
	// Borrowers
	public Borrowers borrowers() {
		Borrowers borrowers = new Borrowers();
		borrowers.setOwner(percentOwnedData.owners());
		borrowers.setProportions(ProportionsList.EQUAL);
		return borrowers;
	}
	
	// BulkReduction
	public BulkReduction bulkReduction() {
		BulkReduction br = new BulkReduction();
		br.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		br.setEstimatedDate(LocalDate.now().plusYears(1));
		return br;
	}
	
	
	// Commission
	public Commission commission() {
		Commission commission = new Commission();
		commission.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		commission.setCommissionPaid(YesNoList.NO);
		commission.setCommissionStructure(CommissionStructureList.OTHER);
		commission.setOtherCommissionStructureDescription(RandomStringUtils.randomAlphabetic(15));
		commission.setPromotionCode(RandomStringUtils.randomAlphabetic(5));
		commission.setThirdPartyReferee(YesNoList.NO);
		commission.setTrail(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		return commission;
	}
	
	// EquityRelease
	public EquityRelease equityRelease() {
		
		EquityRelease eq = new EquityRelease();
		
		AccommodationBond ab = new AccommodationBond();
		ab.setExpectedAdmissionDate(LocalDate.now().minusYears(1));
		ab.setServiceProviderName(RandomStringUtils.randomAlphabetic(15));
		ab.setServiceProviderNumber(RandomStringUtils.randomAlphanumeric(5));
		eq.setAccommodationBond(ab); //object
		
		EquityRelease.Amount amount = new Amount();
		amount.setCalculateAsPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		amount.setCashReserve(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		amount.setInstalmentsAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		amount.setLumpSum(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		amount.setVaryOnValuation(VaryOnValuationList.CASH_RESERVE);
		eq.setAmount(amount); //object
		
		
		Instalments instalments = new Instalments();
		instalments.setAmountPerInstalment(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		instalments.setFrequency(InstalmentsFrequencyList.MONTHLY);
		instalments.setIndexed(YesNoList.YES);
		instalments.setIndexRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		instalments.setNumberOfInstalments(new BigInteger(RandomStringUtils.randomNumeric(3)));
		eq.setInstalments(instalments); //object
		
//		EquityRelease eq = new EquityRelease();
		eq.setProtectedEquity(YesNoList.NO);
		eq.setProtectedEquityPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		
		return eq;
	}
	
	// LoanDetails.Guarantor
	public List<LoanDetails.Guarantor> guarantors() {
		List<LoanDetails.Guarantor> guarantors = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			LoanDetails.Guarantor guarantor = new LoanDetails.Guarantor();
			guarantor.setXGuarantor(RandomStringUtils.randomAlphanumeric(10));
			guarantors.add(guarantor);
		}
		return guarantors;
	}
	
	// FundsDisbursementType
	public List<FundsDisbursementType> fundsDisbursementType() {
		
		List<FundsDisbursementType> funds = new ArrayList<>();
		// funds disbursement
		for(int i=0; i < 2; i++)  {
			FundsDisbursementType fd = new FundsDisbursementType();
			
			//Biller Details
			FundsDisbursementType.BillerDetails biller = new BillerDetails();
			biller.setBillerCode(RandomStringUtils.randomNumeric(3));
			biller.setBillerName(RandomStringUtils.randomAlphabetic(10));
			biller.setCrn(RandomStringUtils.randomAlphabetic(10));
			
			// PersonNameType
			PersonNameType personName = new PersonNameType();
			personName.setFirstName(RandomStringUtils.randomAlphabetic(10));
			personName.setKnownAs(RandomStringUtils.randomAlphabetic(10));
			personName.setMiddleNames(RandomStringUtils.randomAlphabetic(10));
			personName.setNameTitle(NameTitleList.HON);
			personName.setOtherNameTitle(RandomStringUtils.randomAlphabetic(10));
			personName.setSurname(RandomStringUtils.randomAlphabetic(10));
			
			fd.setAccountNumber(accountData.accountNumber());
			fd.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			fd.setBillerDetails(biller); 
			fd.setCompanyName(RandomStringUtils.randomAlphabetic(10));
			// Account Number
			fd.setAccountNumber(accountData.accountNumber());
			fd.setMethod(SurplusFundsDisbursementMethodList.BANK_CHEQUE);
			fd.setPersonName(personName); //object
			fd.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			fd.setXAccount(RandomStringUtils.randomAlphanumeric(15));
			fd.setXAddress(RandomStringUtils.randomAlphanumeric(15));
			funds.add(fd);
		}
		return funds;
	}
	
	// LoanPurpose
	public LoanPurpose loanPurpose() {
		LoanPurpose loanPurpose = new LoanPurpose();
		loanPurpose.setNccpStatus(NccpStatusList.REGULATED);
		loanPurpose.setOccupancy(OccupancyList.OWNER_SECONDARY);
		loanPurpose.setOwnerBuilderApplication(YesNoList.NO);
		loanPurpose.setPrimaryPurpose(PrimaryPurposeLoanPurposeList.INVESTMENT_NON_RESIDENTIAL);
		return loanPurpose;
	}
	
	// LoanDetails package
	public LoanDetails.Package pckage() {
		LoanDetails.Package pckage = new LoanDetails.Package();
		pckage.setCategory(RandomStringUtils.randomAlphabetic(10));
		pckage.setCode(RandomStringUtils.randomAlphabetic(10));
		pckage.setMemberID(RandomStringUtils.randomAlphabetic(10));
		pckage.setName(RandomStringUtils.randomAlphabetic(10));
		pckage.setOptionCode(RandomStringUtils.randomAlphabetic(10));
		pckage.setOrganisation(RandomStringUtils.randomAlphabetic(10));
		return pckage;
	}
	
	
	// ProposedRepayment
	public ProposedRepayment proposedRepayment() {
		ProposedRepayment pr = new ProposedRepayment();
		pr.setAnniversaryDate(LocalDate.now().minusWeeks(1));
		pr.setAuthoriser(authoriser()); //object
		pr.setCreditCard(creditCard()); //object
		pr.setDescription(RandomStringUtils.randomAlphabetic(10));
		pr.setFromAccount(accountData.accountNumber());
		pr.setFromAccount(accountData.accountNumber());
		pr.setMethod(ProposedRepaymentMethodList.AUSTRALIA_POST);
		pr.setPaymentTiming(PaymentTimingList.IN_ADVANCE);
		pr.setPerDiemPaymentAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		pr.setPerDiemPaymentDate(LocalDate.now());
		pr.setPerDiemPaymentGSTAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		pr.setPerDiemPaymentStampDutyAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		pr.setRegular(YesNoList.NO);
		pr.setRegularRepayment(regularRepayment()); //object
		pr.setStructuredPayments(structuredPayments()); //object
		pr.setXAccount(RandomStringUtils.randomAlphabetic(10));
		return pr;
	}
	
	// Authoriser
	public List<Authoriser> authoriser() {
		List<Authoriser> authorisers = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Authoriser authoriser = new Authoriser();
			authoriser.setAuthorised(YesNoList.YES);
			authoriser.setAuthorityVerified(YesNoList.YES);
			authoriser.setXParty(RandomStringUtils.randomAlphanumeric(10));
			authorisers.add(authoriser);
		}
		return authorisers;
	}
	
	// CreditCard
	public CreditCard creditCard() {
		CreditCard creditCard = new CreditCard();
		creditCard.setCardHolderName(RandomStringUtils.randomAlphabetic(10));
		creditCard.setExpiryMonth(RandomStringUtils.randomNumeric(2));
		creditCard.setExpiryYear(RandomStringUtils.randomNumeric(2));
		creditCard.setInclusionType(AccountStatusList.CREATE_NEW);
		creditCard.setType(CreditCardTypeList.AMERICAN_EXPRESS);
		creditCard.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
		creditCard.setXCreditCard(RandomStringUtils.randomAlphabetic(10));
		return creditCard;
	}
	
	// RegularRepayment
	public List<RegularRepayment> regularRepayment() {
		List<RegularRepayment> repayments = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			RegularRepayment repayment = new RegularRepayment();
			repayment.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setDayOfMonth("Third");
			repayment.setDayOfWeek(DayOfWeekList.FRIDAY);
			repayment.setEndOfPeriod(YesNoList.NO);
			repayment.setFirstRepaymentDate(LocalDate.now().plusWeeks(3));
			repayment.setFrequency(FrequencyFullList.DAILY);
			repayment.setFrequencyInterval(new BigInteger(RandomStringUtils.randomNumeric(3)));
			repayment.setGstAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setInterestPayment(YesNoList.NO);
			repayment.setLastRepaymentDate(LocalDate.now());
			repayment.setLoanPaymentScheduleType(LoanPaymentScheduleTypeList.EVEN_PRINCIPAL_PAYMENTS);
			repayment.setMinimumAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setPrincipalPayment(YesNoList.YES);
			repayment.setStampDutyAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			repayment.setTotalRepayments(new BigInteger(RandomStringUtils.randomNumeric(3)));
			repayment.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			repayment.setWeek(WeekList.THIRD);
			repayments.add(repayment);
		}
		return repayments;
	}
	
	// StructuredPayments
	public StructuredPayments structuredPayments() {
		StructuredPayments payments = new StructuredPayments();
		payments.setBaseFrequency(FrequencyFullList.DAILY);
		payments.setPayment(payment()); //object
		return payments;
	}
	
	// List<Payment>
	public List<Payment> payment() {
		List<Payment> payments = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			Payment payment = new Payment();
			payment.setAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			payment.setDate(LocalDate.now());
			payment.setGstAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			payment.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(3)));
			payment.setStampDutyAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			payment.setStampDutyAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
			payment.setUniqueID(RandomStringUtils.randomAlphanumeric(10));
			payments.add(payment);
		}
		return payments;		
	}
	
	//StatementInstructions
	public StatementInstructions statementInstructions() {
		
		List<NameOnStatement> names = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			NameOnStatement name = new NameOnStatement();
			name.setXApplicant(RandomStringUtils.randomAlphanumeric(10));
			names.add(name);
		}
		StatementInstructions instructions = new StatementInstructions();
		instructions.setNameOnStatement(names);
		return instructions;
	}
	
	// SupplementaryCardholder
	public List<SupplementaryCardholder> supplementaryCardholders() {
		List<SupplementaryCardholder> supplies = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			SupplementaryCardholder supp = new SupplementaryCardholder();
			supp.setXSupplementaryCardholder(RandomStringUtils.randomAlphanumeric(10));
			supplies.add(supp);
		}
		return supplies;
	}
	
	// Term
	public Term term() {
		
		Term term = new Term();
		term.setDistinctLoanPeriod(liabilityData.distinct());
		term.setInterestOnlyReason(interestOnlyReason());
		term.setInterestType(InterestTypeList.FIXED_RATE);
		term.setInterestTypeDuration(new BigInteger(RandomStringUtils.randomNumeric(3)));
		term.setInterestTypeUnits(LoanTermUnitsList.MONTHS);
		term.setPaymentType(PaymentTypeList.PREPAID_INTEREST);
		term.setPaymentTypeDuration(new BigInteger(RandomStringUtils.randomNumeric(3)));
		term.setPaymentTypeUnits(LoanTermUnitsList.WEEKS);
		term.setRolloverPeriodDuration(new BigInteger(RandomStringUtils.randomNumeric(3)));
		term.setRolloverPeriodUnits(LoanTermUnitsList.DAYS);
		term.setTotalFeesAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		term.setTotalInterestAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		term.setTotalRepayments(new BigInteger(RandomStringUtils.randomNumeric(3)));
		term.setTotalRepaymentsAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 5)));
		term.setTotalTermDuration(new BigInteger(RandomStringUtils.randomNumeric(3)));
		term.setTotalTermType(TotalTermTypeList.TOTAL_TERM);
		term.setTotalTermUnits(LoanTermUnitsList.WEEKS);
		return term;
	}
	
	
	// InterestOnlyReason
	public List<InterestOnlyReason> interestOnlyReason() {
		List<InterestOnlyReason> reasons = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			InterestOnlyReason reason = new InterestOnlyReason();
			reason.setDescription(RandomStringUtils.randomAlphabetic(10)); 
			reason.setReason(InterestOnlyReasonList.LARGE_NON_RECURRING_EXPENSES);
			reason.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			reasons.add(reason);
		}
		return reasons;
	}
	
}
