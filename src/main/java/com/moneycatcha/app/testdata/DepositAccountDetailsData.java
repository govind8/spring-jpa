package com.moneycatcha.app.testdata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.DepositAccountDetails;
import com.moneycatcha.app.entity.FeaturesSelected;
import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.entity.DepositAccountDetails.Commission;
import com.moneycatcha.app.entity.DepositAccountDetails.SourceOfInitialDeposit;
import com.moneycatcha.app.entity.FeaturesSelected.ExtraFeature;
import com.moneycatcha.app.model.AccountHoldingList;
import com.moneycatcha.app.model.CommissionStructureList;
import com.moneycatcha.app.model.DepositAccountTypeList;
import com.moneycatcha.app.model.SourceOfFundsTypeList;
import com.moneycatcha.app.model.StatementCycleList;
import com.moneycatcha.app.model.YesNoList;


public class DepositAccountDetailsData {

	PercentOwnedTypeData percentTypeData = new PercentOwnedTypeData();
	
	DocumentationInstructionsTypeData documentData = new DocumentationInstructionsTypeData();
	
	FinancialAccountTypeData accountTypeData = new FinancialAccountTypeData();
	
	// List - DepositAccountDetails
	public List<DepositAccountDetails> deposit() {
		List<DepositAccountDetails> deposit = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			deposit.add(new DepositAccountDetails());
		}
		for(int i=0; i < deposit.size(); i++)  {
			deposit.get(i).setInitialDepositAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 12)));
			deposit.get(i).setOriginatorReferenceID(RandomStringUtils.randomAlphabetic(20));
			deposit.get(i).setProductCode(RandomStringUtils.randomAlphanumeric(15));		
			deposit.get(i).setProductName(RandomStringUtils.randomAlphabetic(70));
			deposit.get(i).setProposedAnnualInterestRate(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));			
			deposit.get(i).setStatementCycle(StatementCycleList.HALF_YEARLY);			
			deposit.get(i).setType(DepositAccountTypeList.TRANSACTIONAL);			
			deposit.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			deposit.get(i).setAccountOwners(percentTypeData.percentOwnership());
			deposit.get(i).setCommission(commission());
			deposit.get(i).setDocumentationInstructions(documentData.document());
			deposit.get(i).setFeaturesSelected(featuresSelected());
			deposit.get(i).set_package(setpackage());
			deposit.get(i).setSourceOfInitialDeposit(sourceOfInitialDeposit());
			deposit.get(i).setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
		}
		return deposit;
	}

	
	// Set Commisssion
	public Commission commission() {
		Commission commission = new Commission();
		commission.setCommissionAmount(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
		commission.setCommissionPaid(YesNoList.YES);
		commission.setCommissionStructure(CommissionStructureList.UP_FRONT_ONLY);
		commission.setOtherCommissionStructureDescription(RandomStringUtils.randomAlphabetic(150));
		commission.setPromotionCode(RandomStringUtils.randomAlphanumeric(15));
		commission.setThirdPartyReferee(YesNoList.YES);
		commission.setTrail(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
	
		return commission;
	}
	
	// List - FeaturesSelected
	public List<FeaturesSelected> featuresSelected() {
		List<FeaturesSelected> selected = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			selected.add(new FeaturesSelected());
		}
		for(int i=0; i < selected.size(); i++)  {
			selected.get(i).setChequeBook(YesNoList.YES);
			selected.get(i).setCreditCard(YesNoList.YES);
			selected.get(i).setDebitCard(YesNoList.NO);
			selected.get(i).setDepositBook(YesNoList.YES);
			selected.get(i).setEftposCard(YesNoList.YES);
			selected.get(i).setExtraFeature(extraFeature());
			selected.get(i).setHolidayLeave(YesNoList.NO);
			selected.get(i).setOffset(YesNoList.NO);
			selected.get(i).setOffsetAccount(offset());
			selected.get(i).setOffsetPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 10)));
			selected.get(i).setParentalLeave(YesNoList.YES);
			selected.get(i).setPartialOffset(YesNoList.YES);
			selected.get(i).setPortability(YesNoList.YES);
			selected.get(i).setProgressiveDraw(YesNoList.YES);
			selected.get(i).setRateLock(YesNoList.YES);
			selected.get(i).setRedraw(YesNoList.YES);
			selected.get(i).setSplitLoan(YesNoList.YES);
			selected.get(i).setStatementEmailDelivery(YesNoList.YES);
			selected.get(i).setStatementPaperDelivery(YesNoList.YES);
			selected.get(i).setXApplicant(RandomStringUtils.randomAlphabetic(15));
	
		}
		return selected;
	}
	
	// List - FeaturesSelected
	public List<ExtraFeature> extraFeature() {
		List<ExtraFeature> extra = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			extra.add(new ExtraFeature());
		}
		for(int i=0; i < extra.size(); i++)  {
			extra.get(i).setDescription(RandomStringUtils.randomAlphanumeric(15));
			extra.get(i).setName(RandomStringUtils.randomAlphanumeric(15));
			extra.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
		}
		return extra;
	}
	
	// List - Offset
	public List<OffsetAccount> offset() {
		List<OffsetAccount> offset = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			offset.add(new OffsetAccount());
		}
		for(int i=0; i < offset.size(); i++)  {
			offset.get(i).setAccountNumber(accountTypeData.accountNumber());
			offset.get(i).setIsExisting(YesNoList.YES);
			offset.get(i).setHolding(AccountHoldingList.JOINT);
			offset.get(i).setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			offset.get(i).setXAccount(RandomStringUtils.randomAlphanumeric(15));
		}
		return offset;
	}
	// DepositAccountDetail Package
	public DepositAccountDetails.Package setpackage() {
		DepositAccountDetails.Package p = new DepositAccountDetails.Package();
		p.setCategory(RandomStringUtils.randomAlphabetic(15));
		p.setCode(RandomStringUtils.randomAlphabetic(15));
		p.setMemberID(RandomStringUtils.randomAlphanumeric(15));
		p.setName(RandomStringUtils.randomAlphabetic(15));
		p.setOptionCode(RandomStringUtils.randomAlphabetic(15));
		p.setOrganisation(RandomStringUtils.randomAlphabetic(15));
		return p;
	}
	
	// List Source of Intial Deposit
	public List<SourceOfInitialDeposit> sourceOfInitialDeposit() {
		List<SourceOfInitialDeposit> sources = new ArrayList<>();
		for(int i=1; i < 4; i++) {
			sources.add(new SourceOfInitialDeposit());
		}
		for(int i=0; i < sources.size(); i++)  {
			sources.get(i).setDetails(RandomStringUtils.randomAlphanumeric(15));
			sources.get(i).setPercentage(new BigDecimal(RandomStringUtils.randomNumeric(2, 2)));
			sources.get(i).setType(SourceOfFundsTypeList.EMPLOYMENT_INCOME);
		}
		return sources;	
	}
}
