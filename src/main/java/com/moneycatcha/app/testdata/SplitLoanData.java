package com.moneycatcha.app.testdata;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

import com.moneycatcha.app.entity.SplitLoan;
import com.moneycatcha.app.entity.SplitLoan.SplitLoanComponent;

import lombok.Data;

@Data
public class SplitLoanData {

	
	// SplitLoan
	public List<SplitLoan> splitLoan() {
		
		List<SplitLoan> splitLoan = new ArrayList<>();
		for(int i=0; i < 3; i++) {
			SplitLoan loan = new SplitLoan();
			loan.setUniqueID(RandomStringUtils.randomAlphanumeric(15));
			loan.setSplitLoanComponent(splitLoanComponent());// object
			loan.setSequenceNumber(new BigInteger(RandomStringUtils.randomNumeric(15)));
			splitLoan.add(loan);
		}
		return splitLoan;
	}
	
	// SplitLoanComponent
	public List<SplitLoanComponent> splitLoanComponent() {
		List<SplitLoanComponent> splits = new ArrayList<>();
		for (int i=0; i < 3; i++) {
			SplitLoanComponent loan = new SplitLoanComponent();
			loan.setXLiability(RandomStringUtils.randomAlphanumeric(15));
			loan.setXLoanDetails(RandomStringUtils.randomAlphanumeric(15));
			splits.add(loan);
		}
		return splits;
	}
}
