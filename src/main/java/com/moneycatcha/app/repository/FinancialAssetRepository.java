package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FinancialAsset;

@Repository
public interface FinancialAssetRepository extends CrudRepository<FinancialAsset, Long> {

}