package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ResponsibleLendingType.SignificantChange;

@Repository
public interface SignificantChangeRepository extends CrudRepository<SignificantChange, Long> {

}
