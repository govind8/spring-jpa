package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.VendorTaxInvoiceType.Asset;

@Repository
public interface AssetRepository extends CrudRepository<Asset, Long> {

}
