package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PersonApplicant;

@Repository
public interface PersonApplicantRepository extends CrudRepository<PersonApplicant, Long> {

	@Query(value = "select * from person_applicant where uniqueid = :uniqueId", nativeQuery = true)
	List<PersonApplicant> findPersonApplicantByUniqueID(@Param("uniqueId") String uniqueId);

	@Query(value = "select * from person_applicant where country_of_birth = :countrycode", nativeQuery = true)
	List<PersonApplicant> findPersonApplicantByCountry(@Param("countrycode") String countrycode);

}
