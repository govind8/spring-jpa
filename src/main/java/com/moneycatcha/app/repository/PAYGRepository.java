package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PAYG;

@Repository
public interface PAYGRepository extends CrudRepository<PAYG, Long> {

}
