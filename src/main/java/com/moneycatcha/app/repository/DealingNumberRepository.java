package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DealingNumberType;

@Repository
public interface DealingNumberRepository extends CrudRepository<DealingNumberType, Long> {

}
