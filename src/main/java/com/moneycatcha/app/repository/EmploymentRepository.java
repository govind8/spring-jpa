package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Employment;

@Repository
public interface EmploymentRepository extends CrudRepository<Employment, Long> {

}
