package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RelatedLegalEntities;

@Repository
public interface RelatedLegalEntitiesRepository extends CrudRepository<RelatedLegalEntities, Long> {

}
