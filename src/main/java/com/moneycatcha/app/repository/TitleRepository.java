package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Title;

@Repository
public interface TitleRepository extends CrudRepository<Title, Long> {

}
