package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FinancialAccountType;

@Repository
public interface FinancialAccountTypeRepository extends CrudRepository<FinancialAccountType, Long> {

}
