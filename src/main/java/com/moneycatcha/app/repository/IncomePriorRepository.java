package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.IncomePrior;

@Repository
public interface IncomePriorRepository extends CrudRepository<IncomePrior, Long>{

}
