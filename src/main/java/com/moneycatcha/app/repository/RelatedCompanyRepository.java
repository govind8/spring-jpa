package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RelatedCompany;

@Repository
public interface RelatedCompanyRepository extends CrudRepository<RelatedCompany, Long> {

	@Query(value = "SELECT * FROM related_company where uniqueid = :uniqueId", nativeQuery = true)
	List<RelatedCompany> findRelatedCompanyByUniqueID(@Param("uniqueId") String uniqueId);

}
