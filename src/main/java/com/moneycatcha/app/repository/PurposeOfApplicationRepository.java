package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.NeedsAnalysis.PurposeOfApplication;

@Repository
public interface PurposeOfApplicationRepository extends CrudRepository<PurposeOfApplication, Long> {

}