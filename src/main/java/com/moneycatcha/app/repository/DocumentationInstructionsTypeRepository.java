package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DocumentationInstructionsType;

@Repository
public interface DocumentationInstructionsTypeRepository extends CrudRepository<DocumentationInstructionsType, Long> {

}
