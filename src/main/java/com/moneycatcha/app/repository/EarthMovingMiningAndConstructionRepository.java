package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.EarthMovingMiningAndConstruction;

@Repository
public interface EarthMovingMiningAndConstructionRepository	extends CrudRepository<EarthMovingMiningAndConstruction, Long> {

}
