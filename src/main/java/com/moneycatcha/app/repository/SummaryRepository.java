package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Summary;

@Repository
public interface SummaryRepository extends CrudRepository<Summary, Long> {

}
