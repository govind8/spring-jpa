package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Interview;

@Repository
public interface InterviewRepository extends CrudRepository<Interview, Long> {

}
