package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Settlement;

@Repository
public interface SettlementSecurityRepository extends CrudRepository<Settlement.Security, Long> {

}
