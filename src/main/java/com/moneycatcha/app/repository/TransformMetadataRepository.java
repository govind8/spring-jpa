package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TransformMetadata;

@Repository
public interface TransformMetadataRepository extends CrudRepository<TransformMetadata, Long> {


}
