package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LendingGuarantee;

@Repository
public interface LendingGuaranteeRepository extends CrudRepository<LendingGuarantee, Long> {

	@Query(value = "SELECT * FROM lending_guarantee where uniqueid = :uniqueId", nativeQuery = true)
	List<LendingGuarantee> findLendingGuaranteeByUniqueID(@Param("uniqueId") String uniqueId);

}
