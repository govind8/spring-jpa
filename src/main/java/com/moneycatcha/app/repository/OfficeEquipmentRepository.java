package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.OfficeEquipment;

@Repository
public interface OfficeEquipmentRepository extends CrudRepository<OfficeEquipment, Long> {

}
