package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ConstructionDetails.ConstructionStage;

@Repository
public interface ConstructionStageRepository extends CrudRepository<ConstructionStage, Long> {

}
