package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CustomerTransactionAnalysis.CategorySet;

@Repository
public interface CategorySetRepository extends CrudRepository<CategorySet, Long> {

}
