package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SignatureType;

@Repository
public interface SignatureTypeRepository extends CrudRepository<SignatureType, Long> {

}
