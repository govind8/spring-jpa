package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TrustApplicant.Beneficiary;

@Repository
public interface TrustApplicantBeneficiaryRepository extends CrudRepository<Beneficiary, Long> {

}
