package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Encumbrance;

@Repository
public interface EncumbranceRepository extends CrudRepository<Encumbrance, Long> {

}
