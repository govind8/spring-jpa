package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset.RestrictionOnUseOfLand;

@Repository
public interface RestrictionOnUseOfLandRepository extends CrudRepository<RestrictionOnUseOfLand, Long> {

}
