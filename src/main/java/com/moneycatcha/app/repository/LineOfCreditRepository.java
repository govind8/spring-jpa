package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentTypeDetails.LineOfCredit;

@Repository
public interface LineOfCreditRepository extends CrudRepository<LineOfCredit, Long> {

}