package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.OffsetAccount;

@Repository
public interface OffsetAccountRepository extends CrudRepository<OffsetAccount, Long> {

}
