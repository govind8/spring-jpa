package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PowerOfAttorney;
@Repository
public interface PowerOfAttorneyRepository extends CrudRepository<PowerOfAttorney, Long> {

}
