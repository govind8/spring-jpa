package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RelatedPerson;

@Repository
public interface RelatedPersonRepository extends CrudRepository<RelatedPerson, Long> {

	@Query(value = "SELECT * FROM related_person  where uniqueid = :uniqueId", nativeQuery = true)
	List<RelatedPerson> findRelatedPersonByUniqueID(@Param("uniqueId") String uniqueId);

}
