package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Features;

@Repository
public interface FeaturesRepository extends CrudRepository<Features, Long> {

}
