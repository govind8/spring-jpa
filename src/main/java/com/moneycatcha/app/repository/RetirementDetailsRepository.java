package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RetirementDetails;

@Repository
public interface RetirementDetailsRepository extends CrudRepository<RetirementDetails, Long> {

}
