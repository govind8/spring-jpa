package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Repayment;

@Repository
public interface RepaymentRepository extends CrudRepository<Repayment, Long> {

}
