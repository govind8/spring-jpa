package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TrustDeedVariation;

@Repository
public interface TrustDeedVariationRepository extends CrudRepository<TrustDeedVariation, Long> {

}
