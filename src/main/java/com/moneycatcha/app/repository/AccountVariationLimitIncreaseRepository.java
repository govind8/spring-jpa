package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AccountVariation.LimitIncrease;

@Repository
public interface AccountVariationLimitIncreaseRepository extends CrudRepository<LimitIncrease, Long> {

}