package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Update;

@Repository
public interface UpdateRepository extends CrudRepository<Update, Long> {

}
