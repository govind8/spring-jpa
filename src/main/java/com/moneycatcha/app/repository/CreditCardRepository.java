package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CreditCard;

@Repository
public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {

	@Query(value = "SELECT * FROM credit_card where uniqueid = :uniqueId", nativeQuery = true)
	List<CreditCard> findCreditCardByUniqueID(@Param("uniqueId") String uniqueId);

}
