package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PropertyPart;

@Repository
public interface PropertyPartRepository extends CrudRepository<PropertyPart, Long> {

}
