package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestInAdvance;

@Repository
public interface InterestInAdvanceRepository extends CrudRepository<InterestInAdvance, Long> {

}