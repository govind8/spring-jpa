package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.IncomeRecent;

@Repository
public interface IncomeRecentRepository extends CrudRepository<IncomeRecent, Long> {

}
