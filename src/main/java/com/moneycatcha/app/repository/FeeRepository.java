package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Fee;

@Repository
public interface FeeRepository extends CrudRepository<Fee, Long> {

}
