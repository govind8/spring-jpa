package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DeclaredIncome;

@Repository
public interface DeclaredIncomeRepository extends CrudRepository<DeclaredIncome, Long> {

}
