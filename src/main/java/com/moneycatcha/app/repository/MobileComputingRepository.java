package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.MobileComputing;

@Repository
public interface MobileComputingRepository extends CrudRepository<MobileComputing, Long> {

}
