package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProductSet;

@Repository
public interface ProductSetRepository extends CrudRepository<ProductSet, Long> {

}
