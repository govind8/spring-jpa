package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Security;

@Repository
public interface SecurityRepository extends CrudRepository<Security, Long> {

}
