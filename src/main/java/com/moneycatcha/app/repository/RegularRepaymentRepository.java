package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProposedRepayment.RegularRepayment;

@Repository
public interface RegularRepaymentRepository extends CrudRepository<RegularRepayment, Long> {

	@Query(value = "SELECT * FROM regular_repayment where uniqueid = :uniqueId", nativeQuery = true)
	List<RegularRepayment> findRegularRepaymentByUniqueID(@Param("uniqueId") String uniqueId);

}
