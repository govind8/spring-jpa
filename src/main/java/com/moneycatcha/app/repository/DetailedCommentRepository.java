package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DetailedComment;

@Repository
public interface DetailedCommentRepository extends CrudRepository<DetailedComment, Long> {

}
