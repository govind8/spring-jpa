package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.MedicalEquipment;

@Repository
public interface MedicalEquipmentRepository extends CrudRepository<MedicalEquipment, Long> {

}
