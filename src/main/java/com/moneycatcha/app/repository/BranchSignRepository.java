package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Overview.BranchSign;

@Repository
public interface BranchSignRepository extends CrudRepository<BranchSign, Long> {

}
