package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Submit;

@Repository
public interface SubmitRepository extends CrudRepository<Submit, Long> {

}
