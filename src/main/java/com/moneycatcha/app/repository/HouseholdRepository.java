package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Household;

@Repository
public interface HouseholdRepository extends CrudRepository<Household, Long> {

	@Query(value = "SELECT * FROM household where uniqueid = :uniqueId", nativeQuery = true)
	List<Household> findHouseholdByUniqueID(@Param("uniqueId") String uniqueId);

}
