package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentOptions.SavingsDetails;

@Repository
public interface SavingsDetailsRepository extends CrudRepository<SavingsDetails, Long> {

}