package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.moneycatcha.app.entity.PriorAddress;

public interface PriorAddressRepository extends CrudRepository<PriorAddress, Long> {

}
