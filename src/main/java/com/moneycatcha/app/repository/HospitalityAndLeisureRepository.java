package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.HospitalityAndLeisure;

@Repository
public interface HospitalityAndLeisureRepository extends CrudRepository<HospitalityAndLeisure, Long> {

}
