package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Director;

@Repository
public interface DirectorRepository extends CrudRepository<Director, Long> {

}
