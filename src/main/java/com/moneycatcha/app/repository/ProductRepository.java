package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProductSet.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

}
