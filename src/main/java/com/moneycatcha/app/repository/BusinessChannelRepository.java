package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.moneycatcha.app.entity.BusinessChannel;

public interface BusinessChannelRepository extends CrudRepository<BusinessChannel, Long> {

}
