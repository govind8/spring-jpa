package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PropertyFeatures;

@Repository
public interface PropertyFeaturesRepository extends CrudRepository<PropertyFeatures, Long> {

}
