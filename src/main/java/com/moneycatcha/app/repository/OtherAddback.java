package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Addback;

@Repository
public interface OtherAddback extends CrudRepository<Addback.OtherAddback, Long> {

}
