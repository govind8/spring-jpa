package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RegistrationEvent;

@Repository
public interface RegistrationEventRepository extends CrudRepository<RegistrationEvent, Long> {

}
