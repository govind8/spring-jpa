package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PPSR;

@Repository
public interface PPSRRepository extends CrudRepository<PPSR, Long> {

}
