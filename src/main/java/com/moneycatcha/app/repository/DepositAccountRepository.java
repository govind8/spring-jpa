package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DepositAccount;

@Repository
public interface DepositAccountRepository extends CrudRepository<DepositAccount, Long> {

	@Query(value = "SELECT * FROM deposit_account where uniqueid = :uniqueId", nativeQuery = true)
	List<DepositAccount> findDepositAccountByUniqueID(@Param("uniqueId") String uniqueId);

}
