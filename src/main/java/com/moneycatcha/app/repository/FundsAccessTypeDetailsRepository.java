package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FundsAccessTypeDetails;



@Repository
public interface FundsAccessTypeDetailsRepository extends CrudRepository<FundsAccessTypeDetails, Long> {

}
