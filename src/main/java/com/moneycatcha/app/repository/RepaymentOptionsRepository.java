package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentOptions;

@Repository
public interface RepaymentOptionsRepository extends CrudRepository<RepaymentOptions, Long> {

}
