package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Insurance.InsuredParty;

@Repository
public interface InsuredPartyRepository extends CrudRepository<InsuredParty, Long> {

}
