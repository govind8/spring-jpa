package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Privacy;
@Repository
public interface PrivacyRepository extends CrudRepository<Privacy, Long> {

}
