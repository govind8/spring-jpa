package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.NonRealEstateAsset;

@Repository
public interface NonRealEstateAssetInsuranceRepository extends CrudRepository<NonRealEstateAsset.Insurance, Long> {

}
