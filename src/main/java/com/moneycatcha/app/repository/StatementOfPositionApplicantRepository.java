package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.StatementOfPosition;

@Repository
public interface StatementOfPositionApplicantRepository extends CrudRepository<StatementOfPosition.Applicant, Long> {

}
