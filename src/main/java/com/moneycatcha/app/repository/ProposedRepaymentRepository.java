package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProposedRepayment;

@Repository
public interface ProposedRepaymentRepository extends CrudRepository<ProposedRepayment, Long> {

}
