package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails.StatementInstructions;

@Repository
public interface StatementInstructionsRepository extends CrudRepository<StatementInstructions, Long> {

}
