package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.NonRealEstateAsset;

@Repository
public interface NonRealEstateAssetRepository extends CrudRepository<NonRealEstateAsset, Long> {

	@Query(value = "SELECT * FROM non_real_estate_asset where uniqueid = :uniqueId", nativeQuery = true)
	List<NonRealEstateAsset> findNonRealEstateAssetByUniqueID(@Param("uniqueId") String uniqueId);

}
