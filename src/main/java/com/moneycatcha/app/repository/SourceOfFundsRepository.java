package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PersonApplicant.SourceOfFunds;



@Repository
public interface SourceOfFundsRepository extends CrudRepository<SourceOfFunds, Long> {

}
