package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Registration;

@Repository
public interface RegistrationRepository extends CrudRepository<Registration, Long> {

}
