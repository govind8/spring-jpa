package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.moneycatcha.app.entity.NeedsAnalysis;

public interface NeedsAnalysisRepository extends CrudRepository<NeedsAnalysis, Long> {

	@Query(value = "SELECT * FROM needs_analysis where uniqueid = :uniqueId", nativeQuery = true)
	List<NeedsAnalysis> findNeedsAnalysisByUniqueID(@Param("uniqueId") String uniqueId);

}
