package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CompanyFinancials;

@Repository
public interface CompanyFinancialsRepository extends CrudRepository<CompanyFinancials, Long> {

	@Query(value = "SELECT * FROM company_financials where uniqueid = :uniqueId", nativeQuery = true)
	List<CompanyFinancials> findCompanyFinancialsByUniqueID(@Param("uniqueId") String uniqueId);

}
