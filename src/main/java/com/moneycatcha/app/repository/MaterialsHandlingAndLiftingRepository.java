package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.MaterialsHandlingAndLifting;

@Repository
public interface MaterialsHandlingAndLiftingRepository extends CrudRepository<MaterialsHandlingAndLifting, Long> {

}
