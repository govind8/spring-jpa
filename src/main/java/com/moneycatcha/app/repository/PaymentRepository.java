package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProposedRepayment.StructuredPayments.Payment;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Long> {

	@Query(value = "SELECT * FROM payment where uniqueid = :uniqueId", nativeQuery = true)
	List<Payment> findPaymentByUniqueID(@Param("uniqueId") String uniqueId);

}
