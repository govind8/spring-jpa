package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.BusinessIncomePrior;

@Repository
public interface BusinessIncomePriorRepository extends CrudRepository<BusinessIncomePrior, Long> {

}
