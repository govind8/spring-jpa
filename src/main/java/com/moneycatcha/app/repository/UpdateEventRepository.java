package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Update;

@Repository
public interface UpdateEventRepository extends CrudRepository<Update.Event, Long> {

}
