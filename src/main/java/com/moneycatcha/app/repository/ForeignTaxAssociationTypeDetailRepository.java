package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ForeignTaxAssociationType;

@Repository
public interface ForeignTaxAssociationTypeDetailRepository
		extends CrudRepository<ForeignTaxAssociationType.Detail, Long> {

}
