package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Interview.Attendee;

@Repository
public interface AttendeeRepository extends CrudRepository<Attendee, Long> {

}
