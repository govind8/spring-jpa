package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Content;

@Repository
public interface ContentRepository extends CrudRepository<Content, Long> {

}
