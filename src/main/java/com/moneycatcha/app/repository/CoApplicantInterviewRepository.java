package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CoApplicantInterview;

@Repository
public interface CoApplicantInterviewRepository extends CrudRepository<CoApplicantInterview, Long> {

}
