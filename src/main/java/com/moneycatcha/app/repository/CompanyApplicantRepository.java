package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CompanyApplicant;

@Repository
public interface CompanyApplicantRepository extends CrudRepository<CompanyApplicant, Long> {

	@Query(value = "select * from company_applicant where uniqueid = :uniqueId", nativeQuery = true)
	List<CompanyApplicant> findCompanyApplicantByUniqueID(@Param("uniqueId") String uniqueId);

	@Query(value = "select * from company_applicant where company_name = :companyName", nativeQuery = true)
	List<CompanyApplicant> findByCompanyName(@Param("companyName") String companyName);

}
