package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RentalIncome;

@Repository
public interface RentalIncomeRepository extends CrudRepository<RentalIncome, Long> {

}
