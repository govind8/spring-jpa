package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Overview.BridgingFinance;

@Repository
public interface BridgingFinanceRepository extends CrudRepository<BridgingFinance, Long> {

}
