package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Declarations;

@Repository
public interface DeclarationsRepository extends CrudRepository<Declarations, Long> {

}
