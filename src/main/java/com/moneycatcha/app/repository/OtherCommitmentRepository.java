package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Household.ExpenseDetails.OtherCommitment;

@Repository
public interface OtherCommitmentRepository extends CrudRepository<OtherCommitment, Long> {

}
