package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PostSettlementAddress;

@Repository
public interface PostSettlementAddressRepository extends CrudRepository<PostSettlementAddress, Long> {

}
