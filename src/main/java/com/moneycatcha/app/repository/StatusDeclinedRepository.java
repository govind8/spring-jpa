package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Update.Status;

@Repository
public interface StatusDeclinedRepository extends CrudRepository<Status.Declined, Long> {

}
