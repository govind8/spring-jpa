package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FinancialAnalysis.CompanyFinancials;

@Repository
public interface FinancialAnalysisCompanyFinanicalsRepository
		extends CrudRepository<CompanyFinancials, Long> {

}
