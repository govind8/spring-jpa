package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Aircraft;

@Repository
public interface AircraftRepository extends CrudRepository<Aircraft, Long> {

}
