package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Condition;

@Repository
public interface ConditionProductRepository extends CrudRepository<Condition.Product, Long> {

}
