package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FinancialAnalysis;

@Repository
public interface FinancialAnalysisRepository extends CrudRepository<FinancialAnalysis, Long> {

}
