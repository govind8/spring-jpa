package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ConstructionDetails;

@Repository
public interface ConstructionDetailsRepository extends CrudRepository<ConstructionDetails, Long> {

}
