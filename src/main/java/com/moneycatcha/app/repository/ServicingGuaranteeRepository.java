package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Guarantor.ServicingGuarantee;

@Repository
public interface ServicingGuaranteeRepository extends CrudRepository<ServicingGuarantee, Long> {

}
