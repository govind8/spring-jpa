package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LendingPurpose;

@Repository
public interface LendingPurposeRepository extends CrudRepository<LendingPurpose, Long> {

}
