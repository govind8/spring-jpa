package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Overview.BranchDomicile;

@Repository
public interface BranchDomicileRepository extends CrudRepository<BranchDomicile, Long> {

}
