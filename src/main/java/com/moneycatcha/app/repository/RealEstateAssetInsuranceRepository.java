package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset;

@Repository
public interface RealEstateAssetInsuranceRepository extends CrudRepository<RealEstateAsset.Insurance, Long> {

}
