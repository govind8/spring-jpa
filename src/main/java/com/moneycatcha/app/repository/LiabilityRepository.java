package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Liability;

@Repository
public interface LiabilityRepository extends CrudRepository<Liability, Long> {

	@Query(value = "SELECT * FROM liability where uniqueid = :uniqueId", nativeQuery = true)
	List<Liability> findLiabilityByUniqueID(@Param("uniqueId") String uniqueId);

}
