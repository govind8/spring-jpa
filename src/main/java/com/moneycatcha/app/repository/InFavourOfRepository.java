package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Encumbrance.InFavourOf;

@Repository
public interface InFavourOfRepository extends CrudRepository<InFavourOf, Long> {

}
