package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedAndVariableRate;

@Repository
public interface FixedAndVariableRateRepository extends CrudRepository<FixedAndVariableRate, Long> {

}