package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.MasterAgreement;

@Repository
public interface MasterAgreementRepository extends CrudRepository<MasterAgreement, Long> {

	@Query(value = "SELECT * FROM master_agreement where uniqueid = :uniqueId", nativeQuery = true)
	List<MasterAgreement> findMasterAgreementByUniqueID(@Param("uniqueId") String uniqueId);

}
