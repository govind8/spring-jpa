package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Publisher.RelatedSoftware;

@Repository
public interface RelatedSoftwareRepository extends CrudRepository<RelatedSoftware, Long> {

}
