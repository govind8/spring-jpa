package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CreditHistory;

@Repository
public interface CreditHistoryRepository extends CrudRepository<CreditHistory, Long> {

}
