package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DiscountMargin;

@Repository
public interface DiscountMarginRepository extends CrudRepository<DiscountMargin, Long> {

}
