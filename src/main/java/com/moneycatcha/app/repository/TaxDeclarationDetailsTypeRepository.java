package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TaxDeclarationDetailsType;

@Repository
public interface TaxDeclarationDetailsTypeRepository extends CrudRepository<TaxDeclarationDetailsType, Long> {

}
