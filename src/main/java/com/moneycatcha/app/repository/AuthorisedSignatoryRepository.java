package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AuthorisedSignatory;

@Repository
public interface AuthorisedSignatoryRepository extends CrudRepository<AuthorisedSignatory, Long> {
	
}
