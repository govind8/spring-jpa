package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.BusinessIncomePrevious;

@Repository
public interface BusinessIncomePreviousRepository extends CrudRepository<BusinessIncomePrevious, Long> {

}
