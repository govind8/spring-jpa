package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.MotorVehicle;

@Repository
public interface MotorVehicleRepository extends CrudRepository<MotorVehicle, Long> {

}
