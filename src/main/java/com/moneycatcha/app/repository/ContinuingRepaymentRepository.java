package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Liability.ContinuingRepayment;

@Repository
public interface ContinuingRepaymentRepository extends CrudRepository<ContinuingRepayment, Long> {

	@Query(value = "SELECT * FROM continuing_repayment where uniqueid = :uniqueId", nativeQuery = true)
	List<ContinuingRepayment> findContinuingRepaymentByUniqueID(@Param("uniqueId") String uniqueId);

}
