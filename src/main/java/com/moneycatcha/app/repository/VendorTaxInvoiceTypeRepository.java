package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.VendorTaxInvoiceType;

@Repository
public interface VendorTaxInvoiceTypeRepository extends CrudRepository<VendorTaxInvoiceType, Long> {

	@Query(value = "SELECT * FROM vendor_tax_invoice_type where uniqueid = :uniqueId", nativeQuery = true)
	List<VendorTaxInvoiceType> findVendorTaxInvoiceByUniqueID(@Param("uniqueId") String uniqueId);

}
