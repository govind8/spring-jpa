package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SaleOfAssets;

@Repository
public interface SaleOfAssetsRepository extends CrudRepository<SaleOfAssets, Long> {

}
