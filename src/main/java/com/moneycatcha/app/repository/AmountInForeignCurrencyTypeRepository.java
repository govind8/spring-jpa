package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AmountInForeignCurrencyType;

@Repository
public interface AmountInForeignCurrencyTypeRepository extends CrudRepository<AmountInForeignCurrencyType, Long> {

}