package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.IncomePrevious;

@Repository
public interface IncomePreviousRepository extends CrudRepository<IncomePrevious, Long> {

}
