package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Guarantor;

@Repository
public interface GuarantorRepository extends CrudRepository<Guarantor, Long> {

}
