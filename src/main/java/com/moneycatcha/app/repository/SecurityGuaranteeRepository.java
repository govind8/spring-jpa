package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Guarantor.SecurityGuarantee;

@Repository
public interface SecurityGuaranteeRepository extends CrudRepository<SecurityGuarantee, Long> {

}
