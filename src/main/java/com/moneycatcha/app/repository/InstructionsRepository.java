package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Instructions;

@Repository
public interface InstructionsRepository extends CrudRepository<Instructions, Long> {

}
