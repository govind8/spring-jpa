package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AccountVariation.ReduceLimit;

@Repository
public interface AccountVariationReduceLimitRepository extends CrudRepository<ReduceLimit, Long> {

}