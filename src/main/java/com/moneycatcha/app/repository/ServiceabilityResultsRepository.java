package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ServiceabilityResults;

@Repository
public interface ServiceabilityResultsRepository extends CrudRepository<ServiceabilityResults, Long> {

}
