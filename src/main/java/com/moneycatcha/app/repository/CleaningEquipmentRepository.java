package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CleaningEquipment;

@Repository
public interface CleaningEquipmentRepository extends CrudRepository<CleaningEquipment, Long> {

}
