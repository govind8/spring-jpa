package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Company;

@Repository
public interface CompanyRepository extends CrudRepository<Company, Long> {
	
	@Query(value = "SELECT * FROM company where uniqueid = :uniqueId", nativeQuery = true)
	List<Company> findCompanyByUniqueID(@Param("uniqueId") String uniqueId);

}
