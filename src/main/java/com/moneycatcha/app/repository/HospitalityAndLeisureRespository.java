package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.HospitalityAndLeisure;

@Repository
public interface HospitalityAndLeisureRespository extends CrudRepository<HospitalityAndLeisure, Long> {

}
