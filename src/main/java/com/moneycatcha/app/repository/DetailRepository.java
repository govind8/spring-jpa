package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ForeignTaxAssociationType.Detail;

@Repository
public interface DetailRepository extends CrudRepository<Detail, Long> {

}
