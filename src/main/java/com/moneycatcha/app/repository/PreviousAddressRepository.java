/**
 * 
 */
package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PreviousAddress;

@Repository
public interface PreviousAddressRepository extends CrudRepository<PreviousAddress, Long> {

}
