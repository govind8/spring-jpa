package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentOptions.RecurringIncomeFromSuperannuationDetails;

@Repository
public interface RecurringIncomeFromSuperannuationDetailsRepository extends CrudRepository<RecurringIncomeFromSuperannuationDetails, Long> {

}