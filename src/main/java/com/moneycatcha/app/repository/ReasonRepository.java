package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Reason;

@Repository
public interface ReasonRepository extends CrudRepository<Reason, Long> {

}