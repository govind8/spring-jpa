package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails;

@Repository
public interface LoanDetailsRepository extends CrudRepository<LoanDetails, Long> {

	@Query(value = "SELECT * FROM loan_details where uniqueid = :uniqueId", nativeQuery = true)
	List<LoanDetails> findLoanDetailsByUniqueID(@Param("uniqueId") String uniqueId);

}
