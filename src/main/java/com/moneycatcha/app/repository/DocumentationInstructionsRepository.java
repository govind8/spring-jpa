package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PersonApplicant;

@Repository
public interface DocumentationInstructionsRepository
		extends CrudRepository<PersonApplicant.DocumentationInstructions, Long> {

}
