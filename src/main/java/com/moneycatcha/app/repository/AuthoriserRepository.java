package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProposedRepayment.Authoriser;

@Repository
public interface AuthoriserRepository extends CrudRepository<Authoriser, Long> {

}
