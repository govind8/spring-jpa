package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Message;

@Repository
public interface MessageRepository extends CrudRepository<Message, Long> {

	
}
