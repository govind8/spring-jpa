package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DSH;

@Repository
public interface DSHRepository extends CrudRepository<DSH, Long> {

}
