package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AgriculturalAsset;

@Repository
public interface AgriculturalAssetRepository extends CrudRepository<AgriculturalAsset, Long> {

}
