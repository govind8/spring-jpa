package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Attachment;

@Repository
public interface AttachmentRepository extends CrudRepository<Attachment, Long> {

	@Query(value = "SELECT * FROM attachment where uniqueid = :uniqueId", nativeQuery = true)
	List<Attachment> findAttachmentByUniqueID(@Param("uniqueId") String uniqueId);

}
