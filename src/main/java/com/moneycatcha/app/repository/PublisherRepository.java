package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Publisher;

@Repository
public interface PublisherRepository extends CrudRepository<Publisher, Long> {

}
