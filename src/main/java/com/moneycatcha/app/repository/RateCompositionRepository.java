package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RateComposition;

@Repository
public interface RateCompositionRepository extends CrudRepository<RateComposition, Long> {

	@Query(value = "SELECT * FROM rate_composition where uniqueid = :uniqueId", nativeQuery = true)
	List<RateComposition> findRateCompositionByUniqueID(@Param("uniqueId") String uniqueId);

}
