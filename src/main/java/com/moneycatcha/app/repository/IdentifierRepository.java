package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TransformMetadata.Identifier;

@Repository
public interface IdentifierRepository extends CrudRepository<Identifier, Long> {

	@Query(value = "SELECT * FROM identifier where uniqueid = :uniqueId", nativeQuery = true)
	List<Identifier> findIdentifierByUniqueID(@Param("uniqueId") String uniqueId);

}
