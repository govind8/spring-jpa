package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SalesChannel;

@Repository
public interface SalesChannelRepository extends CrudRepository<SalesChannel, Long> {

}
