package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanWriter;

@Repository
public interface LoanWriterRepository extends CrudRepository<LoanWriter, Long> {

}
