package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails;

@Repository
public interface SuperannuationLumpSumFollowingRetirementDetailsRepository extends CrudRepository<SuperannuationLumpSumFollowingRetirementDetails, Long> {

}