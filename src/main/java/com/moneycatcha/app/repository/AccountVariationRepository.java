package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AccountVariation;

@Repository
public interface AccountVariationRepository extends CrudRepository<AccountVariation, Long> {

}
