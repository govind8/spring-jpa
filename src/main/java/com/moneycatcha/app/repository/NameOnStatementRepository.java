package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails.StatementInstructions.NameOnStatement;

@Repository
public interface NameOnStatementRepository extends CrudRepository<NameOnStatement, Long> {

}
