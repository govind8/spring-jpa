package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset.VisitContact;

@Repository
public interface VisitContactRepository extends CrudRepository<VisitContact, Long> {

}
