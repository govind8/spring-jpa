package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.NotEmployed;

@Repository
public interface NotEmployedRepository extends CrudRepository<NotEmployed, Long> {

}
