package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RefinancingAndConsolidation;

@Repository
public interface RefinancingAndConsolidationRepository extends CrudRepository<RefinancingAndConsolidation, Long> {

}
