package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.OtherIncome;

@Repository
public interface OtherIncomeRepository extends CrudRepository<OtherIncome, Long> {

}
