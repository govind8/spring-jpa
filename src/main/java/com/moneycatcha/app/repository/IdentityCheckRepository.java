package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.IdentityCheck;
@Repository
public interface IdentityCheckRepository extends CrudRepository<IdentityCheck, Long> {

}
