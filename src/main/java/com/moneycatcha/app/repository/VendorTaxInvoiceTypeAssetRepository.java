package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.VendorTaxInvoiceType;

@Repository
public interface VendorTaxInvoiceTypeAssetRepository extends CrudRepository<VendorTaxInvoiceType.Asset, Long> {

}
