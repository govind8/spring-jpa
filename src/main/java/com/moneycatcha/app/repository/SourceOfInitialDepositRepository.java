package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.DepositAccountDetails.SourceOfInitialDeposit;

@Repository
public interface SourceOfInitialDepositRepository extends CrudRepository<SourceOfInitialDeposit, Long> {

}
