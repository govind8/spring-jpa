package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.BusinessIncomeRecent;

@Repository
public interface BusinessIncomeRecentRepository extends CrudRepository<BusinessIncomeRecent, Long> {

}
