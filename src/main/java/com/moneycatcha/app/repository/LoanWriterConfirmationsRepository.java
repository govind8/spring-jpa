package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Guarantor.LoanWriterConfirmations;

@Repository
public interface LoanWriterConfirmationsRepository extends CrudRepository<LoanWriterConfirmations, Long> {

}
