package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset.Rural;

@Repository
public interface RuralRepository extends CrudRepository<Rural, Long> {

}
