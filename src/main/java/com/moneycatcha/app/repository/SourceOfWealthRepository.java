package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SourceOfWealth;

@Repository
public interface SourceOfWealthRepository extends CrudRepository<SourceOfWealth, Long> {

}
