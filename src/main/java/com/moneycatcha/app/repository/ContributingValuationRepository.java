package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanToValuationRatio.ContributingValuation;

@Repository
public interface ContributingValuationRepository extends CrudRepository<ContributingValuation, Long> {

}
