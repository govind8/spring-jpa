package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails.Term.InterestOnlyReason;

@Repository
public interface InterestOnlyReasonRepository extends CrudRepository<InterestOnlyReason, Long> {

}
