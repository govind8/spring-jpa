package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.moneycatcha.app.entity.AddressType;

public interface AddressTypeRepository extends CrudRepository<AddressType, Long> {

	@Query(value = "select * from address_type where uniqueid = :uniqueId", nativeQuery = true)
	List<AddressType> findAddressByUniqueID(@Param("uniqueId") String uniqueId);

	@Query(value = "select * from address_type where city = :cityname", nativeQuery = true)
	List<AddressType> findAddressByCity(@Param("cityname") String cityname);

}
