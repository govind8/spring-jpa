package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ToolsOfTrade;

@Repository
public interface ToolsOfTradeRepository extends CrudRepository<ToolsOfTrade, Long> {

}
