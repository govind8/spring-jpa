package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Overview.BranchStamp;

@Repository
public interface BranchStampRepository extends CrudRepository<BranchStamp, Long> {

}
