package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Commission;

@Repository
public interface CommissionRepository extends CrudRepository<Commission, Long> {

}
