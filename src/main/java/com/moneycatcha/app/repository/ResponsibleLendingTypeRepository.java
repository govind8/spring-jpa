package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ResponsibleLendingType;

@Repository
public interface ResponsibleLendingTypeRepository extends CrudRepository<ResponsibleLendingType, Long> {

}
