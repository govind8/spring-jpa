package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset.PropertyType;

@Repository
public interface PropertyTypeRepository extends CrudRepository<PropertyType, Long> {

}
