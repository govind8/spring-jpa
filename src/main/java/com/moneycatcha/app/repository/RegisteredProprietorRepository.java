package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Title.RegisteredProprietor;

@Repository
public interface RegisteredProprietorRepository extends CrudRepository<RegisteredProprietor, Long> {

}
