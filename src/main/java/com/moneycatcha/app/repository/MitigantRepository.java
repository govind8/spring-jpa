package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant;


@Repository
public interface MitigantRepository extends CrudRepository<Mitigant, Long> {

}
