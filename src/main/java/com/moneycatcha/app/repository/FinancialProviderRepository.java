package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Household.Dependant.FinancialProvider;

@Repository
public interface FinancialProviderRepository extends CrudRepository<FinancialProvider, Long>{

}
