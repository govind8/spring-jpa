package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ContributionFunds;

@Repository
public interface ContributionFundsRepository extends CrudRepository<ContributionFunds, Long> {

}
