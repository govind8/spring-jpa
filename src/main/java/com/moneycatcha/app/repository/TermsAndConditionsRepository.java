package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TermsAndConditions;

@Repository
public interface TermsAndConditionsRepository extends CrudRepository<TermsAndConditions, Long> {

}
