package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SplitLoan;

@Repository
public interface SplitLoanRepository extends CrudRepository<SplitLoan, Long> {

}
