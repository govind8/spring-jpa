package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.EmailAddress;

@Repository
public interface EmailAddressRepository extends CrudRepository<EmailAddress, Long> {

}
