package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PersonApplicant.Insurance;

@Repository
public interface PersonApplicantInsuranceRepository extends CrudRepository<Insurance, Long> {

}