package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails;

@Repository
public interface LoanDetailsGuarantorRepository extends CrudRepository<LoanDetails.Guarantor, Long> {

}
