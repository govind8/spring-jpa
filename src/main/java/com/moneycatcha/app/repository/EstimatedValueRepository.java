package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.EstimatedValue;

@Repository
public interface EstimatedValueRepository extends CrudRepository<EstimatedValue, Long> {

}
