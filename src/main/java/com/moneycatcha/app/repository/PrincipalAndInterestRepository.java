package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RepaymentTypeDetails.PrincipalAndInterest;

@Repository
public interface PrincipalAndInterestRepository extends CrudRepository<PrincipalAndInterest, Long> {

}