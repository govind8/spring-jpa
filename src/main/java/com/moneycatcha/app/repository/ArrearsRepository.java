package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Arrears;

@Repository
public interface ArrearsRepository extends CrudRepository<Arrears, Long> {

}
