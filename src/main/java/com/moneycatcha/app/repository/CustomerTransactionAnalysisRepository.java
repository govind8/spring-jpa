package com.moneycatcha.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.CustomerTransactionAnalysis;

@Repository
public interface CustomerTransactionAnalysisRepository extends CrudRepository<CustomerTransactionAnalysis, Long> {

	@Query(value = "SELECT * FROM customer_transaction_analysis where uniqueid = :uniqueId", nativeQuery = true)
	List<CustomerTransactionAnalysis> findCustomerTransactionAnalysisByUniqueID(@Param("uniqueId") String uniqueId);
	
}
