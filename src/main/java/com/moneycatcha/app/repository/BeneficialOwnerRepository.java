package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.BeneficialOwner;

@Repository
public interface BeneficialOwnerRepository extends CrudRepository<BeneficialOwner, Long> {

}
