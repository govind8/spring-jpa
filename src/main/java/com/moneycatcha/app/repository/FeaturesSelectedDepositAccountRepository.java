package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FeaturesSelected;

@Repository
public interface FeaturesSelectedDepositAccountRepository
		extends CrudRepository<FeaturesSelected.DepositAccount, Long> {

}
