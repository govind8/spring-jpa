package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Instructions.ApplicationInstructions;



@Repository
public interface ApplicationInstructionsRepository extends CrudRepository<ApplicationInstructions, Long> {

}
