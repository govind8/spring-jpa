package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Business;

@Repository
public interface BusinessRepository extends CrudRepository<Business, Long> {

}
