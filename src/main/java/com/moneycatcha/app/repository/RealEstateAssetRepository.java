package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.RealEstateAsset;

@Repository
public interface RealEstateAssetRepository extends CrudRepository<RealEstateAsset, Long> {

	@Query(value = "SELECT * FROM real_estate_asset where uniqueid = :uniqueId", nativeQuery = true)
	List<RealEstateAsset> findRealEstateAssetByUniqueID(@Param("uniqueId") String uniqueId);

}
