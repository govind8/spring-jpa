package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProductPackage;

@Repository
public interface ProductPackageLoanDetailsRepository extends CrudRepository<ProductPackage.LoanDetails, Long> {

}
