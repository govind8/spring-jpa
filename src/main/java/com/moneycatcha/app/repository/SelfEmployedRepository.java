package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.SelfEmployed;

@Repository
public interface SelfEmployedRepository extends CrudRepository<SelfEmployed, Long> {

}
