package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProductPackage;

@Repository
public interface ProductPackageLiabilityRepository extends CrudRepository<ProductPackage.Liability, Long> {

}
