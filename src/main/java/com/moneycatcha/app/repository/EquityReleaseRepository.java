package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanDetails.EquityRelease;

@Repository
public interface EquityReleaseRepository extends CrudRepository<EquityRelease, Long> {

}
