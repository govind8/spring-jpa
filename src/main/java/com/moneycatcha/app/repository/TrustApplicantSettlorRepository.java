package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TrustApplicant;

@Repository
public interface TrustApplicantSettlorRepository extends CrudRepository<TrustApplicant.Settlor, Long>{

}
