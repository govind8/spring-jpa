package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Owner;

@Repository
public interface OwnerRepository extends CrudRepository<Owner, Long> {

}
