package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedRate;

@Repository
public interface FixedRateRepository extends CrudRepository<FixedRate, Long> {

}