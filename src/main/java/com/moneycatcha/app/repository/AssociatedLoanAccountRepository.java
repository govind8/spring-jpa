package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.AssociatedLoanAccount;

@Repository
public interface AssociatedLoanAccountRepository extends CrudRepository<AssociatedLoanAccount, Long> {

}
