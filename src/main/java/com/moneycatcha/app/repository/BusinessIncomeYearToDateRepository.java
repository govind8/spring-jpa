package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.BusinessIncomeYearToDate;

@Repository
public interface BusinessIncomeYearToDateRepository extends CrudRepository<BusinessIncomeYearToDate, Long> {

}
