package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ProposedRepayment.StructuredPayments;

@Repository
public interface StructuredPaymentsRepository extends CrudRepository<StructuredPayments, Long> {


}
