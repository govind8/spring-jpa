package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ExistingCustomer;

@Repository
public interface ExistingCustomerRepository extends CrudRepository<ExistingCustomer, Long> {

}
