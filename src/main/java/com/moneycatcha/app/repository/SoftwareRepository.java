package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Software;

@Repository
public interface SoftwareRepository extends CrudRepository<Software, Long> {

}
