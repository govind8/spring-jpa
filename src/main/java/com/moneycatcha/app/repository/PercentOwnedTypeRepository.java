package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.PercentOwnedType;

@Repository
public interface PercentOwnedTypeRepository extends CrudRepository<PercentOwnedType, Long> {

}
