package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Addback.OtherAddback;

@Repository
public interface OtherAddbackRepository extends CrudRepository<OtherAddback, Long> {

}
