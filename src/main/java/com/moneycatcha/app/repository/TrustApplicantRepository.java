package com.moneycatcha.app.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.TrustApplicant;

@Repository
public interface TrustApplicantRepository extends CrudRepository<TrustApplicant, Long> {

	@Query(value = "SELECT * FROM trust_applicant where uniqueid = :uniqueId", nativeQuery = true)
	List<TrustApplicant> findTrustApplicantByUniqueID(@Param("uniqueId") String uniqueId);

}
