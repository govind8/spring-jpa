package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.ForeignEmployed;

@Repository

public interface ForeignEmployedRepository extends CrudRepository<ForeignEmployed, Long> {

}
