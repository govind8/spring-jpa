package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Aggregator;

@Repository
public interface AggregatorRepository extends CrudRepository<Aggregator, Long> {

}
