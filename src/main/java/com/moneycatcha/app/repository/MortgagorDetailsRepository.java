package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Security.MortgagorDetails;

@Repository
public interface MortgagorDetailsRepository extends CrudRepository<MortgagorDetails, Long> {

}
