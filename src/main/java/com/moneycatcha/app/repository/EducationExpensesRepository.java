package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Household.EducationExpenses;

@Repository
public interface EducationExpensesRepository extends CrudRepository<EducationExpenses, Long> {

}
