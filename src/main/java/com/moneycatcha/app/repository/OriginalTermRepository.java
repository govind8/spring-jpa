package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Liability.OriginalTerm;

@Repository
public interface OriginalTermRepository extends CrudRepository<OriginalTerm, Long> {

}
