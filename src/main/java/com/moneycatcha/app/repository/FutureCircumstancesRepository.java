package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FutureCircumstances;



@Repository
public interface FutureCircumstancesRepository extends CrudRepository<FutureCircumstances, Long> {

}
