package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.Liability;

@Repository
public interface PercentOwnedRepository extends CrudRepository<Liability.PercentOwned, Long> {

}
