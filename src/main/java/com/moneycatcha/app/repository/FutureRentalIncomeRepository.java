package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.FutureRentalIncome;

@Repository
public interface FutureRentalIncomeRepository extends CrudRepository<FutureRentalIncome, Long> {

}
