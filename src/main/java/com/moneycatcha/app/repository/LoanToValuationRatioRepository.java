package com.moneycatcha.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.moneycatcha.app.entity.LoanToValuationRatio;

@Repository
public interface LoanToValuationRatioRepository extends CrudRepository<LoanToValuationRatio, Long> {

}
