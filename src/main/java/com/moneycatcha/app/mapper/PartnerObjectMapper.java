package com.moneycatcha.app.mapper;

import org.springframework.lang.Nullable;

import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.model.Message.Content.Application.RelatedPerson;

public class PartnerObjectMapper {
	
	@ObjectToString
	public static String partnerObject(@Nullable Object object) {
		
		RelatedPerson person = new RelatedPerson();
		
		if (object != null) {
			if (object.getClass().isInstance(person)) {
				return person.getUniqueID();
			}
		}
		return "";
	}
}
