package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.ResponsibleLendingType;
import com.moneycatcha.app.entity.SaleOfAssets;
import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant;
import com.moneycatcha.app.entity.ResponsibleLendingType.SignificantChange;
import com.moneycatcha.app.entity.ResponsibleLendingType.Mitigant.SavingsOrSuperannuation;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {StringObjectMapper.class}
)
public interface ResponsibleLendingTypeMapper {

	/** 
	 * ResponsibleLendingType
	 */
	ResponsibleLendingType toResponsibleLendingType(com.moneycatcha.app.model.ResponsibleLendingType responsible);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.ResponsibleLendingType fromResponsibleLendingType(ResponsibleLendingType entity);

	/*******************************************************************************************/
	
	
	/** 
	 * ResponsibleLendingType::Mitigant
	 */
	Mitigant toMitigant(com.moneycatcha.app.model.ResponsibleLendingType.Mitigant mitigant);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.ResponsibleLendingType.Mitigant fromMitigant(Mitigant entity);
	
	/**
	 * ResponsibleLendingType::Mitigant - List
	 */
	List<Mitigant> toMitigants(List<com.moneycatcha.app.model.ResponsibleLendingType.Mitigant> changes);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.ResponsibleLendingType.Mitigant> fromMitigants(List<Mitigant> entityList);

	
	/** 
	 * ResponsibleLendingType::Mitigant::SaleOfAssets
	 */
	SaleOfAssets toSaleOfAssets(com.moneycatcha.app.model.ResponsibleLendingType.Mitigant.SaleOfAssets mitigant);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.ResponsibleLendingType.Mitigant.SaleOfAssets fromSaleOfAssets(SaleOfAssets entity);
	
	
	/** 
	 * ResponsibleLendingType::Mitigant::SavingsOrSuperannuation
	 */
	SavingsOrSuperannuation toSavingsOrSuperannuation(com.moneycatcha.app.model.ResponsibleLendingType.Mitigant.SavingsOrSuperannuation superannuation);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.ResponsibleLendingType.Mitigant.SavingsOrSuperannuation fromSavingsOrSuperannuation(SavingsOrSuperannuation entity);
	
	
	/*******************************************************************************************/
	
	
	/** 
	 * ResponsibleLendingType::SignificantChange - 
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "change.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "change.endDate", qualifiedBy = GregorianToLocal.class)
	})
	SignificantChange toSignificantChange(com.moneycatcha.app.model.ResponsibleLendingType.SignificantChange change);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	com.moneycatcha.app.model.ResponsibleLendingType.SignificantChange fromSignificantChange(SignificantChange entity);
	
	
	
	/**
	 * ResponsibleLendingType::SignificantChange - List
	 */
	List<SignificantChange> toSignificantChanges(List<com.moneycatcha.app.model.ResponsibleLendingType.SignificantChange> changes);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.ResponsibleLendingType.SignificantChange> fromSignificantChanges(List<SignificantChange> entityList);

	

	
}
