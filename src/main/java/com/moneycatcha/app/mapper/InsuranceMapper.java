package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AssociatedLoanAccount;
import com.moneycatcha.app.entity.Insurance;
import com.moneycatcha.app.entity.Insurance.CommissionPayable;
import com.moneycatcha.app.entity.Insurance.InsuredParty;
import com.moneycatcha.app.entity.Insurance.Premium;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
			StringObjectMapper.class,
			DateMapper.class
			})
public interface InsuranceMapper {

	
	InsuranceMapper INSTANCE = Mappers.getMapper(InsuranceMapper.class);

	
	
	/**
	 * Insurance
	 */
	@Mappings({
		@Mapping(target = "effectiveDate", source = "insurance.effectiveDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "expiryDate", source = "insurance.expiryDate", qualifiedBy = GregorianToLocal.class)
	})
	Insurance toInsurance(Application.Insurance insurance);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "effectiveDate", source = "entity.effectiveDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "expiryDate", source = "entity.expiryDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.Insurance fromInsurance(Insurance entity);

	
	/**
	 * List<Insurance>
	 */
	List<Insurance> toInsuranceList(List<Application.Insurance> insuranceList);
	
	@InheritInverseConfiguration
	List<Application.Insurance> fromInsuranceList(List<Insurance> entities);
	
	
	
	/**
	 * Insurance::AssociatedLoanAccount
	 */
	@Mapping(target = "XAssociatedLoanAccount", source = "associated.XAssociatedLoanAccount", qualifiedBy = ObjectToString.class)
	AssociatedLoanAccount toAssociatedLoanAccount(Application.Insurance.AssociatedLoanAccount associated);
	
	@Mapping(target = "XAssociatedLoanAccount", source = "entity.XAssociatedLoanAccount", qualifiedBy = StringToObject.class)
	Application.Insurance.AssociatedLoanAccount fromAssociatedLoanAccount(AssociatedLoanAccount entity);

	
	/**
	 * List<Insurance>::List<AssociatedLoanAccount>
	 */
	List<AssociatedLoanAccount> toAssociatedLoanAccounts(List<Application.Insurance.AssociatedLoanAccount> accounts);
	
	@InheritInverseConfiguration
	List<Application.Insurance.AssociatedLoanAccount> fromAssociatedLoanAccounts(List<AssociatedLoanAccount> entities);	
	
	
	
	
	/**
	 * Insurance::CommissionPayable
	 */
	@Mapping(target = "XPayer", source = "commission.XPayer", qualifiedBy = ObjectToString.class)
	CommissionPayable toAssociatedLoanAccount(Application.Insurance.CommissionPayable commission);
	
	@Mapping(target = "XPayer", source = "entity.XPayer", qualifiedBy = StringToObject.class)
	Application.Insurance.CommissionPayable fromAssociatedLoanAccount(CommissionPayable entity);

	
	
	/**
	 * Insurance::InsuredParty
	 */
	@Mapping(target = "XInsuredParty", source = "insuredParty.XInsuredParty", qualifiedBy = ObjectToString.class)
	InsuredParty toInsuredParty(Application.Insurance.InsuredParty insuredParty);
	
	@Mapping(target = "XInsuredParty", source = "entity.XInsuredParty", qualifiedBy = StringToObject.class)
	Application.Insurance.InsuredParty fromInsuredParty(InsuredParty entity);

	
	/**
	 * List<Insurance>::List<AssociatedLoanAccount>
	 */
	List<InsuredParty> toInsuredParties(List<Application.Insurance.InsuredParty> insuredParties);
	
	@InheritInverseConfiguration
	List<Application.Insurance.InsuredParty> fromInsuredParties(List<InsuredParty> entities);	
		
	
	
	
	/**
	 * Insurance::Premium
	 */
	Premium toPremium(Application.Insurance.Premium premium);
	
	@InheritInverseConfiguration
	Application.Insurance.Premium fromPremium(Premium entity);

	
	
	
}
