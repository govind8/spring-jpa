package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.SourceOfWealth;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface SourceOfWealthMapper {

	/**
	 * SourceOfWealth 
	 */
	SourceOfWealth toSourceOfWealth(Application.CompanyApplicant.SourceOfWealth sow);
	
	@InheritInverseConfiguration
	Application.CompanyApplicant.SourceOfWealth fromSourceOfWealth(SourceOfWealth entity);
	
	
	
	/**
	 * SourceOfWealth - List
	 */
	List<SourceOfWealth> toSourceOfWealths(List<Application.CompanyApplicant.SourceOfWealth> sows);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant.SourceOfWealth> fromSourceOfWealths(List<SourceOfWealth> entityList);

}
