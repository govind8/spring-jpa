package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.IncomePrevious;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {AddbackMapper.class,
		DateMapper.class,
		StringObjectMapper.class
})
public interface IncomePreviousMapper {

	/**
	 * CompanyApplicant IncomePrevious
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomePrevious.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomePrevious.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrevious.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomePrevious toCAIncomePrevious(Application.CompanyApplicant.IncomePrevious incomePrevious);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.CompanyApplicant.IncomePrevious fromCAIncomePrevious(IncomePrevious entity);
	
	
	/**
	 * TrustApplicant IncomePrevious
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomePrevious.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomePrevious.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrevious.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomePrevious toTAIncomePrevious(Application.TrustApplicant.IncomePrevious incomePrevious);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.TrustApplicant.IncomePrevious fromTAIncomePrevious(IncomePrevious entity);

}
