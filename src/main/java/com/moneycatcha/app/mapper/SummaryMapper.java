package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.Fee;
import com.moneycatcha.app.entity.LoanToValuationRatio;
import com.moneycatcha.app.entity.ServiceabilityResults;
import com.moneycatcha.app.entity.Summary;
import com.moneycatcha.app.entity.Fee.Percentage;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
		DateMapper.class,
		DocumentationInstructionsTypeMapper.class,
		DurationTypeMapper.class,
		FinancialAccountTypeMapper.class,
		StringObjectMapper.class,
		SignatureTypeMapper.class
})
public interface SummaryMapper {

	SummaryMapper INSTANCE = Mappers.getMapper(SummaryMapper.class);

	/**
	 * Summary
	 */
	@Mapping(target = "feesDisclosureDate", source = "summary.feesDisclosureDate", qualifiedBy = GregorianToLocal.class)
	Summary toSummary(Application.Summary summary);
	
	@InheritInverseConfiguration
	@Mapping(target = "feesDisclosureDate", source = "entity.feesDisclosureDate", qualifiedBy = LocalToGregorian.class)
	Application.Summary fromSummary(Summary entity); 

	
	/**
	 * List<Summary>
	 */
	List<Summary> toSummaries(List<Application.Summary> summaries);
	
	@InheritInverseConfiguration
	List<Application.Summary> fromSummaries(List<Summary> entities);

	

	/**
	 * Fee
	 */
	@Mappings({
		@Mapping(target = "gstAmount", source = "fee.GSTAmount"),
		@Mapping(target = "itcAmount", source = "fee.ITCAmount"),
		@Mapping(target = "startDate", source = "fee.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccount", source = "fee.XAccount", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XFinancialProduct", source = "fee.XFinancialProduct", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XProductSet", source = "fee.XProductSet", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XSecurity", source = "fee.XSecurity", qualifiedBy = ObjectToString.class)
	})
	Fee toFee(Application.Summary.Fee fee);
	
	@Mappings({
		@Mapping(target = "GSTAmount", source = "entity.gstAmount"),
		@Mapping(target = "ITCAmount", source = "entity.itcAmount"),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccount", source = "entity.XAccount", qualifiedBy = StringToObject.class),
		@Mapping(target = "XFinancialProduct", source = "entity.XFinancialProduct", qualifiedBy = StringToObject.class),
		@Mapping(target = "XProductSet", source = "entity.XProductSet", qualifiedBy = StringToObject.class),
		@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	})
	Application.Summary.Fee fromFee(Fee entity); 

	
	
	/**
	 * List<Fee>
	 */
	List<Fee> toFees(List<Application.Summary.Fee> fees);
	
	@InheritInverseConfiguration
	List<Application.Summary.Fee> fromFees(List<Fee> entities);

	
	/**
	 * CreditCard
	 */
	CreditCard toCreditCard(Application.Summary.Fee.CreditCard creditCard);
	
	@InheritInverseConfiguration
	Application.Summary.Fee.CreditCard fromCreditCard(CreditCard entity);

	
	/**
	 * Percentage
	 */
	Percentage toPercentage(Application.Summary.Fee.Percentage percentage);
	
	@InheritInverseConfiguration
	Application.Summary.Fee.Percentage fromPercentage(Percentage entity);

	
	/**
	 * LoanToValuationRatio
	 */
	LoanToValuationRatio toLoanToValuationRatio(Application.Summary.LoanToValuationRatio loanToValuationRatio);
	
	@InheritInverseConfiguration
	Application.Summary.LoanToValuationRatio fromLoanToValuationRatio(LoanToValuationRatio entity);

	
	/**
	 * LoanToValuationRatio.ContributingValuation
	 */
	@Mapping(target = "XValuation", source = "valuation.XValuation", qualifiedBy = ObjectToString.class)
	LoanToValuationRatio.ContributingValuation toContributingValuation(Application.Summary.LoanToValuationRatio.ContributingValuation valuation);
	
	@Mapping(target = "XValuation", source = "entity.XValuation", qualifiedBy = StringToObject.class)
	Application.Summary.LoanToValuationRatio.ContributingValuation fromContributingValuation(LoanToValuationRatio.ContributingValuation entity);

	
	
	/**
	 * List<LoanToValuationRatio.ContributingValuation>
	 */
	List<LoanToValuationRatio.ContributingValuation> toContributingValuations(List<Application.Summary.LoanToValuationRatio.ContributingValuation> valuations);
	
	@InheritInverseConfiguration
	List<Application.Summary.LoanToValuationRatio.ContributingValuation> fromContributingValuations(List<LoanToValuationRatio.ContributingValuation> entities);

	
	
	/**
	 * ServiceabilityResults
	 */
	@Mappings({
		@Mapping(target = "dsr", source = "serviceability.DSR"),
		@Mapping(target = "dti", source = "serviceability.DTI"),
		@Mapping(target = "lti", source = "serviceability.LTI"),
		@Mapping(target = "mrim", source = "serviceability.MRIM"),
		@Mapping(target = "nsr", source = "serviceability.NSR"),
	})
	ServiceabilityResults toServiceabilityResults(Application.Summary.ServiceabilityResults serviceability);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "DSR", source = "entity.dsr"),
		@Mapping(target = "DTI", source = "entity.dti"),
		@Mapping(target = "LTI", source = "entity.lti"),
		@Mapping(target = "MRIM", source = "entity.mrim"),
		@Mapping(target = "NSR", source = "entity.nsr")
	})
	Application.Summary.ServiceabilityResults fromServiceabilityResults(ServiceabilityResults entity);


	/**
	 * List<ServiceabilityResults>
	 */
	List<ServiceabilityResults> toServiceabilityResultsList(List<Application.Summary.ServiceabilityResults> results);
	
	@InheritInverseConfiguration
	List<Application.Summary.ServiceabilityResults> fromServiceabilityResultsList(List<ServiceabilityResults> entities);

	
	
	/**
	 * ServiceabilityResults.Applicant
	 */
	@Mapping(target = "XParty", source = "applicant.XParty", qualifiedBy = ObjectToString.class)
	ServiceabilityResults.Applicant toApplicant(Application.Summary.ServiceabilityResults.Applicant applicant);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	Application.Summary.ServiceabilityResults.Applicant fromApplicant(ServiceabilityResults.Applicant entity);


	/**
	 * List<ServiceabilityResults.Applicant>
	 */
	List<ServiceabilityResults.Applicant> toApplicants(List<Application.Summary.ServiceabilityResults.Applicant> applicants);
	
	@InheritInverseConfiguration
	List<Application.Summary.ServiceabilityResults.Applicant> fromApplicants(List<ServiceabilityResults> entities);

	
	/**
	 * ServiceabilityResults.NetDisposableIncome
	 */
	ServiceabilityResults.NetDisposableIncome toNetDisposableIncome(Application.Summary.ServiceabilityResults.NetDisposableIncome netdisposable);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.NetDisposableIncome fromNetDisposableIncome(ServiceabilityResults.NetDisposableIncome entity);

	
	/**
	 * ServiceabilityResults.TotalGrossIncome
	 */
	ServiceabilityResults.TotalGrossIncome toTotalGrossIncome(Application.Summary.ServiceabilityResults.TotalGrossIncome totalgross);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.TotalGrossIncome fromNetDisposableIncome(ServiceabilityResults.TotalGrossIncome entity);


	/**
	 * ServiceabilityResults.TotalNetIncome
	 */
	ServiceabilityResults.TotalNetIncome toTotalNetIncome(Application.Summary.ServiceabilityResults.TotalNetIncome totalnet);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.TotalNetIncome fromTotalNetIncome(ServiceabilityResults.TotalNetIncome entity);

	
	/**
	 * ServiceabilityResults.TotalSystemCalculatedExpenses
	 */
	ServiceabilityResults.TotalSystemCalculatedExpenses toTotalSystemCalculatedExpenses(Application.Summary.ServiceabilityResults.TotalSystemCalculatedExpenses systemCalculated);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.TotalSystemCalculatedExpenses fromTotalSystemCalculatedExpenses(ServiceabilityResults.TotalSystemCalculatedExpenses entity);

	
	/**
	 * ServiceabilityResults.TotalSystemCalculatedLivingExpenses
	 */
	ServiceabilityResults.TotalSystemCalculatedLivingExpenses toTotalSystemCalculatedLivingExpenses(Application.Summary.ServiceabilityResults.TotalSystemCalculatedLivingExpenses systemCalculatedLiving);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.TotalSystemCalculatedLivingExpenses fromTotalSystemCalculatedLivingExpenses(ServiceabilityResults.TotalSystemCalculatedLivingExpenses entity);

	
	/**
	 * ServiceabilityResults.TotalUserStatedLivingExpenses
	 */
	ServiceabilityResults.TotalUserStatedLivingExpenses toTotalUserStatedLivingExpenses(Application.Summary.ServiceabilityResults.TotalUserStatedLivingExpenses userStateLiving);
	
	@InheritInverseConfiguration
	Application.Summary.ServiceabilityResults.TotalUserStatedLivingExpenses fromTotalSystemCalculatedLivingExpenses(ServiceabilityResults.TotalUserStatedLivingExpenses entity);

	
	
}
