package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Household;
import com.moneycatcha.app.entity.Household.Dependant;
import com.moneycatcha.app.entity.Household.EducationExpenses;
import com.moneycatcha.app.entity.Household.ExpenseDetails;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
	PercentOwnedTypeMapper.class, 
	StringObjectMapper.class, 
	DateMapper.class
})
public interface HouseholdMapper {

	HouseholdMapper INSTANCE = Mappers.getMapper(HouseholdMapper.class);

	
	/**
	 * Household
	 */
	Household toHousehold(Application.Household household);
	
	@InheritInverseConfiguration
	Application.Household fromHousehold(Household entity);

	
	/**
	 * List<Household>
	 */
	List<Household> toHouseholds(List<Application.Household> households);
	
	@InheritInverseConfiguration
	List<Application.Household> fromHouseholds(List<Household> entities);
	
	
	
	/**
	 * Household::Dependant
	 */
	@Mapping(target = "dateOfBirth", source = "dependant.dateOfBirth", qualifiedBy = GregorianToLocal.class)
	Dependant toDependant(Application.Household.Dependant dependant);
	
	@InheritInverseConfiguration
	@Mapping(target = "dateOfBirth", source = "entity.dateOfBirth", qualifiedBy = LocalToGregorian.class)
	Application.Household.Dependant fromDependant(Dependant entity);

	
	/**
	 * List<Household>::List<Dependant>
	 */
	List<Dependant> toDependants(List<Application.Household.Dependant> dependants);
	
	@InheritInverseConfiguration
	List<Application.Household.Dependant> fromDependants(List<Dependant> entities);
	
	

	/**
	 * Household::Dependant.FinancialProvider
	 */
	@Mapping(target = "XParty", source = "financialProvider.XParty", qualifiedBy = ObjectToString.class)
	Dependant.FinancialProvider toFinancialProvider(Application.Household.Dependant.FinancialProvider financialProvider);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	Application.Household.Dependant.FinancialProvider fromFinancialProvider(Dependant.FinancialProvider entity);


	
	
	/**
	 * List<Household>::List<Dependant>::List<FinancialProvider>
	 */
	List<Dependant.FinancialProvider> toFinancialProviders(List<Application.Household.Dependant.FinancialProvider> financialProviders);
	
	@InheritInverseConfiguration
	List<Application.Household.Dependant.FinancialProvider> fromFinancialProviders(List<Dependant.FinancialProvider> entities);
	
	

	/**
	 * Household::EducationExpenses
	 */
	EducationExpenses toEducationExpenses(Application.Household.EducationExpenses educationExpenses);
	
	@InheritInverseConfiguration
	Application.Household.EducationExpenses fromEducationExpenses(EducationExpenses entity);

	
	/**
	 * Household::ExpenseDetails
	 */
	ExpenseDetails toExpenseDetails(Application.Household.ExpenseDetails expenseDetails);
	
	@InheritInverseConfiguration
	Application.Household.ExpenseDetails fromExpenseDetails(ExpenseDetails entity);
	
	
	/**
	 * Household::ExpenseDetails::LivingExpense
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "livingExpense.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "livingExpense.endDate", qualifiedBy = GregorianToLocal.class)
	})
	ExpenseDetails.LivingExpense toLivingExpense(Application.Household.ExpenseDetails.LivingExpense livingExpense);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.Household.ExpenseDetails.LivingExpense fromLivingExpense(ExpenseDetails.LivingExpense entity);	
	
	

	/**
	 * Household::ExpenseDetails::List<LivingExpense>
	 */
	List<ExpenseDetails.LivingExpense> toLivingExpenses(List<Application.Household.ExpenseDetails.LivingExpense> livingExpenses);
	
	@InheritInverseConfiguration
	List<Application.Household.ExpenseDetails.LivingExpense> fromLivingExpenses(List<ExpenseDetails.LivingExpense> entities);
	

	
	
	
	
	/**
	 * Household::ExpenseDetails::OtherCommitment
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "otherCommitment.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "otherCommitment.endDate", qualifiedBy = GregorianToLocal.class)
	})
	ExpenseDetails.OtherCommitment toOtherCommitment(Application.Household.ExpenseDetails.OtherCommitment otherCommitment);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.Household.ExpenseDetails.OtherCommitment fromOtherCommitment(ExpenseDetails.OtherCommitment entity);	
	
	
	
	/**
	 * Household::ExpenseDetails::List<OtherCommitment>
	 */	
	List<ExpenseDetails.OtherCommitment> toOtherCommitments(List<Application.Household.ExpenseDetails.OtherCommitment> otherCommitments);

	@InheritInverseConfiguration
	List<Application.Household.ExpenseDetails.OtherCommitment> fromOtherCommitments(List<ExpenseDetails.OtherCommitment> entities);	

	/**
	 * Household::ExpenseDetails::TotalSystemCalculatedLivingExpenses
	 */
	ExpenseDetails.TotalSystemCalculatedLivingExpenses toTotalSystemCalculatedLivingExpenses(Application.Household.ExpenseDetails.TotalSystemCalculatedLivingExpenses systemCalculated);
	
	Application.Household.ExpenseDetails.TotalSystemCalculatedLivingExpenses fromTotalSystemCalculatedLivingExpenses(ExpenseDetails.TotalSystemCalculatedLivingExpenses entity);	

	
	/**
	 * Household::ExpenseDetails::TotalUserStatedLivingExpenses
	 */
	ExpenseDetails.TotalUserStatedLivingExpenses toTotalUserStatedLivingExpenses(Application.Household.ExpenseDetails.TotalUserStatedLivingExpenses userCalculated);
	
	Application.Household.ExpenseDetails.TotalUserStatedLivingExpenses fromTotalUserStatedLivingExpenses(ExpenseDetails.TotalUserStatedLivingExpenses entity);	

}
