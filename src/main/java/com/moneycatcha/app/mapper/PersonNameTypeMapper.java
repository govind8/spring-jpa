package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.PersonNameType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface PersonNameTypeMapper {

	PersonNameTypeMapper INSTANCE = Mappers.getMapper(PersonNameTypeMapper.class);
	
	/**
	 * PersonNameType
	 */
	PersonNameType toPersonNameType(com.moneycatcha.app.model.PersonNameType personNameType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.PersonNameType fromPersonNameType(PersonNameType entity);	
	
}
