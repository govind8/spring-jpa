package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AddressType;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface AddressTypeMapper {
	
	AddressTypeMapper INSTANCE = Mappers.getMapper(AddressTypeMapper.class);

	/**
	 * AddressType
	 */
	AddressType toAddress(com.moneycatcha.app.model.AddressType addressType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType fromAddress(AddressType entity);
	
	
	
	/**
	 * AddressType - List
	 */
	List<AddressType> toAddresses(List<com.moneycatcha.app.model.AddressType> addressType);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.AddressType> fromAddresses(List<AddressType> entityList);
	
	
	/**
	 * AddressType.DeliveryPoint 
	 */
	AddressType.DeliveryPoint toDeliveryPoint(com.moneycatcha.app.model.AddressType.DeliveryPoint point);

	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType.DeliveryPoint fromDeliveryPoint(AddressType.DeliveryPoint entity);

	
	/**
	 * AddressType.DXBox 
	 */
	AddressType.DXBox toDXBox(com.moneycatcha.app.model.AddressType.DXBox dxbox);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType.DXBox fromDXBox(AddressType.DXBox entity);

	
	/**
	 * AddressType.NonStandard 
	 */
	AddressType.NonStandard toNonStandard(com.moneycatcha.app.model.AddressType.NonStandard nonstandard);

	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType.NonStandard fromNonStandard(AddressType.NonStandard entity);
	
	
	
	/**
	 * AddressType.POBox  
	 */
	AddressType.POBox toPOBox(com.moneycatcha.app.model.AddressType.POBox pobox);

	// JPA -to- JAXB
	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType.POBox fromPOBox(AddressType.POBox entity);

	
	
	/**
	 * AddressType.Standard  
	 */
	AddressType.Standard toStandard(com.moneycatcha.app.model.AddressType.Standard standard);

	@InheritInverseConfiguration
	com.moneycatcha.app.model.AddressType.Standard fromStandard(AddressType.Standard entity);
	
	
}
