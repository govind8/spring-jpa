package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.CurrentAddress;
import com.moneycatcha.app.entity.EmailAddress;
import com.moneycatcha.app.entity.PostSettlementAddress;
import com.moneycatcha.app.entity.PreviousAddress;
import com.moneycatcha.app.entity.PriorAddress;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.BusinessChannel;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
	uses = {
			PhoneTypeMapper.class,
			DurationTypeMapper.class,
			ContactPersonMapper.class,
			StringObjectMapper.class,
			DateMapper.class
})
public interface ContactMapper {
	
	ContactMapper INSTANCE = Mappers.getMapper(ContactMapper.class);
	
	/**
	 * BusinessChannel - Contact
	 */
	@Mapping(target = "XAddress", source = "bcContact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toBusinessChannelContact(BusinessChannel.Contact bcContact);
	
	@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class)
	BusinessChannel.Contact fromBusinessChannelContact(Contact entity);
	
	
	
	/**
	 * CompanyApplicant - Contact
	 */
	@Mappings({
		@Mapping(target = "preferredContactCompanyList", source = "caContact.preferredContact"),
		@Mapping(target = "previousRegisteredAddressEndDate", source = "caContact.previousRegisteredAddressEndDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "previousRegisteredAddressStartDate", source = "caContact.previousRegisteredAddressStartDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "principalTradingAddressStartDate", source = "caContact.principalTradingAddressStartDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "registeredAddressStartDate", source = "caContact.registeredAddressStartDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XMailingAddress", source = "caContact.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPreviousRegisteredAddress", source = "caContact.XPreviousRegisteredAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPrincipalTradingAddress", source = "caContact.XPrincipalTradingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XRegisteredAddress", source = "caContact.XRegisteredAddress", qualifiedBy = ObjectToString.class)
	})
	Contact toCompanyApplicantContact(CompanyApplicant.Contact caContact);
	
	
	@Mappings({
		@Mapping(target = "preferredContact", source = "entity.preferredContactCompanyList"),
		@Mapping(target = "previousRegisteredAddressEndDate", source = "entity.previousRegisteredAddressEndDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "previousRegisteredAddressStartDate", source = "entity.previousRegisteredAddressStartDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "principalTradingAddressStartDate", source = "entity.principalTradingAddressStartDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "registeredAddressStartDate", source = "entity.registeredAddressStartDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPreviousRegisteredAddress", source = "entity.XPreviousRegisteredAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPrincipalTradingAddress", source = "entity.XPrincipalTradingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XRegisteredAddress", source = "entity.XRegisteredAddress", qualifiedBy = StringToObject.class)
	})	
	CompanyApplicant.Contact fromCompanyApplicantContact(Contact entity);

	

	
	/**
	 * PersonApplicant - Contact
	 */
	@Mapping(target = "preferredContactPersonList", source = "personApplicant.preferredContact")
	Contact toPersonApplicantContact(PersonApplicant.Contact personApplicant);
	
	@InheritInverseConfiguration
	@Mapping(target = "preferredContact", source = "personApplicant.preferredContactPersonList")
	PersonApplicant.Contact fromPersonApplicantContact(Contact personApplicant);

	/**
	 * Contact-CurrentAddress
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "currentaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XLandlord", source = "currentaddress.XLandlord", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XMailingAddress", source = "currentaddress.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XResidentialAddress", source = "currentaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	CurrentAddress toCurrentAddress(PersonApplicant.Contact.CurrentAddress currentaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XLandlord", source = "entity.XLandlord", qualifiedBy = StringToObject.class),
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Contact.CurrentAddress fromCurrentAddress(CurrentAddress entity);
		

	
	/**
	 * Contact-EmailAddress
	 */
	EmailAddress toEmailAddress(PersonApplicant.Contact.EmailAddress emailaddress);
	
	@InheritInverseConfiguration
	PersonApplicant.Contact.EmailAddress fromEmailAddress(EmailAddress entity);
	
	/**
	 * List<EmailAddress>
	 */
	List<EmailAddress> toEmailAddress(List<PersonApplicant.Contact.EmailAddress> emailaddresses);
	
	@InheritInverseConfiguration
	List<PersonApplicant.Contact.EmailAddress> fromEmailAddress(List<EmailAddress> entities);
	
	
	/**
	 * Contact-PostSettlementAddress
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "settlementaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XMailingAddress", source = "settlementaddress.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XResidentialAddress", source = "settlementaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PostSettlementAddress toPostSettlementAddress(PersonApplicant.Contact.PostSettlementAddress settlementaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Contact.PostSettlementAddress fromPostSettlementAddress(PostSettlementAddress entity);

	
	/**
	 * Contact-PreviousAddress
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "previousaddress.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "previousaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XResidentialAddress", source = "previousaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PreviousAddress toPreviousAddress(PersonApplicant.Contact.PreviousAddress previousaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Contact.PreviousAddress fromPreviousAddress(PreviousAddress entity);
	
	
	/**
	 * Contact-PriorAddress
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "prioraddress.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "prioraddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XResidentialAddress", source = "prioraddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PriorAddress toPriorAddress(PersonApplicant.Contact.PriorAddress prioraddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Contact.PriorAddress fromPriorAddress(PriorAddress entity);
	
	

}
