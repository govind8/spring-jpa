package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Condition;
import com.moneycatcha.app.entity.ErrorInstructions;
import com.moneycatcha.app.entity.Instructions;
import com.moneycatcha.app.entity.Submit;
import com.moneycatcha.app.entity.Update;
import com.moneycatcha.app.entity.Instructions.ApplicationInstructions;
import com.moneycatcha.app.entity.Update.Event;
import com.moneycatcha.app.entity.Update.Status;
import com.moneycatcha.app.entity.Update.Status.Declined;
import com.moneycatcha.app.entity.Update.Status.PreApproved;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = { 
		DateMapper.class,
		StringObjectMapper.class
	}
)
public interface InstructionsMapper {

	InstructionsMapper INSTANCE = Mappers.getMapper(InstructionsMapper.class);

	/**
	 * Message.Instructions
	 */
	Instructions toInstructions(Message.Instructions instructions);
	
	@InheritInverseConfiguration
	Message.Instructions fromInstructions(Instructions entity);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions
	 */
	ApplicationInstructions toApplicationInstructions(Message.Instructions.ApplicationInstructions applicationInstructions);
	
	@InheritInverseConfiguration
	Message.Instructions.ApplicationInstructions fromApplicationInstructions(Instructions entity);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Submit
	 */
	Submit toSubmit(Message.Instructions.ApplicationInstructions.Submit submit);
	
	@InheritInverseConfiguration
	Message.Instructions.ApplicationInstructions.Submit fromSubmit(Submit entity);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Submit.Condition
	 */
	
	@Mapping(target = "XSupportingDocument", source = "condition.XSupportingDocument", qualifiedBy = ObjectToString.class)
	Condition toSubmitCondition(Message.Instructions.ApplicationInstructions.Submit.Condition condition);
	
	@Mapping(target = "XSupportingDocument", source = "entity.XSupportingDocument", qualifiedBy = StringToObject.class)
	Message.Instructions.ApplicationInstructions.Submit.Condition fromSubmitCondition(Condition entity);
	
	
	/**
	 * List<Message.Instructions.ApplicationInstructions.Submit>
	 */
	List<Condition> toSubmitCondition(List<Message.Instructions.ApplicationInstructions.Submit.Condition> conditions);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Submit.Condition> fromSubmitCondition(List<Condition> entities);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Update
	 */
	Update toUpdate(Message.Instructions.ApplicationInstructions.Update update);
	
	@InheritInverseConfiguration
	Message.Instructions.ApplicationInstructions.Update fromUpdate(Update entity);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Update.Event
	 */
	
	@Mapping(target = "dateTime", source = "event.dateTime", qualifiedBy = GregorianToLocalDateTime.class)
	Event toUpdateEvent(Message.Instructions.ApplicationInstructions.Update.Event event);
	
	@InheritInverseConfiguration
	@Mapping(target = "dateTime", source = "entity.dateTime", qualifiedBy = LocalDateTimeToGregorian.class)
	Message.Instructions.ApplicationInstructions.Update.Event fromUpdateEvent(Event entity);
	
	
	/**
	 * List<Message.Instructions.ApplicationInstructions.Update.Event>
	 */
	List<Event> toUpdateEvents(List<Message.Instructions.ApplicationInstructions.Update.Event> events);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Update.Event> fromUpdateEvents(List<Event> entities);
	

	
	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status
	 */
	@Mapping(target = "dateTime", source = "status.dateTime", qualifiedBy = GregorianToLocalDateTime.class)
	Status toUpdateStatus(Message.Instructions.ApplicationInstructions.Update.Status status);
	
	
	@Mapping(target = "dateTime", source = "entity.dateTime", qualifiedBy = LocalDateTimeToGregorian.class)
	Message.Instructions.ApplicationInstructions.Update.Status fromUpdateStatus(Status entity);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.Condition
	 */
	
	@Mappings({
		@Mapping(target = "createdDateTime", source = "condition.createdDateTime", qualifiedBy = GregorianToLocalDateTime.class),
		@Mapping(target = "updatedDateTime", source = "condition.updatedDateTime", qualifiedBy = GregorianToLocalDateTime.class),
		@Mapping(target = "XSupportingDocument", source = "condition.XSupportingDocument", qualifiedBy = ObjectToString.class)
	})
	Condition toUpdateStatusCondition(Message.Instructions.ApplicationInstructions.Update.Status.Condition condition);
	
	@Mappings({
		@Mapping(target = "createdDateTime", source = "entity.createdDateTime", qualifiedBy = LocalDateTimeToGregorian.class),
		@Mapping(target = "updatedDateTime", source = "entity.updatedDateTime", qualifiedBy = LocalDateTimeToGregorian.class),
		@Mapping(target = "XSupportingDocument", source = "entity.XSupportingDocument", qualifiedBy = StringToObject.class)
	})
	Message.Instructions.ApplicationInstructions.Update.Status.Condition fromUpdateStatusCondition(Condition entity);
	
	
	/**
	 * List<Message.Instructions.ApplicationInstructions.Update.Status.Condition>
	 */
	List<Condition> toUpdateStatusConditions(List<Message.Instructions.ApplicationInstructions.Update.Status.Condition> conditions);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Update.Status.Condition> fromUpdateStatusConditions(List<Condition> entities);
	

	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product
	 */
	@Mapping(target = "XFinancialProduct", source = "product.XFinancialProduct", qualifiedBy = ObjectToString.class)
	Condition.Product toUpdateStatusConditionProduct(Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product product);
	
	@Mapping(target = "XFinancialProduct", source = "entity.XFinancialProduct", qualifiedBy = StringToObject.class)
	Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product fromUpdateStatusConditionProduct(Condition.Product entity);
	

	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product
	 */
	List<Condition.Product> toUpdateStatusConditionProductList(List<Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product> products);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product> fromUpdateStatusConditionProductList(List<Condition.Product> entities);

	
	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.Declined
	 */
	Declined toUpdateStatusDeclined(Message.Instructions.ApplicationInstructions.Update.Status.Declined declined);
	
	@InheritInverseConfiguration
	Message.Instructions.ApplicationInstructions.Update.Status.Declined fromUpdateStatusDeclined(Declined entity);
	

	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.Condition.Product
	 */
	List<Declined> toUpdateStatusDeclinedList(List<Message.Instructions.ApplicationInstructions.Update.Status.Declined> declinedList);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Update.Status.Declined> fromUpdateStatusDeclinedList(List<Declined> entities);
	
	
	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.PreApproved
	 */
	PreApproved toUpdateStatusPreApproved(Message.Instructions.ApplicationInstructions.Update.Status.PreApproved preApproved);
	
	@InheritInverseConfiguration
	Message.Instructions.ApplicationInstructions.Update.Status.PreApproved fromUpdateStatusPreApproved(PreApproved entity);
	

	/**
	 * Message.Instructions.ApplicationInstructions.Update.Status.PreApproved
	 */
	List<PreApproved> toUpdateStatusPreApprovedList(List<Message.Instructions.ApplicationInstructions.Update.Status.PreApproved> preApprovedList);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ApplicationInstructions.Update.Status.PreApproved> fromUpdateStatusPreApprovedList(List<PreApproved> entities);
	
	
	
	
	
	
	/**
	 * Message.Instructions.ErrorInstructions
	 */
	ErrorInstructions toErrorInstructions(Message.Instructions.ErrorInstructions errorInstructions);
	
	@InheritInverseConfiguration
	Message.Instructions.ErrorInstructions fromErrorInstructions(ErrorInstructions entity);
	

	/**
	 * Message.Instructions.ErrorInstructions
	 */
	List<ErrorInstructions> toErrorInstructionsList(List<Message.Instructions.ErrorInstructions> errorInstructionsList);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ErrorInstructions> fromErrorInstructionsList(List<ErrorInstructions> entities);
	
	
	/**
	 * Message.Instructions.ErrorInstructions.Annotation
	 */
	ErrorInstructions.Annotation toAnnotations(Message.Instructions.ErrorInstructions.Annotation annotation);
	
	@InheritInverseConfiguration
	Message.Instructions.ErrorInstructions.Annotation fromAnnotation(ErrorInstructions.Annotation entity);
	

	/**
	 * List<Message.Instructions.ErrorInstructions.Annotation>
	 */
	List<ErrorInstructions.Annotation> toAnnotations(List<Message.Instructions.ErrorInstructions.Annotation> annotations);
	
	@InheritInverseConfiguration
	List<Message.Instructions.ErrorInstructions.Annotation> fromAnnotations(List<ErrorInstructions.Annotation> entities);
	
	
	
}
