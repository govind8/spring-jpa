package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.TaxDeclarationDetailsType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface TaxDeclarationDetailsTypeMapper {

	TaxDeclarationDetailsTypeMapper INSTANCE = Mappers.getMapper(TaxDeclarationDetailsTypeMapper.class);

	/**
	 * TaxDeclarationDetailsType
	 */
	TaxDeclarationDetailsType toTaxDeclarationDetailsType(com.moneycatcha.app.model.TaxDeclarationDetailsType taxDeclarationType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.TaxDeclarationDetailsType fromTaxDeclarationDetailsType(TaxDeclarationDetailsType entity);
}
