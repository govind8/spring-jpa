package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.TransformMetadata;
import com.moneycatcha.app.entity.TransformMetadata.Identifier;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
	uses = { 
		StringObjectMapper.class 
	})
public interface TransformMetadataMapper {

	TransformMetadataMapper INSTANCE = Mappers.getMapper(TransformMetadataMapper.class);
	
	/**
	 * TransformMetadata
	 */
	
	TransformMetadata toTransformMetadata(Message.TransformMetadata recipient);
	
	@InheritInverseConfiguration
	Message.TransformMetadata fromTransformMetadata(TransformMetadata entity);


	/**
	 * Identifier
	 */
	@Mapping(target = "XContext", source = "identifier.XContext", qualifiedBy = ObjectToString.class)
	Identifier toIdentifier(Message.TransformMetadata.Identifier identifier);
	
	@Mapping(target = "XContext", source = "entity.XContext", qualifiedBy = StringToObject.class)
	Message.TransformMetadata.Identifier fromIdentifier(Identifier entity);

	
	/**
	 * List<Identifier>
	 */
	List<Identifier> toIdentifiers(List<Message.TransformMetadata.Identifier> identifiers);
	
	@InheritInverseConfiguration
	List<Message.TransformMetadata.Identifier> fromIdentifiers(List<Identifier> entity);


}
