package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.CreditHistory;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface CreditHistoryMapper {

	/**
	 * CreditHistory 
	 */
	CreditHistory toCreditHistory(CompanyApplicant.CreditHistory ch);
	
	@InheritInverseConfiguration
	CompanyApplicant.CreditHistory fromCreditHistory(CreditHistory entity);

	
	/**
	 * CreditHistory - List
	 */
	List<CreditHistory> toCreditHistories(List<CompanyApplicant.CreditHistory> histories);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.CreditHistory> fromCreditHistories(List<CreditHistory> entityList);
	
}
