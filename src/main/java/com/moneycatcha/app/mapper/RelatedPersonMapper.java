package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.RelatedPerson;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
		DateMapper.class,
		ForeignTaxAssociationTypeMapper.class,
		ContactMapper.class,
		PhoneTypeMapper.class,
		PersonNameTypeMapper.class,
		StringObjectMapper.class,
		TaxDeclarationDetailsTypeMapper.class
	})
public interface RelatedPersonMapper {

	RelatedPersonMapper INSTANCE = Mappers.getMapper(RelatedPersonMapper.class);

	/**
	 * RelatedPerson
	 */
	@Mapping(target = "dateOfBirth", source = "relatedPerson.dateOfBirth", qualifiedBy = GregorianToLocal.class)
	RelatedPerson toRelatedPerson(Application.RelatedPerson relatedPerson);
	
	@InheritInverseConfiguration
	@Mapping(target = "dateOfBirth", source = "entity.dateOfBirth", qualifiedBy = LocalToGregorian.class)
	Application.RelatedPerson fromRelatedPerson(RelatedPerson entity); 

	
	/**
	 * List<RelatedPerson>
	 */
	List<RelatedPerson> toRelatedPersons(List<Application.RelatedPerson> relatedPersons);
	
	@InheritInverseConfiguration
	List<Application.RelatedPerson> fromRelatedPersons(List<RelatedPerson> entities);


	/**
	 * RelatedPerson.Contact
	 */
	@Mapping(target = "XAddress", source = "contact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toRelatedPersonContact(Application.RelatedPerson.Contact contact);
	
	@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class)
	Application.RelatedPerson.Contact fromRelatedPersonContact(Contact entity);
	
	
}
