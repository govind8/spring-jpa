package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.SecurityAgreementType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface SecurityAgreementTypeMapper {

	SecurityAgreementTypeMapper INSTANCE = Mappers.getMapper(SecurityAgreementTypeMapper.class);

	/**
	 * SecurityAgreementType
	 */
	SecurityAgreementType toSecurityAgreementType(com.moneycatcha.app.model.SecurityAgreementType securityAgreementType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.SecurityAgreementType fromSecurityAgreementType(SecurityAgreementType entity);

}
