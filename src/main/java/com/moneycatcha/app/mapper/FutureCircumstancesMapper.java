package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.entity.FutureCircumstances;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = { 
			ResponsibleLendingTypeMapper.class,
			StringObjectMapper.class 
	}
)
public interface FutureCircumstancesMapper {

	FutureCircumstancesMapper INSTANCE = Mappers.getMapper(FutureCircumstancesMapper.class);

	
	
	/**
	 * FutureCircumstances
	 */
	FutureCircumstances toFutureCircumstances(Content.NeedsAnalysis.FutureCircumstances futureCircumstances);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.FutureCircumstances fromFutureCircumstances(FutureCircumstances entity);
	
	
	/**
	 * Applicant
	 */
	@Mapping(target = "XApplicant", source = "applicant.XApplicant", qualifiedBy = ObjectToString.class)
	Applicant toApplicant(Content.NeedsAnalysis.FutureCircumstances.Applicant applicant);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	Content.NeedsAnalysis.FutureCircumstances.Applicant fromApplicant(Applicant entity);

	
	/**
	 * List<Applicant>
	 */
	List<Applicant> toApplicants(List<Content.NeedsAnalysis.FutureCircumstances.Applicant> applicants);
	
	@InheritInverseConfiguration
	List<Content.NeedsAnalysis.FutureCircumstances.Applicant> fromApplicants(List<Applicant> entities);
	
	
	

}
