package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.MasterAgreement;
import com.moneycatcha.app.entity.Security;
import com.moneycatcha.app.entity.MasterAgreement.AccountToIncorporate;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		StringObjectMapper.class,
		PercentOwnedTypeMapper.class,
		SecurityAgreementTypeMapper.class
	}
)
public interface MasterAgreementMapper {

	MasterAgreementMapper INSTANCE = Mappers.getMapper(MasterAgreementMapper.class);

	
	/**
	 * MasterAgreement
	 */
	@Mappings({
		@Mapping(target = "dateOfExecution", source = "masterAgreement.dateOfExecution", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "masterAgreement.endDate", qualifiedBy = GregorianToLocal.class)
	})
	MasterAgreement toMasterAgreement(Application.MasterAgreement masterAgreement);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "dateOfExecution", source = "entity.dateOfExecution", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.MasterAgreement fromMasterAgreement(MasterAgreement entity); 
	
	
	/**
	 * List<MasterAgreement>
	 */
	List<MasterAgreement> toMasterAgreements(List<Application.MasterAgreement> masterAgreements);
	
	@InheritInverseConfiguration
	List<Application.MasterAgreement> fromMasterAgreements(List<MasterAgreement> entities);
	
	
	/**
	 * MasterAgreement.AccountToIncorporate
	 */
	@Mappings({
		@Mapping(target = "XExistingAccount", source = "accountTo.XExistingAccount", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XNewAccount", source = "accountTo.XNewAccount", qualifiedBy = ObjectToString.class)
	})
	AccountToIncorporate toAccountToIncorporate(Application.MasterAgreement.AccountToIncorporate accountTo);
	
	@Mappings({
		@Mapping(target = "XExistingAccount", source = "entity.XExistingAccount", qualifiedBy = StringToObject.class),
		@Mapping(target = "XNewAccount", source = "entity.XNewAccount", qualifiedBy = StringToObject.class)
	})
	Application.MasterAgreement.AccountToIncorporate fromAccountToIncorporate(AccountToIncorporate entity); 
	
	
	/**
	 * List<MasterAgreement.AccountToIncorporate>
	 */
	List<AccountToIncorporate> toAccountToIncorporates(List<Application.MasterAgreement.AccountToIncorporate> accountTos);
	
	@InheritInverseConfiguration
	List<Application.MasterAgreement.AccountToIncorporate> fromAccountToIncorporates(List<AccountToIncorporate> entities);
	
	
	
	/**
	 * MasterAgreement::Security
	 */
	@Mapping(target = "XSecurity", source = "security.XSecurity", qualifiedBy = ObjectToString.class)
	Security toSecurity(Application.MasterAgreement.Security security);
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.MasterAgreement.Security fromSecurity(Security entity);	

	
	
	/**
	 * List<MasterAgreement::Security>
	 */
	List<Security> toSecurities(List<Application.MasterAgreement.Security> securities);
	
	@InheritInverseConfiguration
	List<Application.MasterAgreement.Security> fromSecurities(List<Security> entities);	
		

}
