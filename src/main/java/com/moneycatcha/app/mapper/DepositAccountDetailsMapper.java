package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.DepositAccountDetails;
import com.moneycatcha.app.entity.FeaturesSelected;
import com.moneycatcha.app.entity.DepositAccountDetails.SourceOfInitialDeposit;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {DocumentationInstructionsTypeMapper.class,
		PercentOwnedTypeMapper.class, 
		StringObjectMapper.class, 
		DateMapper.class
	})
public interface DepositAccountDetailsMapper {

	DepositAccountDetailsMapper INSTANCE = Mappers.getMapper(DepositAccountDetailsMapper.class);
	
	/**
	 * DepositAccountDetails
	 */
	DepositAccountDetails toDepositAccountDetails(Application.DepositAccountDetails deposit);
	
	@InheritInverseConfiguration
	Application.DepositAccountDetails fromDepositAccountDetails(DepositAccountDetails entity);

	
	/**
	 * List<DepositAccountDetails>
	 */
	List<DepositAccountDetails> toDepositAccountDetailsList(List<Application.DepositAccountDetails> depositAccountDetailsList);
	
	@InheritInverseConfiguration
	List<Application.DepositAccountDetails> fromDepositAccountDetailsList(List<DepositAccountDetails> entities);

	
	
	
	/**
	 * DepositAccountDetails.Commission
	 */
	DepositAccountDetails.Commission toCommission(Application.DepositAccountDetails.Commission commission);
	
	@InheritInverseConfiguration
	Application.DepositAccountDetails.Commission fromCommission(DepositAccountDetails.Commission entity);

	
	
	/**
	 * DepositAccountDetails::FeaturesSelected
	 */
	@Mapping(target = "XApplicant", source = "featuresSelected.XApplicant", qualifiedBy = ObjectToString.class)
	FeaturesSelected toFeaturesSelected(Application.DepositAccountDetails.FeaturesSelected featuresSelected);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	Application.DepositAccountDetails.FeaturesSelected fromFeaturesSelected(FeaturesSelected entity);

	
	/**
	 * List<DepositAccountDetails>::List<FeaturesSelected>
	 */
	List<FeaturesSelected> toFeaturesSelectedList(List<Application.DepositAccountDetails.FeaturesSelected> featuresSelectedList);
	
	@InheritInverseConfiguration
	List<Application.DepositAccountDetails.FeaturesSelected> fromFeaturesSelectedList(List<FeaturesSelected> entities);
	
	
	
	/**
	 * DepositAccountDetails::FeaturesSelected.ExtraFeature
	 */
	FeaturesSelected.ExtraFeature toExtraFeature(Application.DepositAccountDetails.FeaturesSelected.ExtraFeature featuresSelected);
	
	@InheritInverseConfiguration
	Application.DepositAccountDetails.FeaturesSelected fromExtraFeature(FeaturesSelected.ExtraFeature entity);

	
	/**
	 * List<DepositAccountDetails>::List<FeaturesSelected>::List<ExtraFeature>
	 */
	List<FeaturesSelected.ExtraFeature> toExtraFeatures(List<Application.DepositAccountDetails.FeaturesSelected> featuresSelectedList);
	
	@InheritInverseConfiguration
	List<Application.DepositAccountDetails.FeaturesSelected.ExtraFeature> fromExtraFeatures(List<FeaturesSelected.ExtraFeature> entities);
	
		
	
	
	/**
	 * DepositAccountDetails.Package
	 */
	DepositAccountDetails.Package toPackage(Application.DepositAccountDetails.Package Package);
	
	@InheritInverseConfiguration
	Application.DepositAccountDetails.Package fromPackage(DepositAccountDetails.Package entity);
	
	
	
	/**
	 * DepositAccountDetails::SourceOfInitialDeposit
	 */
	SourceOfInitialDeposit toSourceOfInitialDeposit(Application.DepositAccountDetails.SourceOfInitialDeposit sourceOfInitialDeposit);
	
	@InheritInverseConfiguration
	Application.DepositAccountDetails.SourceOfInitialDeposit fromSourceOfInitialDeposit(SourceOfInitialDeposit entity);

	
	/**
	 * List<DepositAccountDetails>::List<SourceOfInitialDeposit>
	 */
	List<SourceOfInitialDeposit> toSourceOfInitialDeposits(List<Application.DepositAccountDetails.SourceOfInitialDeposit> featuresSelectedList);
	
	@InheritInverseConfiguration
	List<Application.DepositAccountDetails.SourceOfInitialDeposit> fromSourceOfInitialDeposits(List<SourceOfInitialDeposit> entities);	
	
	
}
