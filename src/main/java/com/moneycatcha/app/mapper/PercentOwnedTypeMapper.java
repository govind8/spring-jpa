package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Owner;
import com.moneycatcha.app.entity.PercentOwnedType;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface PercentOwnedTypeMapper {

	/**
	 * PercentOwnedType
	 */
	PercentOwnedType toAccountOwners(com.moneycatcha.app.model.PercentOwnedType accountOwners);
	
	com.moneycatcha.app.model.PercentOwnedType fromAccountOwners(PercentOwnedType entity);
	
	
	
	/**
	 * PercentOwnedType::Owner
	 */
	@Mapping(target = "XParty", source = "owner.XParty", qualifiedBy = ObjectToString.class)
	Owner toOwner(com.moneycatcha.app.model.PercentOwnedType.Owner owner);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	com.moneycatcha.app.model.PercentOwnedType.Owner fromOwner(Owner entity);

	
	/**
	 * PercentOwnedType::List<Owner>
	 */
	List<Owner> toOwners(List<com.moneycatcha.app.model.PercentOwnedType.Owner> owners);
	
	List<com.moneycatcha.app.model.PercentOwnedType.Owner> fromOwners(List<Owner> entities);
	
}
