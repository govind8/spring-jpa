package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.BeneficialOwner;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface BeneficialOwnerMapper {

	BeneficialOwnerMapper INSTANCE = Mappers.getMapper(BeneficialOwnerMapper.class);

	/**
	 * BeneficialOwner
	 */
	@Mappings({
		@Mapping(target = "XBeneficialOwner", source = "owner.XBeneficialOwner", qualifiedBy = ObjectToString.class)
	})
	BeneficialOwner toBeneficialOwner(CompanyApplicant.BeneficialOwner owner);
	
	@Mapping(target = "XBeneficialOwner", source = "entity.XBeneficialOwner", qualifiedBy = StringToObject.class)
	CompanyApplicant.BeneficialOwner fromBeneficialOwner(BeneficialOwner entity);
	
	

	/**
	 * BeneficialOwner - List
	 */
	List<BeneficialOwner> toBeneficialOwner(List<CompanyApplicant.BeneficialOwner> owners);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.BeneficialOwner> fromBeneficialOwner(List<BeneficialOwner> entityList);
	
	
}
