package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.entity.CoApplicantInterview;
import com.moneycatcha.app.entity.Interview;
import com.moneycatcha.app.entity.NeedsAnalysis;
import com.moneycatcha.app.entity.RefinancingAndConsolidation;
import com.moneycatcha.app.entity.NeedsAnalysis.PurposeOfApplication;
import com.moneycatcha.app.entity.RefinancingAndConsolidation.RefinancingReason;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = { 
			DateMapper.class,
			DurationTypeMapper.class,
			FutureCircumstancesMapper.class,
			PreferencesMapper.class,
			RetirementDetailsMapper.class,
			StringObjectMapper.class 
	}
)
public interface NeedsAnalysisMapper {

	NeedsAnalysisMapper INSTANCE = Mappers.getMapper(NeedsAnalysisMapper.class);

	
	/**
	 * NeedsAnalysis
	 */
	NeedsAnalysis toNeedsAnalysis(Content.NeedsAnalysis needsAnalysis);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis fromNeedsAnalysis(NeedsAnalysis entity);
	
	
	/**
	 * BenefitToApplicants
	 */
	NeedsAnalysis.BenefitToApplicants toBenefitToApplicants(Content.NeedsAnalysis.BenefitToApplicants benefitsToApplicants);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.BenefitToApplicants fromBenefitToApplicants(NeedsAnalysis.BenefitToApplicants entity);
	
	
	/**
	 * CoApplicantInterview
	 */
	CoApplicantInterview toCoApplicantInterview(Content.NeedsAnalysis.CoApplicantInterview coApplicantInterview);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.CoApplicantInterview fromCoApplicantInterview(CoApplicantInterview entity);
	

	
	/**
	 * Applicant
	 */
	@Mapping(target = "XApplicant", source = "applicant.XApplicant", qualifiedBy = ObjectToString.class)
	Applicant toApplicant(Content.NeedsAnalysis.CoApplicantInterview.Applicant applicant);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	Content.NeedsAnalysis.CoApplicantInterview.Applicant fromApplicant(Applicant entity);

	
	/**
	 * List<Applicant>
	 */
	List<Applicant> toApplicants(List<Content.NeedsAnalysis.CoApplicantInterview.Applicant> applicants);
	
	@InheritInverseConfiguration
	List<Content.NeedsAnalysis.CoApplicantInterview.Applicant> fromApplicants(List<Applicant> entities);
	
	
	/**
	 * Interview
	 */
	
	@Mappings({
		@Mapping(target = "date", source = "interview.date", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XLocation", source = "interview.XLocation", qualifiedBy = ObjectToString.class)
	})
	Interview toInterview(Content.NeedsAnalysis.Interview interview);
	
	@Mappings({
		@Mapping(target = "date", source = "entity.date", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XLocation", source = "entity.XLocation", qualifiedBy = StringToObject.class)
	})
	Content.NeedsAnalysis.Interview fromInterview(Interview entity);
	
	
	/**
	 * Attendee
	 */
	@Mapping(target = "XParty", source = "attendee.XParty", qualifiedBy = ObjectToString.class)
	Interview.Attendee toAttendee(Content.NeedsAnalysis.Interview.Attendee attendee);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	Content.NeedsAnalysis.Interview.Attendee fromAttendee(Interview.Attendee entity);

	
	/**
	 * List<Interview.Attendee>
	 */
	List<Interview.Attendee> toAttendees(List<Content.NeedsAnalysis.Interview.Attendee> attendees);
	
	@InheritInverseConfiguration
	List<Content.NeedsAnalysis.Interview.Attendee> fromAttendees(List<Interview.Attendee> entities);
	
	
	/**
	 * PurposeOfApplication
	 */
	PurposeOfApplication toPurposeOfApplication(Content.NeedsAnalysis.PurposeOfApplication poa);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.PurposeOfApplication fromPurposeOfApplication(PurposeOfApplication entity);

	
	/**
	 * List<PurposeOfApplication>
	 */
	List<PurposeOfApplication> toPurposeOfApplications(List<Content.NeedsAnalysis.PurposeOfApplication> poas);
	
	@InheritInverseConfiguration
	List<Content.NeedsAnalysis.PurposeOfApplication> fromPurposeOfApplications(List<PurposeOfApplication> entities);
	
	
	/**
	 * RefinancingAndConsolidation
	 */
	RefinancingAndConsolidation toRefinancingAndConsolidation(Content.NeedsAnalysis.RefinancingAndConsolidation refinancing);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.RefinancingAndConsolidation fromRefinancingAndConsolidation(RefinancingAndConsolidation entity);

	
	/**
	 * RefinancingReason
	 */
	RefinancingReason toRefinancingReason(Content.NeedsAnalysis.RefinancingAndConsolidation.RefinancingReason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.RefinancingAndConsolidation.RefinancingReason fromRefinancingAndReason(RefinancingReason entity);
		

}
