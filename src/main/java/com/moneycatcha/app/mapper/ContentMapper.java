package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Content;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
		uses = {
			ApplicationMapper.class,
			NeedsAnalysisMapper.class,
			StatementOfPositionMapper.class
		})
public interface ContentMapper {

	ContentMapper INSTANCE = Mappers.getMapper(ContentMapper.class);

	/**
	 * Message.Content
	 */
	Content toContent(Message.Content content);
	
	@InheritInverseConfiguration
	Message.Content fromContent(Content entity);
	
}
