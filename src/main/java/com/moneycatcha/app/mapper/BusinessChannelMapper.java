package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.BusinessChannel;
import com.moneycatcha.app.entity.BusinessChannel.Contact;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", 
	uses = {
		PhoneTypeMapper.class, 
		DurationTypeMapper.class, 
		StringObjectMapper.class
})
public interface BusinessChannelMapper {

	BusinessChannelMapper INSTANCE = Mappers.getMapper(BusinessChannelMapper.class);
	
	/**
	 * BusinessChannel
	 */
	@Mappings({
		@Mapping(target = "abn", source = "bChannel.ABN"),
		@Mapping(target = "acn", source = "bChannel.ACN")
	})
	BusinessChannel toBusinessChannel(Application.BusinessChannel bChannel);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "ABN", source = "entity.abn"),
		@Mapping(target = "ACN", source = "entity.acn")
	})
	Application.BusinessChannel fromBusinessChannel(BusinessChannel entity);
	
	/**
	 * BusinessChannel - Contact
	 */
	@Mapping(target = "XAddress",  source = "contact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toContact(Application.BusinessChannel.Contact contact);
	
	@Mapping(target = "XAddress",  source = "entity.XAddress", qualifiedBy = StringToObject.class)
	Application.BusinessChannel.Contact fromContact(Contact entity);
	
	
	
	/**
	 * BusinessChannel - ContactPerson
	 */
	
	Contact.ContactPerson toContactPerson(Application.BusinessChannel.Contact.ContactPerson person);
	
	@InheritInverseConfiguration
	Application.BusinessChannel.Contact.ContactPerson fromContactPerson(Contact.ContactPerson entity);
	


}
