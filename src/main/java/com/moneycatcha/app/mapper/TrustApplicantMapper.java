package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.DeclaredIncome;
import com.moneycatcha.app.entity.ExistingCustomer;
import com.moneycatcha.app.entity.SourceOfWealth;
import com.moneycatcha.app.entity.TrustApplicant;
import com.moneycatcha.app.entity.TrustDeedVariation;
import com.moneycatcha.app.entity.Trustee;
import com.moneycatcha.app.entity.Contact.ContactPerson;
import com.moneycatcha.app.entity.TrustApplicant.BeneficialOwner;
import com.moneycatcha.app.entity.TrustApplicant.Beneficiary;
import com.moneycatcha.app.entity.TrustApplicant.Settlor;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
	DateMapper.class,
	DurationTypeMapper.class,
	DealingNumberTypeMapper.class,
	FinancialAccountTypeMapper.class,
	FinancialAnalysisMapper.class,
	ForeignTaxAssociationTypeMapper.class,
	IncomePreviousMapper.class,
	IncomePriorMapper.class,
	IncomeRecentMapper.class,
	IncomeYearToDateMapper.class,
	PersonNameTypeMapper.class,
	PhoneTypeMapper.class,
	StringObjectMapper.class,
	TaxDeclarationDetailsTypeMapper.class
})
public interface TrustApplicantMapper {

	TrustApplicantMapper INSTANCE = Mappers.getMapper(TrustApplicantMapper.class);

	
	/**
	 * TrustApplicant
	 */
	@Mappings({
		@Mapping(target = "abn", source = "trustApplicant.ABN"),
		@Mapping(target = "oecdcrsStatus", source = "trustApplicant.OECDCRSStatus"),
		@Mapping(target = "establishmentDate", source = "trustApplicant.establishmentDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "vestingDate", source = "trustApplicant.vestingDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "trustApplicant.XAccountant", qualifiedBy = ObjectToString.class)
	})
	TrustApplicant toTrustApplicant(Application.TrustApplicant trustApplicant);
	
	@Mappings({
		@Mapping(target = "ABN", source = "entity.abn"),
		@Mapping(target = "OECDCRSStatus", source = "entity.oecdcrsStatus"),
		@Mapping(target = "establishmentDate", source = "entity.establishmentDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "vestingDate", source = "entity.vestingDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)
	})
	Application.TrustApplicant fromTrustApplicant(TrustApplicant entity); 

	
	/**
	 * List<TrustApplicant>
	 */
	List<TrustApplicant> toTrustApplicants(List<Application.TrustApplicant> trustApplicants);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant> fromTrustApplicants(List<TrustApplicant> entities);


	
	/**
	 * BeneficialOwner
	 */
	@Mapping(target = "XBeneficialOwner", source = "owner.XBeneficialOwner", qualifiedBy = ObjectToString.class)
	BeneficialOwner toBeneficialOwner(Application.TrustApplicant.BeneficialOwner owner);
	
	@Mapping(target = "XBeneficialOwner", source = "entity.XBeneficialOwner", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.BeneficialOwner fromBeneficialOwner(BeneficialOwner entity);
	
	

	/**
	 * BeneficialOwner - List
	 */
	List<BeneficialOwner> toBeneficialOwners(List<Application.TrustApplicant.BeneficialOwner> owners);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.BeneficialOwner> fromBeneficialOwners(List<BeneficialOwner> entityList);

	
	/**
	 * Beneficiary
	 */
	@Mapping(target = "XBeneficiary", source = "beneficiary.XBeneficiary", qualifiedBy = ObjectToString.class)
	Beneficiary toBeneficiary(Application.TrustApplicant.Beneficiary beneficiary);
	
	@Mapping(target = "XBeneficiary", source = "entity.XBeneficiary", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.Beneficiary fromBeneficiary(Beneficiary entity);
	
	

	/**
	 * List<Beneficiary>
	 */
	List<Beneficiary> toBeneficiaries(List<Application.TrustApplicant.Beneficiary> beneficiaries);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.Beneficiary> fromBeneficiaries(List<Beneficiary> entities);

	
	/**
	 * Business
	 */
	@Mapping(target = "gicsCode", source = "business.GICSCode")
	Business toBusiness(Application.TrustApplicant.Business business);
	
	@InheritInverseConfiguration
	@Mapping(target = "GICSCode", source = "entity.gicsCode")
	Application.TrustApplicant.Business fromBusiness(Business entity);
	

	/**
	 * Contact
	 */
	@Mappings({
		@Mapping(target = "XMailingAddress", source = "contact.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPrincipalTradingAddress", source = "contact.XPrincipalTradingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XRegisteredAddress", source = "contact.XRegisteredAddress", qualifiedBy = ObjectToString.class)
	})
	Contact toContact(Application.TrustApplicant.Contact contact);
	
	@Mappings({
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPrincipalTradingAddress", source = "entity.XPrincipalTradingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XRegisteredAddress", source = "entity.XRegisteredAddress", qualifiedBy = StringToObject.class)
	})
	Application.TrustApplicant.Contact fromContact(Contact entity);
	
	
	/**
	 * ContactPerson
	 */
	@Mapping(target = "XContactPerson", source = "person.XContactPerson", qualifiedBy = ObjectToString.class)
	ContactPerson toContactPerson(Application.TrustApplicant.Contact.ContactPerson person);
	
	@Mapping(target = "XContactPerson", source = "entity.XContactPerson", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.Contact.ContactPerson fromContactPerson(ContactPerson entity);

	
	/**
	 * DeclaredIncome
	 */
	DeclaredIncome toDeclaredIncome(Application.TrustApplicant.DeclaredIncome declaredIncome);
	
	@InheritInverseConfiguration
	Application.TrustApplicant.DeclaredIncome fromDeclaredIncome(DeclaredIncome entity);
	
	
	/**
	 * ExistingCustomer
	 */
	ExistingCustomer toExistingCustomer(Application.TrustApplicant.ExistingCustomer existingCustomer);
	
	@InheritInverseConfiguration
	Application.TrustApplicant.ExistingCustomer fromExistingCustomer(ExistingCustomer entity);
	
	
	/**
	 * Settlor
	 */
	@Mapping(target = "XSettlor", source = "settlor.XSettlor", qualifiedBy = ObjectToString.class)
	Settlor toSettlor(Application.TrustApplicant.Settlor settlor);
	
	@Mapping(target = "XSettlor", source = "entity.XSettlor", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.Settlor fromSettlor(Settlor entity); 

	
	/**
	 * List<SourceOfWealth>
	 */
	List<Settlor> toSettlors(List<Application.TrustApplicant.Settlor> settlors);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.Settlor> fromSettlors(List<Settlor> entities); 

	
	
	/**
	 * SourceOfWealth
	 */
	SourceOfWealth toSourceOfWealth(Application.TrustApplicant.SourceOfWealth sow);
	
	@InheritInverseConfiguration
	Application.TrustApplicant.SourceOfWealth fromSourceOfWealth(SourceOfWealth entity); 

	
	/**
	 * List<SourceOfWealth>
	 */
	List<SourceOfWealth> toSourceOfWealthList(List<Application.TrustApplicant.SourceOfWealth> sowList);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.SourceOfWealth> fromSourceOfWealthList(List<SourceOfWealth> entities); 

	
	
	/**
	 * TrustDeedVariation
	 */
	@Mapping(target = "date", source = "trustdeed.date", qualifiedBy = GregorianToLocal.class)
	TrustDeedVariation toTrustDeedVariation(Application.TrustApplicant.TrustDeedVariation trustdeed);
	
	@InheritInverseConfiguration
	@Mapping(target = "date", source = "entity.date", qualifiedBy = LocalToGregorian.class)
	Application.TrustApplicant.TrustDeedVariation fromTrustDeedVariation(TrustDeedVariation entity); 

	
	/**
	 * List<TrustDeedVariation>
	 */
	List<SourceOfWealth> toTrustDeedVariations(List<Application.TrustApplicant.TrustDeedVariation> deedVariations);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.TrustDeedVariation> fromTrustDeedVariations(List<TrustDeedVariation> entities); 

	
	/**
	 * Trustee
	 */
	@Mapping(target = "XTrustee", source = "trustee.XTrustee", qualifiedBy = ObjectToString.class)
	Trustee toTrustee(Application.TrustApplicant.Trustee trustee);
	
	@Mapping(target = "XTrustee", source = "entity.XTrustee", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.Trustee fromTrustee(Trustee entity); 

	
	/**
	 * List<Trustee>
	 */
	List<Trustee> toTrustees(List<Application.TrustApplicant.Trustee> trustees);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.Trustee> fromTrustees(List<Trustee> entities); 

	
	
}
