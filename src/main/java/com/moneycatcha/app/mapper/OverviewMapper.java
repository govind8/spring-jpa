package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.Overview;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		PercentOwnedTypeMapper.class,
		PhoneTypeMapper.class,
		SignatureTypeMapper.class,
		StringObjectMapper.class
	})
public interface OverviewMapper {

	OverviewMapper INSTANCE = Mappers.getMapper(OverviewMapper.class);

	/**
	 * Overview
	 */
	@Mappings({
		@Mapping(target = "expectedSettlementDate", source = "overview.expectedSettlementDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "smsfLoan", source = "overview.SMSFLoan"),
		@Mapping(target = "XMainContactPoint", source = "overview.XMainContactPoint", qualifiedBy = ObjectToString.class)
	})
	Overview toOverview(Application.Overview overview);
	
	@Mappings({
		@Mapping(target = "expectedSettlementDate", source = "entity.expectedSettlementDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "SMSFLoan", source = "entity.smsfLoan"),
		@Mapping(target = "XMainContactPoint", source = "entity.XMainContactPoint", qualifiedBy = StringToObject.class)
	})
	Application.Overview fromOverview(Overview entity); 

	
	
	/**
	 * Overview.BranchDomicile
	 */
	@Mapping(target = "bsb", source = "branchDomicile.BSB")
	Overview.BranchDomicile toBranchDomicile(Application.Overview.BranchDomicile branchDomicile);
	
	@InheritInverseConfiguration
	@Mapping(target = "BSB", source = "entity.bsb")
	Application.Overview.BranchDomicile fromBranchDomicile(Overview.BranchDomicile entity); 

	
	/**
	 * Overview - Contact - BranchDomicile
	 */
	
	Contact toBranchDomicileContact(Application.Overview.BranchDomicile.Contact contact);
	
	@InheritInverseConfiguration
	Application.Overview.BranchDomicile.Contact fromBranchDomicileContact(Contact entity);


	/**
	 * Overview.BranchSign
	 */
	@Mapping(target = "bsb", source = "branchSign.BSB")
	Overview.BranchSign toBranchSign(Application.Overview.BranchSign branchSign);
	
	@InheritInverseConfiguration
	@Mapping(target = "BSB", source = "entity.bsb")
	Application.Overview.BranchSign fromBranchSign(Overview.BranchSign entity); 



	/**
	 * Overview - Contact - BranchSign
	 */
	Contact toBranchSignContact(Application.Overview.BranchSign.Contact contact);
	
	@InheritInverseConfiguration
	Application.Overview.BranchSign.Contact fromBranchSignContact(Contact entity);



	
	/**
	 * Overview.BranchStamp
	 */
	Overview.BranchStamp toBranchStamp(Application.Overview.BranchStamp branchStamp);
	
	@InheritInverseConfiguration
	Application.Overview.BranchStamp fromBranchStamp(Overview.BranchStamp entity); 


	/**
	 * Overview.BridgingFinance
	 */
	Overview.BridgingFinance toBridgingFinance(Application.Overview.BridgingFinance bridgingFinance);
	
	@InheritInverseConfiguration
	Application.Overview.BridgingFinance fromBridgingFinance(Overview.BridgingFinance entity); 

	
	
}
