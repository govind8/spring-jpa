package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Application;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {
		AccountVariationMapper.class, 
		AddressTypeMapper.class, 
		BusinessChannelMapper.class,
		CompanyApplicantMapper.class, 
		CompanyFinancialsMapper.class, 
		ContributionFundsMapper.class,
		CustomerTransactionAnalysisMapper.class, 
		DeclarationsMapper.class, 
		DepositAccountDetailsMapper.class,
		DetailedCommentMapper.class, 
		HouseholdMapper.class, 
		InsuranceMapper.class,
		LendingGuaranteeMapper.class, 
		LiabilityMapper.class,
		LoanDetailsMapper.class,
		MasterAgreementMapper.class,
		NonRealEstateAssetMapper.class,
		OtherExpenseMapper.class, 
		OtherIncomeMapper.class, 
		OverviewMapper.class,
		PersonApplicantMapper.class,
		ProductPackageMapper.class, 
		ProductSetMapper.class,
		RealEstateAssetMapper.class, 
		RelatedCompanyMapper.class,
		RelatedPersonMapper.class,
		SalesChannelMapper.class,
		SettlementMapper.class,
		SplitLoanMapper.class,
		SummaryMapper.class,
		TrustApplicantMapper.class,
		VendorTaxInvoiceTypeMapper.class})
public interface ApplicationMapper {

	ApplicationMapper INSTANCE = Mappers.getMapper(ApplicationMapper.class);
	
	/**
	 * Application
	 */
	
	@Mappings({
		@Mapping(target = "accountVariation", source="model.accountVariation"),
		@Mapping(target = "address", source="model.address")
	})
	Application toApplication(Message.Content.Application model);

	@InheritInverseConfiguration
	Message.Content.Application fromApplication(Application entity);
	
	
	

}
