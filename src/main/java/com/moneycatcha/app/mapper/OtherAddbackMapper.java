package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Addback.OtherAddback;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant.Employment.SelfEmployed;
import com.moneycatcha.app.model.Message.Content.Application.TrustApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface OtherAddbackMapper {
	
	/**
	 * CompanyApplicant.IncomePrevious.Addback.OtherAddback
	 */
	OtherAddback toCAIncomePreviousOtherAddback(CompanyApplicant.IncomePrevious.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomePrevious.Addback.OtherAddback fromCAIncomePreviousOtherAddback(OtherAddback entity);

	List<OtherAddback> toCAIncomePreviousOtherAddbacks(List<CompanyApplicant.IncomePrevious.Addback.OtherAddback> otherAddbacks);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.IncomePrevious.Addback.OtherAddback> fromCAIncomePreviousOtherAddbacks(List<OtherAddback> entities);

	/**
	 * CompanyApplicant.IncomeRecent.Addback.OtherAddback
	 */
	OtherAddback toCAIncomeRecentOtherAddback(CompanyApplicant.IncomeRecent.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomeRecent.Addback.OtherAddback fromCAIncomeRecentOtherAddback(OtherAddback entity);

	List<OtherAddback> toCAIncomeRecentOtherAddbacks(List<CompanyApplicant.IncomeRecent.Addback.OtherAddback> otherAddbacks);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.IncomeRecent.Addback.OtherAddback> fromCAIncomeRecentOtherAddbacks(List<OtherAddback> entities);
	
	/**
	 * CompanyApplicant.IncomePrior.Addback.OtherAddback
	 */
	OtherAddback toCAIncomePriorOtherAddback(CompanyApplicant.IncomePrior.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomePrior.Addback.OtherAddback fromCAIncomePriorOtherAddback(OtherAddback entity);

	List<OtherAddback> toCAIncomePriorOtherAddbacks(List<CompanyApplicant.IncomePrior.Addback.OtherAddback> otherAddbacks);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.IncomePrior.Addback.OtherAddback> fromCAIncomePriorOtherAddbacks(List<OtherAddback> entities);

	/**
	 * CompanyApplicant.IncomeYearToDate.Addback.OtherAddback
	 */
	OtherAddback toCAIncomeYearToDateOtherAddback(CompanyApplicant.IncomeYearToDate.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomeYearToDate.Addback.OtherAddback fromCAIncomeYearToDateOtherAddback(OtherAddback entity);

	List<OtherAddback> toCAIncomeYearToDateOtherAddbacks(List<CompanyApplicant.IncomeYearToDate.Addback.OtherAddback> otherAddbacks);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.IncomeYearToDate.Addback.OtherAddback> fromCAIncomeYearToDateOtherAddbacks(List<OtherAddback> entities);
	
	
	

	/**
	 * BusinessIncomePrevious.Addback.OtherAddback
	 */
	OtherAddback toBusinessIncomePreviousOtherAddback(SelfEmployed.BusinessIncomePrevious.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomePrevious.Addback.OtherAddback fromBusinessIncomePreviousOtherAddback(OtherAddback entity);

	List<OtherAddback> toBusinessIncomePreviousOtherAddbacks(List<SelfEmployed.BusinessIncomePrevious.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<SelfEmployed.BusinessIncomePrevious.Addback.OtherAddback> fromBusinessIncomePreviousOtherAddbacks(List<OtherAddback> entity);

	/**
	 * BusinessIncomeRecent.Addback.OtherAddback
	 */
	OtherAddback toBusinessIncomeRecentOtherAddback(SelfEmployed.BusinessIncomeRecent.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomeRecent.Addback.OtherAddback fromBusinessIncomeRecentOtherAddback(OtherAddback entity);

	List<OtherAddback> toBusinessIncomeRecentOtherAddbacks(List<SelfEmployed.BusinessIncomeRecent.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<SelfEmployed.BusinessIncomeRecent.Addback.OtherAddback> fromBusinessIncomeRecentOtherAddbacks(List<OtherAddback> entity);
	
	/**
	 * BusinessIncomePrior.Addback.OtherAddback
	 */
	OtherAddback toBusinessIncomePriorOtherAddback(SelfEmployed.BusinessIncomePrior.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomePrior.Addback.OtherAddback fromBusinessIncomePriorOtherAddback(OtherAddback entity);

	List<OtherAddback> toBusinessIncomePriorOtherAddbacks(List<SelfEmployed.BusinessIncomePrior.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<SelfEmployed.BusinessIncomePrior.Addback.OtherAddback> fromBusinessIncomePriorOtherAddbacks(List<OtherAddback> entity);

	/**
	 * BusinessIncomeYearToDate.Addback.OtherAddback
	 */
	OtherAddback toBusinessIncomeYearToDateOtherAddback(SelfEmployed.BusinessIncomeYearToDate.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomeYearToDate.Addback.OtherAddback fromBusinessIncomeYearToDateOtherAddback(OtherAddback entity);

	List<OtherAddback> toBusinessIncomeYearToDateOtherAddbacks(List<SelfEmployed.BusinessIncomeYearToDate.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<SelfEmployed.BusinessIncomeYearToDate.Addback.OtherAddback> fromBusinessIncomeYearToDateOtherAddbacks(List<OtherAddback> entity);
	

	
	/**
	 * TrustApplicant.IncomePrevious.Addback.OtherAddback
	 */
	OtherAddback toTAIncomePreviousOtherAddback(TrustApplicant.IncomePrevious.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomePrevious.Addback.OtherAddback fromTAIncomePreviousOtherAddback(OtherAddback entity);

	List<OtherAddback> toTAIncomePreviousOtherAddbacks(List<TrustApplicant.IncomePrevious.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<TrustApplicant.IncomePrevious.Addback.OtherAddback> fromTAIncomePreviousOtherAddbacks(List<OtherAddback> entity);

	/**
	 * TrustApplicant.IncomeRecent.Addback.OtherAddback
	 */
	OtherAddback toTAIncomeRecentOtherAddback(TrustApplicant.IncomeRecent.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomeRecent.Addback.OtherAddback fromTAIncomeRecentOtherAddback(OtherAddback entity);

	List<OtherAddback> toTAIncomeRecentOtherAddbacks(List<TrustApplicant.IncomeRecent.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<TrustApplicant.IncomeRecent.Addback.OtherAddback> fromTAIncomeRecentOtherAddbacks(List<OtherAddback> entity);
	
	/**
	 * TrustApplicant.IncomePrior.Addback.OtherAddback
	 */
	OtherAddback toTAIncomePriorOtherAddback(TrustApplicant.IncomePrior.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomePrior.Addback.OtherAddback fromTAIncomePriorOtherAddback(OtherAddback entity);

	List<OtherAddback> toTAIncomePriorOtherAddbacks(List<TrustApplicant.IncomePrior.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<TrustApplicant.IncomePrior.Addback.OtherAddback> fromTAIncomePriorOtherAddbacks(List<OtherAddback> entity);

	/**
	 * TrustApplicant.IncomeYearToDate.Addback.OtherAddback
	 */
	OtherAddback toTAIncomeYearToDateOtherAddback(TrustApplicant.IncomeYearToDate.Addback.OtherAddback otherAddback);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomeYearToDate.Addback.OtherAddback fromTAIncomeYearToDateOtherAddback(OtherAddback entity);

	List<OtherAddback> toTAIncomeYearToDateOtherAddbacks(List<TrustApplicant.IncomeYearToDate.Addback.OtherAddback> otherAddback);
	
	@InheritInverseConfiguration
	List<TrustApplicant.IncomeYearToDate.Addback.OtherAddback> fromTAIncomeYearToDateOtherAddbacks(List<OtherAddback> entity);
	
}
