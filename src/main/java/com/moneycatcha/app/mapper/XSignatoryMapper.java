package com.moneycatcha.app.mapper;

import com.moneycatcha.app.mapper.annotations.XSignatoryReference;
import com.moneycatcha.app.model.Message;

import org.springframework.lang.Nullable;

public class XSignatoryMapper {

	private XSignatoryMapper() { }
	
	@XSignatoryReference
	public static String toXSignatoryReference(@Nullable Object object) {
		if (object != null) {
			if (object instanceof Message.Content.Application.CompanyApplicant) {
				return ((Message.Content.Application.CompanyApplicant) object).getUniqueID();
			} else {
				return ((Message.Content.Application.RelatedPerson) object).getUniqueID();
			}
		}
		return "";
	}
}
