package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Arrears;
import com.moneycatcha.app.entity.OtherExpense;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		PercentOwnedTypeMapper.class
	})
public interface OtherExpenseMapper {

	OtherExpenseMapper INSTANCE = Mappers.getMapper(OtherExpenseMapper.class);

	/**
	 * OtherExpense
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "expense.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "expense.endDate", qualifiedBy = GregorianToLocal.class)
	})
	OtherExpense toOtherExpense(Application.OtherExpense expense);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.OtherExpense fromOtherExpense(OtherExpense entity); 

	
	/**
	 * List<OtherExpense>
	 */
	List<OtherExpense> toOtherExpenses(List<Application.OtherExpense> otherExpenses);
	
	@InheritInverseConfiguration
	List<Application.OtherExpense> fromOtherExpenses(List<OtherExpense> entities);
	
	
	/**
	 * Arrears
	 */
	Arrears toArrears(Application.OtherExpense.Arrears expense);
	
	@InheritInverseConfiguration
	Application.OtherExpense.Arrears fromArrears(Arrears entity); 


	
}
