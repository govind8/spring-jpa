package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.FinancialAccountType.BranchDomicile;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface BranchDomicileMapper {

	BranchDomicileMapper INSTANCE = Mappers.getMapper(BranchDomicileMapper.class);

	/**
	 * FinancialAccountType.BranchDomicile
	 */
	BranchDomicile toBranchDomicile(com.moneycatcha.app.model.FinancialAccountType.BranchDomicile domicile);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.FinancialAccountType.BranchDomicile fromBranchDomicile(BranchDomicile entity);


}
