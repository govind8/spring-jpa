package com.moneycatcha.app.mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.springframework.lang.Nullable;

import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.GregorianToYear;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.YearToGregorian;

public class DateMapper {
	
	private DateMapper() { }
	
	@LocalToGregorian
	public static XMLGregorianCalendar localToGregorian(@Nullable LocalDate localDate) throws DatatypeConfigurationException {
		
		if (localDate != null) {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(localDate.toString());
		}
		return null;
	}
	
	@GregorianToLocal
    public static LocalDate gregorianToLocal(@Nullable XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar != null) {
		        return LocalDate.of(
		                xmlGregorianCalendar.getYear(),
		                xmlGregorianCalendar.getMonth(),
		                xmlGregorianCalendar.getDay()
		        );
		}
		return null;
    }

	@LocalDateTimeToGregorian
	public static XMLGregorianCalendar localDateTimeToGregorian(@Nullable LocalDateTime localDateTime) throws DatatypeConfigurationException {
		if (localDateTime != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(localDateTime.format(formatter).toString());
		}
		return null;
	}
	
	@GregorianToLocalDateTime
    public static LocalDateTime gregorianToLocalDateTime(@Nullable XMLGregorianCalendar xmlGregorianCalendar) {
		if (xmlGregorianCalendar != null) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
			String slocalDateTime = xmlGregorianCalendar.toGregorianCalendar().toZonedDateTime().toLocalDateTime().format(formatter);
			return LocalDateTime.parse(slocalDateTime, formatter);
		}
		return null;
    }
	
	/**
	 * Convert XMLGregorianCalendar to Year
	 * @param xmlGregorianCalendar
	 * @return
	 */
	@GregorianToYear
	public static int gregorianToYear(@Nullable XMLGregorianCalendar xmlGregorianCalendar) {
		
		if (xmlGregorianCalendar != null) {
	        return xmlGregorianCalendar.getYear();
		}
		return -1;
	}
	
	/**
	 * convert year to XMLGregorianCalendar
	 * @param year
	 * @return
	 * @throws DatatypeConfigurationException
	 */
	@YearToGregorian
	public static XMLGregorianCalendar yearToGregorian(@Nullable int year) throws DatatypeConfigurationException {
		
		if (year != 0  && year != -1) {
			String syear = String.valueOf(year);
	        return DatatypeFactory.newInstance().newXMLGregorianCalendar(syear);
		}
		return null;
	}
		
}
