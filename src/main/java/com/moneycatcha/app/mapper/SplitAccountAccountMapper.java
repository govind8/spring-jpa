package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.AccountVariation.SplitAccount.Account;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.AccountVariation;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
			StringObjectMapper.class
	}
)
public interface SplitAccountAccountMapper {

	/**
	 * SplitAccount::Account 
	 */
	@Mapping(target = "XAccountDetails", source = "account.XAccountDetails", qualifiedBy = ObjectToString.class)
	Account toAccount(AccountVariation.SplitAccount.Account account);
	
	@Mapping(target = "XAccountDetails", source = "entity.XAccountDetails", qualifiedBy = StringToObject.class)
	AccountVariation.SplitAccount.Account fromAccount(Account entity);
	
	
	
	/**
	 * SplitAccount::Accounts - List
	 */
	List<Account> toPartners(List<AccountVariation.SplitAccount.Account> accounts);
	
	@InheritInverseConfiguration
	List<AccountVariation.SplitAccount.Account> fromPartners(List<Account> entityList);

	
	
}
