package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.IncomeYearToDate;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {AddbackMapper.class,
		DateMapper.class,
		StringObjectMapper.class
})
public interface IncomeYearToDateMapper {

	/**
	 * Company Applicant IncomeYearToDate
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomeYearToDate.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomeYearToDate.endDate", qualifiedBy = GregorianToLocal.class)
	})
	IncomeYearToDate toCAIncomeYearToDate(Application.CompanyApplicant.IncomeYearToDate incomeYearToDate);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.CompanyApplicant.IncomeYearToDate fromCAIncomeYearToDate(IncomeYearToDate entity);

	
	/**
	 * Company Applicant IncomeYearToDate
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomeYearToDate.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomeYearToDate.endDate", qualifiedBy = GregorianToLocal.class)
	})
	IncomeYearToDate toTAIncomeYearToDate(Application.TrustApplicant.IncomeYearToDate incomeYearToDate);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.TrustApplicant.IncomeYearToDate fromTAIncomeYearToDate(IncomeYearToDate entity);

	
	
}
