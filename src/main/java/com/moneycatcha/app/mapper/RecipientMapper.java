package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Recipient;
import com.moneycatcha.app.entity.Software;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface RecipientMapper {

	RecipientMapper INSTANCE = Mappers.getMapper(RecipientMapper.class);

	/**
	 * Recipient
	 */
	@Mapping(target = "lixiCode", source = "recipient.LIXICode")
	Recipient toRecipient(Message.Recipient recipient);
	
	@InheritInverseConfiguration
	@Mapping(target = "LIXICode", source = "entity.lixiCode")
	Message.Recipient fromRecipient(Recipient entity);
	

	/**
	 * List<Recipient>
	 */
	List<Recipient> toRecipients(List<Message.Recipient> recipients);
	
	@InheritInverseConfiguration
	List<Message.Recipient> fromRecipients(List<Recipient> entity);


	/**
	 * Software
	 */
	
	Software toSoftware(Message.Recipient.Software software);
	
	@InheritInverseConfiguration
	Message.Recipient.Software fromSoftware(Software entity);


}
