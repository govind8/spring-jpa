package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Shareholder;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface ShareholderMapper {

	/**
	 * Shareholder 
	 */
	@Mapping(target = "XShareholder", source = "holder.XShareholder", qualifiedBy = ObjectToString.class)
	Shareholder toShareholder(Application.CompanyApplicant.Shareholder holder);
	
	@Mapping(target = "XShareholder", source = "entity.XShareholder", qualifiedBy = StringToObject.class)
	Application.CompanyApplicant.Shareholder fromShareholder(Shareholder entity);
	
	
	/**
	 * Shareholders - List
	 */
	List<Shareholder> toShareholders(List<Application.CompanyApplicant.Shareholder> holders);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant.Shareholder> fromShareholders(List<Shareholder> entityList);
	

}
