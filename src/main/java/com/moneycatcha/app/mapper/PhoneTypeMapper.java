package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.PhoneType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface PhoneTypeMapper {
	
	PhoneTypeMapper INSTANCE = Mappers.getMapper(PhoneTypeMapper.class);
	
	/**
	 * PhoneType
	 */
	
	PhoneType toPhoneType(com.moneycatcha.app.model.PhoneType phoneType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.PhoneType fromPhoneType(PhoneType entity);
	
}
