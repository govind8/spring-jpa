package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.SplitLoan;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
uses = {
		DateMapper.class,
		PhoneTypeMapper.class,
		StringObjectMapper.class
})
public interface SplitLoanMapper {

	SplitLoanMapper INSTANCE = Mappers.getMapper(SplitLoanMapper.class);

	/**
	 * SplitLoan
	 */
	SplitLoan toSplitLoan(Application.SplitLoan splitLoan);
	
	@InheritInverseConfiguration
	Application.SplitLoan fromSplitLoan(SplitLoan entity); 

	
	/**
	 * List<SplitLoan>
	 */
	List<SplitLoan> toSplitLoans(List<Application.SplitLoan> splitLoans);
	
	@InheritInverseConfiguration
	List<Application.SplitLoan> fromSplitLoans(List<SplitLoan> entities);


	/**
	 * SplitLoanComponent
	 */
	@Mappings({
		@Mapping(target = "XLiability", source = "splitLoanComponent.XLiability", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XLoanDetails", source = "splitLoanComponent.XLoanDetails", qualifiedBy = ObjectToString.class)
	})
	SplitLoan.SplitLoanComponent toSplitLoanComponent(Application.SplitLoan.SplitLoanComponent splitLoanComponent);
	
	@Mappings({
		@Mapping(target = "XLiability", source = "entity.XLiability", qualifiedBy = StringToObject.class),
		@Mapping(target = "XLoanDetails", source = "entity.XLoanDetails", qualifiedBy = StringToObject.class)
	})
	Application.SplitLoan.SplitLoanComponent fromSplitLoanComponent(SplitLoan.SplitLoanComponent entity);

	
	/**
	 * List<SplitLoanComponent>
	 */
	List<SplitLoan.SplitLoanComponent> toSplitLoanComponents(List<Application.SplitLoan.SplitLoanComponent> components);
	
	@InheritInverseConfiguration
	List<Application.SplitLoan.SplitLoanComponent> fromSplitLoanComponents(List<SplitLoan.SplitLoanComponent> entities);

}
