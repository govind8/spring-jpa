package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.DealingNumberType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface DealingNumberTypeMapper {

	DealingNumberType toDealingNumberType(com.moneycatcha.app.model.DealingNumberType dealingNumberType);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.DealingNumberType fromDealingNumberType(DealingNumberType entity);
}
