package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.DetailedComment;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface DetailedCommentMapper {

	DetailedCommentMapper INSTANCE = Mappers.getMapper(DetailedCommentMapper.class);

	/**
	 * DetailedComment
	 */
	@Mappings({
		@Mapping(target = "XAuthor", source = "comment.XAuthor", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XContext", source = "comment.XContext", qualifiedBy = ObjectToString.class)
	})
	DetailedComment toDetailedComment(Application.DetailedComment comment);
	
	@Mappings({
		@Mapping(target = "XAuthor", source = "entity.XAuthor", qualifiedBy = StringToObject.class),
		@Mapping(target = "XContext", source = "entity.XContext", qualifiedBy = StringToObject.class)
	})
	Application.DetailedComment fromDetailedComment(DetailedComment entity);
	
	/**
	 * List<DetailedComment>
	 */
	List<DetailedComment> toDetailedComments(List<Application.DetailedComment> comments);
	
	@InheritInverseConfiguration
	List<Application.DetailedComment> fromDetailedComments(List<DetailedComment> entities);
	
}
