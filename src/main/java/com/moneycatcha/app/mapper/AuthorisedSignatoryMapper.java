package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.AuthorisedSignatory;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
		uses = {
			StringObjectMapper.class
		}
)
public interface AuthorisedSignatoryMapper {

	/**
	 * AuthorisedSignatory
	 */
	@Mapping(target = "XSignatory", source = "signatory.XSignatory",  qualifiedBy = ObjectToString.class)

	AuthorisedSignatory toAuthorisedSignatory(CompanyApplicant.AuthorisedSignatory signatory);
	
	
	@Mapping(target = "XSignatory", source = "entity.XSignatory", qualifiedBy = StringToObject.class)
	CompanyApplicant.AuthorisedSignatory fromAuthorisedSignatory(AuthorisedSignatory entity);
	
	
	/**
	 * AuthorisedSignatory - List
	 */
	List<AuthorisedSignatory> toAuthorisedSignatories(List<CompanyApplicant.AuthorisedSignatory> signatories);
	
	@InheritInverseConfiguration
	List<CompanyApplicant.AuthorisedSignatory> fromAuthorisedSignatories(List<AuthorisedSignatory> entityList);

}
