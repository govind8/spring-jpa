package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.CompanyApplicant;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {
		AuthorisedSignatoryMapper.class,
		BeneficialOwnerMapper.class,
		BusinessMapper.class,
		ContactMapper.class,
		CreditHistoryMapper.class,
		DeclaredIncomeMapper.class,
		DirectorMapper.class,
		ExistingCustomerMapper.class,
		FinancialAnalysisMapper.class,
		ForeignTaxAssociationTypeMapper.class,
		IncomePreviousMapper.class,
		IncomePriorMapper.class,
		IncomeRecentMapper.class,
		IncomeYearToDateMapper.class,
		PartnerMapper.class,
		RelatedLegalEntitiesMapper.class,
		ResponsibleLendingTypeMapper.class,
		ShareholderMapper.class,
		SourceOfWealthMapper.class,
		DateMapper.class,
		StringObjectMapper.class
})
public interface CompanyApplicantMapper {

	CompanyApplicantMapper INSTANCE = Mappers.getMapper(CompanyApplicantMapper.class);

	/**
	 * CompanyApplicant
	 */
	@Mappings({
		@Mapping(target = "abn", source = "companyApplicant.ABN"),
		@Mapping(target = "abnVerified", source = "companyApplicant.ABNVerified"),
		@Mapping(target = "acn", source = "companyApplicant.ACN"),
		@Mapping(target = "arbn", source = "companyApplicant.ARBN"),
		@Mapping(target = "arsn", source = "companyApplicant.ARSN"),
		@Mapping(target = "oecdcrsStatus", source = "companyApplicant.OECDCRSStatus"),
		@Mapping(target = "gstRegisteredDate", source = "companyApplicant.GSTRegisteredDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "companyApplicant.XAccountant", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XSoleTrader", source = "companyApplicant.XSoleTrader", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XSolicitor", source = "companyApplicant.XSolicitor", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XTradeReference", source = "companyApplicant.XTradeReference", qualifiedBy = ObjectToString.class)
	})
	CompanyApplicant toCompanyApplicant(Application.CompanyApplicant companyApplicant);
	
	
	@Mappings({
		@Mapping(target = "ABN", source = "entity.abn"),
		@Mapping(target = "ABNVerified", source = "entity.abnVerified"),
		@Mapping(target = "ACN", source = "entity.acn"),
		@Mapping(target = "ARBN", source = "entity.arbn"),
		@Mapping(target = "ARSN", source = "entity.arsn"),
		@Mapping(target = "OECDCRSStatus", source = "entity.oecdcrsStatus"),
		@Mapping(target = "GSTRegisteredDate", source = "entity.gstRegisteredDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class),
		@Mapping(target = "XSoleTrader", source = "entity.XSoleTrader", qualifiedBy = StringToObject.class),
		@Mapping(target = "XSolicitor", source = "entity.XSolicitor", qualifiedBy = StringToObject.class),
		@Mapping(target = "XTradeReference", source = "entity.XTradeReference", qualifiedBy = StringToObject.class)
	})
	Application.CompanyApplicant fromCompanyApplicant(CompanyApplicant entity);
	
	
	/**
	 * CompanyApplicant - List
	 */
	List<CompanyApplicant> toCompanyApplicants(List<Application.CompanyApplicant> companyApplicants);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant> fromCompanyApplicants(List<CompanyApplicant> entityList);


}
