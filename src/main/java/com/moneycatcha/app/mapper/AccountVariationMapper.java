package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AccountVariation;
import com.moneycatcha.app.entity.AccountVariation.AddBorrower;
import com.moneycatcha.app.entity.AccountVariation.AddGuarantee;
import com.moneycatcha.app.entity.AccountVariation.AddGuaranteeSecurity;
import com.moneycatcha.app.entity.AccountVariation.AddGuarantor;
import com.moneycatcha.app.entity.AccountVariation.BalanceIncrease;
import com.moneycatcha.app.entity.AccountVariation.ChangeLoanTerm;
import com.moneycatcha.app.entity.AccountVariation.ChangeRepaymentAmount;
import com.moneycatcha.app.entity.AccountVariation.ChangeRepaymentFrequency;
import com.moneycatcha.app.entity.AccountVariation.ChangeRepaymentType;
import com.moneycatcha.app.entity.AccountVariation.CloseAccount;
import com.moneycatcha.app.entity.AccountVariation.InterestRateDiscount;
import com.moneycatcha.app.entity.AccountVariation.LendingGuaranteeLimitIncrease;
import com.moneycatcha.app.entity.AccountVariation.LimitIncrease;
import com.moneycatcha.app.entity.AccountVariation.ReduceBalance;
import com.moneycatcha.app.entity.AccountVariation.ReduceLimit;
import com.moneycatcha.app.entity.AccountVariation.ReleaseGuarantee;
import com.moneycatcha.app.entity.AccountVariation.ReleaseGuaranteeSecurity;
import com.moneycatcha.app.entity.AccountVariation.ReleaseGuarantor;
import com.moneycatcha.app.entity.AccountVariation.ReleaseSecurity;
import com.moneycatcha.app.entity.AccountVariation.RemoveBorrower;
import com.moneycatcha.app.entity.AccountVariation.SplitAccount;
import com.moneycatcha.app.entity.AccountVariation.SwitchProduct;
import com.moneycatcha.app.entity.AccountVariation.UpdatePartyDetails;
import com.moneycatcha.app.entity.AccountVariation.SplitAccount.Account;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
	uses = {
		AmountInForeignCurrencyTypeMapper.class, 
		DateMapper.class,
		StringObjectMapper.class
		})
public interface AccountVariationMapper {

	AccountVariationMapper INSTANCE = Mappers.getMapper(AccountVariationMapper.class);
	
	/**
	 * AccountVariation
	 */
	@Mappings({
		@Mapping(target = "XAccountToVary", source = "mav.XAccountToVary", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XLendingGuaranteeToVary", source = "mav.XLendingGuaranteeToVary", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPostVariationAccount", source = "mav.XPostVariationAccount", qualifiedBy = ObjectToString.class)
	})
	AccountVariation toAccountVariation(Application.AccountVariation mav);
	
	
	@Mappings({
		@Mapping(target = "XAccountToVary", source = "entity.XAccountToVary", qualifiedBy = StringToObject.class),
		@Mapping(target = "XLendingGuaranteeToVary", source = "entity.XLendingGuaranteeToVary", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPostVariationAccount", source = "entity.XPostVariationAccount", qualifiedBy = StringToObject.class)
	})
	Application.AccountVariation fromAccountVariation(AccountVariation entity);
	
	
	/**
	 * List<AccountVariation>
	 */
	List<AccountVariation> toAccountVariations(List<Application.AccountVariation> avlist);
	
	List<Application.AccountVariation> fromAccountVariations(List<AccountVariation> entityList);
	
	
	/**
	 * AddBorrower
	 */
	@Mapping(target = "XBorrower", source = "mab.XBorrower",  qualifiedBy = ObjectToString.class)
	AddBorrower toAddBorrower(Application.AccountVariation.AddBorrower mab);
	
	
	@Mapping(target = "XBorrower", source = "entity.XBorrower",  qualifiedBy = StringToObject.class)
	Application.AccountVariation.AddBorrower fromAddBorrower(AddBorrower entity);

	
	/**
	 * AddGuarantee
	 */
	@Mapping(target = "XLendingGuarantee", source = "mag.XLendingGuarantee", qualifiedBy = ObjectToString.class)
	AddGuarantee toEntityAddGuarantee(Application.AccountVariation.AddGuarantee mag);

	
	@Mapping(target = "XLendingGuarantee", source = "entity.XLendingGuarantee", qualifiedBy = StringToObject.class)
	Application.AccountVariation.AddGuarantee fromAddGuarantee(AddGuarantee entity);

	
	/**
	 * AddGuaranteeSecurity
	 */
	@Mappings({
		@Mapping(source = "mags.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(source = "mags.XSecurity", target = "XSecurity", qualifiedBy = ObjectToString.class)
	})
	AddGuaranteeSecurity toAddGuaranteeSecurity(Application.AccountVariation.AddGuaranteeSecurity mags);
	
	
	@Mappings({
		@Mapping(target = "actionDate", source = "entity.actionDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	})
	Application.AccountVariation.AddGuaranteeSecurity fromAddGuaranteeSecurity(AddGuaranteeSecurity entity);

	
	/**
	 * AddGuarantor
	 */
	@Mapping(target = "XGuarantor", source = "ag.XGuarantor", qualifiedBy = ObjectToString.class)
	AddGuarantor toAddGuarantor(Application.AccountVariation.AddGuarantor ag);
	
	
	@Mapping(target = "XGuarantor", source = "entity.XGuarantor", qualifiedBy = StringToObject.class)
	Application.AccountVariation.AddGuarantor fromAddGuarantor(AddGuarantor entity);

	
	/**
	 * AddSecurity
	 */
	@Mappings({
		@Mapping(target = "actionDate", source = "mas.actionDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XSecurity", source = "mas.XSecurity", qualifiedBy = ObjectToString.class)
	})
	AccountVariation.AddSecurity toAddSecurity(Application.AccountVariation.AddSecurity mas);
	
	
	@Mappings({
		@Mapping(target = "actionDate", source = "entity.actionDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	})
	Application.AccountVariation.AddSecurity fromAddSecurity(AccountVariation.AddSecurity entity);

	
	/**
	 * BalanceIncrease
	 */
	@Mapping(target = "actionDate", source = "mbi.actionDate", qualifiedBy = GregorianToLocal.class)
	BalanceIncrease toBalanceIncrease(Application.AccountVariation.BalanceIncrease mbi);
	
	
	@Mapping(target = "actionDate", source = "entity.actionDate", qualifiedBy = LocalToGregorian.class)
	Application.AccountVariation.BalanceIncrease fromBalanceIncrease(BalanceIncrease entity);
	
	
	/**
	 * ChangeLoanTerm
	 */
	@Mapping(source = "loanTerm.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class)
	ChangeLoanTerm toChangeLoanTerm(Application.AccountVariation.ChangeLoanTerm loanTerm);
	
	
	@Mapping(source = "entity.actionDate", target = "actionDate", qualifiedBy = LocalToGregorian.class)
	Application.AccountVariation.ChangeLoanTerm fromChangeLoanTerm(ChangeLoanTerm entity);
	
	
	/**
	 * ChangeRepaymentFrequency
	 */
	ChangeRepaymentFrequency toChangeRepaymentFrequency(Application.AccountVariation.ChangeRepaymentFrequency mcrf);
	
	@InheritInverseConfiguration
	Application.AccountVariation.ChangeRepaymentFrequency fromChangeRepaymentFrequency(ChangeRepaymentFrequency entity);

	
	/**
	 * ChangeRepaymentAmount
	 */
	ChangeRepaymentAmount toChangeRepaymentAmount(Application.AccountVariation.ChangeRepaymentAmount mcra);
	
	@InheritInverseConfiguration
	Application.AccountVariation.ChangeRepaymentAmount fromChangeRepaymentAmount(ChangeRepaymentAmount entity);

	
	/**
	 * ChangeRepaymentType
	 */
	ChangeRepaymentType toChangeRepaymentType(Application.AccountVariation.ChangeRepaymentType repaymentType);
	
	@InheritInverseConfiguration
	Application.AccountVariation.ChangeRepaymentType fromChangeRepaymentType(ChangeRepaymentType entity);
	
	
	/**
	 * CloseAccount
	 */
	CloseAccount toCloseAccount(Application.AccountVariation.CloseAccount mca);
	
	@InheritInverseConfiguration
	Application.AccountVariation.CloseAccount fromCloseAccount(CloseAccount entity);

	
	/**
	 * InterestRateDiscount
	 */
	InterestRateDiscount toInterestRateDiscount(Application.AccountVariation.InterestRateDiscount mird);
	
	@InheritInverseConfiguration
	Application.AccountVariation.InterestRateDiscount fromInterestRateDiscount(InterestRateDiscount entity);

	
	/**
	 * LendingGuaranteeLimitDecrease
	 */
	AccountVariation.LendingGuaranteeLimitDecrease toLendingGuaranteeLimitDecrease(Application.AccountVariation.LendingGuaranteeLimitDecrease mlgld);
	
	@InheritInverseConfiguration
	Application.AccountVariation.LendingGuaranteeLimitDecrease fromLendingGuaranteeLimitDecrease(AccountVariation.LendingGuaranteeLimitDecrease entity);

	
	/**
	 * LendingGuaranteeLimitIncrease
	 */
	LendingGuaranteeLimitIncrease toEntityLendingGuaranteeLimitIncrease(Application.AccountVariation.LendingGuaranteeLimitIncrease mlgli);
	
	@InheritInverseConfiguration
	Application.AccountVariation.LendingGuaranteeLimitIncrease fromLendingGuaranteeLimitIncrease(LendingGuaranteeLimitIncrease entity);
	
	
	/**
	 * LimitIncrease
	 */
	@Mapping(source = "mli.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class)
	LimitIncrease toLimitIncrease(Application.AccountVariation.LimitIncrease mli);
	
	
	@Mapping(source = "entity.actionDate", target = "actionDate", qualifiedBy = LocalToGregorian.class)
	Application.AccountVariation.LimitIncrease fromLimitIncrease(LimitIncrease entity);

	
	/**
	 * ReduceBalance
	 */
	@Mapping(source = "mrb.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class)
	ReduceBalance toReduceBalance(Application.AccountVariation.ReduceBalance mrb);
	
	// JPA -to- JAXB
	
	@Mapping(source = "entity.actionDate", target = "actionDate", qualifiedBy = LocalToGregorian.class)
	Application.AccountVariation.ReduceBalance fromReduceBalance(ReduceBalance entity);

	
	/**
	 * ReduceLimit
	 */
	ReduceLimit toReduceLimit(Application.AccountVariation.ReduceLimit mrl);
	
	// JPA -to- JAXB
	@InheritInverseConfiguration
	Application.AccountVariation.ReduceLimit fromReduceLimit(ReduceLimit entity);

	
	/**
	 * ReleaseGuarantee
	 */
	@Mapping(target = "XLendingGuarantee", source = "mrg.XLendingGuarantee", qualifiedBy = ObjectToString.class)
	ReleaseGuarantee toReleaseGuarantee(Application.AccountVariation.ReleaseGuarantee mrg);
	
	// JPA -to- JAXB
	
	@Mapping(target = "XLendingGuarantee", source = "entity.XLendingGuarantee", qualifiedBy = StringToObject.class)
	Application.AccountVariation.ReleaseGuarantee fromReleaseGuarantee(ReleaseGuarantee entity);

	
	/**
	 * ReleaseGuaranteeSecurity
	 */
	@Mapping(target = "XSecurity", source = "release.XSecurity", qualifiedBy = ObjectToString.class)
	ReleaseGuaranteeSecurity toReleaseGuaranteeSecurity(Application.AccountVariation.ReleaseGuaranteeSecurity release);
	
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.AccountVariation.ReleaseGuaranteeSecurity fromReleaseGuaranteeSecurity(ReleaseGuaranteeSecurity entity);


	/**
	 * ReleaseGuarantor
	 */
	@Mapping(target = "XGuarantor", source = "mrg.XGuarantor", qualifiedBy = ObjectToString.class)
	ReleaseGuarantor toReleaseGuarantor(Application.AccountVariation.ReleaseGuarantor mrg);
	
	
	@Mapping(target = "XGuarantor", source = "entity.XGuarantor", qualifiedBy = StringToObject.class)
	Application.AccountVariation.ReleaseGuarantor fromReleaseGuarantor(ReleaseGuarantor entity);
	
	
	/**
	 * ReleaseSecurity
	 */
	@Mappings({
		@Mapping(source = "mrs.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(source = "mrs.XSecurity", target = "XSecurity", qualifiedBy = ObjectToString.class)
	})
	ReleaseSecurity toReleaseSecurity(Application.AccountVariation.ReleaseSecurity mrs);
	
	
	@Mappings({
		@Mapping(target = "actionDate", source = "entity.actionDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	})
	Application.AccountVariation.ReleaseSecurity fromReleaseSecurity(ReleaseSecurity entity);
	
	
	/**
	 * RemoveBorrower
	 */
	@Mapping(target = "XBorrower", source = "mrb.XBorrower", qualifiedBy = ObjectToString.class)
	RemoveBorrower toRemoveBorrower(Application.AccountVariation.RemoveBorrower mrb);
	
	// JPA - to - JAXB
	
	@Mapping(target = "XBorrower", source = "entity.XBorrower", qualifiedBy = StringToObject.class)
	Application.AccountVariation.RemoveBorrower fromRemoveBorrower(RemoveBorrower entity);

	
	/**
	 * SplitAccount
	 */
	@Mapping(source = "msa.actionDate", target = "actionDate", qualifiedBy = GregorianToLocal.class)
	SplitAccount toSplitAccount(Application.AccountVariation.SplitAccount msa);
	
	
	
	@Mapping(source = "entity.actionDate", target = "actionDate", qualifiedBy = LocalToGregorian.class)
	Application.AccountVariation.SplitAccount fromSplitAccount(SplitAccount entity);
	
	
	/**
	 * SplitAccount::Account 
	 */
	@Mapping(target = "XAccountDetails", source = "account.XAccountDetails", qualifiedBy = ObjectToString.class)
	Account toAccount(Application.AccountVariation.SplitAccount.Account account);
	
	
	@Mapping(target = "XAccountDetails", source = "entity.XAccountDetails", qualifiedBy = StringToObject.class)
	Application.AccountVariation.SplitAccount.Account fromAccount(Account entity);
	
	
	
	/**
	 * SplitAccount::Accounts - List
	 */
	List<Account> toPartners(List<Application.AccountVariation.SplitAccount.Account> accounts);
	
	@InheritInverseConfiguration
	List<Application.AccountVariation.SplitAccount.Account> fromPartners(List<Account> entityList);

	
	/**
	 * SwitchProduct
	 */
	@Mapping(target = "XProductDetails", source = "switchProduct.XProductDetails", qualifiedBy = ObjectToString.class)
	SwitchProduct toSwitchProduct(Application.AccountVariation.SwitchProduct switchProduct);
	
	
	@Mapping(target = "XProductDetails", source = "entity.XProductDetails", qualifiedBy = StringToObject.class)
	Application.AccountVariation.SwitchProduct fromSwitchProduct(SwitchProduct entity);

	
	/**
	 * UpdatePartyDetails
	 */
	@Mapping(target = "XLoanParty", source = "mupd.XLoanParty", qualifiedBy = ObjectToString.class)
	UpdatePartyDetails toUpdatePartyDetails(Application.AccountVariation.UpdatePartyDetails mupd);
	
	
	@Mapping(target = "XLoanParty", source = "entity.XLoanParty", qualifiedBy = StringToObject.class)
	Application.AccountVariation.UpdatePartyDetails fromUpdatePartyDetails(UpdatePartyDetails entity);

}
