package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Arrears;
import com.moneycatcha.app.entity.DSH;
import com.moneycatcha.app.entity.DiscountMargin;
import com.moneycatcha.app.entity.DistinctLoanPeriod;
import com.moneycatcha.app.entity.FeaturesSelected;
import com.moneycatcha.app.entity.LendingPurpose;
import com.moneycatcha.app.entity.Liability;
import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.entity.Owner;
import com.moneycatcha.app.entity.RateComposition;
import com.moneycatcha.app.entity.Repayment;
import com.moneycatcha.app.entity.Security;
import com.moneycatcha.app.entity.Software;
import com.moneycatcha.app.entity.TermsAndConditions;
import com.moneycatcha.app.entity.RateComposition.BaseRate;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		AmountInForeignCurrencyTypeMapper.class,
		DateMapper.class,
		DocumentationInstructionsTypeMapper.class,
		FinancialAccountTypeMapper.class,
		SecurityAgreementTypeMapper.class,
		StringObjectMapper.class
	})
public interface LiabilityMapper {

	LiabilityMapper INSTANCE = Mappers.getMapper(LiabilityMapper.class);
	
	/**
	 * Liability
	 */
	@Mappings({
		@Mapping(target = "nccpStatus", source = "liability.NCCPStatus"),
		@Mapping(target = "smsfLoan", source = "liability.SMSFLoan"),
		@Mapping(target = "balloonRepaymentDate", source = "liability.balloonRepaymentDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "maturityDate", source = "liability.maturityDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "originationDate", source = "liability.originationDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "repaidDate", source = "liability.repaidDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XCustomerTransactionAnalysis", source = "liability.XCustomerTransactionAnalysis", qualifiedBy = ObjectToString.class)
	})
	Liability toLiability(Application.Liability liability);
	
	@Mappings({
		@Mapping(target = "NCCPStatus", source = "entity.nccpStatus"),
		@Mapping(target = "SMSFLoan", source = "entity.smsfLoan"),
		@Mapping(target = "balloonRepaymentDate", source = "entity.balloonRepaymentDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "maturityDate", source = "entity.maturityDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "originationDate", source = "entity.originationDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "repaidDate", source = "entity.repaidDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XCustomerTransactionAnalysis", source = "entity.XCustomerTransactionAnalysis", qualifiedBy = StringToObject.class)
	})
	Application.Liability fromLiability(Liability entity); 


	
	/**
	 * List<Liability>
	 */
	List<Liability> toLiabilities(List<Application.Liability> liabilities);
	
	@InheritInverseConfiguration
	List<Application.Liability> fromLiabilities(List<Liability> entities);
	
	

	/**
	 * Liability.Arrears
	 */
	Arrears toArrears(Application.Liability.Arrears arrears);
	
	@InheritInverseConfiguration
	Application.Liability.Arrears fromArrears(Arrears entity);

	
	
	/**
	 * Liability::ContinuingRepayment
	 */
	Liability.ContinuingRepayment toContinuingRepayment(Application.Liability.ContinuingRepayment continueRepayment);
	
	@InheritInverseConfiguration
	Application.Liability.ContinuingRepayment fromContinuingRepayment(Liability.ContinuingRepayment entity);

	
	/**
	 * List<Liability::ContinuingRepayment>
	 */
	List<Liability.ContinuingRepayment> toContinuingRepayments(List<Application.Liability.ContinuingRepayment> continueRepayments);
	
	@InheritInverseConfiguration
	List<Application.Liability.ContinuingRepayment> fromContinuingRepayments(List<Liability.ContinuingRepayment> entities);

	
	/**
	 * Liability.DiscountMargin
	 */
	DiscountMargin toDiscountMargin(Application.Liability.DiscountMargin arrears);
	
	@InheritInverseConfiguration
	Application.Liability.DiscountMargin fromDiscountMargin(DiscountMargin entity);	
	
	
	/**
	 * List<Liability.DiscountMargin>
	 */
	List<DiscountMargin> toDiscountMargins(List<Application.Liability.DiscountMargin> discountMargins);
	
	@InheritInverseConfiguration
	List<Application.Liability.DiscountMargin> fromDiscountMargins(List<DiscountMargin> entities);
		

	/**
	 * Liability.DSH
	 */
	DSH toDSH(Application.Liability.DSH dsh);
	
	@InheritInverseConfiguration
	Application.Liability.DSH fromDSH(DSH entity);	
		
	

	/**
	 * Liability.FeaturesSelected
	 */
	FeaturesSelected toFeaturesSelected(Application.Liability.FeaturesSelected featuresSelected);
	
	@InheritInverseConfiguration
	Application.Liability.FeaturesSelected fromFeaturesSelected(FeaturesSelected entity);	
	
	
	/**
	 * Liability.FeaturesSelected.ExtraFeature
	 */
	FeaturesSelected.ExtraFeature toExtraFeature(Application.Liability.FeaturesSelected.ExtraFeature extraFeature);
	
	@InheritInverseConfiguration
	Application.Liability.FeaturesSelected.ExtraFeature fromExtraFeature(FeaturesSelected.ExtraFeature entity);	
	
	
	/**
	 * List<Liability.FeaturesSelected.ExtraFeature>
	 */
	List<FeaturesSelected.ExtraFeature> toExtraFeatures(List<Application.Liability.FeaturesSelected.ExtraFeature> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.Liability.FeaturesSelected.ExtraFeature> fromExtraFeatures(List<FeaturesSelected.ExtraFeature> entities);
		
	

	/**
	 * Liability.FeaturesSelected.OffsetAccount
	 */
	@Mapping(target = "XAccount", source  = "offsetAccount.XAccount", qualifiedBy = ObjectToString.class)
	OffsetAccount toOffsetAccount(Application.Liability.FeaturesSelected.OffsetAccount offsetAccount);
	
	@Mapping(target = "XAccount", source  = "entity.XAccount", qualifiedBy = StringToObject.class)
	Application.Liability.FeaturesSelected.OffsetAccount fromOffsetAccount(OffsetAccount entity);	
	
	
	/**
	 * List<Liability.FeaturesSelected.OffsetAccount>
	 */
	List<OffsetAccount> toOffsetAccounts(List<Application.Liability.FeaturesSelected.OffsetAccount> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.Liability.FeaturesSelected.OffsetAccount> fromOffsetAccounts(List<OffsetAccount> entities);
		

	/**
	 * Liability.LendingPurpose
	 */
	@Mappings({
		@Mapping(target = "absLendingPurpose", source = "lendingPurpose.ABSLendingPurpose"),
		@Mapping(target = "absLendingPurposeCode", source = "lendingPurpose.ABSLendingPurposeCode")
	})
	LendingPurpose toLendingPurpose(Application.Liability.LendingPurpose lendingPurpose);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "ABSLendingPurpose", source = "entity.absLendingPurpose"),
		@Mapping(target = "ABSLendingPurposeCode", source = "entity.absLendingPurposeCode")
	})
	Application.Liability.LendingPurpose fromLendingPurpose(LendingPurpose entity);	
	
	
	/**
	 * List<Liability.LendingPurpose>
	 */
	List<LendingPurpose> toLendingPurposes(List<Application.Liability.LendingPurpose> lendingPurposes);
	
	@InheritInverseConfiguration
	List<Application.Liability.LendingPurpose> fromLendingPurposes(List<LendingPurpose> entities);
	
	
	
	/**
	 * Liability.OriginalTerm
	 */
	@Mapping(target = "interestOnlyEndDate", source = "originalTerm.interestOnlyEndDate", qualifiedBy = GregorianToLocal.class)
	Liability.OriginalTerm toOriginalTerm(Application.Liability.OriginalTerm originalTerm);
	
	@InheritInverseConfiguration
	@Mapping(target = "interestOnlyEndDate", source = "entity.interestOnlyEndDate", qualifiedBy = LocalToGregorian.class)
	Application.Liability.OriginalTerm fromOriginalTerm(Liability.OriginalTerm entity);


	/**
	 * Liability.OriginalTerm::DistinctLoanPeriod
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "dlp.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "dlp.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XRateComposition", source = "dlp.XRateComposition", qualifiedBy = ObjectToString.class)
	})
	DistinctLoanPeriod toDistinctLoanPeriod(Application.Liability.OriginalTerm.DistinctLoanPeriod dlp);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XRateComposition", source = "entity.XRateComposition", qualifiedBy = StringToObject.class)
	})
	Application.Liability.OriginalTerm.DistinctLoanPeriod fromDistinctLoanPeriod(DistinctLoanPeriod entity);	
	
	
	/**
	 * List<Liability.OriginalTerm::DistinctLoanPeriod>
	 */
	List<DistinctLoanPeriod> toDistinctLoanPeriods(List<Application.Liability.OriginalTerm.DistinctLoanPeriod> distinctLoanPeriods);
	
	@InheritInverseConfiguration
	List<Application.Liability.OriginalTerm.DistinctLoanPeriod> fromDistinctLoanPeriods(List<DistinctLoanPeriod> entities);
		
	
	/**
	 * Liability.OriginalTerm.DistinctLoanPeriod.Repayment
	 */
	@Mappings({
		@Mapping(target = "XContinuingRepayment", source = "repayment.XContinuingRepayment", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XRepayment", source = "repayment.XRepayment", qualifiedBy = ObjectToString.class)
	})
	Repayment toRepayment(Application.Liability.OriginalTerm.DistinctLoanPeriod.Repayment repayment);
	
	@Mappings({
		@Mapping(target = "XContinuingRepayment", source = "entity.XContinuingRepayment", qualifiedBy = StringToObject.class),
		@Mapping(target = "XRepayment", source = "entity.XRepayment", qualifiedBy = StringToObject.class)
	})
	Application.Liability.OriginalTerm.DistinctLoanPeriod.Repayment fromRepayment(Repayment entity);	
	
	
	
	
	/**
	 * List<Liability.OriginalTerm::DistinctLoanPeriod::Repayment>
	 */
	List<Repayment> toRepayments(List<Application.Liability.OriginalTerm.DistinctLoanPeriod.Repayment> repayments);
	
	@InheritInverseConfiguration
	List<Application.Liability.OriginalTerm.DistinctLoanPeriod.Repayment> fromRepayments(List<Repayment> entities);
			
	
	/**
	 * Liability.PercentOwned
	 */
	Liability.PercentOwned toPercentOwned(Application.Liability.PercentOwned percentOwned);
	
	@InheritInverseConfiguration
	Application.Liability.PercentOwned fromPercentOwned(Liability.PercentOwned percentOwned);


	/**
	 * Liability.PercentOwned.Owner
	 */
	@Mapping(target = "XParty", source = "owner.XParty",  qualifiedBy = ObjectToString.class)
	Owner toOwner(Application.Liability.PercentOwned.Owner owner);
	
	@Mapping(target = "XParty", source = "entity.XParty",  qualifiedBy = StringToObject.class)
	Application.Liability.PercentOwned.Owner fromOwner(Owner entity);
	
	
	
	/**
	 * List<Liability.PercentOwned.Owner>
	 */
	List<Owner> toOwners(List<Application.Liability.PercentOwned.Owner> owners);
	
	@InheritInverseConfiguration
	List<Application.Liability.PercentOwned.Owner> fromOwners(List<Owner> entities);
			
	
	
	
	
	
	/**
	 * Liability::RateComposition
	 */
	RateComposition toRateComposition(Application.Liability.RateComposition rateComposition);
	
	@InheritInverseConfiguration
	Application.Liability.RateComposition fromRateComposition(RateComposition entity);	
	
		
	
	/**
	 * List<Liability::RateComposition>
	 */
	List<RateComposition> toRateCompositions(List<Application.Liability.RateComposition> rateCompositions);
	
	@InheritInverseConfiguration
	List<Application.Liability.RateComposition> fromRateCompositions(List<RateComposition> entities);
	
	
	
	/**
	 * Liability::RateComposition::BaseRate
	 */
	BaseRate toBaseRate(Application.Liability.RateComposition.BaseRate baseRate);
	
	@InheritInverseConfiguration
	Application.Liability.RateComposition.BaseRate fromBaseRate(BaseRate entity);	
		
	

	
	/**
	 * Liability::RemainingTerm
	 */
	Liability.RemainingTerm toRemainingTerm(Application.Liability.RemainingTerm remainingTerm);
	
	@InheritInverseConfiguration
	Application.Liability.RemainingTerm fromRemainingTerm(Liability.RemainingTerm entity);

	
	
	/**
	 * Liability.Repayment
	 */
	Repayment toRepaymnt(Application.Liability.Repayment repayment);
	
	@InheritInverseConfiguration
	Application.Liability.Repayment fromRepaymnt(Repayment entity);	
	
	
	
	
	/**
	 * List<Liability.Repayment>
	 */
	List<Repayment> toRepaymnts(List<Application.Liability.Repayment> repaymnts);
	
	@InheritInverseConfiguration
	List<Application.Liability.Repayment> fromRepaymnts(List<Repayment> entities);	
	
	
	
	
	/**
	 * Liability::Security
	 */
	@Mapping(target = "XSecurity", source = "security.XSecurity", qualifiedBy = ObjectToString.class)
	Security toSecurity(Application.Liability.Security security);
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.Liability.Security fromSecurity(Security entity);	
	
	
	
	/**
	 * List<Liability::Security>
	 */
	List<Security> toSecurities(List<Application.Liability.Security> securities);
	
	@InheritInverseConfiguration
	List<Application.Liability.Security> fromSecurities(List<Security> entities);	
		
	
	
	/**
	 * Liability::Security::MortgagorDetails PersonApplicant
	 */
	@Mapping(target = "XMortgagor", source = "mortgagorDetails.XMortgagor",  qualifiedBy = ObjectToString.class)
	Security.MortgagorDetails toMortgagorDetails(Application.Liability.Security.MortgagorDetails mortgagorDetails);
	
	@Mapping(target = "XMortgagor", source = "entity.XMortgagor",  qualifiedBy = StringToObject.class)
	Application.Liability.Security.MortgagorDetails fromMortgagorDetails(Security.MortgagorDetails entity);	
	
	
	
	/**
	 * List<Liability::Security::MortgagorDetails>
	 */
	List<Security.MortgagorDetails> toMortgagorDetailsList(List<Application.Liability.Security.MortgagorDetails> mortgagorDetailsList);
	
	@InheritInverseConfiguration
	List<Application.Liability.Security.MortgagorDetails> fromMortgagorDetailsList(List<Security.MortgagorDetails> entities);	
		

	
	/**
	 * Liability::Software
	 */
	Software toSoftware(Application.Liability.Software software);
	
	@InheritInverseConfiguration
	Application.Liability.Software fromSoftware(Software entity);	
			
	
	/**
	 * Liability::TermsAndConditions
	 */
	TermsAndConditions toTermsAndConditions(Application.Liability.TermsAndConditions termsAndConditions);
	
	@InheritInverseConfiguration
	Application.Liability.TermsAndConditions fromTermsAndConditions(TermsAndConditions entity);	
	
	
	
	/**
	 * List<Liability::TermsAndConditions>
	 */
	List<TermsAndConditions> toTermsAndConditionsList(List<Application.Liability.TermsAndConditions> termsAndConditionsList);
	
	@InheritInverseConfiguration
	List<Application.Liability.TermsAndConditions> fromTermsAndConditionsList(List<TermsAndConditions> entities);	
		
	
	
}
