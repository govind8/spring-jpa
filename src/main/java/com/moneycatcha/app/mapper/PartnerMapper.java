package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Partner;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface PartnerMapper {

	/**
	 * Partner 
	 */
	@Mapping(target = "XPartner", source = "partner.XPartner", qualifiedBy = ObjectToString.class)
	Partner toPartner(Application.CompanyApplicant.Partner partner);
	
	@Mapping(target = "XPartner", source = "entity.XPartner", qualifiedBy = StringToObject.class)
	Application.CompanyApplicant.Partner fromPartner(Partner entity);
	
	/**
	 * Partners - List
	 */
	List<Partner> toPartners(List<Application.CompanyApplicant.Partner> partners);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant.Partner> fromPartners(List<Partner> entityList);

}
