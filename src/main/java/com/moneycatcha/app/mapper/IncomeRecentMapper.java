package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.IncomeRecent;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {AddbackMapper.class,
		DateMapper.class,
		StringObjectMapper.class
})
public interface IncomeRecentMapper {

	/**
	 * Company Applicant - IncomeRecent
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomeRecent.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomeRecent.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomeRecent.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomeRecent toCAIncomeRecent(Application.CompanyApplicant.IncomeRecent incomeRecent);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.CompanyApplicant.IncomeRecent fromCAIncomeRecent(IncomeRecent entity);



	/**
	 * IncomeRecent - Trust Applicant
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomeRecent.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomeRecent.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomeRecent.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomeRecent toTAIncomeRecent(Application.TrustApplicant.IncomeRecent incomeRecent);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.TrustApplicant.IncomeRecent fromTAIncomeRecent(IncomeRecent entity);


}
