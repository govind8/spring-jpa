package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.ExistingCustomer;
import com.moneycatcha.app.mapper.annotations.GregorianToYear;
import com.moneycatcha.app.mapper.annotations.YearToGregorian;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {FinancialAccountTypeMapper.class}
)
public interface ExistingCustomerMapper {

	/**
	 * ExistingCustomer
	 */
	@Mapping(target = "customerSince", source = "customer.customerSince", qualifiedBy = GregorianToYear.class)
	ExistingCustomer toExistingCustomer(CompanyApplicant.ExistingCustomer customer);
	
	@InheritInverseConfiguration
	@Mapping(target = "customerSince", source = "entity.customerSince", qualifiedBy = YearToGregorian.class)
	CompanyApplicant.ExistingCustomer fromExistingCustomer(ExistingCustomer entity);
}
