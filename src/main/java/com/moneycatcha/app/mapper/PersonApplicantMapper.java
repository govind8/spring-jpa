package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.CreditHistory;
import com.moneycatcha.app.entity.CurrentAddress;
import com.moneycatcha.app.entity.EmailAddress;
import com.moneycatcha.app.entity.ExistingCustomer;
import com.moneycatcha.app.entity.FinancialSituationCheck;
import com.moneycatcha.app.entity.IdentityCheck;
import com.moneycatcha.app.entity.PersonApplicant;
import com.moneycatcha.app.entity.PostSettlementAddress;
import com.moneycatcha.app.entity.PowerOfAttorney;
import com.moneycatcha.app.entity.PreviousAddress;
import com.moneycatcha.app.entity.PriorAddress;
import com.moneycatcha.app.entity.Privacy;
import com.moneycatcha.app.entity.ProofOfIdentity;
import com.moneycatcha.app.entity.SourceOfWealth;
import com.moneycatcha.app.entity.PersonApplicant.DocumentationInstructions;
import com.moneycatcha.app.entity.PersonApplicant.PreviousName;
import com.moneycatcha.app.entity.PersonApplicant.SourceOfFunds;
import com.moneycatcha.app.entity.PersonApplicant.Will;
import com.moneycatcha.app.entity.ProofOfIdentity.MedicareCard;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.GregorianToYear;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.mapper.annotations.YearToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		DealingNumberTypeMapper.class,
		DocumentationInstructionsTypeMapper.class,
		EmploymentMapper.class,
		FinancialAnalysisMapper.class,
		FinancialAccountTypeMapper.class,
		FundsDisbursementTypeMapper.class,
		ForeignTaxAssociationTypeMapper.class,
		PercentOwnedTypeMapper.class,
		PersonNameTypeMapper.class,
		ResponsibleLendingTypeMapper.class,
		SecurityAgreementTypeMapper.class,
		StringObjectMapper.class,
		TaxDeclarationDetailsTypeMapper.class
	})
public interface PersonApplicantMapper {

	PersonApplicantMapper INSTANCE = Mappers.getMapper(PersonApplicantMapper.class);

	/**
	 * PersonApplicant
	 */
	@Mappings({
		@Mapping(target = "dateOfBirth", source = "personApplicant.dateOfBirth", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "immigrationDate", source = "personApplicant.immigrationDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "permanentResidencyDate", source = "personApplicant.permanentResidencyDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "poaGranted", source = "personApplicant.POAGranted"),
		@Mapping(target = "temporaryVisaExpiryDate", source = "personApplicant.temporaryVisaExpiryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "personApplicant.XAccountant", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XHousehold", source = "personApplicant.XHousehold", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPersonalReference", source = "personApplicant.XPersonalReference", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XSolicitor", source = "personApplicant.XSolicitor", qualifiedBy = ObjectToString.class)
	})
	PersonApplicant toPersonApplicant(Application.PersonApplicant personApplicant);
	
	@Mappings({
		@Mapping(target = "dateOfBirth", source = "entity.dateOfBirth", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "immigrationDate", source = "entity.immigrationDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "permanentResidencyDate", source = "entity.permanentResidencyDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "POAGranted", source = "entity.poaGranted"),
		@Mapping(target = "temporaryVisaExpiryDate", source = "entity.temporaryVisaExpiryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class),
		@Mapping(target = "XHousehold", source = "entity.XHousehold", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPersonalReference", source = "entity.XPersonalReference", qualifiedBy = StringToObject.class),
		@Mapping(target = "XSolicitor", source = "entity.XSolicitor", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant fromPersonApplicant(PersonApplicant entity); 

	
	/**
	 * List<PersonApplicant>
	 */
	List<PersonApplicant> toPersonApplicants(List<Application.PersonApplicant> personApplicants);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant> fromPersonApplicants(List<PersonApplicant> entities); 
	
	
	/**
	 * PersonApplicant - Contact
	 */
	Contact toContact(Application.PersonApplicant.Contact personApplicant);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.Contact fromContact(Contact entity);

	/**
	 * Contact-CurrentAddress
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "currentaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XLandlord", source = "currentaddress.XLandlord", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XMailingAddress", source = "currentaddress.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XResidentialAddress", source = "currentaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	CurrentAddress toCurrentAddress(Application.PersonApplicant.Contact.CurrentAddress currentaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XLandlord", source = "entity.XLandlord", qualifiedBy = StringToObject.class),
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.Contact.CurrentAddress fromCurrentAddress(CurrentAddress entity);
	
	
	/**
	 * Contact-EmailAddress
	 */
	EmailAddress toEmailAddress(Application.PersonApplicant.Contact.EmailAddress emailaddress);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.Contact.EmailAddress fromEmailAddress(EmailAddress entity);
	
	/**
	 * List<EmailAddress>
	 */
	List<EmailAddress> toEmailAddresses(List<Application.PersonApplicant.Contact.EmailAddress> emailaddresses);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.Contact.EmailAddress> fromEmailAddresses(List<EmailAddress> entities);
	
	
	/**
	 * Contact-PostSettlementAddress
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "settlementaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XMailingAddress", source = "settlementaddress.XMailingAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XResidentialAddress", source = "settlementaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PostSettlementAddress toPostSettlementAddress(Application.PersonApplicant.Contact.PostSettlementAddress settlementaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.Contact.PostSettlementAddress fromPostSettlementAddress(PostSettlementAddress entity);

	
	/**
	 * Contact-PreviousAddress
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "previousaddress.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "previousaddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XResidentialAddress", source = "previousaddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PreviousAddress toPreviousAddress(Application.PersonApplicant.Contact.PreviousAddress previousaddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.Contact.PreviousAddress fromPreviousAddress(PreviousAddress entity);
	
	
	/**
	 * Contact-PriorAddress
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "prioraddress.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "prioraddress.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XResidentialAddress", source = "prioraddress.XResidentialAddress", qualifiedBy = ObjectToString.class)
	})
	PriorAddress toPriorAddress(Application.PersonApplicant.Contact.PriorAddress prioraddress);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XResidentialAddress", source = "entity.XResidentialAddress", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.Contact.PriorAddress fromPriorAddress(PriorAddress entity);
	
	
	/**
	 * CreditHistory
	 */
	CreditHistory toCreditHistory(Application.PersonApplicant.CreditHistory creditHistory);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.CreditHistory fromCreditHistory(CreditHistory entity); 

	
	
	/**
	 * DocumentationInstructions
	 */
	@Mapping(target = "XNominatedAuthority", source = "document.XNominatedAuthority", qualifiedBy = ObjectToString.class)
	DocumentationInstructions toDocumentationInstructions(Application.PersonApplicant.DocumentationInstructions document);
	
	@Mapping(target = "XNominatedAuthority", source = "entity.XNominatedAuthority", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.DocumentationInstructions fromDocumentationInstructions(DocumentationInstructions entity); 

	
	/**
	 * ExistingCustomer
	 */
	@Mapping(target = "customerSince", source = "existingCustomer.customerSince", qualifiedBy = GregorianToYear.class)
	ExistingCustomer toExistingCustomer(Application.PersonApplicant.ExistingCustomer existingCustomer);
	
	@InheritInverseConfiguration
	@Mapping(target = "customerSince", source = "entity.customerSince", qualifiedBy = YearToGregorian.class)
	Application.PersonApplicant.ExistingCustomer fromExistingCustomer(ExistingCustomer entity); 

	
	/**
	 * FinancialSituationCheck
	 */
	FinancialSituationCheck toFinancialSituationCheck(Application.PersonApplicant.FinancialSituationCheck financialSituationCheck);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.FinancialSituationCheck fromFinancialSituationCheck(FinancialSituationCheck entity); 

	/**
	 * List<FinancialSituationCheck>
	 */
	List<FinancialSituationCheck> toFinancialSituationChecks(List<Application.PersonApplicant.FinancialSituationCheck> financialSituationChecks);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.FinancialSituationCheck> fromFinancialSituationChecks(List<FinancialSituationCheck> entities); 
	
	
	
	/**
	 * IdentityCheck
	 */
	@Mapping(target = "date", source = "identityCheck.date", qualifiedBy = GregorianToLocal.class)
	IdentityCheck toIdentityCheck(Application.PersonApplicant.IdentityCheck identityCheck);
	
	@InheritInverseConfiguration
	@Mapping(target = "date", source = "entity.date", qualifiedBy = LocalToGregorian.class)
	Application.PersonApplicant.IdentityCheck fromIdentityCheck(IdentityCheck entity); 

	
	/**
	 * List<IdentityCheck>
	 */
	List<IdentityCheck> toIdentityChecks(List<Application.PersonApplicant.IdentityCheck> identityChecks);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.IdentityCheck> fromIdentityChecks(List<IdentityCheck> entities); 
	
	
	
	/**
	 * Insurance
	 */
	@Mapping(target = "XInsurance", source = "insurance.XInsurance", qualifiedBy = ObjectToString.class)
	PersonApplicant.Insurance toIdentityCheck(Application.PersonApplicant.Insurance insurance);
	
	@Mapping(target = "XInsurance", source = "entity.XInsurance", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.Insurance fromInsurance(PersonApplicant.Insurance entity); 

	
	/**
	 * List<PersonApplicant.Insurance>
	 */
	List<PersonApplicant.Insurance> toInsuranceList(List<Application.PersonApplicant.Insurance> identityChecks);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.Insurance> fromInsuranceList(List<PersonApplicant.Insurance> entities); 
	
	
	
	/**
	 * MaritalStatusDetails
	 */
	@Mapping(target = "XSpouse", source = "maritalStatusDetails.XSpouse", qualifiedBy = ObjectToString.class)
	PersonApplicant.MaritalStatusDetails toMaritalStatusDetails(Application.PersonApplicant.MaritalStatusDetails maritalStatusDetails);
	
	@Mapping(target = "XSpouse", source = "entity.XSpouse", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.MaritalStatusDetails fromMaritalStatusDetails(PersonApplicant.MaritalStatusDetails entity); 

	
	/**
	 * NextOfKin
	 */
	@Mapping(target = "XPerson", source = "nextOfKin.XPerson", qualifiedBy = ObjectToString.class)
	PersonApplicant.NextOfKin toNextOfKin(Application.PersonApplicant.NextOfKin nextOfKin);
	
	@Mapping(target = "XPerson", source = "entity.XPerson", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.NextOfKin fromNextOfKin(PersonApplicant.NextOfKin entity); 

	
	/**
	 * NominatedBorrower
	 */
	@Mapping(target = "XNominee", source = "borrower.XNominee", qualifiedBy = ObjectToString.class)
	PersonApplicant.NominatedBorrower toNominatedBorrower(Application.PersonApplicant.NominatedBorrower borrower);
	
	@Mapping(target = "XNominee", source = "entity.XNominee", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.NominatedBorrower fromNominatedBorrower(PersonApplicant.NominatedBorrower entity); 

	
	
	/**
	 * PowerOfAttorney
	 */
	@Mapping(target = "xpoaHolder", source = "poa.XPOAHolder", qualifiedBy = ObjectToString.class)
	PowerOfAttorney toPowerOfAttorney(Application.PersonApplicant.PowerOfAttorney poa);
	
	@Mapping(target = "XPOAHolder", source = "entity.xpoaHolder", qualifiedBy = StringToObject.class)
	Application.PersonApplicant.PowerOfAttorney fromPowerOfAttorney(PowerOfAttorney entity); 

	
	/**
	 * PreviousName
	 */
	PreviousName toPreviousName(Application.PersonApplicant.PreviousName person);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.PreviousName fromPreviousName(PreviousName entity); 

	 
	/**
	 * Privacy
	 */
	@Mapping(target = "dateSigned", source = "privacy.dateSigned", qualifiedBy = GregorianToLocal.class)
	Privacy toPrivacy(Application.PersonApplicant.Privacy privacy);
	
	@InheritInverseConfiguration
	@Mapping(target = "dateSigned", source = "entity.dateSigned", qualifiedBy = LocalToGregorian.class)
	Application.PersonApplicant.Privacy fromPrivacy(Privacy entity); 

	
	/**
	 * ProofOfIdentity
	 */
	@Mappings({
		@Mapping(target = "dobVerified", source = "poi.DOBVerified"),
		@Mapping(target = "dateDocumentVerified", source = "poi.dateDocumentVerified", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "dateOfIssue", source = "poi.dateOfIssue", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "expiryDate", source = "poi.expiryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XLocationDocumentVerified", source = "poi.XLocationDocumentVerified", qualifiedBy = ObjectToString.class)
	})
	ProofOfIdentity toProofOfIdentity(Application.PersonApplicant.ProofOfIdentity poi);
	
	@Mappings({
		@Mapping(target = "DOBVerified", source = "entity.dobVerified"),
		@Mapping(target = "dateDocumentVerified", source = "entity.dateDocumentVerified", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "dateOfIssue", source = "entity.dateOfIssue", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "expiryDate", source = "entity.expiryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XLocationDocumentVerified", source = "entity.XLocationDocumentVerified", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.ProofOfIdentity fromProofOfIdentity(ProofOfIdentity entity); 

	
	/**
	 * List<ProofOfIdentity>
	 */
	List<ProofOfIdentity> toProofOfIdentities(List<Application.PersonApplicant.ProofOfIdentity> identities);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.ProofOfIdentity> fromProofOfIdentities(List<ProofOfIdentity> entities); 
	

	/**
	 * MedicareCard
	 */
	MedicareCard toMedicareCard(Application.PersonApplicant.ProofOfIdentity.MedicareCard medicareCard);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.ProofOfIdentity.MedicareCard fromMedicareCard(MedicareCard entity); 

	
	/**
	 * SourceOfFunds
	 */
	SourceOfFunds toSourceOfFunds(Application.PersonApplicant.SourceOfFunds sof);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.SourceOfFunds fromSourceOfFunds(SourceOfFunds entity); 

	
	/**
	 * List<SourceOfFunds>
	 */
	List<SourceOfFunds> toSourceOfFundsList(List<Application.PersonApplicant.SourceOfFunds> sofList);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.SourceOfFunds> fromSourceOfFundsList(List<SourceOfFunds> entities); 
	
	
	
	/**
	 * SourceOfWealth
	 */
	SourceOfWealth toSourceOfWealth(Application.PersonApplicant.SourceOfWealth sow);
	
	@InheritInverseConfiguration
	Application.PersonApplicant.SourceOfWealth fromSourceOfWealth(SourceOfWealth entity); 

	
	/**
	 * List<SourceOfWealth>
	 */
	List<SourceOfWealth> toSourceOfWealthList(List<Application.PersonApplicant.SourceOfWealth> sowList);
	
	@InheritInverseConfiguration
	List<Application.PersonApplicant.SourceOfWealth> fromSourceOfWealthList(List<SourceOfWealth> entities); 
	
	
	/**
	 * Will
	 */
	
	@Mappings({
		@Mapping(target = "XExecutor", source = "will.XExecutor", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XWillHeldBy", source = "will.XWillHeldBy", qualifiedBy = ObjectToString.class)
	})
	Will toWill(Application.PersonApplicant.Will will);
	
	@Mappings({
		@Mapping(target = "XExecutor", source = "entity.XExecutor", qualifiedBy = StringToObject.class),
		@Mapping(target = "XWillHeldBy", source = "entity.XWillHeldBy", qualifiedBy = StringToObject.class)
	})
	Application.PersonApplicant.Will fromWill(Will entity); 


	
}
