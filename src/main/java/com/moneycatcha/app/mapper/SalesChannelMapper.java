package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Aggregator;
import com.moneycatcha.app.entity.Commission;
import com.moneycatcha.app.entity.Company;
import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.LoanWriter;
import com.moneycatcha.app.entity.SalesChannel;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
	uses = {
			PhoneTypeMapper.class,
			StringObjectMapper.class
	})
public interface SalesChannelMapper {

	SalesChannelMapper INSTANCE = Mappers.getMapper(SalesChannelMapper.class);

	/**
	 * RelatedCompany
	 */
	SalesChannel toSalesChannel(Application.SalesChannel salesChannel);
	
	@InheritInverseConfiguration
	Application.SalesChannel fromSalesChannel(SalesChannel entity); 

	/**
	 * Aggregator
	 */
	@Mapping(target = "abn", source = "aggregator.ABN")
	Aggregator toAggregator(Application.SalesChannel.Aggregator aggregator);
	
	@InheritInverseConfiguration
	@Mapping(target = "ABN", source = "entity.abn")
	Application.SalesChannel.Aggregator fromAggregator(Aggregator entity); 

	
	/**
	 * Aggregator - Contact
	 */
	@Mapping(target = "XAddress", source ="aggregatorContact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toAggregatorContact(Application.SalesChannel.Aggregator.Contact aggregatorContact);
	
	@Mapping(target = "XAddress", source ="entity.XAddress", qualifiedBy = StringToObject.class)
	Application.SalesChannel.Aggregator.Contact fromAggregatorContact(Contact entity);

	
	/**
	 * Commission
	 */
	Commission toCommission(Application.SalesChannel.Commission aggregator);
	
	@InheritInverseConfiguration
	Application.SalesChannel.Commission fromCommission(Commission entity); 

	
	/**
	 * Company
	 */
	@Mappings({
		@Mapping(target = "abn", source = "company.ABN"),
		@Mapping(target = "bsb", source = "company.BSB")
	})
	Company toCompany(Application.SalesChannel.Company company);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "ABN", source = "entity.abn"),
		@Mapping(target = "BSB", source = "entity.bsb")
	})
	Application.SalesChannel.Company fromCompany(Company entity); 

	
	/**
	 * Company - Contact
	 */
	@Mapping(target = "XAddress",  source = "companyContact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toCompanyContact(Application.SalesChannel.Company.Contact companyContact);
	
	@Mapping(target = "XAddress",  source = "entity.XAddress", qualifiedBy = StringToObject.class)
	Application.SalesChannel.Company.Contact fromCompanyContact(Contact entity);

	
	/**
	 * Introducer
	 */
	SalesChannel.Introducer toIntroducer(Application.SalesChannel.Introducer introducer);
	
	@InheritInverseConfiguration
	Application.SalesChannel.Introducer fromIntroducer(SalesChannel.Introducer entity); 

	
	
	/**
	 * LoanWriter
	 */
	LoanWriter toLoanWriter(Application.SalesChannel.LoanWriter company);
	
	@InheritInverseConfiguration
	Application.SalesChannel.LoanWriter fromLoanWriter(LoanWriter entity); 

	
	
	/**
	 * LoanWriter - Contact
	 */
	@Mapping(target = "XAddress", source = "loanwriterContact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toLoanWriterContact(Application.SalesChannel.LoanWriter.Contact loanwriterContact);
	
	@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class)
	Application.SalesChannel.LoanWriter.Contact fromLoanWriterContact(Contact entity);


}
