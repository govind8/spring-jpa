package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Commission;
import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.DSH;
import com.moneycatcha.app.entity.DiscountMargin;
import com.moneycatcha.app.entity.DistinctLoanPeriod;
import com.moneycatcha.app.entity.FeaturesSelected;
import com.moneycatcha.app.entity.LendingPurpose;
import com.moneycatcha.app.entity.LoanDetails;
import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.entity.Owner;
import com.moneycatcha.app.entity.ProposedRepayment;
import com.moneycatcha.app.entity.RateComposition;
import com.moneycatcha.app.entity.Repayment;
import com.moneycatcha.app.entity.Security;
import com.moneycatcha.app.entity.Software;
import com.moneycatcha.app.entity.SupplementaryCardholder;
import com.moneycatcha.app.entity.TermsAndConditions;
import com.moneycatcha.app.entity.LoanDetails.Borrowers;
import com.moneycatcha.app.entity.LoanDetails.EquityRelease;
import com.moneycatcha.app.entity.LoanDetails.StatementInstructions;
import com.moneycatcha.app.entity.LoanDetails.Term;
import com.moneycatcha.app.entity.ProposedRepayment.StructuredPayments.Payment;
import com.moneycatcha.app.entity.RateComposition.BaseRate;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		AmountInForeignCurrencyTypeMapper.class,
		DateMapper.class,
		DocumentationInstructionsTypeMapper.class,
		DurationTypeMapper.class,
		FinancialAccountTypeMapper.class,
		FundsDisbursementTypeMapper.class,
		SecurityAgreementTypeMapper.class,
		StringObjectMapper.class
	})
public interface LoanDetailsMapper {

	LoanDetailsMapper INSTANCE = Mappers.getMapper(LoanDetailsMapper.class);

	/**
	 * Liability
	 */
	@Mappings({
		@Mapping(target = "smsfLoan", source = "loanDetails.SMSFLoan"),
		@Mapping(target = "balloonRepaymentDate", source = "loanDetails.balloonRepaymentDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "maturityDate", source = "loanDetails.maturityDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "estimatedSettlementDate", source = "loanDetails.estimatedSettlementDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XMasterAgreement", source = "loanDetails.XMasterAgreement", qualifiedBy = ObjectToString.class)
	})
	LoanDetails toLoanDetails(Application.LoanDetails loanDetails);
	
	@Mappings({
		@Mapping(target = "SMSFLoan", source = "entity.smsfLoan"),
		@Mapping(target = "balloonRepaymentDate", source = "entity.balloonRepaymentDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "maturityDate", source = "entity.maturityDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "estimatedSettlementDate", source = "entity.estimatedSettlementDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XMasterAgreement", source = "entity.XMasterAgreement", qualifiedBy = StringToObject.class)
	})
	Application.LoanDetails fromLoanDetails(LoanDetails entity); 

	
	/**
	 * List<LoanDetails>
	 */
	List<LoanDetails> toLoanDetailsList(List<Application.LoanDetails> loanDetailsList);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails> fromLoanDetailsList(List<LoanDetails> entities);
	
	

	/**
	 * LoanDetails.Borrowers
	 */
	Borrowers toBorrowers(Application.LoanDetails.Borrowers borrowers);
	
	@InheritInverseConfiguration
	Application.LoanDetails.Borrowers fromBorrowers(Borrowers entity);	
	
	
	/**
	 * LoanDetails.Borrowers.Owner
	 */
	@Mapping(target = "XParty", source = "owner.XParty", qualifiedBy = ObjectToString.class)
	Owner toBorrowers(Application.LoanDetails.Borrowers.Owner owner);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	Application.LoanDetails.Borrowers.Owner fromBorrowers(Owner entity);	
	
	
	/**
	 * List<LoanDetails.Borrowers.Owner>
	 */
	List<Owner> toOwners(List<Application.LoanDetails.Borrowers.Owner> owners);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Borrowers.Owner> fromOwners(List<Owner> entities);
	
	
	/**
	 * LoanDetails.BulkReduction
	 */
	@Mapping(target = "estimatedDate", source = "bulkReduction.estimatedDate", qualifiedBy = GregorianToLocal.class)
	LoanDetails.BulkReduction toBulkReduction(Application.LoanDetails.BulkReduction bulkReduction);
	
	@InheritInverseConfiguration
	@Mapping(target = "estimatedDate", source = "entity.estimatedDate", qualifiedBy = LocalToGregorian.class)
	Application.LoanDetails.BulkReduction fromBulkReduction(LoanDetails.BulkReduction entity);	
	
	
	
	/**
	 * LoanDetails.Commission
	 */
	@Mappings({
		@Mapping(target = "gstAmount", source = "commission.GSTAmount"),
		@Mapping(target = "itcAmount", source = "commission.ITCAmount")
	})
	Commission toBulkReduction(Application.LoanDetails.Commission commission);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "GSTAmount", source = "entity.gstAmount"),
		@Mapping(target = "ITCAmount", source = "entity.itcAmount")
	})
	Application.LoanDetails.Commission fromBulkReduction(Commission entity);	
	
	
	/**
	 * LoanDetails.DiscountMargin
	 */
	DiscountMargin toDiscountMargin(Application.LoanDetails.DiscountMargin arrears);
	
	@InheritInverseConfiguration
	Application.LoanDetails.DiscountMargin fromDiscountMargin(DiscountMargin entity);	
	
	
	
	/**
	 * LoanDetails.DSH
	 */
	DSH toDSH(Application.LoanDetails.DSH dsh);
	
	@InheritInverseConfiguration
	Application.LoanDetails.DSH fromDSH(DSH entity);	
	
	
	
	/**
	 * LoanDetails.EquityRelease
	 */
	EquityRelease toEquityRelease(Application.LoanDetails.EquityRelease equityRelease);
	
	@InheritInverseConfiguration
	Application.LoanDetails.EquityRelease fromEquityRelease(EquityRelease entity);	
	
	
	/**
	 * LoanDetails.EquityRelease.AccommodationBond
	 */
	EquityRelease.AccommodationBond toAccommodationBond(Application.LoanDetails.EquityRelease.AccommodationBond accommodationBond);
	
	@InheritInverseConfiguration
	Application.LoanDetails.EquityRelease.AccommodationBond fromEquityRelease(EquityRelease.AccommodationBond entity);	
	
	
	/**
	 * LoanDetails.EquityRelease.Amount
	 */
	EquityRelease.Amount toAmount(Application.LoanDetails.EquityRelease.Amount equityRelease);
	
	@InheritInverseConfiguration
	Application.LoanDetails.EquityRelease.Amount fromAmount(EquityRelease.Amount entity);	
	
	
	/**
	 * LoanDetails.EquityRelease.Instalments
	 */
	EquityRelease.Instalments toInstalments(Application.LoanDetails.EquityRelease.Instalments instalments);
	
	@InheritInverseConfiguration
	Application.LoanDetails.EquityRelease.Instalments fromInstalments(EquityRelease.Instalments entity);	
	
	
	

	/**
	 * LoanDetails.FeaturesSelected
	 */
	FeaturesSelected toFeaturesSelected(Application.LoanDetails.FeaturesSelected featuresSelected);
	
	@InheritInverseConfiguration
	Application.LoanDetails.FeaturesSelected fromFeaturesSelected(FeaturesSelected entity);	
	
	
	/**
	 * LoanDetails.FeaturesSelected.DepositAccount
	 */
	FeaturesSelected.DepositAccount toDepositAccount(Application.LoanDetails.FeaturesSelected.DepositAccount extraFeature);
	
	@InheritInverseConfiguration
	Application.LoanDetails.FeaturesSelected.DepositAccount fromDepositAccount(FeaturesSelected.DepositAccount entity);	
	
	
	/**
	 * List<LoanDetails.FeaturesSelected.ExtraFeature>
	 */
	List<FeaturesSelected.DepositAccount> toDepositAccounts(List<Application.LoanDetails.FeaturesSelected.DepositAccount> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.FeaturesSelected.DepositAccount> fromDepositAccounts(List<FeaturesSelected.DepositAccount> entities);
	
	
	
	/**
	 * LoanDetails.FeaturesSelected.ExtraFeature
	 */
	FeaturesSelected.ExtraFeature toExtraFeature(Application.LoanDetails.FeaturesSelected.ExtraFeature extraFeature);
	
	@InheritInverseConfiguration
	Application.LoanDetails.FeaturesSelected.ExtraFeature fromExtraFeature(FeaturesSelected.ExtraFeature entity);	
	
	
	/**
	 * List<LoanDetails.FeaturesSelected.ExtraFeature>
	 */
	List<FeaturesSelected.ExtraFeature> toExtraFeatures(List<Application.LoanDetails.FeaturesSelected.ExtraFeature> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.FeaturesSelected.ExtraFeature> fromExtraFeatures(List<FeaturesSelected.ExtraFeature> entities);
		
	

	/**
	 * LoanDetails.FeaturesSelected.OffsetAccount
	 */
	@Mapping(target = "XAccount", source = "offsetAccount.XAccount", qualifiedBy = ObjectToString.class)
	OffsetAccount toOffsetAccount(Application.LoanDetails.FeaturesSelected.OffsetAccount offsetAccount);
	
	@Mapping(target = "XAccount", source = "entity.XAccount", qualifiedBy = StringToObject.class)
	Application.LoanDetails.FeaturesSelected.OffsetAccount fromOffsetAccount(OffsetAccount entity);	
	
	
	/**
	 * List<LoanDetails.FeaturesSelected.OffsetAccount>
	 */
	List<OffsetAccount> toOffsetAccounts(List<Application.LoanDetails.FeaturesSelected.OffsetAccount> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.FeaturesSelected.OffsetAccount> fromOffsetAccounts(List<OffsetAccount> entities);
		
	
	
	/**
	 * LoanDetails::Guarantor
	 */
	@Mapping(target = "XGuarantor", source = "guarantor.XGuarantor", qualifiedBy = ObjectToString.class)
	LoanDetails.Guarantor toGuarantor(Application.LoanDetails.Guarantor guarantor);
	
	@Mapping(target = "XGuarantor", source = "entity.XGuarantor", qualifiedBy = StringToObject.class)
	Application.LoanDetails.Guarantor fromGuarantor(LoanDetails.Guarantor entity);

	
	/**
	 * List<LoanDetails>::List<Guarantor>
	 */
	List<LoanDetails.Guarantor> toGuarantors(List<Application.LoanDetails.Guarantor> guarantors);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Guarantor> fromGuarantors(List<LoanDetails.Guarantor> entities);	
	
	
	
	/**
	 * LoanDetails.LendingPurpose
	 */
	@Mappings({
		@Mapping(target = "absLendingPurpose", source = "lendingPurpose.ABSLendingPurpose"),
		@Mapping(target = "absLendingPurposeCode", source = "lendingPurpose.ABSLendingPurposeCode")
	})
	LendingPurpose toLendingPurpose(Application.LoanDetails.LendingPurpose lendingPurpose);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "ABSLendingPurpose", source = "entity.absLendingPurpose"),
		@Mapping(target = "ABSLendingPurposeCode", source = "entity.absLendingPurposeCode")
	})
	Application.LoanDetails.LendingPurpose fromLendingPurpose(LendingPurpose entity);	
	
	
	/**
	 * List<LoanDetails.LendingPurpose>
	 */
	
	List<LoanDetails> toLendingPurposes(List<Application.LoanDetails.LendingPurpose> lendingPurposes);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.LendingPurpose> fromLendingPurposes(List<LendingPurpose> entities);
		
	
	
	/**
	 * LoanDetails.LoanPurpose
	 */
	@Mapping(target = "nccpStatus", source = "loanPurpose.NCCPStatus")
	LoanDetails.LoanPurpose toLoanPurpose(Application.LoanDetails.LoanPurpose loanPurpose);
	
	@InheritInverseConfiguration
	@Mapping(target = "NCCPStatus", source = "entity.nccpStatus")
	Application.LoanDetails.LoanPurpose fromLendingPurpose(LoanDetails.LoanPurpose entity);	
		
	
	
	/**
	 * LoanDetails.Package
	 */
	LoanDetails.Package toPackage(Application.LoanDetails.Package ldpackage);
	
	@InheritInverseConfiguration
	Application.LoanDetails.Package fromPackage(LoanDetails.Package entity);	
		
	
	/**
	 * LoanDetails.ProposedRepayment
	 */
	@Mappings({
		@Mapping(target = "XAccount", source = "proposedRepayment.XAccount", qualifiedBy = ObjectToString.class),
		@Mapping(target = "anniversaryDate", source = "proposedRepayment.anniversaryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "perDiemPaymentDate", source = "proposedRepayment.perDiemPaymentDate", qualifiedBy = GregorianToLocal.class)
	})
	ProposedRepayment toProposedRepayment(Application.LoanDetails.ProposedRepayment proposedRepayment);
	
	@Mappings({
		@Mapping(target = "XAccount", source = "entity.XAccount", qualifiedBy = StringToObject.class),
		@Mapping(target = "anniversaryDate", source = "entity.anniversaryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "perDiemPaymentDate", source = "entity.perDiemPaymentDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.LoanDetails.ProposedRepayment fromProposedRepayment(ProposedRepayment entity);	
		
	
	/**
	 * LoanDetails.ProposedRepayment.Authoriser
	 */
	@Mapping(target = "XParty", source = "authoriser.XParty", qualifiedBy = ObjectToString.class)
	ProposedRepayment.Authoriser toAuthoriser(Application.LoanDetails.ProposedRepayment.Authoriser authoriser);
	
	@Mapping(target = "XParty", source = "entity.XParty", qualifiedBy = StringToObject.class)
	Application.LoanDetails.ProposedRepayment.Authoriser fromAuthoriser(ProposedRepayment.Authoriser entity);	
	
	
	/**
	 * List<LoanDetails.ProposedRepayment.Authoriser>
	 */
	List<ProposedRepayment.Authoriser> toAuthorisers(List<Application.LoanDetails.ProposedRepayment.Authoriser> authorisers);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.ProposedRepayment.Authoriser> fromAuthorisers(List<ProposedRepayment.Authoriser> entities);
		
	
	/**
	 * LoanDetails.ProposedRepayment.CreditCard
	 */
	CreditCard toCreditCard(Application.LoanDetails.ProposedRepayment.CreditCard creditCard);
	
	@InheritInverseConfiguration
	Application.LoanDetails.ProposedRepayment.CreditCard fromCreditCard(CreditCard entity);	

	
	
	
	/**
	 * LoanDetails.ProposedRepayment.RegularRepayment
	 */
	@Mappings({
		@Mapping(target = "gstAmount", source = "regularRepayment.GSTAmount"),
		@Mapping(target = "firstRepaymentDate", source = "regularRepayment.firstRepaymentDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "lastRepaymentDate", source = "regularRepayment.lastRepaymentDate", qualifiedBy = GregorianToLocal.class)
	})
	ProposedRepayment.RegularRepayment toRegularRepayment(Application.LoanDetails.ProposedRepayment.RegularRepayment regularRepayment);
	
	
	@Mappings({
		@Mapping(target = "GSTAmount", source = "entity.gstAmount"),
		@Mapping(target = "firstRepaymentDate", source = "entity.firstRepaymentDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "lastRepaymentDate", source = "entity.lastRepaymentDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.LoanDetails.ProposedRepayment.RegularRepayment fromRegularRepayment(ProposedRepayment.RegularRepayment entity);	
	
	
	/**
	 * List<LoanDetails.ProposedRepayment.RegularRepayment>
	 */
	List<ProposedRepayment.RegularRepayment> toRegularRepayments(List<Application.LoanDetails.ProposedRepayment.RegularRepayment> regularRepayments);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.ProposedRepayment.RegularRepayment> fromRegularRepayments(List<ProposedRepayment.RegularRepayment> entities);
		
	
	
	
	/**
	 * LoanDetails.ProposedRepayment.StructuredPayments
	 */
	ProposedRepayment.StructuredPayments toStructuredPayments(Application.LoanDetails.ProposedRepayment.StructuredPayments structuredRepayments);
	
	@InheritInverseConfiguration
	Application.LoanDetails.ProposedRepayment.StructuredPayments fromStructuredPayments(ProposedRepayment.StructuredPayments entity);	
	
	
	/**
	 * LoanDetails.ProposedRepayment.StructuredPayments.Payment
	 */
	@Mapping(target = "gstAmount", source = "payment.GSTAmount")
	Payment toPayment(Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment payment);
	
	@InheritInverseConfiguration
	@Mapping(target = "GSTAmount", source = "entity.gstAmount")
	Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment fromPayment(Payment entity);	
	

	
	/**
	 * List<LoanDetails.ProposedRepayment.StructuredPayments.Payment>
	 */
	List<Payment> toPayments(List<Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment> regularRepayments);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment> fromPayments(List<Payment> entities);
		
	/**
	 * LoanDetails.RateComposition
	 */
	RateComposition toRateComposition(Application.LoanDetails.RateComposition rateComposition);
	
	@InheritInverseConfiguration
	Application.LoanDetails.RateComposition fromRateComposition(RateComposition entity);
	
	
	
	/**
	 * List<LoanDetails.RateComposition>
	 */
	List<RateComposition> toRateCompositions(List<Application.LoanDetails.RateComposition> rateCompositions);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.RateComposition> fromRateCompositions(List<RateComposition> entities);
	
	
	
	/**
	 * LoanDetails::RateComposition::BaseRate
	 */
	BaseRate toBaseRate(Application.LoanDetails.RateComposition.BaseRate baseRate);
	
	@InheritInverseConfiguration
	Application.LoanDetails.RateComposition.BaseRate fromBaseRate(BaseRate entity);	
		
	
	
	/**
	 * LoanDetails.Security
	 */
	@Mapping(target = "XSecurity", source = "security.XSecurity", qualifiedBy = ObjectToString.class)
	Security toSecurity(Application.LoanDetails.Security security);
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.LoanDetails.Security fromSecurity(Security entity);	
	
	
	
	/**
	 * List<LoanDetails.Security>
	 */
	List<Security> toSecurities(List<Application.LoanDetails.Security> securities);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Security> fromSecurities(List<Security> entities);	
		

	
	
	/**
	 * LoanDetails.Software
	 */
	Software toSoftware(Application.LoanDetails.Software software);
	
	@InheritInverseConfiguration
	Application.LoanDetails.Software fromSoftware(Software entity);	
			

	
	
	
	/**
	 * LoanDetails.StatementInstructions
	 */
	StatementInstructions toStatementInstructions(Application.LoanDetails.StatementInstructions statementInstructions);
	
	@InheritInverseConfiguration
	Application.LoanDetails.StatementInstructions fromStatementInstructions(StatementInstructions entity);	
	
	
	
	
	/**
	 * LoanDetails.StatementInstructions.NameOnStatement
	 */
	@Mapping(target = "XApplicant", source = "nameOnStatement.XApplicant", qualifiedBy = ObjectToString.class)
	StatementInstructions.NameOnStatement toNameOnStatement(Application.LoanDetails.StatementInstructions.NameOnStatement nameOnStatement);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	Application.LoanDetails.StatementInstructions.NameOnStatement fromNameOnStatement(StatementInstructions.NameOnStatement entity);	

	
	
	/**
	 * List<LoanDetails.StatementInstructions.NameOnStatement>
	 */
	List<StatementInstructions.NameOnStatement> toNameOnStatements(List<Application.LoanDetails.StatementInstructions.NameOnStatement> nameOnStatements);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.StatementInstructions.NameOnStatement> fromNameOnStatements(List<StatementInstructions.NameOnStatement> entities);	
		

	
	
	/**
	 * LoanDetails.SupplementaryCardholder
	 */
	@Mapping(target = "XSupplementaryCardholder", source = "supplementaryCardholder.XSupplementaryCardholder", qualifiedBy = ObjectToString.class)
	SupplementaryCardholder toSupplementaryCardholder(Application.LoanDetails.SupplementaryCardholder supplementaryCardholder);
	
	@Mapping(target = "XSupplementaryCardholder", source = "entity.XSupplementaryCardholder", qualifiedBy = StringToObject.class)
	Application.LoanDetails.SupplementaryCardholder fromSupplementaryCardholder(SupplementaryCardholder entity);	

	
	
	/**
	 * List<LoanDetails.SupplementaryCardholder>
	 */
	List<SupplementaryCardholder> toSupplementaryCardholders(List<Application.LoanDetails.SupplementaryCardholder> nameOnStatements);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.SupplementaryCardholder> fromSupplementaryCardholders(List<SupplementaryCardholder> entities);	
	
	
	
	
	/**
	 * LoanDetails.Term
	 */
	Term toTerm(Application.LoanDetails.Term term);
	
	@InheritInverseConfiguration
	Application.LoanDetails.Term fromTerm(Term entity);	


	/**
	 * LoanDetails.Term.DistinctLoanPeriod
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "dlp.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "dlp.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XRateComposition", source = "dlp.XRateComposition", qualifiedBy = ObjectToString.class)
	})	
	DistinctLoanPeriod toDistinctLoanPeriod(Application.LoanDetails.Term.DistinctLoanPeriod dlp);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XRateComposition", source = "entity.XRateComposition", qualifiedBy = StringToObject.class)
	})
	Application.LoanDetails.Term.DistinctLoanPeriod fromDistinctLoanPeriod(DistinctLoanPeriod entity);	
	
	
	/**
	 * List<LoanDetails.Term.DistinctLoanPeriod>
	 */
	List<DistinctLoanPeriod> toDistinctLoanPeriods(List<Application.LoanDetails.Term.DistinctLoanPeriod> distinctLoanPeriods);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Term.DistinctLoanPeriod> fromDistinctLoanPeriods(List<DistinctLoanPeriod> entities);
		
	
	/**
	 * LoanDetails.Term.DistinctLoanPeriod.Repayment
	 */
	@Mapping(target = "XRepayment", source = "repayment.XRepayment", qualifiedBy = ObjectToString.class)
	Repayment toRepayment(Application.LoanDetails.Term.DistinctLoanPeriod.Repayment repayment);
	
	@Mapping(target = "XRepayment", source = "entity.XRepayment", qualifiedBy = StringToObject.class)
	Application.LoanDetails.Term.DistinctLoanPeriod.Repayment fromRepayment(Repayment entity);	
	
	
	/**
	 * List<LoanDetails.Term.DistinctLoanPeriod.Repayment>
	 */
	List<Repayment> toRepayments(List<Application.LoanDetails.Term.DistinctLoanPeriod.Repayment> repayments);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Term.DistinctLoanPeriod.Repayment> fromRepayments(List<Repayment> entities);
		
	
	
	

	/**
	 * LoanDetails.Term.InterestOnlyReason
	 */
	Term.InterestOnlyReason toInterestOnly(Application.LoanDetails.Term.InterestOnlyReason interestOnly);
	
	@InheritInverseConfiguration
	Application.LoanDetails.Term.InterestOnlyReason fromInterestOnly(Term.InterestOnlyReason entity);	

	
	
	/**
	 * List<LoanDetails.Term.InterestOnlyReason>
	 */
	List<Term.InterestOnlyReason> toInterestOnlyList(List<Application.LoanDetails.Term.InterestOnlyReason> interestOnlyList);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.Term.InterestOnlyReason> fromInterestOnlyList(List<Term.InterestOnlyReason> entities);	

	
	
	
	/**
	 * LoanDetails::TermsAndConditions
	 */
	TermsAndConditions toTermsAndConditions(Application.LoanDetails.TermsAndConditions termsAndConditions);
	
	@InheritInverseConfiguration
	Application.LoanDetails.TermsAndConditions fromTermsAndConditions(TermsAndConditions entity);	
	
	
	
	/**
	 * List<LoanDetails::TermsAndConditions>
	 */
	List<TermsAndConditions> toTermsAndConditionsList(List<Application.LoanDetails.TermsAndConditions> termsAndConditionsList);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.TermsAndConditions> fromTermsAndConditionsList(List<TermsAndConditions> entities);	
		
	
	
	
}
