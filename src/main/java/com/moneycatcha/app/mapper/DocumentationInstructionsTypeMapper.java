package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.DocumentationInstructionsType;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface DocumentationInstructionsTypeMapper {

	/**
	 * DocumentInstructionsType
	 */
	@Mappings({
		@Mapping(target = "XDeliverTo", source = "document.XDeliverTo", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XNominatedAuthority", source = "document.XNominatedAuthority", qualifiedBy = ObjectToString.class)
	})
	DocumentationInstructionsType toDocumentationInstructionsType(com.moneycatcha.app.model.DocumentationInstructionsType document);
	
	@Mappings({
		@Mapping(target = "XDeliverTo", source = "entity.XDeliverTo", qualifiedBy = StringToObject.class),
		@Mapping(target = "XNominatedAuthority", source = "entity.XNominatedAuthority", qualifiedBy = StringToObject.class)
	})
	com.moneycatcha.app.model.DocumentationInstructionsType fromDocumentationInstructionsType(DocumentationInstructionsType entity);
}
