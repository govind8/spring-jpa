package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.SignatureType;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		PhoneTypeMapper.class,
		StringObjectMapper.class
	})
public interface SignatureTypeMapper {

	
	SignatureTypeMapper INSTANCE = Mappers.getMapper(SignatureTypeMapper.class);


	/**
	 * SignatureType
	 */
	@Mappings({
		@Mapping(target = "date", source = "signature.date", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XSignatory", source = "signature.XSignatory", qualifiedBy = ObjectToString.class)
	})
	SignatureType toSignature(com.moneycatcha.app.model.SignatureType signature);
	
	@Mappings({
		@Mapping(target = "date", source = "entity.date", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XSignatory", source = "entity.XSignatory", qualifiedBy = StringToObject.class)
	})
	com.moneycatcha.app.model.SignatureType fromSignature(SignatureType entity); 

	
	/**
	 * List<SignatureType>
	 */
	List<SignatureType> toSignatures(List<com.moneycatcha.app.model.SignatureType> signatures);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.SignatureType> fromSignatures(List<SignatureType> entities);
	
	
	/**
	 * SignatureType.Capacity
	 */
	@Mappings({
		@Mapping(target = "XParentEntity", source = "capacity.XParentEntity", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XProductSet", source = "capacity.XProductSet", qualifiedBy = ObjectToString.class)
	})
	SignatureType.Capacity toCapacity(com.moneycatcha.app.model.SignatureType.Capacity capacity);
	
	@Mappings({
		@Mapping(target = "XParentEntity", source = "entity.XParentEntity", qualifiedBy = StringToObject.class),
		@Mapping(target = "XProductSet", source = "entity.XProductSet", qualifiedBy = StringToObject.class)
	})
	com.moneycatcha.app.model.SignatureType.Capacity fromCapacity(SignatureType.Capacity entity); 

	
	
	/**
	 * SignatureType.Contact
	 */
	Contact toContact(com.moneycatcha.app.model.SignatureType.Contact signature);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.SignatureType.Contact fromContact(Contact entity); 

	

}
