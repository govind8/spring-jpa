package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.SchemaVersion;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface SchemaVersionMapper {

	SchemaVersionMapper INSTANCE = Mappers.getMapper(SchemaVersionMapper.class);

	/**
	 * SchemaVersion
	 */
	@Mappings({
		@Mapping(target = "lixiCustomVersion", source = "schemaVersion.LIXICustomVersion"),
		@Mapping(target = "lixiTransactionType", source = "schemaVersion.LIXITransactionType"),
		@Mapping(target = "lixiVersion", source = "schemaVersion.LIXIVersion")
	})
	SchemaVersion toSchemaVersion(Message.SchemaVersion schemaVersion);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "LIXICustomVersion", source = "entity.lixiCustomVersion"),
		@Mapping(target = "LIXITransactionType", source = "entity.lixiTransactionType"),
		@Mapping(target = "LIXIVersion", source = "entity.lixiVersion")
	})
	Message.SchemaVersion fromSchemaVersion(SchemaVersion entity);
	
}
