package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.FinancialAccountType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
			BranchDomicileMapper.class
	}
)
public interface FinancialAccountTypeMapper {
	
	/**
	 * FinancialAccountType - AccountNumber
	 */
	@Mapping(target = "bsb", source = "accountNumber.BSB")
	FinancialAccountType toAccountNumber(com.moneycatcha.app.model.FinancialAccountType accountNumber);
	
	@Mapping(target = "BSB", source = "entity.bsb")
	com.moneycatcha.app.model.FinancialAccountType fromAccountNumber(FinancialAccountType entity);

}
