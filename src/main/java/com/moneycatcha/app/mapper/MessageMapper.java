package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Message;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		AttachmentMapper.class,
		ContentMapper.class,
		InstructionsMapper.class,
		PublisherMapper.class,
		RecipientMapper.class,
		SchemaVersionMapper.class,
		TransformMetadataMapper.class
	})
public interface MessageMapper {

	MessageMapper INSTANCE = Mappers.getMapper(MessageMapper.class);
	
	/**
	 * Message
	 */

	Message toMessage(com.moneycatcha.app.model.Message message);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.Message fromMessage(Message message);

}
