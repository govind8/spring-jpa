package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Addback;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant.Employment.SelfEmployed;
import com.moneycatcha.app.model.Message.Content.Application.TrustApplicant;


@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {OtherAddbackMapper.class})
public interface AddbackMapper {


	/**
	 * CompanyApplicant.IncomePrevious - Addback
	 */
	Addback toCAIncomePreviousAddback(CompanyApplicant.IncomePrevious.Addback addBack);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomePrevious.Addback fromCAIncomePreviousAddback(Addback entity);

	/**
	 * CompanyApplicant.IncomeRecent - Addback
	 */
	Addback toCAIncomeRecentAddback(CompanyApplicant.IncomeRecent.Addback addBack);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomeRecent.Addback fromCAIncomeRecentAddback(Addback entity);

	
	/**
	 * CompanyApplicant.IncomePrior - AddBack
	 */
	Addback toCAIncomePriorAddback(CompanyApplicant.IncomePrior.Addback addBack);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomePrior.Addback fromCAIncomePriorAddback(Addback entity);

	
	/**
	 * CompanyApplicant.IncomeYearToDate - Addback
	 */
	Addback toCAIncomeYearToDateAddback(CompanyApplicant.IncomeYearToDate.Addback addBack);
	
	@InheritInverseConfiguration
	CompanyApplicant.IncomeYearToDate.Addback fromCAIncomeYearToDateAddback(Addback entity);

		

	
	/**
	 * BusinessIncomePrevious - Addback
	 */
	Addback toBusinessIncomePreviousAddback(SelfEmployed.BusinessIncomePrevious.Addback addBack);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomePrevious.Addback fromBusinessIncomePreviousAddback(Addback entity);

	/**
	 * BusinessIncomeRecent - Addback
	 */
	Addback toBusinessIncomeRecentAddback(SelfEmployed.BusinessIncomeRecent.Addback addBack);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomeRecent.Addback fromBusinessIncomeRecentAddback(Addback entity);

	
	/**
	 * BusinessIncomePrior - AddBack
	 */
	Addback toBusinessIncomePriorAddback(SelfEmployed.BusinessIncomePrior.Addback addBack);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomePrior.Addback fromBusinessIncomePriorAddback(Addback entity);

	
	/**
	 * BusinessIncomeYearToDate - Addback
	 */
	Addback toBusinessIncomeYearToDateAddback(SelfEmployed.BusinessIncomeYearToDate.Addback addBack);
	
	@InheritInverseConfiguration
	SelfEmployed.BusinessIncomeYearToDate.Addback fromBusinessIncomeYearToDateAddback(Addback entity);


	
	
	/**
	 * TrustApplicant.IncomePrevious - Addback
	 */
	Addback toTAIncomePreviousAddback(TrustApplicant.IncomePrevious.Addback addBack);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomePrevious.Addback fromTAIncomePreviousAddback(Addback entity);

	/**
	 * TrustApplicant.IncomeRecent - Addback
	 */
	Addback toTAIncomeRecentAddback(TrustApplicant.IncomeRecent.Addback addBack);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomeRecent.Addback fromTAIncomeRecentAddback(Addback entity);

	
	/**
	 * TrustApplicant.IncomePrior - AddBack
	 */
	Addback toTAIncomePriorAddback(TrustApplicant.IncomePrior.Addback addBack);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomePrior.Addback fromTAIncomePriorAddback(Addback entity);

	
	/**
	 * TrustApplicant.IncomeYearToDate - Addback
	 */
	Addback toTAIncomeYearToDateAddback(TrustApplicant.IncomeYearToDate.Addback addBack);
	
	@InheritInverseConfiguration
	TrustApplicant.IncomeYearToDate.Addback fromTAIncomeYearToDateAddback(Addback entity);

	
	
	
}
