package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.FinancialAnalysis;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class})
public interface FinancialAnalysisMapper {
	
	/**
	 * CompanyApplicant.FinancialAnalysis
	 */
	FinancialAnalysis toCAFinancialAnalysis(Application.CompanyApplicant.FinancialAnalysis finAnalysis);
	
	@InheritInverseConfiguration
	Application.CompanyApplicant.FinancialAnalysis fromCAFinancialAnalysis(FinancialAnalysis entity);


	/**
	 * CompanyApplicant.FinancialAnalysis.CompanyFinanicals
	 */
	@Mapping(target = "XCompanyFinancials", source = "compfinancials.XCompanyFinancials", qualifiedBy = ObjectToString.class)
	FinancialAnalysis.CompanyFinancials toCompanyFinancials(Application.CompanyApplicant.FinancialAnalysis.CompanyFinancials compfinancials);
	
	@Mapping(target = "XCompanyFinancials", source = "entity.XCompanyFinancials", qualifiedBy = StringToObject.class)
	Application.CompanyApplicant.FinancialAnalysis.CompanyFinancials fromCACompanyFinancials(FinancialAnalysis.CompanyFinancials entity);


	
	/**
	 *  FinancialAnalysis.CompanyFinanicals - List
	 */
	
	List<FinancialAnalysis.CompanyFinancials> toCACompanyFinancialsList(List<Application.CompanyApplicant.FinancialAnalysis.CompanyFinancials> compfinList);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant.FinancialAnalysis.CompanyFinancials> fromCACompanyFinancialsList(List<FinancialAnalysis.CompanyFinancials> entityList);

	
	
	/**
	 * TrustApplicant - FinancialAnalysis
	 */
	FinancialAnalysis toTAFinancialAnalysis(Application.TrustApplicant.FinancialAnalysis finAnalysis);
	
	@InheritInverseConfiguration
	Application.TrustApplicant.FinancialAnalysis  fromTAFinancialAnalysis(FinancialAnalysis entity);


	/**
	 * TrustApplicant.FinancialAnalysis.CompanyFinanicals
	 */
	@Mapping(target = "XCompanyFinancials", source = "compfinancials.XCompanyFinancials", qualifiedBy = ObjectToString.class)
	FinancialAnalysis.CompanyFinancials toTACompanyFinancials(Application.TrustApplicant.FinancialAnalysis.CompanyFinancials compfinancials);
	
	@Mapping(target = "XCompanyFinancials", source = "entity.XCompanyFinancials", qualifiedBy = StringToObject.class)
	Application.TrustApplicant.FinancialAnalysis.CompanyFinancials fromTACompanyFinancials(FinancialAnalysis.CompanyFinancials entity);

	
	
	/**
	 *  TrustApplicant.FinancialAnalysis.CompanyFinanicals - List
	 */
	
	List<FinancialAnalysis.CompanyFinancials> toTACompanyFinancialsList(List<Application.TrustApplicant.FinancialAnalysis.CompanyFinancials> compfinList);
	
	@InheritInverseConfiguration
	List<Application.TrustApplicant.FinancialAnalysis.CompanyFinancials> fromTACompanyFinancialsList(List<FinancialAnalysis.CompanyFinancials> entityList);

	
	/**
	 * NonRealEstateAsset.Business.FinancialAnalysis
	 */
	FinancialAnalysis toNREAFinancialAnalysis(Application.NonRealEstateAsset.Business.FinancialAnalysis finAnalysis);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.Business.FinancialAnalysis fromNREAFinancialAnalysis(FinancialAnalysis entity);


	/**
	 * NonRealEstateAsset.Business.CompanyFinanicals
	 */
	@Mapping(target = "XCompanyFinancials", source = "compfinancials.XCompanyFinancials", qualifiedBy = ObjectToString.class)
	FinancialAnalysis.CompanyFinancials toCompanyFinancials(Application.NonRealEstateAsset.Business.FinancialAnalysis.CompanyFinancials compfinancials);
	
	@Mapping(target = "XCompanyFinancials", source = "entity.XCompanyFinancials", qualifiedBy = StringToObject.class)
	Application.NonRealEstateAsset.Business.FinancialAnalysis.CompanyFinancials fromNREACompanyFinancials(FinancialAnalysis.CompanyFinancials entity);


	
	/**
	 *  NonRealEstateAsset.Business.FinancialAnalysis.CompanyFinanicals - List
	 */
	
	List<FinancialAnalysis.CompanyFinancials> toNREACompanyFinancialsList(List<Application.NonRealEstateAsset.Business.FinancialAnalysis.CompanyFinancials> compfinList);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.Business.FinancialAnalysis.CompanyFinancials> fromNREACompanyFinancialsList(List<FinancialAnalysis.CompanyFinancials> entityList);
}
