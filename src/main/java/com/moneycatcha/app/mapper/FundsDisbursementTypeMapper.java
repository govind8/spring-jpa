package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.FundsDisbursementType;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
			FinancialAccountTypeMapper.class,
			DateMapper.class, 
			PersonNameTypeMapper.class,
			StringObjectMapper.class
	})
public interface FundsDisbursementTypeMapper {
	
	FundsDisbursementTypeMapper INSTANCE = Mappers.getMapper(FundsDisbursementTypeMapper.class);

	/**
	 * FundsDisbursementType
	 */
	@Mappings({
		@Mapping(target = "XAccount", source = "fundsDisbursementType.XAccount", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XAddress", source = "fundsDisbursementType.XAddress", qualifiedBy = ObjectToString.class)
	})
	FundsDisbursementType toOffsetAccount(com.moneycatcha.app.model.FundsDisbursementType fundsDisbursementType);
	
	@Mappings({
		@Mapping(target = "XAccount", source = "entity.XAccount", qualifiedBy = StringToObject.class),
		@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class)
	})
	com.moneycatcha.app.model.FundsDisbursementType fromOffsetAccount(FundsDisbursementType entity);	
	
	
	/**
	 * List<FundsDisbursementType>
	 */
	List<FundsDisbursementType> toFundsDisbursementTypes(List<com.moneycatcha.app.model.FundsDisbursementType> extraFeatures);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.FundsDisbursementType> fromFundsDisbursementTypes(List<FundsDisbursementType> entities);
	
	
	
	/**
	 * FundsDisbursementType.BillerDetails
	 */
	FundsDisbursementType.BillerDetails toBillerDetails(com.moneycatcha.app.model.FundsDisbursementType.BillerDetails billerDetails);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.FundsDisbursementType.BillerDetails fromBillerDetails(FundsDisbursementType.BillerDetails entity);	
	
	
}
