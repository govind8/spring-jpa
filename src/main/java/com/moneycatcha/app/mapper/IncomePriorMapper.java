package com.moneycatcha.app.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.IncomePrior;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {AddbackMapper.class,
		DateMapper.class,
		StringObjectMapper.class
})
public interface IncomePriorMapper {

	
	/**
	 * CA IncomePrior
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomePrior.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomePrior.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrior.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomePrior toCAIncomePrior(Application.CompanyApplicant.IncomePrior incomePrior);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.CompanyApplicant.IncomePrior fromCAIncomePrior(IncomePrior entity);

	
	
	/**
	 * TA - IncomePrior
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "incomePrior.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "incomePrior.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrior.XAccountant", qualifiedBy = ObjectToString.class),
	})
	IncomePrior toTAIncomePrior(Application.TrustApplicant.IncomePrior incomePrior);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)	
	})
	Application.TrustApplicant.IncomePrior fromTAIncomePrior(IncomePrior entity);
	
}
