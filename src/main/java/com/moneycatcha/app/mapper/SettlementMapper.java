package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Settlement;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
uses = {
		DateMapper.class,
		PhoneTypeMapper.class,
		StringObjectMapper.class
})
public interface SettlementMapper {

	SettlementMapper INSTANCE = Mappers.getMapper(SettlementMapper.class);

	/**
	 * Settlement
	 */
	@Mappings({
		@Mapping(target = "settlementBookingDateTime", source = "settlement.settlementBookingDateTime", qualifiedBy = GregorianToLocalDateTime.class),
		@Mapping(target = "XSettlementBookingAddress", source = "settlement.XSettlementBookingAddress", qualifiedBy = ObjectToString.class)
	})
	Settlement toSettlement(Application.Settlement settlement);
	
	@Mappings({
		@Mapping(target = "settlementBookingDateTime", source = "entity.settlementBookingDateTime", qualifiedBy = LocalDateTimeToGregorian.class),
		@Mapping(target = "XSettlementBookingAddress", source = "entity.XSettlementBookingAddress", qualifiedBy = StringToObject.class)
	})
	Application.Settlement fromSettlement(Settlement entity); 

	
	/**
	 * List<Settlement>
	 */
	List<Settlement> toSettlements(List<Application.Settlement> settlements);
	
	@InheritInverseConfiguration
	List<Application.Settlement> fromSettlements(List<Settlement> entities);


	/**
	 * Security
	 */
	@Mapping(target = "XSecurity", source = "security.XSecurity", qualifiedBy = ObjectToString.class)
	Settlement.Security toSecurity(Application.Settlement.Security security);
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.Settlement.Security fromSecurity(Settlement.Security entity);

	
	/**
	 * List<Settlement.Security>
	 */
	List<Settlement.Security> toSecurityList(List<Application.Settlement.Security> securityList);
	
	@InheritInverseConfiguration
	List<Application.Settlement.Security> fromSecurityList(List<Settlement.Security> entities);

	
	
}
