package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.ContributionFunds;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
		StringObjectMapper.class
	}
)
public interface ContributionFundsMapper {

	ContributionFundsMapper INSTANCE = Mappers.getMapper(ContributionFundsMapper.class);

	/**
	 * ContributionFunds
	 */
	@Mappings({
		@Mapping(target = "XAssociatedLoanAccount", source = "contributionFunds.XAssociatedLoanAccount", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XFundsHeldInAccount", source = "contributionFunds.XFundsHeldInAccount", qualifiedBy = ObjectToString.class)
	})
	ContributionFunds toContributionFunds(Application.ContributionFunds contributionFunds);
	
	@Mappings({
		@Mapping(target = "XAssociatedLoanAccount", source = "entity.XAssociatedLoanAccount", qualifiedBy = StringToObject.class),
		@Mapping(target = "XFundsHeldInAccount", source = "entity.XFundsHeldInAccount", qualifiedBy = StringToObject.class)
	})
	Application.ContributionFunds fromContributionFunds(ContributionFunds entity);

	
	/**
	 * List<ContributionFunds>
	 */
	List<ContributionFunds> toContributionFundsList(List<Application.ContributionFunds> contributionFundsList);
	
	@InheritInverseConfiguration
	List<Application.ContributionFunds> fromContributionFundsList(List<ContributionFunds> entities);

}
