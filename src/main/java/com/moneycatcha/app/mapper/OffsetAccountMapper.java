package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {
			DateMapper.class,
			StringObjectMapper.class,
			FinancialAccountTypeMapper.class
		}
	)
public interface OffsetAccountMapper {


	/**
	 * LoanDetails.FeaturesSelected.OffsetAccount
	 */
	@Mapping(target = "XAccount", source = "offsetAccount.XAccount", qualifiedBy = ObjectToString.class)
	OffsetAccount toOffsetAccount(Application.LoanDetails.FeaturesSelected.OffsetAccount offsetAccount);
	
	@Mapping(target = "XAccount", source = "entity.XAccount", qualifiedBy = StringToObject.class)
	Application.LoanDetails.FeaturesSelected.OffsetAccount fromOffsetAccount(OffsetAccount entity);	
	
	
	/**
	 * List<LoanDetails.FeaturesSelected.OffsetAccount>
	 */
	List<OffsetAccount> toOffsetAccounts(List<Application.LoanDetails.FeaturesSelected.OffsetAccount> extraFeatures);
	
	@InheritInverseConfiguration
	List<Application.LoanDetails.FeaturesSelected.OffsetAccount> fromOffsetAccounts(List<OffsetAccount> entities);
}
