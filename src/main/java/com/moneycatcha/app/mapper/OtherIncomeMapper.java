package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.OtherIncome;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		PercentOwnedTypeMapper.class,
		StringObjectMapper.class
	})
public interface OtherIncomeMapper {
	
	OtherIncomeMapper INSTANCE = Mappers.getMapper(OtherIncomeMapper.class);


	/**
	 * OtherIncome
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "otherIncome.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "otherIncome.endDate", qualifiedBy = ObjectToString.class),
		@Mapping(target = "gstAmount", source = "otherIncome.GSTAmount"),
		@Mapping(target = "XAsset", source = "otherIncome.XAsset", qualifiedBy = ObjectToString.class),
	})
	OtherIncome toOtherIncome(Application.OtherIncome otherIncome);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "GSTAmount", source = "entity.gstAmount"),
		@Mapping(target = "XAsset", source = "entity.XAsset", qualifiedBy = StringToObject.class)
	})
	Application.OtherIncome fromOtherIncome(OtherIncome entity); 

	
	/**
	 * List<OtherIncome>
	 */
	List<OtherIncome> toOtherIncomes(List<Application.OtherIncome> otherIncomes);
	
	@InheritInverseConfiguration
	List<Application.OtherIncome> fromOtherIncomes(List<OtherIncome> entities);
	
}
