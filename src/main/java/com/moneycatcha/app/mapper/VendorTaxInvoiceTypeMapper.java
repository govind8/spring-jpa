package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.VendorTaxInvoiceType;
import com.moneycatcha.app.entity.VendorTaxInvoiceType.Asset;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring",
uses = { 
	DateMapper.class,
	StringObjectMapper.class 
})
public interface VendorTaxInvoiceTypeMapper {

	VendorTaxInvoiceTypeMapper INSTANCE = Mappers.getMapper(VendorTaxInvoiceTypeMapper.class);

	/**
	 * VendorTaxInvoiceType
	 */
	
	@Mappings({
		@Mapping(target = "taxInvoiceDate", source = "vendorTaxInvoiceType.taxInvoiceDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XDeliverTo", source = "vendorTaxInvoiceType.XDeliverTo", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPurchaser", source = "vendorTaxInvoiceType.XPurchaser", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XVendor", source = "vendorTaxInvoiceType.XVendor", qualifiedBy = ObjectToString.class)
	})
	VendorTaxInvoiceType toVendorTaxInvoiceType(com.moneycatcha.app.model.VendorTaxInvoiceType vendorTaxInvoiceType);
	
	@Mappings({
		@Mapping(target = "taxInvoiceDate", source = "entity.taxInvoiceDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XDeliverTo", source = "entity.XDeliverTo", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPurchaser", source = "entity.XPurchaser", qualifiedBy = StringToObject.class),
		@Mapping(target = "XVendor", source = "entity.XVendor", qualifiedBy = StringToObject.class)
	})
	com.moneycatcha.app.model.VendorTaxInvoiceType fromVendorTaxInvoiceType(VendorTaxInvoiceType entity);

	
	/**
	 * List<VendorTaxInvoiceType>
	 */
	List<VendorTaxInvoiceType> toVendorTaxInvoiceTypes(List<com.moneycatcha.app.model.VendorTaxInvoiceType> vendorTaxInvoiceType);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.VendorTaxInvoiceType> fromVendorTaxInvoiceTypes(List<VendorTaxInvoiceType> entities);
	
	
	/**
	 * Asset
	 */
	@Mapping(target = "deliveryDate", source = "asset.deliveryDate", qualifiedBy = GregorianToLocal.class)
	Asset toAsset(com.moneycatcha.app.model.VendorTaxInvoiceType.Asset asset);
	
	@InheritInverseConfiguration
	@Mapping(target = "deliveryDate", source = "entity.deliveryDate", qualifiedBy = LocalToGregorian.class)
	com.moneycatcha.app.model.VendorTaxInvoiceType.Asset fromAsset(Asset entity);

	
	/**
	 * List<Asset>
	 */
	List<Asset> toAssets(List<com.moneycatcha.app.model.VendorTaxInvoiceType.Asset> assets);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.VendorTaxInvoiceType.Asset> fromAssets(List<Asset> entities);
	
	
}
