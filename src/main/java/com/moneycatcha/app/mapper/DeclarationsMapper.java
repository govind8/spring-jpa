package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Declarations;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface DeclarationsMapper {


	DeclarationsMapper INSTANCE = Mappers.getMapper(DeclarationsMapper.class);
	
	
	/**
	 * Declarations
	 */
	Declarations toDeclarations(Application.Declarations declarations);
	
	@InheritInverseConfiguration
	Application.Declarations fromDeclarations(Declarations entity);

	
	/**
	 * Declarations.BrokerDeclarations
	 */
	Declarations.BrokerDeclarations toBrokerDeclarations(Application.Declarations.BrokerDeclarations broker);
	
	@InheritInverseConfiguration
	Application.Declarations.BrokerDeclarations fromBrokerDeclarations(Declarations.BrokerDeclarations entity);
	
}
