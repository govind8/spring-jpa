package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.BusinessChannel;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
uses = {
		StringObjectMapper.class
})
public interface ContactPersonMapper {
	
	/**
	 * BusinessChannel - ContactPerson
	 */
	
	Contact.ContactPerson toBusinessChannel_ContactPerson(BusinessChannel.Contact.ContactPerson person);
	
	@InheritInverseConfiguration
	BusinessChannel.Contact.ContactPerson fromBusinessChannel_ContactPerson(Contact.ContactPerson entity);


	
	
	/**
	 * CompanyApplicant - ContactPerson
	 */
	@Mapping(target = "XContactPerson", source = "person.XContactPerson", qualifiedBy = ObjectToString.class)
	Contact.ContactPerson toCompanyApplicant_ContactPerson(CompanyApplicant.Contact.ContactPerson person);
	
	@Mapping(target = "XContactPerson", source = "entity.XContactPerson", qualifiedBy = StringToObject.class)
	CompanyApplicant.Contact.ContactPerson fromCompanyApplicant_ContactPerson(Contact.ContactPerson entity);

}
