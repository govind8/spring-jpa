package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Attachment;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface AttachmentMapper {

	AttachmentMapper INSTANCE = Mappers.getMapper(AttachmentMapper.class);

	/**
	 * Message.Attachment
	 */
	@Mapping(target = "uri", source = "attachment.URI")
	Attachment toAttachment(Message.Attachment attachment);
	
	@InheritInverseConfiguration
	@Mapping(target = "URI", source = "entity.uri")
	Message.Attachment fromAttachment(Attachment entity);
	
	
	/**
	 * List<Message.Attachment>
	 */
	List<Attachment> toAttachments(List<Message.Attachment> attachments);
	
	@InheritInverseConfiguration
	List<Message.Attachment> fromAttachments(List<Attachment> entities);
	

	
	
}
