package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.CustomerTransactionAnalysis;
import com.moneycatcha.app.entity.CustomerTransactionAnalysis.CategorySet;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {StringObjectMapper.class, DateMapper.class})
public interface CustomerTransactionAnalysisMapper {

	/**
	 * CustomerTransactionAnalysis
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "customerTransactionAnalysis.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "customerTransactionAnalysis.endDate", qualifiedBy = GregorianToLocal.class)
	})
	CustomerTransactionAnalysis toCustomerTransactionAnalysis(Application.CustomerTransactionAnalysis customerTransactionAnalysis);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.CustomerTransactionAnalysis fromCustomerTransactionAnalysis(CustomerTransactionAnalysis entity);

	
	/**
	 * List<customerTransactionAnalysis>
	 */
	List<CustomerTransactionAnalysis> toCustomerTransactionAnalysisList(List<Application.CustomerTransactionAnalysis> customerTransactionAnalysisList);
	
	@InheritInverseConfiguration
	List<Application.CustomerTransactionAnalysis> fromCustomerTransactionAnalysisList(List<CustomerTransactionAnalysis> entities);

	
	
	/**
	 * CustomerTransactionAnalysis::CategorySet
	 */
	CategorySet toCategorySet(Application.CustomerTransactionAnalysis.CategorySet categorySet);
	
	@InheritInverseConfiguration
	Application.CustomerTransactionAnalysis.CategorySet fromCategorySet(CategorySet entity);

	
	/**
	 * List<CustomerTransactionAnalysis>::List<CategorySet>
	 */
	List<CategorySet> toCategorySets(List<Application.CustomerTransactionAnalysis.CategorySet> categorySets);
	
	@InheritInverseConfiguration
	List<Application.CustomerTransactionAnalysis.CategorySet> fromCategorySets(List<CategorySet> entities);

	

	/**
	 * CustomerTransactionAnalysis::CategorySet::AggregatedTransactions
	 * 
	 */
	CategorySet.AggregatedTransactions toAggregatedTransactions(Application.CustomerTransactionAnalysis.CategorySet.AggregatedTransactions categorySet);
	
	@InheritInverseConfiguration
	Application.CustomerTransactionAnalysis.CategorySet.AggregatedTransactions fromAggregatedTransactions(CategorySet.AggregatedTransactions entity);

	
	/**
	 * List<CustomerTransactionAnalysis>::List<CategorySet>::List<AggregatedTransactions>
	 */
	List<CategorySet.AggregatedTransactions> toAggregatedTransactionsList(List<Application.CustomerTransactionAnalysis.CategorySet.AggregatedTransactions> categorySets);
	
	@InheritInverseConfiguration
	List<Application.CustomerTransactionAnalysis.CategorySet.AggregatedTransactions> fromAggregatedTransactionsList(List<CategorySet.AggregatedTransactions> entities);

	
	
	/**
	 * CustomerTransactionAnalysis::Result
	 */
	CustomerTransactionAnalysis.Result toResult(Application.CustomerTransactionAnalysis.Result result);
	
	@InheritInverseConfiguration
	Application.CustomerTransactionAnalysis.Result fromResult(CustomerTransactionAnalysis.Result entity);


}
