package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.entity.BusinessIncomePrevious;
import com.moneycatcha.app.entity.BusinessIncomePrior;
import com.moneycatcha.app.entity.BusinessIncomeRecent;
import com.moneycatcha.app.entity.BusinessIncomeYearToDate;
import com.moneycatcha.app.entity.DeclaredIncome;
import com.moneycatcha.app.entity.FinancialAnalysis;
import com.moneycatcha.app.entity.SelfEmployed;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant.Employment;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		AddbackMapper.class,
		DateMapper.class,
		DurationTypeMapper.class,
		FinancialAnalysisMapper.class,
		StringObjectMapper.class
	})
public interface SelfEmployedMapper {


	SelfEmployedMapper INSTANCE = Mappers.getMapper(SelfEmployedMapper.class);

	/**
	 * SelfEmployed
	 */
	@Mappings({
		@Mapping(target = "anzscoOccupationCode", source = "selfemployed.ANZSCOOccupationCode"),
		@Mapping(target = "XAccountant", source = "selfemployed.XAccountant", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XEmployer", source = "selfemployed.XEmployer", qualifiedBy = ObjectToString.class)
	})
	SelfEmployed toEmployment(Employment.SelfEmployed selfemployed);
	
	@Mappings({
		@Mapping(target = "ANZSCOOccupationCode", source = "entity.anzscoOccupationCode"),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class),
		@Mapping(target = "XEmployer", source = "entity.XEmployer", qualifiedBy = StringToObject.class)
	})
	Employment.SelfEmployed fromSelfEmployed(SelfEmployed entity);
	
	

	/**
	 * SelfEmployed.Business
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "business.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "isFranchise", source = "business.isAFranchise")
	})
	Business toBusiness(Employment.SelfEmployed.Business business);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "isAFranchise", source = "entity.isFranchise")
	})
	Employment.SelfEmployed.Business fromBusiness(Business entity);
	
	
	/**
	 * SelfEmployed.BusinessIncomePrevious
	 */
	
	@Mappings({
		@Mapping(target = "endDate", source = "incomePrevious.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "incomePrevious.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrevious.XAccountant", qualifiedBy = ObjectToString.class)
	})
	BusinessIncomePrevious toBusinessIncomePrevious(Employment.SelfEmployed.BusinessIncomePrevious incomePrevious);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)
		})
	Employment.SelfEmployed.BusinessIncomePrevious fromBusinessIncomePrevious(BusinessIncomePrevious entity);
	
	
	/**
	 * SelfEmployed.BusinessIncomePrevious.ForeignSourceIncome
	 */
	@Mapping(target = "audAmount", source = "foreignIncome.AUDAmount")
	BusinessIncomePrevious.ForeignSourcedIncome toBusinessIncomePreviousForeign(Employment.SelfEmployed.BusinessIncomePrevious.ForeignSourcedIncome foreignIncome);
	
	@InheritInverseConfiguration
	@Mapping(target = "AUDAmount", source = "entity.audAmount")
	Employment.SelfEmployed.BusinessIncomePrevious.ForeignSourcedIncome fromBusinessIncomePreviousForeign(BusinessIncomePrevious.ForeignSourcedIncome entity);
	

	/**
	 * SelfEmployed.BusinessIncomePrior
	 */
	
	@Mappings({
		@Mapping(target = "endDate", source = "incomePrior.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "incomePrior.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomePrior.XAccountant", qualifiedBy = ObjectToString.class)
	})
	BusinessIncomePrior toBusinessIncomePrior(Employment.SelfEmployed.BusinessIncomePrior incomePrior);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)
	})
	Employment.SelfEmployed.BusinessIncomePrior fromBusinessIncomePrior(BusinessIncomePrior entity);
	
	
	/**
	 * SelfEmployed.BusinessIncomePrior.ForeignSourceIncome
	 */
	@Mapping(target = "audAmount", source = "foreignIncome.AUDAmount")
	BusinessIncomePrior.ForeignSourcedIncome toBusinessIncomePriorForeign(Employment.SelfEmployed.BusinessIncomePrior.ForeignSourcedIncome foreignIncome);
	
	@InheritInverseConfiguration
	@Mapping(target = "AUDAmount", source = "entity.audAmount")
	Employment.SelfEmployed.BusinessIncomePrior.ForeignSourcedIncome fromBusinessIncomePriorForeign(BusinessIncomePrior.ForeignSourcedIncome entity);
	
	

	
	/**
	 * SelfEmployed.BusinessIncomeRecent
	 */
	
	@Mappings({
		@Mapping(target = "endDate", source = "incomeRecent.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "incomeRecent.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XAccountant", source = "incomeRecent.XAccountant", qualifiedBy = ObjectToString.class)
	})
	BusinessIncomeRecent toBusinessIncomeRecent(Employment.SelfEmployed.BusinessIncomeRecent incomeRecent);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XAccountant", source = "entity.XAccountant", qualifiedBy = StringToObject.class)
	})
	Employment.SelfEmployed.BusinessIncomeRecent fromBusinessIncomeRecent(BusinessIncomeRecent entity);
	
	
	/**
	 * SelfEmployed.BusinessIncomeRecent.ForeignSourceIncome
	 */
	@Mapping(target = "audAmount", source = "foreignIncome.AUDAmount")
	BusinessIncomeRecent.ForeignSourcedIncome toBusinessIncomeRecentForeign(Employment.SelfEmployed.BusinessIncomeRecent.ForeignSourcedIncome foreignIncome);
	
	@InheritInverseConfiguration
	@Mapping(target = "AUDAmount", source = "entity.audAmount")
	Employment.SelfEmployed.BusinessIncomeRecent.ForeignSourcedIncome fromBusinessIncomeRecentForeign(BusinessIncomeRecent.ForeignSourcedIncome entity);
	
	
	
	/**
	 * SelfEmployed.BusinessIncomeYearToDate
	 */
	
	@Mappings({
		@Mapping(target = "endDate", source = "incomeYear.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "incomeYear.startDate", qualifiedBy = GregorianToLocal.class)
	})
	BusinessIncomeYearToDate toBusinessIncomeYearToDate(Employment.SelfEmployed.BusinessIncomeYearToDate incomeYear);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class)
	})
	Employment.SelfEmployed.BusinessIncomeYearToDate fromBusinessIncomeYearToDate(BusinessIncomeYearToDate entity);
	
	
	/**
	 * SelfEmployed.BusinessIncomeRecent.ForeignSourceIncome
	 */
	@Mapping(target = "audAmount", source = "foreignIncome.AUDAmount")
	BusinessIncomeYearToDate.ForeignSourcedIncome toBusinessIncomeYearForeign(Employment.SelfEmployed.BusinessIncomeYearToDate.ForeignSourcedIncome foreignIncome);
	
	@InheritInverseConfiguration
	@Mapping(target = "AUDAmount", source = "entity.audAmount")
	Employment.SelfEmployed.BusinessIncomeYearToDate.ForeignSourcedIncome fromBusinessIncomeYearForeign(BusinessIncomeYearToDate.ForeignSourcedIncome entity);
	
	
	/**
	 * SelfEmployed.DeclaredIncome
	 */
	
	DeclaredIncome toDeclaredIncome(Employment.SelfEmployed.DeclaredIncome incomeYear);
	
	@InheritInverseConfiguration
	Employment.SelfEmployed.DeclaredIncome fromDeclaredIncome(DeclaredIncome entity);

	
	
	/**
	 * SelfEmployed - FinancialAnalysis
	 */
	FinancialAnalysis toFinancialAnalysis(Employment.SelfEmployed.FinancialAnalysis finAnalysis);
	
	@InheritInverseConfiguration
	Employment.SelfEmployed.FinancialAnalysis  fromFinancialAnalysis(FinancialAnalysis entity);


	/**
	 * SelfEmployed.FinancialAnalysis.CompanyFinanicals
	 */
	@Mapping(target = "XCompanyFinancials", source = "compfinancials.XCompanyFinancials", qualifiedBy = ObjectToString.class)
	FinancialAnalysis.CompanyFinancials toCompanyFinancials(Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials compfinancials);
	
	@Mapping(target = "XCompanyFinancials", source = "entity.XCompanyFinancials", qualifiedBy = StringToObject.class)
	Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials fromCompanyFinancials(FinancialAnalysis.CompanyFinancials entity);

	
	
	/**
	 *  SelfEmployed.FinancialAnalysis.CompanyFinanicals - List
	 */
	
	List<FinancialAnalysis.CompanyFinancials> toCompanyFinancialsList(List<Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials> compfinList);
	
	@InheritInverseConfiguration
	List<Employment.SelfEmployed.FinancialAnalysis.CompanyFinancials> fromCompanyFinancialsList(List<FinancialAnalysis.CompanyFinancials> entityList);

	
	
	
}
