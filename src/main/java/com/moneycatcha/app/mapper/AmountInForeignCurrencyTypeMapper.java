package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AmountInForeignCurrencyType;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {DateMapper.class})
public interface AmountInForeignCurrencyTypeMapper {
	
	AmountInForeignCurrencyTypeMapper INSTANCE = Mappers.getMapper(AmountInForeignCurrencyTypeMapper.class);

	@Mapping(target = "exchangeDateTime", source = "currency.exchangeDateTime", qualifiedBy = GregorianToLocalDateTime.class)
	AmountInForeignCurrencyType toNewLimitRequestedInForeignCurrency(com.moneycatcha.app.model.AmountInForeignCurrencyType currency);
	
	@InheritInverseConfiguration
	@Mapping(target = "exchangeDateTime", source = "entity.exchangeDateTime", qualifiedBy = LocalDateTimeToGregorian.class)
	com.moneycatcha.app.model.AmountInForeignCurrencyType fromNewLimitRequestedInForeignCurrency(AmountInForeignCurrencyType entity);
}
