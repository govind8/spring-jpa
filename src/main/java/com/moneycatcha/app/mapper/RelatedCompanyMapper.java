package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Contact;
import com.moneycatcha.app.entity.Partner;
import com.moneycatcha.app.entity.RelatedCompany;
import com.moneycatcha.app.entity.Contact.ContactPerson;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		PhoneTypeMapper.class,
		StringObjectMapper.class
	})
public interface RelatedCompanyMapper {

	RelatedCompanyMapper INSTANCE = Mappers.getMapper(RelatedCompanyMapper.class);

	/**
	 * RelatedCompany
	 */
	@Mappings({
		@Mapping(target = "abn", source = "relatedCompany.ABN"),
		@Mapping(target = "abnVerified", source = "relatedCompany.ABNVerified"),
		@Mapping(target = "acn", source = "relatedCompany.ACN"),
		@Mapping(target = "dateRegistered", source = "relatedCompany.dateRegistered", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "gstRegisteredDate", source = "relatedCompany.GSTRegisteredDate", qualifiedBy = GregorianToLocal.class)
	})
	RelatedCompany toRelatedCompany(Application.RelatedCompany relatedCompany);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "ABN", source = "entity.abn"),
		@Mapping(target = "ABNVerified", source = "entity.abnVerified"),
		@Mapping(target = "ACN", source = "entity.acn"),
		@Mapping(target = "dateRegistered", source = "entity.dateRegistered", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "GSTRegisteredDate", source = "entity.gstRegisteredDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.RelatedCompany fromRelatedCompany(RelatedCompany entity); 

	
	/**
	 * List<RelatedCompany>
	 */
	List<RelatedCompany> toRelatedCompanies(List<Application.RelatedCompany> relatedCompanies);
	
	@InheritInverseConfiguration
	List<Application.RelatedCompany> fromRelatedCompanies(List<RelatedCompany> entities);


	/**
	 * Contact
	 */
	@Mapping(target = "XAddress", source = "contact.XAddress", qualifiedBy = ObjectToString.class)
	Contact toContact(Application.RelatedCompany.Contact contact);
	
	@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class)
	Application.RelatedCompany.Contact fromContact(Contact entity);


	/**
	 * ContactPerson
	 */
	@Mapping(target = "XContactPerson", source = "contactPerson.XContactPerson", qualifiedBy = ObjectToString.class)
	ContactPerson toContactPerson(Application.RelatedCompany.Contact.ContactPerson contactPerson);
	
	@Mapping(target = "XContactPerson", source = "entity.XContactPerson", qualifiedBy = StringToObject.class)
	Application.RelatedCompany.Contact.ContactPerson fromContactPerson(ContactPerson entity);


	/**
	 * Partner
	 */
	@Mapping(target = "XPartner", source = "partner.XPartner", qualifiedBy = ObjectToString.class)
	Partner toPartner(Application.RelatedCompany.Partner partner);
	
	@Mapping(target = "XPartner", source = "entity.XPartner", qualifiedBy = StringToObject.class)
	Application.RelatedCompany.Partner fromPartner(Partner entity);


	
	/**
	 * List<Partner>
	 */
	List<Partner> toPartners(List<Application.RelatedCompany.Partner> partners);
	
	@InheritInverseConfiguration
	List<Application.RelatedCompany.Partner> fromPartners(List<Partner> entities);

	
	
}
