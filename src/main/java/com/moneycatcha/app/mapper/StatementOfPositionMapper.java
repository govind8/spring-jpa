package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.entity.StatementOfPosition;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = { 
		StringObjectMapper.class 
	}
)
public interface StatementOfPositionMapper {

	StatementOfPositionMapper INSTANCE = Mappers.getMapper(StatementOfPositionMapper.class);

	
	/**
	 * StatementOfPosition
	 */
	StatementOfPosition toStatementOfPosition(Content.StatementOfPosition sop);
	
	@InheritInverseConfiguration
	Content.StatementOfPosition fromStatementOfPosition(StatementOfPosition entity);
	
	
	/**
	 * List<StatementOfPosition>
	 */
	List<StatementOfPosition> toStatementOfPositions(List<Content.StatementOfPosition> sops);
	
	@InheritInverseConfiguration
	List<Content.StatementOfPosition> fromStatementOfPositions(List<StatementOfPosition> entities);
	

	
	/**
	 * Applicant
	 */
	@Mapping(target = "XApplicant", source = "applicant.XApplicant", qualifiedBy = ObjectToString.class)
	Applicant toApplicant(Content.StatementOfPosition.Applicant applicant);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	Content.StatementOfPosition.Applicant fromApplicant(Applicant entity);

	
	/**
	 * List<Applicant>
	 */
	List<Applicant> toApplicants(List<Content.StatementOfPosition.Applicant> applicants);
	
	@InheritInverseConfiguration
	List<Content.StatementOfPosition.Applicant> fromApplicants(List<Applicant> entities);
	
	
	
}
