package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AssociatedLoanAccount;
import com.moneycatcha.app.entity.Guarantor;
import com.moneycatcha.app.entity.Interview;
import com.moneycatcha.app.entity.LendingGuarantee;
import com.moneycatcha.app.entity.Security;
import com.moneycatcha.app.entity.Guarantor.LoanWriterConfirmations;
import com.moneycatcha.app.entity.Guarantor.SecurityGuarantee;
import com.moneycatcha.app.entity.Guarantor.ServicingGuarantee;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		StringObjectMapper.class,
		DateMapper.class,
		SecurityAgreementTypeMapper.class
		})
public interface LendingGuaranteeMapper {

	LendingGuaranteeMapper INSTANCE = Mappers.getMapper(LendingGuaranteeMapper.class);

	
	/**
	 * LendingGuarantee
	 */
	LendingGuarantee toLendingGuarantee(Application.LendingGuarantee lendingGuarantee);
	
	@InheritInverseConfiguration
	Application.LendingGuarantee fromLendingGuarantee(LendingGuarantee entity);

	
	/**
	 * List<LendingGuarantee>
	 */
	List<LendingGuarantee> toLendingGuarantees(List<Application.LendingGuarantee> lendingGuarantees);
	
	@InheritInverseConfiguration
	List<Application.LendingGuarantee> fromLendingGuarantees(List<LendingGuarantee> entities);
	
	
	
	
	/**
	 * LendingGuarantee::AssociatedLoanAccount
	 */
	@Mapping(target = "XAssociatedLoanAccount", source = "associated.XAssociatedLoanAccount", qualifiedBy = ObjectToString.class)
	AssociatedLoanAccount toAssociatedLoanAccount(Application.LendingGuarantee.AssociatedLoanAccount associated);
	
	@Mapping(target = "XAssociatedLoanAccount", source = "entity.XAssociatedLoanAccount", qualifiedBy = StringToObject.class)
	Application.LendingGuarantee.AssociatedLoanAccount fromAssociatedLoanAccount(AssociatedLoanAccount entity);

	
	/**
	 * List<LendingGuarantee>::List<AssociatedLoanAccount>
	 */
	List<AssociatedLoanAccount> toAssociatedLoanAccounts(List<Application.LendingGuarantee.AssociatedLoanAccount> accounts);
	
	@InheritInverseConfiguration
	List<Application.LendingGuarantee.AssociatedLoanAccount> fromAssociatedLoanAccounts(List<AssociatedLoanAccount> entities);	
	
	
	/**
	 * LendingGuarantee::Guarantor
	 */
	@Mapping(target = "XGuarantor", source = "guarantor.XGuarantor", qualifiedBy = ObjectToString.class)
	Guarantor toGuarantor(Application.LendingGuarantee.Guarantor guarantor);
	
	@Mapping(target = "XGuarantor", source = "entity.XGuarantor", qualifiedBy = StringToObject.class)
	Application.LendingGuarantee.Guarantor fromGuarantor(Guarantor entity);

	
	/**
	 * List<LendingGuarantee>::List<Guarantor>
	 */
	List<Guarantor> toGuarantors(List<Application.LendingGuarantee.Guarantor> guarantors);
	
	@InheritInverseConfiguration
	List<Application.LendingGuarantee.Guarantor> fromGuarantors(List<Guarantor> entities);	
	
	
	
	/**
	 * LendingGuarantee::Guarantor::Interview
	 */
	@Mapping(target = "date", source = "interview.date", qualifiedBy = GregorianToLocal.class)
	Interview toInterview(Application.LendingGuarantee.Guarantor.Interview interview);
	
	@Mapping(target = "date", source = "entity.date", qualifiedBy = LocalToGregorian.class)
	Application.LendingGuarantee.Guarantor.Interview fromInterview(Interview entity);

	
	
	/**
	 * LendingGuarantee::Guarantor::LoanWriterConfirmations
	 */
	LoanWriterConfirmations toLoanWriterConfirmations(Application.LendingGuarantee.Guarantor.LoanWriterConfirmations loanWriter);
	
	@InheritInverseConfiguration
	Application.LendingGuarantee.Guarantor.LoanWriterConfirmations fromLoanWriterConfirmations(LoanWriterConfirmations entity);



	
	/**
	 * LendingGuarantee::Guarantor::SecurityGuarantee
	 */
	SecurityGuarantee toSecurityGuarantee(Application.LendingGuarantee.Guarantor.SecurityGuarantee security);
	
	@InheritInverseConfiguration
	Application.LendingGuarantee.Guarantor.SecurityGuarantee fromSecurityGuarantee(SecurityGuarantee entity);

	
	/**
	 * LendingGuarantee::Guarantor::ServicingGuarantee
	 */
	ServicingGuarantee toSecurityGuarantee(Application.LendingGuarantee.Guarantor.ServicingGuarantee servicing);
	
	@InheritInverseConfiguration
	Application.LendingGuarantee.Guarantor.ServicingGuarantee fromServicingGuarantee(ServicingGuarantee entity);

	
	
	/**
	 * LendingGuarantee::Security
	 */
	@Mapping(target = "XSecurity", source = "security.XSecurity", qualifiedBy = ObjectToString.class)
	Security toSecurity(Application.LendingGuarantee.Security security);
	
	@Mapping(target = "XSecurity", source = "entity.XSecurity", qualifiedBy = StringToObject.class)
	Application.LendingGuarantee.Security fromSecurity(Security entity);

	
	/**
	 * List<LendingGuarantee>::List<Security>
	 */
	List<Security> toSecurities(List<Application.LendingGuarantee.Security> securities);
	
	@InheritInverseConfiguration
	List<Application.LendingGuarantee.Security> fromSecurities(List<Security> entities);	
		
	
	
		
}
