package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.ForeignTaxAssociationType;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface ForeignTaxAssociationTypeMapper {

	/**
	 * ForeignTaxAssociation
	 */
	ForeignTaxAssociationType toForeignTaxAssociation(com.moneycatcha.app.model.ForeignTaxAssociationType tax);
	
	@InheritInverseConfiguration
	com.moneycatcha.app.model.ForeignTaxAssociationType fromForeignTaxAssociation(ForeignTaxAssociationType entity);


	
	/**
	 * ForeignTaxAssociationType.Detail
	 */
	@Mappings({
		@Mapping(target = "tinNotProvidedReason", source = "detail.TINNotProvidedReason"),
		@Mapping(target = "tinNotProvidedReasonDetail", source = "detail.TINNotProvidedReasonDetail")
	})
	ForeignTaxAssociationType.Detail toDetail(com.moneycatcha.app.model.ForeignTaxAssociationType.Detail detail);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "TINNotProvidedReason", source = "entity.tinNotProvidedReason"),
		@Mapping(target = "TINNotProvidedReasonDetail", source = "entity.tinNotProvidedReasonDetail")
	})
	com.moneycatcha.app.model.ForeignTaxAssociationType.Detail fromDetail(ForeignTaxAssociationType.Detail entity);

	
	/**
	 *  FinancialAnalysis.CompanyFinanicals - List
	 */
	
	List<ForeignTaxAssociationType.Detail> toDetails(List<com.moneycatcha.app.model.ForeignTaxAssociationType.Detail> detailList);
	
	@InheritInverseConfiguration
	List<com.moneycatcha.app.model.ForeignTaxAssociationType.Detail> fromDetails(List<ForeignTaxAssociationType.Detail> entityList);
}
