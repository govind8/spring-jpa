package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.RelatedLegalEntities;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface RelatedLegalEntitiesMapper {

	
	/**
	 * RelatedLegalEntities 
	 */
	RelatedLegalEntities toRelatedLegalEntities(Application.CompanyApplicant.RelatedLegalEntities legal);
	
	@InheritInverseConfiguration
	Application.CompanyApplicant.RelatedLegalEntities fromRelatedLegalEntities(RelatedLegalEntities entity);


}
