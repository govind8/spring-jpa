package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.ConstructionDetails;
import com.moneycatcha.app.entity.ContractDetails;
import com.moneycatcha.app.entity.Encumbrance;
import com.moneycatcha.app.entity.EstimatedValue;
import com.moneycatcha.app.entity.FutureRentalIncome;
import com.moneycatcha.app.entity.PropertyFeatures;
import com.moneycatcha.app.entity.PropertyPart;
import com.moneycatcha.app.entity.RealEstateAsset;
import com.moneycatcha.app.entity.RentalIncome;
import com.moneycatcha.app.entity.Title;
import com.moneycatcha.app.entity.PersonApplicant.Insurance;
import com.moneycatcha.app.entity.RealEstateAsset.Commercial;
import com.moneycatcha.app.entity.RealEstateAsset.HeritageListing;
import com.moneycatcha.app.entity.RealEstateAsset.Industrial;
import com.moneycatcha.app.entity.RealEstateAsset.PropertyType;
import com.moneycatcha.app.entity.RealEstateAsset.Representation;
import com.moneycatcha.app.entity.RealEstateAsset.Residential;
import com.moneycatcha.app.entity.RealEstateAsset.RestrictionOnUseOfLand;
import com.moneycatcha.app.entity.RealEstateAsset.Rural;
import com.moneycatcha.app.entity.RealEstateAsset.VisitContact;
import com.moneycatcha.app.entity.RealEstateAsset.Zoning;
import com.moneycatcha.app.entity.Title.RegisteredProprietor;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		FinancialAnalysisMapper.class,
		FinancialAccountTypeMapper.class,
		FundsDisbursementTypeMapper.class,
		PercentOwnedTypeMapper.class,
		SecurityAgreementTypeMapper.class,
		StringObjectMapper.class
	})
public interface RealEstateAssetMapper {

	RealEstateAssetMapper INSTANCE = Mappers.getMapper(RealEstateAssetMapper.class);

	/**
	 * RealEstateAsset
	 */
	@Mappings({
		@Mapping(target = "nrasConsortium", source = "ra.NRASConsortium"),
		@Mapping(target = "nrasProperty", source = "ra.NRASProperty"),
		@Mapping(target = "XAddress", source = "ra.XAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XPropertyAgent", source = "ra.XPropertyAgent", qualifiedBy = ObjectToString.class)
	})
	RealEstateAsset toRealEstateAsset(Application.RealEstateAsset ra);
	
	@Mappings({
		@Mapping(target = "NRASConsortium", source = "entity.nrasConsortium"),
		@Mapping(target = "NRASProperty", source = "entity.nrasProperty"),
		@Mapping(target = "XAddress", source = "entity.XAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "XPropertyAgent", source = "entity.XPropertyAgent", qualifiedBy = StringToObject.class)
	})
	Application.RealEstateAsset fromRealEstateAsset(RealEstateAsset entity); 

	
	/**
	 * List<RealEstateAsset>
	 */
	List<RealEstateAsset> toRealEstateAssets(List<Application.RealEstateAsset> nras);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset> fromRealEstateAssets(List<RealEstateAsset> entities);


	/**
	 * Commercial
	 */
	Commercial toCommercial(Application.RealEstateAsset.Commercial commercial);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Commercial fromCommercial(Commercial entity); 


	
	/**
	 * ConstructionDetails
	 */
	@Mappings({
		@Mapping(target = "XBuilder", source = "construction.XBuilder", qualifiedBy = ObjectToString.class),
		@Mapping(target = "estimatedCompletionDate", source="construction.estimatedCompletionDate", qualifiedBy = GregorianToLocal.class)
	})
	ConstructionDetails toConstructionDetails(Application.RealEstateAsset.ConstructionDetails construction);
	
	@Mappings({
		@Mapping(target = "XBuilder", source = "entity.XBuilder", qualifiedBy = StringToObject.class),
		@Mapping(target = "estimatedCompletionDate", source="entity.estimatedCompletionDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.RealEstateAsset.ConstructionDetails fromConstructionDetails(ConstructionDetails entity); 
	
	
	
	/**
	 * ConstructionDetails.ConstructionStage
	 */
	ConstructionDetails.ConstructionStage toConstructionStage(Application.RealEstateAsset.ConstructionDetails.ConstructionStage stage);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.ConstructionDetails.ConstructionStage fromConstructionStage(ConstructionDetails.ConstructionStage entity); 

	
	/**
	 * List<ConstructionStage>
	 */
	List<ConstructionDetails.ConstructionStage> toConstructionStages(List<Application.RealEstateAsset.ConstructionDetails.ConstructionStage> stages);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.ConstructionDetails.ConstructionStage> fromConstructionStages(List<ConstructionDetails.ConstructionStage> entities); 

	
	/**
	 * OwnerBuilderExperience
	 */
	ConstructionDetails.OwnerBuilderExperience toOwnerBuilderExperience(Application.RealEstateAsset.ConstructionDetails.OwnerBuilderExperience owner);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.ConstructionDetails.OwnerBuilderExperience fromOwnerBuilderExperience(ConstructionDetails.OwnerBuilderExperience entity); 

	
	
	/**
	 * ContractDetails
	 */
	@Mappings({
		@Mapping(target = "contractDate", source = "contractDetails.contractDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "estimatedSettlementDate", source = "contractDetails.estimatedSettlementDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "financeApprovalDate", source = "contractDetails.financeApprovalDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XVendor", source = "contractDetails.XVendor", qualifiedBy = ObjectToString.class)
	})
	ContractDetails toContractDetails(Application.RealEstateAsset.ContractDetails contractDetails);
	
	@Mappings({
		@Mapping(target = "contractDate", source = "entity.contractDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "estimatedSettlementDate", source = "entity.estimatedSettlementDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "financeApprovalDate", source = "entity.financeApprovalDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XVendor", source = "entity.XVendor", qualifiedBy = StringToObject.class)
	})
	Application.RealEstateAsset.ContractDetails fromContractDetails(ContractDetails entity); 
	
	
	
	/**
	 * Encumbrance
	 */
	@Mappings({
		@Mapping(target = "registrationDate", source = "encumbrance.registrationDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "realEncumbranceType", source = "encumbrance.encumbranceType"),
		@Mapping(target = "securityPriority", source = "encumbrance.priority")
	})
	Encumbrance toEncumbrance(Application.RealEstateAsset.Encumbrance encumbrance);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "registrationDate", source = "entity.registrationDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "encumbranceType", source = "entity.realEncumbranceType"),
		@Mapping(target = "priority", source = "entity.securityPriority")
	})
	Application.RealEstateAsset.Encumbrance fromEncumbrance(Encumbrance entity); 
	

	/**
	 * List<Encumbrance>
	 */
	List<Encumbrance> toEncumbrances(List<Application.RealEstateAsset.Encumbrance> encumbrances);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.Encumbrance> fromEncumbrances(List<Encumbrance> entities); 
	
	
	/**
	 * InFavourOf
	 */
	@Mapping(target = "XInFavourOf", source = "infavour.XInFavourOf", qualifiedBy = ObjectToString.class)
	Encumbrance.InFavourOf toInFavour(Application.RealEstateAsset.Encumbrance.InFavourOf infavour);
	
	@Mapping(target = "XInFavourOf", source = "entity.XInFavourOf", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.Encumbrance.InFavourOf fromInFavour(Encumbrance.InFavourOf entity); 
	

	/**
	 * List<Encumbrance.InFavourOf>
	 */
	List<Encumbrance.InFavourOf> toInFavourOfs(List<Application.RealEstateAsset.Encumbrance.InFavourOf> infavourofs);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.Encumbrance.InFavourOf> fromInFavourOfs(List<Encumbrance.InFavourOf> entities); 
	
	
	/**
	 * EstimatedValue
	 */
	@Mapping(target = "XValuer", source = "estimatedValue.XValuer", qualifiedBy = ObjectToString.class)
	EstimatedValue toInFavour(Application.RealEstateAsset.EstimatedValue estimatedValue);
	
	@Mapping(target = "XValuer", source = "entity.XValuer", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.EstimatedValue fromInFavour(EstimatedValue entity); 	
	

	
	/**
	 * FutureRentalIncome
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "futureRental.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XOwner", source = "futureRental.XOwner", qualifiedBy = ObjectToString.class)
	})
	FutureRentalIncome toFutureRentalIncome(Application.RealEstateAsset.FutureRentalIncome futureRental);
	
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XOwner", source = "entity.XOwner", qualifiedBy = StringToObject.class)
	})
	Application.RealEstateAsset.FutureRentalIncome fromFutureRentalIncome(FutureRentalIncome entity); 
	
	
	/**
	 * List<FutureRentalIncome>
	 */
	List<FutureRentalIncome> toFutureRentalIncomes(List<Application.RealEstateAsset.FutureRentalIncome> futureIncomes);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.FutureRentalIncome> fromFutureRentalIncomes(List<FutureRentalIncome> entities); 

	
	/**
	 * FutureRentalIncome.PropertyPart
	 */
	PropertyPart toPropertyPart(Application.RealEstateAsset.FutureRentalIncome.PropertyPart propertyPart);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.FutureRentalIncome.PropertyPart fromPropertyPart(PropertyPart entity); 
	
	
	/**
	 * List<FutureRentalIncome.PropertyPart>
	 */
	List<PropertyPart> toPropertyParts(List<Application.RealEstateAsset.FutureRentalIncome.PropertyPart> propertyParts);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.FutureRentalIncome.PropertyPart> fromPropertyParts(List<PropertyPart> entities); 
	
	
	
	/**
	 * HeritageListing
	 */
	HeritageListing toHeritageListing(Application.RealEstateAsset.HeritageListing heritageListing);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.HeritageListing fromHeritageListing(HeritageListing entity); 
	
	
	/**
	 * Industrial
	 */
	Industrial toIndustrial(Application.RealEstateAsset.Industrial industrial);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Industrial fromIndustrial(Industrial entity); 
	
	
	/**
	 * Insurance
	 */
	@Mapping(target = "XInsurance", source = "insurance.XInsurance", qualifiedBy = ObjectToString.class)
	RealEstateAsset.Insurance toInsurance(Application.RealEstateAsset.Insurance insurance);
	
	@Mapping(target = "XInsurance", source = "entity.XInsurance", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.Insurance fromInsurance(Insurance entity); 
	
	
	/**
	 * List<Insurance>
	 */
	List<RealEstateAsset.Insurance> toInsuranceList(List<Application.RealEstateAsset.Insurance> insuranceList);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.Insurance> fromInsuranceList(List<RealEstateAsset.Insurance> entities); 
	

	/**
	 * PropertyExpense
	 */
	RealEstateAsset.PropertyExpense toPropertyExpense(Application.RealEstateAsset.PropertyExpense propertyExpense);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.PropertyExpense fromPropertyExpense(RealEstateAsset.PropertyExpense entity); 
	
	/**
	 * PropertyFeatures
	 */
	PropertyFeatures toPropertyFeatures(Application.RealEstateAsset.PropertyFeatures propertyFeatures);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.PropertyFeatures fromPropertyFeatures(PropertyFeatures entity); 
	
	
	/**
	 * List<PropertyFeatures>
	 */
	List<PropertyFeatures> toPropertyFeaturesList(List<Application.RealEstateAsset.PropertyFeatures> propertyFeaturesList);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.PropertyFeatures> fromPropertyFeaturesList(List<PropertyFeatures> entities); 
	
	
	/**
	 * PropertyType
	 */
	PropertyType toPropertyType(Application.RealEstateAsset.PropertyType propertyType);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.PropertyType fromPropertyType(PropertyType entity); 
	
	
	/**
	 * RentalIncome
	 */
	
	@Mapping(target = "XOwner", source = "rentalIncome.XOwner", qualifiedBy = ObjectToString.class)
	RentalIncome toRentalIncome(Application.RealEstateAsset.RentalIncome rentalIncome);
	
	@Mapping(target = "XOwner", source = "entity.XOwner", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.RentalIncome fromRentalIncome(RentalIncome entity); 

	/**
	 * RentalIncome
	 */
	List<RentalIncome> toRentalIncomes(List<Application.RealEstateAsset.RentalIncome> rentalIncomes);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.RentalIncome> fromRentalIncome(List<RentalIncome> entities); 
	
	/**
	 * RentalIncome.PropertyPart
	 */
	PropertyPart toRIPropertyPart(Application.RealEstateAsset.RentalIncome.PropertyPart propertyPart);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.RentalIncome.PropertyPart fromRIPropertyPart(PropertyPart entity); 
	
	
	/**
	 * List<RentalIncome.PropertyPart>
	 */
	List<PropertyPart> toRIPropertyParts(List<Application.RealEstateAsset.RentalIncome.PropertyPart> propertyParts);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.RentalIncome.PropertyPart> fromRIPropertyParts(List<PropertyPart> entities); 
	
	
	/**
	 * Representation
	 */
	@Mappings({
		@Mapping(target = "XConveyancer", source = "representation.XConveyancer", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XVendorConveyancer", source = "representation.XVendorConveyancer", qualifiedBy = ObjectToString.class)
	})
	Representation toRepresentation(Application.RealEstateAsset.Representation representation);
	
	@Mappings({
		@Mapping(target = "XConveyancer", source = "entity.XConveyancer", qualifiedBy = StringToObject.class),
		@Mapping(target = "XVendorConveyancer", source = "entity.XVendorConveyancer", qualifiedBy = StringToObject.class)
	})
	Application.RealEstateAsset.Representation fromRepresentation(Representation entity); 
	
	
	/**
	 * Residential
	 */
	Residential toResidential(Application.RealEstateAsset.Residential residential);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Residential fromResidential(Residential entity); 
	
	
	/**
	 * RestrictionOnUseOfLand
	 */
	RestrictionOnUseOfLand toRestrictionOnUseLand(Application.RealEstateAsset.RestrictionOnUseOfLand restrictionOnUseLand);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.RestrictionOnUseOfLand fromRestrictionOnUseLand(RestrictionOnUseOfLand entity); 
	
	/**
	 * Rural
	 */
	Rural toRural(Application.RealEstateAsset.Rural rural);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Rural fromRural(Rural entity); 
	

	
	/**
	 * Title
	 */
	Title toTitle(Application.RealEstateAsset.Title title);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Title fromTitle(Title entity); 

	
	/**
	 * List<Title>
	 */
	List<Title> toTitles(List<Application.RealEstateAsset.Title> titles);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.Title> fromTitles(List<Title> entities); 
	

	/**
	 * RegisteredProprietor
	 */
	@Mapping(target = "XRegisteredProprietor", source = "registeredPropietor.XRegisteredProprietor", qualifiedBy = ObjectToString.class)
	RegisteredProprietor toRegisteredPropietor(Application.RealEstateAsset.Title.RegisteredProprietor registeredPropietor);
	
	@Mapping(target = "XRegisteredProprietor", source = "entity.XRegisteredProprietor", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.Title.RegisteredProprietor fromRegisteredPropietor(RegisteredProprietor entity); 


	/**
	 * List<Title.RegisteredPropietor>
	 */
	List<RegisteredProprietor> toRegisteredProprietors(List<Application.RealEstateAsset.Title.RegisteredProprietor> registeredProprietors);
	
	@InheritInverseConfiguration
	List<Application.RealEstateAsset.Title.RegisteredProprietor> fromRegisteredProprietors(List<RegisteredProprietor> entities); 
	
	
	
	/**
	 * VisitContact
	 */
	@Mapping(target = "XContactParty", source = "visitcontact.XContactParty", qualifiedBy = ObjectToString.class)
	VisitContact toVisitContact(Application.RealEstateAsset.VisitContact visitcontact);
	
	@Mapping(target = "XContactParty", source = "entity.XContactParty", qualifiedBy = StringToObject.class)
	Application.RealEstateAsset.VisitContact fromVisitContact(VisitContact entity); 
	
	
	/**
	 * Zoning
	 */
	Zoning toZoning(Application.RealEstateAsset.Zoning zoning);
	
	@InheritInverseConfiguration
	Application.RealEstateAsset.Zoning fromZoning(Zoning entity); 
	
	
}
