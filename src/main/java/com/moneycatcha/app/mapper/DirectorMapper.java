package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Director;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {StringObjectMapper.class}
)
public interface DirectorMapper {

	
	/**
	 * Director 
	 */
	@Mapping(target = "XDirector", source = "director.XDirector", qualifiedBy = ObjectToString.class)
	Director toDirector(Application.CompanyApplicant.Director director);
	
	@Mapping(target = "XDirector", source = "entity.XDirector", qualifiedBy = StringToObject.class)
	Application.CompanyApplicant.Director fromDirector(Director entity);
	
	
	
	/**
	 * Directors - List
	 */
	List<Director> toDirectors(List<Application.CompanyApplicant.Director> directors);
	
	@InheritInverseConfiguration
	List<Application.CompanyApplicant.Director> fromDirectors(List<Director> entity);
	

}
