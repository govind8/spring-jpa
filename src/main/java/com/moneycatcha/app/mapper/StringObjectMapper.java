package com.moneycatcha.app.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import com.moneycatcha.app.entity.Company;
import com.moneycatcha.app.entity.CompanyApplicant;
import com.moneycatcha.app.entity.CompanyFinancials;
import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.CustomerTransactionAnalysis;
import com.moneycatcha.app.entity.DepositAccount;
import com.moneycatcha.app.entity.Household;
import com.moneycatcha.app.entity.Insurance;
import com.moneycatcha.app.entity.LendingGuarantee;
import com.moneycatcha.app.entity.Liability;
import com.moneycatcha.app.entity.LoanDetails;
import com.moneycatcha.app.entity.MasterAgreement;
import com.moneycatcha.app.entity.NonRealEstateAsset;
import com.moneycatcha.app.entity.PersonApplicant;
import com.moneycatcha.app.entity.ProposedRepayment;
import com.moneycatcha.app.entity.RateComposition;
import com.moneycatcha.app.entity.RealEstateAsset;
import com.moneycatcha.app.entity.RelatedCompany;
import com.moneycatcha.app.entity.RelatedPerson;
import com.moneycatcha.app.entity.TrustApplicant;
import com.moneycatcha.app.entity.TransformMetadata.Identifier;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.AddressType;
import com.moneycatcha.app.model.VendorTaxInvoiceType;
import com.moneycatcha.app.model.Message.Attachment;
import com.moneycatcha.app.model.Message.TransformMetadata;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.Message.Content.NeedsAnalysis;
import com.moneycatcha.app.repository.AddressTypeRepository;
import com.moneycatcha.app.repository.AttachmentRepository;
import com.moneycatcha.app.repository.CompanyApplicantRepository;
import com.moneycatcha.app.repository.CompanyFinancialsRepository;
import com.moneycatcha.app.repository.CompanyRepository;
import com.moneycatcha.app.repository.ContinuingRepaymentRepository;
import com.moneycatcha.app.repository.CreditCardRepository;
import com.moneycatcha.app.repository.CustomerTransactionAnalysisRepository;
import com.moneycatcha.app.repository.DepositAccountRepository;
import com.moneycatcha.app.repository.HouseholdRepository;
import com.moneycatcha.app.repository.IdentifierRepository;
import com.moneycatcha.app.repository.InsuranceRepository;
import com.moneycatcha.app.repository.LendingGuaranteeRepository;
import com.moneycatcha.app.repository.LiabilityRepository;
import com.moneycatcha.app.repository.LoanDetailsRepository;
import com.moneycatcha.app.repository.MasterAgreementRepository;
import com.moneycatcha.app.repository.NeedsAnalysisRepository;
import com.moneycatcha.app.repository.NonRealEstateAssetRepository;
import com.moneycatcha.app.repository.PaymentRepository;
import com.moneycatcha.app.repository.PersonApplicantRepository;
import com.moneycatcha.app.repository.RateCompositionRepository;
import com.moneycatcha.app.repository.RealEstateAssetRepository;
import com.moneycatcha.app.repository.RegularRepaymentRepository;
import com.moneycatcha.app.repository.RelatedCompanyRepository;
import com.moneycatcha.app.repository.RelatedPersonRepository;
import com.moneycatcha.app.repository.TrustApplicantRepository;
import com.moneycatcha.app.repository.VendorTaxInvoiceTypeRepository;

@Component
public class StringObjectMapper {


	private RelatedPersonRepository relatedPersonRepository;
	
	private AddressTypeRepository addressTypeRepository;
	
	private CompanyApplicantRepository companyApplicantRepository;

	private PersonApplicantRepository personApplicantRepository;

	private TrustApplicantRepository trustApplicantRepository;

	private RelatedCompanyRepository relatedCompanyRepository;

	private RealEstateAssetRepository realEstateAssetRepository;

	private NonRealEstateAssetRepository nonRealEstateAssetRepository;

	private LiabilityRepository liabilityRepository;

	private LoanDetailsRepository loanDetailsRepository;

	private CompanyFinancialsRepository companyFinancialsRepository;

	private MasterAgreementRepository masterAgreementRepository;

	private InsuranceRepository insuranceRepository;

	private LendingGuaranteeRepository lendingGuaranteeRepository;

	private CompanyRepository companyRepository;

	private ContinuingRepaymentRepository continuingRepaymentRepository;

	private PaymentRepository paymentRepository;

	private RegularRepaymentRepository regularRepaymentRepository;

	private RateCompositionRepository rateCompositionRepository;

	private AttachmentRepository attachmentRepository;

	private CreditCardRepository creditCardRepository;

	private DepositAccountRepository depositAccountRepository;

	private CustomerTransactionAnalysisRepository customerTransactionAnalysisRepository;

	private IdentifierRepository identifierRepository;

	private VendorTaxInvoiceTypeRepository vendorTaxInvoiceTypeRepository;

	private NeedsAnalysisRepository needsAnalysisRepository;

	private HouseholdRepository householdRepository;
	
	@Autowired
	RelatedPersonMapper relatedPersonImpl;

	@Autowired
	CompanyApplicantMapper companyApplicantImpl;
	
	@Autowired
	PersonApplicantMapper personApplicantImpl;
	
	@Autowired
	AddressTypeMapper addressTypeImpl;
	
	@Autowired
	TrustApplicantMapper trustApplicantImpl;
	
	@Autowired
	RelatedCompanyMapper relatedCompanyImpl;
	
	@Autowired
	RealEstateAssetMapper realEstateAssetImpl;
	
	@Autowired
	NonRealEstateAssetMapper nonRealEstateAssetImpl;
	
	@Autowired
	LiabilityMapper liabilityImpl;
	
	@Autowired
	LoanDetailsMapper loanDetailsImpl;
	
	@Autowired
	CompanyFinancialsMapper companyFinancialsImpl;
	
	@Autowired
	MasterAgreementMapper masterAgreementImpl;
	
	@Autowired
	InsuranceMapper insuranceImpl;
	
	@Autowired
	LendingGuaranteeMapper lendingGuaranteeImpl;
	
	@Autowired
	SalesChannelMapper salesChannelImpl;
	
	@Autowired
	AttachmentMapper attachmentImpl;
	
	@Autowired
	ProductPackageMapper productPackageImpl;
	
	@Autowired
	CustomerTransactionAnalysisMapper customerTransactionAnalysisImpl;
	
	@Autowired
	TransformMetadataMapper identifierImpl;
	
	@Autowired
	VendorTaxInvoiceTypeMapper vendorTaxInvoiceTypeImpl;
	
	@Autowired
	NeedsAnalysisMapper needsAnalysisImpl;
	
	@Autowired
	HouseholdMapper householdImpl;

	@Autowired
	private StringObjectMapper(
			RelatedPersonRepository relatedPersonRepository,
			AddressTypeRepository addressTypeRepository,
			CompanyApplicantRepository companyApplicantRepository,
			PersonApplicantRepository personApplicantRepository,
			TrustApplicantRepository trustApplicantRepository,
			RelatedCompanyRepository relatedCompanyRepository,
			RealEstateAssetRepository realEstateAssetRepository,
			NonRealEstateAssetRepository nonRealEstateAssetRepository,
			LiabilityRepository liabilityRepository,
			LoanDetailsRepository loanDetailsRepository,
			CompanyFinancialsRepository companyFinancialsRepository,
			MasterAgreementRepository masterAgreementRepository,
			InsuranceRepository insuranceRepository,
			LendingGuaranteeRepository lendingGuaranteeRepository,
			CompanyRepository companyRepository,
			ContinuingRepaymentRepository continuingRepaymentRepository,
			PaymentRepository paymentRepository,
			RegularRepaymentRepository regularRepaymentRepository,
			RateCompositionRepository rateCompositionRepository,
			AttachmentRepository attachmentRepository,
			CreditCardRepository creditCardRepository,
			DepositAccountRepository depositAccountRepository,
			CustomerTransactionAnalysisRepository customerTransactionAnalysisRepository,
			IdentifierRepository identifierRepository,
			VendorTaxInvoiceTypeRepository vendorTaxInvoiceTypeRepository,
			NeedsAnalysisRepository needsAnalysisRepository,
			HouseholdRepository householdRepository) 
	{
		this.relatedPersonRepository = relatedPersonRepository;
		this.addressTypeRepository = addressTypeRepository;
		this.companyApplicantRepository = companyApplicantRepository;
		this.personApplicantRepository = personApplicantRepository;
		this.trustApplicantRepository = trustApplicantRepository;
		this.relatedCompanyRepository = relatedCompanyRepository;
		this.realEstateAssetRepository = realEstateAssetRepository;
		this.nonRealEstateAssetRepository = nonRealEstateAssetRepository;
		this.liabilityRepository = liabilityRepository;
		this.loanDetailsRepository = loanDetailsRepository;
		this.companyFinancialsRepository = companyFinancialsRepository;
		this.masterAgreementRepository = masterAgreementRepository;
		this.insuranceRepository = insuranceRepository;
		this.lendingGuaranteeRepository = lendingGuaranteeRepository;
		this.companyRepository = companyRepository;
		this.continuingRepaymentRepository = continuingRepaymentRepository;
		this.paymentRepository = paymentRepository;
		this.regularRepaymentRepository = regularRepaymentRepository;
		this.rateCompositionRepository = rateCompositionRepository;
		this.attachmentRepository = attachmentRepository;
		this.creditCardRepository = creditCardRepository;
		this.depositAccountRepository = depositAccountRepository;
		this.customerTransactionAnalysisRepository = customerTransactionAnalysisRepository;
		this.identifierRepository = identifierRepository;
		this.vendorTaxInvoiceTypeRepository = vendorTaxInvoiceTypeRepository;
		this.needsAnalysisRepository = needsAnalysisRepository;
		this.householdRepository = householdRepository;
	}

	
	@ObjectToString
	public String objectToString(@Nullable Object object) {

		if (object != null) {
			
			// AddressType
			if(object instanceof AddressType) {
				AddressType address = (AddressType) object;
				return address.getUniqueID();
			}
			
			// Application.Related Person
			if(object instanceof Application.RelatedPerson) {
				Application.RelatedPerson relatedPerson = (Application.RelatedPerson) object;
				return relatedPerson.getUniqueID();
			}
			
			// Application.CompanyApplicant
			if(object instanceof Application.CompanyApplicant) {
				Application.CompanyApplicant companyApplicant = (Application.CompanyApplicant) object;
				return companyApplicant.getUniqueID();
			}
			
			// Application.Person Applicant
			if(object instanceof Application.PersonApplicant) {
				Application.PersonApplicant personApplicant = (Application.PersonApplicant) object;
				return personApplicant.getUniqueID();
			}
			
			// Application.Trust Applicant
			if(object instanceof Application.TrustApplicant) {
				Application.TrustApplicant trustApplicant = (Application.TrustApplicant) object;
				return trustApplicant.getUniqueID();
			}
			
			// Application.Related Company
			if(object instanceof Application.RelatedCompany) {
				Application.RelatedCompany relatedCompany = (Application.RelatedCompany) object;
				return relatedCompany.getUniqueID();
			}
			
			// Application.RealEstateAsset
			if(object instanceof Application.RealEstateAsset ) {
				Application.RealEstateAsset realEstate = (Application.RealEstateAsset) object;
				return realEstate.getUniqueID();
			}
			
			// Application.NonRealEstateAsset
			if(object instanceof Application.NonRealEstateAsset) {
				Application.NonRealEstateAsset nonRealEstate = (Application.NonRealEstateAsset) object;
				return nonRealEstate.getUniqueID();
			}
			

			// Application.Liability
			if(object instanceof Application.Liability) {
				Application.Liability liability = (Application.Liability) object;
				return liability.getUniqueID();
			}
			
			// Application.LoanDetails
			if(object instanceof Application.LoanDetails) {
				Application.LoanDetails loanDetails = (Application.LoanDetails) object;
				loanDetails.getUniqueID();
			}
			
			// Application.CompanyFinancials
			if(object instanceof Application.CompanyFinancials) {
				Application.CompanyFinancials companyFinancials = (Application.CompanyFinancials) object;
				return companyFinancials.getUniqueID();
			}
			
			// Application.masterAgreement
			if(object instanceof Application.MasterAgreement) {
				Application.MasterAgreement masterAgreement = (Application.MasterAgreement) object;
				return masterAgreement.getUniqueID();
			}
			
			
			// Insurance
			if(object instanceof Application.Insurance) {
				Application.Insurance insurance = (Application.Insurance) object;
				return insurance.getUniqueID();
			}
			
			// Application.LendingGuarantee
			if(object instanceof Application.LendingGuarantee) {
				Application.LendingGuarantee lendingGuarantee = (Application.LendingGuarantee) object;
				return lendingGuarantee.getUniqueID();
			}
			
			// Application.SalesChannel.Company
			if(Application.SalesChannel.Company.class.isInstance(object)) {
				Application.SalesChannel.Company company = (Application.SalesChannel.Company) object;
				return company.getUniqueID();
			}
			
			// Application.Liability.ContinuingRepayment
			if(object instanceof Application.Liability.ContinuingRepayment) {
				Application.Liability.ContinuingRepayment continuingRepayment = (Application.Liability.ContinuingRepayment) object;
				return continuingRepayment.getUniqueID();
			}
			
			// Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment
			if(object instanceof Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment) {
				Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment payment = (Application.LoanDetails.ProposedRepayment.StructuredPayments.Payment) object;
				return payment.getUniqueID();
			}
			
			// Application.LoanDetails.ProposedRepayment.RegularRepayment
			if(object instanceof Application.LoanDetails.ProposedRepayment.RegularRepayment) {
				Application.LoanDetails.ProposedRepayment.RegularRepayment regularRepayment = (Application.LoanDetails.ProposedRepayment.RegularRepayment) object;
				return regularRepayment.getUniqueID();
			}
			
			// Application.LoanDetails.RateComposition
			if(object instanceof Application.LoanDetails.RateComposition) {
				Application.LoanDetails.RateComposition rateComposition = (Application.LoanDetails.RateComposition) object;
				return rateComposition.getUniqueID();
			}
			
			// Attachment
			if(object instanceof Attachment) {
				Attachment attachment = (Attachment) object;
				return attachment.getUniqueID();
			}
			
			// Application.ProductPackage.CreditCard
			if(object instanceof Application.ProductPackage.CreditCard) {
				Application.ProductPackage.CreditCard creditCard = (Application.ProductPackage.CreditCard) object;
				return creditCard.getUniqueID();
			}
			
			// ProductPackage.DepositAccount
			if(object instanceof Application.ProductPackage.DepositAccount) {
				Application.ProductPackage.DepositAccount depositAccount = (Application.ProductPackage.DepositAccount) object;
				return depositAccount.getUniqueID();
			}
			
			// CustomerTransactionAnalysis
			if(object instanceof Application.CustomerTransactionAnalysis) {
				Application.CustomerTransactionAnalysis customerTransactionAnalysis = (Application.CustomerTransactionAnalysis) object;
				return customerTransactionAnalysis.getUniqueID();
			}
			
			// TransformMetadata.Identifier
			if(object instanceof TransformMetadata.Identifier) {
				TransformMetadata.Identifier identifier = (TransformMetadata.Identifier) object;
				return identifier.getUniqueID();
			}
			
			// VendorTaxInvoiceType
			if(object instanceof VendorTaxInvoiceType) {
				VendorTaxInvoiceType vendorTax = (VendorTaxInvoiceType) object;
				return vendorTax.getUniqueID();
			}
			
			// NeedsAnalysis
			if(object instanceof NeedsAnalysis) {
				NeedsAnalysis needsAnalysis = (NeedsAnalysis) object;
				return needsAnalysis.getUniqueID();
			}
			
			// Household
			if(object instanceof Application.Household) {
				Application.Household household = (Application.Household) object;
				return household.getUniqueID();
			}
		}
		return null;
	}
	
	@StringToObject
	public Object stringToObject(@Nullable String uniqueId) {
		
		if (uniqueId != null) {
		
			// AddressType
			com.moneycatcha.app.entity.AddressType address = addressTypeRepository.findAddressByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (address != null) {
				return addressTypeImpl.fromAddress(address);
//				return address;
			}
			
			// CompanyApplicant
			CompanyApplicant companyApplicant = companyApplicantRepository.findCompanyApplicantByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (companyApplicant != null) {
				
				return companyApplicantImpl.fromCompanyApplicant(companyApplicant);
				//return companyApplicants.get(0);
			}
	
			// RelatedPerson
			RelatedPerson relatedPerson = relatedPersonRepository.findRelatedPersonByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (relatedPerson != null) {
				
				return relatedPersonImpl.fromRelatedPerson(relatedPerson);
//				String addressUniqueId = relatedPerson.getContact().getXAddress();
//					
//				// AddressType
//				com.moneycatcha.cal.entity.AddressType address = addressTypeRepository.findAddressByUniqueID(addressUniqueId).orElse(null);
//				if (address != null) {
//					return address;
//				}

//				return (Object) relatedPersons.get(0);
				
			
			}
			
			// PersonApplicant
			PersonApplicant personApplicant = personApplicantRepository.findPersonApplicantByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (personApplicant != null) {
				return personApplicantImpl.fromPersonApplicant(personApplicant);
			}
			
			// TrustApplicant
			TrustApplicant trustApplicant = trustApplicantRepository.findTrustApplicantByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (trustApplicant != null) {
				return trustApplicantImpl.fromTrustApplicant(trustApplicant);
			}
			
			// RelatedCompany
			RelatedCompany relatedCompany = relatedCompanyRepository.findRelatedCompanyByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (relatedCompany != null) {
				return relatedCompanyImpl.fromRelatedCompany(relatedCompany);
			}
			
			// RealEstateAsset
			RealEstateAsset realEstateAsset = realEstateAssetRepository.findRealEstateAssetByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (realEstateAsset != null) {
				return realEstateAssetImpl.fromRealEstateAsset(realEstateAsset);
			}
			
			// NonRealEstateAsset
			NonRealEstateAsset nonRealEstateAsset = nonRealEstateAssetRepository.findNonRealEstateAssetByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (nonRealEstateAsset != null) {
				return nonRealEstateAssetImpl.fromNonRealEstateAsset(nonRealEstateAsset);
			}
			
			// Liability
			Liability liability = liabilityRepository.findLiabilityByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (liability != null) {
				return liabilityImpl.fromLiability(liability);
			}
			
			// LoanDetails
			LoanDetails loanDetails = loanDetailsRepository.findLoanDetailsByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (loanDetails != null) {
				return loanDetailsImpl.fromLoanDetails(loanDetails);
			}
			
			// CompanyFinancials
			CompanyFinancials companyFinancials = companyFinancialsRepository.findCompanyFinancialsByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (companyFinancials != null) {
				return companyFinancialsImpl.fromCompanyFinancials(companyFinancials);
			}
			
			// MasterAgreement
			MasterAgreement masterAgreement = masterAgreementRepository.findMasterAgreementByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (masterAgreement != null) {
				return masterAgreementImpl.fromMasterAgreement(masterAgreement);
			}
			
			// Insurance
			Insurance insurance = insuranceRepository.findInsuranceByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (insurance != null) {
				return insuranceImpl.fromInsurance(insurance);
			}
			
			
			// LendingGuarantee
			LendingGuarantee lendingGuarantee = lendingGuaranteeRepository.findLendingGuaranteeByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (lendingGuarantee != null) {
				return lendingGuaranteeImpl.fromLendingGuarantee(lendingGuarantee);
			}
			
			// SalesChannel.Company
			Company company = companyRepository.findCompanyByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (company != null) {
				
				return salesChannelImpl.fromCompany(company);
			}
			
			// Liability.ContinuingRepayment
			Liability.ContinuingRepayment continuingRepayment = continuingRepaymentRepository.findContinuingRepaymentByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (continuingRepayment != null) {
				return liabilityImpl.fromContinuingRepayment(continuingRepayment);
			}
			
			// LoanDetails.ProposedRepayment.StructuredPayments.Payment
			ProposedRepayment.StructuredPayments.Payment payment = paymentRepository.findPaymentByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (payment != null) {
				return loanDetailsImpl.fromPayment(payment);
			}
			
			// LoanDetails.ProposedRepayment.RegularRepayment
			ProposedRepayment.RegularRepayment regularRepayment = regularRepaymentRepository.findRegularRepaymentByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (regularRepayment != null) {
				return loanDetailsImpl.fromRegularRepayment(regularRepayment);
			}
			
			// LoanDetails.RateComposition
			RateComposition rateComposition = rateCompositionRepository.findRateCompositionByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (rateComposition != null) {
				return loanDetailsImpl.fromRateComposition(rateComposition);
			}
			
			// Attachment
			com.moneycatcha.app.entity.Attachment attachment = attachmentRepository.findAttachmentByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (attachment != null) {
				return attachmentImpl.fromAttachment(attachment);
			}
			
			// ProductPackage.CreditCard
			CreditCard creditCard = creditCardRepository.findCreditCardByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (creditCard != null) {
				return productPackageImpl.fromCreditCard(creditCard);
			}
			
			// ProductPackage.DepositAccount
			DepositAccount depositAccount = depositAccountRepository.findDepositAccountByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (depositAccount != null) {
				return productPackageImpl.fromDepositAccount(depositAccount);
			}
			
			
			// CustomerTransactionAnalysis
			CustomerTransactionAnalysis customerTransactionAnalysis = customerTransactionAnalysisRepository.findCustomerTransactionAnalysisByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (customerTransactionAnalysis != null) {
				return customerTransactionAnalysisImpl.fromCustomerTransactionAnalysis(customerTransactionAnalysis);
			}
			
			// TransformMetadata.Identifier
			Identifier identifier = identifierRepository.findIdentifierByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (identifier != null) {
				return identifierImpl.fromIdentifier(identifier);
			}

			// VendorTaxInvoiceType
			com.moneycatcha.app.entity.VendorTaxInvoiceType vendorTaxInvoice = vendorTaxInvoiceTypeRepository.findVendorTaxInvoiceByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (vendorTaxInvoice != null) {
				return vendorTaxInvoiceTypeImpl.fromVendorTaxInvoiceType(vendorTaxInvoice);
			}
			

			// NeedsAnalysis
			com.moneycatcha.app.entity.NeedsAnalysis needsAnalysis = needsAnalysisRepository.findNeedsAnalysisByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (needsAnalysis != null) {
				return needsAnalysisImpl.fromNeedsAnalysis(needsAnalysis);
			}

			// Household
			Household household = householdRepository.findHouseholdByUniqueID(uniqueId).stream().findFirst().orElse(null);
			if (household != null) {
				return householdImpl.fromHousehold(household);
			}
 
		}
		return null;
	}
}
