package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.entity.Business.ConcentrationRisk;
import com.moneycatcha.app.entity.Business.Diversification;
import com.moneycatcha.app.entity.Business.ImportExport;
import com.moneycatcha.app.entity.Business.PropertyInvestment;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.model.Message.Content.Application.CompanyApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
	uses = {DateMapper.class}
)
public interface BusinessMapper {

	BusinessMapper INSTANCE = Mappers.getMapper(BusinessMapper.class);

	/**
	 * Business
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "business.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "isFranchiseIntent", source = "business.isAFranchise")
	})
		
	Business toBusiness(Application.CompanyApplicant.Business business);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "isAFranchise", source = "entity.isFranchiseIntent")
	})
	Application.CompanyApplicant.Business fromBusiness(Business entity);

	
	/**
	 * Business - ConcentrationRisk  
	 */
	ConcentrationRisk toConcentrationRisk(CompanyApplicant.Business.ConcentrationRisk risk);
	
	@InheritInverseConfiguration
	CompanyApplicant.Business.ConcentrationRisk fromConcentrationRisk(ConcentrationRisk entity);

	
	
	/**
	 * Business - Diversification  
	 */
	@Mapping(target = "diversifiedDate", source = "diversification.diversifiedDate", qualifiedBy = GregorianToLocal.class)
	Diversification toDiversification(CompanyApplicant.Business.Diversification diversification);
	
	@InheritInverseConfiguration
	@Mapping(target = "diversifiedDate", source = "entity.diversifiedDate", qualifiedBy = LocalToGregorian.class)
	CompanyApplicant.Business.Diversification fromDiversification(Diversification entity);

	
	
	/**
	 * Business - ImportExport  
	 */
	ImportExport toImportExport(CompanyApplicant.Business.ImportExport diversification);
	
	@InheritInverseConfiguration
	CompanyApplicant.Business.ImportExport fromImportExport(ImportExport entity);

	
	
	
	/**
	 * Business - PropertyInvestment  
	 */
	PropertyInvestment toPropertyInvestment(CompanyApplicant.Business.PropertyInvestment property);
	
	@InheritInverseConfiguration
	CompanyApplicant.Business.PropertyInvestment fromPropertyInvestment(PropertyInvestment entity);

}
