package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.DeclaredIncome;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface DeclaredIncomeMapper {

	/**
	 * DeclaredIncome
	 */
	DeclaredIncome toDeclaredIncome(Application.CompanyApplicant.DeclaredIncome di);
	
	@InheritInverseConfiguration
	Application.CompanyApplicant.DeclaredIncome fromDeclaredIncome(DeclaredIncome entity);


}
