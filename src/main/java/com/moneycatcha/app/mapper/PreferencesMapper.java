package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.FundsAccessTypeDetails;
import com.moneycatcha.app.entity.InterestRateTypeDetails;
import com.moneycatcha.app.entity.OffsetAccount;
import com.moneycatcha.app.entity.Preferences;
import com.moneycatcha.app.entity.Reason;
import com.moneycatcha.app.entity.RepaymentTypeDetails;
import com.moneycatcha.app.entity.FundsAccessTypeDetails.Redraw;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedAndVariableRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.FixedRate;
import com.moneycatcha.app.entity.InterestRateTypeDetails.VariableRate;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestInAdvance;
import com.moneycatcha.app.entity.RepaymentTypeDetails.InterestOnly;
import com.moneycatcha.app.entity.RepaymentTypeDetails.LineOfCredit;
import com.moneycatcha.app.entity.RepaymentTypeDetails.PrincipalAndInterest;
import com.moneycatcha.app.entity.RepaymentTypeDetails.LineOfCredit.RepaymentPlan;
import com.moneycatcha.app.model.Message.Content;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = { 
		DurationTypeMapper.class,
		ResponsibleLendingTypeMapper.class,
		StringObjectMapper.class 
}
)
public interface PreferencesMapper {

	PreferencesMapper INSTANCE = Mappers.getMapper(PreferencesMapper.class);

	/**
	 *  Preferences
	 */
	Preferences toPreferences(Content.NeedsAnalysis.Preferences preferences);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences fromPreferences(Preferences entity);


	
	/**
	 *  FundsAccessTypeDetails
	 */
	FundsAccessTypeDetails toFundsAccessTypeDetails(Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails fundsAccess);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails fromFundsAccessTypeDetails(FundsAccessTypeDetails entity);
	
	
	/**
	 *  FundsAccessTypeDetails.OffsetAccount
	 */
	OffsetAccount toFundsOffsetAccount(Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.OffsetAccount offset);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.OffsetAccount fromFundsOffsetAccount(OffsetAccount entity);
	
	
	/**
	 *  FundsAccessTypeDetails.OffsetAccount.Reason
	 */
	Reason toFundsOffsetAccountReason(Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.OffsetAccount.Reason offset);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.OffsetAccount.Reason fromFundsOffsetAccountReason(Reason entity);
	
	
	/**
	 *  FundsAccessTypeDetails.Redraw
	 */
	Redraw toRedraw(Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.Redraw redraw);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.Redraw fromRedraw(Redraw entity);
	
	
	/**
	 *  FundsAccessTypeDetails.Redraw
	 */
	Reason toRedrawReason(Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.Redraw.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.FundsAccessTypeDetails.Redraw.Reason fromRedrawReason(Reason entity);
	
	
	
	
	/**
	 *  InterestRateTypeDetails
	 */
	InterestRateTypeDetails toInterestRateTypeDetails(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails interestRateType);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails fromInterestRateTypeDetails(InterestRateTypeDetails entity);
	
	
	/**
	 *  FixedAndVariableRate
	 */
	FixedAndVariableRate toFixedAndVariableRate(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedAndVariableRate fixedAndVariableRate);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedAndVariableRate fromFixedAndVariableRate(FixedAndVariableRate entity);
	
	
	/**
	 *  FixedAndVariableRate.Reason
	 *  
	 */
	Reason toFixedAndVariableReason(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedAndVariableRate.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedAndVariableRate.Reason fromFixedAndVariableReason(Reason entity);
	
	
	/**
	 *  FixedRate
	 */
	FixedRate toFixedRate(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedRate fixedRate);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedRate fromFixedRate(FixedRate entity);
	
	
	/**
	 *  FixedRate.Reason
	 *  
	 */
	Reason toFixedRateReason(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedRate.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.FixedRate.Reason fromFixedRateReason(Reason entity);

	
	/**
	 *  VariableRate
	 */
	VariableRate toVariableRate(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.VariableRate variableRate);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.VariableRate fromVariableRate(VariableRate entity);
	
	
	/**
	 *  VariableRate.Reason
	 *  
	 */
	Reason toVariableRateReason(Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.VariableRate.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.InterestRateTypeDetails.VariableRate.Reason fromVariableRateReason(Reason entity);
	

	
	/**
	 *  RepaymentTypeDetails
	 */
	RepaymentTypeDetails toRepaymentTypeDetails(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails repaymentType);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails fromRepaymentTypeDetails(RepaymentTypeDetails entity);
		
	
	
	/**
	 *  InterestInAdvance
	 */
	InterestInAdvance toInterestInAdvance(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestInAdvance interestInAdvance);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestInAdvance fromInterestInAdvance(InterestInAdvance entity);
		
	
	/**
	 *  InterestInAdvance.Reason
	 *  
	 */
	Reason toInterestInAdvanceReason(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestInAdvance.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestInAdvance.Reason fromInterestInAdvanceReason(Reason entity);
	
	
	
	/**
	 *  InterestOnly
	 */
	InterestOnly toInterestOnly(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestOnly interestOnly);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestOnly fromInterestOnly(InterestOnly entity);
		
	
	/**
	 *  InterestOnly.Reason
	 *  
	 */
	Reason toInterestOnlyReason(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestOnly.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.InterestOnly.Reason fromInterestOnlyReason(Reason entity);
	
	
	
	/**
	 *  LineOfCredit
	 */
	LineOfCredit toLineOfCredit(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit interestOnly);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit fromLineOfCredit(LineOfCredit entity);
		
	
	/**
	 *  LineOfCredit.Reason
	 *  
	 */
	Reason toLineOfCreditReason(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit.Reason fromLineOfCreditReason(Reason entity);
	
	
	/**
	 *  LineOfCredit.RepaymentPlan
	 *  
	 */
	RepaymentPlan toRepaymentPlan(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit.RepaymentPlan repaymentPlan);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.LineOfCredit.RepaymentPlan fromRepaymentPlan(RepaymentPlan entity);
	
	
	
	/**
	 *  PrincipalAndInterest
	 */
	PrincipalAndInterest toPrincipalAndInterest(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.PrincipalAndInterest principalAndInterest);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.PrincipalAndInterest fromLineOfCredit(PrincipalAndInterest entity);
		
	
	/**
	 *  PrincipalAndInterest.Reason
	 *  
	 */
	Reason toPrincipalAndInterestReason(Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.PrincipalAndInterest.Reason reason);
	
	@InheritInverseConfiguration
	Content.NeedsAnalysis.Preferences.RepaymentTypeDetails.PrincipalAndInterest.Reason fromPrincipalAndInterestReason(Reason entity);
	
	
	
}
