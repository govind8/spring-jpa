package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.CreditCard;
import com.moneycatcha.app.entity.DepositAccount;
import com.moneycatcha.app.entity.Features;
import com.moneycatcha.app.entity.ProductPackage;
import com.moneycatcha.app.entity.SupplementaryCardholder;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		StringObjectMapper.class
	})
public interface ProductPackageMapper {

	ProductPackageMapper INSTANCE = Mappers.getMapper(ProductPackageMapper.class);

	/**
	 * ProductPackage
	 */
	
	ProductPackage toProductPackage(Application.ProductPackage ppackage);
	
	@InheritInverseConfiguration
	Application.ProductPackage fromProductPackage(ProductPackage entity);
	
	
	/**
	 * List<ProductPackage>
	 */
	
	List<ProductPackage> toProductPackages(List<Application.ProductPackage> ppackages);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage> fromProductPackages(List<ProductPackage> entities);
	
	
	/**
	 * CreditCard
	 */
	
	@Mapping(target = "XCreditCard", source = "creditCard.XCreditCard", qualifiedBy = ObjectToString.class)
	CreditCard toCreditCard(Application.ProductPackage.CreditCard creditCard);
	
	@Mapping(target = "XCreditCard", source = "entity.XCreditCard", qualifiedBy = StringToObject.class)
	Application.ProductPackage.CreditCard fromCreditCard(CreditCard entity);
	
	
	/**
	 * List<CreditCard>
	 */
	
	List<CreditCard> toCreditCards(List<Application.ProductPackage.CreditCard> ppackages);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage.CreditCard> fromCreditCards(List<CreditCard> entities);
	
	
	/**
	 * CreditCard.Features
	 */
	@Mapping(target = "XPrimaryCardholder", source = "features.XPrimaryCardholder", qualifiedBy = ObjectToString.class)
	Features toFeatures(Application.ProductPackage.CreditCard.Features features);
	
	@Mapping(target = "XPrimaryCardholder", source = "entity.XPrimaryCardholder", qualifiedBy = StringToObject.class)
	Application.ProductPackage.CreditCard.Features fromFeatures(Features entity);
	

	/**
	 * SupplementaryCardholder
	 */
	
	@Mapping(target = "XSupplementaryCardholder", source = "supplementarycard.XSupplementaryCardholder", qualifiedBy = ObjectToString.class)
	SupplementaryCardholder toSupplementaryCardholder(Application.ProductPackage.CreditCard.Features.SupplementaryCardholder supplementarycard);
	
	@Mapping(target = "XSupplementaryCardholder", source = "entity.XSupplementaryCardholder", qualifiedBy = StringToObject.class)
	Application.ProductPackage.CreditCard.Features.SupplementaryCardholder fromSupplementaryCardholder(SupplementaryCardholder entity);
	
	
	/**
	 * List<SupplementaryCardholder>
	 */
	
	List<SupplementaryCardholder> toSupplementaryCardholders(List<Application.ProductPackage.CreditCard.Features.SupplementaryCardholder> cardHolders);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage.CreditCard.Features.SupplementaryCardholder> fromSupplementaryCardholders(List<SupplementaryCardholder> entities);
	

	/**
	 * DepositAccount
	 */
	
	@Mapping(target = "XDepositAccount", source = "depositAccount.XDepositAccount", qualifiedBy = ObjectToString.class)
	DepositAccount toDepositAccount(Application.ProductPackage.DepositAccount depositAccount);
	
	@Mapping(target = "XDepositAccount", source = "entity.XDepositAccount", qualifiedBy = StringToObject.class)
	Application.ProductPackage.DepositAccount fromDepositAccount(DepositAccount entity);
	
	
	/**
	 * List<DepositAccount>
	 */
	
	List<DepositAccount> toDepositAccounts(List<Application.ProductPackage.DepositAccount> depositAccounts);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage.DepositAccount> fromDepositAccounts(List<DepositAccount> entities);
	

	/**
	 * Liability
	 */
	@Mapping(target = "XLiability", source = "liability.XLiability", qualifiedBy = ObjectToString.class)
	ProductPackage.Liability toLiability(Application.ProductPackage.Liability liability);
	
	@Mapping(target = "XLiability", source = "entity.XLiability", qualifiedBy = StringToObject.class)
	Application.ProductPackage.Liability fromLiability(ProductPackage.Liability entity);
	
	
	/**
	 * List<Liability>
	 */
	
	List<ProductPackage.Liability> toLiabilities(List<Application.ProductPackage.Liability> liabilities);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage.Liability> fromLiabilities(List<ProductPackage.Liability> entities);
	
	
	/**
	 * LoanDetails
	 */
	@Mapping(target = "XLoanDetails", source = "loanDetails.XLoanDetails", qualifiedBy = ObjectToString.class)
	ProductPackage.LoanDetails toLoanDetails(Application.ProductPackage.LoanDetails loanDetails);
	
	@Mapping(target = "XLoanDetails", source = "entity.XLoanDetails", qualifiedBy = StringToObject.class)
	Application.ProductPackage.LoanDetails fromLoanDetails(ProductPackage.LoanDetails entity);
	
	
	/**
	 * List<LoanDetails>
	 */
	
	List<ProductPackage.LoanDetails> toLoanDetailsList(List<Application.ProductPackage.LoanDetails> detailsList);
	
	@InheritInverseConfiguration
	List<Application.ProductPackage.LoanDetails> fromLoanDetailsList(List<ProductPackage.LoanDetails> entities);
	
	
	
}	
