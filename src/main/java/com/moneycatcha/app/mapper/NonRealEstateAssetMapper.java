package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.AgriculturalAsset;
import com.moneycatcha.app.entity.Aircraft;
import com.moneycatcha.app.entity.Business;
import com.moneycatcha.app.entity.CleaningEquipment;
import com.moneycatcha.app.entity.ContractDetails;
import com.moneycatcha.app.entity.EarthMovingMiningAndConstruction;
import com.moneycatcha.app.entity.Encumbrance;
import com.moneycatcha.app.entity.EstimatedValue;
import com.moneycatcha.app.entity.FinancialAsset;
import com.moneycatcha.app.entity.HospitalityAndLeisure;
import com.moneycatcha.app.entity.ITAndAVEquipment;
import com.moneycatcha.app.entity.MaterialsHandlingAndLifting;
import com.moneycatcha.app.entity.MedicalEquipment;
import com.moneycatcha.app.entity.MobileComputing;
import com.moneycatcha.app.entity.MotorVehicle;
import com.moneycatcha.app.entity.NonRealEstateAsset;
import com.moneycatcha.app.entity.OfficeEquipment;
import com.moneycatcha.app.entity.OtherAsset;
import com.moneycatcha.app.entity.PPSR;
import com.moneycatcha.app.entity.PlantEquipmentAndIndustrial;
import com.moneycatcha.app.entity.Registration;
import com.moneycatcha.app.entity.RegistrationEvent;
import com.moneycatcha.app.entity.ToolsOfTrade;
import com.moneycatcha.app.mapper.DateMapper;
import com.moneycatcha.app.mapper.FinancialAccountTypeMapper;
import com.moneycatcha.app.mapper.FinancialAnalysisMapper;
import com.moneycatcha.app.mapper.FundsDisbursementTypeMapper;
import com.moneycatcha.app.mapper.PercentOwnedTypeMapper;
import com.moneycatcha.app.mapper.SecurityAgreementTypeMapper;
import com.moneycatcha.app.mapper.StringObjectMapper;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.GregorianToYear;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.mapper.annotations.YearToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		FinancialAnalysisMapper.class,
		FinancialAccountTypeMapper.class,
		FundsDisbursementTypeMapper.class,
		PercentOwnedTypeMapper.class,
		SecurityAgreementTypeMapper.class,
		StringObjectMapper.class
	})
public interface NonRealEstateAssetMapper {


	NonRealEstateAssetMapper INSTANCE = Mappers.getMapper(NonRealEstateAssetMapper.class);
	
	/**
	 * NonRealEstateAsset
	 */
	@Mappings({
		@Mapping(target = "XVendorTaxInvoice", source = "nra.XVendorTaxInvoice", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XCustomerTransactionAnalysis", source = "nra.XCustomerTransactionAnalysis", qualifiedBy = ObjectToString.class)
	})
	NonRealEstateAsset toNonRealEstateAsset(Application.NonRealEstateAsset nra);
	
	@Mappings({
		@Mapping(target = "XVendorTaxInvoice", source = "entity.XVendorTaxInvoice", qualifiedBy = ObjectToString.class),
		@Mapping(target = "XCustomerTransactionAnalysis", source = "entity.XCustomerTransactionAnalysis", qualifiedBy = StringToObject.class)
	})
	Application.NonRealEstateAsset fromNonRealEstateAsset(NonRealEstateAsset entity); 

	
	/**
	 * List<NonRealEstateAsset>
	 */
	List<NonRealEstateAsset> toNonRealEstateAssets(List<Application.NonRealEstateAsset> nras);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset> fromNonRealEstateAssets(List<NonRealEstateAsset> entities);
	
	
	/**
	 * AgriculturalAsset
	 */
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "agriculture.registrationExpiryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "agriculture.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "agriculture.year", qualifiedBy = GregorianToYear.class)
	})
	AgriculturalAsset toAgriculturalAsset(Application.NonRealEstateAsset.AgriculturalAsset agriculture);
	
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "entity.registrationExpiryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
		
	Application.NonRealEstateAsset.AgriculturalAsset fromAgriculturalAsset(AgriculturalAsset entity); 

	
	/**
	 * Aircraft
	 */
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "aircraft.registrationExpiryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "aircraft.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "aircraft.year", qualifiedBy = GregorianToYear.class)
	})
	Aircraft toAircraft(Application.NonRealEstateAsset.Aircraft aircraft);
	
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "entity.registrationExpiryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.Aircraft fromAircraft(Aircraft entity); 

	
	/**
	 * Business
	 */
	
	Business toBusiness(Application.NonRealEstateAsset.Business aircraft);
	
	Application.NonRealEstateAsset.Business fromBusiness(Business entity); 

	
	
	
	/**
	 * CleaningEquipment
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "cleaning.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "cleaning.year", qualifiedBy = GregorianToYear.class)
	})
	CleaningEquipment toCleaningEquipment(Application.NonRealEstateAsset.CleaningEquipment cleaning);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.CleaningEquipment fromCleaningEquipment(CleaningEquipment entity); 
	
	
	/**
	 * ContractDetails
	 */
	@Mappings({
		@Mapping(target = "gstAmount", source = "contract.GSTAmount"),
		@Mapping(target = "gstInclusive", source = "contract.GSTInclusive"),
		@Mapping(target = "gstOverwritten", source = "contract.GSTOverwritten"),
		@Mapping(target = "contractDate", source = "contract.contractDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "estimatedSettlementDate", source = "contract.estimatedSettlementDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XVendor", source = "contract.XVendor", qualifiedBy = ObjectToString.class)
	})
	ContractDetails toContractDetails(Application.NonRealEstateAsset.ContractDetails contract);
	
	@Mappings({
		@Mapping(target = "GSTAmount", source = "entity.gstAmount"),
		@Mapping(target = "GSTInclusive", source = "entity.gstInclusive"),
		@Mapping(target = "GSTOverwritten", source = "entity.gstOverwritten"),
		@Mapping(target = "contractDate", source = "entity.contractDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "estimatedSettlementDate", source = "entity.estimatedSettlementDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XVendor", source = "entity.XVendor", qualifiedBy = StringToObject.class)
	})
	Application.NonRealEstateAsset.ContractDetails fromContractDetails(ContractDetails entity); 
	
	
	/**
	 * EarthMovingMiningAndConstruction
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "earthMoving.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "earthMoving.year", qualifiedBy = GregorianToYear.class)
	})
	EarthMovingMiningAndConstruction toEarthMovingMiningAndConstruction(Application.NonRealEstateAsset.EarthMovingMiningAndConstruction earthMoving);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.EarthMovingMiningAndConstruction fromEarthMovingMiningAndConstruction(EarthMovingMiningAndConstruction entity); 
	
	
	/**
	 * Encumbrance
	 */
	@Mappings({
		@Mapping(target = "registrationDate", source = "encumbrance.registrationDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "nonrealEncumbranceType", source = "encumbrance.encumbranceType"),
		@Mapping(target = "nonrealSecurityPriority", source = "encumbrance.priority")
	})
	Encumbrance toEncumbrance(Application.NonRealEstateAsset.Encumbrance encumbrance);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "registrationDate", source = "entity.registrationDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "encumbranceType", source = "entity.nonrealEncumbranceType"),
		@Mapping(target = "priority", source = "entity.nonrealSecurityPriority")
	})
	Application.NonRealEstateAsset.Encumbrance fromEncumbrance(Encumbrance entity); 
	

	/**
	 * List<Encumbrance>
	 */
	List<Encumbrance> toEncumbrances(List<Application.NonRealEstateAsset.Encumbrance> encumbrances);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.Encumbrance> fromEncumbrances(List<Encumbrance> entities); 
	
	
	/**
	 * InFavourOf
	 */
	@Mapping(target = "XInFavourOf", source = "infavour.XInFavourOf", qualifiedBy = ObjectToString.class)
	Encumbrance.InFavourOf toInFavour(Application.NonRealEstateAsset.Encumbrance.InFavourOf infavour);
	
	@Mapping(target = "XInFavourOf", source = "entity.XInFavourOf", qualifiedBy = StringToObject.class)
	Application.NonRealEstateAsset.Encumbrance.InFavourOf fromInFavour(Encumbrance.InFavourOf entity); 
	

	/**
	 * List<Encumbrance.InFavourOf>
	 */
	List<Encumbrance.InFavourOf> toInFavourOfs(List<Application.NonRealEstateAsset.Encumbrance.InFavourOf> infavourofs);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.Encumbrance.InFavourOf> fromInFavourOfs(List<Encumbrance.InFavourOf> entities); 
	
	
	/**
	 * EstimatedValue
	 */
	EstimatedValue toInFavour(Application.NonRealEstateAsset.EstimatedValue infavour);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.EstimatedValue fromInFavour(EstimatedValue entity); 
	
	
	/**
	 * FinancialAsset
	 */
	FinancialAsset toFinancialAsset(Application.NonRealEstateAsset.FinancialAsset financialAsset);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.FinancialAsset fromFinancialAsset(FinancialAsset entity); 
	
	
	/**
	 * FinancialAsset.Shares
	 */
	FinancialAsset.Shares toShares(Application.NonRealEstateAsset.FinancialAsset.Shares shares);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.FinancialAsset.Shares fromFinancialAsset(FinancialAsset.Shares entity); 
	
	
	/**
	 * HospitalityAndLeisure
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "hospitality.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "hospitality.year", qualifiedBy = GregorianToYear.class)
	})
	HospitalityAndLeisure toHospitalityAndLeisure(Application.NonRealEstateAsset.HospitalityAndLeisure hospitality);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.HospitalityAndLeisure fromHospitalityAndLeisure(HospitalityAndLeisure entity); 
	

	/**
	 * NonRealEstateAsset.Insurance
	 */
	@Mapping(target = "XInsurance", source = "insurance.XInsurance", qualifiedBy = ObjectToString.class)
	NonRealEstateAsset.Insurance toInsurance(Application.NonRealEstateAsset.Insurance insurance);
	
	@Mapping(target = "XInsurance", source = "entity.XInsurance", qualifiedBy = StringToObject.class)
	Application.NonRealEstateAsset.Insurance fromInsurance(NonRealEstateAsset.Insurance entity); 
	
	
	/**
	 * List<NonRealEstateAsset.Insurance>
	 */
	List<NonRealEstateAsset.Insurance> toInsuranceList(List<Application.NonRealEstateAsset.Insurance> insurances);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.Insurance> fromInsuranceList(List<NonRealEstateAsset.Insurance> entities); 
	
	/**
	 * ITAndAVEquipment
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "itequipment.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "itequipment.year", qualifiedBy = GregorianToYear.class)
	})
	ITAndAVEquipment toITAndAVEquipment(Application.NonRealEstateAsset.ITAndAVEquipment itequipment);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.ITAndAVEquipment fromITAndAVEquipment(ITAndAVEquipment entity); 
	
	/**
	 * MaterialsHandlingAndLifting
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "materialsHandling.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "materialsHandling.year", qualifiedBy = GregorianToYear.class)
	})
	MaterialsHandlingAndLifting toMaterialsHandlingAndLifting(Application.NonRealEstateAsset.MaterialsHandlingAndLifting materialsHandling);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.MaterialsHandlingAndLifting fromMaterialsHandlingAndLifting(MaterialsHandlingAndLifting entity); 
	
	
	/**
	 * MedicalEquipment
	 */
	MedicalEquipment toMedicalEquipment(Application.NonRealEstateAsset.MedicalEquipment materialsHandling);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.MedicalEquipment fromMedicalEquipment(MedicalEquipment entity); 
	
	
	/**
	 * MobileComputing
	 */
	MobileComputing toMobileComputing(Application.NonRealEstateAsset.MobileComputing mobileComputing);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.MobileComputing fromMobileComputing(MobileComputing entity); 
	
	
	/**
	 * MotorVehicle
	 */
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "motorVehicle.registrationExpiryDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "motorVehicle.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "motorVehicle.year", qualifiedBy = GregorianToYear.class)
	})
	MotorVehicle toMotorVehicle(Application.NonRealEstateAsset.MotorVehicle motorVehicle);
	
	@Mappings({
		@Mapping(target = "registrationExpiryDate", source = "entity.registrationExpiryDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.MotorVehicle fromMotorVehicle(MotorVehicle entity); 
	
	
	/**
	 * OfficeEquipment
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "officeEquipment.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "officeEquipment.year", qualifiedBy = GregorianToYear.class)
	})
	OfficeEquipment toOfficeEquipment(Application.NonRealEstateAsset.OfficeEquipment officeEquipment);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.OfficeEquipment fromOfficeEquipment(OfficeEquipment entity); 
	
	
	/**
	 * OtherAsset
	 */
	OtherAsset toOtherAsset(Application.NonRealEstateAsset.OtherAsset officeEquipment);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.OtherAsset fromOtherAsset(OtherAsset entity); 
	
	
	/**
	 * PlantEquipmentAndIndustrial
	 */
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "plant.XGoodToBeUsedAddress", qualifiedBy = ObjectToString.class),
		@Mapping(target = "year", source = "plant.year", qualifiedBy = GregorianToYear.class)
	})
	PlantEquipmentAndIndustrial toPlantEquipmentAndIndustrial(Application.NonRealEstateAsset.PlantEquipmentAndIndustrial plant);
	
	@Mappings({
		@Mapping(target = "XGoodToBeUsedAddress", source = "entity.XGoodToBeUsedAddress", qualifiedBy = StringToObject.class),
		@Mapping(target = "year", source = "entity.year", qualifiedBy = YearToGregorian.class)
	})
	Application.NonRealEstateAsset.PlantEquipmentAndIndustrial fromPlantEquipmentAndIndustrial(PlantEquipmentAndIndustrial entity); 

	
	/**
	 * PPSR
	 */
	@Mappings({
		@Mapping(target = "pmsi", source = "ppsr.PMSI"),
		@Mapping(target = "ppsRegistrable", source = "ppsr.PPSRegistrable")
	})
	PPSR toPPSR(Application.NonRealEstateAsset.PPSR ppsr);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "PMSI", source = "entity.pmsi"),
		@Mapping(target = "PPSRegistrable", source = "entity.ppsRegistrable")
	})
	Application.NonRealEstateAsset.PPSR fromPPSR(PPSR entity); 

	
	/**
	 * Registration
	 */
	@Mappings({
		@Mapping(target = "registrationEndDateTime", source = "registration.registrationEndDateTime", qualifiedBy = GregorianToLocalDateTime.class),
		@Mapping(target = "registrationStartDateTime", source = "registration.registrationStartDateTime", qualifiedBy = GregorianToLocalDateTime.class)
	})
	Registration toRegistration(Application.NonRealEstateAsset.PPSR.Registration registration);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "registrationEndDateTime", source = "entity.registrationEndDateTime", qualifiedBy = LocalDateTimeToGregorian.class),
		@Mapping(target = "registrationStartDateTime", source = "entity.registrationStartDateTime", qualifiedBy = LocalDateTimeToGregorian.class)
	})
	Application.NonRealEstateAsset.PPSR.Registration fromRegistration(Registration entity); 
	

	/**
	 * List<Registration>
	 */
	List<Registration> toRegistrations(List<Application.NonRealEstateAsset.PPSR.Registration> registrations);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.PPSR.Registration> fromRegistrations(List<Registration> entities); 

	
	
	/**
	 * RegistrationEvent
	 */
	@Mappings({
		@Mapping(target = "ppsrTransactionID", source = "event.PPSRTransactionID"),
		@Mapping(target = "changeDateTime", source = "event.changeDateTime", qualifiedBy = GregorianToLocalDateTime.class)
	})
	RegistrationEvent toRegistrationEvent(Application.NonRealEstateAsset.PPSR.Registration.RegistrationEvent event);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "PPSRTransactionID", source = "entity.ppsrTransactionID"),
		@Mapping(target = "changeDateTime", source = "entity.changeDateTime", qualifiedBy = LocalDateTimeToGregorian.class)
	})
	Application.NonRealEstateAsset.PPSR.Registration.RegistrationEvent fromRegistrationEvent(RegistrationEvent entity); 
	

	/**
	 * List<RegistrationEvent>
	 */
	List<RegistrationEvent> toRegistrationEvents(List<Application.NonRealEstateAsset.PPSR.Registration.RegistrationEvent> events);
	
	@InheritInverseConfiguration
	List<Application.NonRealEstateAsset.PPSR.Registration.RegistrationEvent> fromRegistrationEvents(List<RegistrationEvent> entities); 

	
	/**
	 * ToolsOfTrade
	 */
	ToolsOfTrade toToolsOfTrade(Application.NonRealEstateAsset.ToolsOfTrade tools);
	
	@InheritInverseConfiguration
	Application.NonRealEstateAsset.ToolsOfTrade fromToolsOfTrade(ToolsOfTrade entity); 
	
	
	
}
