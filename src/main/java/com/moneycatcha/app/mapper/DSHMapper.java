package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.moneycatcha.app.entity.Arrears;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface DSHMapper {

	/**
	 * Liability.DSH
	 */
	Arrears toArrears(Application.Liability.Arrears arrears);
	
	@InheritInverseConfiguration
	Application.Liability.Arrears fromArrears(Arrears entity);

}
