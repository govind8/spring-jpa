package com.moneycatcha.app.mapper;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.DurationType;
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring")
public interface DurationTypeMapper {

	DurationTypeMapper INSTANCE = Mappers.getMapper(DurationTypeMapper.class);
	
	/**
	 * DurationType
	 */
	
	DurationType toDurationType(com.moneycatcha.app.model.DurationType durationType);
	
	// JPA - to - JAXB
	@InheritInverseConfiguration
	com.moneycatcha.app.model.DurationType fromDurationType(DurationType entity);

}
