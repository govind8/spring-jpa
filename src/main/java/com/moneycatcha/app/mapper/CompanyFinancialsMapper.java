package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.CompanyFinancials;
import com.moneycatcha.app.entity.CompanyFinancials.BalanceSheet;
import com.moneycatcha.app.entity.CompanyFinancials.CurrentMarketData;
import com.moneycatcha.app.entity.CompanyFinancials.ProfitAndLoss;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", uses = {DateMapper.class})
public interface CompanyFinancialsMapper {
	
	CompanyFinancialsMapper INSTANCE = Mappers.getMapper(CompanyFinancialsMapper.class);

	/**
	 * CompanyFinancials
	 */
	@Mappings({
		@Mapping(target = "startDate", source = "companyFinancials.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "endDate", source = "companyFinancials.endDate", qualifiedBy = GregorianToLocal.class)
	})
	CompanyFinancials toCompanyFinancials(Application.CompanyFinancials companyFinancials);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class)
	})
	Application.CompanyFinancials fromCompanyFinancials(CompanyFinancials entity);

	
	/**
	 * List<CompanyFinancials>
	 */
	List<CompanyFinancials> toCompanyFinancialsList(List<Application.CompanyFinancials> companyFinancialsList);
	
	@InheritInverseConfiguration
	List<Application.CompanyFinancials> fromCompanyFinancialsList(List<CompanyFinancials> entities);

	
	
	
	/**
	 * CompanyFinancials::BalanceSheet
	 */
	BalanceSheet toBalanceSheet(Application.CompanyFinancials.BalanceSheet balanceSheet);
	
	@InheritInverseConfiguration
	Application.CompanyFinancials.BalanceSheet fromBalanceSheet(BalanceSheet entity);

	
	
	/**
	 * CompanyFinancials::BalanceSheet::Assets
	 */
	BalanceSheet.Assets toAssets(Application.CompanyFinancials.BalanceSheet.Assets assets);
	
	@InheritInverseConfiguration
	Application.CompanyFinancials.BalanceSheet.Assets fromAssets(BalanceSheet.Assets entity);

	
	
	/**
	 * CompanyFinancials::BalanceSheet::Liabilities
	 */
	BalanceSheet.Liabilities toAssets(Application.CompanyFinancials.BalanceSheet.Liabilities liabilities);
	
	@InheritInverseConfiguration
	Application.CompanyFinancials.BalanceSheet.Liabilities fromAssets(BalanceSheet.Liabilities entity);

	
	
	/**
	 * CompanyFinancials::CurrentMarketData
	 */
	CurrentMarketData toCurrentMarketData(Application.CompanyFinancials.CurrentMarketData marketData);
	
	@InheritInverseConfiguration
	Application.CompanyFinancials.CurrentMarketData fromCurrentMarketData(CurrentMarketData entity);
	
		
	
	/**
	 * CompanyFinancials::ProfitAndLoss
	 */
	ProfitAndLoss toProfitAndLoss(Application.CompanyFinancials.ProfitAndLoss profitLoss);
	
	@InheritInverseConfiguration
	Application.CompanyFinancials.ProfitAndLoss fromProfitAndLoss(ProfitAndLoss entity);
		


}
