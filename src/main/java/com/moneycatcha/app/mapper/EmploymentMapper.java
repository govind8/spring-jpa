package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Employment;
import com.moneycatcha.app.entity.ForeignEmployed;
import com.moneycatcha.app.entity.NotEmployed;
import com.moneycatcha.app.entity.PAYG;
import com.moneycatcha.app.mapper.annotations.GregorianToLocal;
import com.moneycatcha.app.mapper.annotations.LocalToGregorian;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application.PersonApplicant;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		DateMapper.class,
		SelfEmployedMapper.class,
		StringObjectMapper.class
	})
public interface EmploymentMapper {

	EmploymentMapper INSTANCE = Mappers.getMapper(EmploymentMapper.class);

	/**
	 * Employment
	 */
	Employment toEmployment(PersonApplicant.Employment employment);
	
	@InheritInverseConfiguration
	PersonApplicant.Employment fromEmployment(Employment entity);
	
	
	/**
	 * List<Employment>
	 */
	List<Employment> toEmployments(List<PersonApplicant.Employment> employments);
	
	@InheritInverseConfiguration
	List<PersonApplicant.Employment> fromEmployments(List<Employment> entities);

	
	/**
	 * ForeignEmployed
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "foreignEmployed.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "gicsCode", source = "foreignEmployed.GICSCode"),
		@Mapping(target = "probationDateEnds", source = "foreignEmployed.probationDateEnds", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "probationDateStarts", source = "foreignEmployed.probationDateStarts", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "foreignEmployed.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XEmployer", source = "foreignEmployed.XEmployer", qualifiedBy = ObjectToString.class)
	})
	ForeignEmployed toForeignEmployed(PersonApplicant.Employment.ForeignEmployed foreignEmployed);
	
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "GICSCode", source = "entity.gicsCode"),
		@Mapping(target = "probationDateEnds", source = "entity.probationDateEnds", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "probationDateStarts", source = "entity.probationDateStarts", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XEmployer", source = "entity.XEmployer", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Employment.ForeignEmployed fromForeignEmployed(ForeignEmployed entity);
	
	
	/**
	 * ForeignEmployed.Income
	 */
	ForeignEmployed.Income toForeignEmployedIncome(PersonApplicant.Employment.ForeignEmployed.Income income);
	
	@InheritInverseConfiguration
	PersonApplicant.Employment.ForeignEmployed.Income fromForeignEmployedIncome(ForeignEmployed.Income entity);
	
	
	
	
	/**
	 * NotEmployed
	 */
	@Mappings({
		@Mapping(target = "endDate", source = "notEmployed.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "notEmployed.startDate", qualifiedBy = GregorianToLocal.class)
	})
	NotEmployed toNotEmployed(PersonApplicant.Employment.NotEmployed notEmployed);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class)
	})
	PersonApplicant.Employment.NotEmployed fromNotEmployed(NotEmployed entity);
	
	
	/**
	 * NotEmployed.Income
	 */
	@Mapping(target = "smsf", source = "income.SMSF")
	NotEmployed.Income toNotEmployedIncome(PersonApplicant.Employment.NotEmployed.Income income);
	
	@InheritInverseConfiguration
	@Mapping(target = "SMSF", source = "entity.smsf")
	PersonApplicant.Employment.NotEmployed.Income fromNotEmployedIncome(NotEmployed.Income entity);

	
	
	/**
	 * PAYG
	 */
	@Mappings({
		@Mapping(target = "anzscoOccupationCode", source = "payg.ANZSCOOccupationCode"),
		@Mapping(target = "gicsCode", source = "payg.GICSCode"),
		@Mapping(target = "endDate", source = "payg.endDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "probationDateEnds", source = "payg.probationDateEnds", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "probationDateStarts", source = "payg.probationDateStarts", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "startDate", source = "payg.startDate", qualifiedBy = GregorianToLocal.class),
		@Mapping(target = "XEmployer", source = "payg.XEmployer", qualifiedBy = ObjectToString.class)
	})
	PAYG toPayg(PersonApplicant.Employment.PAYG payg);
	
	@Mappings({
		@Mapping(target = "ANZSCOOccupationCode", source = "entity.anzscoOccupationCode"),
		@Mapping(target = "GICSCode", source = "entity.gicsCode"),
		@Mapping(target = "endDate", source = "entity.endDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "probationDateEnds", source = "entity.probationDateEnds", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "probationDateStarts", source = "entity.probationDateStarts", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "startDate", source = "entity.startDate", qualifiedBy = LocalToGregorian.class),
		@Mapping(target = "XEmployer", source = "entity.XEmployer", qualifiedBy = StringToObject.class)
	})
	PersonApplicant.Employment.PAYG fromPayg(PAYG entity);
	
	
	/**
	 * PAYG.Income
	 */
	PAYG.Income toPaygIncome(PersonApplicant.Employment.PAYG.Income income);
	
	@InheritInverseConfiguration
	PersonApplicant.Employment.PAYG.Income fromPaygIncome(PAYG.Income entity);

	
	
}
