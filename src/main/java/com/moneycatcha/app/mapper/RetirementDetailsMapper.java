package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Applicant;
import com.moneycatcha.app.entity.RepaymentOptions;
import com.moneycatcha.app.entity.RetirementDetails;
import com.moneycatcha.app.entity.RepaymentOptions.RecurringIncomeFromSuperannuationDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SaleOfAssetsDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SavingsDetails;
import com.moneycatcha.app.entity.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.NeedsAnalysis;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = { 
		StringObjectMapper.class 
	}
)
public interface RetirementDetailsMapper {

	RetirementDetailsMapper INSTANCE = Mappers.getMapper(RetirementDetailsMapper.class);

	
	/**
	 * RetirementDetails
	 */
	RetirementDetails toRetirementDetails(NeedsAnalysis.RetirementDetails retirementDetails);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails fromRetirementDetails(RetirementDetails entity);
	
	
	/**
	 * Applicant
	 */
	@Mapping(target = "XApplicant", source = "applicant.XApplicant", qualifiedBy = ObjectToString.class)
	Applicant toApplicant(NeedsAnalysis.RetirementDetails.Applicant applicant);
	
	@Mapping(target = "XApplicant", source = "entity.XApplicant", qualifiedBy = StringToObject.class)
	NeedsAnalysis.RetirementDetails.Applicant fromApplicant(Applicant entity);

	
	/**
	 * List<Applicant>
	 */
	List<Applicant> toApplicants(List<NeedsAnalysis.RetirementDetails.Applicant> applicants);
	
	@InheritInverseConfiguration
	List<NeedsAnalysis.RetirementDetails.Applicant> fromApplicants(List<Applicant> entities);
	
	
	/**
	 * Applicant.RepaymentOptions
	 */
	RepaymentOptions toApplicantRepaymentOptions(NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions repaymentOptions);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions fromApplicantRepaymentOptions(RepaymentOptions entity);
	
	
	/**
	 *  Applicant.RepaymentOptions.RecurringIncomeFromSuperannuationDetails
	 */
	RecurringIncomeFromSuperannuationDetails toRecurringIncomeFromSuperannuationDetails(NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.RecurringIncomeFromSuperannuationDetails recurring);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.RecurringIncomeFromSuperannuationDetails fromRecurringIncomeFromSuperannuationDetails(RepaymentOptions entity);
	
	
	/**
	 * Applicant.RepaymentOptions.SaleOfAssetsDetails
	 */
	SaleOfAssetsDetails toSaleOfAssetsDetails(NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SaleOfAssetsDetails saleofAssets);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SaleOfAssetsDetails fromSaleOfAssetsDetails(SaleOfAssetsDetails entity);
	

	/**
	 * Applicant.RepaymentOptions.SavingDetails
	 */
	SavingsDetails toSavingDetails(NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SavingsDetails savings);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SavingsDetails fromSavingDetails(SavingsDetails entity);
	

	/**
	 * Applicant.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails
	 */
	@Mappings({
		@Mapping(target="fpDocumentation", source="superannuation.financialPlannerDocumentation"),
		@Mapping(target="fpDocumentationDescription", source="superannuation.financialPlannerDocumentationDescription"),
		@Mapping(target="ssspSuperannuationAmount", source="superannuation.superStatementShowingProjectedSuperannuationAmount"),
		@Mapping(target="ssspSuperannuationAmountDescription", source="superannuation.superStatementShowingProjectedSuperannuationAmountDescription")
	})
	SuperannuationLumpSumFollowingRetirementDetails toSuperannuationLumpSumFollowingRetirementDetails(NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails superannuation);
	
	@InheritInverseConfiguration
	@Mappings({
		@Mapping(target="financialPlannerDocumentation", source="entity.fpDocumentation"),
		@Mapping(target="financialPlannerDocumentationDescription", source="entity.fpDocumentationDescription"),
		@Mapping(target="superStatementShowingProjectedSuperannuationAmount", source="entity.ssspSuperannuationAmount"),
		@Mapping(target="superStatementShowingProjectedSuperannuationAmountDescription", source="entity.ssspSuperannuationAmountDescription")
	})
	NeedsAnalysis.RetirementDetails.Applicant.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails fromSuperannuationLumpSumFollowingRetirementDetails(SuperannuationLumpSumFollowingRetirementDetails entity);
	


	/**
	 * RetirementDetails.RepaymentOptions
	 */
	RepaymentOptions toRetirementRepaymentOptions(NeedsAnalysis.RetirementDetails.RepaymentOptions repaymentOptions);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.RepaymentOptions fromRetirementRepaymentOptions(RepaymentOptions entity);
	
	

	/**
	 *  RetirementDetails.RepaymentOptions.RecurringIncomeFromSuperannuationDetails
	 */
	RecurringIncomeFromSuperannuationDetails toRecurring(NeedsAnalysis.RetirementDetails.RepaymentOptions.RecurringIncomeFromSuperannuationDetails recurring);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.RepaymentOptions.RecurringIncomeFromSuperannuationDetails fromRecurring(RecurringIncomeFromSuperannuationDetails entity);
	
	
	/**
	 * RetirementDetails.RepaymentOptions.SaleOfAssetsDetails
	 */
	SaleOfAssetsDetails toSaleOfAssets(NeedsAnalysis.RetirementDetails.RepaymentOptions.SaleOfAssetsDetails saleofAssets);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.RepaymentOptions.SaleOfAssetsDetails fromSaleOfAssets(SaleOfAssetsDetails entity);
	

	/**
	 * RetirementDetails.RepaymentOptions.SavingDetails
	 */
	SavingsDetails toSavings(NeedsAnalysis.RetirementDetails.RepaymentOptions.SavingsDetails savings);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.RepaymentOptions.SavingsDetails fromSavings(SavingsDetails entity);
	

	/**
	 * RetirementDetails.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails
	 */
	SuperannuationLumpSumFollowingRetirementDetails toSuperannuation(NeedsAnalysis.RetirementDetails.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails superannuation);
	
	@InheritInverseConfiguration
	NeedsAnalysis.RetirementDetails.RepaymentOptions.SuperannuationLumpSumFollowingRetirementDetails fromSuperannuation(SuperannuationLumpSumFollowingRetirementDetails entity);
	

}
