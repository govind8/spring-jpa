package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.Publisher;
import com.moneycatcha.app.entity.Software;
import com.moneycatcha.app.entity.Publisher.RelatedSoftware;
import com.moneycatcha.app.mapper.annotations.GregorianToLocalDateTime;
import com.moneycatcha.app.mapper.annotations.LocalDateTimeToGregorian;
import com.moneycatcha.app.model.Message;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = { 
		DateMapper.class,
		PhoneTypeMapper.class
	}
)
public interface PublisherMapper {

	PublisherMapper INSTANCE = Mappers.getMapper(PublisherMapper.class);

	/**
	 * Publisher
	 */
	
	@Mappings({
		@Mapping(target = "publishedDateTime", source = "publisher.publishedDateTime", qualifiedBy = GregorianToLocalDateTime.class),
		@Mapping(target = "lixiCode", source = "publisher.LIXICode")
	})
	Publisher toPublisher(Message.Publisher publisher);
	
	@Mappings({
		@Mapping(target = "publishedDateTime", source = "entity.publishedDateTime", qualifiedBy = LocalDateTimeToGregorian.class),
		@Mapping(target = "LIXICode", source = "entity.lixiCode")
	})
	Message.Publisher fromPublisher(Publisher entity);
	

	/**
	 * List<Publisher>
	 */
	List<Publisher> toPublisherList(List<Message.Publisher> publisher);
	
	@InheritInverseConfiguration
	List<Message.Publisher> fromPublisherList(List<Publisher> entity);

	
	/**
	 * Publisher.RelatedSoftware
	 */
	
	RelatedSoftware toRelatedSoftware(Message.Publisher.RelatedSoftware relatedSoftware);
	
	@InheritInverseConfiguration
	Message.Publisher.RelatedSoftware fromRelatedSoftware(RelatedSoftware entity);
	

	/**
	 * List<Publisher>
	 */
	List<RelatedSoftware> toRelatedSoftwareList(List<Message.Publisher.RelatedSoftware> publishers);
	
	@InheritInverseConfiguration
	List<Message.Publisher.RelatedSoftware> fromRelatedSoftwareList(List<RelatedSoftware> entity);

	
	/**
	 * Software
	 */
	
	Software toSoftware(Message.Publisher.Software software);
	
	@InheritInverseConfiguration
	Message.Publisher.Software fromSoftware(Software entity);

	
	
}
