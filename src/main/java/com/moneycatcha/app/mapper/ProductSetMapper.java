package com.moneycatcha.app.mapper;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.moneycatcha.app.entity.ProductSet;
import com.moneycatcha.app.entity.ProductSet.Contact;
import com.moneycatcha.app.entity.ProductSet.Product;
import com.moneycatcha.app.mapper.annotations.ObjectToString;
import com.moneycatcha.app.mapper.annotations.StringToObject;
import com.moneycatcha.app.model.Message.Content.Application;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel= "spring", 
uses = {
		StringObjectMapper.class
	})
public interface ProductSetMapper {

	ProductSetMapper INSTANCE = Mappers.getMapper(ProductSetMapper.class);

	/**
	 * ProductSet
	 */
	
	@Mapping(target = "XPrimaryApplicant", source = "productSet.XPrimaryApplicant", qualifiedBy = ObjectToString.class)
	ProductSet toProductSet(Application.ProductSet productSet);
	
	@Mapping(target = "XPrimaryApplicant", source = "entity.XPrimaryApplicant", qualifiedBy = StringToObject.class)
	Application.ProductSet fromProductSet(ProductSet entity);
	
	
	/**
	 * List<ProductSet>
	 */
	
	List<ProductSet> toProductSets(List<Application.ProductSet> productSets);
	
	@InheritInverseConfiguration
	List<Application.ProductSet> fromProductSets(List<ProductSet> entities);
	
	
	/**
	 * ProductSet.Contact
	 */
	@Mapping(target = "XMailingAddress", source = "contact.XMailingAddress", qualifiedBy = ObjectToString.class)
	Contact toContact(Application.ProductSet.Contact contact);
	
	@Mapping(target = "XMailingAddress", source = "entity.XMailingAddress", qualifiedBy = StringToObject.class)
	Application.ProductSet.Contact fromContact(Contact entity);
	
	
	/**
	 * ProductSet.Product
	 */
	@Mapping(target = "XFinancialProduct", source = "product.XFinancialProduct", qualifiedBy = ObjectToString.class)
	Product toProduct(Application.ProductSet.Product product);
	
	@Mapping(target = "XFinancialProduct", source = "entity.XFinancialProduct", qualifiedBy = StringToObject.class)
	Application.ProductSet.Product fromProduct(Product entity);
	

	/**
	 * List<ProductSet.Product>
	 */
	List<Product> toProducts(List<Application.ProductSet.Product> products);
	
	@InheritInverseConfiguration
	List<Application.ProductSet.Product> fromProducts(List<Product> entity);
	
	
}
