package com.moneycatcha.app.service;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.Message;
import com.moneycatcha.app.repository.MessageRepository;

@Service
public class MessageService {

	@Autowired
	MessageRepository messageRepository;
	
	/**
	 * 
	 * @param entity
	 * @return
	 * @throws EntityNotFoundException
	 */
	public void save(Message message)  {
		messageRepository.save(message);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public Optional<Message> getMessageById(Long id) {
		
		return messageRepository.findById(id);

	}
}
