package com.moneycatcha.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.AddressTypeMapper;
import com.moneycatcha.app.model.AddressType;
import com.moneycatcha.app.repository.AddressTypeRepository;

@Service
public class AddressService {

	
	@Autowired
	AddressTypeRepository repository;
	
	@Autowired
	AddressTypeMapper mapper;

	/**
	 * find address by primary key (long)
	 * @param id
	 * @return (model)
	 */
	public AddressType findById(Long id) throws EntityNotFoundException {
		
		Optional<com.moneycatcha.app.entity.AddressType> addressEntity = repository.findById(id);
		if (addressEntity.isPresent()) {
			return mapper.fromAddress(addressEntity.get());
		}
		
		return null;
	}
	
	/**
	 * find addresses by unique id
	 * @param uniqueId
	 * @return (model)
	 */
	public List<AddressType> findByUniqueId(String uniqueId) throws EntityNotFoundException {
		
		// model
		List<AddressType> addressesModel = new ArrayList<>();
		// entity
		List<com.moneycatcha.app.entity.AddressType> addressesEntity = repository.findAddressByUniqueID(uniqueId);
		
		if (!addressesEntity.isEmpty()) {
			addressesModel = mapper.fromAddresses(addressesEntity);
		}
		
		return addressesModel;
	}
	
	/**
	 * find addresses by city
	 * @param city
	 * @return (model)
	 */
	public List<AddressType> findByCity(String cityname) throws EntityNotFoundException {
		
		// model
		List<AddressType> addressesModel = new ArrayList<>();
		// entity
		List<com.moneycatcha.app.entity.AddressType> addressesEntity = repository.findAddressByCity(cityname);
		
		if (!addressesEntity.isEmpty()) {
			addressesModel = mapper.fromAddresses(addressesEntity);
		}
		
		return addressesModel;
	}


	/**
	 * 
	 * @param 
	 * @return
	 */
	public List<AddressType> findAll() throws EntityNotFoundException {
		// model
		List<AddressType> addressesModel = new ArrayList<>();
		// entity
		List<com.moneycatcha.app.entity.AddressType> addressesEntity = new ArrayList<>();
		
		repository.findAll().iterator().forEachRemaining(addressesEntity::add);
		if (!addressesEntity.isEmpty()) {
			addressesModel = mapper.fromAddresses(addressesEntity);
		}
		
		return addressesModel;
	}

}
