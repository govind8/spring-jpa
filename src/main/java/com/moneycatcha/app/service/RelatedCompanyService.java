package com.moneycatcha.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.RelatedCompany;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.RelatedCompanyMapper;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.repository.RelatedCompanyRepository;

@Service
public class RelatedCompanyService {
	
	@Autowired
	RelatedCompanyRepository repository;
	
	@Autowired
	RelatedCompanyMapper mapper;
	
	public Application.RelatedCompany findById(Long id) throws EntityNotFoundException {
		
		Optional<RelatedCompany> relatedCompanyEntity = repository.findById(id);
		if (relatedCompanyEntity.isPresent()) {
			return mapper.fromRelatedCompany(relatedCompanyEntity.get());
		}
		
		return null;
	}
	
	/**
	 * find all related companies
	 * @return (model)
	 */
	public List<Application.RelatedCompany> findAll() throws EntityNotFoundException {
		
		// model
		List<Application.RelatedCompany> relatedCompanyModel = new ArrayList<>();
		
		// entity
		List<RelatedCompany> relatedCompanyEntity = new ArrayList<>();
		
		repository.findAll().iterator().forEachRemaining(relatedCompanyEntity::add);
				
		if (!relatedCompanyEntity.isEmpty()) {
			relatedCompanyModel = mapper.fromRelatedCompanies(relatedCompanyEntity);
		}
		
		return relatedCompanyModel;
	}
	

}
