package com.moneycatcha.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.AccountVariation;
import com.moneycatcha.app.repository.AccountVariationRepository;

@Service
public class AccountVariationService {
	
	@Autowired
	AccountVariationRepository avRepository;
	
	public boolean save(AccountVariation entity) {
		avRepository.save(entity);
		return true;
	}
	
	public boolean saveAll(List<AccountVariation> accountVariations) {
		avRepository.saveAll(accountVariations);
		return true;
	}
	
	public Iterable<AccountVariation> findAll() {
		Iterable<AccountVariation> results = avRepository.findAll();
		return results;
	}
	
	public Optional<AccountVariation> findById(Long id) {
		return avRepository.findById(id);
	}
}
