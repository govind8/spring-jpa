package com.moneycatcha.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.CompanyApplicant;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.CompanyApplicantMapper;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.repository.CompanyApplicantRepository;

@Service
public class CompanyApplicantService {
	
	@Autowired
	CompanyApplicantRepository repository;
	
	@Autowired
	CompanyApplicantMapper mapper;
	
	public Application.CompanyApplicant findById(Long id) throws EntityNotFoundException {
		
		Optional<CompanyApplicant> companyApplicantEntity = repository.findById(id);
		if (companyApplicantEntity.isPresent()) {
			return mapper.fromCompanyApplicant(companyApplicantEntity.get());
		}
		
		return null;
	}
	
	/**
	 * find addresses by unique id
	 * @param uniqueId
	 * @return (model)
	 */
	public List<Application.CompanyApplicant> findByUniqueId(String uniqueId) throws EntityNotFoundException {
		
		// model
		List<Application.CompanyApplicant> companyApplicantModel = new ArrayList<>();
		// entity
		List<CompanyApplicant> companyApplicantEntity = repository.findCompanyApplicantByUniqueID(uniqueId);
		
		if (!companyApplicantEntity.isEmpty()) {
			companyApplicantModel = mapper.fromCompanyApplicants(companyApplicantEntity);
		}
		
		return companyApplicantModel;
	}
	
	/**
	 * find company applicant by company name
	 * @param city
	 * @return (model)
	 */
	public List<Application.CompanyApplicant> findByCompanyName(String companyName) throws EntityNotFoundException {
		
		// model
		List<Application.CompanyApplicant> companyApplicantModel = new ArrayList<>();
		// entity
		List<CompanyApplicant> companyApplicantEntity = repository.findByCompanyName(companyName);
		
		if (!companyApplicantEntity.isEmpty()) {
			companyApplicantModel = mapper.fromCompanyApplicants(companyApplicantEntity);
		}
		
		return companyApplicantModel;
	}
}
