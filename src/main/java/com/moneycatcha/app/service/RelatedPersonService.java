package com.moneycatcha.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.RelatedPerson;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.RelatedPersonMapper;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.repository.RelatedPersonRepository;

@Service
public class RelatedPersonService {

	@Autowired
	RelatedPersonRepository repository;
	
	@Autowired
	RelatedPersonMapper mapper;
	
	
	/**
	 * fetch the related person for the given id from the table
	 * @param id
	 * @return
	 * @throws EntityNotFoundException
	 */
	public Application.RelatedPerson findById(Long id) throws EntityNotFoundException {
		
		Optional<RelatedPerson> relatedPersonEntity = repository.findById(id);
		if (relatedPersonEntity.isPresent()) {
			return mapper.fromRelatedPerson(relatedPersonEntity.get());
		}
		
		return null;
	}
	
	/**
	 * find all related persons
	 * @return (model)
	 */
	public List<Application.RelatedPerson> findAll() throws EntityNotFoundException {
		
		// model
		List<Application.RelatedPerson> relatedPersonModel = new ArrayList<>();
		
		// entity
		List<RelatedPerson> relatedPersonEntity = new ArrayList<>();
		
		repository.findAll().iterator().forEachRemaining(relatedPersonEntity::add);
				
		if (!relatedPersonEntity.isEmpty()) {
			relatedPersonModel = mapper.fromRelatedPersons(relatedPersonEntity);
		}
		
		return relatedPersonModel;
	}
	
}
