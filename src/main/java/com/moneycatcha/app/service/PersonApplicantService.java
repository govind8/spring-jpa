package com.moneycatcha.app.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.moneycatcha.app.entity.PersonApplicant;
import com.moneycatcha.app.exception.EntityNotFoundException;
import com.moneycatcha.app.mapper.PersonApplicantMapper;
import com.moneycatcha.app.model.Message.Content.Application;
import com.moneycatcha.app.repository.PersonApplicantRepository;

@Service
public class PersonApplicantService {

	@Autowired
	PersonApplicantRepository repository;
	
	@Autowired
	PersonApplicantMapper mapper;
	
	public Application.PersonApplicant findById(Long id) throws EntityNotFoundException {
		
		Optional<PersonApplicant> personApplicantEntity = repository.findById(id);
		if (personApplicantEntity.isPresent()) {
			return mapper.fromPersonApplicant(personApplicantEntity.get());
		}
		
		return null;
	}
	
	/**
	 * find addresses by unique id
	 * @param uniqueId
	 * @return (model)
	 */
	public List<Application.PersonApplicant> findByUniqueId(String uniqueId) throws EntityNotFoundException {
		
		// model
		List<Application.PersonApplicant> personApplicantModel = new ArrayList<>();
		// entity
		List<PersonApplicant> personApplicantEntity = repository.findPersonApplicantByUniqueID(uniqueId);
		
		if (!personApplicantEntity.isEmpty()) {
			personApplicantModel = mapper.fromPersonApplicants(personApplicantEntity);
		}
		
		return personApplicantModel;
	}
	
	/**
	 * find addresses by country
	 * @param city
	 * @return (model)
	 */
	public List<Application.PersonApplicant> findByCountry(String countrycode) throws EntityNotFoundException {
		
		// model
		List<Application.PersonApplicant> personApplicantModel = new ArrayList<>();
		// entity
		List<PersonApplicant> personApplicantEntity = repository.findPersonApplicantByCountry(countrycode);
		
		if (!personApplicantEntity.isEmpty()) {
			personApplicantModel = mapper.fromPersonApplicants(personApplicantEntity);
		}
		
		return personApplicantModel;
	}
}
